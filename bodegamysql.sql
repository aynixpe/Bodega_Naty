-- phpMyAdmin SQL Dump
-- version 4.4.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 13-05-2015 a las 13:23:01
-- Versión del servidor: 5.6.19-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `bodega`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacen`
--

CREATE TABLE IF NOT EXISTS `almacen` (
  `idAlmacen` int(10) unsigned zerofill NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `almacen`
--

INSERT INTO `almacen` (`idAlmacen`, `nombre`, `estado`) VALUES
(0000000001, 'Principal', '\0'),
(0000000002, 'Tienda 2', '\0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacenarticulo`
--

CREATE TABLE IF NOT EXISTS `almacenarticulo` (
  `idAlmacenArticulo` int(10) unsigned zerofill NOT NULL,
  `idAlmacen` int(10) unsigned zerofill NOT NULL,
  `idArticulo` int(10) unsigned zerofill NOT NULL,
  `stock` int(11) NOT NULL,
  `stockMinimo` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `almacenarticulo`
--

INSERT INTO `almacenarticulo` (`idAlmacenArticulo`, `idAlmacen`, `idArticulo`, `stock`, `stockMinimo`) VALUES
(0000000001, 0000000001, 0000000001, 553, 0),
(0000000002, 0000000001, 0000000003, 30, 0),
(0000000003, 0000000001, 0000000002, 13, 0),
(0000000004, 0000000002, 0000000001, 13, 0),
(0000000005, 0000000002, 0000000002, 8, 0),
(0000000006, 0000000002, 0000000003, 4, 0),
(0000000007, 0000000001, 0000000868, 7, 5),
(0000000008, 0000000001, 0000000866, 0, 0),
(0000000009, 0000000001, 0000000880, 0, 5),
(0000000010, 0000000001, 0000000007, 0, 45),
(0000000011, 0000000001, 0000000009, 0, 0),
(0000000012, 0000000001, 0000000011, 0, 0),
(0000000013, 0000000001, 0000000025, 0, 0),
(0000000014, 0000000001, 0000000035, 0, 0),
(0000000015, 0000000001, 0000001030, 0, 0),
(0000000016, 0000000001, 0000001031, 0, 0),
(0000000017, 0000000001, 0000001033, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE IF NOT EXISTS `articulo` (
  `idArticulo` int(10) unsigned zerofill NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `presentacion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `stockMinimo` int(11) DEFAULT NULL,
  `tamanio` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `unidad` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `codigoBarras` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idCategoria` int(10) unsigned zerofill NOT NULL,
  `precioVenta` decimal(4,2) NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1034 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`idArticulo`, `nombre`, `presentacion`, `descripcion`, `stockMinimo`, `tamanio`, `unidad`, `codigoBarras`, `idCategoria`, `precioVenta`, `estado`) VALUES
(0000000001, 'Gaseosa Coca Cola 1.5 L', 'botella', 'Bebida gasificada saborizada', 5, '1.5', 'Litro', '123456789', 0000000002, 5.20, 'A'),
(0000000002, 'Jabón líquido', 'botella 1/2 litro', 'Sapolio hogar', 5, '1', 'Botella', '123456788', 0000000001, 2.40, 'A'),
(0000000003, 'Jabón líquido', 'botella 1 litro', 'Sapolio hogar', 5, '1', 'Botella', '123456787', 0000000001, 2.40, 'A'),
(0000000004, 'Don Victorio Canuto Rayado 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.20, 'A'),
(0000000005, 'Don Victorio Corbata Grande 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.20, 'A'),
(0000000006, 'Don Victorio Corbata Mediana 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.20, 'A'),
(0000000007, 'Don Victorio Lingúini gosso 500 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.30, 'A'),
(0000000008, 'Don Victorio Spaguetti 500 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.30, 'A'),
(0000000009, 'Molitalia Canuto 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.10, 'A'),
(0000000010, 'Molitalia Tallarin 500 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.20, 'A'),
(0000000011, 'Sayón Canuto Rallado 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000012, 'Sayón Codito 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000013, 'Sayón Corbata 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000014, 'Sayón Tallarín 500 g', 'bidón', '', NULL, NULL, '', NULL, 0000000007, 1.90, 'A'),
(0000000015, 'Capri 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.70, 'A'),
(0000000016, 'Capri 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.70, 'A'),
(0000000017, 'Capri 5 L', 'bidón', '', NULL, NULL, '', NULL, 0000000007, 29.00, 'A'),
(0000000018, 'Cil 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.70, 'A'),
(0000000019, 'Cil 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 1.70, 'A'),
(0000000020, 'Cil 5 L', 'bidón', '', NULL, NULL, '', NULL, 0000000007, 28.50, 'A'),
(0000000021, 'Cil 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.50, 'A'),
(0000000022, 'Cocinero 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.70, 'A'),
(0000000023, 'Cocinero 5 L', 'bidón', '', NULL, NULL, '', NULL, 0000000007, 27.50, 'A'),
(0000000024, 'Cocinero 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.50, 'A'),
(0000000025, 'Cristalino 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.70, 'A'),
(0000000026, 'Cristalino 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 1.70, 'A'),
(0000000027, 'Cristalino 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.50, 'A'),
(0000000028, 'Friol 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.50, 'A'),
(0000000029, 'Friol 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 1.50, 'A'),
(0000000030, 'Friol 5 L', 'bidón', '', NULL, NULL, '', NULL, 0000000007, 27.50, 'A'),
(0000000031, 'Friol 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.40, 'A'),
(0000000032, 'Ideal 1L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.80, 'A'),
(0000000033, 'Ideal 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 2.00, 'A'),
(0000000034, 'Ideal 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.80, 'A'),
(0000000035, 'Primor Clásico 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 7.00, 'A'),
(0000000036, 'Primor Clásico 5 L', 'bidón', '', NULL, NULL, '', NULL, 0000000007, 30.00, 'A'),
(0000000037, 'Primor Clásico 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.80, 'A'),
(0000000038, 'Primor Premium 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 7.40, 'A'),
(0000000039, 'Primor Premium 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.90, 'A'),
(0000000040, 'Sao 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.00, 'A'),
(0000000041, 'Sao 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 1.70, 'A'),
(0000000042, 'Sao 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.30, 'A'),
(0000000043, 'Tondero 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 5.50, 'A'),
(0000000044, 'Tondero 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.00, 'A'),
(0000000045, 'graté de atún florida 165 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 2.50, 'A'),
(0000000046, 'filete de atún florida 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 5.50, 'A'),
(0000000047, 'filete de atún gloria 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 5.50, 'A'),
(0000000048, 'sardinas en salsa de tomate real 425 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 7.00, 'A'),
(0000000049, 'filete de atún compass 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 5.50, 'A'),
(0000000050, 'graté de sardina gloria 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 2.50, 'A'),
(0000000051, 'trozos de atún monteverde 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 4.50, 'A'),
(0000000052, 'atún desmenuzado antartic 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 2.80, 'A'),
(0000000053, 'filete de atún marinero 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 4.50, 'A'),
(0000000054, 'salsa molitalia pomarola 160 g', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 1.80, 'A'),
(0000000055, 'salsa completa don vittorio 200 g', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 1.80, 'A'),
(0000000056, 'mayonesa alacena 500 ml', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 7.80, 'A'),
(0000000057, 'mayonesa alacena 200 ml', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 4.20, 'A'),
(0000000058, 'mayonesa alacena 115 ml', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 2.20, 'A'),
(0000000059, 'ketchup alacena 100 g', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 1.80, 'A'),
(0000000060, 'rocoto molido alacena 100 ml', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 1.80, 'A'),
(0000000061, 'crema de ají tarí 85 g', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 2.00, 'A'),
(0000000062, 'Don Victorio Canuto Rayado 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.20, 'A'),
(0000000063, 'Don Victorio Corbata Grande 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.20, 'A'),
(0000000064, 'Don Victorio Corbata Mediana 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.20, 'A'),
(0000000065, 'Don Victorio Lingúini gosso 500 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.30, 'A'),
(0000000066, 'Don Victorio Spaguetti 500 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.30, 'A'),
(0000000067, 'Molitalia Canuto 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.10, 'A'),
(0000000068, 'Molitalia Tallarin 500 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.20, 'A'),
(0000000069, 'Sayón Canuto Rallado 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000070, 'Sayón Codito 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000071, 'Sayón Corbata 250 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000072, 'Sayón Tallarín 500 g', 'bidón', '', NULL, NULL, '', NULL, 0000000007, 1.90, 'A'),
(0000000073, 'Capri 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.70, 'A'),
(0000000074, 'Capri 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.70, 'A'),
(0000000075, 'Capri 5 L', 'bidón', '', NULL, NULL, '', NULL, 0000000007, 29.00, 'A'),
(0000000076, 'Cil 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.70, 'A'),
(0000000077, 'Cil 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 1.70, 'A'),
(0000000078, 'Cil 5 L', 'bidón', '', NULL, NULL, '', NULL, 0000000007, 28.50, 'A'),
(0000000079, 'Cil 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.50, 'A'),
(0000000080, 'Cocinero 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.70, 'A'),
(0000000081, 'Cocinero 5 L', 'bidón', '', NULL, NULL, '', NULL, 0000000007, 27.50, 'A'),
(0000000082, 'Cocinero 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.50, 'A'),
(0000000083, 'Cristalino 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.70, 'A'),
(0000000084, 'Cristalino 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 1.70, 'A'),
(0000000085, 'Cristalino 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.50, 'A'),
(0000000086, 'Friol 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.50, 'A'),
(0000000087, 'Friol 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 1.50, 'A'),
(0000000088, 'Friol 5 L', 'bidón', '', NULL, NULL, '', NULL, 0000000007, 27.50, 'A'),
(0000000089, 'Friol 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.40, 'A'),
(0000000090, 'Ideal 1L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.80, 'A'),
(0000000091, 'Ideal 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 2.00, 'A'),
(0000000092, 'Ideal 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.80, 'A'),
(0000000093, 'Primor Clásico 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 7.00, 'A'),
(0000000094, 'Primor Clásico 5 L', 'bidón', '', NULL, NULL, '', NULL, 0000000007, 30.00, 'A'),
(0000000095, 'Primor Clásico 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.80, 'A'),
(0000000096, 'Primor Premium 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 7.40, 'A'),
(0000000097, 'Primor Premium 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.90, 'A'),
(0000000098, 'Sao 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 6.00, 'A'),
(0000000099, 'Sao 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 1.70, 'A'),
(0000000100, 'Sao 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.30, 'A'),
(0000000101, 'Tondero 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 5.50, 'A'),
(0000000102, 'Tondero 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 3.00, 'A'),
(0000000103, 'graté de atún florida 165 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 2.50, 'A'),
(0000000104, 'filete de atún florida 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 5.50, 'A'),
(0000000105, 'filete de atún gloria 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 5.50, 'A'),
(0000000106, 'sardinas en salsa de tomate real 425 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 7.00, 'A'),
(0000000107, 'filete de atún compass 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 5.50, 'A'),
(0000000108, 'graté de sardina gloria 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 2.50, 'A'),
(0000000109, 'trozos de atún monteverde 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 4.50, 'A'),
(0000000110, 'atún desmenuzado antartic 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 2.80, 'A'),
(0000000111, 'filete de atún marinero 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 4.50, 'A'),
(0000000112, 'salsa molitalia pomarola 160 g', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 1.80, 'A'),
(0000000113, 'salsa completa don vittorio 200 g', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 1.80, 'A'),
(0000000114, 'mayonesa alacena 500 ml', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 7.80, 'A'),
(0000000115, 'mayonesa alacena 200 ml', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 4.20, 'A'),
(0000000116, 'mayonesa alacena 115 ml', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 2.20, 'A'),
(0000000117, 'ketchup alacena 100 g', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 1.80, 'A'),
(0000000118, 'rocoto molido alacena 100 ml', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 1.80, 'A'),
(0000000119, 'crema de ají tarí 85 g', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 2.00, 'A'),
(0000000120, 'mostaza libbys 100 g', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 1.80, 'A'),
(0000000121, 'ketchup B&D 100 g', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 1.40, 'A'),
(0000000122, 'sopa instantánea ajinomen pollo 86 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000123, 'sopa instantánea ajinomen gallina 86 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000124, 'sopa instantánea ajinomen gallina picante 86 ', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000125, 'sopa instantánea ajinomen carne 86 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000126, 'sopa instantánea ajinomen oriental 86 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000127, 'mezcla para apanar maggie 80 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.50, 'A'),
(0000000128, 'harina favorita cocinera  180 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 0.70, 'A'),
(0000000129, 'sémola molitalia 200 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.20, 'A'),
(0000000130, 'sal yodada de mesa marina 1 Kg', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.10, 'A'),
(0000000131, 'harina espiga de oro 1 Kg', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 4.80, 'A'),
(0000000132, 'harina blanca flor 1 Kg', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 5.80, 'A'),
(0000000133, 'vinagre del firme blanco 1 L', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 3.00, 'A'),
(0000000134, 'vinagre del firme tinto 1 L', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 3.00, 'A'),
(0000000135, 'vinagre compass blanco 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 2.50, 'A'),
(0000000136, 'vinagre compass tinto 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000007, 2.50, 'A'),
(0000000137, 'sillao tito 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 2.50, 'A'),
(0000000138, 'sillao tito 150 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000139, 'maizena duryea 100 g', 'caja', '', NULL, NULL, '', NULL, 0000000007, 1.30, 'A'),
(0000000140, 'gelatina negrita piña 180 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.80, 'A'),
(0000000141, 'gelatina negrita fresa 180 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.80, 'A'),
(0000000142, 'gelatina negrita naranja 180 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.80, 'A'),
(0000000143, 'gelatina negrita piña 80 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.50, 'A'),
(0000000144, 'gelatina negrita fresa 80 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.50, 'A'),
(0000000145, 'gelatina negrita naranja 80 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.50, 'A'),
(0000000146, 'gelatina royal fresa 180 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 3.20, 'A'),
(0000000147, 'gelatina royal naranja 180 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 3.20, 'A'),
(0000000148, 'gelatina royal piña 180 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 3.20, 'A'),
(0000000149, 'gelatina royal limón 180 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 3.20, 'A'),
(0000000150, 'mazamorra negrita 170 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.50, 'A'),
(0000000151, 'flan royal chocolate 80 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.20, 'A'),
(0000000152, 'flan royal vainilla 80 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.20, 'A'),
(0000000153, 'flan royal manjar 80 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.20, 'A'),
(0000000154, 'pudín royal xxx g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 3.00, 'A'),
(0000000155, 'polvo de hornear universal 25 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000156, 'colapiz universal 20 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 1.70, 'A'),
(0000000157, 'manjar blanco bonlé 200 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 2.80, 'A'),
(0000000158, 'manjar blanco nestlé 200 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000007, 3.50, 'A'),
(0000000159, 'esencia de vainilla negrita 30 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 0.60, 'A'),
(0000000160, 'esencia de vainilla negrita 90 ml', 'botella', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000161, 'Café tostado y molido cafetal 50 g', 'sobre', '', NULL, NULL, '', NULL, 0000000007, 1.80, 'A'),
(0000000162, 'café para pasar altomayo 50 g', 'sobre', '', NULL, NULL, '', NULL, 0000000007, 1.50, 'A'),
(0000000163, 'refrescos negrita maracuyá 15 g', 'sobre', '', NULL, NULL, '', NULL, 0000000007, 0.80, 'A'),
(0000000164, 'refrescos negrita naranja 15 g', 'sobre', '', NULL, NULL, '', NULL, 0000000007, 0.80, 'A'),
(0000000165, 'refrescos negrita granadilla 15 g', 'sobre', '', NULL, NULL, '', NULL, 0000000007, 0.80, 'A'),
(0000000166, 'refrescos negrita chicha morada 15 g', 'sobre', '', NULL, NULL, '', NULL, 0000000007, 0.80, 'A'),
(0000000167, 'frutísimos negrita chicha morada 35 g', 'sobre', '', NULL, NULL, '', NULL, 0000000007, 1.00, 'A'),
(0000000168, 'mermelada gloria fresa 100 g', 'sachet', '', NULL, NULL, '', NULL, 0000000007, 1.20, 'A'),
(0000000169, 'duraznos en mitades monteverde 820 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 8.00, 'A'),
(0000000170, 'duraznos en mitades fanny 820 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 8.50, 'A'),
(0000000171, 'duraznos en mitades dos caballos 820 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 8.50, 'A'),
(0000000172, 'duraznos en mitades aconcagua 820 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 8.20, 'A'),
(0000000173, 'piña en rodajas florida 560 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 4.50, 'A'),
(0000000174, 'coctel de frutas florida 820 g', 'lata', '', NULL, NULL, '', NULL, 0000000007, 9.20, 'A'),
(0000000175, 'agua Cielo Con Gas 625 ml', 'botella', '', NULL, NULL, '', NULL, 0000000008, 1.00, 'A'),
(0000000176, 'agua Cielo Q10 350 ml', 'botella', '', NULL, NULL, '', NULL, 0000000008, 1.00, 'A'),
(0000000177, 'agua Cielo Q10 625 ml', 'botella', '', NULL, NULL, '', NULL, 0000000008, 1.50, 'A'),
(0000000178, 'agua Cielo Sin Gas 2.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000008, 2.50, 'A'),
(0000000179, 'agua Cielo Sin Gas 7 L', 'bidón', '', NULL, NULL, '', NULL, 0000000008, 6.50, 'A'),
(0000000180, 'agua Cielo Sin Gas 625ml', 'botella', '', NULL, NULL, '', NULL, 0000000008, 1.00, 'A'),
(0000000181, 'agua San Carlos 2.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000008, 2.80, 'A'),
(0000000182, 'agua San Carlos Con Gas 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000008, 1.30, 'A'),
(0000000183, 'agua San Carlos Sin Gas 600 ml', 'botella', '', NULL, NULL, '', NULL, 0000000008, 1.20, 'A'),
(0000000184, 'agua San Luis Con Gas 2.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000008, 3.00, 'A'),
(0000000185, 'agua San Luis Con Gas 625 ml', 'botella', '', NULL, NULL, '', NULL, 0000000008, 1.30, 'A'),
(0000000186, 'agua San Luis Sin Gas 20 L', 'caja', '', NULL, NULL, '', NULL, 0000000008, 21.50, 'A'),
(0000000187, 'agua San Luis Sin Gas 20 L', 'bidón', '', NULL, NULL, '', NULL, 0000000008, 17.50, 'A'),
(0000000188, 'agua San Luis Sin Gas 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000008, 1.20, 'A'),
(0000000189, 'agua San Luis Sin Gas 625 ml', 'botella', '', NULL, NULL, '', NULL, 0000000008, 1.30, 'A'),
(0000000190, 'agua San Luis Sin Gas 7 L', 'bidón', '', NULL, NULL, '', NULL, 0000000008, 8.50, 'A'),
(0000000191, 'agua San Lus Sin Gas 2.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000008, 3.00, 'A'),
(0000000192, 'agua San Mateo Con Gas 2.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000008, 3.20, 'A'),
(0000000193, 'agua San Mateo Con Gas 600 ml', 'botella', '', NULL, NULL, '', NULL, 0000000008, 1.40, 'A'),
(0000000194, 'agua San Mateo Sin Gas 2.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000008, 3.00, 'A'),
(0000000195, 'agua San Mateo Sin Gas 600 ml', 'botella', '', NULL, NULL, '', NULL, 0000000008, 1.30, 'A'),
(0000000196, 'agua San Mateo Sin Gas 7 L', 'bidón', '', NULL, NULL, '', NULL, 0000000008, 8.50, 'A'),
(0000000197, 'agua casinelli 20 L', 'bidón', '', NULL, NULL, '', NULL, 0000000008, 12.50, 'A'),
(0000000198, 'Papel higienico suave jumbo x 2', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 1.80, 'A'),
(0000000199, 'Papel higienico suave jumbo x 4', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 3.50, 'A'),
(0000000200, 'suave jumbo x 6', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 5.20, 'A'),
(0000000201, 'suave jumbo x 20 rollos (plancha)', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 16.50, 'A'),
(0000000202, 'Papel higienico suave extra x 2', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000203, 'Papel higienico suave extra x 20 – Plancha', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 12.80, 'A'),
(0000000204, 'papel higienico noble x 2', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 1.30, 'A'),
(0000000205, 'Papel higienico elite doble hoja naranja x 2', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000206, 'Papel higienico elite doble hoja naranja x 20', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 12.80, 'A'),
(0000000207, 'Papel higienico elite doble hoja maxima suavi', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 1.80, 'A'),
(0000000208, 'Papel higienico elite doble hoja maxima suavi', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 3.50, 'A'),
(0000000209, 'Papel higienico elite doble hoja maxima suavi', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 5.20, 'A'),
(0000000210, 'Papel higienico elite doble hoja maxima suavi', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 16.50, 'A'),
(0000000211, 'papel higienico elite ultra doble hoja purpur', 'rollo', '', NULL, NULL, '', NULL, 0000000006, 1.80, 'A'),
(0000000212, 'Toallas higienicas always pink pack 8 unidade', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.70, 'A'),
(0000000213, 'Toallas higienicas always active pack 8 unida', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.70, 'A'),
(0000000214, 'Toallas higienicas always suave pack 8 unidad', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000215, 'Toallas higienicas always noche regular pack ', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 5.50, 'A'),
(0000000216, 'Toallas higienicas always noche ultra fino pa', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 5.50, 'A'),
(0000000217, 'Toallas higienicas ladysoft delgada pack 10 u', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.70, 'A'),
(0000000218, 'Toallas higienicas ladysoft natural normal pa', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000219, 'Toallas higienicas ladysoft nocturna normal p', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.80, 'A'),
(0000000220, 'Toallas higienicas ladysoft normal pack 10 un', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000221, 'Toallas higienicas ladysoft planes pack 8 uni', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000222, 'Toallas higienicas nosotras invisible rapigel', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.70, 'A'),
(0000000223, 'toallas higienicas nosotras clasica tela pack', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.50, 'A'),
(0000000224, 'toallas higienicas nosotras natural alas tela', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.30, 'A'),
(0000000225, 'toallas higienicas kotex normal pack 10 unida', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.50, 'A'),
(0000000226, 'toallas higienicas nosotras natural buenas no', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 5.90, 'A'),
(0000000227, 'Practipañal plenitud pack 10 unidades', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.70, 'A'),
(0000000228, 'Pañales Pampers tripack m', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.40, 'A'),
(0000000229, 'Pañales Pampers tripack g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.70, 'A'),
(0000000230, 'Pañales Pampers tripack xg', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000231, 'Pañales Pampers tripack xxg', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.30, 'A'),
(0000000232, 'Pañales Pampers m x 11', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 6.90, 'A'),
(0000000233, 'Pañales Pampers g x 10', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 6.90, 'A'),
(0000000234, 'Pañales Pampers xg x 8', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 6.90, 'A'),
(0000000235, 'Pañales Pampers xxg x 8', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 6.90, 'A'),
(0000000236, 'Pañales huggies m', 'unidad', '', NULL, NULL, '', NULL, 0000000006, 0.80, 'A'),
(0000000237, 'Pañales huggies g', 'unidad', '', NULL, NULL, '', NULL, 0000000006, 0.90, 'A'),
(0000000238, 'Pañales huggies xg', 'unidad', '', NULL, NULL, '', NULL, 0000000006, 1.00, 'A'),
(0000000239, 'Pañales huggies xxg', 'unidad', '', NULL, NULL, '', NULL, 0000000006, 1.10, 'A'),
(0000000240, 'Pañales huggies m x -', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 44.00, 'A'),
(0000000241, 'Pañales huggies g x -', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 44.00, 'A'),
(0000000242, 'Pañales huggies xg x 52', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 44.00, 'A'),
(0000000243, 'Pañales huggies xxg x 48', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 44.00, 'A'),
(0000000244, 'Pañales Babysec ultra m', 'unidad', '', NULL, NULL, '', NULL, 0000000006, 0.70, 'A'),
(0000000245, 'Pañales Babysec ultra g', 'unidad', '', NULL, NULL, '', NULL, 0000000006, 0.80, 'A'),
(0000000246, 'Pañales Babysec ultra xg', 'unidad', '', NULL, NULL, '', NULL, 0000000006, 0.90, 'A'),
(0000000247, 'Pañales Babysec ultra xxg', 'unidad', '', NULL, NULL, '', NULL, 0000000006, 1.00, 'A'),
(0000000248, 'pañales babysec premium xxg x 14', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 13.00, 'A'),
(0000000249, 'pañales babysec premium xg x 16', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 13.00, 'A'),
(0000000250, 'pañales babysec premium g x 18', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 13.00, 'A'),
(0000000251, 'Pasta Dental Kolynos 22 ml', 'caja', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000252, 'Pasta Dental Kolynos 75 ml', 'caja', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000253, 'Pasta Dental Kolynos 100 ml', 'caja', '', NULL, NULL, '', NULL, 0000000006, 4.00, 'A'),
(0000000254, 'Pasta Dental Kolynos Herbal 90 g', 'caja', '', NULL, NULL, '', NULL, 0000000006, 2.00, 'A'),
(0000000255, 'Pasta Dental Colgate Triple Acción Menta Orig', 'caja', '', NULL, NULL, '', NULL, 0000000006, 1.60, 'A'),
(0000000256, 'Pasta Dental Colgate 90 g', 'caja', '', NULL, NULL, '', NULL, 0000000006, 2.10, 'A'),
(0000000257, 'Pasta Dental Colgate Total 12 50 ml', 'caja', '', NULL, NULL, '', NULL, 0000000006, 4.50, 'A'),
(0000000258, 'Pasta Dental Colgate Máxima Protección Anti C', 'caja', '', NULL, NULL, '', NULL, 0000000006, 2.90, 'A'),
(0000000259, 'Pasta Dental Colgate Máxima Protección Anti C', 'caja', '', NULL, NULL, '', NULL, 0000000006, 3.50, 'A'),
(0000000260, 'Pasta Dental Colgate Triple Acción Extra Blan', 'caja', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000261, 'Pasta Dental Colgate Triple Acción Menta Orig', 'caja', '', NULL, NULL, '', NULL, 0000000006, 4.50, 'A'),
(0000000262, 'Pasta Dental Colgate Triple Acción Menta Orig', 'caja', '', NULL, NULL, '', NULL, 0000000006, 7.60, 'A'),
(0000000263, 'Pasta Dental Oral B Complete 50 ml + H&S', 'caja', '', NULL, NULL, '', NULL, 0000000006, 2.90, 'A'),
(0000000264, 'Pasta Dental Oral B 1 2 3 50 ml', 'caja', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000265, 'Pasta Dental Oral B 3DWHITE 53 ml', 'caja', '', NULL, NULL, '', NULL, 0000000006, 4.50, 'A'),
(0000000266, 'Pasta Dental Oral B Complete con Enjuage Buca', 'caja', '', NULL, NULL, '', NULL, 0000000006, 4.30, 'A'),
(0000000267, 'Pasta Dental Dento 150 ml + Cepillo', 'caja', '', NULL, NULL, '', NULL, 0000000006, 4.00, 'A'),
(0000000268, 'Pasta Dental Dento 75 ml', 'caja', '', NULL, NULL, '', NULL, 0000000006, 2.00, 'A'),
(0000000269, 'Pasta Dental Dento 3 75 ml', 'caja', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000270, 'Pasta Dental Dentito Chicle Globo 75 ml', 'caja', '', NULL, NULL, '', NULL, 0000000006, 2.00, 'A'),
(0000000271, 'Pasta Dental Dentito Chicha Morada 75 ml', 'caja', '', NULL, NULL, '', NULL, 0000000006, 2.00, 'A'),
(0000000272, 'Jaboncillo Protex Avena 130 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000273, 'Jaboncillo Protex Fresh 130 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000274, 'Jaboncillo Protex Limpieza Profunda 130 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000275, 'Jaboncillo Protex Avena 75 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.80, 'A'),
(0000000276, 'Jaboncillo Protex Fresh 75 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.80, 'A'),
(0000000277, 'Jaboncillo Protex Limpieza Profunda 75 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.80, 'A'),
(0000000278, 'Jaboncillo Rexona Acqua Fresh 90 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.60, 'A'),
(0000000279, 'Jaboncillo Rexona Menta Fresh 90 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.60, 'A'),
(0000000280, 'Jaboncillo Rexona Sensible Fresh 90g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.60, 'A'),
(0000000281, 'Jaboncillo Rexona Nutritiva Fresh 90g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.60, 'A'),
(0000000282, 'Jaboncillo Rexona Acqua Fresh 125 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.70, 'A'),
(0000000283, 'Jaboncillo Rexona Menta Fresh 125 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.70, 'A'),
(0000000284, 'Jaboncillo Rexona Sensible Fresh 125 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.70, 'A'),
(0000000285, 'Jaboncillo Rexona Nutritiva Fresh 125 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.70, 'A'),
(0000000286, 'Jaboncillo LUX Tono Luminoso 90 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000287, 'Jaboncillo LUX Suavidad De Pétalos 90 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000288, 'Jaboncillo LUX Refréscate 90 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000289, 'Jaboncillo LUX Tono Luminoso 125 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000290, 'Jaboncillo LUX Suavidad De Pétalos 125 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000291, 'Jaboncillo LUX Refréscate 125 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000292, 'Jaboncillo Palmolive Sensación Humectante 75 ', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.40, 'A'),
(0000000293, 'Jaboncillo Palmolive Exfoliación Diaria 75 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.40, 'A'),
(0000000294, 'Jaboncillo Spa Humectante Coco 90 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000295, 'Jaboncillo Spa Energizante Naranja y Lima 90 ', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000296, 'Jaboncillo Nivea 90 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.30, 'A'),
(0000000297, 'Jaboncillo Etiquet Exfoliante 90 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.70, 'A'),
(0000000298, 'Jaboncillo Etiquet 90 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000299, 'Jaboncillo Camay Clásico 90 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000300, 'Jaboncillo Camay Pink 90 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000301, 'Jaboncillo Camay Romantic 90 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000302, 'Jaboncillo Neko Extra Protección 75 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.90, 'A'),
(0000000303, 'Jaboncillo Dermex Glicerina Kiwi Semillas Nat', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.20, 'A'),
(0000000304, 'Jaboncillo Dermex Glicerina Té Verde 100 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.20, 'A'),
(0000000305, 'Jaboncillo Dermex Glicerina Guaraná 100 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.20, 'A'),
(0000000306, 'Jaboncillo Dermex Glicerina Mujer 100 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.20, 'A'),
(0000000307, 'Jaboncillo Dermex Glicerina Maracuyá 100 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.20, 'A'),
(0000000308, 'Jaboncillo Jhonsons Avena y Aceite de Almendr', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000309, 'Jaboncillo Heno de Pravia Original 85 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.20, 'A'),
(0000000310, 'Jaboncillo Heno de Pravia Frescura y Protecci', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.20, 'A'),
(0000000311, 'Jaboncillo Heno de Pravia Con Crema Hidratant', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.20, 'A'),
(0000000312, 'Jaboncillo Heno de Pravia Original 150 g', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.60, 'A'),
(0000000313, 'Jaboncillo Heno de Pravia Frescura y Protecci', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.60, 'A'),
(0000000314, 'Jaboncillo Heno de Pravia Con Crema Hidratant', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.60, 'A'),
(0000000315, 'Jaboncillo Moncler 160 g', 'caja', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000316, 'Jaboncillo Dove Exfoliación Diaria 90 g', 'caja', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000317, 'Jaboncillo Dove Energiaznte 90 g', 'caja', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000318, 'Toallitas Húmedas babysec ultra 72 unidades', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 7.00, 'A'),
(0000000319, 'Toallitas húmedas pampers 64 und', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 9.80, 'A'),
(0000000320, 'Toallitas húmedas huggies clasic 70 und', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 6.20, 'A'),
(0000000321, 'toallitas húmedas huggies recien nacido 48 un', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 10.50, 'A'),
(0000000322, 'toallitas húmedas babysec premium 50 und', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 6.10, 'A'),
(0000000323, 'Shampoo pantene pro-v rizos definidos 400 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 18.00, 'A'),
(0000000324, 'Shampoo pantene pro-v hidrocauterización 400 ', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 18.00, 'A'),
(0000000325, 'Shampoo con Acondicionador pantene pro-v cuid', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 18.00, 'A'),
(0000000326, 'Shampoo pantene pro-v rizos definidos 200 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 12.00, 'A'),
(0000000327, 'Shampoo pantene pro-v hidrocauterización 200 ', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 12.00, 'A'),
(0000000328, 'Shampoo pantene pro-v restauración 200 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 12.00, 'A'),
(0000000329, 'Shampoo pantene pro-v cuidado brillo extremo ', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 12.00, 'A'),
(0000000330, 'Shampoo con Acondicionador pantene pro-v rizo', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 4.80, 'A'),
(0000000331, 'Shampoo con Acondicionador pantene pro-v cuid', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 4.80, 'A'),
(0000000332, 'Shampoo pantene pro-v restauración 100 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 4.80, 'A'),
(0000000333, 'Acondicinador Pantene Pro-V rizos definidos 2', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 11.60, 'A'),
(0000000334, 'Shampoo Savital Savila 530 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 14.00, 'A'),
(0000000335, 'Acondicionador Head & Shoulders fuerza rejuve', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 18.50, 'A'),
(0000000336, 'shampoo Head & Shoulders Manzana Fresh 400 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 18.90, 'A'),
(0000000337, 'shampoo Head & Shoulders Men 400 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 18.90, 'A'),
(0000000338, 'shampoo Head & Shoulders Limpieza Renovadora ', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 18.90, 'A'),
(0000000339, 'shampoo Head & Shoulders Protección Caída 400', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 18.90, 'A'),
(0000000340, 'shampoo Head & Shoulders Suave y Manejable 40', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 18.90, 'A'),
(0000000341, 'Crema para Peinar Pantane Pro-V Rizos definid', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 4.80, 'A'),
(0000000342, 'shampoo Head & Shoulders Limpieza Renovadora ', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 4.80, 'A'),
(0000000343, 'shampoo Head & Shoulders men 90 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 4.80, 'A'),
(0000000344, 'shampoo Head & Shoulders Men 200 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 12.50, 'A'),
(0000000345, 'shampoo Head & Shoulders Limpieza Renovadora ', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 12.50, 'A'),
(0000000346, 'shampoo Head & Shoulders Protección Caída 200', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 12.50, 'A'),
(0000000347, 'shampoo Head & Shoulders Suave y Manejable 20', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 12.50, 'A'),
(0000000348, 'Desodorante All Spice After Party 150ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 11.50, 'A'),
(0000000349, 'Desodorante All Spice Pure Sport 150ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 11.50, 'A'),
(0000000350, 'Desodorante All Spice Fresh 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000351, 'Desodorante All Spice Show Time 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000352, 'Desodorante All Spice After Party 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000353, 'Desodorante All Spice Champion 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000354, 'Desodorante Gillete 150 ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 12.50, 'A'),
(0000000355, 'Desodorante Rexona Teens Tropical Energy 108 ', 'spray', '', NULL, NULL, '', NULL, 0000000006, 5.80, 'A'),
(0000000356, 'Desodorante Rexona Teens Beauty 108 ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 5.80, 'A'),
(0000000357, 'Desodorante Rexona Women 108 ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 5.80, 'A'),
(0000000358, 'Desodorante Rexona UV8 50 ml x 2', 'caja', '', NULL, NULL, '', NULL, 0000000006, 10.20, 'A'),
(0000000359, 'Desodorante Rexona cotton 50 ml x 2', 'caja', '', NULL, NULL, '', NULL, 0000000006, 10.20, 'A'),
(0000000360, 'Crema Desodorante Etiquet 55 gr', 'caja', '', NULL, NULL, '', NULL, 0000000006, 12.50, 'A'),
(0000000361, 'Desodorante Rexona Men Sensitive 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000362, 'Desodorante Rexona Men Extreme 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000363, 'Desodorante Rexona Men UV8 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000364, 'Desodorante Rexona Men Quatom 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000365, 'Desodorante Rexona Men Xtracool 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000366, 'Desodorante Rexona Women Cotton 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000367, 'Desodorante Rexona Women extra fresh 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000368, 'Desodorante Rexona Women Nutritive 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000369, 'Desodorante Rexona Women Active Emotion 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000370, 'Desodorante Rexona Women Crystal 50 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 9.50, 'A'),
(0000000371, 'Crema para Peinar Sedal yuko 300 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 10.60, 'A'),
(0000000372, 'Crema para Peinar Sedal ceramidas 300 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 10.60, 'A'),
(0000000373, 'Shampoo konzil 375 ml + 200 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 16.50, 'A'),
(0000000374, 'Espuma para afeitar Gillete 250 ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 15.90, 'A'),
(0000000375, 'Talco Desodorante Hansaplast 250 gr', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 12.00, 'A'),
(0000000376, 'Crema Corporal Nivea Hidratación Express 400 ', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 17.30, 'A'),
(0000000377, 'Enguaje Bucal Listerine Antimanchas 500 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 15.50, 'A'),
(0000000378, 'Enguaje Bucal Listerine Zero 500 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 15.00, 'A'),
(0000000379, 'Desodorante Dove 169 ml + Jabón', 'spray', '', NULL, NULL, '', NULL, 0000000006, 12.50, 'A'),
(0000000380, 'Desodorante Dove Dermo Aclarante 169 ml + Jab', 'spray', '', NULL, NULL, '', NULL, 0000000006, 12.50, 'A'),
(0000000381, 'Shampoo Dove + Acondicionador Dove 400 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 26.50, 'A'),
(0000000382, 'Shampoo konzil 200 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 8.90, 'A'),
(0000000383, 'Nivea Creme 30 ml', 'lata', '', NULL, NULL, '', NULL, 0000000006, 3.20, 'A'),
(0000000384, 'Leche de Magnesia 120 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 8.50, 'A'),
(0000000385, 'Leche de Magnesia cereza 120 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 8.50, 'A'),
(0000000386, 'Leche de Magnesia ciruela 120 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 8.50, 'A'),
(0000000387, 'Leche de Magnesia durazno 120 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 8.50, 'A'),
(0000000388, 'Shampoo Amens 112 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 6.90, 'A'),
(0000000389, 'Quita esmalte sivela 30 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 1.20, 'A'),
(0000000390, 'Esmalte DLady 7 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 1.00, 'A'),
(0000000391, 'Quita esmalte Samy 30 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 1.50, 'A'),
(0000000392, 'Quita esmalte Samy 75 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000393, 'Enguaje Bucal Listerine Zero 180 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 5.50, 'A'),
(0000000394, 'Enguaje Bucal Listerine Antimanchas 250 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 7.90, 'A'),
(0000000395, 'Enguaje Bucal Listerine Zero 500 ml + 180 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 19.00, 'A'),
(0000000396, 'Enguaje Bucal Listerine Antimanchas 500 ml + ', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 21.50, 'A'),
(0000000397, 'Jabón Líquido Aval Pétalos de rosa 400 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 5.50, 'A'),
(0000000398, 'Jabón Líquido Aval Almendras y Miel 400 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 5.50, 'A'),
(0000000399, 'Jabón Líquido Aval Lavanda Francesa 400 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 5.50, 'A'),
(0000000400, 'Jabón Líquido Aval Pasión Cítrica 400 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 5.50, 'A'),
(0000000401, 'Jabón Líquido Aval Fresco Limón 400 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 5.50, 'A'),
(0000000402, 'Shampoo Jhonsons Baby 100 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 4.90, 'A'),
(0000000403, 'Talco Desodorante Hansaplast 120 gr', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 7.90, 'A'),
(0000000404, 'Espuma para afeitar Gillete Piel Sensible 322', 'spray', '', NULL, NULL, '', NULL, 0000000006, 17.90, 'A'),
(0000000405, 'Gel para peinar ego black 200 gr', 'pote', '', NULL, NULL, '', NULL, 0000000006, 6.40, 'A'),
(0000000406, 'Gel para peinar ego black 100 gr', 'pote', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000407, 'Gel para peinar ego xtreme 100 gr', 'pote', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000408, 'Gel para peinar ego xtreme 27 gr', 'sachet', '', NULL, NULL, '', NULL, 0000000006, 0.70, 'A'),
(0000000409, 'Desodorante Rexona Men UV8 98ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 5.80, 'A'),
(0000000410, 'Desodorante Rexona Men Active 98 ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 5.80, 'A'),
(0000000411, 'Desodorante Rexona Women 108 ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 5.80, 'A'),
(0000000412, 'Desodorante Nivea 43 gr', 'barra', '', NULL, NULL, '', NULL, 0000000006, 8.30, 'A'),
(0000000413, 'Desodorante Rexona Women Bamboo 30 ml', 'rollon', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000414, 'Desodorante Rexona Men UV8 30 ml', 'rollon', '', NULL, NULL, '', NULL, 0000000006, 3.00, 'A'),
(0000000415, 'Desodorante Gillete triple protection system ', 'rollon', '', NULL, NULL, '', NULL, 0000000006, 7.50, 'A'),
(0000000416, 'Desodorante Gillete sport 60 gr', 'rollon', '', NULL, NULL, '', NULL, 0000000006, 7.50, 'A'),
(0000000417, 'Desodorante Axe Black 90 ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 5.80, 'A'),
(0000000418, 'Desodorante Axe Exite 90 ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 5.80, 'A'),
(0000000419, 'Desodorante Axe Anarchy 90 ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 5.80, 'A'),
(0000000420, 'Oxigenta higora 60 ml', 'frasco', '', NULL, NULL, '', NULL, 0000000006, 2.60, 'A'),
(0000000421, 'Tinte para cabello pallete', 'caja', '', NULL, NULL, '', NULL, 0000000006, 12.50, 'A'),
(0000000422, 'Protector solar banana boat ultra defense 50 ', 'tubo', '', NULL, NULL, '', NULL, 0000000006, 8.50, 'A'),
(0000000423, 'Protector solar banana boat ultra defense kid', 'tubo', '', NULL, NULL, '', NULL, 0000000006, 8.50, 'A'),
(0000000424, 'Espuma de afeitar Aval hidratante 200 ml', 'spray', '', NULL, NULL, '', NULL, 0000000006, 7.50, 'A'),
(0000000425, 'Espuma de afeitar Aval extra proteccion 200 m', 'spray', '', NULL, NULL, '', NULL, 0000000006, 7.50, 'A'),
(0000000426, 'Shampoo Savital 90 ml', 'bolsa', '', NULL, NULL, '', NULL, 0000000006, 2.00, 'A'),
(0000000427, 'Prestobarba Guillette dos hojas', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000428, 'Prestobarba Guillette tres hojas hombre', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.50, 'A'),
(0000000429, 'Prestobarba Guillette tres hojas mujer', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.50, 'A'),
(0000000430, 'prestobarba shick dos', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.20, 'A'),
(0000000431, 'Prestobarba shick dos hojas hombre', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000432, 'prestobarba shick tres hojas hombre', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.50, 'A'),
(0000000433, 'Prestobarba shick dos hojas mujer', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000434, 'prestobarba shick tres hojas mujer', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 3.50, 'A'),
(0000000435, 'Prestobarba shick 4 hijas titanium', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 4.00, 'A'),
(0000000436, 'Prestobarba Colt II', 'paquete', '', NULL, NULL, '', NULL, 0000000006, 1.40, 'A'),
(0000000437, 'Cepillo de Dientes Kolynos Master Plus', 'estuche', '', NULL, NULL, '', NULL, 0000000006, 1.70, 'A'),
(0000000438, 'Cepillo de Dientes colgate extra suave 5 años', 'estuche', '', NULL, NULL, '', NULL, 0000000006, 2.00, 'A'),
(0000000439, 'cepillo de dientes oral b 1 2 3 con tapa', 'estuche', '', NULL, NULL, '', NULL, 0000000006, 1.70, 'A'),
(0000000440, 'cepillo de dientes pro doble acción', 'estuche', '', NULL, NULL, '', NULL, 0000000006, 5.00, 'A'),
(0000000441, 'cepillo de dientes pro kids suave', 'estuche', '', NULL, NULL, '', NULL, 0000000006, 4.50, 'A'),
(0000000442, 'cepillo de dientes colgate triple acción 1 2 ', 'estuche', '', NULL, NULL, '', NULL, 0000000006, 2.50, 'A'),
(0000000443, 'cepillo de dientes colgate premier', 'estuche', '', NULL, NULL, '', NULL, 0000000006, 1.80, 'A'),
(0000000444, 'cepillo de dientes dento galaxy niños', 'estuche', '', NULL, NULL, '', NULL, 0000000006, 2.00, 'A'),
(0000000445, 'desodorante lady speed stick double defense 1', 'sachet', '', NULL, NULL, '', NULL, 0000000006, 0.70, 'A'),
(0000000446, 'desodorante lady speed stick active 12 g', 'sachet', '', NULL, NULL, '', NULL, 0000000006, 0.70, 'A'),
(0000000447, 'desodorante rexona nutritive 10 g', 'sachet', '', NULL, NULL, '', NULL, 0000000006, 0.70, 'A'),
(0000000448, 'desodorante rexona men v8 15 g', 'sachet', '', NULL, NULL, '', NULL, 0000000006, 0.70, 'A'),
(0000000449, 'desodorante nivea men invisible 10 ml', 'sachet', '', NULL, NULL, '', NULL, 0000000006, 1.00, 'A'),
(0000000450, 'crema ponds aclaradora 15 g', 'sachet', '', NULL, NULL, '', NULL, 0000000006, 0.80, 'A'),
(0000000451, 'crema ponds de limpieza 10 g', 'sachet', '', NULL, NULL, '', NULL, 0000000006, 0.80, 'A'),
(0000000452, 'crema ponds rejuvenece 10 g', 'sachet', '', NULL, NULL, '', NULL, 0000000006, 0.80, 'A'),
(0000000453, 'crema ponds humectante 10 g', 'sachet', '', NULL, NULL, '', NULL, 0000000006, 0.80, 'A'),
(0000000454, 'pilas panasonic AA x2', 'paquete', '', NULL, NULL, '', NULL, 0000000009, 1.40, 'A'),
(0000000455, 'pilas panasonic AAA x2', 'paquete', '', NULL, NULL, '', NULL, 0000000009, 1.40, 'A'),
(0000000456, 'pilas panasonic D x2', 'paquete', '', NULL, NULL, '', NULL, 0000000009, 2.80, 'A'),
(0000000457, 'pilas duracell AAA x2', 'paquete', '', NULL, NULL, '', NULL, 0000000009, 4.50, 'A'),
(0000000458, 'pilas duracell AA x2', 'paquete', '', NULL, NULL, '', NULL, 0000000009, 4.50, 'A'),
(0000000459, 'batería duracell 9V x1', 'paquete', '', NULL, NULL, '', NULL, 0000000009, 9.50, 'A'),
(0000000460, '7up 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 4.00, 'A'),
(0000000461, '7up 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 6.50, 'A'),
(0000000462, '7up 355 ml', 'lata', '', NULL, NULL, '', NULL, 0000000002, 2.00, 'A'),
(0000000463, '7up 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.50, 'A'),
(0000000464, 'Aquarius Granadilla 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.00, 'A'),
(0000000465, 'Aquarius Granadilla 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 2.00, 'A'),
(0000000466, 'Aquarius Manzana 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.00, 'A'),
(0000000467, 'Aquarius Manzana 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 2.00, 'A'),
(0000000468, 'Aquarius Pera 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.00, 'A'),
(0000000469, 'Aquarius Pera 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 2.00, 'A'),
(0000000470, 'Chicha Morada Gloria 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 4.50, 'A'),
(0000000471, 'Chicha Morada Gloria 400 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.00, 'A'),
(0000000472, 'Cifrut granadilla 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 3.00, 'A'),
(0000000473, 'Cifrut Granadilla 250 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 0.60, 'A'),
(0000000474, 'Cifrut Granadilla 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.50, 'A'),
(0000000475, 'Cifrut Granadilla 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.20, 'A');
INSERT INTO `articulo` (`idArticulo`, `nombre`, `presentacion`, `descripcion`, `stockMinimo`, `tamanio`, `unidad`, `codigoBarras`, `idCategoria`, `precioVenta`, `estado`) VALUES
(0000000476, 'Cifrut Naranja 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 3.00, 'A'),
(0000000477, 'Cifrut Naranja 250 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 0.60, 'A'),
(0000000478, 'Cifrut Naranja 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.50, 'A'),
(0000000479, 'Cifrut Naranja 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.20, 'A'),
(0000000480, 'Cifrut Tropical 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 3.00, 'A'),
(0000000481, 'Cifrut Tropical 250 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 0.60, 'A'),
(0000000482, 'Cifrut Tropical 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.50, 'A'),
(0000000483, 'Cifrut Tropical 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.20, 'A'),
(0000000484, 'Coca Cola 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.00, 'A'),
(0000000485, 'Coca Cola 192 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 0.70, 'A'),
(0000000486, 'Coca Cola 2.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 6.50, 'A'),
(0000000487, 'Coca Cola 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 8.50, 'A'),
(0000000488, 'Coca Cola 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.80, 'A'),
(0000000489, 'Coca Cola Zero 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.80, 'A'),
(0000000490, 'Concordia Fresa 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 3.50, 'A'),
(0000000491, 'Concordia Fresa 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.50, 'A'),
(0000000492, 'Concordia Fresa 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.30, 'A'),
(0000000493, 'Concordia Naranja 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 3.50, 'A'),
(0000000494, 'Concordia Naranja 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.50, 'A'),
(0000000495, 'Concordia Naranja 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.30, 'A'),
(0000000496, 'Concordia Piña 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 3.50, 'A'),
(0000000497, 'Concordia Piña 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.50, 'A'),
(0000000498, 'Concordia Piña 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.30, 'A'),
(0000000499, 'Crush 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 4.00, 'A'),
(0000000500, 'Crush 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 6.00, 'A'),
(0000000501, 'Crush 450 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.50, 'A'),
(0000000502, 'Everves 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.00, 'A'),
(0000000503, 'Everves 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 2.00, 'A'),
(0000000504, 'Fanta 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 4.50, 'A'),
(0000000505, 'Fanta 192 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 0.70, 'A'),
(0000000506, 'Fanta 2.25 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.50, 'A'),
(0000000507, 'Fanta Kola Inglesa 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 6.50, 'A'),
(0000000508, 'Fanta Kola Inglesa 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.80, 'A'),
(0000000509, 'Fanta Naranja 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 6.50, 'A'),
(0000000510, 'Fanta Naranja 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.80, 'A'),
(0000000511, 'Fanta Zero 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.80, 'A'),
(0000000512, 'Frugos 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000002, 3.20, 'A'),
(0000000513, 'Frugos 1.5 L', 'caja', '', NULL, NULL, '', NULL, 0000000002, 4.20, 'A'),
(0000000514, 'Frugos 235 ml', 'caja', '', NULL, NULL, '', NULL, 0000000002, 1.00, 'A'),
(0000000515, 'Frugos 296 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.50, 'A'),
(0000000516, 'Frugos Kids', 'caja', '', NULL, NULL, '', NULL, 0000000002, 0.50, 'A'),
(0000000517, 'Inka Cola 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.00, 'A'),
(0000000518, 'Inka Cola 192 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 0.70, 'A'),
(0000000519, 'Inka Cola 2.25 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 6.50, 'A'),
(0000000520, 'Inka Cola 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 8.50, 'A'),
(0000000521, 'Inka Cola 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.80, 'A'),
(0000000522, 'Inka Cola Zero 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.80, 'A'),
(0000000523, 'Kr Cola 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.20, 'A'),
(0000000524, 'Kr Fresa 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.20, 'A'),
(0000000525, 'Kr Naranja 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.20, 'A'),
(0000000526, 'Kr Piña 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.20, 'A'),
(0000000527, 'Oro 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.00, 'A'),
(0000000528, 'Oro 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.00, 'A'),
(0000000529, 'Pepsi 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 4.50, 'A'),
(0000000530, 'Pepsi 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 6.50, 'A'),
(0000000531, 'Pepsi 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.50, 'A'),
(0000000532, 'Powerade Lima Limón 473 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.70, 'A'),
(0000000533, 'Powerade Mandarina 473 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.70, 'A'),
(0000000534, 'Powerade Mora 473 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.70, 'A'),
(0000000535, 'Powerade Tropical 473 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.70, 'A'),
(0000000536, 'Pulp durazno 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 2.50, 'A'),
(0000000537, 'Pulp 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 3.50, 'A'),
(0000000538, 'Pulp 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.00, 'A'),
(0000000539, 'Pulp Botella', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.50, 'A'),
(0000000540, 'Pulpin 145ml', 'caja', '', NULL, NULL, '', NULL, 0000000002, 0.50, 'A'),
(0000000541, 'Schweppers 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.00, 'A'),
(0000000542, 'Schweppers 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 2.00, 'A'),
(0000000543, 'Schweppers Citrus 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.00, 'A'),
(0000000544, 'Schweppers Citrus 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 2.00, 'A'),
(0000000545, 'Sprite 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 4.50, 'A'),
(0000000546, 'Sprite 2.25 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 6.50, 'A'),
(0000000547, 'Sprite 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.80, 'A'),
(0000000548, 'bebida energizante maltin power 269 ml', 'lata', '', NULL, NULL, '', NULL, 0000000002, 1.20, 'A'),
(0000000549, 'bebida energizante maltin power 330 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.20, 'A'),
(0000000550, 'gaseosa guaraná 355 ml', 'lata', '', NULL, NULL, '', NULL, 0000000002, 1.50, 'A'),
(0000000551, 'gaseosa guaraná 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.30, 'A'),
(0000000552, 'gaseosa guaraná 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.50, 'A'),
(0000000553, 'gaseosa casinelli 500 ml', 'botella', '', NULL, NULL, '', NULL, 0000000002, 1.00, 'A'),
(0000000554, 'gaseosa casinelli 1.5 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 2.50, 'A'),
(0000000555, 'gaseosa casinelli 3 L', 'botella', '', NULL, NULL, '', NULL, 0000000002, 5.00, 'A'),
(0000000556, 'cereales angel chock 22 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 0.50, 'A'),
(0000000557, 'cereales angel mel 22 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 0.50, 'A'),
(0000000558, 'cereales angel zuck 22 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 0.50, 'A'),
(0000000559, 'cereales angel frutt 22 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 0.50, 'A'),
(0000000560, 'cereales angel alfa y beto 22 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 0.50, 'A'),
(0000000561, 'cereales angel copix 20 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 0.40, 'A'),
(0000000562, 'cereales angel tito almohada 28 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 0.50, 'A'),
(0000000563, 'cereales angel fresia almohada 18 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 0.50, 'A'),
(0000000564, 'cereales angel fibra con salvado 22 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 0.50, 'A'),
(0000000565, 'cereales hummana ojuelas azucaradas 22 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 0.50, 'A'),
(0000000566, 'cereales hummana bolitas de chocolate 22 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 0.50, 'A'),
(0000000567, 'cereales hummana ojuelas de maíz 22 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 0.50, 'A'),
(0000000568, 'cereales angel chock 140 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 2.20, 'A'),
(0000000569, 'cereales angel mel 140 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 2.20, 'A'),
(0000000570, 'cereales angel zuck 140 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 2.20, 'A'),
(0000000571, 'cereales angel frutt 140 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 2.20, 'A'),
(0000000572, 'cereales angel alfa y beto 140 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 2.20, 'A'),
(0000000573, 'cereales angel copix fresa 130 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 2.20, 'A'),
(0000000574, 'cereales angel tito almohada 110 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 2.50, 'A'),
(0000000575, 'cereales angel meli almohada 110 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 2.50, 'A'),
(0000000576, 'cereales angel flakes 150 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 2.20, 'A'),
(0000000577, 'cereales angel frutilla almohada 110 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000010, 2.50, 'A'),
(0000000578, 'Chocolate Nesquik 15 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 0.70, 'A'),
(0000000579, 'Chocolate Milo 18 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 1.00, 'A'),
(0000000580, 'Café Kirma 10 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 1.00, 'A'),
(0000000581, 'Café Kirma 18 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 2.00, 'A'),
(0000000582, 'Cebada Eco 8 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 0.50, 'A'),
(0000000583, 'Café Nescafé decaf 9 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 1.00, 'A'),
(0000000584, 'café altomayo 10 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 1.00, 'A'),
(0000000585, 'Café nescafé tradición 10 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 1.10, 'A'),
(0000000586, 'café nescafé tradición 18 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 2.00, 'A'),
(0000000587, 'café nescafé kirma 7 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 0.70, 'A'),
(0000000588, 'Cocoa winter 11 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 0.50, 'A'),
(0000000589, 'cocoa winter 23 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 1.00, 'A'),
(0000000590, 'cocoa winter 45 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 2.00, 'A'),
(0000000591, 'cocoa winter 400 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 5.50, 'A'),
(0000000592, 'café kirma 200 g', 'lata', '', NULL, NULL, '', NULL, 0000000011, 22.00, 'A'),
(0000000593, 'café kirma 50 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 5.50, 'A'),
(0000000594, 'café nescafé tradición 50 g', 'frasco', '', NULL, NULL, '', NULL, 0000000011, 7.20, 'A'),
(0000000595, 'chocolate milo 200 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 8.90, 'A'),
(0000000596, 'chocolate milo 50 g', 'sobre', '', NULL, NULL, '', NULL, 0000000011, 2.80, 'A'),
(0000000597, 'chocolate milo 400 g', 'lata', '', NULL, NULL, '', NULL, 0000000011, 16.50, 'A'),
(0000000598, 'cebada eco 58 g', 'lata', '', NULL, NULL, '', NULL, 0000000011, 3.00, 'A'),
(0000000599, 'cebada eco 195 g', 'lata', '', NULL, NULL, '', NULL, 0000000011, 6.90, 'A'),
(0000000600, 'mermelada gloria fresa 320 g', 'frasco', '', NULL, NULL, '', NULL, 0000000011, 4.00, 'A'),
(0000000601, 'mermelada gloria fresa 1 Kg', 'frasco', '', NULL, NULL, '', NULL, 0000000011, 8.00, 'A'),
(0000000602, 'cebada kimbo 190 g', 'frasco', '', NULL, NULL, '', NULL, 0000000011, 5.80, 'A'),
(0000000603, 'hierba luisa Mc Collins 25 unds', 'caja', '', NULL, NULL, '', NULL, 0000000012, 2.00, 'A'),
(0000000604, 'manzanilla mc collins 25 unds', 'caja', '', NULL, NULL, '', NULL, 0000000012, 2.00, 'A'),
(0000000605, 'anís mc collins 25 unds', 'caja', '', NULL, NULL, '', NULL, 0000000012, 2.00, 'A'),
(0000000606, 'té mc collins 20 unds', 'caja', '', NULL, NULL, '', NULL, 0000000012, 1.20, 'A'),
(0000000607, 'hierba luisa Mc Collins 1 und', 'sobre', '', NULL, NULL, '', NULL, 0000000012, 0.10, 'A'),
(0000000608, 'manzanilla mc collins 1 und', 'sobre', '', NULL, NULL, '', NULL, 0000000012, 0.10, 'A'),
(0000000609, 'anís mc collins 1 und', 'sobre', '', NULL, NULL, '', NULL, 0000000012, 0.10, 'A'),
(0000000610, 'té mc collins 1 und', 'sobre', '', NULL, NULL, '', NULL, 0000000012, 0.10, 'A'),
(0000000611, 'Animalitos 150 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000612, 'Animalitos 60 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000004, 0.50, 'A'),
(0000000613, 'Casino Bum Chocolate', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.60, 'A'),
(0000000614, 'Casino Bum Menta', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.60, 'A'),
(0000000615, 'Casino Menta', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.60, 'A'),
(0000000616, 'Chocman 30 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.50, 'A'),
(0000000617, 'Chocosoda', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.60, 'A'),
(0000000618, 'Chocotravesura', 'bolsa', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000619, 'Doña Pepa', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.60, 'A'),
(0000000620, 'Full Cherry', 'bolsa', '', NULL, NULL, '', NULL, 0000000004, 0.50, 'A'),
(0000000621, 'Full Limon', 'bolsa', '', NULL, NULL, '', NULL, 0000000004, 0.50, 'A'),
(0000000622, 'Full Mandarina', 'bolsa', '', NULL, NULL, '', NULL, 0000000004, 0.50, 'A'),
(0000000623, 'Glacitas Chocolate', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.60, 'A'),
(0000000624, 'Glacitas Choconieve', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.60, 'A'),
(0000000625, 'Glacitas Fresa', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.60, 'A'),
(0000000626, 'Lentejas 16 g', 'caja', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000627, 'Margarita 180 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 1.30, 'A'),
(0000000628, 'Margarita 55 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.50, 'A'),
(0000000629, 'Mentitas', 'bolsa', '', NULL, NULL, '', NULL, 0000000004, 0.50, 'A'),
(0000000630, 'Miniglacitas Chocolate', 'bolsa', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000631, 'Miniglacitas Fresa', 'bolsa', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000632, 'Miniglacitas Vainilla', 'bolsa', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000633, 'Nik Chocolate 24 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.50, 'A'),
(0000000634, 'Nik Chocolate 77 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000635, 'Nik Fresa 24 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.50, 'A'),
(0000000636, 'Nik Fresa 77 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000637, 'Nik Vainilla 24 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.50, 'A'),
(0000000638, 'Nik Vainilla 77 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000639, 'Obsesion', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.70, 'A'),
(0000000640, 'Princesa 32 g', 'barra', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000641, 'Rellenitas Chocolate 42 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.40, 'A'),
(0000000642, 'Rellenitas Chocolate Fresa 42 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.40, 'A'),
(0000000643, 'Rellenitas Coco 42 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.40, 'A'),
(0000000644, 'Rellenitas Fresa 42 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.40, 'A'),
(0000000645, 'Rellenitas Menta 42 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.40, 'A'),
(0000000646, 'Soda Field 146 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 2.00, 'A'),
(0000000647, 'Soda Field 34 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.50, 'A'),
(0000000648, 'Sublime 16 g', 'barra', '', NULL, NULL, '', NULL, 0000000004, 0.60, 'A'),
(0000000649, 'Sublime 30 g', 'barra', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000650, 'Sublime 66 g', 'barra', '', NULL, NULL, '', NULL, 0000000004, 2.00, 'A'),
(0000000651, 'Sublime Almendra 24 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000652, 'Sublime Almendra 55 g', 'barra', '', NULL, NULL, '', NULL, 0000000004, 2.00, 'A'),
(0000000653, 'Sublime Blanco 30 g', 'barra', '', NULL, NULL, '', NULL, 0000000004, 1.20, 'A'),
(0000000654, 'Triangulo 32 g', 'barra', '', NULL, NULL, '', NULL, 0000000004, 1.20, 'A'),
(0000000655, 'Triangulo Doble Sensación 40 g', 'barra', '', NULL, NULL, '', NULL, 0000000004, 2.00, 'A'),
(0000000656, 'Vainilla Field 146 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 2.00, 'A'),
(0000000657, 'Vainilla Field 34 g', 'paquete', '', NULL, NULL, '', NULL, 0000000004, 0.50, 'A'),
(0000000658, 'Vizzio (más grande)', 'caja', '', NULL, NULL, '', NULL, 0000000004, 7.90, 'A'),
(0000000659, 'Vizzio 182 g', 'lata', '', NULL, NULL, '', NULL, 0000000004, 13.50, 'A'),
(0000000660, 'Vizzio 24 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000004, 1.00, 'A'),
(0000000661, 'Vizzio 72 g', 'caja', '', NULL, NULL, '', NULL, 0000000004, 3.90, 'A'),
(0000000662, 'Vizzio Bombóm 96 g', 'caja', '', NULL, NULL, '', NULL, 0000000004, 13.50, 'A'),
(0000000663, 'Leche Chocolatada Chicolac 145 g', 'sachet', '', NULL, NULL, '', NULL, 0000000003, 0.50, 'A'),
(0000000664, 'Gloria Calcio 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 1.80, 'A'),
(0000000665, 'Gloria Chocolatada 180 ml', 'caja', '', NULL, NULL, '', NULL, 0000000003, 1.20, 'A'),
(0000000666, 'Gloria Deslactosada 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 1.80, 'A'),
(0000000667, 'Gloria Light 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 4.20, 'A'),
(0000000668, 'Leche Condensada Gloria 395 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 4.80, 'A'),
(0000000669, 'Leche Condensada Nestlé 100 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 2.00, 'A'),
(0000000670, 'Leche Condensada Nestlé 397 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 5.20, 'A'),
(0000000671, 'Leche Gloria Azul 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 3.80, 'A'),
(0000000672, 'Leche Gloria Azul 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 1.70, 'A'),
(0000000673, 'Leche Gloria Azul 400 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 3.00, 'A'),
(0000000674, 'Leche Gloria Calcio 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 4.20, 'A'),
(0000000675, 'Leche Gloria Calcio 400 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 3.20, 'A'),
(0000000676, 'Leche Gloria Chocolatada 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 4.20, 'A'),
(0000000677, 'Leche Gloria Deslactosada 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 4.00, 'A'),
(0000000678, 'Leche Gloria Deslactosada 400 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 3.20, 'A'),
(0000000679, 'Leche Gloria Light 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 1.80, 'A'),
(0000000680, 'Leche Gloria Light 400 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 3.20, 'A'),
(0000000681, 'Leche Gloria Niños 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 4.20, 'A'),
(0000000682, 'Leche Gloria Niños 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 1.80, 'A'),
(0000000683, 'Leche Gloria Niños 400 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 3.20, 'A'),
(0000000684, 'Leche Ideal Amanecer 400 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 2.30, 'A'),
(0000000685, 'Leche Ideal Cremosita 400 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 3.00, 'A'),
(0000000686, 'Leche Laive Chocolatada 180 g', 'caja', '', NULL, NULL, '', NULL, 0000000003, 1.20, 'A'),
(0000000687, 'Leche Laive Deslactosada 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 4.00, 'A'),
(0000000688, 'Leche Laive Deslactosada 400 g', 'caja', '', NULL, NULL, '', NULL, 0000000003, 3.20, 'A'),
(0000000689, 'Leche Laive Familia 500 g', 'caja', '', NULL, NULL, '', NULL, 0000000003, 2.60, 'A'),
(0000000690, 'Leche Nan 2 Años 400 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 4.20, 'A'),
(0000000691, 'Leche Nan 3 Años 400 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 3.80, 'A'),
(0000000692, 'Leche Pura Vida 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 1.20, 'A'),
(0000000693, 'Leche Pura Vida 400 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 2.30, 'A'),
(0000000694, 'Leche Soyvida 170 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 0.80, 'A'),
(0000000695, 'Leche Soyvida 400 g', 'lata', '', NULL, NULL, '', NULL, 0000000003, 1.50, 'A'),
(0000000696, 'yogurt gloria fresa 190 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 1.00, 'A'),
(0000000697, 'yogurt gloria fresa 500 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.00, 'A'),
(0000000698, 'yogurt gloria durazno 190 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 1.00, 'A'),
(0000000699, 'yogurt gloria durazno 500 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.00, 'A'),
(0000000700, 'yogurt gloria vainilla 190 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 1.00, 'A'),
(0000000701, 'yogurt gloria vainilla 500 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.00, 'A'),
(0000000702, 'yogurt gloria lúcuma 190 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 1.00, 'A'),
(0000000703, 'yogurt gloria lúcuma 500 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.00, 'A'),
(0000000704, 'yogurt gloria guanábana 190 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 1.00, 'A'),
(0000000705, 'yogurt gloria  guanábana 500 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.00, 'A'),
(0000000706, 'yogurt gloria fresa 1 Kg', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.00, 'A'),
(0000000707, 'yogurt gloria durazno 1 Kg', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.00, 'A'),
(0000000708, 'yogurt gloria vainilla 1 Kg', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.00, 'A'),
(0000000709, 'yogurt gloria lúcuma 1 Kg', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.00, 'A'),
(0000000710, 'yogurt gloria guanábana 1 Kg', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.00, 'A'),
(0000000711, 'yogurt gloria acti bio granadilla con linasa ', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.50, 'A'),
(0000000712, 'yogurt gloria acti bio fresa con linasa 500 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.50, 'A'),
(0000000713, 'yogurt gloria acti bio frutos secos con linas', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.50, 'A'),
(0000000714, 'yogurt gloria acti bio granadilla con linasa ', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.50, 'A'),
(0000000715, 'yogurt gloria acti bio fresa con linasa 1 Kg', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.50, 'A'),
(0000000716, 'yogurt gloria acti bio frutos secos con linas', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.50, 'A'),
(0000000717, 'yogurt gloria batimix con ojuelas azucaradas ', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.50, 'A'),
(0000000718, 'yogurt gloria batimix con bolitas de chocolat', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.50, 'A'),
(0000000719, 'yogurt laive fresa 190 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 1.00, 'A'),
(0000000720, 'yogurt laive fresa 500 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.00, 'A'),
(0000000721, 'yogurt laive durazno 190 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 1.00, 'A'),
(0000000722, 'yogurt laive durazno 500 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.00, 'A'),
(0000000723, 'yogurt laive vainilla 190 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 1.00, 'A'),
(0000000724, 'yogurt laive vainilla 500 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.00, 'A'),
(0000000725, 'yogurt laive lúcuma 190 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 1.00, 'A'),
(0000000726, 'yogurt laive lúcuma 500 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.00, 'A'),
(0000000727, 'yogurt laive guanábana 190 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 1.00, 'A'),
(0000000728, 'yogurt laive  guanábana 500 g', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.00, 'A'),
(0000000729, 'yogurt laive fresa 1 Kg', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.00, 'A'),
(0000000730, 'yogurt laive durazno 1 Kg', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.00, 'A'),
(0000000731, 'yogurt laive vainilla 1 Kg', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.00, 'A'),
(0000000732, 'yogurt laive lúcuma 1 Kg', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.00, 'A'),
(0000000733, 'yogurt laive guanábana 1 Kg', 'botella', '', NULL, NULL, '', NULL, 0000000003, 5.00, 'A'),
(0000000734, 'yogurt laive laivemix con ojuelas azucaradas ', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.50, 'A'),
(0000000735, 'yogurt laive laivemix con bolitas de chocolat', 'botella', '', NULL, NULL, '', NULL, 0000000003, 3.50, 'A'),
(0000000736, 'yogurt laive yopimix con ojuelas azucaradas 1', 'botella', '', NULL, NULL, '', NULL, 0000000003, 1.50, 'A'),
(0000000737, 'yogurt laive yopimix con bolitas de chocolate', 'botella', '', NULL, NULL, '', NULL, 0000000003, 1.50, 'A'),
(0000000738, 'Leche Chocolatada Chicolac 180 ml', 'caja', '', NULL, NULL, '', NULL, 0000000003, 1.20, 'A'),
(0000000739, 'Leche Chocolatada UHT Laive 180 ml', 'caja', '', NULL, NULL, '', NULL, 0000000003, 1.20, 'A'),
(0000000740, 'Leche Gloria UHT Entera 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 4.00, 'A'),
(0000000741, 'Leche Gloria UHT Deslactosada 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 4.20, 'A'),
(0000000742, 'Leche Gloria UHT Light 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 4.20, 'A'),
(0000000743, 'Leche Gloria UHT Niños 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 4.20, 'A'),
(0000000744, 'Leche Gloria UHT Calcio + Hierro 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 4.20, 'A'),
(0000000745, 'Leche Gloria UHT Chocolatada 1 L', 'caja', '', NULL, NULL, '', NULL, 0000000003, 4.20, 'A'),
(0000000746, 'Anís Najar Crema Especial 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 35.00, 'A'),
(0000000747, 'Anís Najar Seco Especial 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 31.00, 'A'),
(0000000748, 'Anís Najar Semi Dulce Especial 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 31.00, 'A'),
(0000000749, 'Anisado Torre Blanca 250 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 5.50, 'A'),
(0000000750, 'Anisado Torre Blanca 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 12.00, 'A'),
(0000000751, 'Coctel Bercheva Café 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 16.00, 'A'),
(0000000752, 'Coctel Bercheva Leche 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 16.00, 'A'),
(0000000753, 'Coctel Piconi Cacao 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 21.60, 'A'),
(0000000754, 'Coctel Piconi Café 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 21.60, 'A'),
(0000000755, 'Coctel Thiago Café 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 18.90, 'A'),
(0000000756, 'Crema de Whisky Baileys 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 56.50, 'A'),
(0000000757, 'Dry Gin Paramonga Gin Seco 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 18.50, 'A'),
(0000000758, 'Espumante Tabernero Especial 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 15.50, 'A'),
(0000000759, 'Licor de Menta Le Mans 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 16.00, 'A'),
(0000000760, 'Licor de Menta Tres Plumas 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 25.00, 'A'),
(0000000761, 'Licor de Ron 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 22.00, 'A'),
(0000000762, 'Pisco Del Fino Quebranta 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 20.90, 'A'),
(0000000763, 'Pisco Rotondo Acholado', 'botella', '', NULL, NULL, '', NULL, 0000000005, 43.00, 'A'),
(0000000764, 'Pisco Rotondo Italia', 'botella', '', NULL, NULL, '', NULL, 0000000005, 43.00, 'A'),
(0000000765, 'Pisco Rotondo Quebranta', 'botella', '', NULL, NULL, '', NULL, 0000000005, 43.00, 'A'),
(0000000766, 'Pisco Santiago Queirolo Acholado', 'bolsa', '', NULL, NULL, '', NULL, 0000000005, 31.00, 'A'),
(0000000767, 'Pisco Santiago Queirolo Italia', 'bolsa', '', NULL, NULL, '', NULL, 0000000005, 29.50, 'A'),
(0000000768, 'Pisco Santiago Queirolo Quebranta', 'botella', '', NULL, NULL, '', NULL, 0000000005, 31.00, 'A'),
(0000000769, 'Pisco Tabernero Acholado 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 31.50, 'A'),
(0000000770, 'Pisco Tabernero Italia 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 31.50, 'A'),
(0000000771, 'Pisco Tabernero Quebranta 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 31.50, 'A'),
(0000000772, 'Pisco Vargas 125 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 5.50, 'A'),
(0000000773, 'Pisco Vargas 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 24.00, 'A'),
(0000000774, 'Pisco Viña La Esperanza Quebranta 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 20.50, 'A'),
(0000000775, 'Ron Apleton Special 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000005, 29.50, 'A'),
(0000000776, 'Ron Apleton Special 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 10.00, 'A'),
(0000000777, 'Ron Apleton Special 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 27.50, 'A'),
(0000000778, 'Ron Barceló Añejo', 'caja', '', NULL, NULL, '', NULL, 0000000005, 36.00, 'A'),
(0000000779, 'Ron Cabo Blanco Black 375 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 7.50, 'A'),
(0000000780, 'Ron Cabo Blanco Black 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 12.50, 'A'),
(0000000781, 'Ron Cabo Blanco Durazno 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 12.50, 'A'),
(0000000782, 'Ron Cabo Blanco Limón 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 12.50, 'A'),
(0000000783, 'Ron Cabo Blanco Silver 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 12.50, 'A'),
(0000000784, 'Ron Cartavio 3 Años', 'botella', '', NULL, NULL, '', NULL, 0000000005, 25.00, 'A'),
(0000000785, 'Ron Cartavio 5 Años', 'botella', '', NULL, NULL, '', NULL, 0000000005, 25.00, 'A'),
(0000000786, 'Ron Cartavio Black 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000005, 22.00, 'A'),
(0000000787, 'Ron Cartavio Black 125 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 5.00, 'A'),
(0000000788, 'Ron Cartavio Black 250 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 7.50, 'A'),
(0000000789, 'Ron Cartavio Black 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 18.50, 'A'),
(0000000790, 'Ron Cartavio Blanco 125 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 5.00, 'A'),
(0000000791, 'Ron Cartavio Blanco 250 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 7.00, 'A'),
(0000000792, 'Ron Cartavio Blanco 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 16.00, 'A'),
(0000000793, 'Ron Cartavio Selecto 5 años 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 23.00, 'A'),
(0000000794, 'Ron Cartavio Solera 12 Años 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 72.00, 'A'),
(0000000795, 'Ron Cartavio Superior 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000005, 19.50, 'A'),
(0000000796, 'Ron Cartavio Superior 125 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 5.00, 'A'),
(0000000797, 'Ron Cartavio Superior 250 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 7.00, 'A'),
(0000000798, 'Ron Cartavio Superior 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 15.50, 'A'),
(0000000799, 'Ron Flor De Caña 5 Años 200 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 11.50, 'A'),
(0000000800, 'Ron Flor De Caña 5 Años 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 42.50, 'A'),
(0000000801, 'Ron Flor De Caña 7 Años 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 67.50, 'A'),
(0000000802, 'Ron Kankun Durazno', 'botella', '', NULL, NULL, '', NULL, 0000000005, 14.50, 'A'),
(0000000803, 'Ron Kankun Limón', 'botella', '', NULL, NULL, '', NULL, 0000000005, 14.50, 'A'),
(0000000804, 'Ron Kankun Oro', 'botella', '', NULL, NULL, '', NULL, 0000000005, 14.50, 'A'),
(0000000805, 'Ron Medellin Añejo 3 Años 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 23.00, 'A'),
(0000000806, 'Ron Pomalca Oro Añejo 250 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 6.50, 'A'),
(0000000807, 'Ron Pomalca Oro Añejo 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 15.50, 'A'),
(0000000808, 'Ron Pomalca Special Black 3 Años 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000005, 20.00, 'A'),
(0000000809, 'Sangría Tabernero', 'caja', '', NULL, NULL, '', NULL, 0000000005, 11.50, 'A'),
(0000000810, 'Tequila José Cuervo Black 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 64.50, 'A'),
(0000000811, 'Vino Blanco Clos', 'caja', '', NULL, NULL, '', NULL, 0000000005, 11.00, 'A'),
(0000000812, 'Vino Blanco Gato Negro', 'caja', '', NULL, NULL, '', NULL, 0000000005, 11.00, 'A'),
(0000000813, 'Vino Blanco Zumuva', 'caja', '', NULL, NULL, '', NULL, 0000000005, 11.00, 'A'),
(0000000814, 'Vino Ocucaje El Abuelo', 'botella', '', NULL, NULL, '', NULL, 0000000005, 23.50, 'A'),
(0000000815, 'Vino Oporto Tres Piernas', 'botella', '', NULL, NULL, '', NULL, 0000000005, 17.50, 'A'),
(0000000816, 'Vino Santiago Queirolo Blanco 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 17.00, 'A'),
(0000000817, 'Vino Santiago Queirolo Borgoña 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 17.00, 'A'),
(0000000818, 'Vino Santiago Queirolo Magdalena 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 17.00, 'A'),
(0000000819, 'Vino Santiago Queirolo Rose Semi Seco 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 17.00, 'A'),
(0000000820, 'Vino Señorío de Najar Borgoña 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 19.50, 'A'),
(0000000821, 'Vino Tabernero Blanco 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 18.50, 'A'),
(0000000822, 'Vino Tabernero Borgoña 187.5 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 4.50, 'A'),
(0000000823, 'Vino Tabernero Borgoña 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 17.50, 'A'),
(0000000824, 'Vino Tabernero Rose Semi Seco 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 17.50, 'A'),
(0000000825, 'Vino Tacama Blanco 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 21.00, 'A'),
(0000000826, 'Vino Tacama Rose Semi Seco 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 21.00, 'A'),
(0000000827, 'Vino Tacama Tinto 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 21.00, 'A'),
(0000000828, 'Vino Tinto Clos', 'caja', '', NULL, NULL, '', NULL, 0000000005, 11.00, 'A'),
(0000000829, 'Vino Tinto Gato Negro', 'caja', '', NULL, NULL, '', NULL, 0000000005, 11.00, 'A'),
(0000000830, 'Vino Tinto Zumuva', 'caja', '', NULL, NULL, '', NULL, 0000000005, 11.00, 'A'),
(0000000831, 'Vino Viña Los Reyes Borgoña 187.5 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 4.50, 'A'),
(0000000832, 'Vodka Premium Stolichnaya 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 35.00, 'A'),
(0000000833, 'Vodka Russkaya Citrus', 'botella', '', NULL, NULL, '', NULL, 0000000005, 19.50, 'A'),
(0000000834, 'Vodka Russkaya Clásico', 'botella', '', NULL, NULL, '', NULL, 0000000005, 18.50, 'A'),
(0000000835, 'Vodka Sky', 'botella', '', NULL, NULL, '', NULL, 0000000005, 42.00, 'A'),
(0000000836, 'Whisky Ballantines 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 52.00, 'A'),
(0000000837, 'Whisky Chivas 12 Años', 'botella', '', NULL, NULL, '', NULL, 0000000005, 99.99, 'A'),
(0000000838, 'Whisky Jhonnie Walker Black Label 12 Años 750', 'botella', '', NULL, NULL, '', NULL, 0000000005, 99.99, 'A'),
(0000000839, 'Whisky Jhonnie Walker Double Black', 'botella', '', NULL, NULL, '', NULL, 0000000005, 99.99, 'A'),
(0000000840, 'Whisky Jhonnie Walker Red Label', 'botella', '', NULL, NULL, '', NULL, 0000000005, 58.00, 'A'),
(0000000841, 'Whisky Old Times Red 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 30.00, 'A'),
(0000000842, 'Whisky Something Special 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 55.00, 'A'),
(0000000843, 'Whisky Vat 69 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 30.00, 'A'),
(0000000844, 'Whisky White Horse 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 64.00, 'A'),
(0000000845, 'Vino De La Mancha Borgoña 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 17.50, 'A'),
(0000000846, 'Vino De La Mancha Rose 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 17.50, 'A'),
(0000000847, 'Vino Astica Trapiche 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 20.00, 'A'),
(0000000848, 'Vino Frontera Chile Cabernet Sauvignon 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 16.00, 'A'),
(0000000849, 'Vino Casillero Del Diablo Cabernet Sauvignon ', 'botella', '', NULL, NULL, '', NULL, 0000000005, 28.00, 'A'),
(0000000850, 'Crema de Ron Cartavio 750 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 23.50, 'A'),
(0000000851, 'Cerveza cristal 650 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 4.20, 'A'),
(0000000852, 'cerveza cusqueña malta 620 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 5.00, 'A'),
(0000000853, 'cerveza cusqueña dorada 620 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 4.50, 'A'),
(0000000854, 'cerveza pilsen callao 630 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 4.20, 'A'),
(0000000855, 'cerveza cusqueña trigo 620 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 5.00, 'A'),
(0000000856, 'cerveza cusqueña red lager 620 ml', 'botella', '', NULL, NULL, '', NULL, 0000000005, 5.00, 'A'),
(0000000857, 'cerveza cristal 473 ml', 'lata', '', NULL, NULL, '', NULL, 0000000005, 3.50, 'A'),
(0000000858, 'cerveza pilsen callao 473 ml', 'lata', '', NULL, NULL, '', NULL, 0000000005, 3.50, 'A'),
(0000000859, 'cerveza cusqueña dorada 455 ml', 'lata', '', NULL, NULL, '', NULL, 0000000005, 3.50, 'A'),
(0000000860, 'cerveza backus ice 355 ml', 'lata', '', NULL, NULL, '', NULL, 0000000005, 3.00, 'A'),
(0000000861, 'cerveza malta 330 ml no retornable', 'botella', '', NULL, NULL, '', NULL, 0000000005, 3.80, 'A'),
(0000000862, 'cerveza cristal 330 ml', 'lata', '', NULL, NULL, '', NULL, 0000000005, 3.00, 'A'),
(0000000863, 'cerveza pilsen callao 330 ml', 'lata', '', NULL, NULL, '', NULL, 0000000005, 3.00, 'A'),
(0000000864, 'cerveza miller 355 ml no retornable', 'botella', '', NULL, NULL, '', NULL, 0000000005, 4.20, 'A'),
(0000000865, 'Detergente Ace Aroma Zen 2.3 Kg', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 28.50, 'A'),
(0000000866, 'Detergente Marsella Max 2.6 Kg', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 18.60, 'A'),
(0000000867, 'Detergente Opal Ultra 2.6 Kg', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 21.50, 'A'),
(0000000868, 'Detergente Magia Blanca 3 en 1 2.6 Kg', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 18.60, 'A'),
(0000000869, 'Detergente Marsella Max 850 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 5.70, 'A'),
(0000000870, 'Detergente Ariel Líquido Con Downy 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000001, 16.00, 'A'),
(0000000871, 'Detergente Ariel Líquido Nueva Fórmula 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000001, 16.00, 'A'),
(0000000872, 'Detergente Ace Blancos Diamante 1 L', 'botella', '', NULL, NULL, '', NULL, 0000000001, 15.00, 'A'),
(0000000873, 'Detergente Ariel Líquido 450 ml', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 6.50, 'A'),
(0000000874, 'Enjuague Downy 450 ml', 'botella', '', NULL, NULL, '', NULL, 0000000001, 4.00, 'A'),
(0000000875, 'Enjuague Amor 180 ml', 'botella', '', NULL, NULL, '', NULL, 0000000001, 2.00, 'A'),
(0000000876, 'Detergente Caricia 100 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 2.00, 'A'),
(0000000877, 'Detergente Ariel 520 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 5.00, 'A'),
(0000000878, 'Detergente Ace 520 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 4.00, 'A'),
(0000000879, 'Detergente Bolívar 520 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 4.00, 'A'),
(0000000880, 'Detergente Bolívar Matic 520 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 5.00, 'A'),
(0000000881, 'Detergente Marsella Max 520 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 3.70, 'A'),
(0000000882, 'Detergente Ariel 360 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 4.20, 'A'),
(0000000883, 'Detergente Ace 360 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 3.00, 'A'),
(0000000884, 'Detergente Bolívar Matic 360 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 3.50, 'A'),
(0000000885, 'Detergente Bolívar 360 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 3.00, 'A'),
(0000000886, 'Detergente Opal Ultra 2 en 1 360 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 3.00, 'A'),
(0000000887, 'Detergente Opal Ultra 360 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 2.70, 'A'),
(0000000888, 'Detergente Magia Blanca 160 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 1.10, 'A'),
(0000000889, 'Detergente Patito 150 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 1.00, 'A'),
(0000000890, 'Detergente Magia Blanca Bebé 3 en 1 150 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 1.10, 'A'),
(0000000891, 'Detergente Ace Limón 160 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 1.50, 'A'),
(0000000892, 'Detergente Ariel 90 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 1.00, 'A'),
(0000000893, 'Detergente Marsella Max 160 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 1.30, 'A'),
(0000000894, 'Detergente Opal Ultra 160 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 1.30, 'A'),
(0000000895, 'Detergente Bolívar Negros Intensos 360 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 3.50, 'A'),
(0000000896, 'Detergente Bolívar Blancos Perfectos 360 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 3.50, 'A'),
(0000000897, 'Detergente Bolívar Color 360 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 3.50, 'A'),
(0000000898, 'Detergente Ariel 160 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 1.90, 'A'),
(0000000899, 'Detergente Magia Blanca Bebé 3 en 1 360 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 2.70, 'A'),
(0000000900, 'Detergente Magia Blanca 520 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000001, 3.50, 'A'),
(0000000901, 'Lavavajillas Ayudín Repuesto 150 g', 'pote', '', NULL, NULL, '', NULL, 0000000001, 1.00, 'A'),
(0000000902, 'Lavavajillas Ayudín 180 g', 'pote', '', NULL, NULL, '', NULL, 0000000001, 1.50, 'A'),
(0000000903, 'Lavavajillas Ayudín 330 g', 'pote', '', NULL, NULL, '', NULL, 0000000001, 2.50, 'A'),
(0000000904, 'Lavavajillas Ayudín 600 g', 'pote', '', NULL, NULL, '', NULL, 0000000001, 4.60, 'A'),
(0000000905, 'Lavavajillas Ayudín 900 g', 'pote', '', NULL, NULL, '', NULL, 0000000001, 7.00, 'A'),
(0000000906, 'Lavavajillas Lava 1 Kg', 'pote', '', NULL, NULL, '', NULL, 0000000001, 7.20, 'A'),
(0000000907, 'Lavavajillas Ayudín Líquido 300 ml', 'botella', '', NULL, NULL, '', NULL, 0000000001, 5.50, 'A'),
(0000000908, 'Sacagrasa Mr Músculo Advance 500 ml', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 4.90, 'A'),
(0000000909, 'Lejía Clorox Total 345 g', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.00, 'A'),
(0000000910, 'Lejía Clorox Ropa Blancos Intensos 264 ml', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.20, 'A'),
(0000000911, 'Lejía Clorox Ropa Poder Dual Colores 264 ml', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.90, 'A'),
(0000000912, 'Lejía Patito 308 g', 'botella', '', NULL, NULL, '', NULL, 0000000001, 0.60, 'A'),
(0000000913, 'Lejía Clorox Power Gel 292 ml', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.50, 'A'),
(0000000914, 'Lejía Clorox Power Gel Magia Floral 292 ml', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.50, 'A'),
(0000000915, 'Lejía Clorox Total 680 g', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.50, 'A'),
(0000000916, 'Lejía Clorox Ropa Poder Dual Colores 480 ml', 'botella', '', NULL, NULL, '', NULL, 0000000001, 3.00, 'A'),
(0000000917, 'Lejía Patito 615 g', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.10, 'A'),
(0000000918, 'Desinfectante Sapolio Limpia Todo Floral 250 ', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.00, 'A'),
(0000000919, 'Desinfectante Sapolio Limpia Todo Limón 250 m', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.00, 'A'),
(0000000920, 'Desinfectante Sapolio Limpia Todo Lavanda 250', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.00, 'A'),
(0000000921, 'Desinfectante Sapolio Limpia Todo Manzana 250', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.00, 'A'),
(0000000922, 'Desinfectante Sapolio Limpia Todo Floral 900 ', 'botella', '', NULL, NULL, '', NULL, 0000000001, 2.80, 'A'),
(0000000923, 'Desinfectante Sapolio Limpia Todo Limón 900 m', 'botella', '', NULL, NULL, '', NULL, 0000000001, 2.80, 'A'),
(0000000924, 'Desinfectante Sapolio Limpia Todo Lavanda 900', 'botella', '', NULL, NULL, '', NULL, 0000000001, 2.80, 'A'),
(0000000925, 'Desinfectante Sapolio Limpia Todo Manzana 900', 'botella', '', NULL, NULL, '', NULL, 0000000001, 2.80, 'A'),
(0000000926, 'Desinfectante Sapolio Limpia Todo Bebe 900 ml', 'botella', '', NULL, NULL, '', NULL, 0000000001, 2.80, 'A'),
(0000000927, 'Desinfectante Poett Limpia Todo Bebe 295 ml', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.20, 'A'),
(0000000928, 'Desinfectante Poett Limpia Todo Lavanda 295 m', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.20, 'A'),
(0000000929, 'Desinfectante Poett Limpia Todo Primavera 295', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.20, 'A'),
(0000000930, 'Desinfectante Poett Limpia Todo solo para ti ', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.20, 'A'),
(0000000931, 'Desinfectante Poett Limpia Todo Bebe 648 ml', 'botella', '', NULL, NULL, '', NULL, 0000000001, 2.20, 'A'),
(0000000932, 'Desinfectante Poett Limpia Todo Lavanda 648 m', 'botella', '', NULL, NULL, '', NULL, 0000000001, 2.20, 'A'),
(0000000933, 'Desinfectante Poett Limpia Todo Primavera 648', 'botella', '', NULL, NULL, '', NULL, 0000000001, 2.20, 'A'),
(0000000934, 'Desinfectante Poett Limpia Todo solo para ti ', 'botella', '', NULL, NULL, '', NULL, 0000000001, 2.20, 'A'),
(0000000935, 'Desinfectante Pinesol Limpia Todo 267 ml', 'botella', '', NULL, NULL, '', NULL, 0000000001, 1.60, 'A'),
(0000000936, 'Jabon de ropa bolivar bebe 240 gr', 'barra', '', NULL, NULL, '', NULL, 0000000001, 1.90, 'A'),
(0000000937, 'Jabon de ropa bolivar limon 240 gr', 'barra', '', NULL, NULL, '', NULL, 0000000001, 1.90, 'A'),
(0000000938, 'Jabon de ropa bolivar floral 240 gr', 'barra', '', NULL, NULL, '', NULL, 0000000001, 1.90, 'A'),
(0000000939, 'jabon de ropa bolivar antibacterial 240 gr', 'barra', '', NULL, NULL, '', NULL, 0000000001, 1.90, 'A'),
(0000000940, 'Jabon de ropa marsella bebe 240 gr', 'barra', '', NULL, NULL, '', NULL, 0000000001, 1.60, 'A'),
(0000000941, 'Jabon de ropa marsella bebe 240 gr', 'barra', '', NULL, NULL, '', NULL, 0000000001, 1.60, 'A'),
(0000000942, 'Jabon de ropa ace 240 gr', 'barra', '', NULL, NULL, '', NULL, 0000000001, 1.60, 'A'),
(0000000943, 'Jabon de ropa trome –gr', 'barra', '', NULL, NULL, '', NULL, 0000000001, 1.00, 'A'),
(0000000944, 'Limpia vidrios cif 450ml', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 4.00, 'A'),
(0000000945, 'Limpia vidrios sapolio 650 ml', 'spray', '', NULL, NULL, '', NULL, 0000000001, 5.80, 'A'),
(0000000946, 'ceras sapolio pasta 300ml negra', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.00, 'A'),
(0000000947, 'ceras sapolio pasta 300ml amarilla', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.00, 'A'),
(0000000948, 'ceras sapolio pasta 300ml neutro', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.00, 'A'),
(0000000949, 'ceras sapolio pasta 300ml verde', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.00, 'A'),
(0000000950, 'ceras sapolio pasta 300ml roja', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.00, 'A'),
(0000000951, 'ceras sapolio liquida 300ml negra', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 2.80, 'A'),
(0000000952, 'ceras sapolio liquida 300ml amarilla', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 2.80, 'A'),
(0000000953, 'ceras sapolio liquida 300ml neutro', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 2.80, 'A'),
(0000000954, 'ceras sapolio liquida 300ml verde', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 2.80, 'A'),
(0000000955, 'ceras sapolio liquida 300ml roja', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 2.80, 'A'),
(0000000956, 'ceras emperatriz pasta 300ml negra', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.50, 'A'),
(0000000957, 'ceras emperatriz pasta 300ml amarilla', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.50, 'A'),
(0000000958, 'ceras emperatriz pasta 300ml neutro', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.50, 'A'),
(0000000959, 'ceras emperatriz pasta 300ml verde', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.50, 'A'),
(0000000960, 'ceras emperatriz pasta 300ml roja', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.50, 'A'),
(0000000961, 'ceras emperatriz liquida 300ml negra', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.20, 'A'),
(0000000962, 'ceras emperatriz liquida 300ml amarilla', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.20, 'A'),
(0000000963, 'ceras emperatriz liquida 300ml neutro', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.20, 'A'),
(0000000964, 'ceras emperatriz liquida 300ml verde', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.20, 'A'),
(0000000965, 'ceras emperatriz liquida 300ml roja', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 3.20, 'A'),
(0000000966, 'aromatizadores sapolio 360 ml lavanda silvest', 'spray', '', NULL, NULL, '', NULL, 0000000001, 6.00, 'A'),
(0000000967, 'aromatizadores sapolio 360 ml arullos de bebe', 'spray', '', NULL, NULL, '', NULL, 0000000001, 6.00, 'A');
INSERT INTO `articulo` (`idArticulo`, `nombre`, `presentacion`, `descripcion`, `stockMinimo`, `tamanio`, `unidad`, `codigoBarras`, `idCategoria`, `precioVenta`, `estado`) VALUES
(0000000968, 'aromatizadores sapolio 360 ml jardin de rosas', 'spray', '', NULL, NULL, '', NULL, 0000000001, 6.00, 'A'),
(0000000969, 'aromatizadores poett 360 ml bebe', 'spray', '', NULL, NULL, '', NULL, 0000000001, 6.50, 'A'),
(0000000970, 'aromatizadores poett 360 ml espiritu joven', 'spray', '', NULL, NULL, '', NULL, 0000000001, 6.50, 'A'),
(0000000971, 'aromatizadores poett 360 ml bosque de bambú', 'spray', '', NULL, NULL, '', NULL, 0000000001, 6.50, 'A'),
(0000000972, 'aromatizadores glade 360 ml campos de lavanda', 'spray', '', NULL, NULL, '', NULL, 0000000001, 6.50, 'A'),
(0000000973, 'aromatizadores glade 360 ml espiritu potpourr', 'spray', '', NULL, NULL, '', NULL, 0000000001, 6.50, 'A'),
(0000000974, 'aromatizadores glade 360 ml caricias de bebe', 'spray', '', NULL, NULL, '', NULL, 0000000001, 6.50, 'A'),
(0000000975, 'insecticidas sapolio mata arañas 360 ml', 'spray', '', NULL, NULL, '', NULL, 0000000001, 7.20, 'A'),
(0000000976, 'insecticidas sapolio mata pulgas y garrapatas', 'spray', '', NULL, NULL, '', NULL, 0000000001, 7.00, 'A'),
(0000000977, 'insecticidas sapolio mata cucarachas y hormig', 'spray', '', NULL, NULL, '', NULL, 0000000001, 7.00, 'A'),
(0000000978, 'insecticidas sapolio mata moscas y zancudos 3', 'spray', '', NULL, NULL, '', NULL, 0000000001, 7.00, 'A'),
(0000000979, 'insecticidas sapolio mata polillas 360 ml', 'spray', '', NULL, NULL, '', NULL, 0000000001, 7.00, 'A'),
(0000000980, 'insecticidas sapolio mata acaros 360 ml', 'spray', '', NULL, NULL, '', NULL, 0000000001, 11.50, 'A'),
(0000000981, 'insecticidas sapolio mata todo 360 ml', 'spray', '', NULL, NULL, '', NULL, 0000000001, 7.00, 'A'),
(0000000982, 'insecticidas raid mata moscas mosquitos y zan', 'spray', '', NULL, NULL, '', NULL, 0000000001, 10.50, 'A'),
(0000000983, 'insecticidas raid max mata cucarachas y araña', 'spray', '', NULL, NULL, '', NULL, 0000000001, 10.50, 'A'),
(0000000984, 'insecticidas baygon 360 ml', 'spray', '', NULL, NULL, '', NULL, 0000000001, 10.50, 'A'),
(0000000985, 'cif crema limon (oferta + paño) 500 ml', 'Pomo', '', NULL, NULL, '', NULL, 0000000001, 15.50, 'A'),
(0000000986, 'Removedor de manchas vanish 100 ml blanco', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 1.30, 'A'),
(0000000987, 'Removedor de manchas vanish 100 ml color', 'sachet', '', NULL, NULL, '', NULL, 0000000001, 1.20, 'A'),
(0000000988, 'Paño absorbente virutex', 'unidad', '', NULL, NULL, '', NULL, 0000000001, 1.50, 'A'),
(0000000989, 'guantes virutex talla S', 'par', '', NULL, NULL, '', NULL, 0000000001, 4.50, 'A'),
(0000000990, 'guantes virutex talla L', 'par', '', NULL, NULL, '', NULL, 0000000001, 4.50, 'A'),
(0000000991, 'guantes virutex talla M', 'par', '', NULL, NULL, '', NULL, 0000000001, 4.50, 'A'),
(0000000992, 'paño multiusos scoth-brite', 'unidad', '', NULL, NULL, '', NULL, 0000000001, 1.20, 'A'),
(0000000993, 'esponjas scoth-brite- la maquina limpieza pro', 'unidad', '', NULL, NULL, '', NULL, 0000000001, 1.60, 'A'),
(0000000994, 'esponja verde scoth-brite', 'unidad', '', NULL, NULL, '', NULL, 0000000001, 1.50, 'A'),
(0000000995, 'acida harpic-power ultra 750 ml', 'pomo', '', NULL, NULL, '', NULL, 0000000001, 11.00, 'A'),
(0000000996, 'Piqueo Snack 24 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.60, 'A'),
(0000000997, 'Piqueo Snack 42 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 1.20, 'A'),
(0000000998, 'chizitos chipy 20 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.50, 'A'),
(0000000999, 'doritos chipy 24 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.60, 'A'),
(0000001000, 'cheetos chipy 16 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.50, 'A'),
(0000001001, 'cheese tris chipy 14 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.60, 'A'),
(0000001002, 'tor-tees chipy 30 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.60, 'A'),
(0000001003, 'papas lays chipy 38 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 1.20, 'A'),
(0000001004, 'habas karinto 18 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.50, 'A'),
(0000001005, 'mani clásico karinto 20 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.50, 'A'),
(0000001006, 'mani salado karinto 20 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.50, 'A'),
(0000001007, 'mani picante karinto 20 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.50, 'A'),
(0000001008, 'mani confitado karinto 20 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.50, 'A'),
(0000001009, 'mani clásico karinto 45 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 1.00, 'A'),
(0000001010, 'mani salado karinto 45 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 1.00, 'A'),
(0000001011, 'mani picante karinto 45 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 1.00, 'A'),
(0000001012, 'mani confitado karinto 45 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 1.00, 'A'),
(0000001013, 'los cuates karinto 40 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.50, 'A'),
(0000001014, 'los cuates picante 40 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.50, 'A'),
(0000001015, 'free papas karinto 20 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.50, 'A'),
(0000001016, 'dulci ricas ricas 28 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.50, 'A'),
(0000001017, 'papas lays 16 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.60, 'A'),
(0000001018, 'nachos picantes ricas 40 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 0.50, 'A'),
(0000001019, 'mini kraps 80 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 1.00, 'A'),
(0000001020, 'sublime power en cubos 51 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 2.50, 'A'),
(0000001021, 'olé olé x 8 40 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 1.00, 'A'),
(0000001022, 'mini ritz 50 g', 'bolsa', '', NULL, NULL, '', NULL, 0000000013, 1.00, 'A'),
(0000001023, 'vela llanas', 'unidad', '', NULL, NULL, '', NULL, 0000000014, 0.30, 'A'),
(0000001024, 'vela misionera 10 h', 'unidad', '', NULL, NULL, '', NULL, 0000000014, 0.50, 'A'),
(0000001025, 'vela misionera 12 h', 'unidad', '', NULL, NULL, '', NULL, 0000000014, 0.70, 'A'),
(0000001026, 'vela misionera 15 h', 'unidad', '', NULL, NULL, '', NULL, 0000000014, 0.90, 'A'),
(0000001027, 'vela misionera 24 h', 'unidad', '', NULL, NULL, '', NULL, 0000000014, 1.60, 'A'),
(0000001028, 'espiral tokay x5', 'caja', '', NULL, NULL, '', NULL, 0000000014, 2.50, 'A'),
(0000001029, 'Condones', NULL, NULL, NULL, NULL, NULL, NULL, 0000000007, 7.80, 'A'),
(0000001030, 'pastillas', NULL, NULL, NULL, NULL, NULL, NULL, 0000000009, 0.20, 'A'),
(0000001031, 'pastillas', NULL, NULL, NULL, NULL, NULL, NULL, 0000000007, 0.30, 'A'),
(0000001032, 'candados', NULL, NULL, NULL, NULL, NULL, NULL, 0000000007, 5.00, 'A'),
(0000001033, 'muletas', NULL, NULL, NULL, NULL, NULL, NULL, 0000000007, 75.00, 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE IF NOT EXISTS `caja` (
  `idCaja` int(10) unsigned zerofill NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `monto` float(10,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `caja`
--

INSERT INTO `caja` (`idCaja`, `nombre`, `monto`) VALUES
(0000000001, 'Principal', 14.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `idCategoria` int(10) unsigned zerofill NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` blob
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`idCategoria`, `nombre`, `imagen`) VALUES
(0000000001, 'Limpieza', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000a7549444154789ced9d5f6c1c471dc7bf33e7fbb3f7c7e73fb19dd80976e3d8286d9ab606ab4422364aaa484d2a815281f314514085940744284f11112245a214f28248c443699010181e502b55a99222449287284aeb124313294ed298c67f0f9f73be5beffddb191eae67fbecdbbbbddb9dbdbdcb7d244be79dd999b9fbdefc7e33bf999d237f6afe26471ddb402bdd803ab9d405b11975416c465d109b5117c466d405b11975416c465d109b5117c466d405b11975416c465d109b5117c46654ad20324f625a8d40e6c94a37c5541a2add8042dcecde86d78e7ea3609e97cfbd899d939f2248247888addf8e2eaab687ac25c1d39867512c301969b04a37c710b616e47f4d8d45f32c3635afbc967912b3ea12224c014375aebbd95a905053b0689ec5a6a69cff1938223c5eb5fec5d6821881816381c998654b88f374a59ba31b5b0b120a9666b2f291e42ae659142116ab0aff42ecbac9e1d4d111c45d4e309afb9d4941055fd362ca18dcc9045e3ef7a6ae7283c48300f5808298d95cd3b0ed38f156f75621e546781c513581209510206e217518c1d6264b140c1c8b6c19d36ac476fee59114244b1a0cf32c8a3916b58d7f79a405c992e0694cab112c30b9e2f3175bf990c9cded90dd19bbde3d3b9f93a68215fcb0eef53c0600f0c4e3e89c9d29ab7e9927a1a82904881b412a955586516c35ca3a7574c4b033df7eff13dd23ae423480a2997a2111a7e1b24aa16eb234488321c46298635124b96a59bd75418a90e069ccb225cb0297b6f2217666ad7f1139b1acf79012c8062e67d52561814b5bf410e697901ce847679b0faa8b83391ce05289b368250ea2aad8d2e6071d1e041bbb09446521ed4d836181c9889184e90b63151b6531bf84c4f0d39047f621b5434c98844f4c828d9e07bbf48130710040224e34532f1a4c3038960bc2fc12e491fd9047f681fb2d1aebc796c146cf431d7d4fa83066042e2d152439d08ff02fbe679d10eb892d23fde35f818fdd145605054133f5c2475c65dd6f9920caa13d7878e2a8155515453d7516ecdd4b39d71cef2de6e679bef03a4b315cc48126e22dd9bf58e2d4e5917d58fac1d7c556125b06fc5e5d591d3f3906f87d60a3e7b1dcc63139c44094dc6f347f5145f7650a6fa83cf393e42ae679146ed28056ead3ed5f840b121f7a4abc18c08a8f207ddda05f1904191e04d9bc4933bfe38747819910e4f96bb875580594752192c32a36dd24650b92251bb8ccc6c78af917c78bd2d33f35546301985fc2c26f8f032eed78909262f86f248e45258d45258d16a9bcd8111fbb097ef506301302bf7a036cf43c301b02f9fc63201a3de7c613ffc6bd6df79068cc6fb5233d1cb14ea063dcf8e8290915319e002104ee02664c680f897de7054d07aea418deb915c2f5a968ce754f03c5504f13867a9a2039f57f1064e0f10dd7d8bb97c02e7d00c7c963a0435fdc90bec81f20b2553b4e15f91c875366001cbadb5188ecc2581471cdc0a530a7cefc12e62e9ece9ba6a418ce5c9bc27434a1797f67c08d97beb0597f8f892d23b5ff5b9ac98e375ecd2b0a00a49efb361c7f7d9073cda853d7839b34a09978e122ab820beb21caa13d9a6917ee840b8a0100d3d104defa7016af3cdba5afa7f8bd20fd3de0b7efe74d567f7616e48faf03add340f222b83ab792e6f8cdc63008fd656ecfc51c057fdf0d3e6ede4796e069ccf225f8880bcdd40b0a62bd206125852bf71fea2a633a9ac05b633378e5d92e5df9c9c0e39a82202a835df935c8feb18d691d79ca7a72dd5afb9300792e0976da07fe7e79730c2dd6062e850417d52dad9ae190cbf72325957537ace0b24e01e90bc39a6964773abf1825428fcb20bbcddf18910d5c0a112439d0af99f69fb958c9e55d98084349155f8b207ddd205bdaf2a77d77b9e47a35eb31b1acf5081124d597bf778495141695d2bf5df13433dc4bc876f356fdcc2c6b3d8204d996f7faddb052769917ef8411565245f3d123078180afec7a2a8d10417820ffdc23bc6cccf65e9c0817cfe4f7c271e4f98d6dba6ade660533cb5a8f981ea2e1d08df41000b83e152dbb97f0b73d86ea1655d67a2c5dc2d5e3988b313a3e5f3c93df9b8955ad818f37809d366ecad8699fa97391f5582a48b1c9a01eee86155d3d8d1e1a061d1eccb9b6224ab28c59f81c152e0650a59b1cfe3c3e573c1300c7c9632bc360b23b0dc7b908e87119702d16b9330f1d0cf4b80cc7b98890794816cb0431c35c655954d2b8a0d7c1bff12a10f081ec316f97889965adc73241cc30576bb97827accb7491be6e349c3d0968cf554ba757dc3cc416db80cae5f71fcee0475fde5634224cfaba411abb01dc06007cacb8104a95f65d6c73323c21897f88b4aa0589a799fe88b0e4013e33fda11445285dc61a87057b33aad2a9af653a9ac0996b53c57d547adcb43a3744824da4ea05014a10e53352bcf475f272ee29879a1004284d94876ae96fbb9c7bcaa16604010a88a2de35bd2e5111df9a1204c888f2da3fef636a697598cd9980eda37e31fb0b6b4e102033fa3a736d2a47942ca154f93b488cdcab17cb046996ac1d61e788c266cdafa043ccd354960952ee063823c4d30ca3e3f348a7cc17847454910f717d3491f77a2544998e26f0d1ecea3abe91d19215232d4b7d88d5662b8b925c5a799d34309fc8b9d757034ebdb7a532cf8574791f14cf542a82028c4204a1b1fcdb6476b456e8419d352cb3f27b88917bf522449086dbf9bf919d81ca1f8724abe57fa846eed58ba5264b7252eceab07e8bcef6c6fc830c23880a300a11c439f1a9665a2504598b9120a1150146218238661634d376b5fb4b7aeec36c1ec961af73427b54233929f6763769a69b4daf00739545c4660761923bef688b32d4d3549149a2192647b4d9122688d6480bc8f492afeed47e2053140fd3c6dfae1965144258e99ecbff2a98beabc387bd3de24d57a788496116016b22c204718ddd2e9ae76b3b3761b0abf861c94608b88ced272e04f19b1ff11526088d29f05cb95134df91dded7869608b9091576f8b843ddb56cf8f9f2f67a7c93acc28a31042a37dbebffc03f1bd4f15cdb7abc387132d3db832f910d71fe8dbe1ae85e4a4d8d5eec7e0d6007a5b247039046147f30a5813112a886bec363c576ee8124572521cd8d182033b5a30b594c0c7f332ee2c28988e260a6e5c909c149d0137ba1adde86d91364c3cd73e6d6b3ad5260800044ffd0189bffdbca41380ba1a331ff0811dabd7d63f0ee769a0e86a2c2d3666c6c44ef4e450f8d493c614b47eff3448cc98736d919ce86d9156fe748bc15717a7ccd8ef9d53868035114b568c9c130fd07ef804165f3f86e4337d5654b98a7a6fe5658f3b8d36a731557c74f57e115b812c5bc2cbf694e4403f960fee8172f04b5655bd428fdb5e07efe7a3a2275b2707fa9178a61fea96d6cc9f5f82aaf148b55ef8c4241095c16742999381dc7f0739f489492dce85fdce0bfeb6b96b3cb63a6a1cc89c68106231246cf633125661bb8d7214041d34802011f7a4ab9db19d20598254423b0dd8f6a78944615b4100c0431ad0e908163c81add6b0b520c0a367c26c2f48962095d046fd356fc2aa46102073a4f7664763ce9178b546550902647ef966336db4e54fde9941d50992a5997a6bd28455ad20406d9ab0aa1604c898b0761a28fbf07bbb51f5820099a1712bf5a195faaade84d58420597cc485761aa86a1356538200999f89a86613f67f7e5fd2e0b2d262e10000000049454e44ae426082),
(0000000002, 'Bebidas', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a000008f249444154789ced9d4f6c1c571dc7bf336f76f69f77371bff8dff5574718bea402908caa14442549168f853212a71a9e0c28122a1b6aa10420a07a40a0448850b48dc10528544c52dc0a1429436b83d2416b1c9216ed2d8d978b35e3bf6ae7777766667e671b026dd38bb3bffdecc9b71fd39d9c99bdf3cef77def7fde6fd5be199b15f531c131944de1538e67e8e058918c782448c634122c6b12011e3589088712c48c43816246248bc2bd0cb53b3cbf8d2f475476597aaf37873fdf301d7287c22d542168a1b8ecbcee53603ac093f22d542fe56c9e3647603633250d3740040cb3c18d959cc267153d1902122261209acdc2da06dec2043467956993942d4c6b2aa89256c49ef0e2d93531f455efd040080083272640aa210a967cb3391b22c0030a1da96a142f7decf06d550d7cbd0cc5690d50a8d4809b22d5dc6b6b46c5bae29df4053feb0f3a730d134aa681b3b41562f14222548d15844ca1cb72d97300ac874e71ff8f78e59475d2fc3a47a10d50b8548094268125da1615bce10db1069a2ffffc5dcc22225080018827d1f62f6f421fdb02c4c317659552b3422254847ac392edb2575db328ab98b86be090ad34fb542255282680eeccac210da8ecae9b483bdee06ba54f15aad508994208ab8e5b8ace6a085585098d8d72bb1b0b04809d212cb8ecb6a92fb14370e16165b4154b2ede91e51b7b0c808e2460c0b55f2264a942d2c328234c8fbaeafe948155ff78ca285454690a6971642fc0f9558166650cd772c164442104d68b87a07b1e8923a0cd159fa3b0c0a1375bd8c8ee93c730b8a4808e2c5ae2c149fb6d54bdbd841d3a872b5b04808e2a543b7f0dab10f42335ba8eb656e16c65d104350d120cee6d1fbd191eed88e6db9c5a43a370be32e4843f46e57167eb3ad41f0b030ee82389990b2a32ddf625093fe846d615c05f19a5d1d4625db4cb2ad418469615c05f1935d1d8665b63588302c8cab20bbe42ab358ed4470b6d58b66b6d0d03703b3306e821882cac4ae2cbaa4ce3cdb1a84413534f44da8e63ef3d8dc0461915d1d26a86cab1f14265a460d2da3c6d4c2f809e2e3dd63104a223c412c54739fa9857113c4cfdbf9203406838d5e606961dcd65fbe3853c293a36c3df83fdb32aa1f7f1c2ffde25b989a3be929c6f2c535bcf6e337b0b6e2ee81b12c4ca71d64c828048fcf3ab7b5bd3ff8f45f715266e7bdd987179039fb4d94bef6acef585d55c79f7fff4f681d1dd756caf8f785ffbaba9e083246c8048820bbbe37971692225d666248d9113c72fe97c87ff20926f100209194f0fc8b67effdbeb67a1b2f9c7b0dcdbab3695fcbc23264144931e7eade5cfa90c9ec1eb358a597cf3315a31f0ba767f0bb0b2fb9baa6370b7303174166f23799c4494e9e42f10b6798c4b263e1f40cce9c7bdcf575aab9ef6abd3117cba2307cc798facef7917bec53d01af78f2f754d01ba19cc73f6bd9f7c055fffee9378e5b93fb8bace5a6f9c25e390c5ecd0b25c04f958feaeef18a9874a901f2a3db89b84002209264f999c3f81c9f9139eaeb5d61ba76861e8ae2f2e8298d47f0b0100511421080293584ea094c234fd25231db38e2e5506eefa8aed3eb09b3f7b05a597cf63fccbcf8476cfe58b6b78e1dc6f7cc71966615c3a75016c9e6ab51aee50496583dd48c0a05d5f5c0411e1fe85a91f8d2b9799c4714a65dd7fdf7798c3bbbeb808a21884491c752bdc1672cde5708a537a777d711164bd31c1244ed896d5ac07374d6c591817416e37e698c56aacf85f24e194cbefac057e0f2e59d63575077facec2143fc3f0fdf5edfc0e7021e3a01d876e8c3e02288226ee1c2369ba1f76fa81f6077f50d26b186a13b1c58f40b1741d2269b3e0400dacd3d74ebc1bf1c124689881d5cfa104293cc626d6cba5bdbbba976704369e186e26e1f7bbdc92655b7834b0bc99ab3cc62fdfdad4b78f6cc826db94db583bf6c95b16d18908904839a80a1e3b989192c66f3b6d79737c2b12c6e73ea32b5ff109c50a9d96fa5564c037fbab38ea64870229d43464e2397cc229d1ac1eb5b6547ada55a0d6789113741128c04d9acd92fefbcb4bf878e409092eeb74a4924c8ca19bc53b7cfa02ebdcb6e0dd930b809c2d2b6b6f6870fb75f6d359094faf7014949c6d5967d2b6b36c35901cf4d10969956b93a7c4ab8282560d2fe1f28a51469d13e835a5d09ee2dbd176e82383986c9297699d6c3e92c946eff436d946e078f65872f446877fa9f3c1404b1efd481834c6b189fcd15f1482a8d7db5054a3fb437a5db816c1af8ead8a9a1d7375be17d4c5c57bfb3ea479c645acf4fcde3a95c012da581dd761db5e65d3c2a27f1c3b992ad657d70c3fec82856709d3164f582e824d30280a78b1378bae8beefaa6d85770c07d71692a2ecfa915a23b8676be9edf0d60cc7764efd30afffa385577ffb23e671d756ca585df939f3b883e0da429c1c09eb94dbb76e338bd54be516fb69db617015c4cbf9268378fbbd3799c5ea65ed4a30d3b683e02608ab1db8165d52476583eda90e4078135316dc04d995fec73c66b9ccdeb63e128218828a1dc27e2efce27bff621e33a8952683e02248437cdfd1f9bc6eb9555e671ed3e99e1056f01124800d9f00502eb3ddabbe7c31f8552687e1b3508e6167decbd2d212d378fb21b70e8093204ece77f70acb561276ca0b44e03420d6b0ccb4c2eed0015e9b3ecd71a6ef20bdbcfad35f612ef14526b1d6563f22828c98b3810972fd4a0ddb9df03b635670b1ac51fd3381c51ed14a81c50e032e82c8348f59edac7d419714952740cc0cf3b861c26df8bd682c021a5049bce5fb2551a409143aa7fb7e0d52dce03a1f523416513416d120d7d112cbf7beaec2ee609aa43e060048980524f531a4f4a9c0eb1a169198a0ca1b25e48d07bdffe0d0fc3bd0698743adf810e9f7100122f2d23452628177554223d2825864c82846c8a4e7238fe2446cfe4259cc222f4d7b3af2284ec44610e0e01caabc34edfac8a338112b4180837e254bc68fdcb7445bc44e108b9458404e3a75e4fa9558ff3509218d1389f923d5afc45a10e0c0c20ad2ec91e957622f8845968c234bc6636f61f1aefd2192620e7969baef395471e14809021ca4c60569d6f628bda872e404010efa95113289b458e45d15d71c49412cd2a418bb2197f8d4d423b29845419a8d4d6a7ce405010051906233e4f27fae0784819b42c4340000000049454e44ae426082),
(0000000003, 'Lácteos', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000e4549444154789ced9d5b6c1cd779c77fe79c99bdef7297146fa26ea14ccb90aca046add44683a08d534348d1d6455ae4c906d2020dd097f4ad6f7def4351204fcd438b2269fbd0a080512041d1a28d813a682e76d5c4966cc9b26c49214591d4ee72ef3b9773fa30e4f2b6e4ee7297e46cb47f80203133e7cc99f9cdf9beef7ce7cc50ccfff33f19460a8de4493760a49d1a010999464042a6119090690424641a010999464042a6119090690424641a010999464042a6119090c93ae90674d2df2d247961fe378ea4ee1bf7dee28f3eaa1e49dd8755e881dc797483978adf3f9abaebb3c0b34752f761157a20957a11d97cef48ea5e2f38900a17905f2a1f6262d398d874d7c7eb7a03e7c1c3236c51ef0a7d0fe946eec2d7f1677f0b632501105e15f5e83fb03ffa56c7b25ebe0040e4dcd9236d63b70a3d90d9fca778f5fd9f627df975fcb3afedd866ac24ded9d730cbb791b7beb36fd9d3751b381f2a28a13759af346e1eb8ffc9cc970fb50fe0d5d816682f5f0885f90a3590571a374999c681c7d4d6ee532814f66c2f140ad4d6ee1f58362ddcd04109b5c9bae6dceb788c6816595d5d65757595783c0e40bd5e0720dd2c762cff6b9115febdb165aabc7c01994a618de70ed9eafe14ea1ef2c5e6c1e60a20b7fa83d6dff57abd0563f7befdf46a746f8f701e3c6cf995e356687bc873ee12a7fdce37259dff31176efe054f667f1b373a0580dd5c61e2d1f74896deef587e4e55b96c17b8e5eeec119ba6ebb87b4a6881bc567fb7eb6393a5f7bbbaf9fbe92bf17bdc727f75cff69380125a9375cdf9f8d8cef552e4f1befb8edb7c85b6875c99b580e319175c0558db7fff71f694d0f690b0e9b87aca08480f3a0e2823203dca79f010bd2db41eb446400ea1e6dd7b470625b44efd30d242d3b01af8d2435b0dd036ca8b10f76328ad06761ee3fb34efde23facc3c72233b3028fdd20029474b885895843458ca414596d15e16cfcbd0d0eb349b51d2cdecc0c01c1594a137595a68f2a915c6c696c8a53ec1b64b08e180d00855430a8f4ce201a7b2f7a9a65670a53bb0736f4219a4f91a7a20c5e41ab9888730c1a548ab843141c7373a86b44a2034006396a19e5ac397fec0ce3f6828430da462579988e7b1232b681d43fb29101eadcbd21652d530c6c277a690769ef1c463caf1fc40db314828430dc48f9750560da16a58d1258c97c2e8c8562f1106d068e7142ab282b44a08552311ad0ed474c1e0a00c2d90a66a129506df3985f63200a8e832c64f8370404710b289f6b2483b8f900d8c9fc06fce102542c31e7cd8ba09c5f8873789430bc4951e09cb415a1540e23b53f8ce24c86670e3dd71b49741a81ac64fe13b53183f81500d84aa0561f1112880f2f1a1a10c6dd8ab856e3df940eb4a8c9f401b9be059d38109b3dbf88c5ae6e8da566fd0bcfb31d1672e22546f61f6d0029146a2dd71840c9c36c642080fa16a3b8e13d241bbe318630306211c8c891c79fb0e0b65684d96ad2d6a5e04a31348d540459791761eed2791d63aca5e0b9cbb9f44a80a2af278c3d94702d3e50e7684dd4e9b507a315f430b24ea4769fa1269af215405a32341686bad63740c211d8c8e22ad42d043fc0442365091159a38c4bdd8b1b4b35728430b04c06e6470bd0cda4fe13b3341582b1d8c9f08c623466090a8c84ae0e4dd2cda4f51af8f616bfbd8dad90b94a10692701394aae378ce29a4aa041195dee61fa4b70147a3226b682f4bbe3649ba3176ec6ded16ca50030148d7c7596f46f075109f682f8b900e00423682318a91186351742589ca29a43999cbde8472908636cada9434926cf51465378a89254928434439181dc378293c6d51a99ec36bc64937c74e0cc6a63657dcefb78e78a880f8d2c78995c072306e946833dd4aa7a79d3438691a5683ba748109009456c4fdd88983d8ae8316770f0d9046b44c227b1fdb4fa0a2cbc1c83cb5427d7d8eb8936c1d17f362c4389e08aa1fed07253c8fcd016a5a0d12d9870863235403af7e01a96a2804b1cce240d3e9c7a9768bbb8702881fab208c44a82a1836c61816d22aa384a46987ebc5cd5eb41bca5000b1ed0ad2ce6f84b46263024a81911b69f5e64937b12f6d8732143ec475836cad54150c2084d7dae73767d05ef4e41a37206dfa94a1e821aa9142a80ada4f06e1ecc65c87d1717ce11175936dcb69a169aa267a630a37ecf2f285e10012f562948a9f41a83a6082892863e11b43a374baed4a9246b48cc82d131b7b82ce2ee31dd1fcc7a03514260b20d918a3b6f62c7eb402720c7c8b68638c781b185a68e29945a410186d13954d9cb13a3cb978022def4d470a24adebbcd2bcc9171b3799f30b5cf21e755dd66bb34d6dfc008828a8a9f6f30cd248ccea736c0f863bcd48bcf7fc5b145c8bef1526f9cb954b5db773d01247f121e5b4aef37aed6ddea8beddf1a5cdae1577c1daf205c24e604d2d7455d4341ad0c39c84e769befb78823f5fbada7333fbd5c0813ce72ef1cde2b7bb7a1dad2bc55db8b806919d7d46386922e7beda7d3dae8ba954316bf90050175aaf6baedffe1c8b6ea29716f7a5813af5e7dc25fe3effadc1c150ba2d8c6ee47d7807e7873f423fd9984fb76d442e8b5c98479e3f0b5d4cab8ec5253fb8fc13aec58fef0daa8101d984313013053055de8251ee6dac618a456422817be367985d6ba544268d5a9847c43ae7bc6211c93f3efb33e6ec5ac76307a1810049eb3adf2c7e7bb03000a62ac1efd571c4f2999ea088580c6159c84402ff178b7b0fb06de4fc0588749e398c45246f2ebcd3f5b9fbd14080bc5e7b7b70666a53d97a60b200c64a98d41a24dc008adfb9d9727a0a5dad621c07cc3e6e52c9c07c75a1e924fcf1f8276df7bd3ceb723635980467df616f5ad779a3faf620dab253a96df9a98807b3ebb03686589fc4f855b85069edf6efdec37fbc823a7f1675660e00393b0352a24b25ac0b17f63d8d88c510b92ca6d0f9ab0f7f76fa3e7f9bff0c00d7cf3b5c3fd7e4fa7987742400fedd8fa2fcd58d040f2b877fe5a16f20af343b7f8fe4504a387b36093f83ca4ce12dee4c599be23a2a95c2bbf9410b0804bd444e4fb5ad5e3fc9231271443c8e9c9ec4ef02483697e06f7efd092fcc09cea482deebdf7f80fb7805fb855fe10f1702505ff9fe1837f387bbb57d9bac6ebe477228c50f580cbdabd5c6db70fcdbc71aae872995db16f73eb88dffd15d9c1ffe2870f8b6bdbf838fd88853e34174b630cfef5c322d1800382eba546e050ee988e15fbebc7e6813d63790e7dca57eab682fd52e21584157ab1babdab7249209bcc545d4f973ad6ddeed3bb8efde403f5ed9538b1002994c2163b12d879fda36d6502a0891e72fa02e2d2067670260ae87ffc97dbc8feeb600a8858b44bff49b884cba553c1d31fcf5172a1c467d9bac5ed221fdca4c17302b1e9cae035b2b0fadab57b0ae5ed9796cd301df47974a7bcc96a95631f506ba52c1ba1040144a41268dc86577dcdced727efc138454884804af780bfbdacecf71985219fff163d4f4342fcfa47979d6e57f1ef5b6fe2bdcd95ec7829f9f874f27b7b66d1f9b1c20393d89c86577f8944d599fbd8a989ec4baf46c6bbf983c853c7f16efe62dbcf76fb5af333386cc6410a924d6f397f7ecd72babe8a565fcc5c06a7cf599de7d6bb8b3bd4d854ca4d0e5de47eaeacc5c5b1800d816d6c2333bb70911943b7716916c9f2ab13e7ba5edf6d6399f99474e4db67ad89589defd48df40de89ccf3e25139f67413535b82e98347c9a654c6b82e3293017bff4bd24ff288540a110d56371abd2d92f35cf07c442e15ecab9530ce3ac6ec9c1eaeba82f7ea1b2b1f4d93146d7ce846b626a20570fde06bdca5be812caa1c2ff65b493bf91294c64c1f3ce0d4ab6b781fdc0602471ff9c2e7db1ee73c7e1363d6a09ff1ab2f89ad8df162a206e9cef3f80f8a177a3e45df3ee4bfa20777e343abde9d3334b51a2a9743e5c6c1f3f70d750deb0757b494834fda8f5980e001f97016513f87583ddd55db1cbff7b9febe81fc67ec0a8bea083e5b54dbf5524ddd86dbb370677a272cd70b5224f57a9056dfcf64990e2f79961308f78045d8c538329a42269310eb6e8e5e1de2f60e24cafa87447b33d1972a3b9f2eb1741e159f814a041e6e3d00eafc398cd3c45f5b432d5c446c7c55c1fbbf9fe3bcf5dffbf6983d9ace63b2cb071e629a2ebabc86493cd9b9c3976d736cff5befdd230c24cafa4ef2f3fc5ee3ddc10e128bf1961f0110426e8dc8b7a7556c6b8fdf30a532f81a393686fbdefbd82f3ddff97cd90e6fe54e5431fe2a26e2ed3df6ee2c2a3e855e7f8439b305f517f5de5f7b18d838e41bd93728cb01bf26b6926afda927efa3e30fe04c11664b1d8b1ad705373053c6df356af6257c3887bcbf006b6d6e9a2fe1f61ce2ce59286f0b81a7caedc13932e899aaff99c58101595439be96fb93c14259496ff98b7433c8f84e95f749ab6c4964d220057eb1b0239dd2d29324528d61aadec6971f76a96623550a953b8b284c746e67d4c35b7908ee4ef358764fc0a96fd787f669fe60e21bfcd49e1f4c85be844f27ba8eb8b6cbfedc8b445ffd523038d4bb32c7e9065a1730d9224cb459176c694ca3815e5f876ebef8f0ec23985fc69c5bddb1f956b9fbffd4d03a75cf253a6851e5f8dac4d7f9ddea4ff9d3e2bf71461d2ec9d652dd860f66f66e8f46f1d7dba738764b271fc2766b1277e1d20139b8b88b99ca63fc62d023bbd1eeecb4b198b47acf301c59eae45f93d778533ecfc54fdfe1f72377b96c0723b2cb56818cdc3bd7711cda1ed60a2f587e6a8c411b103a82e34729fb164d2303db21690d246d2f4dde9794da58cba880c95d7352be91e4e8fd7df8235997b55d61f8c0fd30e9c8b3bdd6780e7ba6775bfab4ea58d2eff6ccf489fdb78161d3b1cd87d873a791f1f0bffb77d23a362042a9437d1de769d3b1ce180650e647500ed0b14fe1ca781c7baebbf4f5d3a81399531f455efbebc416398c22aff63ad15527a3c86baf4e14c828f2daab135f97358abc76eac481c028f2daae50008151e4b5a9d0008151e405210302a3c82b74409ef6c82b7440e0e98ebc4209049edec8ebff014a93ea5584518f480000000049454e44ae426082),
(0000000004, 'Golosinas', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000fa049444154789ced9d6d7053d799c7ff92aede2dd9f8457e1132d89671b011a1b59798848e099492618ab730617771f6cbaec966779b366e5366baec4c893bd374da401a4fd399ed1036d30f98b669970d99b890c4216431186a270db231b61c19646c6319cbe8fdc557d27e9025fb5af7ca9275255d157e5fec7befb9e71e9dbfce739ef39c73ae78c11ded413c8233f0335d804750792408c7782408c7782408c7782408c7782408c720325d00d6d9bc167e8f03a4cc173925f00a80f920885147060b161fd92f48b112d856093c5905e8d4008040f775783b3aa3d30a0181761d84a25c109324f8339e34177665b25790cd6b816f6d0989b10ce1aeadf0755f877f6034ea9a7ff40efc0bffcbfeed1f40f44c0237eea6b8b0f1937d7d885c0cbcbc1bf8d9015a31c2880f3db3725e1585a17c5ede1dca97036495207e8d1cae5ac05fa95c312da1d342b0491b5fc65fdf08fce69f80caa2244b983c5923c8bc4e0197cd00f22f4370b6bd06f71b9d083add31ef11edda1aff03e422e0578780dd1b932c6972648520f33a05dcb7be40d0b5d809cf7f7c1dce975e83df38c1781fd1a88b99afa0521d7df2fbbb332a0ae705f16fc883fbd617b4d702660b9c6daf61befb3aed759e5cca68b6783209787229fd435f68ca98f9e2b420413901d7f4cd15d3b93b3a19451154d0b40200025d35738672117c4fe623284fbf13ca6941dc6b7d143315336d4727adf9a2354b0875fa4c04cc16787ef72778aae22b279b705610529b03d2783ba17b5c477f19d5d1f354f9b46963f52fee374283caf9a111f80b8509952159382b88973f97f03d4197275299b11054a8c16710ca77ee126540e9ca7383f407122ecb6ae1e648bd5809f1e1ed11b342ea431514189b00a91f45606c0201b385f656f29a1ea47e34a649123537d19ef71b27e079eb2ce55cf08e09064f11ca8242e4e64856f36912829b827c6b0ba54223ffebb491ca247bf598efd563fee3e8cedc7be63c08dd8bb459f355f910d28c4ffcc609b88efe92f69ebcba5c0cbe3b86d242053425792004a9332cdc34599bd7ae988468d441dad6829cb77e04e14e6a05fb074623ad0acbfa14c94b2d517985c5607220e482501e53f7edf862640a5647ea8292dc13442e062a0be34ece57e543dad602d94f5ea4f40b6137d83fb6e87989f6354599b2f9eeeb31c500009816f3f0fa480c7e398db1094b4afa16ee0952b5ba0119a1d342de712432ee207b6f84fe2eb41441851ae296c58063d0e986eb27a7e0eee88ccbb52ed015508e53d55ab8274802ad63393cb914f28e2310eedc8aa0cb03bf7102fe8151082ad490bdfa22787229824e37bc67cec3d1da0ef29a3ebe7c6512f072a247f5a9682ddcebd473e8c3e07ee344c4830a9a2d515e165f950f9e2a1f844e0be1c218c3f73fdd1131fcfa51ccf7ea41f6de887bb00900c4133a489edf8fc00fdf05403f6f3275df0e8bcd0dada620694f8c7b822c2160b6c0f7ee25cc775f5bb11203660b60b644c6107c4d31e0f68257b406f643ff91f0b3853bb742d4dc1419e9cbebd4b09c1f604c1f6e2dc97a629c1424e874c373f22cad4b1b0be2091d848d3af0abd6c2f38bd3e06f5e07c1ba12089fdab2e2f84550a106bf420d42a705d1a88b0a3c3a0799a3ca4b49b6b5704e10d26c86bbb53d21b322dcb915e29667c057e5873aeba36f22303d0b517313dc1d9d90bed402d97fb6529fa31f0d458319625dcbf1db62cfbd2c2599d6c23941609c895b0cc1262da46d2d1177372c867f6c02c29d5b21587071c38147c9f3fb23f7c61ac9d3415a13f7a656d35a38e765f17df1154972783fe4af52c71e9e936723e30e4165285e1576837def5d82f3e89b8c266b25e23559cb49d413e39e20261b7852e66f134f2681fc8d2351f128523f4ae973f80b422c0d93f80746e1fceecf19e74e98b05dfd32a1f47484c72d16ab2b663ace090200c4fa72daf33c9904b257bf436bf73dcbd661853b65e1aeade0c916050eba3c707774c271f8c7f09dbb14578b998de15d2582d747e2d6ed19dcba3dc3d85ab8d7870010ce4b314f739e498cf9eeeb51151b4ec7934b21fadb1df09e394fb91e305b4291ddb7ce8278420741a51a824d5a082ad5511e562c77773558ac2ef43b3ca8d614203f5746b9c6494188213bf8257908cc3e889c931cdecfe8112dafece5880f3d432b5a18f29a9e71d4eeafac84777c75fd4e2cfcfe006edd9e417eae0c5a4d41c413e3a4c9020071d1bac8ff824dda9873187415bdfc1c5d94371eee0dc5b6f9c962b1bad03f3411e95b382b88506f87405d0c0090b631572653071d98a60a42e8b4901cde4f9b96097f6525e6f43309ddb31ac2ade5d6ed19ee0a020012bf0ac29d5b19a75b01d0aedf0542b38bcb11353745cd9d302215c3f8c1747c6959c26275715b10c1b813d240ec70bc9fa6e281e8161246dad6129728130fd6c0fb20fdabe3392d0800e0c321e0a321da4b915941ba6b3142ebd2b69698e6cba6aa4a8ba9a283fb8200c0890f1945612260b6c45c662a6a6e82fc8d23510be96caa2a98ba6eafa694ac901d820021517efd6942b7f8ce5d8a795d50a986bce308a42fb5805f5e8a7bc275191503c8264100e07fff027cfb0c60bc1f5772b2f7c68a2be401c04eca70f3233bee5fcdfcc61d4e0e0c63629c01bedd195aa1fe37c53193065d1ef8ce5d62dcbc63bbfa25c64f5c80f54af2b12ab6c83e41c27c3804c21d84ec47ff0edf0797e1bf6140d015dd1abc67ce47422300e0bd3b07eb95514c9dfcbf5547705349f60a0200db6b406c7f1cc4f6901bebbad807cba93f412ca55a62e7db9fc23ae18173703225611036c96e41b6aca71c5a871f3074cac674948615b2ab535f8ab60428cea59c329fe9c95061d8237b0579e671caa1777c164ebd294385618fec1564b9b9ba3c9c9972b04c76f62125794015d5e5b5747d1695cc29b2634269825532072f418d4b110102729f02b99e3528701541ee53a4b4c8f1929d826cafa11cfa6d6e58deff3c724cf24918f38761ce9962cc82e493b04ae66095ccc19467849894a0d85186325b398840e6aa252b05211bab2905b75ebeb5788d4f425fd20fa7c89e509e5ec203539e11934a13ca6ce5191326fbfa901c09887aea2b352c5d8badc394674c588ca5907c12a63c23fad65ec674cee4aaf3592d59278845a7893eb760ae720ed46152c98ea745f249180a6f425fd20f924fb292673c64d46479833e4cfb670100e364b4bdd710a50080624101c43c11487f00bcaf51dfb260eb1906697541fbab569882434074df9e1456c91cfad65e86ee5e7d5a3afeb40a62f6cfc2307f0726ff14cc7e0bbc415fec1bbc8ba648cc1341492a51375582f5c322a86a6a0100b35d9f63dd4fff0eaa434fa2e7fb6fa7a4dc249fc4e765d7507dbf16c58eb2943c230c2fd52f52b6061c18f08d403f6f802dc0de1bdd94a56a343cd78ab9d37fc6eef7ff0b0070e6f9bfc7787f2f6bcfa023d5a2a4ac8578833e747b7a31e033a4247fdbd4043e3efe63ecfac1b194e4cf84a130f4aa8f5489921241067c06747b7a5736492ca069d896f2672c2795a2b02e4897fbd394b50a3a541b163b798962e5179bb1c558fe08727c0ad63b7a56ddde748bb11c4d4363da9e45f249dc547dc1ba4bcc9a203d9ecf322a060094d7a7d77c8547f76cc28a20d680033d4b5cd44ca1aaa985b234be2d6a6c31b910bc640b5604e9f1b23c1a4b80e56eaeaef960dacb305238c85a5eac086298bfc34636abc2d44715a4a1a515e29cf486d2bd8487b5b8172b82a4c3bd65c2f0c905cab158a144c373ad0ca95347ac507f22645d707139e6e19bb04d5197f334b4b4a6bd2fb14ae6928a3287c97a4100a0eff429cab158a14cfb081e006665c92fd0664510ede627d9c866d5e8df7b075e07f5db59fdf41ed4b7fc735acbc186b7c58a201b0e1c60239b55e3b5dbd07dbc3deafcf617be4719c9a71ace08b2a9f92034f5e91b25d33170ee9d281758ac50e2d0c9dfa7ddeb4a06d6fa9003af9fccf807ef3af67294e9ca36515811c4e7b073e2835b27efd29a2e554d6dc6cb162fac08327231341650d5d4e25fdfbf9251f33570ee1df477fe77d4f9b028e9ec5356033ba1935fff22f27fa8a5fc0e7b5f399eb66fa444495de3db7dbc1d03effd212a5d5894ea1ddf4849390a5cc9bfc05ff0cafaa75f4936938fef5f02c043f992f0b7aaa60e5f79f61f212f2cc2ecd868946d670365a91abb7e700cfb7efa26ac9377611e597c71bfe1930f905ba689ccbd8721c4626cdcd30cb14289b12bb1b7bc25ca5aeb7ae424393fc28a2006f20e6efdf9625405106231ca745f45c373add0d46f8358a184f3be392971541b36e2b13dcdd873f455347df78750d5d40100ca1bb661ecca2770ce2e6e7763120500ca745f45f58e3d98bdfd256c53c96f6593fb14d0ce266f0e5959e4d0e3f92c127edfdb7e029bf63d1b33bdd76ec3f4f04d8cf75f8575f22eac93cc15a2aaa98544a184a67e1b8a6b6a218e312be8b5db70ead9afc33143ddf0ffd40bdfc3532fb431de67b87801ddc7dba34230f14204086c997c021292e1f74812801541bc411fde769c8dac2aa97e7a0ff6b69f48ab57e375d8d175ec65182e5ea0bd5eded088fdafbf15b34c868b1730f0de1f60f8e483b89f5b282fc7fa610d2b62002c2e03329153f8adb32b721c8e27add45a92c5ebb0a3eff429f4759e82d76e0310fac64a48191c221b25ad58a1c4def6132b76ea5ebb0d868b1760eaef85797810e611ea1e7965a91ae50ddb50ad6bc2ccbfbccbeae761755dd680cf802e37752fb958a144434b2b74cd07598dc0daa626d077fa54288e655fac78224060a3f971e47ad6c0983f42bbb4b4bca1117bdb5f4faa3c7e9b1b03fb7ecefa2621d617ca99c8299c757d443b47a2aaa945f58e3d286f6884aaa62e2193e675d861eabb8af1be5e98faafc23c1cfd53484480885af2392b9b81a170907631c2a6e6836868694d786ce2b33b30f4cd1329d9b19592958bdea00f5dee4f579c49142b94285ef08034340b14c6fbaf0200a6876f525a011d65b672943fa8a4dd42105a383dc8181e57d5d442b7ef20340ddb628ae375d83178ee8f701eeb816036353ff292d2a5a426720a3ddecf304ede4bd523a07294426d2b8f6b7d5478734eaca86cf84ba22cd320b734f4b319d6a9bba1be64f866ca9792a67c6d2f105a95d2e71bc0a0cb088f20fe17123321f72950ec2845beab6855de8d553287e99cc984a65d890081eafb75ac8cc663911641c28cdf7b00c3ecbd85e94e079c223b3c843b6affdf52c4a40412521ad90f28f7e5b0e662927c12b33233a53ccbc9f5ac81ca518a02972a2d3baad22a080038dd3e8c4dcec196c25fa9c966d23ea72e978ab0a9aa1815656b2048e16f39652b19ab91d22225b66c288d7a6fedc34e46bfa2621181c7d617e1b1f5458f5acb029ca885fc5c19ea37aa515ac8fd19bd54c30941008010f051a1ce475d5531c4a2acdc3ecf0a9c11244c6e8e04f51bd5d02c7bd3cfc302e70409a329c9c3e31b4aa14cc3cf9d7209ce0a023c9c2e72567cca87c945ce0a418087c745ceba4ff6d7ee22679d20c05fb78bfcff65054777e41b568d0000000049454e44ae426082),
(0000000005, 'Licores', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a0000083149444154789ced9d5b6c1c571980bf19efd5bb5e7b1d27b19d38b6e38b62d204a75552c54a55681e680308a84951692544c54d8ae0c1a2209a07788127e001848484402a425521e58587c41234821ae28834699b3a171ce7623b8e2fb19d78edf8b2bb33c3c366bd76b2de8b772e67c6fb49237977cefefbcb9fcf9ef39f333b96e8eed328220cb2d50914594d5188601485084651886014850846518860b8ac4e60253f38f7faf2cff1c638f1c6f8f2e35fcdfeda8a944ca7d84304c33e42eeddb53a0353b08f90e11b70ab1f14c5ea4c0cc53e420022f7e0dac7b0306f752686612f2100d1a58494f1db56676208f61392647c04ae5f71dc47987d85003c88c0d50f121f650ec1de4220d1436ef5c39d41ab33d105fb0b49323906fdf61ff08511e29397329f77c7339e0760711e6e5cb675cd228c901af704ae2acff221074a4096968f9ad05c6e8114c5d6358b506b59a5cf54a41ec423c494d8fa83256b96fa56f097169e9c4908d3430cc186358bb38524b151cdb23184806d6a16db08090774f845daa066114648d83593f17c9927aadf9b095cb30823a4a224b39072bfcebf3c416b166184d4b827329edf5c66c067bf80358b30421abdc319cf87fcd3c6bdb940fb2c42087932d09775e94425caa19dd78d4b42909ac572213e798923e5a7736a7ba0f1a3dcd6b40ac1e29ac572219de193597b471285455ed9ff9ec1196169cd62e95a5667e529dafc0379bd26e81fe3d5fdbdfce9dcc1b4e78f3ffd49de38b037e77891d91c172d81c9b8ca9e8b6339b75f0f96f5908ee079f695f6adebb59b423739dcdaffd8f32f35efe0bbed6d85a6b626952e99af6c3276a1d212218dde618e54e4366eacc5deba0bb46c995cf5dc82aa51e6711714371332f065270ae9ac3c59700c1595cf3e7166f971c8e3e6c5e68682e366e340a98bda12e3e29b2ee4c9401f1525115d6269d21ccf36ff0f80e777d4b06f4b5897b89990258997e50786d52ca60be9089ed735dede6d8949c1eeca0a5ac2e5bac64e874b929094b861358ba942c2ae19aab32c91e48bcb354bd8bfc83f46c6758d9b898e9acd891f0ca8594c15d2e6cb6f8a9b0b2a2a4f373e3ee33292c6b260ea81ce358ba9421abd4386c4dd563199bd918e54fa3cab9fd0719fc55421351e7d3fae92047dfa4c120a46877d165385e835bb7a14975cc0d5297a53e03e8be56b597a10d7623494054c7b3f295b8302f6591c2104e0d5d606d3deab6f3af3eee632ebd867718c90d9688c8b770ddcc47a485cd308e5b33c93e73e8b25abbd71bc84dd7519db28da100b4ae6bf2c150548dc3be7ccd824df7bf7cf949e3995572eb33f7d33aff692c7cbd0ec3a06edf111989b85865628597bedc51221b34a35af37fd30639b9ea977e899fc6be638f1089294107279fa3ef70f3c477d9e42f245737bb970779d157ab266a96b8250fa651e4b3fb2145565767e21edb1148da1695ada231ddd43a354b5b411dbb4d5b07c35594676b9f8f98757d61f244bcd62a9104dd3882b4ada4351353458f348c7898121ae3cfb45c3f28d7b7c8c3c58d027d81a358b63067580bf0c0cb2e9f0e70de9259a2c237b7dfca64fc7659a34358ba94226a64699981ae57e449fd990aaaacb0740efd82497a667e87bf13bbac45f49cc17602e16e7cdab37f50dfc48cde2a81e02f093ff5e644bfb7e063b5ed02d66dce3a5c4ede667172ee916f3311ed62c8e13d23b3649f7d028ae578e11697ea2e078aacb85e40f70fbc182febde351a24bce1302d0f5eff30ccfcd33f1cd3798f8c4fe75c7515c6ed4d232a2aaca974e9970f9110e1bd49344a231be71fa2c318f9fd8b11fd3ff5c67de31e25e1f04cad08063efbdcf1dbd66575970a410804bd3331cedee21128d11e8fc3a7dc77fcbc8539fcafa3ac5ed21162c47f295a26a1adffed7394e0ede313ee18708f5a54fbdb9343dc367fe769a5f1e7a8a83b5dbe1b5ef33f0d2b798bd789ec0e82d00025e1f009a5c82e4f62049123230b514e5b577cff2be09eb632b71b41080e1b9798e76f770b479075ded6dd40543f83b3ebd7c5e7a58f92797d4a3aaca5bd706397ef6230bb2b54848a34f9f8bd94a64195553736a7b6260881303431cacaea2abbd8d83d555abce4f2d45f9c587578c9f4965c19231e4e6a23e3b7c8a9a9b8c95f48e4d72b4bb87deb1d43efcd45294bd6f9fb45c06387850cfc6ef2fa7ae80f9a30022926c58216756f490b7aeddb22e9147d8b04222d1d4c7a65935462e6c5821a2521422184521825114221845218261e9d249892c5356ea4f7bcebbe84292d25f2398f5ca411b638910599be746e4e38c6dee47c7513481aed935094b84f8b5dbfceeea8f3237722b68eecc4b236b74205b531c4304a32844308a4204a32844308a4204a328443036ac90dd95a99b0c1ca9afb53093d51485008792370210006708c9f31f90873c6eba56dcc6e9e5967a6a03e99770ccc65421236a9321712373a19cdb1eacaee29de79f617b30759b258f2cf3cf2f1ce66bbb1a8d482f2f2433ffc1bd575ae0739e3fb04dcee16696392c9da02564bcfdc10b2c29ab2f2dea6a6fa363c5a53ebb2bcbd3de4b4b4af38d2c55d3b8b7628bb76f6a86affefd3fd973d6015385ac62611e86af27beb4526419ebc6107f29b4ee81dafa8cdf4add68583fa8575543cb1e08e43e0e3819eb850078bcd0d496f53bdc1b01318424098561d73e0857656feb50c41202891e52d7043bdb123d6783219e9024c150626ca9aab63a1353115708247a4b6d7d428ccf3eff69ad10c41692243945debacdf183be3d8424d9baddf153647b0981d414b96ea7237b8bfd8424096f4e4c91d7b8cd915db1af1048f4908656474d91ed2d248983a6c8ce1002a929f2ce365b4f919d23244930949a22db10e70949b2753bec6ab7dd14d9b942203545b6d19ecbff010d82e3f3a9bc14f70000000049454e44ae426082),
(0000000006, 'Aseo Personal', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000b6849444154789ced9d5b4c1cd719c7ffcc2e7b3397f5b2e0858dc1601c2e75b510373889719afadaba12add5c6b6f2e0c496d28726aa2a5589fad6c73ef4a595928754a9133fd432a9e414dca46e0d562db01d233b06713158162120ee0b0bbbb097d961b60f9b59ef2cb3f7736666b17f4fccedec61fffb7ddf39e73b734e5ee0f29e109ea11a18a52bf00c31cf045119cf045119cf045119cf0451195aa52b400a765b3578c6807cff2c34c115a5ab93315b4610b7ed27e00c360080d63f876d4b77605c79a070add2674bb82c765b75440c00e00c36acda4fc2b9fbd708469dcf05b684203e73b3e479ce60c3f2aef3f01736c85ca3ccc97941788d013e7353dceb218d016b810df03c2f63ad3227e76388b7e4e5e4f74cde021b6261b194c0a037c850abccc9790b89e7ae04f885af81a0171cc76161611eae9565555b4b4e0be233376323df9cf01e7efe6bd1b1c7e3c1dcfc2cfc013fcdaa654cce0b920c7ee1fea6736ab6969c156423df0c76dbae84f708ee2a1e6ab4969c1564adec50d27b62dd95146ab3969c15c4b4741b4573ff46be7f2eee3d52ee2a1e6ab1969c68f6cecdd830365c1739aeda3d015bf93c4cfe3b302dddc18a9f81d776184cd93e20df0420b9bb9242b096c2c242141799c130f2ff5ef37221857bf1a3b3f876bc6ad3f9bac631ec6978086bf9bfc227f24dd0541d83a6ea38b8a18fc1cfa76e21b168b55a45fa2daa17646ec686bffee5ed84f7e8743e34365d47a3a30b3abd376c25695a473ce4b616d50bd2f1591b06ee3b52ba57a7f3a1697f271a1dd789d6414e6b5175505f71995316030058d688be9ed3b8f6f97b58735b89d523dd96d88acb8cf68ba7d07ef154da9fa56a41eef6eecfe8b9b9e93a745efe0326c793771cd3219596d88acb8c8ffefc2b8c8dd4616ca40e1312b12f11aa15c4ef33a0ff5eead6110bcb1a71e3cb7730375d97fce63448662ded174f21e0d7675cbe6a0519b8efc8ea1f1328285c22509bcd08d6e2f33d693cfce7ea71cccfee881cdb2ae6b0abe6dbb4ca556d3fe4ab0cdd553416eb140a8a9c046a230dc77158742ec26834c1397700777b5b22d70c463f4e9dfd0778831521468fbca0074cd09db44c550a3270cf81555771d6e5d436dc26509be42cce9bd0d9de263ad7f67a274cbbabb05e7630724ee71a807ebe276159aa7459fd69b4ac1251592dcf24871b5fbe039635468e5f3d7c13cfef5b42204a0c0060b73be02f3f92b02cd50932315e25d92b4f97caea7eaaee4aa0afe70c969d3b23c736fb18f638ae60bde284e4fdc1e27a048be3e7f85527c8dd9eec63070054d6d0b78ec9f1668c0c3cf9c5ebf45eb41efe04c6c6d7015d51dce7f8fcc2b8d75425c88acb8cb11132cd54da82acb9ade8ed3a2f3ad77af81394ec6d407ee9f7123eabf54ec7bda62a416e5e7f95483995d5fde1312d8ac4c68d4647176af6b961aa6b4bf014a05fe881261704f1fb0c181dae2752166deb888d1b16eb14f6ffa803054d6f257c2eb8388c8de93b09ef514db3f76eef7e221d4180ae205271e3d0890f51d0f416f2b4f1071f799f0beb43ed58e37c301a4d28b194488e20abc64248740401a0b6fe363577152f6e9436bf00edf69ac4cf0e7c8a10e70300f87c5ecccc4e8b7af902aa1064e01e99611280ae75f4769f938c1b869aa3099ff38e7562c333233ac7f33c169d8b58742e8ac6c45421c8ffba7e48a41c9dde4b4d90febe36d140a5c53a85e603ff4d1e37168610988cdf3b8fb516c563c8c4781591611220dcbaa2c1dc741dfafb9eb49e747a2f5a8f5c80a5e574f2b831fc59d2f2056b311a4dca5bc8cdeb64ac03a0e7ae6287f05b5adb61dfdf9056dc48059fcfabac202b2e3391611280aebb8a16a4b6e1166a1b6e65143752415141487504017aee0a80281ddcf4e255006177148f647123118a09926ebe3c198d4d64273608acb9ad58f39400000a8a9c91014befa34ec9fb373c3329c58d782826c84016e9d9580a8a9cb058a78895178d6824b7e251e4efe0c210d687db453122c4f9369d4b17455a597e9f81584710a0ebaea2274ad8ec63a26beccc3d041786a1abf801f2b40670aef18ce246348a0832365c47ac230884032d2da205916a3484385fc6f1420a455c16a98e2040d75d4d8e37477ae6728c20030a08422a5f2e505b4f2f6f1e6d1d34ad301ad90521952f17a025081b30e1f1e82b00c2562847061290591052f972019ad37ca287d8695a612cb20a42b2a90bd09be6c3064c18e90ff7c4757a2f1a1d5d543e470ad90421dd1104e84df31919381209e634f32b52c82608c96112809ebb8ab60e00b25a0720932024f3e502b4dc55ac75c831b72b1a590421992f17a8ad27df0c5d735b23790f9dde8b968397897f46326411847453975627adafe74ce4efc8eb7132435d10d21d41804e226a72bc1993df8457152a2872a2a9457a349736d40521398828405a10366012cd2669696d275a7e3a50156462bc4af4020b0968b8abdeaef34fc6ac6a1ec8d62b9782aa2024f3e502a4bfac9181a31157a5d37b15b50e80a22024f3e502a4f3e6cbce9de8eb391d396e6ab92a7b3337166a8290ee080264dd151b30e1da95f723c736fb18f1f7db33818a207e9f81f8300940d65d5dfbfcbd48dc10deeb50035404c9f4fdf244907457bd5de745b9f296d676c55d9500714148e7cb0548e5cd7bbbce47f21cc09379566a81b820a4f3e50224ac6364e0a8480c8b754af156552cc40521992f172091b17bfcf080a84525cccf55627824114405191dae233e4c0264efae1e3f3c80deee73a2732dadedd426476403514168047320bb090693e3cd9bc4687474a92a6e44434c90b9191bf18e2090dd349f65e7ce4d6f3c55d63c5064583d55880912bdce0749327557cbce9db876e57dd11b4f16eb946afa1bf12022088d7cb94026ae454a0c9dde8b1f9ffc93ea82782c4404a1153b327157b92c064040906c171a4b44a3a33badfbd9800937be7857520c35b6a8a4c85a10520b8d4991ce341f36600aafb5f8ddbb1c02874e7c983362000404a1314c02a437cd4710237a7c0a08bf431efb0a81dac94a101af9728154a7f9241243ad7d8d4464fc7e08af3160c79bbfc0b13a1e431d3c6606c92eff9b8abb92124358ea22d72c4320634184bd3b76bdc460d74b0c66874218fc278f89afb2df61201577154f8c5c0ae052642c48ecde4fe57bf350be5783b50506f72ef178d495b930c9a66f2e3b77e2c617ef8a02f8561003c82286142cdc00b3be79dda782b23cbcf65b0ddeb8a0c5f77fc640b72dfdb2138dec0afd8c68312cd629fcf2ecef735e0c80c0daefebfae7e0b1be06989f97bcceae03831d3c063b36c0ae272fafb2ba1f877efa81e4b5f010fa19513fa3b6e1165a5adb73a2d3970ac416e35fe50bb15ef232988ad6b8f73ceae671ff120fcf7cfc8f8cd73aeaeb39b3699daaa6964ed967a7d386e8ee081cc7c1e567107ceeb8687395581e75f318ece0b134bef9a3df78fb37a25ffb9adb8adeee739b56e2693d72614bb8a858a86c57e1f379b1e4f621aff2083455c7e30a333b14c2fdbf6f449accb1eeaabfaf0d23fd4745b3431a1d5d8acdbb95036afb87f03c8f55f70a3cfe0d30652f40537b127946e92d2496be0961b0834759e86f2277f5e9071f47feae6db885a617959fc8461bea1bbaf8037e2c2f2f81e33830f65630f683602cd22fef707d7f04bf3c1a397e3cfa0ad88049b64591d5806c3becacaeae60d5bd0a00602cf5606a4f6e122678f37708f99e8e2f3e1eb26e79c40659b85c2e04bedb1025cf6885a6f6e760ece135d2d96b6fca5515d5a2c81e541e8f1babeed5c8e28f79462b98aa63d818bd2477555487629b82711c07d78a4b72a9d4a719c5d6cbd26ab528b596a2d45aaac8068e6a45f16fc26834a1a2dc8ec2c2f83b063c4d282e0800300c83ed660bcaca7640ab557ce55a455185200206bd0115e5761417d1c942e602aa1244a0b8d88c8a723bf432ef43ab06542908100efa3bca7660bb79fb5315f455ff9f161616a1a2dc0ea3517a8072aba17a418070d0179ac85b3de8e7d47f67349aa0d71bc2a3c81e8fd2d5a1c2ff01ca87fc2d2289427c0000000049454e44ae426082),
(0000000007, 'Abarrotes', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000b5d49444154789ced9d6b6c5be51dc61fdbc78e8fefc749ecd8b4499d344d4349180997b5e352064d270445144de2b27d9bd8f665d3a4f19d4f036948684cd33434b49bd46963432adaa6ad62a5acd0d2b5346b0385429c5b133bb1133bf6f1e51cfbd8de07c789d33ac939f67b2e4efbfb92443ee77fdef8f1ffbd3cefc5bac7da5e2de1169a41af76016eb11e4aed0254e09c1970ce74cdd76c111728cea87089d441754152de04c69e3e0bce99d9f4ba8eb12ef4ff6d58a152a987ea5596183100607e601a930f7ca64089d445554116f784448951617e605ac6d26803550561bd0949d773ce0c4a94eab5acaca82a089db048be4770ba5034d33294461ba82a886bba5dd2f5ad53bb000005ab1582c38992de2043a9d4455541cc090b769cdf2dea5a2a6742e7c76bbdac92d108c1b5fdb245f50ab9f7dd413cb3770ea6fbbedcf09acb97fb30ffd72330b3f6f52fe8742858ad289a4ca0d824506a7ed341754100a02f6fc073b75fdbf0f50933f0e29f0d486df07ac968449e71c39062a1cfe5e429a442a83e0ee9ee0b61ffc14f37bfa6fb1a7efaea2bb0d936e922eb7428d81d28d81d804e47b894caa1aa20dd7d21bcfcc62f11d813dafa5a31a200289a4cc8336e144d2652c55414d504a98861b571e2ef5911654b2ad962b3375db6a822483d62acdedb7d0d3f7ef1d7a2ae2db6b440703128199bc798545c9046c4a870e8d087a24529e9f5101c4e14acd6a6c81645052121460529a20040d14c97b345e3d68b62823cfac405bc7eec352262543874e8437ceffbc7445f5fd2eb21385d2858a45b364aa1d8c725f4d4397ce3fd7dc4e352b6a8e47b8ab40525a30986740a3a41205ea646504c108a33e274d4413ceeb08e423de64989a220385dd067333064c44f01c88d62559667c1254b5c67c8d7d0fd45daa229a352b946fd62b72c610bff6d7c5a574b46a562829c3db50f8ffb634463fa4d0514163c6482ad18956a678b6282a4591afb40761cd0936f211a0fa8ca961633f1d86250741cc2ccb4118d679fdd4134de2a3a1d0a369b2a46a5a28224cef7c0612c108b97bef01562b16aa18651a9a8201f9dba030fb64b5bd8b01144db8fcd50d8d657dccbeae5c9187d5d592b913862299a4c8a18958a0b629ff01289434f7711892305258c4ac505593ed74ba41d499e1b22509afa58352a65c816c50519fbb80707988d66c7c5d1612cdeb8e04161d6650b41d499a04a37367e0870da716b8b661a82d345ccd6574510eb7863fe931aedc766548c4a12b6be2ab335b1b37d481f7badeefb0f8fbc40b034e42061ebab92211357fd888419351e2d3b258a82e070d66d54aab6ea2412aa5f90c13b3f2758121968c0a854a70db16771c7f044ddf7df39a8714156a8c7d657459081e16043f76b3e43aa59c916b1d68b2a82ec7f78f3a5a35b31387875cb158c5a43ac51d994190200834d526dad438451a9ca42398f2fde709ca6aab6ae63b36c515c1012d901007736b12000d6b2e53aa3527141f61ffc84489ceeee6b4dd78ed4e27aa35251411aedee5ecf81031789c552936aa3525141485557159ab91da945d14c2beb6589eaee2600242b3f4bd07100aa578b56ed7c1bc1071819f940f4f317b2ad58e0ca3b7f53790b826c270020c876222d587029b657742cb95054907519c2038800b856822e82b200d297e94ac24b2fc14b2fadfe7dc073639597166804d92e04d94e04939de59f2bc22981628274f785e011e2d09d2c953fe532bff9f562a5b218643ec720b3561da6051a9762fd381319c2a5f85e2c64c92e67aa4627f701665e6b0c4ff69dc683ddff036364e57c94624cb03bf1f6f4619c08dd4f3cb66c8278ad313c3f70028f042ec8115e132c645bf187e053448591459047bbcfe385a1e3b01ac96dced13297e37bf1d2e80f90121a9f3124deeded6642f8d17d7fba69c4008041e673bc74d7eb4462111764ff0e3223f166a3ba13d008c405619e9e241df2a682b820176ff381d3f84e573978af2740240e7141388ac22ff6df8b29469e2d6c5ae4bd9e004e121244968ff2326dc69bf70ce1ae50185f0f4ec295dd9e0dfca8df87933d012cd3e436f7c85ab78cfa7d18f5fb1088c57157681efd9128cc1adb862c9579bb0da37e1fae78da890a514191ca7ed2cd60d2cd00e84720164720be8c402c8e5df165251edf10cbb419930c8349b70b930c238b08d528defaae8ab352e7fad8143a58164c96432016875910d0c136b618bb1e388ac2bcdd86384daf88e042d86157bc83a27a77286cb7216cb795ffa86a18cd82005fb2ec7df9d8d46a55e7ca7260b2d9ba9e55ced2b5e7721405ce685c7bbe06505d908de0286af50d9c743330f14504c6b328cdf11092ebdba1d08e164ceeb660a9bd798e61da08cd0a52c13fcb63e0228b5dc18db3c237cb63f8a3245807858fbfeac0d57dca6e772389660531f1457cedd432f65ca9fd8d09b5b027051c3c1143df9534fe79a40db916d58f94948c264bec9fe5f1fc9b61496254e39be571e4ad084c7c51d27ded331cda67d41d33692e43fa3e4de3e089b52338a88edb60397c1485f939643f7c17c55452549cd6681e47de8ae02fdfead8f4baf6190e9d632cba3e59ebd99d7ed68768a73a2739684a905a6278de7807530b51b85c2eb43ef64d447ff8ace878add13c062eb2181bba713f62d7580a5d632cdaae69cb45d04c95654f0a38f0fefa81a2e5f0519c19bd84977ff20adc6e375aee18867177bfa4b803a3ebc734ed331c1ef9cd1c86ff11dd500cb5b203d09020779f4dd6acf353a914c6c7c7f1c4e347ea8a6b4f0af0cff270467278f058180ffc310c6764e3d3afc3bdea6e28d54c9555ab5b9bf9d7db18f9d571f87ef61abc5e2ff84be7901f97fe2d3bc327633086c5554dd14e75cfccd28c20b510e6e710f9ee93088c1c85303f8ba533ffae2b8e2b9283d8fe5ae856869409ed68a99925c2fc1c92bfff7943b1f5228fc148784cc838d57d4b34d386c839ba2e8afc1a8b658ffae7c56b4690a91e1aa11de44f88038082c8854e09af3ccf97826604018033071959ec8e7c49dc883d712b43d6b3d46ec49987c8cec5f3c592e80cd102c405091fef6de8feabfbac3835e226541ae08bddea0df2ea81b820cb171b3fa08c9428a746dc38f3a407d303f24f402583640e6633f45a465e2212690521d182dc120d7bff12f446696e6b354b1e13c23bcdf0cff26891e8da0265312a3db7706f7963e5564e6e788f156cabf449ae64d08b2f7efb304a42e3e7fd12170400b2330ec4cff961b00ab0748a73676bc13a285cdd674581d2a32d9a87414463c03a289c38d286a9ddeb47dc8b9d66649c46b4cf701bc661dd462c4a18a9f3712b66deb917d3c7ef212206a0c0fe10535b169e9149b88616606aad6f2e1c284f58ed0a661118cfa2359a87bd6a1a97755008ed6cc1540f8da99ecddfd08aa765ac9175e3773b70f991d62dcb920931983f7d3ba2e77ba4ff235b20bb20d5d8f72ee1feef9c85eeb60ce639f5161618f92286ff1e85ffcbf5dbaa17779af19fe76e3c5cadc099900c7a11ffa413c9a0177c4cbeb22b2a4805af3f8ec7bf7d1a1d8361704c01e1ac0dcb39b3e222edbe9044ff07f1d56c59dc69c67b47bb909e639009b99109b9577f570a5504d90c535b16a6761e46bf0093878781cec1527588bfa367a1eed87cdcbafae92e702664e6dc70e432189a98442f378d092e80df259f69f87f6804cd988b15728b34728b34f059f974b682c52afbe1c557b05fd6f852d0d448fd7a7482002a9900954c4057acbf0bdd4c685a900aba7c1e543c06432ab52dbe8078333457656d869ee7a0cff128d074f9d8bc26f85e42a9344586aca35482219381311e839ee7d52e0d719a4f900aa5120c2916543c0e5d3eaf766988d1bc82aca02b16d61afe6d204cd30b524197cf834a266048a59aba47d6548dba18f43c073dcfa160b13465c3bf6d32e47a561b7eae7e43530db6ad2000ca0d7f3a0d2a1e6f9a1ed9f61664055db150ee913541c37f53085261b5e167939a6df86f2a412ae873b9b215934e6bce8ad976bd2c29e8b96cb947a6212be6ff71420ec96c2b672f0000000049454e44ae426082),
(0000000008, 'Aguas', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a0000060349444154789ced9d6f4c1b651cc7bf77d7026da10c1849b110196822d498f94216b3b8f086cdb8f86ae1c58c2f66b277982cf18df18d8ef81203f385d198eddd4c541c6ed16413b26cd90c364e13d76d3a6120cd8a43b12bb0d28eb6f79c2ff09863b4bbb6f7dcfd389ecfcbeb73bffb95cf3dffee29cf49af6cff4083800cb2dd09081e4608218610420c2184184208318410620821c41042882184104308218610420c97dd09bcf7cd8b053fef7ff5078b32a181a821c41042882184104308218610420c218418e485686076a76029b6cd43baf6b563ffe19de8dc192c58eedd2f1b7079780a17877fb328337b91acfe91436ba8117d833d68eddc5ed47933bffe838fde1ac3cc8d794e99d1c0d226abbbb70303e70e162d03005a3bb763e0dc4174f77670c88c0e9609e9eeed40df604fd971fa067b1c2dc512215dfbda4d91a1d337d883ae7deda6c5a30477213e7f25fa86cc93a1d337d4039fbfd2f4b876c35dc8a1a37be0ada9303daeb7a602878eee313daedd7015e2f357726defbb7b3b1c574bb8ce433a7737e1c68f7f142ca38141d5b225c597a1a0737713ae9c9d29e97c8a701512eca8043c8b05cb48652611784606ce961180185c9bac405b35cff0abd768f722c392dcaf63155c6b48a0cdc7333c0020d8568f7bea1c6aa516b8a4d2fa937ccbc8762c1f937fb868040d0c4bea9f8e7810e9082100c0b42c1673b74d95628760c70801809cb6827bea9c69f1ee66a72def9f1c250400322c699a14bd29b4b239749c100058614b48b38469f1322c89bbd9695363e6c3f61fcaf162599d87040555b23f6f99eede0eec3ffc7cdee580e3e79fc0579f5ec0b92fc2d0c0b0acce23c392a8560250243797bc1d59437492ea1c72daca23c75b438d18f8eeb5c72e94b5878278fbc3d771fcfc3b78ead9660040564b632117454a8d73c9d9d14200603177fb2129ada146f40f1f286a91ac3d14c4b191236b523430a4581c0bb9e886c2cbc1f14234b0b5e17063b31ffdc3074a7afaecf37b706ce408022d0d6bc772da0a1672512cabf3a675fa8e17023c90f246ff4b652d05f8fc1ebcf9fe81478ea75902895c14592d554e9a00b6881060b5a97a616ff9ab8cbb5f7e6eade9fa3fab13d318eea97365d516c78eb2d613dad582d1af2f4396142852fe5aa269ecb1fd4268570b6e5d8f6df8d90a5b42862551a304502117ff7075cb08e9ec6a42b0adc660e9c7346b7213ce9cc8ffb13ea1acd0aae1931b8b1a226f1921c1b67acb6365581259968257698047ae3374ce96e943ec429f50ae1f7ee74308b108a3134a21c462f40965be21b2106203396d058bb9d886134a21c446369a500a2136a34f28f535972d33eca54e862571974d8b1a42090d4c08a18610420c2184184208318410620821c41042882184104308218610420c2184184208318410620821c41042882184104308218610420c2184184208318410620821c41042882184104308218610420c218418420831b80ab9bf9ce3191e00904e66b85fc34ab80a99b95678475233988cdce17e0d2be12a247c7a96677800c08591eb86cac5a6ccdbbdc7cc58ebe15b43220bf865ec2fc3e56353f1a2be6c78740293578dd5908b23370cc7b532d67ab877eaa7877e372ce5d267b3183b69ec4d3ae1d1099c1cb864388ff0e804c2a31305cb44c6a3888c47cb8e530ecad3debd47b945ff8f9be1f86a7fa201db025570553cb80fe6a693b8f55302a7872610bdb684c5590dd7c7670177061a34f8ebbd6b65d3c90c7ebe3885531f874bba4b23e351c4a6e3f0d779d11078b0cd466c2a8eefbfbd893327ae20321e452ea3c2535df1d0b527afdec1a94fc218fb3c52e25fc11896bff2a818eeb3252cab7f3b623f5ea390fea7cf2ad98f4ab91a693581344b6c0931e4278612647895066c733d89ca021b5a3a05f2427414c98d1a25805a5733dc92c7ee74b841bac9da08b7e445adcb8bfb6c092916072bf1dd2354d9744274aa643faa643f526adc51fdcba669b2f2e1551a50ef6e734cffb2e98500ab1d7f8d12409d6bc7a6ef5f1c21444791dca875b5a0d6d50c99d356e0bc7194101db7e445bd6b077c4a23a44df615376da76e048f5c872ab91669358114e3f784d64c36d7ed5302fac4b2ceb5a3a47d74ade65f6dd1fe74a48651200000000049454e44ae426082),
(0000000009, 'Baterias', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a0000053349444154789ced9dcd6f1b4518877ffb6537b11b5bc1b86e5a0248a1295205116ab87204016aff0024ee70e5d0432f5cf9a80407da5b90684f884b4145ad84c2b1880a241a0ea50a1052cb35969b26a9e3afb5677b70bcd9759c8fb57767df64df47b2e41d7b6726fbecbc33b3bb192b179fffc602430635ec0a306e580831580831580831580831580831580831580831580831580831580831580831580831580831580831580831580831580831580831749985bd7ee13872b3094ffbfc7363157f7e5df6bd2ea359036f7cfe1c8c516fe7e44f1ffe876ac9f4bd3e5da4b610af320020f5623c809a00a3cfea9e6574f70b12a92de4305069afa1d4fcdfde6e88aafdde50e2c8c626a12a839fe7525b885915328bdb464354ed57a5bd36501ed5f6ba2b1f27a6d540a9b90c610dfe772a7e3d9775eebb293fb2f14cfdeeeff8e1631d75c7c179e7b35731fed24828f5f9eadd1b43b594031fb29aa28152f3a12b6d9833d40f4cab81878dbf918d4dc250bdf5813cec0d080181527319a66878da8f8504c820527c0b59c9f67dbfb2f2c4e3cafab634b3d6463c1ecc70d92b5d2969238b8496daf3fbbe093955bbe457569eb8b7380d60da95b6f6a086ccdbd950ead30f018115b308007b4ae190259115b3888d3d86db2c44322b66116bad9d2f05b19010586f3db243582f077e1e42817551c3cdda026e5517706c9ffb7443d7b89173a5b39001714ab8555bb0d3dfc7e97de7d19592d6b3f6ac9e8578a42be166ed2e9e88fad0f96db4d7d01475fb520b0bd9077e4be8a57b51321b9b64213b11b4845eba52588803d9127a31ad060b095b422f9114424d8293c808a12cc1c9a11672bbbe886f377e252fc18934214a7a16d6ea1d2965fd76ec012e167e44beb522a53c3f91762d4bcd9d87929e9552d6f2d8e303290390284494e7a19df9124a72f74b0bea0b1f481347116942acf23c00403bf305a01fed5f99dcf94d216765558b1c522fbf5be579e0c804b499b96d5294e469a85317ecf75145aa10b1d94a94e434b4cd830f00d08f6eb69c64e7730e5972b0caf340bd00005072e7ec16a1cdcc014726b6bea827dddb1142fa1d4351fe79abf093ef419b9983929cdef6bda8862de942ace275d7f64e1d7854c3967c21957b76d8da8d7ead260a84f29083336ced445487bea108e90d5b00805605ad5fde025a153b298afd483842fa842d91bf0ad40b104b97edb428b692d09ecb12f96b5b1bad8abd2df2d76055fe02c02d442add49220088c54f80d613c7f6a79d3711ecd8c37b72b15ee8b4847a01a2a74fb156efc02a7e1fc99156a88f925ac5eb104b57fa7ed65eba0cb42a919b8f847ac7d0d58ff4522f40e4af42499f9576638b02a41fb6164b57007d2cec6a4885b41000ae617014202fc439fa8a02f485440c16420c16420c16420c16420c16420c16420c16420c16420c16420c16420c16420cdfee87dc1ff9c8afac3c9198fa1740cb95f64a6a026faa2f87529fdbf863a8fd7d1352d14ef9959527b4e42300ee7fce491923784d39194a7d8615c2218b182c84182c8418be2da40c74d6cb2d3597615ade964665b6f0b585a88a8a4cec04546e7803e3fb91d315a3b3f6134b1988408e9aa1c691899d0822eb434f60a7715c1dddb69e20b33781c6958496c298fe4c90451c3a020ff4293db3af25b6990e527ade712387113529a3a8038fb4a1d0b8711c864263817ccac85b0d48513bab6e2a86ac220f2452270baaa22263f0c47137a41f19438df3c4711742392a861a47daa0f3fb1e9408ed344d68299e38f621d4b891d0523c47e921f4403e6ee4588a83d085001d293c47e940420880ce8f30b2143a42ba13c7a80f879f02abcdb7d643777a410000000049454e44ae426082);
INSERT INTO `categoria` (`idCategoria`, `nombre`, `imagen`) VALUES
(0000000010, 'Cereales', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000b4949444154789ced9d5b5054e71dc07fcbb2ee2d2c97851585959b22e26a698bad69b424253113dbc63ab14d9387dc1e1c7d483b493b6dd387a4693ad34c6792ced469ccf4a5c6e9a8996823e90c691d9386b438b6600b06454091155158586497eccd65973e6c7615d985bd9efd48cfef09cee53bffd91fdff7ff7fdf397b50fcf81fcc22230c39d90e40662eb210c1908508862c4430642182210b110c598860c8420443162218b210c1908508862c4430642182210b110c598860c8420443162218b210c1904c8856a9a3465f8b56a9636b71331643c39c6d8d8577d358787764bf56a9c3626898b7ad465f8bc5d000c0d6e2660a971959a931d3587837000faff85ed46d2b3566566acc6c2d6e0660dbf26fb15263a67099911a7d2dc119a93e89855148754f7d6ff5f354eb6ba5b85452f48ef5f36afbeb98d664378edcec5e5e1c8241981e8760004cab21274b9f8c9c43eec03509d7ce91b5214cbabf03771f041d925d2e61dca3911f7dae9014d36a58a697360cb987c4c0e782911eb8e992f6bab2900508064252a66dd25d5316b208c100d82e4a2745161227b68b307139f3d791852480e33ad806327b0d5948824c8fc3b59ecc95c5b29024f03833375791852489cf0557bbd35f16cb4252c0ef4bff5c6549af650d5d323274a968ceb6ca9a492a6bec92c5109eab1457419e29f5f6969c90a14b46ba3acae8ee2c8fbabf0d506b66d8fcf5cb6cde3a8446ebcf784ce1b90aa42e65c908999ad4f2d7967aface2d5ff4589f3797b6136be8ea28e7fb4f9fa174a553820843523c0e525ac25f1239a4aba39c375fdf1a978cdb71dcd072e08dcd8c5e336428b2f94c8fa73657115ec8f1231b69797b233e6f729dd9e7cde5f8918d698e6a61a6c74315583265b1d0428e1fd918335724c2d835031f9d90f6566078093f5129c20a49978c30a73fae4a5b5bf1e27381f54c6265b19042ba3aca9392b1e7fe67b198dba2eef37973b9d093580e4a07e1b2385e29c209095753c960d04ed0bcfe20ab8ce7a3ee9732b9df4e3000c3ddf12de10b27e4f8db5f483881ab556ed6947602d0debf8bed5fdc8f5ae59e77dcd025635a624c9678eeab0825a4aba31ceb1d33ef7878ec6bbf64e7a6d770b84be81c7c88d959056b4a3b321061ead82e2e5c160b35318c5509a9556e342a170e7749e477009f5f87c5dc86c96005c0e6ac00a0bdff111aabdfa767b8694e3ba323d20f5901a51e7bd10eecc61d0472f428832e6a079e810117c555f31f3712a687747594e3b8a18dbaafb1ba95ed0dfb81908c3dcdcfb2a5f62800f9baf1c87103a38d000cdbeb23926e47abcbfc32caedb8f41bb858bd0f5bc9e30472428fafe4045c2803aed07d952865b1304216aa8034b9eec807bca7f959d42a37a6fc218048afb1392b223dc2e12ec1e12e9997dcf30b3d19887c3e01a59eeba5bbb95cf16bfcaa5b8b5bcaa08b8ae15f457e0fcf55fcde5be70a3164793daa0597454cf94391614aad72d3dab5870d9f95b7cb0d566cce0a0e9f7a71ce394e4f71e6025e00977e0357573e374784ca6fa3d8de4281e324cac0dcfa377c5fa5cc127a064c0821f1cc0f1cee121aabdf07c0e92ec16cece5a9a69f61325839d0f62a3ebf6ed13632b92c1f50eab9be7c375305cd916d7a770f055327299c3ab9e0b9e1b94a699d2042e2991f2814b37c63fd4100aed8ebf1f975980c565abbf64492f9ed988dbd78fd731f3bacac994c4fc077e0ccdbcc48d973913c61983e8d71b205bdeb93b8db080642c39770424c062bbe195d243784f1faf518b413913c111ea2a2c95853da89cfaf9bb34fa3f5a7bd87dc542d67a4ec395c3a0b0005531f601a3fc432ff58d26d0a21646a32545d59cc6d34af3f486bd75e2ce6367a869b70b84b2265efb07d1dad5d7b80e822c23456b7462aae306bd727ff2145c356f23876e30e004ce387304eb6cccb0fc9208410c70d2d6a959be6f50779b7e3475cb1d7b3bd613fc313a1251493c1ca6ffe72987bd61e5db4aded0dfb311b7bf9a0e7c939db1b368da425d670d20e2af518ed2d691311460821001bcc6d383c255cb1d793af1b8f5455e125919f7cfb31dafb1f8979bec960a5d9f21666632f9d830fcde9419535f645872b57508d336f335e4d3510aa8cb4de4134de4100bc9a6aae97eec6aba9ca888830c20851ab5c9161c6fcd9fcc1626e8be403b5ca1de931b1e81fdd446bd7de79f9a769dbc598e7f47af2d937ba8e938e15609ebf5fefee2190a3c7aba9c2347e8855c3af644444182184547c56fd3456bd8fcd51c996b54769ef7f847b6a8fe17097f0e1b927d8deb09f316765cc366cce8aa879a5ce3216b377ec1b5dc7bed1ba056373e92c28832e560ffe20d25b328910420a0add0c5faee79eda63ecdcf41a366705ed7dbb18b8be098727f4d77ef8d48b71cd356e47a3f5f3e0c3d197e27f7ae5cbbc3bb92abef8a63e9044060822a4b26692eece8db4f7edc25c7c9e0f7b9e00e65652367fecaa2a163b1e3d4b41d1fce592938e1571cb50065d28039f267ced641142489d658cbfbde70f25ed05127722346d1ba0ce12bdd4fddde8ba84da324eb6a423a4b810627151a3f5f3d5ad43696baf61d355eedd16fba6c3054f7edc6d996c87329ac4ef440821009bb70e451d5e12e5de6d03ec78f46c1a220ae50e297b07082444a3f5f3e85367927ef4337c7ed3023d23ccfdf9d7173dc638f91ee5d77e9b542ca9208c1080d2954e9edcfbaf84a5346cbaca0f7ffe51cc9c7127afae3ac357ee9a88ba4fefeea1cafa022b46ff90500ce942ba576b947e936a4d695cc77a3d2ada4eaca6abb31caf4715f5188dd64f9d658ca60706921eea7a3df9f47af219b9a9c7e4efe3d02969f3453484a8b2ee44a3f5f3e08e5e9ab65dc43a5834ef5e78658d3d32994c85755a07ebb4a197199cb38d665d06082864d6e78edc685603b5d5766aaba31c97e6bbb1ba192feb0da9bfb6c1351360c8ed5dfcc0180825243036c8ec74f4b13dd3988197eaa2984f8277466cbc3392dc72bf50493d5b32d2cd77cb92ffd68e504264041392535496ed10d2c21b8357933e57a81c9253548622af18667c916db30a0dfea2dd90a3ce6264b139f19fdff3effe5b77326d3e3fe3be9b49b727941000854a0daa5b1fbe02c855f4e32f783a7b41c560c269e58f9d6fa5b54da186ac58285da72028cd53878970f49f2fa7bdcd25210440e93e95ed10e6d139703ced6d2e1d219eff663b84399c1968c1ed9b4a7bbb92e590c9899be814bec50f8cc9590a4a163f4a2ace0f47ffea5caa2c991e0210705fc9760811acb6ae8cb42b999099e42bc1082209397fe5a38cb42bd990f5a7eb6dac9da9e6f4d820e6bb0a2952eb393d36c87d65759cb55f459baba25c5fc8e9b14176d534f2f7910bf3b6e5e59ce281e22d52851c9309e7fc2f03a50bc9ee8740e84d6c9e145e3bd2b8e63b3cbff3cfe90b2849ce0cb4f0dabb3b33d2b6a439a464352853e893991a2612a5632073f7d92515a2d284a4248bdb37c5c73de99d19278ad5d6cdc73d0732d6bee45596be08f257247ffeb1f69771fbb2f7caf2375b33bb849395b2b7b80ad449de9c1b770cf1cae1fbb222e5cdd6673256ee86c9da3cc494423eb1daba78e5f07d586ddde90d2a06134e2b2f1cf8524687aa3092565977326dbbf56abc64f9bae529766d79896243e2cffe2ec684d34adb270738d69efe45c458645508c0e885d0ffec48950a53034d1b9ea469f536b4f9893dbb7b3b8a4ffbb0daba39da7d24238b878b5e3fdb42823370f5ecdc2fcfa7c22fd655539fa767f6aeb5909b07b986d0cfb1f05e43e10d7ddd4d3115fab6562a0f29a44ad66f50e5e442e9dad0eb8bd2c1b8ef26e4e9517cda17d9a698f830f136b244d67b48981bc330399ced28b28f30abbd8566d066e7fd6242218c1008bd5a2295a595cf034209c9c94d6d69e5f380504220f5a595a58e704220b5a595a58e904220b5a595a58cb04296e9c15899ed28a447582110fad70ffac45f52baa4115a0884862e9526db514887f042c24b2bff2ffc0f9b380215ee059b7f0000000049454e44ae426082),
(0000000011, 'Desayuno', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000ccb49444154789ced9d6d6c5bd779c77fbcbc7c114d5194444b946cc74b6c2556ec34ae81d8b133a4499af505c3967af960f443932e438761fbb0adfbb2b535b61619b6622952a02f18960149db0f8b8315711a245903bb059cc6f562d7915f342bb12559b6de284bf41579495e5e92f7ee834c8aa4488997e2bde115f80304e81e9e739e43fef99c739e73cebd7424feb157a745d3207cd20d68514a4b9026a3254893d112a4c96809d264b40469325a8234192d419a8c96204d464b902643b4dce09924e2500a2192b5dab421b45e91ecde36b20ffb2cb56b99200e45c7f34ab4e985c82344b2b87f19471c4a91fe6a17bad7618d5d4bac009e5725db88518c10c9e279256a9d3d2b8c8843298409d50a53a62044b28843296b6c5962e47ac60a33a6e21c495b62c71a41a49c15664cc5a158b36dd49af636199608a2852d9f5d371cabde832582642c9ecb9b8155efc11241f4a013f5a98015a64c417d2a801e745a62cbb2be24bbb70dedeb3f46bf779f55261b86feef8f5b66cbd2415df75bf32db333960ae250352bcdd9124bd7b21ce7aee098b65990e871a049b98d35863847d2b88f2fe2483f5f481bd92c3297d3d9aae8dc93acec39633e8149af83cdaacea05c39cfa4d7c1151774e4607fb272f01611e1629b838e1c7c2aa5e3ad906dd1091ff81c78743890d4f11499d33d0e94bfe8b64414d3057148393cc7a492b4378053b796171aff14d85356ee2cf06a5283e4d2f5e781cf95e519057eace8a02c5dcf004f95e589023fc8422abea4c239e02fcbf2a4807fcb2de7790ff866f17b48ebe827e3f074709577da184c1f432a2dca9d2abb3e5ba15c79dab50a792eaf512f2c0950dc82d1b26b80e9b2b4e89d7cc5287319aedd4a91d5cc5d42315d907237afb4903d5d21ed76853ce51f64f98756a9fe4af6cacbd552cf0d49674ece70612ac1a262de3682e982bc2be50adfdc29e0950a79a2c0ab2c7de0f9ffcb3f9054517a0af8e59dfaca79a5a8ec292a7bdfbb2c8b7096ca9ef506cb1e380afcb7aa73f542867456637826c9f882628ab738cc3cfdae283adffbfe2d94f4c63960ffd89f7869f32fed1e7a44819d9bbd74781b37149bea21e787521b4a0c80eb5796bb2b33bcc55441ae58b4a963250bb32ba7df3331b561638ba982ccceda2c08ac81f8edcaf150de5b6ede5edf97d05441365a77550b37a53417a61224d4fa76495b3b86269050735c984ad4e52d2d414ca41e6f6909623246bda5258845d4ea2d2d412c24a1e6b83c93642656fdd0a0e5c741f61d39cca78f7cc96ab375f1e1b1e39c3ff67a43ebcc693ae30b0a0b890c039bdbf088a53e61b920c16dfddc7df021abcdd6c5f8e90f4cab3ba6e4189a4a7057a787be80bb906edb2e6b568d56fcdf4ee4bde5f24c82747629e0b4a520bf912ef393e9779173298e8ebeccd16b2f7fd24d5a17796f9989a9d67759ebe1e773a7188a8ff2be74991d6dfd7cf9d2f3ec6ddfc98bf795ef01da8fbcb7d8ca437e32f90eef4b4bbb14a3a96912390561729aa3a32f23e7acb95dc06c6c2388eb851ff1d3e7dec59f285d517dcf1fe5427c94d3e7deb4b43d0eaf1f67471fe296dd88bd03383bfa7088eeb50bae812dba2cf75f7f03e76bc7f9cfbffd14f2269181f13857ef6e2fc9f3c3f4fb7cf17fbac97de1b3a6b523b82dc4f6470669dbbf0387c75f314f367295ccd819f46c7d372835bd20ce63afe37ced38007f7862927bc7620c8cc5f8ab7f39005010a76f2e85eb9bdf403b7b023dd0be5a9586d97e689081271fa4635b68cdbc62ef00ceeeed28678fd5254ad30be27ae14785fff75d8ab2ef529499deb642dad1172f3230162b5c8b2ffc90cc77fea121b6451f7cf15f9fc5d76d4c6087e8c673ff1fa05c7ccbb0cda61e4384e1111c932bcfa4f445527cf7f9f3fccd4b574ac40070be7372dd76d5649cb173efd0becd61588c3c42471867f776c3e59ada431c372b9d2b59e2d13391ca652a086884c5d97126864e92ab730c28c6d9bd9ddcc284a1324d2d887079c47099db807cfa03861d737cdbfd6b1e09ee21eceee4c1f69dfc7eb0fc7ce432b94c9ac9e1df109dfc681d2d2ec519ec335ca6a90531c224f03b2001f0f457197bb817beb58ff7a5cbf4ba3b7924f80043f151f6b6ef585136159b6762e857a462f30d6d53b599d86a34b520b9430f217e6fed7c97eefce5997aa08bb7beb57c635044bdcd77afff17614f1717e23b78b6fff385d752b179ae9e3e5ed245699a862c27d1340d4110f07a3db8ddae06bca3b569ea41bd162629150360cba5287f7ee404a1f17821cdb520f374cfa32562446f8e3072eab515e3852008f8fd3ec08124c5999e9e63727216498aa169e6dee3d2d4826887f6affaba0afcb6ca6b63077b982f0a1e27db33ecf4f517aea3374798b8f0abaa750b824030d8ced6adbd04027eb2d91c92146772324232a9d4d6fec4424df94aec1a2e6131daee5d555f3b0fac75f26bf0e4f24c6df6a32bc0da62142308025d5d1df4f7f7e076bbd0348db9b905a2d1c535cbea8a5c938d629a7a0c01d077ef82e195b3ad0430b64ab9c113530c9e5812e39edf4688f5b6b1f5199721318a71bb5d84c321a2d1456439492c26238a4e0281ea03b79630be4fd3f41e92adb2dd6b64427ccf99087bdfb88ede26d425461e411008853a0906976ef18e46175194eaa7493469c6b88dba5b6711daa1fde8dbb6ac489f34584fe0a1fb99890c35a44dc1603ba15027009214af9847cfaae41637a0200099bf2bdd80bacd9d78a346049f87d09f7db621d1771ebfdf4720e04751d215bdc468849ec71682e48e1c2e997195df5db51677fdd33368cec69f33eeeaeac0e7f3569c75e52257ebaad3168200a82fff80dc91c300c89eda36825c8176ee7bf1ebd0e92949d7348d584c46926224930aaa5aff29fd50a87345d0a82b725ddd15d848103dd08efafd7f2635f37ff4bcf5ea9af9ef3a7298878fff0769ffca6e4a100402013fa228128d4a4c4fcf71e3c60cd1e822d9acb153eb8220208aa5f751666e9c375447314d3fedad44c7ee5decfcda335c7be9a72bd2b71f394cdf179ec0130e317cf267abd6e3f7fbf0fb7d48529c584c2efc0583ed8599542d78bdcb1ea867d5bac70fb0a920000f7ce7efd9f1b5af90bcb3dcdeb17b17aea29dc2b173efd43c880783ed04029b989fbf4d32a92049719249857038842018eb44b2d3c3756fdf828d0501f06ddb82afc294585e98627176dc505d8220d0d3d34d2c26138d2ea2aa19e6e6a284c36b6fdbe6d11599ec54f9ddf3c6b05c10e9e6b4a947340162d2c5bacbe6c796f9f9db284a1a498ad5dc7d656e9c5f977780c9b7451ffd76e55d3d33b9ff8ff733f847ab2f4ad682aa66989d9d47d334b66e0daf18b8cbc92dce90bef8f6baedda6696550b2e9f879d4fee6d485d6eb78b9e9e2e0062b1b5170933a3ffdb10bb1b4a9081271fc4d5b6fec36a79bc5e0fa15027c9e4eaa72233373eac6ba9bd121b469072efc8667385658df5047e7ebf0fb7db55b50e2db14066a2feb8a31c5bcfb28a29f70e5174a22869643989a2a41104019fcf8bdfef2b891b6a21bf90588e9e55513f7a6f5ded2e67c308b2fdd0e08ab47ce027cb492429862c2791e5247ebf8faeae8e9a638c6af93263671ad655156c35b4b632bc16fdc4c3e6fbb6ac7aa0cdeff7b1756bb8307d95e524d3d373ebeacab291ab64eb5c405c0d530509f75a7352a392775422186c2f44dfd96c8ed9d979c36b57b0346ea81f577aa8d3fa315590c15dc6faea7ae9fff43d35e7f57a3d2bf6c78d90bc35df9078a31aa60ab26f6fdbda99d649705bc8f05457149d84c3a1c2ec4996933595cb24d3a4874fae3b1a5f0dd3c790c73fb3c94c1384ee5bb996550b8220100e8710452792145b337f2699e6dacfdfc4e3347e92c450bb4cad1d78e2313f7d26feb2c0a650fdcf94cf2f286a9abeea61854c32cdef5e7a83fe7ea96a9e46614960f8dcb35ddcfd7b8d8ba08ba9e5269ad570bb5d747575541ddcf3620cee89e1729b3f6bb44410afd7c173cf76f2c4637ecba6c246c8c72be54837e7b9fe8b372d13032c0e0c1fffcc260e1ef0f1e18514e3d755c6afab2816fd949051948505b48fde66cb960c60dd97c8f248ddeb7570f0808f83076aff8194d3e3d5075d6f6fe31fff9d9d1a461b3b83c1159686608bc5c54deeea1fbaa6543ea8560f7a5645fdf814ead89986d569145b08b2bbcf575514adcee336e5e41667503e7cdd94e51023d842105170b0abb70da7b0b22fcf2d4cac2b50cb7b45fae2db759d566f34b61004969e22bda7cfb74294fc076a143dab9299388f72f6d827ee15c5d86af97d93dbc9c0e6364622a54b1db98509d48f4fe1bef7d135ebd015996ce4e3751fd7310b5b0902d0e513d9b9b98d6bb74ab755b391abe4166770f5ef410886113675034b0268e9389a34436ee146c3f72f1a8ded0401e8f1bb48a4732b9e5da82bf2273a436a04b61943cab9bbdb4b8fdf9afd162bb1ad20003b37b7ad1aa3d8115b0b02abc72876c4f682ac16a3d811db0b02d563143bb2210481e518c5ee6c1841603946b1331b4a10588a518a9f146d37fe1ffb6b0bc6ca10c0a70000000049454e44ae426082),
(0000000012, 'Filtrantes', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a0000114149444154789ced9d796c23d77dc73f7371484aa22452dad5b13abc92b5bbce6eec780f3576e075d64913d78e9df8af38059c146d022488fd4f03146d0334281aa4015a20488ad8499ad875eb386de1ab3e121bb1610749ebddf56e7cc6bb8af75edd374971c8b95effe09222454a22c5114539fa008234338ff39ee63bbfe3bd797c23fd63e84ec1163583bcd10dd8229f2d416a8c2d416a8c2d416a8c2d416a8c2d416a8c2d416a8c4d2b48e7ed07b9e5a56fd2ffc54fa0360436ba399e216dd68ee14d4f7d9d407b73767be4d9e3bcf7c31730466736b05595a36e7403d642e7ed07f3c400e8b8ed201db71d64e4d9e30c3f739c99136736a87595b12905e9fbe227963d961166e6e419cefce8f94d27cca61324bcbf2f6b1d8a2cb3abb78b78c2606c7a06d3b217cb5ddf47f8feaf10fbfd08171efd15c3cf1cdfa8269785f2317df73736ba11e5b0f7efee26d01e06607ba499c6fa3a027e9dd6e6267c9a86914ae1b86eb6bc1e6960dbe1bd74de7e083b6e101b1ad9a8a697c4a60aeae1fd7d1cbcff2b40da3aaee9eb41910b13c5f9f80293b3f3c41346c1312316e717fff1df3cf5f04f31620bc4458a3a49272069b4cb21fad5560ef8bad151b9eccce152ddcbb3a90439f8c057085fdf07405b4b98b648f38ae5d3ae6cb6a83066dce0bd475fe2e2d3c79819192726929cb367981309e26e8ac37a3f877cbdccba0952c22e72f6f561d30852aa7514c3b46cc6a66798998f153d9e499995b105da9510979d399e4bbec38230f94ce05a029246d44d7af6bfacc4a689214b6347a82e58f2671545a6b1be8e706308c775312d1b2116efc386814e7aeebe89c8ee6edc990582e329f6fbbad124856fc59ee75aad93362584212ccfffaf82b66e0641c2fbfbe8bf92ea2ab24c6fc77664492afb3c19615a9a1b9165192399ca13c6d713a1e14ff611b8be1b7b6c9e6d137044dfc583c6ab44e43a7ad53086b0d635aa6c8aa193dc7e476bb8a96457b51c8a2cd31669e69abe1eda5ac2f8b4fcecdfffa16e9afffc230034c9013e1f1ce4b9d43bc4dd143b942664cabf194aa5e60509efefcb06721c9b8bbf7d1533e58d3fcf0ab3332d4c2eb33ffe75f6efab94081ff50df0bd85579091b84a8da04bebd385ab794172ad6361f82ca74e1ce5e99f3cc0ccc498a7f598d6627c48fef622c6c98b79c70febfd34cb415eb5ce2323b143692224fb3d6d03d4b82005d6f1ceeb002c44e778e6c10778f7b5573da9c7b4ecbc0c2cd73a72f9947f2faf9ae7490a0b1989ed720311b9ce933664a86941965a87954ae51d3ffee22f78fea70f55ecc2c6a61747888b5947863d6a1b4149e3b43d91dd1796836c571a3c8b2b352bc872d6b194f14be779fcfeef3076f1fc9aea29d53a327c48ebe2b875216f5f48f27b16ec6b5690d5ac23173395e485471fe2f88bbf28bb9e52ad23c36e751be34e0c4b3879fb7549f524d8d7a420a55ac752de7ded559e7ef001e2f37325952fd73a00c2721d0db2ce79a7f0419817c1be2687dfcbb18ea5cc4e8cf1cc830f70f0964fd2b7efba15cb966b1d197a94306fdb235cadb666f7591f0c62ef498f1e84682528bb58ae43f497c358e3856369cb51731692671d50b275e462a692fce6b927f9cdb34f2e1bf0d7621d19f66bdd1c3717c57376f8306f6ec4e9d1b33f5257005f4f3d917baec6d7557a2656738274de7e30fbb739395c96752ce5ccdbaf2fdb6759ab7500ec509a18505b792af916a25ec1f86c0bc29fbe94ad9ddda42c8bed5d3d00489a4cf3a77bf1f5d69774ee9a1224d01ea6e3b645412ebd75a2e27316ebb354621d19ee0a5cc751f31c3ff01f63726e8ec99959de38f57bbef5bdef73ffa38f313b319e2d2b3768045a4ab3929a1a5cdcf39777d230d099de706c744d213e3385eb382b7fb00446cebdc7d8c5f3740fec666c660e23650269eb588b20baa4728bbe8bd74313fce7cb2f71e2ed538c4c4e71f387ff882fddf339144dc5588867cbd79f7570c792ab3e5ba9a9e721b7bcf44dd4fa25198a63931cbfccf0a9375988ce575cc7b61d3db41db829bb3dfad59f96e5ae9692fc74187b60f57961faf373686f2c30e71a4cbaf165cbd594cb7ae5ce7fe0cc8f5ec08ee7046245c5dfd14bdf913bd87de311ea428d15d511ee594c18ca8d1d9520d9e9fbbe490ed0a18496ed44d694cb724d9b999367b8f4f8ff214c9bd0ae4e64df6266aed485085fb58be6480bc6fc4cd901bf2ed448ebdec51835f9cd67b1472bb33a7b770037a2ad5a4e3d6d204fa5dd954f52a9937c24b00a9ed9d794201932c22cbc7d1e296512ecd986e481307d076e44a90b016b8f1d4b295990a145410054492624fb49081387c5593235d531ec18eca763f06a3a06fb693fd40f806544b112b32c580a861ac6cd199af0b576d277a4137372984b6f9d5831c6d4851af1b57666b7f59918cd877a983d7661d9cf9482f095e8f5edc2502d23d1ad3433eec6b2cfec373ca847f674f2c12fdc4cefc7f7a10655ccd834c6fc1856621ecb88169477eb3b719baf46a88581d49c1c66eafcef991abe54706cf78d47b282a84e82163581af21829b70997cf13497feed28f153e3059f5b0d67872fdd439700253f2e88cc3e21d05f8e22c597cf1633c17ec304e918ece7c07db7d27ea89fc4f4258cb93192f3a53f7472eb3b719a07402d1c377262b38c9f7a232b4c5da891be2377648faba3479192e98ea1bfb18d40531bc1481773c72f70ee5f7e55b1d5ac950591aabe20be50808f7efb4fe9fdd83e927363cc5d7e1bc7cc1feb11928cd0745c5547c82a42d30170551da47c1721a921645f18a4423f9e11a6a5f7eaac7548a959d491c2075b8a2f40d38ebdf89bda987a7188dffdf5ff60c7aa33f52797aa0ae20b05b8e3917b89ecee64f6c2eb24a6175d8ba3d7e16a015c5f00a1ea659f7b256172719397c18c215b06b26920a7e2486231a806235d34f75c47fcd43827eff9f7aa8b52d52cebb69f7c996dd7f6327ff91d16a62e20140dbb2e8cd5d886eb0f21343fc86bcc33dc14c29a036123293a484a4111e118086b066419a1eab87a1d4eb011a1fa90848be4d8584614e1d884765e45e3759d8c3ef16685ff757954ad63d877db87683fd48f6326884f9cc5aa6f2115e9c1093615b8a14a10761427711e37398a7096b8426bbaf003928ce36fc06ceac46c6c474832f189b3b88e45d3c11eb6dd7a8d676d2b05cfd3de86ce703a580ff6d3d0b938b5c64ea4fb0aae63930a77adc92d958370e208278ea40490b4c8957d2b3f9770f53acc7037be998b08c702452372533f133fffddbab635174f053970dfadecbff793c52b0aa605d002211459a15ad3978563209ccba5975754a4a676145ffa61931dad6e0cf1cc57dcf0f5bb961563296dad3dc84ef566949783ecd8b475edcd6e573ba87b2248c7603ffb3e7fb8e4f27a204457c700baa87c58dd4bfc08ba3a065054df86b5c11397b5ef0b3717ec1b3df61ea71f3b466c789ac89e1d5c73f78d34eddcb658b1a6b3a36b2f2923cae4d4454c248487c1bd5424e1e243d0dad28d1e0855bdfea5782248efc7f6e56d9fffe55b3cffe57fcd6e8f1c7d0f3d1428ead2f440881d5d7bb1ad14b1e804092386e9b8b84a61daea15b2e3e053648281069a235deb56cf5aa858908ec1fe827daf7df7e7e53744d3698e7491f94e946da5482ccc9248cc61dae9af00b8025ca5f426cb8e8d2c812249a88a4a30d844b0ae19555bdf0caf12d665b477fadde1827d53ef969ee9405aa050531ba1a6b665cba48c28a95422bbadeb41cfdd4efcddf2071c2ba16ac3ef66b4f4b949a5a20742ebeef7376596550a2347dfab56559e52ed91df751124b7879ecbe8b1cd25cadcf1ea0fc3572cc8549178d1317875d1b29bcd4a36e2b948c582985183e953f9a2ecbfaf788ffdf463472badaeaa547ba4173c7259a71f3b96b7ddd0192eda598c0dcf14cdc06a91f8a97192c3a5cda2f7126f0479fc28662c3f8bbae16f3f43efc7f715947deba157bca872ddb9f4f0b1d50bad039e0862468da217fa13dfff0b76dd3598b7eff4e347890dd7f62263c9e139469f786343eaf62ccb7aedbb3f2f9a45ddfcedcf71c723f7e6655e2fffd5235e55bb2ebcfb374f6f58dd9e3ec23df7cbb7e83ebc87604b7e672d13535aaed94173df7662c333b47ea09b404b8357557bc6fc894b9cfddec6b955cf2739e44e64d8ccc44f8d337bec0273c72e307bec42d57aeceb32ebc4170a30f8b54f71cddd377a7dea0d237e6a9cd127de60f48937d7551ccf05e918ec67d75d830cdc75c8cbd3d614a34fbec9d8136fac4bc7d133411a3ac3dcf0f5bb0a9e8dac8e40b802a9c205652ac14e2d00204912b2e2432a71887feac52186bef582a7fd154f467b237b3ab9e3917bf195b0a0b163a5b09331849bfff856d174d44023d21a965d5a2b42086c631ec7ca9f3d2fc90aaabf016595e7262db70cd074a88793f73cbca679c1c5a8f8b62c470ce13a5889b90231202d542a3a816b9b9536a9245cdb24159d281003566ee752d4069deb1fbe87fadddb3d6957c5821cb8efd692c40056fd07adc43cf1c973a46253889c9545bd44b82ea9d814f1c973588995bfac538a20901665e7574b9fe4b1e2b92a3d4139314352d4f42c45b1ccc59664100ea9f8349611456f88a0052afb0a5b2e96314f2a368deb588bf52ddb58b9e4580269f7e505150b928a1ae8a1d22c449264f4ba664c631e51645e96eaafc34925705d1bd7b1d25f51884ea0051ad1028dabfaf46238560acb98c732e6f3ac4e965514bdf8ba8d92a2e20b342295310bc6ab54b86241cebffc3abbeef870c9e5254545af8fe05849ecd4429e309224a3faeb118e8de398b8b689705dcc8559cc8559644543f1059195c58b292b1ab2a2e13a56f6ce7752095cc7c631138bd6700559f5a12c9349498a8aaad7a168e5af5532f6b237d34d2b16e4d7fffc337a3f7a2d7a43e9ab8402289a1f45f3235c07d74ee158295cc706e1a62f8ca2822f88706c5cd74efb7321b08c2b7e3f5e64e2f412645945563424594196d5421124392daea623ab3a92bcb6a94756cce0edef3c858fd5bf6bb81a95bbac98c1535ffa27eefce1d7ca1605d229a6e20b66e7d2e60a90f991affcce4388a24157921558923a4bb252f05354a03560c50cfef7cb3fc08e263d11a4e28ea1eb37119a4d43478423dff8333a0fecaab8512b21845b34fe2c4552d4b262c05a983e7186d7fffebf488ccea2993e7ca9cad760acbca72e099cba244882b01264f7ed3730f0a53f26d8bef232e09b99c4e82c433f7c814bcfa6d762915d097fa21e4954dea9f564e844282e7575d0a42c665b5db7ed7fdf09b35408004948f8134164d79ba9af9e08522fe9b4a9f5a4fc068e9aefd743031decfcec47683bfc01b44df8ae282b6630f6ca3b9cfdd9af892e79d585622be8c90092f0ce35562c882ea9790b40da9a85e54be2ca85a70d0d74d076f803b45cbf93c8febe82e3b5c2f489334c9d3ccbd82bef1488006917a5997e54abf220be948ad20c1989f6250ba9a896866a694585890e8d101d1a61e8ca76e4fa9d340e74101ae8c8feae36d1a111e6afb46b7e6884e99367972dbb9e4264a8c842ba95e65557e1746507cb67e2a836425abdaa607b3381f6661a073a501b0268f57e1a73842ac7b2a673de3f353f3482154f62c70ce68746304667498cceae7a0e494828b68a66fa3c8b132bb1ee93ad6537ed67212d8ea3da388a5d106b3224ae5ca895eed4f546b115144745b1d5aa88904b45825c76e6d8a13495bc56adec2ac8a682467a4cca516c5cd9c5551c84e42e2bd27aa2d80a9290911d05d995519c8d5d8fa7a2da5d44d9a2e4a2382a8a03e40c3709c9c5955d842c10527a30d051ec9ce3e02aab0b273b0ab91e3273a1252123b912b22b7b9a1d7945c5b74346946eb5198dcacd5b12328a2343ce35cf58d41f029edc222e8251275af5379abd1ff1cc6653c2de90d7ccbddff0d4896e8952399e47b594b019712a5fcef50f957549330c6131ee167f67e0162bb36e795fd44d6e89b206d63511df12a57cd6bd67147593ccb9de7f47fdfd4a55baaa936e9ca8a8fe82929b91aa8d1d8c3bb12d514aa0aa8339e34e8c05b1f617b4fc2150f5d1b5312756d5f7936f36aa2e486630724b94e26cc8f8f39628cbb3610f0432a26c8d7be5b3a14f68b64429e4ff01015370138f500d3b0000000049454e44ae426082),
(0000000013, 'Snacks', 0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000a0b49444154789ced9d5b6c1c571dc6bf3397bd8dbd5e1bdf62c78ed3d62589936095540d299552b5a1095551830479a954a004f1c0431069251efa52043c3412052488221e52212e116a2a845002b4e212d569a16d1c50d224a46513dbf1651dd7f1de7776e6f0b099cdce7ad7de9d393b73d6999f646976cf9973ceceb7ffcb39337b4c1e3bfe04850737086e0fc0c38c270867788270862708677882708627086778827086270867788270862708677882708627086778827086274805d2b3292426e2a0baf30be1929d935fdd7d048ad26d7a4f1d1f43fcdbfb8baf3bfe3a6b2a4f1f7f19e9578f143affd42e845f79dd54be74683ff2e7c7aa9697d7093e7b18c1af3c5f571b0b8ff6148f5b7ff43ae4d15d553fa3367f05df7af765c4b46cd53a2cb16521272fbfc66a1cdc72f2cc514c7d1c77ac3f5b82fc7efa2cb4f80cabb1704772f23d1cbb7e15e9b914320b1947fab41d432e44df64310e2ef9e53bbf291e67173248cfa61a1e576c0bf2c71b6fb118077768f169bc3679c5f45e2e9e43722ad150516c0bf25e3e013a7f65f58a4dc69973cb930900d0b21ae2d1256859ad21fddacab20ca2e78fe3bed6010000bd3103f99ea16299f8cf9f9b3b4c5d2d964badfab272b9550759a17c599dd4d5badb281d9f14fd13c4dc7953b94a097e71f1ef553f2fd529925309043a83f0857d55eb5981b078eae40b4b317cfde6148bf170c169da821fea9d35d5f5770410e80830eb9bc9c4f07fbe208b66b8619cd67e8159077b2682fc27d0c2a2196e38434375d56719ec992d9dac152b19a701242c5c1656c19e99207312dbe0e616e3b01e0f8c609f5bca596e8399201fad110b395747fca804d5a9ad993d3b97e55f1b825ca56c2cdd6ab067264852105935c51ec50f323200f4770072f5a9d72c244bf1a31a46b0d755bde673984c0c01ce33ad901fd8b21ec4781d5b027deb32a0e64dd5a629b3cb5144cb6a484cc4a1f4b740f4affea5657a832ac5b39594d2150622cb535b3b017d25a84e919888d714ec990ab256027ba348cfa590994faf58c7bb855b82dd0cab16b28b59a4a69355833d5341d6ca5ca4d1a849b56ab0671ac5669b44105da3c83e300ce435080b7188738b10a76e1653decfadf70300c6660b3e7fff5010e71754fcfba60a00d8d5e3c3bd610963b3397cb894afdcc92a540bf6ecd30a5e50fc404e5b96490100288520098024007d1dd0fa3a40284562a2e030f6acf7637b878cef00984e69581712f10c0af1f12f9359f4292246da253c331c44324f71f462127f9eacff210823d807bb43c5657ca68224457eb22cf2f9070a07aa06dc4a951502fe50d9477ff89338a5527cf574ccf4f6ba90f933ed59efc7472556a148047bd6fb2d0962909e4b41cf69087406d90ac2d50263325bb01259043a5bcb0a49c55314996047afbff163ab4076310b5dd5d75e964522853b974859fbc6bef0601b760c2a204265d12ad1d522e3078fb463dfc6205a7cd62fa99a54d79620f4c9af813cf11cf4d6faee6794230a8024d77e693a5b447cb63f80ef3e14c1ef9eeac6be8dd63d45730b32bc0e18ea02220a68d0876cfe3c528b7f406ed766686de56eca19149960df46eb5f88a6ceb2c8e8d09d638d40ccaad0f285dc9e521d80334986e233f793a86331b11cd705218136483b0f824406a18dff165a746cc5fa42ef56889bf6822e4e40cb470ba92b008814a22c14057112b1cccffcf7636b7313c0659745026d081cfa17f4e818f26f1f83bcfbf965e54690167ab72278e85d08bd2390771f06890c80d07673fd9240ac3bf4e43aa93df6d784ab16228e1e000984417ab78246c7903bfd2200c0f7f44f000042ef08486400d9e35f84bcfb30b4e818f2e327e07bfac700001208836ab72ab64d1dfa250129370f00e373d6e7244c2da4de7b22daa553a09925f8f6be04dfdeef81660a17571afd32e8e275a8a75f040984216eda0b616817e8e275d3f9349534bd26cdb2fcbf024c05d99649d4559f2e4e20f3ca83d02e9d8630f41904bef9e69d794485ba24d0061268030088430f83688aa90e2125f1c32113112acc57a693d69f3c713da88ba307901f3f01ba380169e741889bf615cbf4990b000012192cd6259101a87f3b021219843ef93e707f57c57675ad7182f4a425205c38ae1443669a551069e737208d1e40e6e86340e616a49d078b6542ef56d0cc2de8d1b3107a47a0de8e2ffacc85a26b2323cbad8990c61b87586288f5cce86bc1750b31dc10e9dd0a7de602b4f113d037ed8376e9140020737c7fc5f3d289206e9e6dc5c096b2f64401d4988b68006970582917e4c6350d1d59010b7e6be9b7ab82e4df3e56b40c7de642c152505d048385d90e5cbbbc01adc32b87400a0a526521b15184558217ce85f1d36d714c29f5bb2ed72d243f7ea2aefab98c0fd72e6fa85aeeece50704d1dc636a9222a0113cf7410b5eda5139255f09a682287a637ec4528a9a0ea05dbe7d11a214f33f534de554a7c549a120aa2015a22ea5b4e2fbd5da2947cf01b9db37479392b97f7581a25d2668a3d67c2553413666577ea282054afb126e955ccbec872bf96a7afbaf5a99758c5ef358debf8f00b33e6b31a429577be550e385b7cbfbedd61eb86e4e4194d4ea955c266631cb622ac83d3967beb9cd6021d742d6e22953419c08ea002029fc0b120d595b826f4e9715e2db65c5fc3a52a2b5a4a1295d96206910fdd67fa5d468e62d6658006341420eb92c005c0b72b1555dbd52159809e254fc30f0879ddba1a75ea216033ac05010a7dc9501cf9956cccf81204e5b08cf7311ab292fc0501027964d4ae13586d811036028484fdef90be40bd777cbd809ae599c7f183013a4db0541789c8fc46ca4bc401307750090399cb15f0c7362214ece410c788c2356974c0c980852efe33face06d2e9212a9e52513032682b8113f0c78725b76332c809520aa7b82883e7edc965d770534b9cb02f89a20f263212eba2c9ee62256ef129662fb21872ed18ff6a78ea2747d33fb8f37a0cd17f67c97376f87bc799be99cd4c95f158fcbcbf5d82c3267de28be0e3cf23884ae1ed3f9a57568880258b4fb3198606795d7c0b620bbdbee05eddf617a2ff3cef78b9be1e3d93e488f9bcbd3279fbcf3a2ac3c1f1b43fae4afef0cf0912f412e6bbfbc0e19de0f9a72662bf06ab07057000397f5e9c8fd2cc6610b69c390db4340ccc7812054a7e8ebdeb67ac506236fdee4f610f8b090e45402a14f0c3319881da40d836e0fc1d64da9526c09f250df96d52b3980d059db2ed48dc4ee2aaf81ada0fee87ddb990cc22ed2864193db523fb8e4f81858a4bc0083bddf8dbd6a1bf5df02ee366c6759442050fa5b0adb1d79d886c955240241685d7d1bb6785486d9d75af48b50fa5b3c516cc2d4cf78a2d887b9e317fd2242eb94d52b7a54a42191580a4a0876dbdbb3ea6ea561a9912fecf344b1404373554f94fa69f8e4c117f6c11f716763c966c491d95ca033085f6b736cb2ec368e4daf833d214f941a7074bd23d81382acc84e76d97438be0015ec09d5f48f4dee561c17c4588cf444a98c2b4bb49e28d5716dcddc10c55bf732e3ee36b19e28cbf83f895178537a6279d90000000049454e44ae426082),
(0000000014, 'Velas', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `idCliente` int(10) unsigned zerofill NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `dni` char(8) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nombre`, `dni`) VALUES
(0000000001, 'edder sanchez baca', '10234567'),
(0000000002, 'paulo torres gaviño', '47455489');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE IF NOT EXISTS `compra` (
  `idCompra` int(10) unsigned zerofill NOT NULL,
  `fecha` date NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `igv` decimal(10,2) NOT NULL,
  `idOrdenCompra` int(10) unsigned zerofill DEFAULT NULL,
  `idProveedor` int(10) unsigned zerofill DEFAULT NULL,
  `idUsuario` int(10) unsigned zerofill NOT NULL,
  `idMovimientoCaja` int(10) unsigned zerofill DEFAULT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `factura` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `idMovimiento` int(10) unsigned zerofill DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `compra`
--

INSERT INTO `compra` (`idCompra`, `fecha`, `monto`, `igv`, `idOrdenCompra`, `idProveedor`, `idUsuario`, `idMovimientoCaja`, `estado`, `factura`, `idMovimiento`) VALUES
(0000000002, '2015-04-30', 255.00, 45.90, NULL, 0000000001, 0000000001, 0000000004, 'G', '', 0000000146),
(0000000003, '2015-05-01', 75.00, 13.50, NULL, 0000000001, 0000000001, 0000000005, 'G', '', 0000000147),
(0000000004, '2015-05-01', 514.00, 92.52, NULL, 0000000001, 0000000001, 0000000007, 'G', '001-201586', 0000000149);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comprobante`
--

CREATE TABLE IF NOT EXISTS `comprobante` (
  `idComprobante` int(10) unsigned zerofill NOT NULL,
  `idSerie` int(10) unsigned zerofill NOT NULL,
  `numero` int(10) unsigned NOT NULL,
  `nombreCliente` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha` date NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `igv` decimal(10,2) NOT NULL,
  `idMovimientoCaja` int(10) unsigned zerofill DEFAULT NULL,
  `idUsuario` int(10) unsigned zerofill NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `comprobante`
--

INSERT INTO `comprobante` (`idComprobante`, `idSerie`, `numero`, `nombreCliente`, `fecha`, `monto`, `igv`, `idMovimientoCaja`, `idUsuario`, `estado`) VALUES
(0000000001, 0000000001, 1, '', '2015-05-06', 9.36, 1.68, 0000000024, 0000000001, 'G'),
(0000000002, 0000000001, 2, '', '2015-05-07', 26.00, 4.68, 0000000025, 0000000001, 'G'),
(0000000003, 0000000001, 3, '', '2015-05-07', 25.80, 4.64, 0000000026, 0000000001, 'G'),
(0000000004, 0000000001, 4, '', '2015-05-07', 20.80, 3.74, 0000000027, 0000000001, 'G'),
(0000000005, 0000000001, 5, 'edder', '2015-05-12', 5.20, 0.94, 0000000028, 0000000001, 'G'),
(0000000006, 0000000001, 6, 'fefe', '2015-05-12', 5.20, 0.94, 0000000029, 0000000001, 'G'),
(0000000007, 0000000001, 7, '', '2015-05-13', 5.20, 0.94, 0000000030, 0000000001, 'G'),
(0000000008, 0000000001, 8, '', '2015-05-13', 5.20, 0.94, 0000000031, 0000000001, 'G'),
(0000000009, 0000000001, 9, '', '2015-05-13', 5.20, 0.94, 0000000032, 0000000001, 'G'),
(0000000010, 0000000001, 10, '', '2015-05-13', 5.20, 0.94, 0000000033, 0000000001, 'G');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallecompra`
--

CREATE TABLE IF NOT EXISTS `detallecompra` (
  `idDetalleCompra` int(10) unsigned zerofill NOT NULL,
  `idCompra` int(10) unsigned zerofill NOT NULL,
  `idAlmacenArticulo` int(10) unsigned zerofill NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `total` float(10,2) NOT NULL,
  `igv` decimal(10,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `detallecompra`
--

INSERT INTO `detallecompra` (`idDetalleCompra`, `idCompra`, `idAlmacenArticulo`, `cantidad`, `precio`, `total`, `igv`) VALUES
(0000000001, 0000000002, 0000000010, 51, 5.00, 255.00, 45.90),
(0000000002, 0000000003, 0000000002, 5, 5.00, 25.00, 4.50),
(0000000003, 0000000003, 0000000002, 5, 5.00, 25.00, 4.50),
(0000000004, 0000000003, 0000000007, 5, 5.00, 25.00, 4.50),
(0000000005, 0000000004, 0000000001, 2, 2.00, 4.00, 0.72),
(0000000006, 0000000004, 0000000010, 102, 5.00, 510.00, 91.80);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallecomprobante`
--

CREATE TABLE IF NOT EXISTS `detallecomprobante` (
  `idDetalleComprobante` int(10) unsigned zerofill NOT NULL,
  `idComprobante` int(1) unsigned zerofill NOT NULL,
  `idArticulo` int(10) unsigned zerofill NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `igv` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallemovimiento`
--

CREATE TABLE IF NOT EXISTS `detallemovimiento` (
  `idDetalleMovimiento` int(10) unsigned zerofill NOT NULL,
  `idMovimiento` int(10) unsigned zerofill NOT NULL,
  `idArticulo` int(10) unsigned zerofill NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `detallemovimiento`
--

INSERT INTO `detallemovimiento` (`idDetalleMovimiento`, `idMovimiento`, `idArticulo`, `cantidad`) VALUES
(0000000001, 0000000002, 0000000002, 2),
(0000000005, 0000000008, 0000000214, 1),
(0000000006, 0000000009, 0000000177, 1),
(0000000007, 0000000015, 0000000001, 34),
(0000000008, 0000000016, 0000000001, 34),
(0000000009, 0000000017, 0000000001, 4),
(0000000010, 0000000018, 0000000001, 52),
(0000000011, 0000000019, 0000000002, 5),
(0000000012, 0000000020, 0000000001, 32),
(0000000013, 0000000021, 0000000001, 34),
(0000000014, 0000000022, 0000000001, 4),
(0000000015, 0000000023, 0000000001, 52),
(0000000016, 0000000024, 0000000001, 23),
(0000000017, 0000000025, 0000000001, 34),
(0000000018, 0000000026, 0000000001, 54),
(0000000019, 0000000027, 0000000001, 4),
(0000000020, 0000000040, 0000000003, 8),
(0000000021, 0000000042, 0000000001, 7),
(0000000022, 0000000046, 0000000001, 4),
(0000000023, 0000000047, 0000000002, 4),
(0000000024, 0000000057, 0000000001, 4),
(0000000025, 0000000058, 0000000001, 5),
(0000000026, 0000000059, 0000000001, 4),
(0000000027, 0000000060, 0000000001, 5),
(0000000028, 0000000061, 0000000001, 5),
(0000000029, 0000000062, 0000000001, 4),
(0000000030, 0000000063, 0000000001, 4),
(0000000031, 0000000064, 0000000001, 4),
(0000000032, 0000000065, 0000000001, 4),
(0000000033, 0000000066, 0000000001, 4),
(0000000034, 0000000067, 0000000001, 4),
(0000000035, 0000000068, 0000000001, 4),
(0000000036, 0000000069, 0000000001, 7),
(0000000037, 0000000070, 0000000001, 4),
(0000000038, 0000000071, 0000000001, 4),
(0000000039, 0000000072, 0000000001, 4),
(0000000040, 0000000073, 0000000001, 4),
(0000000041, 0000000074, 0000000001, 4),
(0000000042, 0000000075, 0000000001, 4),
(0000000043, 0000000076, 0000000001, 5),
(0000000044, 0000000077, 0000000001, 2),
(0000000045, 0000000078, 0000000001, 3),
(0000000046, 0000000079, 0000000001, 4),
(0000000047, 0000000080, 0000000001, 4),
(0000000048, 0000000081, 0000000001, 2),
(0000000049, 0000000082, 0000000001, 2),
(0000000050, 0000000083, 0000000001, 4),
(0000000051, 0000000084, 0000000001, 4),
(0000000052, 0000000085, 0000000001, 4),
(0000000053, 0000000086, 0000000001, 3),
(0000000054, 0000000087, 0000000001, 4),
(0000000055, 0000000088, 0000000001, 6),
(0000000056, 0000000089, 0000000001, 4),
(0000000057, 0000000090, 0000000001, 4),
(0000000058, 0000000091, 0000000001, 5),
(0000000059, 0000000092, 0000000001, 4),
(0000000060, 0000000093, 0000000001, 4),
(0000000061, 0000000094, 0000000001, 3),
(0000000062, 0000000095, 0000000001, 4),
(0000000063, 0000000096, 0000000002, 1),
(0000000064, 0000000097, 0000000001, 1),
(0000000065, 0000000098, 0000000001, 1),
(0000000066, 0000000099, 0000000001, 3),
(0000000067, 0000000100, 0000000001, 2),
(0000000068, 0000000101, 0000000001, 2),
(0000000069, 0000000102, 0000000001, 2),
(0000000070, 0000000103, 0000000001, 8),
(0000000071, 0000000104, 0000000001, 8),
(0000000072, 0000000105, 0000000001, 7),
(0000000073, 0000000106, 0000000001, 5),
(0000000074, 0000000107, 0000000001, 3),
(0000000076, 0000000110, 0000000007, 51),
(0000000077, 0000000111, 0000000001, 1),
(0000000078, 0000000114, 0000000001, 10),
(0000000079, 0000000118, 0000000001, 10),
(0000000080, 0000000119, 0000000001, 20),
(0000000081, 0000000120, 0000000001, 10),
(0000000082, 0000000121, 0000000001, 20),
(0000000083, 0000000122, 0000000001, 5),
(0000000084, 0000000124, 0000000001, 10),
(0000000085, 0000000127, 0000000001, 10),
(0000000086, 0000000129, 0000000001, 10),
(0000000087, 0000000131, 0000000001, 10),
(0000000088, 0000000133, 0000000001, 10),
(0000000089, 0000000133, 0000000001, 10),
(0000000090, 0000000135, 0000000001, 10),
(0000000091, 0000000135, 0000000001, 10),
(0000000092, 0000000137, 0000000001, 10),
(0000000093, 0000000138, 0000000001, 10),
(0000000094, 0000000139, 0000000001, 10),
(0000000095, 0000000140, 0000000001, 10),
(0000000097, 0000000142, 0000000001, 10),
(0000000098, 0000000143, 0000000001, 10),
(0000000101, 0000000146, 0000000007, 51),
(0000000102, 0000000147, 0000000003, 5),
(0000000103, 0000000147, 0000000003, 5),
(0000000104, 0000000147, 0000000868, 5),
(0000000106, 0000000149, 0000000001, 2),
(0000000107, 0000000149, 0000000007, 102),
(0000000108, 0000000150, 0000000001, 5),
(0000000109, 0000000151, 0000000001, 1),
(0000000110, 0000000151, 0000000003, 2),
(0000000111, 0000000151, 0000000868, 1),
(0000000112, 0000000152, 0000000001, 1),
(0000000113, 0000000152, 0000000003, 2),
(0000000114, 0000000152, 0000000868, 1),
(0000000115, 0000000153, 0000000001, 3),
(0000000116, 0000000154, 0000000001, 3),
(0000000117, 0000000155, 0000000001, 1),
(0000000118, 0000000156, 0000000001, 1),
(0000000119, 0000000157, 0000000001, 2),
(0000000120, 0000000158, 0000000001, 1),
(0000000121, 0000000111, 0000000001, 1),
(0000000122, 0000000105, 0000000001, 7),
(0000000123, 0000000161, 0000000001, 2),
(0000000124, 0000000162, 0000000001, 1),
(0000000125, 0000000163, 0000000001, 1),
(0000000126, 0000000164, 0000000001, 1),
(0000000127, 0000000165, 0000000001, 1),
(0000000128, 0000000166, 0000000001, 1),
(0000000129, 0000000167, 0000000001, 1),
(0000000130, 0000000168, 0000000001, 2),
(0000000131, 0000000169, 0000000001, 2),
(0000000132, 0000000170, 0000000003, 1),
(0000000133, 0000000171, 0000000001, 1),
(0000000134, 0000000172, 0000000001, 1),
(0000000135, 0000000173, 0000000001, 1),
(0000000136, 0000000174, 0000000001, 11),
(0000000137, 0000000174, 0000000003, 1),
(0000000138, 0000000175, 0000000001, 11),
(0000000139, 0000000175, 0000000003, 1),
(0000000140, 0000000176, 0000000001, 11),
(0000000141, 0000000176, 0000000003, 1),
(0000000142, 0000000177, 0000000001, 11),
(0000000143, 0000000177, 0000000003, 1),
(0000000144, 0000000178, 0000000001, 55),
(0000000145, 0000000179, 0000000001, 10),
(0000000146, 0000000180, 0000000001, 10),
(0000000147, 0000000181, 0000000001, 5),
(0000000148, 0000000182, 0000000001, 5),
(0000000149, 0000000183, 0000000001, 5),
(0000000150, 0000000184, 0000000001, 5),
(0000000151, 0000000185, 0000000001, 4),
(0000000152, 0000000186, 0000000001, 1),
(0000000153, 0000000187, 0000000001, 1),
(0000000154, 0000000188, 0000000001, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallemovimientocaja`
--

CREATE TABLE IF NOT EXISTS `detallemovimientocaja` (
  `idDetalleMovimientoCaja` int(10) unsigned zerofill NOT NULL,
  `idMovimientoCaja` int(10) unsigned zerofill NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `descripcion` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `detallemovimientocaja`
--

INSERT INTO `detallemovimientocaja` (`idDetalleMovimientoCaja`, `idMovimientoCaja`, `monto`, `descripcion`) VALUES
(0000000003, 0000000004, 255.00, 'Pago por compra'),
(0000000004, 0000000005, 25.00, 'Pago por compra'),
(0000000005, 0000000005, 25.00, 'Pago por compra'),
(0000000006, 0000000005, 25.00, 'Pago por compra'),
(0000000008, 0000000007, 4.00, 'Pago por compra'),
(0000000009, 0000000007, 510.00, 'Pago por compra'),
(0000000010, 0000000010, 10.00, 'TEST'),
(0000000011, 0000000010, 10.00, 'TEST'),
(0000000012, 0000000010, 10.00, 'TEST'),
(0000000013, 0000000011, 10.00, 'TEST'),
(0000000014, 0000000011, 10.00, 'TEST'),
(0000000015, 0000000011, 10.00, 'TEST'),
(0000000016, 0000000012, 10.00, 'TEST'),
(0000000017, 0000000012, 10.00, 'TEST'),
(0000000018, 0000000012, 10.00, 'TEST'),
(0000000019, 0000000013, 10.00, 'TEST'),
(0000000020, 0000000013, 10.00, 'TEST'),
(0000000021, 0000000013, 10.00, 'TEST'),
(0000000022, 0000000014, 10.00, 'TEST'),
(0000000023, 0000000014, 10.00, 'TEST'),
(0000000024, 0000000014, 10.00, 'TEST'),
(0000000025, 0000000015, 10.00, 'TEST'),
(0000000026, 0000000015, 10.00, 'TEST'),
(0000000027, 0000000015, 10.00, 'TEST'),
(0000000035, 0000000021, 14.00, 'TEST'),
(0000000036, 0000000021, 14.00, 'TEST'),
(0000000037, 0000000021, 14.00, 'TEST'),
(0000000038, 0000000021, 14.00, 'TEST'),
(0000000039, 0000000022, 14.00, 'TEST'),
(0000000040, 0000000022, 14.00, 'TEST'),
(0000000041, 0000000022, 14.00, 'TEST'),
(0000000042, 0000000022, 14.00, 'TEST'),
(0000000043, 0000000023, 1.00, 'TEST'),
(0000000044, 0000000023, 2.00, 'TEST'),
(0000000045, 0000000023, 3.00, 'TEST'),
(0000000046, 0000000023, 4.00, 'TEST');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleordencompra`
--

CREATE TABLE IF NOT EXISTS `detalleordencompra` (
  `idDetalleOrdenCompra` int(10) unsigned zerofill NOT NULL,
  `idOrdenCompra` int(10) unsigned zerofill NOT NULL,
  `idArticulo` int(10) unsigned zerofill NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `total` float(10,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `detalleordencompra`
--

INSERT INTO `detalleordencompra` (`idDetalleOrdenCompra`, `idOrdenCompra`, `idArticulo`, `cantidad`, `precio`, `total`) VALUES
(0000000001, 0000000001, 0000000003, 11, 1.00, 0.00),
(0000000002, 0000000001, 0000000001, 1, 10.00, 0.00),
(0000000003, 0000000002, 0000000001, 1, 0.00, 0.00),
(0000000004, 0000000003, 0000000001, 1, 0.00, 0.00),
(0000000005, 0000000004, 0000000001, 1, 2.00, 0.00),
(0000000006, 0000000004, 0000001031, 1, 8.00, 0.00),
(0000000007, 0000000005, 0000000001, 51, 2.00, 0.00),
(0000000008, 0000000006, 0000000001, 1, 8.00, 0.00),
(0000000009, 0000000008, 0000000001, 4, 5.00, 0.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleventa`
--

CREATE TABLE IF NOT EXISTS `detalleventa` (
  `idDetalleVenta` int(10) unsigned NOT NULL,
  `idVenta` int(10) unsigned zerofill NOT NULL,
  `idAlmacenArticulo` int(10) unsigned zerofill NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `descuento` decimal(10,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `detalleventa`
--

INSERT INTO `detalleventa` (`idDetalleVenta`, `idVenta`, `idAlmacenArticulo`, `cantidad`, `precio`, `descuento`) VALUES
(1, 0000000001, 0000000001, 3, 5.20, 0.00),
(2, 0000000002, 0000000001, 1, 5.20, 0.00),
(3, 0000000003, 0000000001, 1, 5.20, 0.00),
(4, 0000000004, 0000000001, 1, 5.20, 0.00),
(5, 0000000005, 0000000001, 2, 5.20, 0.00),
(6, 0000000006, 0000000001, 1, 5.20, 0.00),
(7, 0000000007, 0000000001, 1, 5.20, 0.00),
(8, 0000000008, 0000000001, 1, 5.20, 0.00),
(9, 0000000009, 0000000001, 1, 5.20, 0.00),
(10, 0000000010, 0000000001, 1, 5.20, 0.00),
(11, 0000000011, 0000000001, 1, 5.20, 0.00),
(12, 0000000012, 0000000001, 2, 5.20, 0.00),
(13, 0000000013, 0000000001, 2, 5.20, 0.00),
(14, 0000000014, 0000000002, 1, 2.40, 0.00),
(15, 0000000015, 0000000001, 1, 5.20, 0.00),
(16, 0000000016, 0000000001, 1, 5.20, 0.00),
(17, 0000000017, 0000000001, 1, 5.20, 0.00),
(18, 0000000018, 0000000001, 11, 5.20, 1.00),
(19, 0000000018, 0000000002, 1, 2.40, 0.00),
(20, 0000000019, 0000000001, 11, 5.20, 1.00),
(21, 0000000019, 0000000002, 1, 2.40, 0.00),
(22, 0000000020, 0000000001, 11, 5.20, 1.00),
(23, 0000000020, 0000000002, 1, 2.40, 0.00),
(24, 0000000021, 0000000001, 11, 5.20, 1.00),
(25, 0000000021, 0000000002, 1, 2.40, 0.00),
(26, 0000000022, 0000000001, 55, 5.20, 0.00),
(27, 0000000023, 0000000001, 10, 5.20, 0.00),
(28, 0000000024, 0000000001, 10, 5.20, 0.00),
(29, 0000000025, 0000000001, 5, 5.20, 0.00),
(30, 0000000026, 0000000001, 5, 5.20, 0.20),
(31, 0000000027, 0000000001, 5, 5.20, 0.00),
(32, 0000000028, 0000000001, 5, 5.20, 0.00),
(33, 0000000029, 0000000001, 4, 5.20, 0.00),
(34, 0000000030, 0000000001, 1, 5.20, 2.00),
(35, 0000000031, 0000000001, 1, 5.20, 0.00),
(36, 0000000032, 0000000001, 1, 5.20, 0.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `flujocaja`
--

CREATE TABLE IF NOT EXISTS `flujocaja` (
  `idFlujoCaja` int(10) unsigned zerofill NOT NULL,
  `idDetalleMovimientoCaja` int(10) unsigned zerofill NOT NULL,
  `idCaja` int(10) unsigned zerofill NOT NULL,
  `movimiento` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `monto` float NOT NULL,
  `montoBefore` float NOT NULL,
  `montoAfter` float NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `flujocaja`
--

INSERT INTO `flujocaja` (`idFlujoCaja`, `idDetalleMovimientoCaja`, `idCaja`, `movimiento`, `monto`, `montoBefore`, `montoAfter`, `fecha`) VALUES
(0000000001, 0000000035, 0000000001, 'E', 14, 40000, 39986, '2015-05-05'),
(0000000002, 0000000035, 0000000001, 'I', 14, -14, 0, '2015-05-05'),
(0000000003, 0000000036, 0000000001, 'E', 14, 14, 0, '2015-05-05'),
(0000000004, 0000000036, 0000000001, 'I', 14, -14, 0, '2015-05-05'),
(0000000005, 0000000037, 0000000001, 'E', 14, 14, 0, '2015-05-05'),
(0000000006, 0000000037, 0000000001, 'I', 14, -14, 0, '2015-05-05'),
(0000000007, 0000000038, 0000000001, 'E', 14, 14, 0, '2015-05-05'),
(0000000008, 0000000038, 0000000001, 'I', 14, -14, 0, '2015-05-05'),
(0000000009, 0000000039, 0000000001, 'E', 14, 14, 0, '2015-05-05'),
(0000000010, 0000000039, 0000000001, 'I', 14, 0, 14, '2015-05-05'),
(0000000011, 0000000040, 0000000001, 'E', 14, 14, 0, '2015-05-05'),
(0000000012, 0000000040, 0000000001, 'I', 14, 0, 14, '2015-05-05'),
(0000000013, 0000000041, 0000000001, 'E', 14, 14, 0, '2015-05-05'),
(0000000014, 0000000041, 0000000001, 'I', 14, 0, 14, '2015-05-05'),
(0000000015, 0000000042, 0000000001, 'E', 14, 14, 0, '2015-05-05'),
(0000000016, 0000000042, 0000000001, 'I', 14, 0, 14, '2015-05-05'),
(0000000017, 0000000043, 0000000001, 'E', 1, 14, 13, '2015-05-05'),
(0000000018, 0000000043, 0000000001, 'I', 1, 13, 14, '2015-05-05'),
(0000000019, 0000000044, 0000000001, 'E', 2, 14, 12, '2015-05-05'),
(0000000020, 0000000044, 0000000001, 'I', 2, 12, 14, '2015-05-05'),
(0000000021, 0000000045, 0000000001, 'E', 3, 14, 11, '2015-05-05'),
(0000000022, 0000000045, 0000000001, 'I', 3, 11, 14, '2015-05-05'),
(0000000023, 0000000046, 0000000001, 'E', 4, 14, 10, '2015-05-05'),
(0000000024, 0000000046, 0000000001, 'I', 4, 10, 14, '2015-05-05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kardex`
--

CREATE TABLE IF NOT EXISTS `kardex` (
  `idKardex` int(10) unsigned zerofill NOT NULL,
  `idAlmacenArticulo` int(10) unsigned zerofill NOT NULL,
  `movimiento` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `stockBefore` int(11) NOT NULL,
  `stockAfter` int(11) NOT NULL,
  `idDetalleMovimiento` int(10) unsigned zerofill NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `kardex`
--

INSERT INTO `kardex` (`idKardex`, `idAlmacenArticulo`, `movimiento`, `cantidad`, `fecha`, `stockBefore`, `stockAfter`, `idDetalleMovimiento`) VALUES
(0000000003, 0000000010, 'E', 51, '2015-04-30', -51, -102, 0000000101),
(0000000004, 0000000002, 'I', 5, '2015-05-01', 13, 18, 0000000102),
(0000000005, 0000000002, 'I', 5, '2015-05-01', 18, 23, 0000000103),
(0000000006, 0000000007, 'I', 5, '2015-05-01', 0, 5, 0000000104),
(0000000008, 0000000001, 'I', 2, '2015-05-01', 594, 596, 0000000106),
(0000000009, 0000000010, 'I', 102, '2015-05-01', -102, 0, 0000000107),
(0000000010, 0000000004, 'E', 5, '2015-04-27', 18, 13, 0000000108),
(0000000011, 0000000001, 'I', 5, '2015-04-27', 596, 601, 0000000108),
(0000000012, 0000000001, 'I', 1, '2015-05-04', 601, 602, 0000000109),
(0000000013, 0000000002, 'I', 2, '2015-05-04', 23, 25, 0000000110),
(0000000014, 0000000007, 'I', 1, '2015-05-04', 5, 6, 0000000111),
(0000000015, 0000000001, 'I', 1, '2015-05-04', 602, 603, 0000000112),
(0000000016, 0000000002, 'I', 2, '2015-05-04', 25, 27, 0000000113),
(0000000017, 0000000007, 'I', 1, '2015-05-04', 6, 7, 0000000114),
(0000000018, 0000000001, 'I', 3, '2015-05-04', 603, 606, 0000000115),
(0000000019, 0000000001, 'I', 3, '2015-05-04', 606, 609, 0000000116),
(0000000020, 0000000001, 'I', 1, '2015-05-05', 609, 610, 0000000117),
(0000000021, 0000000001, 'I', 1, '2015-05-05', 610, 611, 0000000118),
(0000000022, 0000000001, 'E', 2, '2015-03-17', 611, 609, 0000000119),
(0000000023, 0000000001, 'I', 1, '2015-05-05', 609, 610, 0000000120),
(0000000024, 0000000001, 'I', 1, '2015-04-09', 610, 611, 0000000121),
(0000000025, 0000000001, 'I', 7, '2015-04-09', 611, 618, 0000000122),
(0000000026, 0000000001, 'I', 2, '2015-05-05', 618, 620, 0000000123),
(0000000027, 0000000001, 'I', 1, '2015-05-05', 620, 621, 0000000124),
(0000000028, 0000000001, 'I', 1, '2015-05-05', 621, 622, 0000000125),
(0000000029, 0000000001, 'I', 1, '2015-05-05', 622, 623, 0000000126),
(0000000030, 0000000001, 'I', 1, '2015-05-05', 623, 624, 0000000127),
(0000000031, 0000000001, 'I', 1, '2015-05-05', 624, 625, 0000000128),
(0000000032, 0000000001, 'I', 1, '2015-05-05', 625, 626, 0000000129),
(0000000033, 0000000001, 'I', 2, '2015-05-05', 626, 628, 0000000130),
(0000000034, 0000000001, 'I', 2, '2015-05-05', 628, 630, 0000000131),
(0000000035, 0000000002, 'I', 1, '2015-05-05', 27, 28, 0000000132),
(0000000036, 0000000001, 'I', 1, '2015-05-05', 630, 631, 0000000133),
(0000000037, 0000000001, 'I', 1, '2015-05-05', 631, 632, 0000000134),
(0000000038, 0000000001, 'I', 1, '2015-05-06', 632, 633, 0000000135),
(0000000039, 0000000001, 'I', 11, '2015-05-06', 633, 644, 0000000136),
(0000000040, 0000000002, 'I', 1, '2015-05-06', 28, 29, 0000000137),
(0000000041, 0000000001, 'I', 11, '2015-05-06', 644, 655, 0000000138),
(0000000042, 0000000002, 'I', 1, '2015-05-06', 29, 30, 0000000139),
(0000000043, 0000000001, 'I', 11, '2015-05-06', 655, 666, 0000000140),
(0000000044, 0000000002, 'I', 1, '2015-05-06', 30, 31, 0000000141),
(0000000045, 0000000001, 'E', 11, '2015-05-06', 666, 655, 0000000142),
(0000000046, 0000000002, 'E', 1, '2015-05-06', 31, 30, 0000000143),
(0000000047, 0000000001, 'E', 55, '2015-05-06', 655, 600, 0000000144),
(0000000048, 0000000001, 'E', 10, '2015-05-06', 600, 590, 0000000145),
(0000000049, 0000000001, 'E', 10, '2015-05-07', 590, 580, 0000000146),
(0000000050, 0000000001, 'E', 5, '2015-05-07', 580, 575, 0000000147),
(0000000051, 0000000001, 'E', 5, '2015-05-07', 575, 570, 0000000148),
(0000000052, 0000000001, 'E', 5, '2015-05-07', 570, 565, 0000000149),
(0000000053, 0000000001, 'E', 5, '2015-05-07', 565, 560, 0000000150),
(0000000054, 0000000001, 'E', 4, '2015-05-07', 560, 556, 0000000151),
(0000000055, 0000000001, 'E', 1, '2015-05-12', 556, 555, 0000000152),
(0000000056, 0000000001, 'E', 1, '2015-05-12', 555, 554, 0000000153),
(0000000057, 0000000001, 'E', 1, '2015-05-12', 554, 553, 0000000154);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lote`
--

CREATE TABLE IF NOT EXISTS `lote` (
  `idLote` int(10) unsigned zerofill NOT NULL,
  `idArticulo` int(10) unsigned zerofill NOT NULL,
  `fechaVencimiento` date DEFAULT NULL,
  `lote` date DEFAULT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimiento`
--

CREATE TABLE IF NOT EXISTS `movimiento` (
  `idMovimiento` int(10) unsigned zerofill NOT NULL,
  `idTipoMovimiento` int(10) unsigned zerofill NOT NULL,
  `fecha` date NOT NULL,
  `idAlmacenOrigen` int(10) unsigned zerofill DEFAULT NULL,
  `idAlmacenDestino` int(10) unsigned zerofill DEFAULT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `movimiento`
--

INSERT INTO `movimiento` (`idMovimiento`, `idTipoMovimiento`, `fecha`, `idAlmacenOrigen`, `idAlmacenDestino`, `estado`) VALUES
(0000000002, 0000000002, '2015-03-11', 0000000001, 0000000002, 'G'),
(0000000003, 0000000009, '2015-03-11', 0000000001, NULL, 'G'),
(0000000004, 0000000002, '2015-03-11', 0000000001, 0000000002, 'G'),
(0000000008, 0000000002, '2015-03-11', NULL, 0000000002, 'G'),
(0000000009, 0000000002, '2015-03-11', NULL, 0000000002, 'G'),
(0000000015, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000016, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000017, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000018, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000019, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000020, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000021, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000022, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000023, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000024, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000025, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000026, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000027, 0000000009, '2015-03-11', NULL, 0000000001, 'G'),
(0000000028, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000029, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000030, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000031, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000032, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000033, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000034, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000035, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000036, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000037, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000038, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000039, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000040, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000041, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000042, 0000000001, '2015-03-13', 0000000001, 0000000002, 'G'),
(0000000043, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000044, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000045, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000046, 0000000009, '2015-03-13', NULL, 0000000001, 'G'),
(0000000047, 0000000009, '2015-03-14', NULL, 0000000001, 'G'),
(0000000048, 0000000009, '2015-03-14', NULL, 0000000001, 'G'),
(0000000049, 0000000009, '2015-03-14', NULL, 0000000001, 'G'),
(0000000050, 0000000009, '2015-03-14', NULL, 0000000001, 'G'),
(0000000051, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000052, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000053, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000054, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000055, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000056, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000057, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000058, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000059, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000060, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000061, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000062, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000063, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000064, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000065, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000066, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000067, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000068, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000069, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000070, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000071, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000072, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000073, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000074, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000075, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000076, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000077, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000078, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000079, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000080, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000081, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000082, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000083, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000084, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000085, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000086, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000087, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000088, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000089, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000090, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000091, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000092, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000093, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000094, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000095, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000096, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000097, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000098, 0000000009, '2015-03-16', NULL, 0000000001, 'G'),
(0000000099, 0000000009, '2015-03-17', NULL, 0000000001, 'G'),
(0000000100, 0000000009, '2015-03-17', NULL, 0000000001, 'A'),
(0000000101, 0000000009, '2015-03-17', NULL, 0000000001, 'G'),
(0000000102, 0000000009, '2015-03-17', NULL, 0000000001, 'G'),
(0000000103, 0000000009, '2015-04-09', NULL, 0000000001, 'G'),
(0000000104, 0000000001, '2015-04-09', 0000000001, NULL, 'G'),
(0000000105, 0000000001, '2015-04-09', 0000000001, NULL, 'A'),
(0000000106, 0000000009, '2015-04-09', NULL, 0000000001, 'G'),
(0000000107, 0000000009, '2015-04-09', NULL, 0000000001, 'G'),
(0000000109, 0000000001, '2015-04-09', 0000000001, NULL, 'G'),
(0000000110, 0000000001, '2015-04-09', 0000000001, NULL, 'G'),
(0000000111, 0000000001, '2015-04-09', 0000000001, NULL, 'A'),
(0000000114, 0000000002, '2015-04-27', NULL, 0000000001, 'A'),
(0000000118, 0000000002, '2015-04-27', 0000000001, 0000000001, 'A'),
(0000000119, 0000000002, '2015-04-27', NULL, 0000000001, 'A'),
(0000000120, 0000000002, '2015-04-27', 0000000001, 0000000001, 'G'),
(0000000121, 0000000002, '2015-04-27', 0000000001, 0000000001, 'G'),
(0000000122, 0000000001, '2015-04-27', 0000000001, 0000000002, 'A'),
(0000000124, 0000000002, '2015-04-27', NULL, 0000000001, 'G'),
(0000000126, 0000000010, '2015-04-27', 0000000001, NULL, 'G'),
(0000000127, 0000000002, '2015-04-27', NULL, 0000000001, 'A'),
(0000000128, 0000000010, '2015-04-27', 0000000001, NULL, 'G'),
(0000000129, 0000000002, '2015-04-27', NULL, 0000000001, 'A'),
(0000000130, 0000000010, '2015-04-27', 0000000001, NULL, 'G'),
(0000000131, 0000000002, '2015-04-27', NULL, 0000000001, 'A'),
(0000000132, 0000000010, '2015-04-27', 0000000001, NULL, 'G'),
(0000000133, 0000000002, '2015-04-27', NULL, 0000000001, 'A'),
(0000000134, 0000000010, '2015-04-27', 0000000001, NULL, 'G'),
(0000000135, 0000000002, '2015-04-27', NULL, 0000000002, 'A'),
(0000000136, 0000000010, '2015-04-27', 0000000002, NULL, 'G'),
(0000000137, 0000000002, '2015-04-27', NULL, 0000000002, 'A'),
(0000000138, 0000000010, '2015-04-27', 0000000002, NULL, 'G'),
(0000000139, 0000000002, '2015-04-27', NULL, 0000000001, 'A'),
(0000000140, 0000000010, '2015-04-27', 0000000001, NULL, 'G'),
(0000000142, 0000000002, '2015-04-27', NULL, 0000000002, 'A'),
(0000000143, 0000000010, '2015-04-27', 0000000002, NULL, 'G'),
(0000000146, 0000000005, '2015-04-30', 0000000001, NULL, 'G'),
(0000000147, 0000000005, '2015-05-01', NULL, 0000000001, 'G'),
(0000000149, 0000000005, '2015-05-01', NULL, 0000000001, 'G'),
(0000000150, 0000000010, '2015-04-27', 0000000002, 0000000001, 'G'),
(0000000151, 0000000009, '2015-05-04', NULL, 0000000001, 'G'),
(0000000152, 0000000009, '2015-05-04', NULL, 0000000001, 'G'),
(0000000153, 0000000009, '2015-05-04', NULL, 0000000001, 'G'),
(0000000154, 0000000009, '2015-05-04', NULL, 0000000001, 'G'),
(0000000155, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000156, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000157, 0000000010, '2015-03-17', 0000000001, NULL, 'G'),
(0000000158, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000159, 0000000010, '2015-04-09', NULL, 0000000001, 'G'),
(0000000160, 0000000010, '2015-04-09', NULL, 0000000001, 'G'),
(0000000161, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000162, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000163, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000164, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000165, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000166, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000167, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000168, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000169, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000170, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000171, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000172, 0000000009, '2015-05-05', NULL, 0000000001, 'G'),
(0000000173, 0000000009, '2015-05-06', NULL, 0000000001, 'G'),
(0000000174, 0000000009, '2015-05-06', NULL, 0000000001, 'G'),
(0000000175, 0000000009, '2015-05-06', NULL, 0000000001, 'G'),
(0000000176, 0000000009, '2015-05-06', NULL, 0000000001, 'G'),
(0000000177, 0000000009, '2015-05-06', 0000000001, NULL, 'G'),
(0000000178, 0000000009, '2015-05-06', 0000000001, NULL, 'G'),
(0000000179, 0000000009, '2015-05-06', 0000000001, NULL, 'G'),
(0000000180, 0000000009, '2015-05-07', 0000000001, NULL, 'G'),
(0000000181, 0000000009, '2015-05-07', 0000000001, NULL, 'G'),
(0000000182, 0000000009, '2015-05-07', 0000000001, NULL, 'A'),
(0000000183, 0000000009, '2015-05-07', 0000000001, NULL, 'A'),
(0000000184, 0000000009, '2015-05-07', 0000000001, NULL, 'A'),
(0000000185, 0000000009, '2015-05-07', 0000000001, NULL, 'A'),
(0000000186, 0000000009, '2015-05-12', 0000000001, NULL, 'G'),
(0000000187, 0000000009, '2015-05-12', 0000000001, NULL, 'G'),
(0000000188, 0000000009, '2015-05-12', 0000000001, NULL, 'G');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientocaja`
--

CREATE TABLE IF NOT EXISTS `movimientocaja` (
  `idMovimientoCaja` int(10) unsigned zerofill NOT NULL,
  `idTipoMovimientoCaja` int(10) unsigned zerofill NOT NULL,
  `referencia` int(10) unsigned zerofill NOT NULL,
  `fecha` date NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `idUsuario` int(10) unsigned zerofill NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `idCajaOrigen` int(10) unsigned zerofill NOT NULL,
  `idCajaDestino` int(10) unsigned zerofill NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `movimientocaja`
--

INSERT INTO `movimientocaja` (`idMovimientoCaja`, `idTipoMovimientoCaja`, `referencia`, `fecha`, `monto`, `idUsuario`, `estado`, `idCajaOrigen`, `idCajaDestino`) VALUES
(0000000004, 0000000003, 0000000002, '2015-04-30', 255.00, 0000000001, 'G', 0000000001, 0000000001),
(0000000005, 0000000003, 0000000002, '2015-05-01', 75.00, 0000000001, 'G', 0000000001, 0000000001),
(0000000007, 0000000003, 0000000002, '2015-05-01', 514.00, 0000000001, 'G', 0000000001, 0000000001),
(0000000010, 0000000001, 0000000001, '2015-05-04', 1.00, 0000000001, 'G', 0000000001, 0000000001),
(0000000011, 0000000001, 0000000001, '2015-05-04', 1.00, 0000000001, 'G', 0000000001, 0000000001),
(0000000012, 0000000001, 0000000001, '2015-05-04', 1.00, 0000000001, 'A', 0000000001, 0000000001),
(0000000013, 0000000001, 0000000001, '2015-05-04', 1.00, 0000000001, 'A', 0000000001, 0000000001),
(0000000014, 0000000004, 0000000001, '2015-05-04', 1.00, 0000000001, 'G', 0000000001, 0000000001),
(0000000015, 0000000006, 0000000001, '2015-05-04', 1.00, 0000000001, 'G', 0000000001, 0000000001),
(0000000021, 0000000001, 0000000001, '2015-05-05', 40.00, 0000000001, 'E', 0000000001, 0000000001),
(0000000022, 0000000001, 0000000001, '2015-05-05', 40.00, 0000000001, 'E', 0000000001, 0000000001),
(0000000023, 0000000001, 0000000001, '2015-05-05', 40.00, 0000000001, 'E', 0000000001, 0000000001),
(0000000024, 0000000002, 0000000002, '2015-05-06', 9.36, 0000000001, 'G', 0000000001, 0000000001),
(0000000025, 0000000002, 0000000002, '2015-05-07', 26.00, 0000000001, 'G', 0000000001, 0000000001),
(0000000026, 0000000002, 0000000002, '2015-05-07', 25.80, 0000000001, 'G', 0000000001, 0000000001),
(0000000027, 0000000002, 0000000002, '2015-05-07', 20.80, 0000000001, 'G', 0000000001, 0000000001),
(0000000028, 0000000002, 0000000002, '2015-05-12', 5.20, 0000000001, 'G', 0000000001, 0000000001),
(0000000029, 0000000002, 0000000002, '2015-05-12', 5.20, 0000000001, 'G', 0000000001, 0000000001),
(0000000030, 0000000002, 0000000002, '2015-05-13', 5.20, 0000000001, 'G', 0000000001, 0000000001),
(0000000031, 0000000002, 0000000002, '2015-05-13', 5.20, 0000000001, 'G', 0000000001, 0000000001),
(0000000032, 0000000002, 0000000002, '2015-05-13', 5.20, 0000000001, 'G', 0000000001, 0000000001),
(0000000033, 0000000002, 0000000002, '2015-05-13', 5.20, 0000000001, 'G', 0000000001, 0000000001);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordencompra`
--

CREATE TABLE IF NOT EXISTS `ordencompra` (
  `idOrdenCompra` int(10) unsigned zerofill NOT NULL,
  `fecha` date NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `fechaEntrega` datetime DEFAULT NULL,
  `idProveedor` int(10) unsigned zerofill NOT NULL,
  `idUsuario` int(10) unsigned zerofill NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ordencompra`
--

INSERT INTO `ordencompra` (`idOrdenCompra`, `fecha`, `monto`, `fechaEntrega`, `idProveedor`, `idUsuario`, `estado`) VALUES
(0000000001, '2015-03-26', 21.00, '2015-03-26 00:00:00', 0000000001, 0000000001, 'P'),
(0000000002, '2015-03-26', 0.00, '2015-03-26 00:00:00', 0000000001, 0000000001, 'P'),
(0000000003, '2015-03-26', 0.00, '2015-03-26 00:00:00', 0000000001, 0000000001, 'P'),
(0000000004, '2015-04-09', 10.00, '2015-04-09 00:00:00', 0000000001, 0000000001, 'P'),
(0000000005, '2015-04-09', 102.00, '2015-04-09 00:00:00', 0000000001, 0000000001, 'P'),
(0000000006, '2015-04-09', 8.00, '2015-04-09 00:00:00', 0000000001, 0000000001, 'P'),
(0000000007, '2015-04-29', 0.00, '2015-04-29 00:00:00', 0000000001, 0000000001, 'A'),
(0000000008, '2015-04-29', 20.00, '2015-04-29 00:00:00', 0000000001, 0000000001, 'G');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE IF NOT EXISTS `pago` (
  `idPago` int(10) unsigned zerofill NOT NULL,
  `idTipoPago` int(10) unsigned zerofill NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `idComprobante` int(10) unsigned zerofill NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE IF NOT EXISTS `proveedor` (
  `idProveedor` int(10) unsigned zerofill NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `preventa` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mercaderista` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idProveedor`, `nombre`, `preventa`, `mercaderista`, `direccion`, `telefono`, `descripcion`) VALUES
(0000000001, 'Backus', 'preventa', 'mercaderista', 'direccion', 'telefono', 'descripcion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serie`
--

CREATE TABLE IF NOT EXISTS `serie` (
  `idSerie` int(10) unsigned zerofill NOT NULL,
  `idTipoComprobante` int(10) unsigned zerofill NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `serie`
--

INSERT INTO `serie` (`idSerie`, `idTipoComprobante`, `nombre`) VALUES
(0000000001, 0000000001, '001'),
(0000000002, 0000000002, '002');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocomprobante`
--

CREATE TABLE IF NOT EXISTS `tipocomprobante` (
  `idTipoComprobante` int(10) unsigned zerofill NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipocomprobante`
--

INSERT INTO `tipocomprobante` (`idTipoComprobante`, `nombre`) VALUES
(0000000001, 'Factura'),
(0000000002, 'Boleta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipomovimiento`
--

CREATE TABLE IF NOT EXISTS `tipomovimiento` (
  `idTipoMovimiento` int(10) unsigned zerofill NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipomovimiento`
--

INSERT INTO `tipomovimiento` (`idTipoMovimiento`, `tipo`, `nombre`) VALUES
(0000000001, 'T', 'Transferencia'),
(0000000002, 'I', 'Cargo por Ajuste'),
(0000000003, 'I', 'Cargo por Bonificación'),
(0000000004, 'E', 'Descargo por avería'),
(0000000005, 'I', 'Cargo por Compra'),
(0000000006, 'E', 'Descargo por Robo'),
(0000000007, 'E', 'Descargo por Pérdida'),
(0000000008, 'E', 'Descargo por Vencimiento'),
(0000000009, 'E', 'Descargo por venta'),
(0000000010, 'A', 'Anulacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipomovimientocaja`
--

CREATE TABLE IF NOT EXISTS `tipomovimientocaja` (
  `idTipoMovimientoCaja` int(10) unsigned zerofill NOT NULL,
  `descripcion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipomovimientocaja`
--

INSERT INTO `tipomovimientocaja` (`idTipoMovimientoCaja`, `descripcion`, `tipo`) VALUES
(0000000001, 'Egreso pago de servicios', 'E'),
(0000000002, 'Ingreso por Venta', 'I'),
(0000000003, 'Egreso por compra', 'E'),
(0000000004, 'Ingreso Anulacion', 'I'),
(0000000005, 'Egreso Anulacion', 'E'),
(0000000006, 'Anulacion', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipopago`
--

CREATE TABLE IF NOT EXISTS `tipopago` (
  `idTipoPago` int(10) unsigned zerofill NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipousuario`
--

CREATE TABLE IF NOT EXISTS `tipousuario` (
  `idtipoUsuario` int(10) unsigned zerofill NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `p_movimiento` int(11) NOT NULL,
  `p_venta` int(11) NOT NULL,
  `p_comprobante` int(11) NOT NULL,
  `p_ordenCompra` int(11) NOT NULL,
  `p_compra` int(11) NOT NULL,
  `p_detalleMovimientoCaja` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipousuario`
--

INSERT INTO `tipousuario` (`idtipoUsuario`, `nombre`, `p_movimiento`, `p_venta`, `p_comprobante`, `p_ordenCompra`, `p_compra`, `p_detalleMovimientoCaja`) VALUES
(0000000001, 'Administrador', 15, 15, 15, 15, 15, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `idusuario` int(10) unsigned zerofill NOT NULL,
  `idTipoUsuario` int(10) unsigned zerofill NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `idTipoUsuario`, `nombre`, `usuario`, `password`) VALUES
(0000000001, 0000000001, 'Edder Sanchez', 'esanchez', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE IF NOT EXISTS `venta` (
  `idVenta` int(10) unsigned zerofill NOT NULL,
  `fecha` date NOT NULL,
  `idCliente` int(10) unsigned zerofill DEFAULT NULL,
  `monto` decimal(10,2) NOT NULL,
  `idComprobante` int(10) unsigned zerofill DEFAULT NULL,
  `idUsuario` int(10) unsigned zerofill NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `idMovimiento` int(10) unsigned zerofill NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`idVenta`, `fecha`, `idCliente`, `monto`, `idComprobante`, `idUsuario`, `estado`, `idMovimiento`) VALUES
(0000000001, '2015-05-04', 0000000001, 15.60, NULL, 0000000001, 'G', 0000000154),
(0000000002, '2015-05-05', NULL, 5.20, 0000000010, 0000000001, 'G', 0000000155),
(0000000003, '2015-05-05', NULL, 5.20, NULL, 0000000001, 'G', 0000000156),
(0000000004, '2015-05-05', NULL, 5.20, NULL, 0000000001, 'G', 0000000158),
(0000000005, '2015-05-05', NULL, 10.40, NULL, 0000000001, 'G', 0000000161),
(0000000006, '2015-05-05', NULL, 5.20, NULL, 0000000001, 'G', 0000000162),
(0000000007, '2015-05-05', NULL, 5.20, NULL, 0000000001, 'G', 0000000163),
(0000000008, '2015-05-05', NULL, 5.20, NULL, 0000000001, 'G', 0000000164),
(0000000009, '2015-05-05', NULL, 5.20, NULL, 0000000001, 'G', 0000000165),
(0000000010, '2015-05-05', NULL, 5.20, NULL, 0000000001, 'G', 0000000166),
(0000000011, '2015-05-05', NULL, 5.20, NULL, 0000000001, 'G', 0000000167),
(0000000012, '2015-05-05', NULL, 10.40, NULL, 0000000001, 'G', 0000000168),
(0000000013, '2015-05-05', NULL, 10.40, NULL, 0000000001, 'G', 0000000169),
(0000000014, '2015-05-05', NULL, 2.40, NULL, 0000000001, 'G', 0000000170),
(0000000015, '2015-05-05', NULL, 5.20, NULL, 0000000001, 'G', 0000000171),
(0000000016, '2015-05-05', NULL, 5.20, NULL, 0000000001, 'G', 0000000172),
(0000000017, '2015-05-06', NULL, 5.20, NULL, 0000000001, 'G', 0000000173),
(0000000018, '2015-05-06', NULL, 57.60, NULL, 0000000001, 'G', 0000000174),
(0000000019, '2015-05-06', NULL, 57.60, NULL, 0000000001, 'G', 0000000175),
(0000000020, '2015-05-06', NULL, 57.60, NULL, 0000000001, 'G', 0000000176),
(0000000021, '2015-05-06', NULL, 57.60, NULL, 0000000001, 'G', 0000000177),
(0000000022, '2015-05-06', NULL, 286.00, NULL, 0000000001, 'G', 0000000178),
(0000000023, '2015-05-06', NULL, 52.00, NULL, 0000000001, 'G', 0000000179),
(0000000024, '2015-05-07', NULL, 52.00, NULL, 0000000001, 'G', 0000000180),
(0000000025, '2015-05-07', NULL, 26.00, NULL, 0000000001, 'G', 0000000181),
(0000000026, '2015-05-07', NULL, 25.60, 0000000003, 0000000001, 'A', 0000000182),
(0000000027, '2015-05-07', NULL, 26.00, NULL, 0000000001, 'A', 0000000183),
(0000000028, '2015-05-07', NULL, 26.00, NULL, 0000000001, 'A', 0000000184),
(0000000029, '2015-05-07', NULL, 20.80, 0000000004, 0000000001, 'A', 0000000185),
(0000000030, '2015-05-12', NULL, 1.20, NULL, 0000000001, 'G', 0000000186),
(0000000031, '2015-05-12', NULL, 5.20, 0000000005, 0000000001, 'G', 0000000187),
(0000000032, '2015-05-12', NULL, 5.20, 0000000006, 0000000001, 'G', 0000000188);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `almacen`
--
ALTER TABLE `almacen`
  ADD PRIMARY KEY (`idAlmacen`);

--
-- Indices de la tabla `almacenarticulo`
--
ALTER TABLE `almacenarticulo`
  ADD PRIMARY KEY (`idAlmacenArticulo`),
  ADD KEY `idAlmacen` (`idAlmacen`),
  ADD KEY `idArticulo` (`idArticulo`);

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`idArticulo`),
  ADD KEY `idCategoria` (`idCategoria`);

--
-- Indices de la tabla `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`idCaja`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idCategoria`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`idCompra`),
  ADD KEY `idOrdenCompra` (`idOrdenCompra`),
  ADD KEY `idProveedor` (`idProveedor`),
  ADD KEY `idUsuario` (`idUsuario`),
  ADD KEY `idMovimientoCaja` (`idMovimientoCaja`),
  ADD KEY `idMovimiento` (`idMovimiento`);

--
-- Indices de la tabla `comprobante`
--
ALTER TABLE `comprobante`
  ADD PRIMARY KEY (`idComprobante`),
  ADD KEY `idSerie` (`idSerie`),
  ADD KEY `idUsuario` (`idUsuario`),
  ADD KEY `idMovimientoCaja` (`idMovimientoCaja`);

--
-- Indices de la tabla `detallecompra`
--
ALTER TABLE `detallecompra`
  ADD PRIMARY KEY (`idDetalleCompra`),
  ADD KEY `idCompra` (`idCompra`),
  ADD KEY `idAlmacenArticulo` (`idAlmacenArticulo`);

--
-- Indices de la tabla `detallecomprobante`
--
ALTER TABLE `detallecomprobante`
  ADD PRIMARY KEY (`idDetalleComprobante`),
  ADD KEY `idComprobante` (`idComprobante`),
  ADD KEY `idArticulo` (`idArticulo`);

--
-- Indices de la tabla `detallemovimiento`
--
ALTER TABLE `detallemovimiento`
  ADD PRIMARY KEY (`idDetalleMovimiento`),
  ADD KEY `idMovimiento` (`idMovimiento`),
  ADD KEY `idArticulo` (`idArticulo`);

--
-- Indices de la tabla `detallemovimientocaja`
--
ALTER TABLE `detallemovimientocaja`
  ADD PRIMARY KEY (`idDetalleMovimientoCaja`),
  ADD KEY `idMovimientoCaja` (`idMovimientoCaja`);

--
-- Indices de la tabla `detalleordencompra`
--
ALTER TABLE `detalleordencompra`
  ADD PRIMARY KEY (`idDetalleOrdenCompra`),
  ADD KEY `idOrdenCompra` (`idOrdenCompra`),
  ADD KEY `idArticulo` (`idArticulo`);

--
-- Indices de la tabla `detalleventa`
--
ALTER TABLE `detalleventa`
  ADD PRIMARY KEY (`idDetalleVenta`),
  ADD KEY `idVenta` (`idVenta`),
  ADD KEY `idAlmacenArticulo` (`idAlmacenArticulo`);

--
-- Indices de la tabla `flujocaja`
--
ALTER TABLE `flujocaja`
  ADD PRIMARY KEY (`idFlujoCaja`),
  ADD KEY `idDetalleMovimientoCaja` (`idDetalleMovimientoCaja`),
  ADD KEY `idCaja` (`idCaja`);

--
-- Indices de la tabla `kardex`
--
ALTER TABLE `kardex`
  ADD PRIMARY KEY (`idKardex`),
  ADD KEY `idAlmacenArticulo` (`idAlmacenArticulo`),
  ADD KEY `idDetalleMovimiento` (`idDetalleMovimiento`);

--
-- Indices de la tabla `lote`
--
ALTER TABLE `lote`
  ADD PRIMARY KEY (`idLote`),
  ADD KEY `idArticulo` (`idArticulo`);

--
-- Indices de la tabla `movimiento`
--
ALTER TABLE `movimiento`
  ADD PRIMARY KEY (`idMovimiento`),
  ADD KEY `idAlmacenOrigen` (`idAlmacenOrigen`),
  ADD KEY `idAlmacenDestino` (`idAlmacenDestino`),
  ADD KEY `idTipoMovimiento` (`idTipoMovimiento`);

--
-- Indices de la tabla `movimientocaja`
--
ALTER TABLE `movimientocaja`
  ADD PRIMARY KEY (`idMovimientoCaja`),
  ADD KEY `idUsuario` (`idUsuario`),
  ADD KEY `idTipoMovimientoCaja` (`idTipoMovimientoCaja`),
  ADD KEY `idCaja` (`idCajaOrigen`),
  ADD KEY `idCajaDestino` (`idCajaDestino`);

--
-- Indices de la tabla `ordencompra`
--
ALTER TABLE `ordencompra`
  ADD PRIMARY KEY (`idOrdenCompra`),
  ADD KEY `idProveedor` (`idProveedor`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`idPago`),
  ADD KEY `idTipoPago` (`idTipoPago`),
  ADD KEY `idComprobante` (`idComprobante`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idProveedor`);

--
-- Indices de la tabla `serie`
--
ALTER TABLE `serie`
  ADD PRIMARY KEY (`idSerie`),
  ADD KEY `idTipoComprobante` (`idTipoComprobante`);

--
-- Indices de la tabla `tipocomprobante`
--
ALTER TABLE `tipocomprobante`
  ADD PRIMARY KEY (`idTipoComprobante`);

--
-- Indices de la tabla `tipomovimiento`
--
ALTER TABLE `tipomovimiento`
  ADD PRIMARY KEY (`idTipoMovimiento`);

--
-- Indices de la tabla `tipomovimientocaja`
--
ALTER TABLE `tipomovimientocaja`
  ADD PRIMARY KEY (`idTipoMovimientoCaja`);

--
-- Indices de la tabla `tipopago`
--
ALTER TABLE `tipopago`
  ADD PRIMARY KEY (`idTipoPago`);

--
-- Indices de la tabla `tipousuario`
--
ALTER TABLE `tipousuario`
  ADD PRIMARY KEY (`idtipoUsuario`),
  ADD UNIQUE KEY `nombre_UNIQUE` (`nombre`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `idTipoUsuario` (`idTipoUsuario`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idVenta`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `idUsuario` (`idUsuario`),
  ADD KEY `idComprobante` (`idComprobante`),
  ADD KEY `idMovimiento` (`idMovimiento`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `almacen`
--
ALTER TABLE `almacen`
  MODIFY `idAlmacen` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `almacenarticulo`
--
ALTER TABLE `almacenarticulo`
  MODIFY `idAlmacenArticulo` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `idArticulo` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1034;
--
-- AUTO_INCREMENT de la tabla `caja`
--
ALTER TABLE `caja`
  MODIFY `idCaja` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `idCategoria` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `compra`
--
ALTER TABLE `compra`
  MODIFY `idCompra` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `comprobante`
--
ALTER TABLE `comprobante`
  MODIFY `idComprobante` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `detallecompra`
--
ALTER TABLE `detallecompra`
  MODIFY `idDetalleCompra` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `detallecomprobante`
--
ALTER TABLE `detallecomprobante`
  MODIFY `idDetalleComprobante` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detallemovimiento`
--
ALTER TABLE `detallemovimiento`
  MODIFY `idDetalleMovimiento` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=155;
--
-- AUTO_INCREMENT de la tabla `detallemovimientocaja`
--
ALTER TABLE `detallemovimientocaja`
  MODIFY `idDetalleMovimientoCaja` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT de la tabla `detalleordencompra`
--
ALTER TABLE `detalleordencompra`
  MODIFY `idDetalleOrdenCompra` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `detalleventa`
--
ALTER TABLE `detalleventa`
  MODIFY `idDetalleVenta` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `flujocaja`
--
ALTER TABLE `flujocaja`
  MODIFY `idFlujoCaja` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `kardex`
--
ALTER TABLE `kardex`
  MODIFY `idKardex` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT de la tabla `lote`
--
ALTER TABLE `lote`
  MODIFY `idLote` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `movimiento`
--
ALTER TABLE `movimiento`
  MODIFY `idMovimiento` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=189;
--
-- AUTO_INCREMENT de la tabla `movimientocaja`
--
ALTER TABLE `movimientocaja`
  MODIFY `idMovimientoCaja` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `ordencompra`
--
ALTER TABLE `ordencompra`
  MODIFY `idOrdenCompra` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `idPago` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idProveedor` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `serie`
--
ALTER TABLE `serie`
  MODIFY `idSerie` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipocomprobante`
--
ALTER TABLE `tipocomprobante`
  MODIFY `idTipoComprobante` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipomovimiento`
--
ALTER TABLE `tipomovimiento`
  MODIFY `idTipoMovimiento` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `tipomovimientocaja`
--
ALTER TABLE `tipomovimientocaja`
  MODIFY `idTipoMovimientoCaja` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tipopago`
--
ALTER TABLE `tipopago`
  MODIFY `idTipoPago` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipousuario`
--
ALTER TABLE `tipousuario`
  MODIFY `idtipoUsuario` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `idVenta` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `almacenarticulo`
--
ALTER TABLE `almacenarticulo`
  ADD CONSTRAINT `almacenarticulo_ibfk_1` FOREIGN KEY (`idAlmacen`) REFERENCES `almacen` (`idAlmacen`),
  ADD CONSTRAINT `almacenarticulo_ibfk_2` FOREIGN KEY (`idArticulo`) REFERENCES `articulo` (`idArticulo`);

--
-- Filtros para la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD CONSTRAINT `articulo_ibfk_1` FOREIGN KEY (`idCategoria`) REFERENCES `categoria` (`idCategoria`);

--
-- Filtros para la tabla `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `compra_ibfk_1` FOREIGN KEY (`idOrdenCompra`) REFERENCES `ordencompra` (`idOrdenCompra`),
  ADD CONSTRAINT `compra_ibfk_2` FOREIGN KEY (`idProveedor`) REFERENCES `proveedor` (`idProveedor`),
  ADD CONSTRAINT `compra_ibfk_3` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idusuario`),
  ADD CONSTRAINT `compra_ibfk_4` FOREIGN KEY (`idMovimientoCaja`) REFERENCES `movimientocaja` (`idMovimientoCaja`),
  ADD CONSTRAINT `compra_ibfk_5` FOREIGN KEY (`idMovimiento`) REFERENCES `movimiento` (`idMovimiento`);

--
-- Filtros para la tabla `comprobante`
--
ALTER TABLE `comprobante`
  ADD CONSTRAINT `comprobante_ibfk_1` FOREIGN KEY (`idSerie`) REFERENCES `serie` (`idSerie`),
  ADD CONSTRAINT `comprobante_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idusuario`),
  ADD CONSTRAINT `comprobante_ibfk_3` FOREIGN KEY (`idMovimientoCaja`) REFERENCES `movimientocaja` (`idMovimientoCaja`);

--
-- Filtros para la tabla `detallecompra`
--
ALTER TABLE `detallecompra`
  ADD CONSTRAINT `detallecompra_ibfk_1` FOREIGN KEY (`idAlmacenArticulo`) REFERENCES `almacenarticulo` (`idAlmacenArticulo`),
  ADD CONSTRAINT `detallecompra_ibfk_2` FOREIGN KEY (`idCompra`) REFERENCES `compra` (`idCompra`);

--
-- Filtros para la tabla `detallecomprobante`
--
ALTER TABLE `detallecomprobante`
  ADD CONSTRAINT `detallecomprobante_ibfk_1` FOREIGN KEY (`idArticulo`) REFERENCES `articulo` (`idArticulo`),
  ADD CONSTRAINT `detallecomprobante_ibfk_2` FOREIGN KEY (`idComprobante`) REFERENCES `comprobante` (`idComprobante`);

--
-- Filtros para la tabla `detallemovimiento`
--
ALTER TABLE `detallemovimiento`
  ADD CONSTRAINT `detallemovimiento_ibfk_1` FOREIGN KEY (`idMovimiento`) REFERENCES `movimiento` (`idMovimiento`),
  ADD CONSTRAINT `detallemovimiento_ibfk_2` FOREIGN KEY (`idArticulo`) REFERENCES `articulo` (`idArticulo`);

--
-- Filtros para la tabla `detallemovimientocaja`
--
ALTER TABLE `detallemovimientocaja`
  ADD CONSTRAINT `detallemovimientocaja_ibfk_1` FOREIGN KEY (`idMovimientoCaja`) REFERENCES `movimientocaja` (`idMovimientoCaja`);

--
-- Filtros para la tabla `detalleordencompra`
--
ALTER TABLE `detalleordencompra`
  ADD CONSTRAINT `detalleordencompra_ibfk_1` FOREIGN KEY (`idArticulo`) REFERENCES `articulo` (`idArticulo`),
  ADD CONSTRAINT `detalleordencompra_ibfk_2` FOREIGN KEY (`idOrdenCompra`) REFERENCES `ordencompra` (`idOrdenCompra`);

--
-- Filtros para la tabla `detalleventa`
--
ALTER TABLE `detalleventa`
  ADD CONSTRAINT `detalleventa_ibfk_1` FOREIGN KEY (`idVenta`) REFERENCES `venta` (`idVenta`),
  ADD CONSTRAINT `detalleventa_ibfk_2` FOREIGN KEY (`idAlmacenArticulo`) REFERENCES `almacenarticulo` (`idAlmacenArticulo`);

--
-- Filtros para la tabla `flujocaja`
--
ALTER TABLE `flujocaja`
  ADD CONSTRAINT `flujocaja_ibfk_1` FOREIGN KEY (`idDetalleMovimientoCaja`) REFERENCES `detallemovimientocaja` (`idDetalleMovimientoCaja`),
  ADD CONSTRAINT `flujocaja_ibfk_2` FOREIGN KEY (`idCaja`) REFERENCES `caja` (`idCaja`);

--
-- Filtros para la tabla `kardex`
--
ALTER TABLE `kardex`
  ADD CONSTRAINT `fg_id_detallemovimiento` FOREIGN KEY (`idDetalleMovimiento`) REFERENCES `detallemovimiento` (`idDetalleMovimiento`),
  ADD CONSTRAINT `kardex_ibfk_1` FOREIGN KEY (`idAlmacenArticulo`) REFERENCES `almacenarticulo` (`idAlmacenArticulo`);

--
-- Filtros para la tabla `lote`
--
ALTER TABLE `lote`
  ADD CONSTRAINT `lote_ibfk_1` FOREIGN KEY (`idArticulo`) REFERENCES `articulo` (`idArticulo`);

--
-- Filtros para la tabla `movimiento`
--
ALTER TABLE `movimiento`
  ADD CONSTRAINT `movimiento_ibfk_1` FOREIGN KEY (`idAlmacenOrigen`) REFERENCES `almacen` (`idAlmacen`),
  ADD CONSTRAINT `movimiento_ibfk_2` FOREIGN KEY (`idAlmacenDestino`) REFERENCES `almacen` (`idAlmacen`),
  ADD CONSTRAINT `movimiento_ibfk_3` FOREIGN KEY (`idTipoMovimiento`) REFERENCES `tipomovimiento` (`idTipoMovimiento`);

--
-- Filtros para la tabla `movimientocaja`
--
ALTER TABLE `movimientocaja`
  ADD CONSTRAINT `movimientocaja_ibfk_1` FOREIGN KEY (`idTipoMovimientoCaja`) REFERENCES `tipomovimientocaja` (`idTipoMovimientoCaja`),
  ADD CONSTRAINT `movimientocaja_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idusuario`),
  ADD CONSTRAINT `movimientocaja_ibfk_3` FOREIGN KEY (`idCajaOrigen`) REFERENCES `caja` (`idCaja`),
  ADD CONSTRAINT `movimientocaja_ibfk_4` FOREIGN KEY (`idCajaDestino`) REFERENCES `caja` (`idCaja`);

--
-- Filtros para la tabla `ordencompra`
--
ALTER TABLE `ordencompra`
  ADD CONSTRAINT `ordencompra_ibfk_1` FOREIGN KEY (`idProveedor`) REFERENCES `proveedor` (`idProveedor`),
  ADD CONSTRAINT `ordencompra_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idusuario`);

--
-- Filtros para la tabla `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `pago_ibfk_1` FOREIGN KEY (`idTipoPago`) REFERENCES `tipopago` (`idTipoPago`),
  ADD CONSTRAINT `pago_ibfk_2` FOREIGN KEY (`idComprobante`) REFERENCES `comprobante` (`idComprobante`);

--
-- Filtros para la tabla `serie`
--
ALTER TABLE `serie`
  ADD CONSTRAINT `serie_ibfk_1` FOREIGN KEY (`idTipoComprobante`) REFERENCES `tipocomprobante` (`idTipoComprobante`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`idTipoUsuario`) REFERENCES `tipousuario` (`idtipoUsuario`);

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`),
  ADD CONSTRAINT `venta_ibfk_2` FOREIGN KEY (`idComprobante`) REFERENCES `comprobante` (`idComprobante`),
  ADD CONSTRAINT `venta_ibfk_3` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idusuario`),
  ADD CONSTRAINT `venta_ibfk_4` FOREIGN KEY (`idMovimiento`) REFERENCES `movimiento` (`idMovimiento`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
