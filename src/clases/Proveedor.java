/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;


/**
 *
 * @author Edder
 */
public class Proveedor {
    private int idProveedor;
    private String nombre;
    private String preventa;
    private String mercaderista;
    private String direccion;
    private String telefono;
    private String descripcion;

    public Proveedor() {
    }

    public Proveedor(int idProveedor, String nombre, String preventa, String mercaderista, String direccion, String telefono, String descripcion) {
        this.idProveedor = idProveedor;
        this.nombre = nombre;
        this.preventa = preventa;
        this.mercaderista = mercaderista;
        this.direccion = direccion;
        this.telefono = telefono;
        this.descripcion = descripcion;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPreventa() {
        return preventa;
    }

    public void setPreventa(String preventa) {
        this.preventa = preventa;
    }

    public String getMercaderista() {
        return mercaderista;
    }

    public void setMercaderista(String mercaderista) {
        this.mercaderista = mercaderista;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Override
    public String toString(){
        return this.nombre;
    }
}
