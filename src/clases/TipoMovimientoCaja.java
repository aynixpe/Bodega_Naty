/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Edder
 */
public class TipoMovimientoCaja {
    private int idTipoMovimientoCaja;
    private String descripcion;
    private char tipo; //Ingreso - Egreso

    public TipoMovimientoCaja() {
    }

    public TipoMovimientoCaja(int idTipoMovimientoCaja, String descripcion, char tipo) {
        this.idTipoMovimientoCaja = idTipoMovimientoCaja;
        this.descripcion = descripcion;
        this.tipo = tipo;
    }

    public int getIdTipoMovimientoCaja() {
        return idTipoMovimientoCaja;
    }

    public void setIdTipoMovimientoCaja(int idTipoMovimientoCaja) {
        this.idTipoMovimientoCaja = idTipoMovimientoCaja;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public char getTipo() {
        return tipo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }
    
    @Override
    public String toString(){
        return this.descripcion;
    }
}
