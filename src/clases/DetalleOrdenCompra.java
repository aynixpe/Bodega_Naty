/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Edder
 */
public class DetalleOrdenCompra {
    private int idDetalleOrdenCompra;
    private OrdenCompra ordenCompra;
    private Articulo articulo;
    private int cantidad;
    private float precio;
    private float total;

    public DetalleOrdenCompra() {
    }

    public DetalleOrdenCompra(int idDetalleOrdenCompra, OrdenCompra ordenCompra, Articulo articulo, int cantidad, float precio, float total) {
        this.idDetalleOrdenCompra = idDetalleOrdenCompra;
        this.ordenCompra = ordenCompra;
        this.articulo = articulo;
        this.cantidad = cantidad;
        this.precio = precio;
        this.total = total;
    }

    public int getIdDetalleOrdenCompra() {
        return idDetalleOrdenCompra;
    }

    public void setIdDetalleOrdenCompra(int idDetalleOrdenCompra) {
        this.idDetalleOrdenCompra = idDetalleOrdenCompra;
    }

    public OrdenCompra getOrdenCompra() {
        return ordenCompra;
    }

    public void setOrdenCompra(OrdenCompra ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    public float getTotal() {
		return total;
	}
    
    public void setTotal(float total) {
		this.total = total;
	}
}
