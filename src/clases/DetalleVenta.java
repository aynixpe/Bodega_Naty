/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Edder
 */
public class DetalleVenta {
    private int idDetalleVenta;
    private Venta venta;
    private AlmacenArticulo almacenArticulo;
    private int cantidad;
    private float precio;
    private float descuento;
    private DetalleComprobante detalleComprobante;

    public DetalleVenta() {
    }

    public DetalleVenta(int idDetalleVenta, Venta venta, AlmacenArticulo almacenArticulo, int cantidad, 
    		float precio, float descuento, DetalleComprobante detalleComprobante) {
        this.idDetalleVenta = idDetalleVenta;
        this.venta = venta;
        this.almacenArticulo = almacenArticulo;
        this.cantidad = cantidad;
        this.precio = precio;
        this.descuento = descuento;
        this.detalleComprobante = detalleComprobante;
    }

    public int getIdDetalleVenta() {
        return idDetalleVenta;
    }

    public void setIdDetalleVenta(int idDetalleVenta) {
        this.idDetalleVenta = idDetalleVenta;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public AlmacenArticulo getAlmacenArticulo() {
        return almacenArticulo;
    }

    public void setAlmacenArticulo(AlmacenArticulo almacenArticulo) {
        this.almacenArticulo = almacenArticulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getDescuento() {
        return descuento;
    }

    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }
    
    public DetalleComprobante getDetalleComprobante(){
    	return this.detalleComprobante;
    }
    
    public void setDetalleComprobante(DetalleComprobante detalleComprobante){
    	this.detalleComprobante = detalleComprobante;
    }
}
