/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.time.LocalDate;

/**
 *
 * @author Edder
 */
public class Comprobante {
    private int idComprobante;
    private Serie serie;
    private int numero;
    private String nombreCliente;
    private LocalDate fecha;
    private float monto;
    private float igv;
    private Usuario usuario;
    private MovimientoCaja movimientoCaja;
    private String descripcion;
    private char estado;

    public Comprobante() {
    }

    public Comprobante(int idComprobante, Serie serie, int numero, 
    		String nombreCliente, LocalDate fecha, 
    		float monto, float igv, Usuario usuario, 
    		MovimientoCaja movimientoCaja, char estado, String descripcion) {
        this.idComprobante = idComprobante;
        this.serie = serie;
        this.numero = numero;
        this.nombreCliente = nombreCliente;
        this.fecha = fecha;
        this.monto = monto;
        this.igv = igv;
        this.usuario = usuario;
        this.movimientoCaja = movimientoCaja;
        this.estado = estado;
    }

    public int getIdComprobante() {
        return idComprobante;
    }

    public void setIdComprobante(int idComprobante) {
        this.idComprobante = idComprobante;
    }

    public Serie getSerie() {
        return serie;
    }

    public void setSerie(Serie serie) {
        this.serie = serie;
    }
    
    public int getNumero() {
        return this.numero;
    }
    
    public void setNumero(int numero){
    	this.numero = numero;
    }

    public void setSerie(int numero) {
        this.numero = numero;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public float getIgv() {
        return igv;
    }

    public void setIgv(float igv) {
        this.igv = igv;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public MovimientoCaja getMovimientoCaja(){
    	return this.movimientoCaja;
    }
    
    public void setMovimientoCaja(MovimientoCaja movimientoCaja){
    	this.movimientoCaja = movimientoCaja;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }
    
    public void setDescripcion(String descripcion){
    	this.descripcion = descripcion;
    }
    
    public String getDescripcion(){
    	return descripcion;
    }
    
    @Override
    public String toString(){
        return String.valueOf(this.numero);
    }
}
