/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Edder
 */
public class Almacen {
    private int idAlmacen;
    private String nombre;
    private char estado;

    public Almacen(int idAlmacen, String nombre, char estado) {
        this.idAlmacen = idAlmacen;
        this.nombre = nombre;
        this.estado = estado;
    }

    public Almacen() {
    }

    public int getIdAlmacen() {
        return idAlmacen;
    }

    public void setIdAlmacen(int idAlmacen) {
        this.idAlmacen = idAlmacen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }
    
    @Override
    public String toString(){
        return this.nombre;
    }
}
