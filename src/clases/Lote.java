/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.time.LocalDate;

/**
 *
 * @author Edder
 */
public class Lote {
    private int idLote;
    private Articulo articulo;
    private LocalDate fechaVencimiento;
    private String lote;
    private char estado;

    public Lote(int idLote, Articulo articulo, LocalDate fechaVencimiento, String lote, char estado) {
        this.idLote = idLote;
        this.articulo = articulo;
        this.fechaVencimiento = fechaVencimiento;
        this.lote = lote;
        this.estado = estado;
    }

    public Lote() {
    }

    public int getIdLote() {
        return idLote;
    }

    public void setIdLote(int idLote) {
        this.idLote = idLote;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }
    
    @Override
    public String toString(){
        return this.lote;
    }
}
