/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Edder
 */
public class Caja {
    private int idCaja;
    private String nombre;
    private float monto;

    public Caja() {
    }

    public Caja(int idCaja, String nombre, float monto) {
        this.idCaja = idCaja;
        this.nombre = nombre;
        this.monto = monto;
    }

    public int getIdCaja() {
        return idCaja;
    }

    public void setIdCaja(int idCaja) {
        this.idCaja = idCaja;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setMonto(float monto) {
		this.monto = monto;
	}
    
    public float getMonto() {
		return monto;
	}
    
    public String toString(){
    	return nombre;
    }
}
