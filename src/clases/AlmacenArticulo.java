/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Edder
 */
public class AlmacenArticulo {
    private int idAlmacenArticulo;
    private Almacen almacen;
    private Articulo articulo;
    private int stock;
    private int stockMinimo;

    public AlmacenArticulo(int idAlmacenArticulo, Almacen almacen, Articulo articulo, int stock, int stockMinimo) {
        this.idAlmacenArticulo = idAlmacenArticulo;
        this.almacen = almacen;
        this.articulo = articulo;
        this.stock = stock;
        this.stockMinimo = stockMinimo;
    }

    public AlmacenArticulo() {
    }

    public int getIdAlmacenArticulo() {
        return idAlmacenArticulo;
    }

    public void setIdAlmacenArticulo(int idAlmacenArticulo) {
        this.idAlmacenArticulo = idAlmacenArticulo;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(int stockMinimo) {
        this.stockMinimo = stockMinimo;
    }    
}
