/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Edder
 */
public class DetalleComprobante {
    private int idDetalleComprobante;
    private Comprobante comprobante;
    private Articulo articulo;
    private int cantidad;
    private float precio;
    private float igv;

    public DetalleComprobante() {
    }

    public DetalleComprobante(int idDetalleComprobante, Comprobante comprobante, Articulo articulo, int cantidad, float precio, float igv) {
        this.idDetalleComprobante = idDetalleComprobante;
        this.comprobante = comprobante;
        this.articulo = articulo;
        this.cantidad = cantidad;
        this.precio = precio;
        this.igv = igv;
    }

    public int getIdDetalleComprobante() {
        return idDetalleComprobante;
    }

    public void setIdDetalleComprobante(int idDetalleComprobante) {
        this.idDetalleComprobante = idDetalleComprobante;
    }

    public Comprobante getComprobante() {
        return comprobante;
    }

    public void setComprobante(Comprobante comprobante) {
        this.comprobante = comprobante;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getIgv() {
        return igv;
    }

    public void setIgv(float igv) {
        this.igv = igv;
    }
}
