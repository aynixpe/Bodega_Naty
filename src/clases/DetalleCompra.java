/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Edder
 */
public class DetalleCompra {
    private int idDetalleCompra;
    private Compra compra;
    private AlmacenArticulo almacenArticulo;
    private int cantidad;
    private float precio;
    private float total;
    private float igv;

    public DetalleCompra() {
    }

    public DetalleCompra(int idDetalleCompra, Compra compra, AlmacenArticulo almacenArticulo, int cantidad, float precio, float total, float igv) {
        this.idDetalleCompra = idDetalleCompra;
        this.compra = compra;
        this.almacenArticulo = almacenArticulo;
        this.cantidad = cantidad;
        this.precio = precio;
        this.total = total;
        this.igv = igv;
    }

    public int getIdDetalleCompra() {
        return idDetalleCompra;
    }

    public void setIdDetalleCompra(int idDetalleCompra) {
        this.idDetalleCompra = idDetalleCompra;
    }

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    public AlmacenArticulo getAlmacenArticulo() {
        return almacenArticulo;
    }

    public void setAlmacenArticulo(AlmacenArticulo almacenArticulo) {
        this.almacenArticulo = almacenArticulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    public void setTotal(float total) {
		this.total = total;
	}
    
    public float getTotal() {
		return total;
	}

    public float getIgv() {
        return igv;
    }

    public void setIgv(float igv) {
        this.igv = igv;
    }
}
