/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clases;

/**
 *
 * @author edder
 */
public class Articulo {
    private int idArticulo;
    private String nombre;
    private String presentacion;
    private String descripcion;
    private int stockMinimo;
    private String tamanio;
    private String unidad;
    private String codigoBarras;
    private Categoria categoria;
    private float precioVenta;
    private char estado;

    public Articulo(int idArticulo, String nombre, String presentacion, String descripcion, int stockMinimo, String tamanio, String unidad, String codigoBarras, Categoria categoria, float precioVenta, char estado) {
        this.idArticulo = idArticulo;
        this.nombre = nombre;
        this.presentacion = presentacion;
        this.descripcion = descripcion;
        this.stockMinimo = stockMinimo;
        this.tamanio = tamanio;
        this.unidad = unidad;
        this.codigoBarras = codigoBarras;
        this.categoria = categoria;
        this.precioVenta = precioVenta;
        this.estado = estado;
    }

    public Articulo() {
    }

    public int getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(int idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(int stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public String getTamanio() {
        return tamanio;
    }

    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public float getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(float precioVenta) {
        this.precioVenta = precioVenta;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }
    
    @Override
    public String toString(){
        return this.nombre + " " 
        		+ this.presentacion + " " 
        		+ this.tamanio + " "
        		+ this.unidad;
    }
}
