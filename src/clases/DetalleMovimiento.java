/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Edder
 */
public class DetalleMovimiento {
    private int idDetalleMovimiento;
    private Movimiento movimiento;
    private Articulo articulo;
    private int cantidad;

    public DetalleMovimiento() {
    }

    public DetalleMovimiento(int idDetalleMovimiento, Movimiento movimiento, Articulo articulo, int cantidad) {
        this.idDetalleMovimiento = idDetalleMovimiento;
        this.movimiento = movimiento;
        this.articulo = articulo;
        this.cantidad = cantidad;
    }

    public int getIdDetalleMovimiento() {
        return idDetalleMovimiento;
    }

    public void setIdDetalleMovimiento(int idDetalleMovimiento) {
        this.idDetalleMovimiento = idDetalleMovimiento;
    }

    public Movimiento getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(Movimiento movimiento) {
        this.movimiento = movimiento;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    @Override
    public String toString(){
        return this.articulo.toString();
    }
}
