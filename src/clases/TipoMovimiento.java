/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Edder
 */
public class TipoMovimiento {
    private int idTipoMovimiento;
    private char tipo;
    private String nombre;
    
    public TipoMovimiento(){        
    }

    public TipoMovimiento(int idTipoMovimiento, char tipo, String nombre) {
        this.idTipoMovimiento = idTipoMovimiento;
        this.tipo = tipo;
        this.nombre = nombre;
    }

    public int getIdTipoMovimiento() {
        return idTipoMovimiento;
    }

    public void setIdTipoMovimiento(int idTipoMovimiento) {
        this.idTipoMovimiento = idTipoMovimiento;
    }

    public char getTipo() {
        return tipo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Override
    public String toString(){
        return this.nombre;
    }
}
