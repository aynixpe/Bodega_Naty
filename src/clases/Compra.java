/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.time.LocalDate;

/**
 *
 * @author Edder
 */
public class Compra {
    private int idCompra;
    private LocalDate fecha;
    private float monto;
    private float igv;
    private OrdenCompra ordenCompra;
    private Proveedor proveedor;
    private Usuario usuario;
    private MovimientoCaja movimientoCaja;
    private char estado;
    private Movimiento movimiento;
    private String factura;

    public Compra() {
    }

    public Compra(int idCompra, LocalDate fecha, float monto, float igv, 
    		OrdenCompra ordenCompra, Proveedor proveedor, 
    		Usuario usuario, MovimientoCaja movimientoCaja, 
    		char estado, Movimiento movimiento, String factura) {
    	this.idCompra = idCompra;
    	this.fecha = fecha;
        this.monto = monto;
        this.igv = igv;
        this.ordenCompra = ordenCompra;
        this.proveedor = proveedor;
        this.usuario = usuario;
        this.movimientoCaja = movimientoCaja;
        this.estado = estado;
        this.movimiento = movimiento;
        this.factura = factura;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }

    public int getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(int idCompra) {
        this.idCompra = idCompra;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public float getIgv() {
        return igv;
    }

    public void setIgv(float igv) {
        this.igv = igv;
    }

    public OrdenCompra getOrdenCompra() {
        return ordenCompra;
    }

    public void setOrdenCompra(OrdenCompra ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }
    
    public MovimientoCaja getMovimientoCaja(){
    	return this.movimientoCaja;
    }
    
    public void setMovimientoCaja(MovimientoCaja movimientoCaja){
    	this.movimientoCaja = movimientoCaja;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public Movimiento getMovimiento(){
    	return this.movimiento;
    }
    
    public void setMovimiento(Movimiento movimiento){
    	this.movimiento = movimiento;
    }

    public String getFactura(){
        return this.factura;
    }

    public void setFactura(String factura){
        this.factura = factura;
    }
}
