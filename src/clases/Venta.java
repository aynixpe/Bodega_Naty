/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.time.LocalDate;

/**
 *
 * @author Edder
 */
public class Venta {
    private int idVenta;
    private LocalDate fecha;
    private Cliente cliente;
    private float monto;
    private Usuario usuario;
    private char estado;
    private Movimiento movimiento;

    public Venta() {
    }

    public Venta(int idVenta, LocalDate fecha, Cliente cliente, float monto, Usuario usuario, char estado, Movimiento movimiento) {
        this.idVenta = idVenta;
        this.fecha = fecha;
        this.cliente = cliente;
        this.monto = monto;
        this.usuario = usuario;
        this.estado = estado;
        this.movimiento = movimiento;
    }

    public int getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(int idVenta) {
        this.idVenta = idVenta;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }
    
    public Movimiento getMovimiento(){
    	return this.movimiento;
    }
    
    public void setMovimiento(Movimiento movimiento){
    	this.movimiento = movimiento;
    }
}
