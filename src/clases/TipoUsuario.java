/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Edder
 */
public class TipoUsuario {
    private int idTipoUsuario;
    private String nombre;
    private int p_movimiento;
    private int p_venta;
    private int p_comprobante;
    private int p_ordenCompra;
    private int p_compra;
    private int p_movimiento_caja;
    
    /**
     * ACCIONES
     */
    public static final int MOVIMIENTO = 1;
    public static final int VENTA = 2;
    public static final int COMPROBANTE = 3;
    public static final int ORDEN_COMPRA = 4;
    public static final int COMPRA = 5;
    public static final int MOVIMIENTO_CAJA = 6;    
    
    /**
     * PERMISOS
     */
    public static final int SIN_PERMISO = 0;
    public static final int CONSULTAR = 1;
    public static final int INSERTAR = 2;
    public static final int EDITAR = 4;
    public static final int ELIMINAR = 8;   

    public TipoUsuario() {
    }

    public TipoUsuario(int idTipoUsuario, String nombre, int p_movimiento, 
            int p_venta, int p_comprobante, int p_ordenCompra, int p_compra, 
            int p_movimiento_caja) {
        this.idTipoUsuario = idTipoUsuario;
        this.nombre = nombre;
        this.p_movimiento = p_movimiento;
        this.p_venta = p_venta;
        this.p_comprobante = p_comprobante;
        this.p_ordenCompra = p_ordenCompra;
        this.p_compra = p_compra;
        this.p_movimiento_caja = p_movimiento_caja;
    }

    public int getIdTipoUsuario() {
        return idTipoUsuario;
    }

    public void setIdTipoUsuario(int idTipoUsuario) {
        this.idTipoUsuario = idTipoUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getP_movimiento() {
        return p_movimiento;
    }

    public void setP_movimiento(int p_movimiento) {
        this.p_movimiento = p_movimiento;
    }

    public int getP_venta() {
        return p_venta;
    }

    public void setP_venta(int p_venta) {
        this.p_venta = p_venta;
    }

    public int getP_comprobante() {
        return p_comprobante;
    }

    public void setP_comprobante(int p_comprobante) {
        this.p_comprobante = p_comprobante;
    }

    public int getP_ordenCompra() {
        return p_ordenCompra;
    }

    public void setP_ordenCompra(int p_ordenCompra) {
        this.p_ordenCompra = p_ordenCompra;
    }

    public int getP_compra() {
        return p_compra;
    }

    public void setP_compra(int p_compra) {
        this.p_compra = p_compra;
    }

    public int getP_movimiento_caja() {
        return p_movimiento_caja;
    }

    public void setP_movimiento_caja(int p_movimiento_caja) {
        this.p_movimiento_caja = p_movimiento_caja;
    }
    
    @Override
    public String toString(){
        return this.nombre;
    }
}
