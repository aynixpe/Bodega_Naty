/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.time.LocalDate;

/**
 *
 * @author Edder
 */
public class OrdenCompra {
    private int idOrdenCompra;
    private LocalDate fecha;
    private float monto;
    private LocalDate fechaEntrega;
    private Proveedor proveedor;
    private Usuario usuario;
    private char estado;

    public OrdenCompra() {
    }

    public OrdenCompra(int idOrdenCompra, LocalDate fecha, float monto, LocalDate fechaEntrega, Proveedor proveedor, Usuario usuario, char estado) {
        this.idOrdenCompra = idOrdenCompra;
        this.fecha = fecha;
        this.monto = monto;
        this.fechaEntrega = fechaEntrega;
        this.proveedor = proveedor;
        this.usuario = usuario;
        this.estado = estado;
    }

    public int getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(int idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public LocalDate getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(LocalDate fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }
}
