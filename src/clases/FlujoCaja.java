package clases;

import java.time.LocalDate;

public class FlujoCaja {
	private int idFlujoCaja;
	private DetalleMovimientoCaja detalleMovimientoCaja;
	private Caja caja;
	private char movimiento;
	private float monto;
	private float montoBefore;
	private float montoAfter;
	private LocalDate fecha;
	
	public FlujoCaja(){}
	
	public FlujoCaja(int idFlujoCaja, DetalleMovimientoCaja detalleMovimientoCaja, Caja caja, char movimiento, float monto, float montoBefore, float montoAfter, LocalDate fecha) {
		// TODO Auto-generated constructor stub
		this.idFlujoCaja = idFlujoCaja;
		this.detalleMovimientoCaja = detalleMovimientoCaja;
		this.caja = caja;
		this.movimiento = movimiento;
		this.monto = monto;
		this.montoBefore = montoBefore;
		this.montoAfter = montoAfter;
		this.fecha = fecha;
	}
	
	public Caja getCaja() {
		return caja;
	}
	
	public DetalleMovimientoCaja getDetalleMovimientoCaja() {
		return detalleMovimientoCaja;
	}
	
	public void setMovimiento(char movimiento) {
		this.movimiento = movimiento;
	}
	
	public void setMontoBefore(float montoBefore) {
		this.montoBefore = montoBefore;
	}
	
	public void setMontoAfter(float montoAfter) {
		this.montoAfter = montoAfter;
	}
	
	public void setMonto(float monto) {
		this.monto = monto;
	}
	
	public void setIdFlujoCaja(int idFlujoCaja) {
		this.idFlujoCaja = idFlujoCaja;
	}
	
	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	public void setDetalleMovimientoCaja(
			DetalleMovimientoCaja detalleMovimientoCaja) {
		this.detalleMovimientoCaja = detalleMovimientoCaja;
	}
	
	public void setCaja(Caja caja) {
		this.caja = caja;
	}
	
	public char getMovimiento() {
		return movimiento;
	}
	
	public float getMontoBefore() {
		return montoBefore;
	}
	
	public float getMontoAfter() {
		return montoAfter;
	}
	
	public float getMonto() {
		return monto;
	}
	
	public int getIdFlujoCaja() {
		return idFlujoCaja;
	}
	
	public LocalDate getFecha() {
		return fecha;
	}
}
