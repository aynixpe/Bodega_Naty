/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.time.LocalDate;

/**
 *
 * @author Edder
 */
public class MovimientoCaja {
    private int idMovimientoCaja;
    private TipoMovimientoCaja tipoMovimientoCaja;
    private int referencia;//compra, venta, otros
    private LocalDate fecha;
    private float monto;
    private Usuario usuario;
    private char estado;
    private Caja cajaOrigen;
    private Caja cajaDestino;

    public MovimientoCaja() {
    }

    public MovimientoCaja(int idMovimientoCaja, TipoMovimientoCaja tipoMovimientoCaja, int referencia, LocalDate fecha, float monto, Usuario usuario, char estado, Caja cajaOrigen, Caja cajaDestino) {
        this.idMovimientoCaja = idMovimientoCaja;
        this.tipoMovimientoCaja = tipoMovimientoCaja;
        this.referencia = referencia;
        this.fecha = fecha;
        this.monto = monto;
        this.usuario = usuario;
        this.estado = estado;
        this.cajaOrigen = cajaOrigen;
        this.cajaDestino = cajaDestino;
    }

    public int getIdMovimientoCaja() {
        return idMovimientoCaja;
    }

    public void setIdMovimientoCaja(int idMovimientoCaja) {
        this.idMovimientoCaja = idMovimientoCaja;
    }

    public TipoMovimientoCaja getTipoMovimientoCaja() {
        return tipoMovimientoCaja;
    }

    public void setTipoMovimientoCaja(TipoMovimientoCaja tipoMovimientoCaja) {
        this.tipoMovimientoCaja = tipoMovimientoCaja;
    }

    public int getReferencia() {
        return this.referencia;
    }

    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }
    
    public void setCajaOrigen(Caja cajaOrigen) {
		this.cajaOrigen = cajaOrigen;
	}
    
    public Caja getCajaOrigen() {
		return cajaOrigen;
	}
    
    public void setCajaDestino(Caja cajaDestino) {
		this.cajaDestino = cajaDestino;
	}
    
    public Caja getCajaDestino() {
		return cajaDestino;
	}
}
