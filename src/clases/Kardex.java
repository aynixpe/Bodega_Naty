/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.time.LocalDate;

/**
 *
 * @author Edder
 */
public class Kardex {
    private int idKardex;
    private AlmacenArticulo almacenArticulo;
    private char movimiento;
    private int cantidad;
    private int stockBefore;
    private int stockAfter;
    private LocalDate fecha;
    private DetalleMovimiento detalleMovimiento;

    public Kardex() {
    }

    public Kardex(int idKardex, AlmacenArticulo almacenArticulo, char movimiento, int cantidad, int stockBefore, int stockAfter, LocalDate fecha, DetalleMovimiento detalleMovimiento) {
        this.idKardex = idKardex;
        this.almacenArticulo = almacenArticulo;
        this.movimiento = movimiento;
        this.cantidad = cantidad;
        this.stockBefore = stockBefore;
        this.stockAfter = stockAfter;
        this.fecha = fecha;
        this.detalleMovimiento = detalleMovimiento;
    }

    public int getIdKardex() {
        return idKardex;
    }

    public void setIdKardex(int idKardex) {
        this.idKardex = idKardex;
    }

    public AlmacenArticulo getAlmacenArticulo() {
        return almacenArticulo;
    }

    public void setAlmacenArticulo(AlmacenArticulo almacenArticulo) {
        this.almacenArticulo = almacenArticulo;
    }

    public char getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(char movimiento) {
        this.movimiento = movimiento;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    public void setStockBefore(int stockBefore) {
		this.stockBefore = stockBefore;
	}
    
    public void setStockAfter(int stockAfter) {
		this.stockAfter = stockAfter;
	}
    
    public int getStockBefore() {
		return stockBefore;
	}
    
    public int getStockAfter() {
		return stockAfter;
	}

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public DetalleMovimiento getDetalleMovimiento(){
        return this.detalleMovimiento;
    }

    public void setDetalleMovimiento(DetalleMovimiento detalleMovimiento){
        this.detalleMovimiento = detalleMovimiento;
    }
}
