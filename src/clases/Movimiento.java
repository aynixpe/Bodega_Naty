/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.time.LocalDate;

/**
 *
 * @author Edder
 */
public class Movimiento {
    private int idMovimiento;
    private TipoMovimiento tipoMovimiento;
    private LocalDate fecha;
    private Almacen almacenOrigen;
    private Almacen almacenDestino;
    private char estado;

    public Movimiento() {
    }

    public Movimiento(int idMovimiento, TipoMovimiento tipoMovimiento, LocalDate fecha, Almacen almacenOrigen, Almacen almacenDestino, char estado) {
        this.idMovimiento = idMovimiento;
        this.tipoMovimiento = tipoMovimiento;
        this.fecha = fecha;
        this.almacenOrigen = almacenOrigen;
        this.almacenDestino = almacenDestino;
        this.estado = estado;
    }

    public int getIdMovimiento() {
        return idMovimiento;
    }

    public void setIdMovimiento(int idMovimiento) {
        this.idMovimiento = idMovimiento;
    }

    public TipoMovimiento getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(TipoMovimiento tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Almacen getAlmacenOrigen() {
        return almacenOrigen;
    }

    public void setAlmacenOrigen(Almacen almacenOrigen) {
        this.almacenOrigen = almacenOrigen;
    }

    public Almacen getAlmacenDestino() {
        return almacenDestino;
    }

    public void setAlmacenDestino(Almacen almacenDestino) {
        this.almacenDestino = almacenDestino;
    }
    
    public void setEstado(char estado) {
		this.estado = estado;
	}
    
    public char getEstado() {
		return estado;
	}
    
    @Override
    public String toString(){
        return this.tipoMovimiento.toString();
    }
}
