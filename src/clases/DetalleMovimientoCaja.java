/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Edder
 */
public class DetalleMovimientoCaja {
    private int idDetalleMovimientoCaja;
    private MovimientoCaja movimientoCaja;
    private float monto;
    private String descripcion;

    public DetalleMovimientoCaja() {
    }

    public DetalleMovimientoCaja(int idDetalleMovimientoCaja, MovimientoCaja movimientoCaja, float monto, String descripcion) {
        this.idDetalleMovimientoCaja = idDetalleMovimientoCaja;
        this.movimientoCaja = movimientoCaja;
        this.monto = monto;
        this.descripcion = descripcion;
    }

    public int getIdDetalleMovimientoCaja() {
        return idDetalleMovimientoCaja;
    }

    public void setIdDetalleMovimientoCaja(int idDetalleMovimientoCaja) {
        this.idDetalleMovimientoCaja = idDetalleMovimientoCaja;
    }

    public MovimientoCaja getMovimientoCaja() {
        return movimientoCaja;
    }

    public void setMovimientoCaja(MovimientoCaja movimientoCaja) {
        this.movimientoCaja = movimientoCaja;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Override
    public String toString(){
        return this.descripcion;
    }            
}
