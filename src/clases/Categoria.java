/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.ImageIcon;

/**
 *
 * @author edder
 */
public class Categoria {
    private int idCategoria;
    private String nombre;
    private File imagen;

    public Categoria(int idCategoria, String nombre, File imagen) {
        this(idCategoria, nombre);
        this.imagen = imagen;
    }
    
    public Categoria(int idCategoria, String nombre, InputStream imagen){
    	this(idCategoria, nombre);
    	if(imagen != null){
    		String x = System.getProperty("user.dir") + "/img/" + nombre + ".png";
        	File targetFile = new File(x);
            OutputStream outStream;
    		try {
    			outStream = new FileOutputStream(targetFile);
    			byte[] buffer = new byte[8 * 1024];
    	        int bytesRead; 
    	        while ((bytesRead = imagen.read(buffer)) != -1) {
    	            outStream.write(buffer, 0, bytesRead);
    	        }
    		} catch (FileNotFoundException e) {
    			e.printStackTrace();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
            this.imagen = targetFile;
    	}
    }
    
    public Categoria (int idCategoria, String nombre){
    	this.idCategoria = idCategoria;
    	this.nombre = nombre;
    }

    public Categoria() {
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public File getImagen(){
    	return imagen;
    }
    
    public void setImagen(File imagen){
    	this.imagen = imagen;
    }
    
    public ImageIcon getImage(){
    	if(imagen != null)
    		return new ImageIcon(imagen.getAbsolutePath());
    	return null;
    }
    
    @Override
    public String toString(){
        return this.nombre;
    }
}
