/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.Caja;
import clases.Comprobante;
import clases.MovimientoCaja;
import clases.Serie;
import clases.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class MdlComprobante{

    /**
     *
     * @param comprobante
     * @param Conexion
     * @throws SQLException
     */
    
    public static void insert(Comprobante comprobante) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO comprobante (idSerie, numero, nombreCliente, fecha, monto, igv, idMovimientoCaja, "
        		+ "idUsuario, estado, descripcion) "
                + "VALUES (" + comprobante.getSerie().getIdSerie() + ", "
                + comprobante.getNumero() + ", "
                + "'" + comprobante.getNombreCliente() + "', "
                + "'" + comprobante.getFecha().toString() + "', "
                + comprobante.getMonto() + ", "
                + comprobante.getIgv() + ", "
                + comprobante.getMovimientoCaja().getIdMovimientoCaja() + ", "
                + comprobante.getUsuario().getIdUsuario() + ", "
                + "'" + comprobante.getEstado() + "',"
        		+ "'" + comprobante.getDescripcion() + "');";
        Conexion.ejecutar(sql);
    }


    public static void setMovimiento(Comprobante objeto) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "UPDATE comprobante SET idMovimientoCaja = " + objeto.getMovimientoCaja().getIdMovimientoCaja() + " "
    			+ "WHERE idComprobante = " + objeto.getIdComprobante();
    	Conexion.ejecutar(sql);
    }

    /**
     *
     * @param Conexion
     * @return
     * @throws SQLException
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idComprobante) FROM comprobante;";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     *
     * @param comprobante
     * @param Conexion
     * @throws SQLException
     */
    
    public static void update(Comprobante comprobante) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE comprobante SET "
                + "serie = " + comprobante.getSerie().getIdSerie() + ", "
                + "numero = " + comprobante.getNumero() + ", "
                + "nombreCliente = '" + comprobante.getNombreCliente() + "', "
                + "fecha = '" + comprobante.getFecha().toString() + "', "
                + "monto = " + comprobante.getMonto() + ", "
                + "igv = " + comprobante.getIgv() + ", "
                + "usuario = " + comprobante.getUsuario().getIdUsuario() + ", "
                + "estado = '" + comprobante.getEstado() + "', "
                + "idMovimientoCaja = " + comprobante.getMovimientoCaja().getIdMovimientoCaja() + " "
                + "WHERE idComprobante = " + comprobante.getIdComprobante();
        Conexion.ejecutar(sql);
    }

    /*
	 * Devuelve el ultimo numero de la serie especificada
	 */
    public static int getLastNumero_serie(int idSerie) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String query = "SELECT MAX(numero) FROM comprobante where idserie = " + idSerie;
    	ResultSet rs = Conexion.consultar(query);
    	if (rs.next()) {
			return rs.getInt(1);
		}
    	return -1;
    }

    /**
     *
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException
     */
    
    public static ArrayList<Comprobante> get() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT * FROM comprobante;";
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<Comprobante> comprobantes = new ArrayList<>();
        while(rs.next()){
            comprobantes.add(new Comprobante(
                    rs.getInt(1),
                    new Serie(){{
                    	setIdSerie(rs.getInt(2));
                    }},
                    rs.getInt(3),
                    rs.getString(4),
                    LocalDate.parse(rs.getString(5)),
                    rs.getFloat(6),
                    rs.getFloat(7),
                    new Usuario(){{
                    	setIdUsuario(rs.getInt(8));
                    }},
                    new MovimientoCaja(){{
                    	setIdMovimientoCaja(rs.getInt("idMovimientoCaja"));
                    }},
                    rs.getString(9).charAt(0),
                    rs.getString("descripcion")
            ));
        }
        return comprobantes;
    }

    
    public static Comprobante get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM comprobante WHERE idComprobante = " + id;
        ResultSet rs = Conexion.consultar(sql);
        Comprobante comprobante = null;
        if (rs.next()) {
            comprobante = new Comprobante(
                    rs.getInt(1),
                    new Serie(){{
                    	setIdSerie(rs.getInt(2));
                    }},
                    rs.getInt(3),
                    rs.getString(4),
                    LocalDate.parse(rs.getString(5)),
                    rs.getFloat(6),
                    rs.getFloat(7),
                    new Usuario(){{
                    	setIdUsuario(rs.getInt(8));
                    }},
                    new MovimientoCaja(){{
                    	setIdMovimientoCaja(rs.getInt("idMovimientoCaja"));
                    }},
                    rs.getString(9).charAt(0),
                    rs.getString("descripcion")
            );
        }
        return comprobante;
    }


	public static ArrayList<Object[]> getConsulta(String fechaInicio, String fechaFin, String cliente, Caja caja)
			throws SQLException, ClassNotFoundException {
		Conexion.abrirTransaccion();
		String where = caja == null ? "" : "AND ca.idCaja = " + caja.getIdCaja();
		
		String sql = "SELECT c.idComprobante, tc.nombre as tipo, s.nombre as serie, "
				+ "LPAD(CAST(c.numero AS text), 5, CAST(0 AS text)) as numero, c.nombreCliente as cliente, "
				+ "c.monto, c.igv, c.fecha, u.nombre as usuario, c.estado, ca.nombre as caja "
				+ "FROM comprobante as c "
				+ "INNER JOIN usuario as u "
				+ "ON c.idUsuario = u.idUsuario "
				+ "INNER JOIN serie as s "
				+ "ON c.idSerie = s.idSerie "
				+ "INNER JOIN tipocomprobante as tc "
				+ "ON s.idTipoComprobante = tc.idTipoComprobante "
				+ "INNER JOIN movimientocaja as ma "
				+ "ON c.idMovimientoCaja = ma.idMovimientoCaja "
				+ "INNER JOIN caja as ca "
				+ "ON ma.idCajaDestino = ca.idCaja "
				+ "WHERE c.fecha >='"+fechaInicio+"' AND c.fecha <= '"+fechaFin+"' "
				+ "AND c.nombreCliente LIKE '%" + cliente + "%' " + where
				+ " ORDER BY c.fecha desc";
		ArrayList<Object[]> comprobantes = new ArrayList<>();
		ResultSet rs = Conexion.consultar(sql);
		while(rs.next()){
			comprobantes.add(new Object[]{
					rs.getInt("idComprobante"),
					rs.getString("tipo").charAt(0) + " " + rs.getString("serie") + "-" + rs.getString("numero"),
					rs.getString("cliente"),
					rs.getFloat("monto"),
					rs.getFloat("igv"),
					rs.getString("fecha"),
					rs.getString("usuario"),
					rs.getString("estado"),
					rs.getString("caja")
			});
		}
		return comprobantes;
	}

	public static void anularComprobante(int idComprobante) throws SQLException, ClassNotFoundException{
		Conexion.abrirTransaccion();
		String sql = "UPDATE comprobante SET "
    			+ "estado = 'A' "
    			+ "WHERE idComprobante = " + idComprobante;
    	Conexion.ejecutar(sql);
	}
}
