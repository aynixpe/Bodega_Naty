/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.Cliente;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author togapaulo
 */
public class MdlCliente{

    /**
     * 
     * @param cliente
     * @param Conexion
     * @throws SQLException 
     */
    public static void insert(Cliente cliente) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO Cliente(nombre, dni)"
                + "VALUES ('" + cliente.getNombre() + "','" + cliente.getDni() + "');";
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param Conexion
     * @return
     * @throws SQLException 
     */
    public static int getLastId() throws SQLException, ClassNotFoundException {
        String sql = "SELECT idCliente FROM Cliente ORDER BY idCliente DESC LIMIT 1";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     * 
     * @param cliente
     * @param Conexion
     * @throws SQLException 
     */
    public static void update(Cliente cliente) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE Cliente SET"
                + "nombre = '" + cliente.getNombre() + "' "
                + "dni = '" + cliente.getDni() + "' "
                + "WHERE idCliente = " + cliente.getIdCliente();
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException 
     */
    public static ArrayList<Cliente> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM cliente WHERE UPPER(nombre) LIKE UPPER('%" + query + "%');";
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<Cliente> clientes = new ArrayList<>();
        
        while(rs.next()){
            Cliente cliente;
            cliente = new Cliente(rs.getInt("idCliente"),
                    rs.getString("nombre"),
                    rs.getString("dni"));
            clientes.add(cliente);
        }
        return clientes;
    }

    /**
     * 
     * @param idCliente
     * @param Conexion
     * @return
     * @throws SQLException 
     */
    public static Cliente get(int idCliente) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM cliente WHERE idCliente = " + idCliente;
        ResultSet rs = Conexion.consultar(sql);
        Cliente cliente = null;
        
        if (rs.next()) {
            cliente = new Cliente(rs.getInt(1),rs.getString(2),rs.getString(3));
        }
        return cliente;
    }
    
    public static String getNombre(int idCliente) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT nombre FROM cliente WHERE idCliente = " + idCliente;
        ResultSet rs = Conexion.consultar(sql);        
        if (rs.next()) {
            return rs.getString("nombre");
        }
        return "";
    }    
}
