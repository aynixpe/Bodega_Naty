/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.OrdenCompra;
import clases.Proveedor;
import clases.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Edder
 */
public class MdlOrdenCompra{
    
    public static void insert(OrdenCompra objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO ordencompra(fecha, monto, fechaEntrega, idProveedor, idUsuario, estado) "
                + "VALUES('"
                + objeto.getFecha().toString() + "', "
                + objeto.getMonto() + ", '"
                + objeto.getFechaEntrega().toString() + "', "
                + objeto.getProveedor().getIdProveedor() + ", "
                + objeto.getUsuario().getIdUsuario() + ", '"
                + objeto.getEstado() + "');";
        Conexion.ejecutar(sql);
    }

    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT idordencompra from ordencompra order by idordencompra desc limit 1";
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return rs.getInt(1);
        return -1;
    }

    
    public static void update(OrdenCompra objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE ordencompra SET "
                + "fecha = '" + objeto.getFecha().toString() + "', "
                + "monto = " + objeto.getMonto() + ", "
                + "fechaEntrega = '" + objeto.getFechaEntrega().toString() + "', "
                + "idProveedor = " + objeto.getProveedor().getIdProveedor() + ", "
                + "idUsuario = " + objeto.getUsuario().getIdUsuario() + ", "
                + "estado = '" + objeto.getEstado() + "' "
                + "WHERE idOrdenCompra = " + objeto.getIdOrdenCompra();
        Conexion.ejecutar(sql);
    }

    
    public static ArrayList<OrdenCompra> get() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ArrayList<OrdenCompra> ordenCompras = new ArrayList<>();
        String sql = "SELECT * FROM ordencompra;";
        ResultSet rs = Conexion.consultar(sql);
        while(rs.next()){
            ordenCompras.add(new OrdenCompra(
                    rs.getInt("idOrdenCompra"),
                    LocalDate.parse(rs.getString("fecha").substring(0, 10)),
                    rs.getFloat("monto"),
                    LocalDate.parse(rs.getString("fechaEntrega").substring(0, 10)),
                    new Proveedor(){{
                    	setIdProveedor(rs.getInt("idProveedor"));
                    }},
                    new Usuario(){{
                    	setIdUsuario(rs.getInt("idUsuario"));
                    }},
                    rs.getString("estado").charAt(0))
            );
        }
        return ordenCompras;
    }

    
    public static OrdenCompra get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM ordencompra WHERE idOrdenCompra = " + id;
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return new OrdenCompra(
                    rs.getInt("idOrdenCompra"),
                    LocalDate.parse(rs.getString("fecha")),
                    rs.getFloat("monto"),
                    LocalDate.parse(rs.getString("fechaEntrega")),
                    new Proveedor(){{
                    	setIdProveedor(rs.getInt("idProveedor"));
                    }},
                    new Usuario(){{
                    	setIdUsuario(rs.getInt("idUsuario"));
                    }},
                    rs.getString("estado").charAt(0));
        return null;
    }

    public static Object[] lastOrdenCompra_Articulo(int id) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "select (if(MAX(oc.idOrdenCompra) is null,0,MAX(oc.idOrdenCompra))) as idOrdenCompra, doc.idArticulo, p.nombre, oc.monto "
    			+ "from ordencompra as oc "
    			+ "inner join "
    			+ "proveedor as p "
    			+ "on oc.idProveedor = p.idProveedor "
    			+ "inner join "
    			+ "detalleordencompra as doc "
    			+ "on oc.idOrdenCompra = doc.idOrdenCompra "
    			+ "where doc.idArticulo = " + id;
    	ResultSet rs = Conexion.consultar(sql);
    	if (rs.next()) {
			return new Object[]{
				rs.getInt("idOrdenCompra"),
				rs.getInt("idArticulo"),
				rs.getString("nombre"),
				rs.getFloat("monto")
			};
		} else {
			return null;
		}
    }

    public static void setEstado(int id) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "UPDATE ordencompra SET estado = 'P' WHERE idOrdenCompra = " + id;
    	Conexion.ejecutar(sql);
    }

    /*
     * Obtener ordenes para el formulario de consultar ordenes de compra
     */
    public static ArrayList<Object[]> getConsulta(
    		boolean tipoFecha, String fechaInicio, String fechaFin, Proveedor proveedor, boolean estado)
    				throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String where = "";
    	String fecha = "";

    	if (!estado)
			where = " AND oc.estado NOT LIKE 'A' ";

    	if(proveedor != null)
    		where += "AND oc.idProveedor = " + proveedor.getIdProveedor() + " ";
    	
    	fecha = (tipoFecha)? "oc.fecha" : "oc.fechaEntrega";

    	String sql = "SELECT oc.idOrdenCompra, p.nombre as proveedor, oc.monto, oc.fecha, oc.fechaEntrega, u.nombre as usuario, oc.estado "
				+ "FROM ordencompra as oc "
				+ "INNER JOIN proveedor as p "
				+ "ON oc.idProveedor = p.idProveedor "
				+ "INNER JOIN usuario as u "
				+ "ON oc.idUsuario = u.idUsuario "
				+ "WHERE " + fecha + " >='" + fechaInicio + "' AND " + fecha + " <= '" + fechaFin + "' " 
				+ where
				+ "ORDER BY " + fecha + " desc";
    	ArrayList<Object[]> ordenes = new ArrayList<>();
    	ResultSet rs = Conexion.consultar(sql);
    	while(rs.next()){
    		ordenes.add(new Object[]{
    				rs.getInt("idOrdenCompra"),
    				rs.getString("proveedor"),
    				rs.getFloat("monto"),
    				rs.getString("fecha"),
    				rs.getString("fechaEntrega"),
    				rs.getString("usuario"),
    				rs.getString("estado")
    		});
    	}
    	return ordenes;
    }

    public static void anularOrdenCompra(int idOrdenCompra) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "UPDATE ordencompra SET "
    			+ "estado = 'A' "
    			+ "WHERE idOrdenCompra = " + idOrdenCompra + " "
    			+ "AND estado NOT LIKE 'A'";
    	Conexion.ejecutar(sql);
    }
}
