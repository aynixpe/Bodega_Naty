package modelo;

import clases.Almacen;
import clases.AlmacenArticulo;
import clases.Articulo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Modelo de Almacen - Articulo
 * @author Edder
 * @version 1.0
 */
public class MdlAlmacenArticulo{

    /**
     * Inserta stock y stock mínimo para cada artículo en cada almacén
     * @param objeto AlmacenArticulo sin ID
     * @param trans indica si es una transaccion
     * @throws SQLException
     */
    public static void insert(AlmacenArticulo objeto)
    		throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO almacenarticulo(idAlmacen, idArticulo, stock, stockMinimo) VALUES("
                + objeto.getAlmacen().getIdAlmacen() + ", "
                + objeto.getArticulo().getIdArticulo() + ", "
                + objeto.getStock() + ", "
                + objeto.getStockMinimo() + ");";
        Conexion.ejecutar(sql);
    }

    /**
     * Obtiene el último ID insertado
     * @param cnx Conexión previamente abierta
     * @return Último ID
     * @throws SQLException
     */
    public static int getLastId() throws SQLException, ClassNotFoundException {
        Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idAlmacenArticulo) FROM almacenarticulo";
        ResultSet rs = Conexion.consultar(sql);
        int id = -1;
        if(rs.next())
            id = rs.getInt(1);
        return id;
    }

    /**
     * Asigna stock y stock mínimo
     * @param objeto Almacen articulo (con ID)
     * @param cnx Conexión previamente abierta
     * @throws SQLException
     */
    public static void update(AlmacenArticulo objeto) throws SQLException, ClassNotFoundException {
        Conexion.abrirTransaccion();
        String sql = "UPDATE almacenarticulo SET "
                + "stock = " + objeto.getStock() + ", "
                + "stockMinimo = " + objeto.getStockMinimo() + " "
                + "WHERE idAlmacenArticulo = " + objeto.getIdAlmacenArticulo();
        Conexion.ejecutar(sql);
    }

    /**
     * Asigna stock
     * @param idAlmacenArticulo
     * @param variacion
     * @param cnx Conexión previamente abierta
     * @throws SQLException
     */
    public static void updateStock(int idAlmacenArticulo, int variacion)
    		throws SQLException, ClassNotFoundException {
        Conexion.abrirTransaccion();
        String sql = "UPDATE almacenarticulo SET "
                + "stock = stock + " + variacion + " "
                + "WHERE idAlmacenArticulo = " + idAlmacenArticulo;
        Conexion.ejecutar(sql);
    }

    public static ArrayList<Object[]> getStockforArticuloInAlmacen(int idArticulo)
            throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        ArrayList<Object[]> almacenstocks = new ArrayList<Object[]>();
    	String sql = "SELECT  a.nombre, aa.stock "
                + "FROM almacenarticulo as aa "
                + "INNER JOIN almacen as a "
                + "ON aa.idalmacen = a.idalmacen "
                + "WHERE idArticulo = " + idArticulo;
        ResultSet rs = Conexion.consultar(sql);
        while(rs.next()){
        	almacenstocks.add(new Object[]{
        			rs.getString("nombre"),
        			rs.getInt("stock")
        	});
        }
        return almacenstocks;
    }

    /**
     *
     * @param idAlmacen
     * @param cnx
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public static ArrayList<AlmacenArticulo> getAlmacenArticulos_almacen(int idAlmacen)
    		throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "SELECT * "
                + "FROM almacenarticulo "
                + "WHERE idAlmacen = " + idAlmacen;
        return MdlAlmacenArticulo.get(sql);
    }

    /**
     * Retorna lista de AlmacenArticulos
     * @param sql
     * @param cnx
     * @return
     * @throws SQLException
     */
    private static ArrayList<AlmacenArticulo> get(String sql) throws SQLException, ClassNotFoundException {
        Conexion.abrirTransaccion();
        ArrayList<AlmacenArticulo> almacenArticulos = new ArrayList<>();
        ResultSet rs = Conexion.consultar(sql);
        while(rs.next()){
            almacenArticulos.add(new AlmacenArticulo(
                    rs.getInt("idAlmacenArticulo"),
                    new Almacen(){{this.setIdAlmacen(rs.getInt("idAlmacen"));}},
                    new Articulo(){{this.setIdArticulo(rs.getInt("idArticulo"));}},
                    rs.getInt("stock"),
                    rs.getInt("stockMinimo")));
        }
        return almacenArticulos;
    }

    public static AlmacenArticulo get(int id) throws SQLException, ClassNotFoundException {
        Conexion.abrirTransaccion();
        String sql = "SELECT * FROM almacenarticulo WHERE idAlmacenArticulo = " + id;
        ResultSet rs = Conexion.consultar(sql);
        AlmacenArticulo almacenArticulo = null;
        if(rs.next())
            almacenArticulo = new AlmacenArticulo(
                    rs.getInt("idAlmacenArticulo"),
                    new Almacen(){{this.setIdAlmacen(rs.getInt("idAlmacen"));}},
                    new Articulo(){{this.setIdArticulo(rs.getInt("idArticulo"));}},
                    rs.getInt("stock"),
                    rs.getInt("stockMinimo"));
        return almacenArticulo;
    }

    public static AlmacenArticulo get(int idAlmacen, int idArticulo)
            throws SQLException, ClassNotFoundException {
        Conexion.abrirTransaccion();
        String sql = "SELECT * FROM almacenarticulo WHERE idAlmacen = " + idAlmacen + " and idArticulo = " + idArticulo;
        ResultSet rs = Conexion.consultar(sql);
        AlmacenArticulo almacenArticulo = null;
        if(rs.next())
            almacenArticulo = new AlmacenArticulo(
                    rs.getInt("idAlmacenArticulo"),
                    new Almacen(){{this.setIdAlmacen(rs.getInt("idAlmacen"));}},
                    new Articulo(){{this.setIdArticulo(rs.getInt("idArticulo"));}},
                    rs.getInt("stock"),
                    rs.getInt("stockMinimo"));
        return almacenArticulo;
    }


    public static int getIdArticulo(int idAlmacenArticulo) throws SQLException, ClassNotFoundException{
        Conexion.abrirTransaccion();
        String sql = "SELECT idArticulo FROM almacenarticulo WHERE idAlmacenArticulo = " + idAlmacenArticulo + ";";
        ResultSet rs = Conexion.consultar(sql);
        int id = -1;
        if(rs.next())
            id = rs.getInt("idArticulo");
        return id;
    }

}
