package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Edder
 */
public class Conexion {
    public static String driver;
    public static String connectString;
    private static String user;
    private static String pass;
    public static Connection cnx;
    
    public static final int MYSQL = 1;
    public static final int POSTGRESQL = 2;
    public static final int SQLITE = 3;
    
    public static void setDefaultConexion(String host, int puerto, String baseDatos,
            String user, String pass, int gestor){        
        switch(gestor){
            case MYSQL:
                Conexion.driver = "com.mysql.jdbc.Driver";
                Conexion.connectString = "jdbc:mysql://" + host + ":" + puerto;
                Conexion.connectString += "/" + baseDatos;
                break;                
            case POSTGRESQL:
            	Conexion.driver = "org.postgresql.Driver";
            	Conexion.connectString = "jdbc:postgresql://" + host + ":" + puerto;
            	Conexion.connectString += "/" + baseDatos;
                break;
            case SQLITE:
            	Conexion.driver = "org.sqlite.JDBC";
            	Conexion.connectString = "jdbc:sqlite:" + baseDatos;
                break;
        }
        
        Conexion.user = user;
        Conexion.pass = pass;
        Conexion.cnx = null;
    }
    
    public static void abrirConexion() throws ClassNotFoundException, SQLException{
    	if(Conexion.cnx == null || Conexion.cnx.isClosed()){
    		Class.forName(Conexion.driver);
    		Conexion.cnx = DriverManager.getConnection(Conexion.connectString, Conexion.user,
    				Conexion.pass);
    	}
    }
    
    public static void cerrarConexion() throws SQLException{
        Conexion.cnx.close();        
    }
    
    public static ResultSet consultar(String sql) throws SQLException{
        Statement stmt = Conexion.cnx.createStatement();
        return stmt.executeQuery(sql);
    }
    
    public static ResultSet conectarConsultar(String sql) throws ClassNotFoundException, SQLException{
        Conexion.abrirConexion();
        return Conexion.consultar(sql);
    }
    
    public static void ejecutar(String sql) throws SQLException{
        Statement stmt = Conexion.cnx.createStatement();
        stmt.execute(sql);
    }
    
    public static void conectarEjecutar(String sql) throws ClassNotFoundException, SQLException{
        Conexion.abrirConexion();
        Conexion.ejecutar(sql);
    }
    
    public static void abrirTransaccion() throws SQLException, ClassNotFoundException{
    	Conexion.abrirConexion();
    	Conexion.cnx.setAutoCommit(false);
    }
    
    public static void cerrarTransaccion() throws SQLException{
    	Conexion.cnx.commit();
    	Conexion.cnx.setAutoCommit(true);
    }
    
    public static void rollback() throws SQLException{
    	Conexion.cnx.rollback();
    	Conexion.cnx.setAutoCommit(true);
    	Conexion.cnx.close();
    }
    
    public static void evaluateTrans(boolean trans) throws SQLException{
    	if(!trans){
        	Conexion.cerrarTransaccion();
        	Conexion.cerrarConexion();
        }
    }
}