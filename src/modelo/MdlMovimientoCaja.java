package modelo;

import clases.Caja;
import clases.MovimientoCaja;
import clases.TipoMovimientoCaja;
import clases.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Edder
 */
public class MdlMovimientoCaja{

    /**
     *
     * @param movimientoCaja
     * @param Conexion
     * @throws SQLException, ClassNotFoundException
     */
    
    public static void insert(MovimientoCaja movimientoCaja) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "";    	
    	String campos = "";
    	String values = "";
    	
    	if(movimientoCaja.getCajaOrigen() != null){
    		campos += ", idCajaOrigen";
    		values += ", " + movimientoCaja.getCajaOrigen().getIdCaja();
    	}
    	if(movimientoCaja.getCajaDestino() != null){
    		campos += ", idCajaDestino";
    		values += ", " + movimientoCaja.getCajaDestino().getIdCaja();
    	}    	
    	sql = "INSERT INTO movimientocaja (idTipoMovimientoCaja, referencia, fecha, monto, idUsuario, estado " + campos + ") "
                + "VALUES (" + movimientoCaja.getTipoMovimientoCaja().getIdTipoMovimientoCaja()+", "
                + movimientoCaja.getReferencia() + ", "
                + "'" + movimientoCaja.getFecha().toString() + "', "
                + movimientoCaja.getMonto() + ", "
                + movimientoCaja.getUsuario().getIdUsuario() + ", "
                + "'" + movimientoCaja.getEstado() + "'"
                + values + ");";
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idMovimientoCaja) FROM movimientocaja";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     *
     * @param movimientoCaja
     * @param Conexion
     * @throws SQLException, ClassNotFoundException
     */
    
    public static void update(MovimientoCaja movimientoCaja) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	/**
    	 * Evaluar, caja origen o destino pueden ser null
    	 */
        String sql = "UPDATE movimientocaja SET "
                + "idTipoMovimientoCaja = " + movimientoCaja.getTipoMovimientoCaja().getIdTipoMovimientoCaja()+ ", "
                + "referencia = " + movimientoCaja.getReferencia() + ", "
                + "fecha = '" + movimientoCaja.getFecha().toString() + "', "
                + "monto = " + movimientoCaja.getMonto() + ", "
                + "idUsuario = " + movimientoCaja.getUsuario().getIdUsuario() + ", "
                + "estado = '" + movimientoCaja.getEstado() + "', "
                + "idCajaOrigen = " + movimientoCaja.getCajaOrigen().getIdCaja() + ", "
                + "idCajaDestino = " + movimientoCaja.getCajaDestino().getIdCaja() + " "
                + "WHERE idMovimientoCaja = " + movimientoCaja.getIdMovimientoCaja() + ";";
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static ArrayList<MovimientoCaja> get() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT * FROM movimientocaja;";
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<MovimientoCaja> movimientos;
        movimientos = new ArrayList<>();
        while(rs.next()){
            movimientos.add(new MovimientoCaja(
            		rs.getInt(1),
            		new TipoMovimientoCaja(){{
            			setIdTipoMovimientoCaja(rs.getInt("idTipoMovimientoCaja"));
            		}},
            		rs.getInt(3),
            		LocalDate.parse(rs.getString(4)),
            		rs.getFloat(5),
            		new Usuario(){{
            			setIdUsuario(rs.getInt(6));
            		}},
            		rs.getString(7).charAt(0),
            		new Caja(){{
            			setIdCaja(rs.getInt("idCajaOrigen"));
            		}},
            		new Caja(){{
            			setIdCaja(rs.getInt("idCajaDestino"));
            		}}
            		)
            );
        }
        return movimientos;
    }

    /**
     *
     * @param idMovimientoCaja
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static MovimientoCaja get(int idMovimientoCaja) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM movimientocaja WHERE idMovimientoCaja =" + idMovimientoCaja;
        ResultSet rs = Conexion.consultar(sql);
        MovimientoCaja movimientocaja = null;
        if (rs.next()) {
            movimientocaja = new MovimientoCaja(
            		rs.getInt(1),
            		new TipoMovimientoCaja(){{
            			setIdTipoMovimientoCaja(rs.getInt("idTipoMovimientoCaja"));
            		}},
            		rs.getInt(3),
            		LocalDate.parse(rs.getString(4)),
            		rs.getFloat(5),
            		new Usuario(){{
            			setIdUsuario(rs.getInt(6));
            		}},
            		rs.getString(7).charAt(0),
            		new Caja(){{
            			setIdCaja(rs.getInt("idCajaOrigen"));
            		}},
            		new Caja(){{
            			setIdCaja(rs.getInt("idCajaDestino"));
            		}}
            );
        }
        return movimientocaja;
    }

    public static ArrayList<Object[]> getConsulta(
    		String fechaInicio, String fechaFin, Caja caja, TipoMovimientoCaja tipomovimientocaja, boolean estado) 
    				throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	ArrayList<Object[]> row = new ArrayList<Object[]>();
    	String where = "";

    	if(!estado)
    		where = " AND mc.estado NOT LIKE 'A' ";

    	if(caja != null)
    		where += " AND (mc.idcajaorigen = " + caja.getIdCaja() + " "
    				+ "OR mc.idcajadestino = " + caja.getIdCaja() + ") ";
    	
    	if(tipomovimientocaja != null)
    		where += " AND mc.idtipomovimientocaja = " + tipomovimientocaja.getIdTipoMovimientoCaja();
    	
    	String sql = "SELECT mc.idmovimientocaja, tmc.descripcion as tipomovimientocaja, c0.nombre as origen, c1.nombre as destino, mc.fecha "
    			+ "FROM movimientocaja as mc "
    			+ "INNER JOIN tipomovimientocaja as tmc ON mc.idtipomovimientocaja = tmc.idtipomovimientocaja "
    			+ "LEFT JOIN caja as c0 ON mc.idcajaorigen = c0.idcaja "
    			+ "LEFT JOIN caja as c1 ON mc.idcajadestino = c1.idcaja "
    			+ "WHERE "
    			+ "mc.fecha >='"+fechaInicio+"' AND mc.fecha <= '"+fechaFin+"' "
    			+ where
    			+ "ORDER BY mc.fecha desc";
    	ResultSet rs = Conexion.consultar(sql);
    	while(rs.next()){
    		row.add(new Object[]{
        			rs.getInt("idMovimientoCaja"),
        			rs.getString("tipomovimientocaja"),
        			rs.getString("origen"),
        			rs.getString("destino"),
        			rs.getString("fecha")
        	});
    	}
    	return row;
    }

    public static void anularMovimientoCaja(int idMovimientoCaja) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "UPDATE movimientocaja SET "
    			+ "estado = 'A' "
    			+ "WHERE idMovimientoCaja = " + idMovimientoCaja;
    	Conexion.ejecutar(sql);
    }

    public static boolean movimientoCajaComprobante(int idMovimientoCaja) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
		String sql = "SELECT COUNT(idComprobante) as n "
				+ "FROM comprobante "
				+ "WHERE estado NOT LIKE 'A' "
				+ "AND idMovimientoCaja = " + idMovimientoCaja;
		ResultSet rs = Conexion.consultar(sql);
		if (rs.next()) {
			if(rs.getInt("n") == 0) return false;
		}
		return true;
    }

    public static boolean movimientoCajaCompra(int idMovimientoCaja) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
		String sql = "SELECT COUNT(idCompra) as n "
				+ "FROM compra "
				+ "WHERE estado NOT LIKE 'A' "
				+ "AND idMovimientoCaja = " + idMovimientoCaja;
		ResultSet rs = Conexion.consultar(sql);
		if (rs.next()) {
			if(rs.getInt("n") == 0) return false;
		}
		return true;
    }
}