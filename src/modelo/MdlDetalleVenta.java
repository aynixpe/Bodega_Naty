/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.AlmacenArticulo;
import clases.DetalleComprobante;
import clases.DetalleVenta;
import clases.Venta;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class MdlDetalleVenta{

    /**
     *
     * @param detalleventa
     * @param Conexion
     * @throws SQLException
     */
    
    public static void insert(DetalleVenta detalleventa) throws SQLException, ClassNotFoundException {
    	Conexion.abrirConexion();
        String sql = "INSERT INTO detalleventa(idVenta, idAlmacenArticulo, cantidad, precio, descuento) "
                + "VALUES (" + detalleventa.getVenta().getIdVenta() + ", "
                + detalleventa.getAlmacenArticulo().getIdAlmacenArticulo() + ", "
                + detalleventa.getCantidad() + ", "
                + detalleventa.getPrecio() + ", "
                + detalleventa.getDescuento() + ");";
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param Conexion
     * @return
     * @throws SQLException
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idDetalleVenta) FROM detalleventa;";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     *
     * @param detalleventa
     * @param Conexion
     * @throws SQLException
     */
    
    public static void update(DetalleVenta detalleventa) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE detalleVenta SET "
                + "venta = " + detalleventa.getVenta().getIdVenta() + ", "
                + "almacenArticulo = " + detalleventa.getAlmacenArticulo().getIdAlmacenArticulo() + ", "
                + "cantidad = " + detalleventa.getCantidad() + ", "
                + "precio = " + detalleventa.getPrecio() + ", "
                + "descuento = " + detalleventa.getDescuento() + " "
                + "WHERE idDetalleVenta = " + detalleventa.getIdDetalleVenta();
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException
     */
    
    public static ArrayList<DetalleVenta> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ResultSet rs = Conexion.consultar(query);
        ArrayList<DetalleVenta> detalles = new ArrayList<>();
        while(rs.next()){
            detalles.add(new DetalleVenta(
                    rs.getInt(1),
                    new Venta(){{
                    	setIdVenta(rs.getInt(2));
                    }},
                    new AlmacenArticulo(){{
                    	setIdAlmacenArticulo(rs.getInt(3));
                    }},
                    rs.getInt(4),
                    rs.getFloat(5),
                    rs.getFloat(6),
                    new DetalleComprobante(){{
                    	setIdDetalleComprobante(rs.getInt("iddetallecomprobante"));
                    }}
            ));
        }
        return detalles;
    }

    /**
     *
     * @param id
     * @param Conexion
     * @return
     * @throws SQLException
     */
    
    public static DetalleVenta get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM detalleVenta WHERE idDetalleVenta LIKE " + id;
        ResultSet rs = Conexion.consultar(sql);
        DetalleVenta detalleventa = null;
        if (true) {
            detalleventa = new DetalleVenta(
                    rs.getInt(rs.getInt(1)),
                    new Venta(){{
                    	setIdVenta(rs.getInt(2));
                    }},
                    new AlmacenArticulo(){{
                    	setIdAlmacenArticulo(rs.getInt(3));
                    }},
                    rs.getInt(4),
                    rs.getFloat(5),
                    rs.getFloat(6),
                    new DetalleComprobante(){{
                    	setIdDetalleComprobante(rs.getInt("detalleComprobante"));
                    }}
            );
        }
        return detalleventa;
    }
    
    public static void setDetalleComprobante(DetalleVenta detalleVenta) throws ClassNotFoundException, SQLException{
    	Conexion.abrirTransaccion();
    	String sql = "UPDATE detalleventa set iddetallecomprobante = "
    			+ detalleVenta.getDetalleComprobante().getIdDetalleComprobante() + " "
				+ "WHERE iddetalleventa = " + detalleVenta.getIdDetalleVenta();
    	Conexion.ejecutar(sql);
    }

	public static ArrayList<Object[]> getConsulta_Detalle(int idVenta) throws SQLException, ClassNotFoundException {
		Conexion.abrirTransaccion();
		String sql = "SELECT dv.idDetalleVenta, "
				+ "CONCAT(a.nombre, ' ', a.presentacion, ' ', a.tamanio, ' ', a.unidad) as articulo, dv.cantidad, dv.precio, dv.descuento "
				+ "FROM detalleventa as dv "
				+ "INNER JOIN almacenarticulo as aa "
				+ "ON dv.idAlmacenArticulo = aa.idAlmacenArticulo "
				+ "INNER JOIN articulo as a "
				+ "ON aa.idArticulo = a.idArticulo "
				+ "WHERE dv.idVenta = "+ idVenta;
		ArrayList<Object[]> detalles = new ArrayList<>();
		ResultSet rs = Conexion.consultar(sql);
		while(rs.next()){
			detalles.add(new Object[]{
				rs.getInt("idDetalleVenta"),
				rs.getString("articulo"),
				rs.getInt("cantidad"),
				rs.getFloat("precio"),
				rs.getFloat("descuento")
			});
		}
		return detalles;
	}

	public static ArrayList<DetalleVenta> getDetalles(int idVenta) throws SQLException, ClassNotFoundException {
		Conexion.abrirTransaccion();
        String sql = "SELECT * FROM detalleventa WHERE idVenta = " + idVenta;
        ArrayList<DetalleVenta> detalles = new ArrayList<>();
        detalles = get(sql);
        return detalles;
    }
}
