/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.Caja;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Edder
 */
public class MdlCaja{

    /**
     *
     * @param caja
     * @param cnx
     * @throws SQLException
     */
    
    public static void insert(Caja caja) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO caja(nombre) "
                + "VALUES ('"+ caja.getNombre() +"');";
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param cnx
     * @return
     * @throws SQLException
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT max(idCaja) FROM caja";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    public static void updateMonto(int idCaja, float variacion) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "UPDATE caja SET monto = monto + " + variacion + " WHERE idCaja = " + idCaja;
    	Conexion.ejecutar(sql);
    }

    /**
     *
     * @param caja
     * @param cnx
     * @throws SQLException
     */
    
    public static void update(Caja caja) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE caja SET "
                + "nombre = '" + caja.getNombre() +"' "
                + "monto = " + caja.getMonto() +" "
                + "WHERE idCaja = " + caja.getIdCaja() + ";";
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param query
     * @param cnx
     * @return
     * @throws SQLException
     */
    
    public static ArrayList<Caja> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT * FROM caja WHERE UPPER(nombre) LIKE UPPER('%" + query + "%');";
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<Caja> cajas;
        cajas = new ArrayList<>();
        while(rs.next()){
            cajas.add(new Caja(rs.getInt(1), rs.getString(2), rs.getFloat(3)));
        }        
        return cajas;
    }

    /**
     *
     * @param idCaja
     * @param cnx
     * @return
     * @throws SQLException
     */
    
    public static Caja get(int idCaja) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM caja WHERE idCaja = " + idCaja;
        ResultSet rs = Conexion.consultar(sql);
        Caja caja = null;
        if (rs.next()) {
            caja = new Caja(rs.getInt(1), rs.getString(2), rs.getFloat(3));
        }
        return caja;
    }

}
