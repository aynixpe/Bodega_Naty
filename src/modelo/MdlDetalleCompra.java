package modelo;

import clases.AlmacenArticulo;
import clases.Compra;
import clases.DetalleCompra;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Edder
 */
public class MdlDetalleCompra{
    
    public static void insert(DetalleCompra objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO detallecompra(idCompra, idAlmacenArticulo, cantidad, precio, total, igv) "
                + "VALUES(" + objeto.getCompra().getIdCompra() + ", "
                + objeto.getAlmacenArticulo().getIdAlmacenArticulo() + ", "
                + objeto.getCantidad() + ", "
                + objeto.getPrecio() + ", "
                + objeto.getTotal() + ", "
                + objeto.getIgv() + " );";
        Conexion.ejecutar(sql);
    }

    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idDetalleCompra) FROM detallecompra";
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return rs.getInt(1);
        return -1;
    }

    
    public static void update(DetalleCompra objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE detallecompra SET "
                + "idCompra = , "
                + "idAlmacenArticulo = , "
                + "cantidad = , "
                + "precio = , "
                + "total = ,"
                + "igv = "
                + "WHERE idDetalleCompra = " + objeto.getIdDetalleCompra();
        Conexion.ejecutar(sql);
    }
    
    public static ArrayList<DetalleCompra> getDetalleCompras_compra(int idCompra) 
    		throws SQLException, ClassNotFoundException{
        String sql = "SELECT * FROM detallecompra WHERE idCompra = " + idCompra;
        return MdlDetalleCompra.get(sql);        
    }

    
    public static ArrayList<DetalleCompra> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ArrayList<DetalleCompra> detallecompras = new ArrayList<>();
        ResultSet rs = Conexion.consultar(query);
        while(rs.next()){
            detallecompras.add(new DetalleCompra(
                    rs.getInt(rs.getInt("idDetalleCompra")),
                    new Compra(){{
                    	setIdCompra(rs.getInt("idCompra"));
                    }},
                    new AlmacenArticulo(){{
                    	setIdAlmacenArticulo(rs.getInt("idAlmacenArticulo"));
                    }},
                    rs.getInt("cantidad"),
                    rs.getFloat("precio"),
                    rs.getFloat("total"),
                    rs.getFloat("igv"))
            );
        }
        return detallecompras;
    }

    
    public static DetalleCompra get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM detallecompra where idDetalleCompra = " + id;
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return new DetalleCompra(
                    rs.getInt(rs.getInt("idDetalleCompra")),
                    new Compra(){{
                    	setIdCompra(rs.getInt("idCompra"));
                    }},
                    new AlmacenArticulo(){{
                    	setIdAlmacenArticulo(rs.getInt("idAlmacenArticulo"));
                    }},
                    rs.getInt("cantidad"),
                    rs.getFloat("precio"),
                    rs.getFloat("total"),
                    rs.getFloat("igv"));
        return null;
    }
    
    public static ArrayList<Object[]> getConsulta_Detalle(int idCompra) 
    		throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "SELECT dc.idDetalleCompra, "
    			+ "CONCAT(a.nombre, ' ', a.presentacion, ' ', a.tamanio, ' ', a.unidad)as articulo, dc.cantidad, dc.precio, dc.total, dc.igv "
				+ "FROM detallecompra as dc "
				+ "INNER JOIN almacenarticulo as aa "
				+ "ON dc.idAlmacenArticulo = aa.idAlmacenArticulo "
				+ "INNER JOIN articulo as a "
				+ "ON aa.idArticulo = a.idArticulo "
				+ "WHERE dc.idCompra = "+ idCompra;
    	ArrayList<Object[]> detalles = new ArrayList<>();
    	ResultSet rs = Conexion.consultar(sql);
    	while(rs.next()){
    		detalles.add(new Object[]{
    				rs.getInt("idDetalleCompra"),
    				rs.getString("articulo"),
    				rs.getInt("cantidad"),
    				rs.getFloat("precio"),
    				rs.getFloat("total"),
    				rs.getFloat("igv")
    		});
    	}
    	return detalles;
    }
    
    public static float lastCost(int idAlmacen, int idArticulo) throws ClassNotFoundException, SQLException{
    	Conexion.abrirTransaccion();
    	String sql = "select dc.precio as costo "
    			+ "from detallecompra as dc "
    			+ "inner join almacenarticulo as aa "
    			+ "on dc.idalmacenarticulo = aa.idalmacenarticulo "
    			+ "where aa.idalmacen = " + idAlmacen + " "
				+ "and aa.idarticulo = " + idArticulo + " "
    			+ "order by dc.iddetallecompra desc;";
    	ResultSet rs = Conexion.consultar(sql);
    	float costo = 0f;
    	if(rs.next()){
    		try{
    			costo = rs.getFloat("costo");
    		}catch(Exception e){
				costo = 0f;
			}
    	}
    	return costo;
    	
    }
}