package modelo;

import clases.TipoUsuario;
import clases.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class MdlUsuario{

    /**
     * 
     * @param usuario
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */    
    public static void insert(Usuario usuario) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO usuario(tipoUsuario, nombre) "
                + "VALUES(" + usuario.getTipoUsuario().getIdTipoUsuario() + ", "
                + "'" + usuario.getNombre() + "');";
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idUsuario) FROM usuario;";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     * 
     * @param usuario
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */    
    public static void update(Usuario usuario) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE usuario SET "
                + "tipoUsuario = " + usuario.getTipoUsuario().getIdTipoUsuario() + ", "
                + "nombre = '" + usuario.getNombre() + "' "
                + "WHERE idUsuario = " + usuario.getIdUsuario();
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */    
    public static ArrayList<Usuario> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ResultSet rs = Conexion.consultar(query);
        ArrayList<Usuario> usuarios;
        usuarios = new ArrayList<Usuario>();
        while(rs.next()){
            usuarios.add(new Usuario(
                    rs.getInt(1),
                    new TipoUsuario(){{
                    	setIdTipoUsuario(rs.getInt(2));
                    }},
                    rs.getString(3)
            ));
        }
        return usuarios;
    }
    
    public static ArrayList<Usuario> getUsuarios(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT * FROM usuario WHERE UPPER(usuario) LIKE UPPER('%"+ query +"%');";
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<Usuario> usuarios;
        usuarios = new ArrayList<Usuario>();
        while(rs.next()){
            usuarios.add(new Usuario(
                    rs.getInt(1),
                    new TipoUsuario(){{
                    	setIdTipoUsuario(rs.getInt(2));
                    }},
                    rs.getString(3)
            ));
        }
        return usuarios;
    }
    
    /**
     * Devuelve un usuario, si usuario y contraseña coinciden
     * @param user nombre de usuario
     * @param pass contraseña
     * @param Conexion conexión
     * @return Usuario, sin tipoAdmin
     * @throws SQLException, ClassNotFoundException
     */
    public static Usuario get(String user, String pass) throws SQLException, ClassNotFoundException{
    	String sql = "SELECT * FROM usuario WHERE UPPER(usuario) LIKE UPPER('"
    			+ user + "') AND password LIKE '" + pass + "'";
    	ArrayList<Usuario> result = MdlUsuario.get(sql);    	
    	return result.size() > 0 ? result.get(0) : null;
    }

    /**
     * 
     * @param id
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */    
    public static Usuario get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM usuario WHERE idUsuario = " + id;
        ResultSet rs = Conexion.consultar(sql);
        Usuario usuario = null;
        if (rs.next()) {
            usuario = new Usuario(
                    rs.getInt(1),
                    new TipoUsuario(){{
                    	setIdTipoUsuario(rs.getInt(2));
                    }},
                    rs.getString(3)
            );
        }
        return usuario;
    }

	public static int get_Permiso(int permiso, int idusuario) throws SQLException, ClassNotFoundException {
		Conexion.abrirTransaccion();
		String tabla = MdlUsuario.getTabla(permiso);
		
		String sql = "SELECT " + tabla + " "
				+ "FROM tipousuario as tu "
				+ "INNER JOIN usuario as u "
				+ "ON tu.idTipoUsuario = u.idTipoUsuario "
				+ "WHERE u.idUsuario = " + idusuario;
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return 0;
	}
	
	public static String getTabla(int permiso){
		String tabla = "";
		switch(permiso){
	        case TipoUsuario.MOVIMIENTO:
	            tabla = "tu.p_movimiento";
	            break;
	        case TipoUsuario.VENTA:
	            tabla = "tu.p_venta";
	            break;
	        case TipoUsuario.COMPROBANTE:
	        	tabla = "tu.p_comprobante";
	        	break;
	        case TipoUsuario.ORDEN_COMPRA:
	        	tabla = "tu.p_ordenCompra";
	        	break;
	        case TipoUsuario.COMPRA:
	        	tabla = "tu.p_venta";
	        	break;
	        case TipoUsuario.MOVIMIENTO_CAJA:
	        	tabla = "tu.p_detalleMovimientoCaja";
	        	break;
	        default:
	            tabla = "";
	            break;
		}
		return tabla;
	}
	
	public static ArrayList<Object[]> getUsers() throws SQLException, ClassNotFoundException{
		Conexion.abrirTransaccion();
		ArrayList<Object[]> usuarios = new ArrayList<Object[]>();
		String sql = "select u.idusuario, u.nombre, u.usuario, tu.nombre as tipousuario"
				+ "from usuario as u "
				+ "inner join tipousuario as tu "
				+ "on u.idtipousuario = tu.idtipousuario";
		ResultSet rs = Conexion.consultar(sql);
		while(rs.next()){
			usuarios.add(new Object[]{
				rs.getInt("idusuario"),
				rs.getString("nombre"),
				rs.getString("usuario"),
				rs.getString("tipousuario")
			});
		}
		return usuarios;
	}
    
}
