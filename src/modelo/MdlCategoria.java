/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import clases.Categoria;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 *
 * @author edder
 */
public class MdlCategoria{
    /**
     *
     * @param categoria
     * @param cnx
     * @throws SQLException
     * @throws FileNotFoundException
     */
    public static void insert(Categoria categoria) throws SQLException, FileNotFoundException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "";
    	PreparedStatement ps;
    	if(categoria.getImagen() != null){
            sql = "INSERT INTO categoria(nombre, imagen) "
                    + "VALUES(?, ?);";
            FileInputStream fis = new FileInputStream(categoria.getImagen());
            ps = Conexion.cnx.prepareStatement(sql);
            ps.setString(1, categoria.getNombre());
            ps.setBinaryStream(2, fis, categoria.getImagen().length());
       }else{        
            sql = "INSERT INTO categoria(nombre) "
                    + "VALUES(?);";
            ps = Conexion.cnx.prepareStatement(sql);
            ps.setString(1, categoria.getNombre());
          }
          ps.executeUpdate();
    }


    /**
     *
     * @param cnx
     * @return
     * @throws SQLException
     */
    public static int getLastId() throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "SELECT idCategoria FROM categoria ORDER BY idCategoria DESC LIMIT 1;";
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next()){
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     *
     * @param categoria
     * @param cnx
     * @throws SQLException
     */
    public static void update(Categoria categoria) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "UPDATE categoria SET "
                + "nombre = '" + categoria.getNombre() + "' "
                + "WHERE idCategoria = " + categoria.getIdCategoria() + ";";
        Conexion.ejecutar(sql);
    }

    public static void changeImagen(Categoria categoria) 
    		throws SQLException, FileNotFoundException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "UPDATE categoria SET imagen = ? WHERE idCategoria = ?";
        PreparedStatement ps = Conexion.cnx.prepareStatement(sql);

        FileInputStream fis = new FileInputStream(categoria.getImagen());

        ps.setBinaryStream(1, fis, categoria.getImagen().length());
        ps.setInt(2, categoria.getIdCategoria());

        ps.executeUpdate();
    }

    /**
     * Elimina registro de la base de datos
     * @param categoria
     * @param cnx
     * @throws SQLException
     */
    public static void eliminar(Categoria categoria) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "DELETE FROM categoria WHERE idCategoria = " + categoria.getIdCategoria();
        Conexion.ejecutar(sql);
    }

    /**
     * Devuelve un arraylist de categorias cuyo nombre coincide con query
     * @param query
     * @param cnx
     * @return
     * @throws SQLException
     */
    public static ArrayList<Categoria> getCategorias(String query) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM categoria WHERE UPPER(nombre) LIKE UPPER('%" + query + "%') ORDER BY nombre;";
        return MdlCategoria.get(sql);
    }

    /**
     *
     * @param sql
     * @param cnx
     * @return
     * @throws SQLException
     */
    public static ArrayList<Categoria> get(String sql) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<Categoria> categorias = new ArrayList<>();
        while(rs.next()){
            Categoria categoria = new Categoria(rs.getInt("idCategoria"), rs.getString("nombre"), rs.getBinaryStream("imagen"));
            categorias.add(categoria);
        }
        return categorias;
    }

    /**
     *
     * @param idCategoria
     * @param cnx
     * @return
     * @throws SQLException
     */
    public static Categoria get(int idCategoria) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM categoria WHERE idCategoria = " + idCategoria;
        ResultSet rs = Conexion.consultar(sql);
        Categoria categoria = null;
        if(rs.next()){
        	try{
	            categoria = new Categoria(rs.getInt("idCategoria"), rs.getString("nombre"), rs.getBinaryStream("imagen"));
        	}catch(Exception e){

        	}
        }
        return categoria;
    }
}