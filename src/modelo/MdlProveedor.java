/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import clases.Proveedor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author edder
 */
public class MdlProveedor{

    /**
     * Inserta Proveedor en la BD
     * @param objeto
     * @param Conexion
     * @throws SQLException, ClassNotFoundException
     */
    
    public static void insert(Proveedor objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO proveedor(nombre, preventa, mercaderista, direccion, telefono, descripcion) "
                + "VALUES('" + objeto.getNombre() + "', "
                + "'" + objeto.getPreventa() + "', "
                + "'" + objeto.getMercaderista() + "', "
                + "'" + objeto.getDireccion() + "', "
                + "'" + objeto.getTelefono() + "', "
                + "'" + objeto.getDescripcion() + "');";
        Conexion.ejecutar(sql);
    }

    /**
     * Obtiene el último ID
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idProveedor) FROM proveedor";
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return rs.getInt(1);
        return -1;
    }

    /**
     * Actualiza proveedor
     * @param objeto
     * @param Conexion
     * @throws SQLException, ClassNotFoundException
     */
    
    public static void update(Proveedor objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE proveedor SET "
                + "nombre = '" + objeto.getNombre() + "', "
                + "preventa = '" + objeto.getPreventa() + "', "
                + "mercaderista = '" + objeto.getMercaderista() + "', "
                + "direccion = '" + objeto.getDireccion() + "', "
                + "telefono = '" + objeto.getTelefono() + "', "
                + "descripcion = '" + objeto.getDescripcion() + "' "
                + "WHERE idProveedor = " + objeto.getIdProveedor() + ";";
        Conexion.ejecutar(sql);
    }
    
    public static ArrayList<Proveedor> getProveedores_nombre(String query) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM proveedor "
                + "WHERE UPPER(nombre) LIKE UPPER('%" + query + "%');";
        return MdlProveedor.get(sql);
    }
    
    public static ArrayList<Proveedor> getProveedores_busqueda(String query)
    		throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM proveedor "
                + "WHERE UPPER(nombre) LIKE UPPER('%" + query +"%') "
                + "OR UPPER(preventa) LIKE UPPER('%" + query +"%') "
                + "OR UPPER(mercaderista) LIKE UPPER('%" + query +"%') "
                + "OR UPPER(direccion) LIKE UPPER('%" + query +"%') "
                + "OR UPPER(telefono) LIKE UPPER('%" + query +"%') "
                + "OR UPPER(descripcion) LIKE ('%" + query +"%');";
        return MdlProveedor.get(sql);
    }

    /**
     * Obtiene lista de Proveedor, según String query
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static ArrayList<Proveedor> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ArrayList<Proveedor> proveedores = new ArrayList<>();
        ResultSet rs = Conexion.consultar(query);
        while(rs.next()){
            proveedores.add(new Proveedor(
                    rs.getInt("idProveedor"),
                    rs.getString("nombre"),
                    rs.getString("preventa"),
                    rs.getString("mercaderista"),
                    rs.getString("direccion"),
                    rs.getString("telefono"),
                    rs.getString("descripcion"))
            );
        }
        return proveedores;
    }

    /**
     * Obtiene proveedor según id
     * @param id
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static Proveedor get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM proveedor WHERE idProveedor = " + id;
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return new Proveedor(
                    rs.getInt("idProveedor"),
                    rs.getString("nombre"),
                    rs.getString("preventa"),
                    rs.getString("mercaderista"),
                    rs.getString("direccion"),
                    rs.getString("telefono"),
                    rs.getString("descripcion"));
        return null;
    }
}