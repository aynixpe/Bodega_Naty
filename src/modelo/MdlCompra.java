package modelo;

import clases.Compra;
import clases.Movimiento;
import clases.MovimientoCaja;
import clases.OrdenCompra;
import clases.Proveedor;
import clases.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author edder
 */
public class MdlCompra{

    public static void insert(Compra compra) throws SQLException, ClassNotFoundException {
    	Conexion.abrirConexion();
    	String llave = compra.getOrdenCompra() != null ? ", idOrdenCompra " : "";
    	String valor = compra.getOrdenCompra() != null ? ", " + compra.getOrdenCompra().getIdOrdenCompra() : "";
        String sql = "";
        sql = "INSERT INTO compra(fecha, monto, igv" + llave + ", idProveedor, idUsuario, idMovimientoCaja, estado, factura, idMovimiento) "
            + "VALUES('" + compra.getFecha().toString() + "', "
            + compra.getMonto() + ", "
            + compra.getIgv()
            + valor + ", "
            + compra.getProveedor().getIdProveedor() + ", "
            + compra.getUsuario().getIdUsuario() + ", "
            + compra.getMovimientoCaja().getIdMovimientoCaja() + ", '"
            + compra.getEstado() + "', '"
            + compra.getFactura() + "', "
            + compra.getMovimiento().getIdMovimiento() + ");";        
        Conexion.ejecutar(sql);
    }

    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idCompra) FROM compra";
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return rs.getInt(1);
        return -1;
    }

    /*
     * Revisar Orden Compra puede ser null
     */
    public static void update(Compra compra) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE compra SET "
                + "fecha = '" + compra.getFecha().toString() + "', "
                + "monto = " + compra.getMonto() + ", "
                + "igv = " + compra.getIgv() + ", "
                + "idOrdenCompra = " + compra.getOrdenCompra().getIdOrdenCompra() + ", "
                + "idProveedor = " + compra.getProveedor().getIdProveedor() + ", "
                + "idUsuario = " + compra.getUsuario().getIdUsuario() + ", "
                + "idMovimientoCaja = " + compra.getMovimientoCaja().getIdMovimientoCaja() + ", "
                + "estado = '" + compra.getEstado() + "', "
                + "factura = '" + compra.getFactura() + "', "
                + "idMovimiento = " + compra.getMovimiento().getIdMovimiento() + " "
                + "WHERE idCompra = " + compra.getIdCompra();
        Conexion.ejecutar(sql);
    }

    public static void setMovimiento(Compra objeto) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "UPDATE compra SET idMovimientoCaja = " 
    			+ objeto.getMovimientoCaja().getIdMovimientoCaja() + " "
    			+ "WHERE idCompra = " + objeto.getIdCompra();
    	Conexion.ejecutar(sql);
    }

    public static ArrayList<Compra> get() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ArrayList<Compra> compras = new ArrayList<>();
        String sql = "SELECT * FROM compra;";
        ResultSet rs = Conexion.consultar(sql);
        while(rs.next()){
            compras.add(new Compra(
            		rs.getInt("idCompra "),
            		LocalDate.parse(rs.getString("fecha")),
            		rs.getFloat("monto"),
            		rs.getFloat("igv"),
            		new OrdenCompra(){{
            			setIdOrdenCompra(rs.getInt("idOrdenCompra"));
            		}},
            		new Proveedor(){{
            			setIdProveedor(rs.getInt("idProveedor"));
            		}},
            		new Usuario(){{
            			setIdUsuario(rs.getInt("idUsuario"));
            		}},
            		new MovimientoCaja(){{
            			setIdMovimientoCaja(rs.getInt("idMovimientoCaja"));
            		}},
            		rs.getString("estado").charAt(0),
            		new Movimiento(){{
            			setIdMovimiento(rs.getInt("idMovimiento"));
            		}},
            		rs.getString("factura")
        	));
        }
        return compras;
    }

    public static Compra get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM compra WHERE idCompra = " + id;
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return new Compra(
                    rs.getInt("idCompra"),
                    LocalDate.parse(rs.getString("fecha")),
                    rs.getFloat("monto"),
                    rs.getFloat("igv"),
                    new OrdenCompra(){{
                    	setIdOrdenCompra(rs.getInt("idOrdenCompra"));
                    }},
                    new Proveedor(){{
                    	setIdProveedor(rs.getInt("idProveedor"));
                    }},
                    new Usuario(){{
                    	setIdUsuario(rs.getInt("idUsuario"));
                    }},
                    new MovimientoCaja(){{
                    	setIdMovimientoCaja(rs.getInt("idMovimientoCaja"));
                    }},
                    rs.getString("estado").charAt(0),
                    new Movimiento(){{
                    	setIdMovimiento(rs.getInt("idMovimiento"));
                    }},
        			rs.getString("factura")
        	);
        return null;
    }

    public static ArrayList<Object[]> getConsulta(
    		String fechaInicio, String fechaFin, Proveedor proveedor, boolean estado) 
    				throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String where = "";
    	if(!estado)
    		where = "AND c.estado != 'A' ";
    	if(proveedor != null)
    		where += "AND c.idProveedor = " + proveedor.getIdProveedor() + " ";    	

    	String sql = "SELECT c.idCompra, p.nombre as proveedor, c.monto, c.igv, c.fecha, u.nombre as usuario, c.factura, c.estado "
					+ "FROM compra as c "
					+ "INNER JOIN proveedor as p "
					+ "ON c.idProveedor = p.idProveedor "
					+ "INNER JOIN usuario as u "
					+ "ON c.idUsuario = u.idUsuario "
					+ "WHERE  c.fecha >='"+fechaInicio+"' AND c.fecha <= '"+fechaFin+"' " + where
					+ "ORDER BY c.fecha desc";
    	ArrayList<Object[]> compras = new ArrayList<>();
    	ResultSet rs = Conexion.consultar(sql);
    	while(rs.next()){
    		compras.add(new Object[]{
    				rs.getInt("idCompra"),
    				rs.getString("proveedor"),
    				rs.getFloat("monto"),
    				rs.getFloat("igv"),
    				rs.getString("fecha"),
    				rs.getString("usuario"),
    				rs.getString("factura"),
    				rs.getString("estado")
    		});
    	}
    	return compras;
    }

    public static void anularCompra(int idCompra) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "UPDATE compra SET "
    			+ "estado = 'A' "
    			+ "WHERE idCompra = " + idCompra + " "
    			+ "AND estado NOT LIKE 'A';";
    	Conexion.ejecutar(sql);
    }

}
