/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.Almacen;
import clases.Movimiento;
import clases.TipoMovimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Edder
 */
public class MdlMovimiento{

    /**
     *
     * @param movimiento
     * @param Conexion
     * @throws SQLException, ClassNotFoundException
     */
    
    public static void insert(Movimiento movimiento) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	//int origen = (movimiento.getAlmacenOrigen()==null)?null:movimiento.getAlmacenOrigen().getIdAlmacen();
    	//int destino = (movimiento.getAlmacenDestino()==null)?null:movimiento.getAlmacenDestino().getIdAlmacen();
    	String sql = "";
        if (movimiento.getAlmacenOrigen()==null) {
        	sql = "INSERT INTO movimiento (idTipoMovimiento, fecha, idAlmacenDestino, estado) "
                    + "VALUES("+ movimiento.getTipoMovimiento().getIdTipoMovimiento() + ","
                    + "'" + movimiento.getFecha().toString() + "', "
                    + movimiento.getAlmacenDestino().getIdAlmacen() + ", "
                    + "'"+movimiento.getEstado()+"');";
		}else if(movimiento.getAlmacenDestino()==null){
			sql = "INSERT INTO movimiento (idTipoMovimiento, fecha, idAlmacenOrigen, estado) "
                    + "VALUES("+ movimiento.getTipoMovimiento().getIdTipoMovimiento() + ","
                    + "'" + movimiento.getFecha().toString() + "', "
                    + movimiento.getAlmacenOrigen().getIdAlmacen() + ", "
                    + "'"+movimiento.getEstado()+"');";
		}else{
			sql = "INSERT INTO movimiento (idTipoMovimiento, fecha, idAlmacenOrigen, idAlmacenDestino, estado) "
                    + "VALUES("+ movimiento.getTipoMovimiento().getIdTipoMovimiento() + ","
                    + "'" + movimiento.getFecha().toString() + "', "
                    +  movimiento.getAlmacenOrigen().getIdAlmacen() + ", "
                    + movimiento.getAlmacenDestino().getIdAlmacen() + ", "
                    + "'"+movimiento.getEstado()+"');";
		}
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idMovimiento) FROM movimiento";
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next()){
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     *
     * @param movimiento
     * @param Conexion
     * @throws SQLException, ClassNotFoundException
     */
    
    public static void update(Movimiento movimiento) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "UPDATE movimiento SET "
                + "idTipoMovimiento = " + movimiento.getTipoMovimiento().getIdTipoMovimiento() + ", "
                + "fecha = '" + movimiento.getFecha().toString() + "', "
                + "idAlmacenOrigen = " + movimiento.getAlmacenOrigen().getIdAlmacen() + ", "
                + "idAlmacenDestino = " + movimiento.getAlmacenDestino().getIdAlmacen() + ", "
                + "estado = " + movimiento.getEstado() + " "
                + "WHERE idMovimiento = " + movimiento.getIdMovimiento();
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param idMovimiento
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static Movimiento get(int idMovimiento) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM movimiento WHERE idMovimiento = " + idMovimiento;
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return new Movimiento(idMovimiento,
                    new TipoMovimiento(rs.getInt("idTipoMovimiento"), ' ',null),
                    LocalDate.parse(rs.getString("fecha")),
                    rs.getInt("idAlmacenOrigen") == 0 ? null : new Almacen(){{this.setIdAlmacen(rs.getInt("idAlmacenOrigen"));}},
                    rs.getInt("idAlmacenDestino") == 0 ? null : new Almacen(){{this.setIdAlmacen(rs.getInt("idAlmacenDestino"));}},
                    rs.getString("estado").charAt(0));
        return null;
    }

    /**
     *
     * @param sql
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static ArrayList<Movimiento> get(String sql) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ArrayList<Movimiento> movimientos = new ArrayList<>();
        ResultSet rs = Conexion.consultar(sql);
        while(rs.next()){
            movimientos.add(new Movimiento(
                    rs.getInt("idMovimiento"),
                    new TipoMovimiento(){
                        {
                            this.setIdTipoMovimiento(rs.getInt("idTipoMovimiento"));
                        }
                    },
                    LocalDate.parse(rs.getString("fecha")),
                    new Almacen(){
                        {
                            this.setIdAlmacen(rs.getInt("idAlmacenOrigen"));
                        }
                    },
                    new Almacen(){
                        {
                            this.setIdAlmacen(rs.getInt("idAlmacenOrigen"));
                        }
                    },
                    rs.getString("estado").charAt(0)
                    )
            );
        }
        return movimientos;
    }

    /*
     * Metodo usado para consultar movimientos en frmconsultamovimiento
     */
	public static ArrayList<Object[]> getConsulta(
			String fechaInicio, String fechaFin, Almacen almacen, TipoMovimiento tipoMovimiento, boolean estado) 
					throws SQLException, ClassNotFoundException {
		Conexion.abrirTransaccion();
		String where = "";

		if (!estado)
            where = " AND m.estado NOT LIKE 'A' ";

		if (tipoMovimiento != null)
			where += "AND m.idTipoMovimiento = " + tipoMovimiento.getIdTipoMovimiento() + " ";
			
		if(almacen != null)
			where += "AND  (a0.idAlmacen = "+almacen.getIdAlmacen()+" or a1.idAlmacen = "+almacen.getIdAlmacen()+") ";
		
		String sql = "SELECT m.idMovimiento, tm.nombre as tipomovimiento, a0.nombre as origen, a1.nombre as destino, m.fecha "
				+ "FROM tipomovimiento as tm "
				+ "INNER JOIN movimiento as m "
				+ "ON tm.idTipoMovimiento = m.idTipoMovimiento "
				+ "LEFT JOIN almacen as a0 "
				+ "ON m.idAlmacenOrigen = a0.idAlmacen "
				+ "LEFT JOIN almacen as a1 "
				+ "ON m.idAlmacenDestino = a1.idAlmacen "
				+ "WHERE m.fecha >='"+fechaInicio+"' AND m.fecha <= '"+fechaFin+"' " 
				+ where
				+ "ORDER BY m.idMovimiento desc";
    	ArrayList<Object[]> movimientos = new ArrayList<>();
    	ResultSet rs = Conexion.consultar(sql);
    	while(rs.next()){
    		movimientos.add(new Object[]{
    			rs.getInt("idMovimiento"),
    			rs.getString("tipomovimiento"),
    			rs.getString("origen"),
    			rs.getString("destino"),
    			rs.getString("fecha")
    		});
    	}
    	return movimientos;
	}

	public static boolean compraMovimiento(int movimiento) throws SQLException, ClassNotFoundException{
		Conexion.abrirTransaccion();
		String sql = "SELECT COUNT(idCompra) as n "
				+ "FROM compra "
				+ "WHERE estado NOT LIKE 'A' "
				+ "AND idMovimiento = " + movimiento;
		ResultSet rs = Conexion.consultar(sql);
		if (rs.next()) {
			if( rs.getInt("n") == 0) return false;
		}
		return true;
	}

	public static boolean ventaMovimiento(int movimiento) throws SQLException, ClassNotFoundException{
		Conexion.abrirTransaccion();
		String sql = "SELECT COUNT(idVenta) as n "
				+ "FROM venta "
				+ "WHERE estado NOT LIKE 'A'"
				+ "AND idMovimiento = " + movimiento;
		ResultSet rs = Conexion.consultar(sql);
		if (rs.next()) {
			if( rs.getInt("n") == 0) return false;
		}
		return true;
	}

	public static void anularMovimiento(int movimiento) throws SQLException, ClassNotFoundException{
		Conexion.abrirTransaccion();
		String sql = "UPDATE movimiento SET "
					+ "estado = 'A' "
	                + "WHERE idMovimiento = " + movimiento;
		Conexion.ejecutar(sql);
	}


}
