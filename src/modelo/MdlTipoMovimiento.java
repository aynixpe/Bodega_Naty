/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.TipoMovimiento;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Edder
 */
public class MdlTipoMovimiento{
    
    /**
     *
     * @param tipoMovimiento
     * @param Conexion
     * @throws SQLException, ClassNotFoundException
     */
    
    public static void insert(TipoMovimiento tipoMovimiento) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO tipomovimiento(tipo, nombre) "
                + "VALUES('" + tipoMovimiento.getTipo() + "',"
                + "'" + tipoMovimiento.getNombre() + "');";
        Conexion.ejecutar(sql);
    }
    
    /**
     *
     * @param Conexion
     * @return
     * @throws java.sql.SQLException, ClassNotFoundException
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idTipoMovimiento) FROM tipoMovimiento";
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next()){
            return rs.getInt(1);
        }
        return -1;
    }
    
    /**
     *
     * @param tipoMovimiento
     * @param Conexion
     * @throws SQLException, ClassNotFoundException
     */
    
    public static void update(TipoMovimiento tipoMovimiento) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "UPDATE tipomovimiento SET "
                + "tipo = '" + tipoMovimiento.getTipo() + "', "
                + "nombre = '" + tipoMovimiento.getNombre() + "' "
                + "WHERE idTipoMovimiento = " + tipoMovimiento.getIdTipoMovimiento();
        Conexion.ejecutar(sql);        
    }
    
    public static ArrayList<TipoMovimiento> getTipoMovimientos(String query) throws SQLException, ClassNotFoundException{        
        String sql = "SELECT * FROM tipomovimiento WHERE UPPER(nombre) LIKE UPPER('%" + query + "%')";
        return MdlTipoMovimiento.get(sql);        
    }
    
    /**
     *
     * @param sql
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static ArrayList<TipoMovimiento> get(String sql) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        ArrayList<TipoMovimiento> tipoMovimientos = new ArrayList<>();        
        ResultSet rs = Conexion.consultar(sql);
        while(rs.next()){
            tipoMovimientos.add( new TipoMovimiento(rs.getInt(1), 
                    rs.getString(2).charAt(0), 
                    rs.getString(3)));
        }
        return tipoMovimientos;
    }

    /**
     *
     * @param id
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static TipoMovimiento get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM tipomovimiento WHERE idTipoMovimiento = " + id;
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next()){
            return new TipoMovimiento(rs.getInt("idTipoMovimiento"),
                    rs.getString("tipo").charAt(0),
                    rs.getString("nombre"));
        }
        return null;
    }
}
