package modelo;

import clases.Almacen;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author Edder
 */
public class MdlAlmacen{
    /**
     *
     * @param almacen
     * @param trans indica si es una transaccion
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void insert(Almacen almacen) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO almacen(nombre, estado) "
                + "VALUES('" + almacen.getNombre() + "', '" + almacen.getEstado() + "');";
        Conexion.ejecutar(sql);

    }

    /**
     *
     * @param trans indica si es una transaccion
     * @return
     * @throws SQLException
     */
    public static int getLastId() throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "SELECT idAlmacen FROM almacen ORDER BY idalmacen DESC LIMIT 1";
        ResultSet rs = Conexion.consultar(sql);
        int id = -1;
        if(rs.next()){
            id = rs.getInt(1);
        }
        return id;
    }

    /**
     *
     * @param almacen
     * @param trans indica si es una transaccion
     * @throws SQLException
     */
    public static void update(Almacen almacen) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "UPDATE almacen SET "
                + "nombre = '" + almacen.getNombre() + "', "
                + "estado = '" + almacen.getEstado() + "' "
                + "WHERE idalmacen = " + almacen.getIdAlmacen();
        Conexion.ejecutar(sql);
    }

    public static void dar_baja(Almacen almacen, boolean estado) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String state = "";
    	if(estado){
    		state = "estado = 'I' ";
    	}else{
    		state = "estado = 'A' ";
    	}
        String sql = "UPDATE almacen SET "
                + state
                + "WHERE idalmacen = " + almacen.getIdAlmacen();
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param query
     * @param trans indica si es una transaccion
     * @return
     * @throws SQLException
     */
    public static ArrayList<Almacen> getAlmacenes(String query) throws SQLException, ClassNotFoundException{
        String sql = "SELECT * FROM almacen WHERE UPPER(nombre) LIKE UPPER('%" + query + "%') "
                + "ORDER BY nombre;";
        return MdlAlmacen.get(sql);
    }

    /**
     *
     * @param query
     * @param estado
     * @param trans indica si es una transaccion
     * @return
     * @throws SQLException
     */
    public static ArrayList<Almacen> getAlmacenes_estado(String query, char estado)
    		throws SQLException, ClassNotFoundException{
        String sql = "SELECT * FROM almacen WHERE UPPER(nombre) LIKE UPPER('%" + query + "%') "
                + "AND estado LIKE '" + estado + "' ORDER BY nombre;";
        return MdlAlmacen.get(sql);
    }

    /**
     * Este metodo consulta un Almacen segun su ID
     * @param idAlmacen Este es un entero con el id del almacen
     * @param trans indica si es una transaccion Esta es la conexion que se usa para la consulta
     * @return Un objeto de Almacen
     * @throws SQLException
     */
    public static Almacen get(int idAlmacen) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM almacen WHERE idalmacen = " + idAlmacen;
        ResultSet rs = Conexion.consultar(sql);
        Almacen almacen = null;
        if(rs.next()){
            almacen = new Almacen(rs.getInt(1), rs.getString(2), rs.getString(3).charAt(0));
        }
        return almacen;
    }

    public static ArrayList<Almacen> get(String sql) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<Almacen> almacenes = new ArrayList<>();
        while(rs.next()){
            Almacen almacen = new Almacen(rs.getInt("idalmacen"),
                    rs.getString("nombre"),
                    rs.getString("estado").charAt(0));
            almacenes.add(almacen);
        }
        return almacenes;
    }
}
