package modelo;

import clases.TipoPago;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class MdlTipoPago{

    /**
     * 
     * @param tipopago
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */    
    public static void insert(TipoPago tipopago) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO tipoPago(nombre) "
                + "VALUES ('" +tipopago.getNombre() + "');";
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idTipoPago) FROM tipoPAgo;";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     * 
     * @param tipopago
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static void update(TipoPago tipopago) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE tipoPago SET "
                + "nombre = '" + tipopago.getNombre() + "' "
                + "WHERE idTipoPago = " + tipopago.getIdTipoPago();
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static ArrayList<TipoPago> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ResultSet rs = Conexion.consultar(query);
        ArrayList<TipoPago> tipos = new ArrayList<>();
        while(rs.next()){
            tipos.add(new TipoPago(rs.getInt(1), rs.getString(2)));
        }
        return tipos;
    }
    
    /**
     * 
     * @param id
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static TipoPago get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM tipoPago WHERE idTipoPago LIKE " + id;
        ResultSet rs = Conexion.consultar(sql);
        TipoPago tipo = null;
        if (rs.next()) {
            tipo = new TipoPago(rs.getInt(1), rs.getString(2));
        }
        return tipo;
    }
    
}
