package modelo;

import clases.Almacen;
import clases.AlmacenArticulo;
import clases.Articulo;
import clases.DetalleMovimiento;
import clases.Kardex;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Edder
 * @version 1.0
 */
public class MdlKardex{

    /**
     * Inserta Kardex (sin ID)
     * @param objeto
     * @param Conexion
     * @throws SQLException, ClassNotFoundException
     */
    
    public static void insert(Kardex objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO kardex( idAlmacenArticulo, movimiento, cantidad, stockBefore, stockAfter, fecha, idDetalleMovimiento) VALUES("
                + objeto.getAlmacenArticulo().getIdAlmacenArticulo() + ", '"
                + objeto.getMovimiento() + "', "
                + objeto.getCantidad() + ", "
                + objeto.getStockBefore() + ", "
                + objeto.getStockAfter() + ", "
                + "'" + objeto.getFecha().toString() + "', "
                + objeto.getDetalleMovimiento().getIdDetalleMovimiento() + ");";
        Conexion.ejecutar(sql);
    }

    /**
     * Devuelve el máximo ID insertado
     * @param Conexion Conexión previamente abierta
     * @return Último ID
     * @throws SQLException, ClassNotFoundException
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idKardex) FROM kardex";
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return rs.getInt(1);
        return -1;
    }

    /**
     * Actualiza kardex
     * @param objeto Kardex
     * @param Conexion Conexión previamente abierta
     * @throws SQLException, ClassNotFoundException
     */
    
    public static void update(Kardex objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE kardex SET "
                + "movimiento = '" + objeto.getMovimiento() + "', "
                + "cantidad = " + objeto.getCantidad() + ", "
                + "fecha = '" + objeto.getFecha().toString() + "',"
                + "stockBefore = " + objeto.getStockBefore() + ", "
                + "stockAfter = " + objeto.getStockAfter() + ", "
                + "idMovimiento = " + objeto.getDetalleMovimiento().getIdDetalleMovimiento() + " "
                + "WHERE "
                + "idKadex = " + objeto.getIdKardex() + ";";
        Conexion.ejecutar(sql);
    }

    /**
     * No implementado, retorna null
     * @param sql
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static ArrayList<Kardex> get() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ArrayList<Kardex> kardexs = new ArrayList<>();
        String sql = "SELECT * FROM kardex;";
        ResultSet rs = Conexion.consultar(sql);
        while(rs.next()){
            kardexs.add(new Kardex(
                rs.getInt("idKardex"),
                new AlmacenArticulo(){{
                	setIdAlmacenArticulo(rs.getInt("idAlmacenArticulo"));
                }},
                rs.getString("movimiento").charAt(0),
                rs.getInt("cantidad"),
                rs.getInt("stockBefore"),
                rs.getInt("stockAfter"),
                LocalDate.parse("fecha"),
                new DetalleMovimiento(){{
                	setIdDetalleMovimiento(rs.getInt("idDetalleMovimiento"));
                }})
            );
        }
        return kardexs;
    }

    /**
     * Devuelve Kardex según ID
     * @param id Id de kardex a consultar
     * @param Conexion Conexión previamente abierta
     * @return Kardex
     * @throws SQLException, ClassNotFoundException
     */
    
    public static Kardex get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM kardex WHERE idKardex = " + id;
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return new Kardex(
                    rs.getInt("idKardex"),
                    new AlmacenArticulo(){{this.setIdAlmacenArticulo(rs.getInt("idAlmacenArticulo"));}},
                    rs.getString("movimiento").charAt(0),
                    rs.getInt("cantidad"),
                    rs.getInt("stockBefore"),
                    rs.getInt("stockAfter"),
                    LocalDate.parse(rs.getString("fecha")),
                    new DetalleMovimiento(){{
                    	setIdDetalleMovimiento(rs.getInt("idDetalleMovimiento"));
                    }});
        return null;
    }

    public static ArrayList<Object[]> getConsulta(
    		String fechaInicio, String fechaFin, Articulo articulo, Almacen almacen) 
    				throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	
    	String where = "";
    	if(almacen != null & articulo != null)
    		where += " AND a.idArticulo = " + articulo.getIdArticulo() + " "
    				+ " AND al.idAlmacen = " + almacen.getIdAlmacen() + " ";
    		
    	String sql = "SELECT k.idKardex, a.nombre as articulo, k.movimiento, k.cantidad, k.stockBefore, k.stockAfter, al.nombre as almacen, k.fecha, t.nombre as tipomovimiento, m.estado as estadomovimiento "
				+ "FROM articulo as a "
				+ "INNER JOIN almacenarticulo as aa "
				+ "ON a.idArticulo = aa.idArticulo "
				+ "INNER JOIN kardex as k "
				+ "ON aa.idAlmacenArticulo = k.idAlmacenArticulo "
				+ "INNER JOIN almacen as al "
				+ "ON aa.idAlmacen = al.idAlmacen "
                + "INNER JOIN detallemovimiento as dm "
                + "ON k.idDetalleMovimiento = dm.idDetalleMovimiento "
                + "INNER JOIN movimiento as m "
                + "ON dm.idMovimiento = m.idMovimiento "
                + "INNER JOIN tipomovimiento as t "
                + "ON m.idTipoMovimiento = t.idTipoMovimiento "
				+ "WHERE k.fecha >='"+fechaInicio+"' AND k.fecha <= '"+fechaFin+"' " + where
				+ " ORDER BY k.idKardex asc, k.fecha desc";
    	ArrayList<Object[]> kardex = new ArrayList<>();
    	ResultSet rs = Conexion.consultar(sql);
    	//id, fecha,  almacen, articulo, tipo(I, E), Movimiento(Anulacion, transferencia, etc), Estado, Cantidad, antes, después
    	while(rs.next()){
    		kardex.add(new Object[]{
    				rs.getInt("idKardex"),
    				rs.getString("fecha"),
    				rs.getString("almacen"),
    				rs.getString("articulo"),
    				rs.getString("tipomovimiento"),
    				rs.getString("movimiento"),
    				rs.getString("estadomovimiento"),
    				rs.getInt("cantidad"),
    				rs.getInt("stockBefore"),
    				rs.getInt("stockAfter")
    		});
    	}
    	return kardex;
    }
}