package modelo;

import clases.Articulo;
import clases.Lote;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Edder
 * @version 1.0
 */
public class MdlLote{

    /**
     * Inserta lote(sin ID) de un artículo
     *
     * @param objeto Lote sin id
     * @param Conexion Conexión previamente abierta
     * @throws SQLException, ClassNotFoundException
     */
    public static void insert(Lote objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO lote(idArticulo, fechaVencimiento, lote, estado) VALUES("
                + objeto.getArticulo().getIdArticulo() + ", '"
                + objeto.getFechaVencimiento().toString() + "', '"
                + objeto.getLote() + "', '"
                + objeto.getEstado() + "');";
        Conexion.ejecutar(sql);
    }

    /**
     * Devuelve el último ID insertado
     *
     * @param Conexion Conexión previamente abierta
     * @return ID
     * @throws SQLException, ClassNotFoundException
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idLote) FROM lote";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     * Actualiza un Lote
     * @param objeto Lote
     * @param Conexion Conexión previamente abierta
     * @throws SQLException, ClassNotFoundException
     */
    
    public static void update(Lote objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE lote SET "
                + "idArticulo = " + objeto.getArticulo().getIdArticulo() + ", "
                + "fechaVencimiento = '" + objeto.getFechaVencimiento().toString() + "', "
                + "lote = '" + objeto.getLote() + "', "
                + "estado = '" + objeto.getEstado() + "' WHERE "
                + "idLote = " + objeto.getIdLote();
        Conexion.ejecutar(sql);
    }

    /**
     * Devuelve lotes que coinciden con la fecha de ventimiento
     * @param query Fecha de vencimiento
     * @param Conexion Conexión previamente abierta
     * @return Lista de Lotes
     * @throws SQLException, ClassNotFoundException
     */
    
    public static ArrayList<Lote> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM lote WHERE fechaVentimiento LIKE '" + query + "';";
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<Lote> lotes = new ArrayList<>();
        while(rs.next()){
            lotes.add(new Lote(
                    rs.getInt("idLote"), 
                    new Articulo(){{this.setIdArticulo(rs.getInt("idArticulo"));}},
                    LocalDate.parse(rs.getString("fechaVencimiento")), 
                    rs.getString("lote"),
                    rs.getString("estado").charAt(0)));
        }
        return lotes;
    }

    /**
     * Devuelve un lote según ID
     * @param id
     * @param Conexion Conexión previamente abierta
     * @return Lote
     * @throws SQLException, ClassNotFoundException
     */
    
    public static Lote get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM lote WHERE idLote = " + id;
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return new Lote(
                    rs.getInt("idLote"), 
                    new Articulo(){{this.setIdArticulo(rs.getInt("idArticulo"));}},
                    LocalDate.parse(rs.getString("fechaVencimiento")), 
                    rs.getString("lote"),
                    rs.getString("estado").charAt(0));
        return null;
    }

}
