package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import clases.Caja;
import clases.DetalleMovimientoCaja;
import clases.FlujoCaja;

public class MdlFlujoCaja{
	
	public static void insert(FlujoCaja objeto) throws SQLException, ClassNotFoundException{
		Conexion.abrirTransaccion();
		String sql = "INSERT INTO flujocaja (idDetalleMovimientoCaja, idCaja, movimiento, monto, montoBefore, montoAfter, fecha) "
				+ "VALUES ( "
				+ objeto.getDetalleMovimientoCaja().getIdDetalleMovimientoCaja() + ", "
				+ objeto.getCaja().getIdCaja() + ", "
				+ "'" + objeto.getMovimiento() + "', "
				+ objeto.getMonto() + ", "
				+ objeto.getMontoBefore() + ", "
				+ objeto.getMontoAfter() + ", "
				+ "'" + objeto.getFecha().toString() + "')";
		Conexion.ejecutar(sql);
	}

	
	public static int getLastId() throws SQLException, ClassNotFoundException {
		Conexion.abrirTransaccion();
		String sql = "SELECT max(idFlujoCaja) FROM flujocaja";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
	}

	
	public static void update(FlujoCaja objeto) throws SQLException, ClassNotFoundException {
		Conexion.abrirTransaccion();
		String sql = "UPDATE flujocaja SET "
				+ "idDetalleMovimiento = " + objeto.getDetalleMovimientoCaja().getIdDetalleMovimientoCaja() + ", "
				+ "idCaja = " + objeto.getCaja().getIdCaja() + ", "
				+ "movimiento = '" + objeto.getMovimiento() + "', "
				+ "monto = " + objeto.getMonto() + ", "
				+ "montoBefore = " + objeto.getMontoBefore() + ", "
				+ "montoAfter = " + objeto.getMontoAfter() + ", "
				+ "fecha = '" + objeto.getFecha().toString() + "' "
				+ "WHERE idFlujoCaja = " + objeto.getIdFlujoCaja();
		Conexion.ejecutar(sql);
	}

	
	public static ArrayList<FlujoCaja> get(String query)
			throws SQLException, ClassNotFoundException {
		Conexion.abrirTransaccion();
		ArrayList<FlujoCaja> flujosCaja = new ArrayList<FlujoCaja>();
		String sql = "SELECT * FROM flujocaja";
		ResultSet rs = Conexion.consultar(sql);
		while(rs.next()){
			flujosCaja.add(new FlujoCaja(
					rs.getInt("idFlujoCaja"),
					new DetalleMovimientoCaja(){{
						setIdDetalleMovimientoCaja(rs.getInt("idDetalleMovimientoCaja"));
					}},
					new Caja(){{
						setIdCaja(rs.getInt("idCaja"));
					}},
					rs.getString("movimiento").charAt(0),
					rs.getFloat("monto"),
					rs.getFloat("montoBefore"),
					rs.getFloat("montoAfter"),
					LocalDate.parse(rs.getString("fecha"))
			));
		}
		return flujosCaja;
	}

	
	public static FlujoCaja get(int id) throws SQLException, ClassNotFoundException {
		Conexion.abrirTransaccion();
		FlujoCaja flujoCaja = null;
		String sql = "SELECT * FROM flujocaja WHERE idFlujoCaja = " + id;
		ResultSet rs = Conexion.consultar(sql);
		if(rs.next())
			flujoCaja = new FlujoCaja(
					rs.getInt("idFlujoCaja"),
					new DetalleMovimientoCaja(){{
						setIdDetalleMovimientoCaja(rs.getInt("idDetalleMovimientoCaja"));
					}},
					new Caja(){{
						setIdCaja(rs.getInt("idCaja"));
					}},
					rs.getString("movimiento").charAt(0),
					rs.getFloat("monto"),
					rs.getFloat("montoBefore"),
					rs.getFloat("montoAfter"),
					LocalDate.parse(rs.getString("fecha"))
			);
		return flujoCaja;
	}
}