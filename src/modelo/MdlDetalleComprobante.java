/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.Articulo;
import clases.Comprobante;
import clases.DetalleComprobante;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class MdlDetalleComprobante{

    /**
     * 
     * @param detallecomprobante
     * @param Conexion
     * @throws SQLException 
     */
    
    public static void insert(DetalleComprobante detallecomprobante) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO detallecomprobante(idComprobante, idArticulo, cantidad, precio, igv) "
                + "VALUES (" + detallecomprobante.getComprobante().getIdComprobante() + ", "
                + detallecomprobante.getArticulo().getIdArticulo() + ", "
                + detallecomprobante.getCantidad() + ", "
                + detallecomprobante.getPrecio() + ", "
                + detallecomprobante.getIgv() + ");";
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param Conexion
     * @return
     * @throws SQLException 
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idDetalleComprobante) FROM detallecomprobante;";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     * 
     * @param detallecomprobante
     * @param Conexion
     * @throws SQLException 
     */
    
    public static void update(DetalleComprobante detallecomprobante) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE detallecomprobante SET "
                + "comprobante = " + detallecomprobante.getComprobante().getIdComprobante() + ", "
                + "articulo = " + detallecomprobante.getArticulo().getIdArticulo() + ", "
                + "cantidad = " + detallecomprobante.getCantidad() + ", "
                + "precio = " + detallecomprobante.getPrecio() + ", "
                + "igv = " + detallecomprobante.getIgv() + " "
                + "WHERE idDetalleComprobante = " + detallecomprobante.getIdDetalleComprobante();
        Conexion.ejecutar(sql);
    }
    
    /**
     * 
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException 
     */
    
    public static ArrayList<DetalleComprobante> get() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT * FROM detallecomprobante;";
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<DetalleComprobante> detalles = new ArrayList<>();
        while(rs.next()){
            detalles.add(new DetalleComprobante(
                    rs.getInt(1),
                    new Comprobante(){{
                    	setIdComprobante(rs.getInt(2));
                    }},
                    new Articulo(){{
                    	setIdArticulo(rs.getInt(3));
                    }},
                    rs.getInt(4),
                    rs.getFloat(5),
                    rs.getFloat(6)
            ));
        }
        return detalles;
    }

    /**
     * 
     * @param id
     * @param Conexion
     * @return
     * @throws SQLException 
     */
    
    public static DetalleComprobante get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM detallecomprobante WHERE idDetalleComprobante = " + id;
        ResultSet rs = Conexion.consultar(sql);
        DetalleComprobante detalle = null;
        if (rs.next()) {
            detalle = new DetalleComprobante(
                    rs.getInt(1),
                    new Comprobante(){{
                    	setIdComprobante(rs.getInt(2));
                    }},
                    new Articulo(){{
                    	setIdArticulo(rs.getInt(3));
                    }},
                    rs.getInt(4),
                    rs.getFloat(5),
                    rs.getFloat(6)
            );
        }
        return detalle;
    }

	public static ArrayList<Object[]> getConsulta_Detalle(int idComprobante) throws SQLException, ClassNotFoundException {
		Conexion.abrirTransaccion();
		String sql = "SELECT dc.idDetalleComprobante, a.nombre as articulo, dc.cantidad, dc.precio, dc.igv "
				+ "FROM detallecomprobante as dc "
				+ "INNER JOIN articulo as a "
				+ "ON dc.idArticulo = a.idArticulo "
				+ "WHERE dc.idComprobante = "+ idComprobante;
		ArrayList<Object[]> detalles = new ArrayList<>();
		ResultSet rs = Conexion.consultar(sql);
		while(rs.next()){
			detalles.add(new Object[]{
				rs.getInt("idDetalleComprobante"),
				rs.getString("articulo"),
				rs.getInt("cantidad"),
				rs.getFloat("precio"),
				rs.getFloat("igv")
			});
		}
		return detalles;
	}
    
}
