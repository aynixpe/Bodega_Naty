package modelo;

import clases.TipoMovimientoCaja;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class MdlTipoMovimientoCaja{

    /**
     * 
     * @param tipomovimientocaja
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */    
    public static void insert(TipoMovimientoCaja tipomovimientocaja) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO tipomovimientocaja(descripcion, tipo) "
                + "VALUES('" + tipomovimientocaja.getDescripcion() + "', "
                + "'" + tipomovimientocaja.getTipo() + "');";
       Conexion.ejecutar(sql);
    }
    
    /**
     * 
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idTipoMovimientoCaja) FROM tipomovimientocaja;";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     * 
     * @param tipomovimientocaja
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */    
    public static void update(TipoMovimientoCaja tipomovimientocaja) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE tipomovimientocaja SET "
                + "descripcion = '" + tipomovimientocaja.getDescripcion() + "', "
                + "tipo = '" + tipomovimientocaja.getTipo() + "' "
                + "WHERE idTipoMovimientoCaja = " + tipomovimientocaja.getIdTipoMovimientoCaja();
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static ArrayList<TipoMovimientoCaja> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ResultSet rs = Conexion.consultar(query);
        ArrayList<TipoMovimientoCaja> tipos;
        tipos = new ArrayList<>();
        while (rs.next()) {
            tipos.add(new TipoMovimientoCaja(rs.getInt(1), rs.getString(2),rs.getString(3).charAt(0)));
        }
        return tipos;
    }

    /**
     * 
     * @param id
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */    
    public static TipoMovimientoCaja get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM tipomovimientocaja where idTipoMovimientoCaja = " + id;
        ResultSet rs = Conexion.consultar(sql);
        TipoMovimientoCaja tipo = null;
        if (rs.next()) {
            tipo = new TipoMovimientoCaja(rs.getInt(1), rs.getString(2), rs.getString(3).charAt(0));
        }
        return tipo;
    }
    
    public static ArrayList<TipoMovimientoCaja> getTipoMovimientosCaja(String query) throws ClassNotFoundException, SQLException{
    	Conexion.abrirConexion();
    	String sql = "SELECT * FROM tipomovimientocaja where UPPER(descripcion) LIKE UPPER('%" + query + "%');";    	
    	ArrayList<TipoMovimientoCaja> tipos =  MdlTipoMovimientoCaja.get(sql);
    	return tipos;
    }
}