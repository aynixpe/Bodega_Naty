/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.Articulo;
import clases.DetalleMovimiento;
import clases.Movimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Edder
 */
public class MdlDetalleMovimiento{

    /**
     * Inserta un detalle de movimiento
     * @param objeto DetalleMovimiento sin ID
     * @param Conexion Conexión previamente abierta
     * @throws SQLException
     */
    
    public static void insert(DetalleMovimiento objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO detallemovimiento(idMovimiento, idArticulo, cantidad) VALUES("
                + objeto.getMovimiento().getIdMovimiento() + ", "
                + objeto.getArticulo().getIdArticulo() + ", "
                + objeto.getCantidad() + ");";
        Conexion.ejecutar(sql);
    }

    /**
     * Devuelve el último ID insertado
     * @param Conexion Conexión previamente abierta
     * @return
     * @throws SQLException
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idDetalleMovimiento) FROM detallemovimiento";
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return rs.getInt(1);
        return -1;
    }

    public static ArrayList<DetalleMovimiento> getDetalleMovimientos_movimiento(int idMovimiento) 
    		throws SQLException, ClassNotFoundException{
        String sql = "SELECT * FROM detallemovimiento WHERE "
                + "idMovimiento = " + idMovimiento;
        return MdlDetalleMovimiento.get(sql);
    }

    /**
     *
     * @param sql
     * @param Conexion
     * @return null
     * @throws SQLException
     */
    
    public static ArrayList<DetalleMovimiento> get(String sql) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ArrayList<DetalleMovimiento> detalleMovimientos = new ArrayList<>();
        ResultSet rs = Conexion.consultar(sql);
        while(rs.next()){
            detalleMovimientos.add(new DetalleMovimiento(
	                    rs.getInt("idDetalleMovimiento"),
	                    new Movimiento(){{
	                    	setIdMovimiento(rs.getInt("idMovimiento"));
	                    }},
	                    new Articulo(){{
	                    	setIdArticulo(rs.getInt("idArticulo"));
	                    }},
	                    rs.getInt("cantidad")
            ));
        }
        return detalleMovimientos;
    }

    /**
     * Devuelve DetalleMovimiento según ID
     * @param id
     * @param Conexion
     * @return
     * @throws SQLException
     */
    
    public static DetalleMovimiento get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM detallemovimiento WHERE idDetalleMovimiento = " + id;
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return new DetalleMovimiento(
                    rs.getInt("idDetalleMovimiento"),
                    new Movimiento(){{this.setIdMovimiento(rs.getInt("idMovimiento"));}},
                    new Articulo(){{this.setIdArticulo(rs.getInt("idArticulo"));}},
                    rs.getInt("cantidad"));
        return null;
    }

    public static ArrayList<Object[]> getConsulta(int movimiento) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "SELECT dm.idDetalleMovimiento, "
    			+ "CONCAT(a.nombre, ' ', a.presentacion, ' ', a.tamanio, ' ', a.unidad) as nombre, "
    			+ "dm.cantidad "
				+ "FROM "
				+ "detallemovimiento as dm "
				+ "INNER JOIN "
				+ "articulo as a "
				+ "ON dm.idArticulo = a.idArticulo "
				+ "WHERE dm.idMovimiento = " + movimiento;
    	ArrayList<Object[]> detalles = new ArrayList<>();
    	ResultSet rs = Conexion.consultar(sql);
    	while(rs.next()){
    		detalles.add(new Object[]{
    				rs.getInt("idDetalleMovimiento"),
    				rs.getString("nombre"),
    				rs.getInt("cantidad")
    		});
    	}
    	return detalles;
    }

    /*
     * Metodo usado para saber la cantidad en detalle.
     */
    public static ArrayList<Object[]> articulosCantidad(int almacen, int movimiento)
    		throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ArrayList<Object[]> rows = new ArrayList<>();
    	String sql = "SELECT dm.idArticulo, dm.cantidad "
    			+ "FROM movimiento as m "
    			+ "INNER JOIN detallemovimiento as dm "
    			+ "ON m.idMovimiento = dm.idMovimiento "
    			+ "WHERE dm.idMovimiento = " + movimiento + " "
    			+ "AND m.idAlmacenDestino = " + almacen;
        ResultSet rs = Conexion.consultar(sql);
        while(rs.next()){
            rows.add(new Object[]{
            		rs.getInt("idArticulo"),
            		rs.getInt("cantidad")
            });
        }
        return rows;
    }

}
