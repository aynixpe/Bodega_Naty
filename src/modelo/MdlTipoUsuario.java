package modelo;

import clases.TipoUsuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class MdlTipoUsuario{

    /**
     * 
     * @param tipousuario
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */    
    public static void insert(TipoUsuario tipousuario) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO tipousuario("
        		+ "nombre, p_movimiento, p_venta, p_comprobante, p_ordenCompra, p_compra, p_movimiento_caja) "
                + "VALUES('" + tipousuario.getNombre() + "', "
                + tipousuario.getP_movimiento() + ", "
                + tipousuario.getP_venta() + ", "
                + tipousuario.getP_comprobante() + ", "
                + tipousuario.getP_ordenCompra() + ", "
                + tipousuario.getP_compra() + ", "
                + tipousuario.getP_movimiento_caja() + ")";
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idTipoUsuario) FROM tipousuario;";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     * 
     * @param tipousuario
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */    
    public static void update(TipoUsuario tipousuario) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE tipousuario SET "
                + "nombre = '" + tipousuario.getNombre() + "', "
                + "p_movimiento = " + tipousuario.getP_movimiento() + ", "
                + "p_venta = " + tipousuario.getP_venta() + ", "
                + "p_comprobante = " + tipousuario.getP_comprobante() + ", "
                + "p_ordenCompra = " + tipousuario.getP_ordenCompra() + ", "
                + "p_compra = " + tipousuario.getP_compra() + ", "
                + "p_movimiento_caja = " + tipousuario.getP_movimiento_caja() + " "
                + "WHERE idTipoUsuario = " + tipousuario.getIdTipoUsuario();
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */    
    public static ArrayList<TipoUsuario> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ResultSet rs = Conexion.consultar(query);
        ArrayList<TipoUsuario> tipos;
        tipos = new ArrayList<TipoUsuario>();
        while(rs.next()){
            tipos.add(new TipoUsuario(
            		rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4),rs.getInt(5),rs.getInt(6),rs.getInt(7),rs.getInt(8))
            );
        }
        return tipos;
    }
    
    /**
     * Retorna todos los tipos de usuario
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    public static ArrayList<TipoUsuario> get() throws SQLException, ClassNotFoundException{
    	return MdlTipoUsuario.get("SELECT * FROM tipousuario");
    }

    /**
     * 
     * @param id
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */    
    public static TipoUsuario get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM tipousuario WHERE idTipoUsuario = " + id;
        ResultSet rs = Conexion.consultar(sql);
        TipoUsuario tipo = null;
        if (rs.next()) {
            tipo = new TipoUsuario(
            		rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4),rs.getInt(5),rs.getInt(6),rs.getInt(7),rs.getInt(8)
            		);
        }
        return tipo;
    }
}
