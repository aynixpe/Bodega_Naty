/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.Articulo;
import clases.DetalleOrdenCompra;
import clases.OrdenCompra;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Edder
 */
public class MdlDetalleOrdenCompra{
    
    public static void insert(DetalleOrdenCompra objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO detalleordencompra(idOrdenCompra, idArticulo, cantidad, precio, total) "
                + "VALUES(" + objeto.getOrdenCompra().getIdOrdenCompra() + ", "
                + objeto.getArticulo().getIdArticulo() + ", "
                + objeto.getCantidad() + ", "
                + objeto.getPrecio() + ", "
                + objeto.getTotal() + ");";
        Conexion.ejecutar(sql);
    }

    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idDetalleOrdenCompra) FROM detalleordencompra";
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return rs.getInt(1);
        return -1;
    }

    
    public static void update(DetalleOrdenCompra objeto) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE detalleordencompra SET "
                + "idOrdenCompra = " + objeto.getOrdenCompra().getIdOrdenCompra() + ", "
                + "idArticulo = " + objeto.getArticulo().getIdArticulo() + ", "
                + "cantidad = " + objeto.getCantidad() + ", "
                + "precio = " + objeto.getPrecio() + ", "
                + "total = " + objeto.getTotal() + " "
                + "WHERE idDetalleOrdenCompra = " + objeto.getIdDetalleOrdenCompra();
        Conexion.ejecutar(sql);
    }

    
    public static ArrayList<DetalleOrdenCompra> get() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT * FROM detalleordencompra;";
        ArrayList<DetalleOrdenCompra> detalleordencompras = new ArrayList<>();
        ResultSet rs = Conexion.consultar(sql);
        while(rs.next()){
            detalleordencompras.add(new DetalleOrdenCompra(
                    rs.getInt("idDetalleOrdenCompra"),
                    new OrdenCompra(){{
                    	setIdOrdenCompra(rs.getInt("idOrdenCompra"));
                    }},
                    new Articulo(){{
                    	setIdArticulo(rs.getInt("idArticulo"));
                    }},
                    rs.getInt("cantidad"),
                    rs.getFloat("precio"),
                    rs.getFloat("total"))
            );
        }
        return detalleordencompras;
    }

    
    public static DetalleOrdenCompra get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM detalleordencompra WHERE idDetalleOrdenCompra = " + id;
        ResultSet rs = Conexion.consultar(sql);
        if(rs.next())
            return new DetalleOrdenCompra(
            		rs.getInt("idDetalleOrdenCompra"),
                    new OrdenCompra(){{
                    	setIdOrdenCompra(rs.getInt("idOrdenCompra"));
                    }},
                    new Articulo(){{
                    	setIdArticulo(rs.getInt("idArticulo"));
                    }},
                    rs.getInt("cantidad"),
                    rs.getFloat("precio"),
                    rs.getFloat("total"));
        return null;
    }

    public static ArrayList<Object[]> getConsulta_Detalle(int idOrdenCompra) throws SQLException, ClassNotFoundException{
    	Conexion.abrirTransaccion();
    	String sql = "SELECT a.idarticulo, "
    				+ "CONCAT(a.nombre, ' ', a.presentacion, ' ', a.tamanio, ' ', a.unidad) as nombre, "
    				+ "doc.cantidad, doc.precio, doc.total, (doc.total * 0.18) as igv "
					+ "FROM detalleordencompra as doc "
					+ "INNER JOIN articulo as a "
					+ "ON doc.idArticulo = a.idArticulo "
					+ "WHERE doc.idOrdenCompra = "+ idOrdenCompra;
    	ArrayList<Object[]> detalles = new ArrayList<>();
    	ResultSet rs = Conexion.consultar(sql);
    	while(rs.next()){
    		detalles.add(new Object[]{
    				rs.getInt("idarticulo"),
    				rs.getString("nombre"),
    				rs.getInt("cantidad"),
    				rs.getFloat("precio"),
    				rs.getFloat("total"),
    				rs.getFloat("igv")
    		});
    	}
    	return detalles;
    }
}