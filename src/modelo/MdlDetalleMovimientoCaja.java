/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.DetalleMovimientoCaja;
import clases.MovimientoCaja;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class MdlDetalleMovimientoCaja{

    /**
     *
     * @param detallemovimientocaja
     * @param Conexion
     * @throws SQLException
     */
    
    public static void insert(DetalleMovimientoCaja detallemovimientocaja) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO detallemovimientocaja (idMovimientoCaja, monto, descripcion) "
                + "VALUES(" + detallemovimientocaja.getMovimientoCaja().getIdMovimientoCaja() + ", "
                + detallemovimientocaja.getMonto() + ", "
                + "'" + detallemovimientocaja.getDescripcion() + "');";
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param Conexion
     * @return
     * @throws SQLException
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idDetalleMovimientoCaja) FROM detallemovimientocaja;";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     *
     * @param detallemovimientocaja
     * @param Conexion
     * @throws SQLException
     */
    
    public static void update(DetalleMovimientoCaja detallemovimientocaja) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE detallemovimientocaja SET "
                + "idMovimientoCaja = " + detallemovimientocaja.getMovimientoCaja().getIdMovimientoCaja() + ", "
                + "monto = " + detallemovimientocaja.getMonto() + ", "
                + "descripcion = '" + detallemovimientocaja.getDescripcion() + " "
                + "WHERE idDetalleMovimientoCaja = " + detallemovimientocaja.getIdDetalleMovimientoCaja();
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException
     */
    
    public static ArrayList<DetalleMovimientoCaja> get() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT * FROM detallemovimientocaja;";
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<DetalleMovimientoCaja> detalles;
        detalles = new ArrayList<>();
        while(rs.next()){
            detalles.add(new DetalleMovimientoCaja(
                    rs.getInt(1),
                    new MovimientoCaja(){{
                    	setIdMovimientoCaja(rs.getInt(2));
                    }},
                    rs.getFloat(3),
                    rs.getString(4)
            ));
        }
        return detalles;
    }

    /**
     *
     * @param id
     * @param Conexion
     * @return detalle objeto de la clase DetalleMovimientoCaja
     * @throws SQLException
     */
    
    public static DetalleMovimientoCaja get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM detallemovimientocaja WHERE idDetalleMovimientoCaja = " + id;
        ResultSet rs = Conexion.consultar(sql);
        DetalleMovimientoCaja detalle = null;
        if (rs.next()) {
            detalle = new DetalleMovimientoCaja(
                    rs.getInt(1),
                    new MovimientoCaja(){{
                    	setIdMovimientoCaja(rs.getInt(2));
                    }},
                    rs.getFloat(3),
                    rs.getString(4)
            );
        }
        return detalle;
    }

    public static ArrayList<DetalleMovimientoCaja> getDetalles_MovimientCaja(int idMovimientoCaja) 
    		throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql =  "SELECT * FROM detallemovimientocaja WHERE idmovimientocaja = " + idMovimientoCaja;
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<DetalleMovimientoCaja> detalles;
        detalles = new ArrayList<>();
        while(rs.next()){
            detalles.add(new DetalleMovimientoCaja(
                    rs.getInt(1),
                    new MovimientoCaja(){{
                    	setIdMovimientoCaja(rs.getInt(2));
                    }},
                    rs.getFloat(3),
                    rs.getString(4)
            ));
        }
        return detalles;
    }
    
    public static ArrayList<Object[]> getConsultaDetalles(int idmovimientocaja) throws SQLException, ClassNotFoundException{
    	ArrayList<Object[]> detalles = new ArrayList<Object[]>();
    	Conexion.abrirTransaccion();
    	String sql = "SELECT iddetallemovimientocaja, descripcion, monto FROM detallemovimientocaja "
    			+ "WHERE idmovimientocaja = " + idmovimientocaja;
    	ResultSet rs = Conexion.consultar(sql);
    	while(rs.next()){
    		detalles.add(new Object[]{
    				rs.getInt("iddetallemovimientocaja"),
    				rs.getString("descripcion"),
    				rs.getFloat("monto")
    		});
    	}
    	return detalles;
    }

}
