/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.Serie;
import clases.TipoComprobante;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class MdlSerie{

    /**
     * 
     * @param serie
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static void insert(Serie serie) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO serie(tipoComprobante, nombre) "
                + "VALUES(" + serie.getTipoComprobante().getIdTipoComprobante() +", "
                + "'" + serie.getNombre() + "')";
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idSerie) FROM serie;";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    
    public static void update(Serie serie) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE serie SET "
                + "tipoComprobante = " + serie.getTipoComprobante().getIdTipoComprobante() + ", "
                + "nombre = '" + serie.getNombre() + "' "
                + "WHERE idSerie = " + serie.getIdSerie();
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static ArrayList<Serie> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT * FROM serie where UPPER(nombre) LIKE UPPER('%" + query +"%');";
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<Serie> series = new ArrayList<>();
        while(rs.next()){
            series.add(new Serie(
                    rs.getInt(1),
                    new TipoComprobante(){{
                    	setIdTipoComprobante(rs.getInt(2));
                    }},
                    rs.getString(3)
            ));
        }
        return series;
    }
    
    public static ArrayList<Serie> getSeries_TipoComprobante(int idTipoComprobante) 
    		throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT * FROM serie where idTipoComprobante = " + idTipoComprobante +";";
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<Serie> series = new ArrayList<>();
        while(rs.next()){
            series.add(new Serie(
                    rs.getInt(1),
                    new TipoComprobante(){{
                    	setIdTipoComprobante(rs.getInt(2));
                    }},
                    rs.getString(3)
            ));
        }
        return series;
    }

    /**
     * 
     * @param id
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static Serie get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT * FROM serie WHERE idSerie = " + id;
    	ResultSet rs = Conexion.consultar(sql);
    	Serie serie = null;
	    if (rs.next()) {
	        serie = new Serie(
	                rs.getInt(1),
	                new TipoComprobante(){
	                    {
	                        this.setIdTipoComprobante(rs.getInt(2));
	                    }
	                },
	                rs.getString(3)
	        );
	    }
        return serie;
    }
}