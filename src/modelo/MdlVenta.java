/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.Almacen;
import clases.Cliente;
import clases.Movimiento;
import clases.Usuario;
import clases.Venta;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class MdlVenta{

    /**
     *
     * @param venta
     * @param Conexion
     * @throws SQLException, ClassNotFoundException
     */    
    public static void insert(Venta venta) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "";
    	String campo = "";
    	String valor = "";
    	
    	if (venta.getCliente() != null) {
    		campo = ", idCliente";
    		valor = ", " + venta.getCliente().getIdCliente();
    	}
		sql = "INSERT INTO venta(fecha" + campo + ", monto, idUsuario, estado, idMovimiento) "
                + "VALUES ('" + venta.getFecha().toString() + "' "
				+ valor + ", "
                + venta.getMonto() + ", "
                + venta.getUsuario().getIdUsuario() + ", "
                + "'" + venta.getEstado() + "', "
                + venta.getMovimiento().getIdMovimiento() + ");";		
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idVenta) FROM venta;";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /*
     *
     * @param venta
     * @param Conexion
     * @throws SQLException, ClassNotFoundException
     */    
//    public static void setComprobante(Venta venta) throws SQLException, ClassNotFoundException {
//    	Conexion.abrirTransaccion();
//    	String sql = "UPDATE venta SET "
//                + "idComprobante = " + venta.getComprobante().getIdComprobante() + ", "
//                + "estado = 'P'"
//                + "WHERE idVenta = " + venta.getIdVenta();
//        Conexion.ejecutar(sql);
//    }
    
    public static void update(Venta venta) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String cliente = "";
        if(venta.getCliente() != null){
            cliente = "idCliente = " + venta.getCliente().getIdCliente() + ", ";
        }
        String sql = "UPDATE venta SET "
                + "fecha = '" + venta.getFecha().toString() + "', "
                + cliente
                + "monto = " + venta.getMonto() + ", "
                + "idUsuario = " + venta.getUsuario().getIdUsuario() + ", "
                + "estado = '" + venta.getEstado() + "', "
                + "idMovimiento = " + venta.getMovimiento().getIdMovimiento() + " "
                + "WHERE idVenta = " + venta.getIdVenta();
        Conexion.ejecutar(sql);
    }

    /**
     *
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */
    
    public static ArrayList<Venta> get() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT * FROM venta;";
        ResultSet rs = Conexion.consultar(sql);
        ArrayList<Venta> ventas = new ArrayList<>();
        while(rs.next()){
            ventas.add(new Venta(
                    rs.getInt(1),
                    LocalDate.parse(rs.getString(2)),
                    new Cliente(){{
                    	setIdCliente(rs.getInt(3));
                    }},
                    rs.getFloat(4),
                    new Usuario(){{
                    	setIdUsuario(rs.getInt(6));
                    }},
                    rs.getString(7).charAt(0),
                    new Movimiento(){{
                    	setIdMovimiento(rs.getInt(8));
                    }}
            ));
        }
        return ventas;
    }

    /**
     *
     * @param id
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException
     */    
    public static Venta get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM venta WHERE idVenta = " + id;
        ResultSet rs = Conexion.consultar(sql);
        Venta venta = null;
        if (rs.next()) {
            venta = new Venta(
                    rs.getInt("idventa"),
                    LocalDate.parse(rs.getString("fecha")),
                    new Cliente(){{
                    	setIdCliente(rs.getInt("idcliente"));
                    }},
                    rs.getFloat("monto"),
                    new Usuario(){{
                    	setIdUsuario(rs.getInt("idusuario"));
                    }},
                    rs.getString("estado").charAt(0),
                    new Movimiento(){{
                    	setIdMovimiento(rs.getInt("idmovimiento"));
                    }}
            );
        }
        return venta;
    }

	public static ArrayList<Object[]> getConsulta(
			String fechaInicio, String fechaFin, Cliente cliente, Almacen almacen, boolean estado) 
					throws SQLException, ClassNotFoundException {
		Conexion.abrirTransaccion();
		String where = "";

		if(!estado)
			where = "AND v.estado NOT LIKE 'A' ";
		if(cliente != null)
			where += "AND v.idCliente = " + cliente.getIdCliente() + " ";
		if(almacen != null)
			where += "AND m.idAlmacenOrigen= " + almacen.getIdAlmacen()+ " ";
		
		String sql = "SELECT v.idVenta, cl.nombre as cliente, v.monto, v.fecha, "
				+ "u.nombre as usuario, a.nombre as almacen, v.estado "
				+ "FROM venta as v LEFT JOIN cliente as cl "
				+ "ON v.idCliente = cl.idCliente "
				+ "INNER JOIN usuario as u "
				+ "ON v.idUsuario = u.idUsuario "
				+ "INNER JOIN movimiento as m "
				+ "ON v.idMovimiento = m.idMovimiento "
				+ "LEFT JOIN almacen as a "
				+ "ON m.idAlmacenOrigen = a.idAlmacen "
				+ "WHERE v.fecha >= '" + fechaInicio + "' AND v.fecha <= '" + fechaFin + "' " 
				+ where
				+ " ORDER BY v.fecha desc, v.idVenta desc";
		ArrayList<Object[]> ventas = new ArrayList<>();
		ResultSet rs = Conexion.consultar(sql);
		while(rs.next()){
			ventas.add(new Object[]{
				rs.getInt("idVenta"), 
				rs.getString("cliente"),
				rs.getFloat("monto"),
				rs.getString("fecha"),
				rs.getString("usuario"),
				rs.getString("almacen"),
				rs.getString("estado")
			});
		}
		return ventas;
	}
	
	public static void procesarVenta(int idVenta) throws ClassNotFoundException, SQLException{
		Conexion.abrirTransaccion();
		String sql = "UPDATE venta SET "
				+ "estado = 'P' "
				+ "WHERE idVenta = " + idVenta;
		Conexion.ejecutar(sql);
	}

	public static void anularVenta(int idVenta) throws SQLException, ClassNotFoundException{
		Conexion.abrirTransaccion();
		String sql = "UPDATE venta SET "
				+ "estado = 'A' "
				+ "WHERE idVenta = " + idVenta
				+ " AND estado NOT LIKE 'P' "
				+ "AND estado NOT LIKE 'A';";
		Conexion.ejecutar(sql);
	}
}
