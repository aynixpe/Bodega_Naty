/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.TipoComprobante;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class MdlTipoComprobante{

    /**
     * 
     * @param tipocomprobante
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static void insert(TipoComprobante tipocomprobante) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO tipoComprobante(nombre) "
                + "VALUES ('" + tipocomprobante.getNombre() + "');";
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idTipoComprobante) FROM tipoComprobante;";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     * 
     * @param tipocomprobante
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static void update(TipoComprobante tipocomprobante) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE tipoComprobante SET "
                + "nombre = '" + tipocomprobante.getNombre() + "' "
                + "WHERE idTipoComprobante = " + tipocomprobante.getIdTipoComprobante();
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static ArrayList<TipoComprobante> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
    	String sql = "SELECT * FROM tipocomprobante WHERE UPPER(nombre) LIKE UPPER('%"+query+"%')";
        ResultSet rs = Conexion.consultar(sql);
        ArrayList <TipoComprobante> tipos;
        tipos = new ArrayList<>();
        while (rs.next()){
            tipos.add(new TipoComprobante(rs.getInt(1),rs.getString(2)));
        }
        return tipos;
    }

    /**
     * 
     * @param id
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static TipoComprobante get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM tipoComprobante WHERE idTipoComprobante = " + id;
        ResultSet rs = Conexion.consultar(sql);
        TipoComprobante tipo = null;
        if (rs.next()) {
            tipo = new TipoComprobante(rs.getInt(1),rs.getString(2));
        }
        return tipo;
    }
}