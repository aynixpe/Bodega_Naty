/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import clases.Comprobante;
import clases.Pago;
import clases.TipoPago;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author root
 */
public class MdlPago{

    /**
     * 
     * @param pago
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static void insert(Pago pago) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "INSERT INTO pago (tipoPago, monto, comprobante, estado) "
                + "VALUES(" + pago.getTipoPago().getIdTipoPago() + ", "
                + pago.getMonto() + ", "
                + pago.getComprobante().getIdComprobante() + ", "
                + "'" + pago.getEstado() + "');";
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static int getLastId() throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT MAX(idPago) FROM pago;";
        ResultSet rs = Conexion.consultar(sql);
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

    /**
     * 
     * @param pago
     * @param Conexion
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static void update(Pago pago) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "UPDATE pago SET "
                + "tipoPago = " + pago.getTipoPago().getIdTipoPago() + ", "
                + "monto = " + pago.getMonto() + ", "
                + "comprobante = " + pago.getComprobante().getIdComprobante() + ", "
                + "estado = '" + pago.getEstado() + "' "
                + "WHERE idPago = " + pago.getIdPago();
        Conexion.ejecutar(sql);
    }

    /**
     * 
     * @param query
     * @param Conexion
     * @return
     * @throws SQLException, ClassNotFoundException 
     */
    
    public static ArrayList<Pago> get(String query) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        ResultSet rs = Conexion.consultar(query);
        ArrayList<Pago> pagos = new ArrayList<>();
        while(rs.next()){
            pagos.add(new Pago(
                    rs.getInt(1),
                    new TipoPago(){{
                    	setIdTipoPago(rs.getInt(2));
                    }},
                    rs.getFloat(3),
                    new Comprobante(){{
                    	setIdComprobante(rs.getInt(4));
                    }},
                    rs.getString(5).charAt(0)
            ));
        }
        return pagos;
    }

   /**
    * 
    * @param id
    * @param Conexion
    * @return
    * @throws SQLException, ClassNotFoundException 
    */ 
    
    public static Pago get(int id) throws SQLException, ClassNotFoundException {
    	Conexion.abrirTransaccion();
        String sql = "SELECT * FROM pago WHERE idPago LIKE " + id;
        ResultSet rs = Conexion.consultar(sql);
        Pago pago = null;
        if (rs.next()) {
            pago = new Pago(
                    rs.getInt(1),
                    new TipoPago(){{
                    	setIdTipoPago(rs.getInt(2));
                    }},
                    rs.getFloat(3),
                    new Comprobante(){{
                    	setIdComprobante(rs.getInt(4));
                    }},
                    rs.getString(5).charAt(0)
            );
        }
        return pago;
    }    
}
