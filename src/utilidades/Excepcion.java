package utilidades;

@SuppressWarnings("serial")
public class Excepcion extends Exception{

private int codigo;
	
	public Excepcion(int codigo, String mensaje){
		super(mensaje);
		this.codigo = codigo;	
	}
	
	public int getCodigo() {
		return codigo;
	}
}
