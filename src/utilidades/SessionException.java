package utilidades;

@SuppressWarnings("serial")
public class SessionException extends Exception {
	private int codigo;
	
	public SessionException(int codigo, String mensaje){
		super(mensaje);
		this.codigo = codigo;	
	}
	
	public int getCodigo() {
		return codigo;
	}
}
