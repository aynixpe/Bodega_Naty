/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

import com.alee.laf.button.WebButton;
import com.alee.laf.table.WebTable;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.event.MouseAdapter;
/**
 *
 * @author Edder
 */
public final class Utilidades {
    /**
     * Da formato a moneda
     * @param monto
     * @return 
     */
	public static final String[] N_GUARDAR = {"/resources/img/icGuardar.png", "/resources/img/icGuardar_press.png"};
	public static final String[] N_EDITAR = {"/resources/img/icEditar.png", "/resources/img/icEditar_press.png"};
	public static final String[] N_ELIMINAR = {"/resources/img/icEliminar.png", "/resources/img/icEliminar_press.png"};
	public static final String[] N_CANCELAR = {"/resources/img/icCancelar.png", "/resources/img/icCancelar_press.png"};
	public static final String[] N_AGREGAR = {"/resources/img/icAgregar.png", "/resources/img/icAgregar_press.png"};
	public static final String[] N_DESCUENTO = {"/resources/img/icDescuento.png", "/resources/img/icDescuento_press.png"};
	
	public static final String[] N_IBUSCAR = {"/resources/img/oicBuscar.png", "/resources/img/oicBuscar_press.png"};
	public static final String[] N_IEDITAR = {"/resources/img/oicEditar.png", "/resources/img/oicEditar_press.png"};
	public static final String[] N_IAGREGAR = {"/resources/img/oicAgregar.png", "/resources/img/oicAgregar_press.png"};
	
	public static final String roboto_font = "/resources/fonts/roboto.ttf"; 
	
    public static String setUpMoneda(float monto){
        NumberFormat moneda = NumberFormat.getCurrencyInstance(new Locale("es", "PE"));
        return moneda.format(monto);
    }
    
    public static float getMoneda(String moneda){
    	NumberFormat m = NumberFormat.getCurrencyInstance(new Locale("es", "PE"));
        try {
			return m.parse(moneda).floatValue();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return 0f;
		}
    }
        
    public static void alinearColumna_derecha(WebTable tabla, String col){
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(DefaultTableCellRenderer.RIGHT);
        tabla.getColumn(col).setCellRenderer( rightRenderer );
    }
    
    public static void resizeColumnWidth(WebTable table) {
        TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
        	int width = 50;
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width, width);
            }
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }
    
    public static void packColumns(WebTable table, int margin) {
        for (int c=0; c<table.getColumnCount(); c++) {
          Utilidades.packColumn(table, c, 2);
        }
    }    

    public static void packColumn(WebTable table, int vColIndex, int margin) {
        DefaultTableColumnModel colModel = (DefaultTableColumnModel)table.getColumnModel();
        TableColumn col = colModel.getColumn(vColIndex);
        int width = 0;
        TableCellRenderer renderer = col.getHeaderRenderer();
        if (renderer == null) {
            renderer = table.getTableHeader().getDefaultRenderer();
        }
        Component comp = renderer.getTableCellRendererComponent(
        		table, col.getHeaderValue(), false, false, 0, 0);
        width = comp.getPreferredSize().width;
        
        for (int r=0; r<table.getRowCount(); r++) {
        	renderer = table.getCellRenderer(r, vColIndex);
            comp = renderer.getTableCellRendererComponent(
            		table, table.getValueAt(r, vColIndex), false, false, r,
            		vColIndex);
            width = Math.max(width, comp.getPreferredSize().width);
        }           
        width += 2*margin;
        col.setPreferredWidth(width);
    }
    
    public static WebButton getButton(ImageIcon imageIcon, ImageIcon pressed){
    	WebButton boton = new WebButton(imageIcon);
    	boton.setBorder(null);
    	boton.setBorderPainted(false);
    	boton.setPreferredSize(new Dimension(88,25));
    	boton.setMargin(new Insets(-7, -7, -5, -7));
    	boton.addMouseListener(new MouseAdapter(){
    		public void mousePressed(MouseEvent me){
        		boton.setIcon(pressed);
        	}        	
        	public void mouseReleased(MouseEvent me){
        		boton.setIcon(imageIcon);
        	}
    	});
    	return boton;
    }
    
    public static WebButton getIconButton(ImageIcon imageIcon, ImageIcon pressed){
    	WebButton boton = WebButton.createIconWebButton(imageIcon);
    	boton.setBorder(null);
    	boton.setBorderPainted(false);
    	boton.setMargin(new Insets(-7, -7, -5, -7));
    	boton.addMouseListener(new MouseAdapter(){
    		public void mousePressed(MouseEvent me){
        		boton.setIcon(pressed);
        	}        	
        	public void mouseReleased(MouseEvent me){
        		boton.setIcon(imageIcon);
        	}
    	});
    	return boton;
    }
    
    public static boolean validacionInteger(String numero){
    	boolean flag = false;
    	try {
			int n = Integer.parseInt(numero);
			if (n>0) {
				flag = true;
			} else if (n<0){
				flag = false;
			}
		} catch (NumberFormatException nfe) {
			// TODO: handle exception
			flag = false;
		}
    	return flag;
    }
    
    public static boolean validacionFloat(String valor) {
		boolean flag = false;
		try{
			float n = Float.parseFloat(valor);
			if(n>0) {
				flag = true;
			}else if(n<0){
				flag = false;
			}
		}catch (NumberFormatException nfe){
			flag = false;
		}
		return flag;
	}
    
    public static String md5(String cadena){
    	try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(cadena.getBytes());
			byte byteData[] = md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
	
	public static int compararFechas(Date fechaInicio, Date fechaFin){
        LocalDate fechaI = fechaInicio.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate fechaF = fechaFin.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return fechaF.toString().compareTo(fechaI.toString());
	}
	    
    public static Image getImageFromImageIcon(ImageIcon imageIcon, int size){
    	return imageIcon.getImage().getScaledInstance(size, size, Image.SCALE_DEFAULT);
    }
    
    public static Font getRobotoFont(InputStream is){
    	try {
			Font font = Font.createFont(Font.TRUETYPE_FONT, is);
			Font sizedFont = font.deriveFont(16f);
			return sizedFont;
		} catch (FontFormatException | IOException e) {
			return null;
		}
    }
}
