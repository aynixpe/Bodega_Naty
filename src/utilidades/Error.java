package utilidades;

public class Error {
	/** ERRORES DE MODELOS 
	 * 0
	 */
	public static final int ERR_CONEXION = 0;
	
	/** ERRORES DE CONTROLADORES 
	 * 100 
	 */
	public static final int ERR_SESION = 100;
	public static final int ERR_PERMISO = 101;
	public static final int ERR_ANULACION = 102; 
	/** ERRORES DE VISTAS;
	 * 200
	 */
	
	/** OTROS
	 * 300
	 */
	
}
