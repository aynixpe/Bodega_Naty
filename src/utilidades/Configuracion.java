package utilidades;

import java.io.FileInputStream;
import java.util.Properties;

public final class Configuracion {
	public static final String PROPIEDADES = "bodega.properties";
	
	public static float getIgv(){
		float igv = 0f;
		try{
			Properties properties = new Properties();
			properties.load(new FileInputStream(PROPIEDADES));
			igv = Float.parseFloat(properties.getProperty("igv"));			
		}catch(Exception e){
			igv = 0f;
		}
		return igv;
	}
	
	public static String[] getDB(){
		String[] dataConexion = new String[4];
		try{
			Properties properties = new Properties();
			properties.load(new FileInputStream(PROPIEDADES));
			dataConexion[0] = properties.getProperty("host");
			dataConexion[1] = properties.getProperty("db");
			dataConexion[2] = properties.getProperty("user");
			dataConexion[3] = properties.getProperty("pass");
		}catch(Exception e){
			dataConexion = null;
		}
		return dataConexion;
	}
	
	public static int getIdCaja(){
		int idCaja = -1;
		try{
			Properties properties = new Properties();
			properties.load(new FileInputStream(PROPIEDADES));
			idCaja = Integer.parseInt(properties.getProperty("caja"));
		}catch(Exception e){
			idCaja = -1;
		}
		return idCaja;
	}
	
	public static int getIdAlmacen(){
		int idAlmacen = -1;
		try{
			Properties properties = new Properties();
			properties.load(new FileInputStream(PROPIEDADES));
			idAlmacen = Integer.parseInt(properties.getProperty("almacen"));
		}catch(Exception e){
			idAlmacen = -1;
		}
		return idAlmacen;
	}
}
