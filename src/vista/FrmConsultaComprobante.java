package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import clases.Caja;

import com.alee.extended.date.WebDateField;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.splitpane.WebSplitPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.text.WebTextField;

import controlador.CtrlCaja;
import controlador.CtrlComprobante;
import utilidades.Utilidades;

@SuppressWarnings("serial")
public class FrmConsultaComprobante extends WebFrame{
	private WebDateField txtFechaInicio, txtFechaFin;
	private WebTextField txtCliente;
	private WebComboBox cboCaja;
	private WebTable tblComprobante,
					 tblDetalles;
	private WebButton btnAnular,
	                  btnCerrar,
	                  btnBuscar;
	private WebCheckBox chbAnulados;

	public FrmConsultaComprobante() {
		setTitle("Registro de Comprobantes");
		setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
		setShowResizeCorner(false);
		setSize(1000, 550);
		setLocationRelativeTo(null);
		initComponents();
	}

	private void initComponents(){
		txtFechaInicio = new WebDateField(new Date());
		txtFechaFin = new WebDateField(new Date());
		txtCliente = new WebTextField();
		cboCaja = new WebComboBox();
		tblComprobante = new WebTable();
		tblDetalles = new WebTable();
		btnAnular = new WebButton("Anular");
		btnCerrar = new WebButton("Cerrar");
		btnBuscar = new WebButton("Buscar");
		chbAnulados = new WebCheckBox("Anulados");
		
		txtCliente.setInputPrompt("Búsqueda");
		cargarCajas();
		buscar();
		
		add(new WebPanel(){{
			setBackground(Color.white);
			setLayout(new BorderLayout());
			add(new GroupPanel(){{
				setLayout(new GridBagLayout());
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.weightx = 0.2;
				add(new GroupPanel(){{
					add(new WebLabel("Inicio"));
					add(txtFechaInicio);					
				}}, gbc);
				
				gbc.gridx = 1;
				add(new GroupPanel(){{
					add(new WebLabel("Fin"));
					add(txtFechaFin);					
				}}, gbc);
				
				gbc.gridx = 2;
				add(new GroupPanel(){{					
					add(new WebLabel("Caja"));
					add(cboCaja);
				}}, gbc);
				
				gbc.gridx = 3;
				add(chbAnulados, gbc);
				
				gbc.gridx = 4;
				add(new GroupPanel(){{
					setLayout(new BorderLayout());
					add(btnBuscar, BorderLayout.EAST);
				}}, gbc);
			}}, BorderLayout.NORTH);
			add(new WebSplitPane(
					WebSplitPane.HORIZONTAL_SPLIT,
					new WebScrollPane(tblComprobante),
					new WebScrollPane(tblDetalles)){{
						setDividerLocation(700);
					}}
			, BorderLayout.CENTER);
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				add(btnAnular, BorderLayout.WEST);
				add(btnCerrar, BorderLayout.EAST);
			}}, BorderLayout.SOUTH);
		}});
		
		ListSelectionModel tblSelect = tblComprobante.getSelectionModel();
		tblSelect.addListSelectionListener((ListSelectionEvent e) ->{
			configTablaDetalles();	
		});
		
		btnBuscar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buscar();
			}
		});
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent we){
				FrmPrincipal.bolFrmConsultaComprobante = false;
			}
		});

	}

	@SuppressWarnings("unchecked")
	private void cargarCajas(){
		try{
			ArrayList<Caja> cajas = CtrlCaja.get("", false);
			cajas.add(0, new Caja(){{setNombre("Todas");}});

			cboCaja.removeAllItems();
			@SuppressWarnings("rawtypes")
			DefaultComboBoxModel model = new DefaultComboBoxModel(cajas.toArray());
			cboCaja.setModel(model);
		}catch(Exception e){

		}
	}

	private void buscar(){
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Comprobante");
		model.addColumn("Cliente");
		model.addColumn("Monto");
		model.addColumn("IGV");
		model.addColumn("Fecha");
		model.addColumn("Usuario");
		model.addColumn("Est");
		model.addColumn("Caja");
		tblComprobante.setModel(model);
		tblComprobante.setEditable(false);
		
		try{
			Caja caja = cboCaja.getSelectedIndex() == 0 ? null : (Caja)cboCaja.getSelectedItem();
			LocalDate fechaInicio = (txtFechaInicio.getDate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			LocalDate fechaFin = (txtFechaFin.getDate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			ArrayList<Object[]> comprobantes = CtrlComprobante.getConsulta(
					fechaInicio.toString(), fechaFin.toString(), txtCliente.getText(), caja, false);
			for(Object[] comprobante : comprobantes){
				comprobante[3] = utilidades.Utilidades.setUpMoneda((float)comprobante[3]);
				comprobante[4] = utilidades.Utilidades.setUpMoneda((float)comprobante[4]);
				model.addRow(comprobante);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		Utilidades.packColumns(tblComprobante, 0);
	}

	private void configTablaDetalles(){
		DefaultTableModel model = new DefaultTableModel();
		tblDetalles.setModel(model);
		model.addColumn("ID");
		model.addColumn("Articulo");
		model.addColumn("Cant");
		model.addColumn("S/.");
		model.addColumn("IGV");
		int row = tblComprobante.getSelectedRow();
		if(row >= 0){
			int id = (int)tblComprobante.getValueAt(row, 0);
			try{
				ArrayList<Object[]> detallesOC = CtrlComprobante.getConsulta_Detalle(id, false);
				for (Object[] detalle : detallesOC) {
					detalle[3] = utilidades.Utilidades.setUpMoneda((float)detalle[3]);
					detalle[4] = utilidades.Utilidades.setUpMoneda((float)detalle[4]);
					model.addRow(detalle);
				}				
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		tblDetalles.removeColumn(tblDetalles.getColumnModel().getColumn(0));
		Utilidades.packColumns(tblDetalles, 0);
		tblDetalles.setEditable(false);
	}
}
