package vista;

import clases.Almacen;
import clases.Articulo;
import clases.Categoria;

import com.alee.extended.list.CheckBoxListModel;
import com.alee.extended.list.WebCheckBoxList;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.splitpane.WebSplitPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.text.WebTextField;

import controlador.CtrlAlmacen;
import controlador.CtrlArticulo;
import controlador.CtrlCategoria;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

import utilidades.Utilidades;

/**
 *
 * @author Edder
 */
@SuppressWarnings("serial")
public class FrmArticulos extends WebFrame{
    private WebTextField txtBusqueda;
    private WebCheckBox chbInactivos;
    private WebCheckBoxList chbLstCategorias;
    private WebTable tblArticulos;
    private WebTable tblStocks;
    private WebButton btnBuscar, btnAgregar, btnEditar, btnCancelar, btnAsignar;
    
    FrmArticulos(){
        setTitle("Artículos");
        setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
        setSize(900,600);
        setShowResizeCorner(false);
        initComponents();
        setLocationRelativeTo(null);
    }
    
    private void initComponents(){
        
        txtBusqueda = new WebTextField ();
        txtBusqueda.setInputPrompt ( "Búsqueda" );
        txtBusqueda.setInputPromptFont ( txtBusqueda.getFont ().deriveFont ( Font.ITALIC ) );
        
        chbInactivos = new WebCheckBox("Inactivos");
        tblArticulos = new WebTable();
        tblStocks = new WebTable();
        
        btnBuscar = Utilidades.getIconButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[1]))
        		);
        btnAgregar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_AGREGAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_AGREGAR[1]))
        		);
        btnEditar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_EDITAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_EDITAR[1]))
        		);
        btnCancelar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[1]))
        		);
        
        btnAsignar = new WebButton("Asignar");
        
        cargarCategorias();
        
        add(new WebPanel(){{
        	setBackground(Color.white);
        	setLayout(new BorderLayout());
        	
        	add(new GroupPanel(){{
        		setLayout(new BorderLayout());
        		add(new WebLabel("Búsqueda"), BorderLayout.WEST);
        		add(txtBusqueda, BorderLayout.CENTER);
        		add(new GroupPanel(){{
        			add(chbInactivos);
        			add(btnBuscar);
        		}}, BorderLayout.EAST);
        	}}, BorderLayout.NORTH);
        	
        	add(new WebScrollPane(chbLstCategorias), BorderLayout.WEST);
        	
        	add(new WebSplitPane(
        			WebSplitPane.HORIZONTAL_SPLIT,
        			new WebScrollPane(tblArticulos),
        			new WebScrollPane(tblStocks)
        	), BorderLayout.CENTER);
        	
        	add(new GroupPanel(){{
        	}}, BorderLayout.EAST);
        	
        	add(new GroupPanel(){{
        		setLayout(new FlowLayout(FlowLayout.RIGHT));
        		add(btnAsignar, btnAgregar, btnEditar, btnCancelar);
        	}}, BorderLayout.SOUTH);
        }});

		buscar();
		cargarStocks();
        
        txtBusqueda.addActionListener((ActionEvent e) -> {
            buscar();
        });
        
        btnBuscar.addActionListener((ActionEvent e )->{
        	buscar();
        });
        
        ListSelectionModel tblSelect = tblArticulos.getSelectionModel();
		tblSelect.addListSelectionListener((ListSelectionEvent e) ->{
			cargarStocks();	
		});
               
        btnAgregar.addActionListener((ActionEvent e)->{
            FrmArticulo fa = new FrmArticulo(this, false, null);
            fa.setVisible(true);
            
        });
                
        btnEditar.addActionListener((ActionEvent e) -> {
        	 //Si no se ha seleccionado, devuelve -1, entonces carga por defecto el primer elemento
        	int row = tblArticulos.getSelectedRow() == -1 ? 0: tblArticulos.getSelectedRow();
        	try{
	            Articulo articulo = CtrlArticulo.get((int) tblArticulos.getModel().getValueAt(row, 0), false);
	            FrmArticulo fa = new FrmArticulo(
	                    this, 
	                    false, 
	                    articulo);
	            fa.setVisible(true);
        	}catch(Exception ex){
        		WebOptionPane.showMessageDialog(this, "Error al cargar el artículo, intente nuevamente", "Error interno",
        				WebOptionPane.ERROR_MESSAGE);
        	}
	            
        });
        
        btnAsignar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				asignarAlmacen();
			}
		});
                
        this.btnCancelar.addActionListener((ActionEvent e)->{
            this.dispose();
        });
                
        this.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent evt){
                FrmPrincipal.bolFrmArticulos = false;
            }
        });
    }
    
    public void buscar(){
        try {            
            ArrayList<Articulo> articulos = CtrlArticulo.getArticulos_categorias(
                    txtBusqueda.getText(),
                    getSelected(),
                    chbInactivos.isSelected(),
                    false
                    );
            DefaultTableModel model = new DefaultTableModel();
            this.tblArticulos.setModel(model);
            model.addColumn("ID");
            model.addColumn("Nombre");
            model.addColumn("Precio");
            for(Articulo articulo : articulos){
            	model.addRow(new Object[]{
            			articulo.getIdArticulo(),
            			articulo.toString(),
            			Utilidades.setUpMoneda(articulo.getPrecioVenta())
            	});
            	
            }
//            tblArticulos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            Utilidades.alinearColumna_derecha(tblArticulos, "Precio");
            Utilidades.packColumns(tblArticulos, 0);
            tblArticulos.setEditable(false);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }    
    
    private ArrayList<Categoria> getSelected(){
        ArrayList<Categoria> categorias = new ArrayList<>();
        CheckBoxListModel model = chbLstCategorias.getCheckBoxListModel();        
        for(int i=1; i < model.getSize(); i++){
        	if(model.get(0).isSelected() || model.get(i).isSelected())
                categorias.add((Categoria) model.getElementAt(i).getUserObject());
        }
        return categorias;
    }
    
    private void cargarCategorias(){
        CheckBoxListModel model = new CheckBoxListModel();
        model.addCheckBoxElement("Todos");
        try {
            CtrlCategoria.getCategorias("", false).stream().forEach((cat) -> {
                model.addCheckBoxElement(cat);
            });
        }catch(Exception ex){
            Logger.getLogger(FrmArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        chbLstCategorias = new WebCheckBoxList(model);
    }
    
    public void cargarStocks(){
        try {
            DefaultTableModel model = new DefaultTableModel();
            this.tblStocks.setModel(model);
            model.addColumn("Almacén");
            model.addColumn("Stock");
            int row = tblArticulos.getSelectedRow();
            if(row >= 0){
            	int idArticulo = (int)tblArticulos.getValueAt(row, 0);
            	ArrayList<Object[]> stocks = CtrlArticulo.getStockforArticuloInAlmacen(idArticulo, false);
                stocks.stream().forEach((stock)->{
                    model.addRow(stock);
                });
            }
            Utilidades.resizeColumnWidth(this.tblStocks);
        }catch(Exception ex){
            Logger.getLogger(FrmArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void asignarAlmacen(){
    	int[] rows = tblArticulos.getSelectedRows();
    	if(rows.length > 0){    		
    		ArrayList<Integer> ids = new ArrayList<>();
    		ShowAlmacen alm = new ShowAlmacen(this, true);
    		alm.setVisible(true);
    		
    		Almacen almacen = alm.getAlmacen();
    		if(almacen != null){
    			for(int i = 0; i < rows.length; i++){
    				ids.add((int)tblArticulos.getValueAt(rows[i], 0));
    			}
    			
    			try {
    				CtrlArticulo.asignarAlmacen(ids, almacen.getIdAlmacen(), false);
    				WebOptionPane.showMessageDialog(this,  
    						"La información se registró correctamente",
    						"Éxito",
    						WebOptionPane.INFORMATION_MESSAGE);
    			}catch(Exception ex){
    				WebOptionPane.showMessageDialog(this, "Error al intentar guardar la información",
    						"Error", WebOptionPane.ERROR_MESSAGE);
    				ex.printStackTrace();
    			}    			
    		}
    	}
    }
    
    class ShowAlmacen extends WebDialog{
    	private WebComboBox cboAlmacen;
    	private WebButton btnAceptar;
    	private boolean flag = false;
    	
    	public ShowAlmacen(WebFrame owner, boolean modal){
    		super(owner, modal);
    		setDefaultCloseOperation(WebDialog.HIDE_ON_CLOSE);
    		setSize(100,100);
    		initComponents();
    		setLocationRelativeTo(null);
    	}
    	
    	private void initComponents(){
    		cboAlmacen = new WebComboBox();
    		btnAceptar = new WebButton("Aceptar");
    		cargarAlmacenes();
    		
    		add(new WebPanel(){{
    			setBackground(Color.white);
    			setLayout(new BorderLayout());    			
    			add(new WebLabel("Almacén"), BorderLayout.WEST);
    			add(cboAlmacen, BorderLayout.CENTER);
    			add(new GroupPanel(){{
    				setLayout(new BorderLayout());
    				add(btnAceptar, BorderLayout.EAST);
    			}}, BorderLayout.SOUTH);
    		}});
    		pack();
    		
    		btnAceptar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					flag = true;
					setVisible(false);					
				}
			});
    	}
    	
    	@SuppressWarnings({ "rawtypes", "unchecked" })
		private void cargarAlmacenes(){
    		try{
    			ArrayList<Almacen> almacenes = CtrlAlmacen.getAlmacenes("", false);
    			cboAlmacen.removeAllItems();
    			DefaultComboBoxModel<Almacen> model = new DefaultComboBoxModel(almacenes.toArray());
    			cboAlmacen.setModel(model);
    		}catch(Exception ex){
    			ex.printStackTrace();
    		}
    		
    	}
    	
    	public Almacen getAlmacen(){
    		return flag ? (Almacen)cboAlmacen.getSelectedItem() : null;
    	}
    }
}
