package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;

import utilidades.Utilidades;
import clases.TipoUsuario;

import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;

@SuppressWarnings("serial")
public class ItmTipoUsuario extends WebPanel{

	private final TipoUsuario tipoUsuario;
	private WebLabel lblTipo;
	private WebButton btnEditar;
	public ItmTipoUsuario(WebFrame owner, TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
		initComponents();
	}

	private void initComponents(){
		setBackground(Color.WHITE);
		setMaximumSize(new Dimension(600,35));
		lblTipo = new WebLabel();
		lblTipo.setText(this.tipoUsuario.getNombre());
		btnEditar = Utilidades.getIconButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_IEDITAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_IEDITAR[1])));
		setLayout(new BorderLayout());
        add(lblTipo, BorderLayout.CENTER);
        add(new WebPanel(){{
            setBackground(new Color(0,0,0,0));
            setLayout(new FlowLayout(FlowLayout.RIGHT));
            add(btnEditar);
        }}, BorderLayout.EAST);
        btnEditar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				btnEditar.setBackground(new Color(79,192,232));
			}
		});

        addMouseListener(new MouseListener(){
			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {
		        setBackground(new Color(79,192,232));
		        lblTipo.setForeground(Color.white);
		    }

		    @Override
		    public void mouseExited(MouseEvent e) {
		        setBackground(Color.white);
		        lblTipo.setForeground(Color.black);
		    }

		});
	}
}
