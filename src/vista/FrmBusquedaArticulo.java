/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import clases.Almacen;
import clases.Articulo;

import com.alee.laf.button.WebButton;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.text.WebTextField;

import controlador.CtrlArticulo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import utilidades.Utilidades;

/**
 *
 * @author togapaulo
 */
@SuppressWarnings("serial")
public class FrmBusquedaArticulo extends WebDialog{

    private WebTextField txtBusqueda;
    private WebTable tblArticulos;
    private WebButton btnAceptar;
    private Almacen almacen;
    private Articulo articulo;
    private int stock;
    private boolean showStock, verificarStock;
    
    FrmBusquedaArticulo(WebFrame owner, boolean modal, 
    		String busqueda, Almacen almacen, boolean showStock, boolean verificarStock){
    	super(owner, modal);
        setTitle("Artículos");
        setDefaultCloseOperation(WebDialog.DISPOSE_ON_CLOSE);
        setSize(400, 600);
        setShowResizeCorner(false);
        setShowMaximizeButton(false);
        this.showStock = showStock;
        this.verificarStock = verificarStock;
        initComponents(busqueda, almacen);
        setLocationRelativeTo(null);
    }
    
    private void initComponents(String busqueda, Almacen almacen) {
        this.almacen = almacen;
        this.txtBusqueda = new WebTextField(15);
        this.txtBusqueda.setText(busqueda);
        this.tblArticulos = new WebTable();
        this.btnAceptar = new WebButton("Aceptar");

        add(new WebPanel(){
            {
                setLayout(new BorderLayout());
                add(txtBusqueda, BorderLayout.NORTH);
                add(
                        new WebScrollPane(tblArticulos){
                            {
                                this.setMinimumHeight(200);
                            }
                        }
                        , BorderLayout.CENTER);
                add(btnAceptar, BorderLayout.SOUTH);
            }
        });
        llenarTabla(busqueda, this.almacen);
        this.txtBusqueda.addActionListener((ActionEvent e) -> {
            llenarTabla(this.txtBusqueda.getText(), this.almacen);
        });
        
        btnAceptar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				seleccionar();
			}
        	
        });
        
        tblArticulos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e){
                if (e.getClickCount() == 2) {
                    seleccionar();
                }
            }
        });
    }
    
    private void seleccionar(){
    	try {
            int row = tblArticulos.getSelectedRow();
            articulo = CtrlArticulo.get((int) tblArticulos.getModel().getValueAt(row, 0), false);
            
            if(verificarStock){
            	stock = CtrlArticulo.getArticuloStock(articulo.getIdArticulo(), almacen.getIdAlmacen(), false);
				if (stock == 0) {
	            	WebOptionPane.showMessageDialog(
	                    null,
	                    "No hay stock para descargar",
	                    "Error",
	                    WebOptionPane.ERROR_MESSAGE);
	            	articulo = null;
	            	stock = -1;
				}
            }else{
            	stock = 100000;
            }
    	}catch(Exception ex){
            articulo = null;
        	stock = -1;
        }finally{
			setVisible(false);	            	
        }
    }
    
    private void llenarTabla(String busqueda, Almacen almacen){
        try {
            if (showStock) {
                ArrayList<Object[]> artStocks = CtrlArticulo.getArticulosStock_almacen(almacen.getIdAlmacen(), busqueda, false);
                DefaultTableModel model = new DefaultTableModel();
                this.tblArticulos.setModel(model);
                model.addColumn("ID");
                model.addColumn("Nombre");
                model.addColumn("Stock");
                for(Object[] artStock : artStocks){
                    model.addRow(artStock);
                }
                this.tblArticulos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                Utilidades.resizeColumnWidth(tblArticulos);
                this.tblArticulos.setEditable(false);
            }
            else{
                ArrayList<Articulo> articulos = CtrlArticulo.getArticulos(busqueda, false);
                DefaultTableModel model = new DefaultTableModel();
                this.tblArticulos.setModel(model);
                model.addColumn("idArticulo");
                model.addColumn("Nombre");
                articulos.stream().forEach((articulo) -> {
                    model.addRow(new Object[]{
                        articulo.getIdArticulo(),
                        articulo.toString(),
                    });
                });
                this.tblArticulos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                this.tblArticulos.removeColumn(this.tblArticulos.getColumnModel().getColumn(0));
                Utilidades.resizeColumnWidth(tblArticulos);
                this.tblArticulos.setEditable(false);
            }
        }catch(Exception ex){
            Logger.getLogger(FrmBusquedaArticulo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    public Articulo getArticulo(){
    	return articulo;
    }
    
    public int getStock(){
    	return stock;
    }
}