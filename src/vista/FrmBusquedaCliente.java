package vista;

import clases.Cliente;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

import utilidades.Utilidades;

import com.alee.laf.button.WebButton;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.text.WebTextField;

import controlador.CtrlCliente;

@SuppressWarnings("serial")
public class FrmBusquedaCliente extends WebDialog implements ActionListener{
	private WebTextField txtCliente;
	private WebTable tblClientes;
	private WebButton btnAceptar;
	
	private Cliente cliente;
	
	public FrmBusquedaCliente(WebFrame frame, boolean modal){
		super(frame, modal);
		setTitle("Clientes");
		setDefaultCloseOperation(WebDialog.HIDE_ON_CLOSE);
		setResizable(false);
		setSize(400, 400);
		setShowResizeCorner(false);
		setShowMinimizeButton(false);
		initComponents();
	}
	
	private void initComponents(){
		
		txtCliente = new WebTextField();
		tblClientes = new WebTable();
		llenarTabla("");
		btnAceptar = new WebButton("Aceptar");
		btnAceptar.addActionListener(this);
		
		setLayout(new BorderLayout());
		add(txtCliente, BorderLayout.NORTH);
		add(
				new WebScrollPane(tblClientes){
					{
						this.setMinimumHeight(200);
					}
				}, BorderLayout.CENTER);
		this.add(btnAceptar, BorderLayout.SOUTH);
		
		this.txtCliente.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				llenarTabla(txtCliente.getText());
			}
			
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==btnAceptar) {
			int row = tblClientes.getSelectedRow();
			try {
				cliente = CtrlCliente.get((int)this.tblClientes.getModel().getValueAt(row, 0), false);
				setVisible(false);
			}catch(Exception e1){
				e1.printStackTrace();
			}
		}
	}
	
	private void llenarTabla(String busqueda){
		try {
			ArrayList<Cliente> clientes = CtrlCliente.getClientes(busqueda, false);
			DefaultTableModel modelo = new DefaultTableModel();
			this.tblClientes.setModel(modelo);
			modelo.addColumn("idCliente");
			modelo.addColumn("DNI");
			modelo.addColumn("Nombre");
			for(Cliente cliente : clientes){
				modelo.addRow(new Object[]{
						cliente.getIdCliente(),
						cliente.getDni(),
						cliente.getNombre()
				});
			}
			this.tblClientes.removeColumn(this.tblClientes.getColumnModel().getColumn(0));
	        Utilidades.resizeColumnWidth(this.tblClientes);
	        this.tblClientes.setEditable(false);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public Cliente getCliente(){
		return cliente;
	}
}
