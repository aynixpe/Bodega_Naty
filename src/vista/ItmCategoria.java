/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import clases.Categoria;

import com.alee.extended.image.WebDecoratedImage;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

import utilidades.Utilidades;

/**
 *
 * @author Edder
 */
@SuppressWarnings("serial")
public class ItmCategoria extends WebPanel implements ActionListener{
    private final Categoria categoria;
    private final FrmCategorias parent;
    private WebLabel lblCategoria;
    private WebDecoratedImage imgIcono;
    private WebButton btnEditar;
    
    public ItmCategoria(FrmCategorias parent, Categoria categoria){
        this.parent = parent;
        this.categoria = categoria;
        initComponents();
    }
    
    /**
     * Inicializa los componentes del panel
     */
    private void initComponents(){
        setBackground(Color.white);
        setMaximumSize(new Dimension(600, 35));
        lblCategoria = new WebLabel();
        imgIcono = new WebDecoratedImage();
        
        btnEditar = Utilidades.getIconButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_IEDITAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_IEDITAR[1])));
        
        cargarCategoria();
        
        setBackground(Color.white);
        setLayout(new BorderLayout());
        add(imgIcono, BorderLayout.WEST);
        add(lblCategoria, BorderLayout.CENTER);
        add(new GroupPanel(){{
        	setBackground(new Color(0,0,0,0));
        	setLayout(new FlowLayout(FlowLayout.RIGHT));
        	add(btnEditar);
        }}, BorderLayout.EAST);
        
        btnEditar.addActionListener(this);
        addMouseListener(new MouseAdapter(){
        	@Override
        	public void mouseEntered(MouseEvent e) {
                setBackground(new Color(79,192,232));
                lblCategoria.setForeground(Color.white);
            }

            @Override
            public void mouseExited(MouseEvent e) { 
                setBackground(Color.white);
                lblCategoria.setForeground(Color.black);
            }
        });
    }
    
    private void cargarCategoria(){
    	lblCategoria.setText(categoria.getNombre());
    	ImageIcon img = categoria.getImagen() != null ? 
    			new ImageIcon(categoria.getImagen().getAbsolutePath()) :
    			new ImageIcon(getClass().getResource("/resources/img/categoria_default.png")); 
    	imgIcono.setImage(
    			Utilidades.getImageFromImageIcon(img, 25),
    			true);	
    }

    /**
     * Abre Dialog para editar categoria,
     * luego actualiza la información a mostrar
     */
    private void editarCategoria(){
        FrmCategoria fc = new FrmCategoria(null, true, categoria);
        fc.setVisible(true);
        this.parent.llenarCategorias();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.btnEditar){
            editarCategoria();
        }
    }
}
