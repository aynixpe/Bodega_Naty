package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.SpringLayout;

import clases.Almacen;
import clases.Caja;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.text.WebPasswordField;
import com.alee.laf.text.WebTextField;

import controlador.CtrlAlmacen;
import controlador.CtrlCaja;
import controlador.CtrlSesion;

@SuppressWarnings("serial")
public class FrmInicioSesion extends WebFrame {
	private WebTextField txtUsuario;
	private WebPasswordField txtPassword;
	private WebButton btnIngresar;
	private WebComboBox cboAlmacen, cboCaja;
	private WebCheckBox chbOpcionAvanzada;
	
	public FrmInicioSesion(){
        setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(300, 300));
        setShowResizeCorner(false);
        setShowMaximizeButton(false);
        initComponents();
        setLocationRelativeTo(null);
	}
	
	private void initComponents(){
		cboCaja = new WebComboBox();
		cboAlmacen = new WebComboBox();
		txtUsuario = new WebTextField("esanchez");
		txtPassword = new WebPasswordField("123456");
		btnIngresar = new WebButton("Ingresar");
		chbOpcionAvanzada = new WebCheckBox("Avanzadas");
		
		cargarAlmacenes();
		cargarCajas();
		
		add(new WebPanel(){{
			setBackground(Color.WHITE);
			setLayout(new BorderLayout());
			add(new WebPanel(){{
				add(new WebLabel("Información"));
			}}, BorderLayout.CENTER);
			
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				add(new GroupPanel(){{
					setLayout(new SpringLayout());
					add(new WebLabel("Usuario"));
					add(txtUsuario);
					add(new WebLabel("Contraseña"));
					add(txtPassword);
					add(chbOpcionAvanzada);
					add(new WebLabel(""));
					add(new WebLabel("Caja"));
					add(cboCaja);
					add(new WebLabel("Almacén"));
					add(cboAlmacen);
					
					SpringUtilities.makeCompactGrid(this,
							5, 2,
							6, 6,
							6, 6);
				}}, BorderLayout.CENTER);
				
				add(new WebPanel(){{
					setLayout(new FlowLayout());
					add(btnIngresar);
				}}, BorderLayout.SOUTH);
			}}, BorderLayout.SOUTH);
		}});
		
		txtPassword.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				login();
			}
		});
		
		btnIngresar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				login();
			}
		});
		
		chbOpcionAvanzada.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cboAlmacen.setEnabled(chbOpcionAvanzada.isSelected());
				cboCaja.setEnabled(chbOpcionAvanzada.isSelected());
			}
		});
	}
	
	private void cargarAlmacenes(){
		ArrayList<Almacen> almacenes;
		try {
			almacenes = CtrlAlmacen.getAlmacenes("", false);
			cboAlmacen = new WebComboBox(almacenes.toArray());
			cboAlmacen.setEnabled(false);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void cargarCajas(){
		ArrayList<Caja> cajas;
		try{
			cajas = CtrlCaja.get("", false);
			cboCaja = new WebComboBox(cajas.toArray());
			cboCaja.setEnabled(false);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void login(){
		Caja caja = chbOpcionAvanzada.isSelected() ? (Caja) cboCaja.getSelectedItem() : null;
		Almacen almacen = chbOpcionAvanzada.isSelected() ? (Almacen) cboAlmacen.getSelectedItem() : null;
		
		try {
			if(CtrlSesion.login(
					txtUsuario.getText(),
					String.copyValueOf(txtPassword.getPassword()),
					caja,
					almacen)){
				setVisible(false);
				FrmPrincipal fp = new FrmPrincipal();
				fp.setVisible(true);
			}else
				WebOptionPane.showMessageDialog(this, "Usuario y/o contraseña incorrectos",
        				"Error de inicio de sesión", WebOptionPane.ERROR_MESSAGE);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
