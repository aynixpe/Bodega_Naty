package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import utilidades.Configuracion;
import utilidades.Excepcion;
import utilidades.SessionException;
import utilidades.Utilidades;
import clases.Almacen;
import clases.AlmacenArticulo;
import clases.Compra;
import clases.DetalleCompra;
import clases.OrdenCompra;
import clases.Proveedor;

import com.alee.extended.button.WebSwitch;
import com.alee.extended.date.DateSelectionListener;
import com.alee.extended.date.WebDateField;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.spinner.WebSpinner;
import com.alee.laf.splitpane.WebSplitPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.text.WebTextField;

import controlador.CtrlArticulo;
import controlador.CtrlCompra;
import controlador.CtrlOrdenCompra;
import controlador.CtrlProveedor;
import controlador.CtrlAlmacen;
import controlador.CtrlSesion;

@SuppressWarnings("serial")
public class FrmCompra extends WebFrame{
	private WebTextField txtOrdenCompra,
						txtArticulo,
						txtFactura;
	private WebDateField txtFecha;
	private WebTable tblArticulos,
					 tblArticulosCompra;
	private WebComboBox cboProveedor, cboAlmacen;
	private WebSwitch btnConOrden;
	private WebButton btnBuscarOrden,
					btnBuscarArticulo,
					btnAgregar,
					btnBorrar,
					btnEditar,
					btnGuardar;
	private WebLabel lblIdCompra, lblMonto;
	private GroupPanel pnlOrden;

	private Compra compra;
	private OrdenCompra ordenCompra;
	private ArrayList<DetalleCompra> detalles;

	public FrmCompra() {
        setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
        setSize(900, 500);
        setTitle("Compra");
        setShowResizeCorner(false);
        setLocationRelativeTo(null);
		initComponents();
	}

	private void initComponents(){

		GroupPanel pnlArticulos = new GroupPanel();
		GroupPanel pnlArticulosCompra = new GroupPanel();

		lblIdCompra = new WebLabel("ID: ");
		lblMonto = new WebLabel("S/. 0.0");
		pnlOrden = new GroupPanel();
		txtOrdenCompra = new WebTextField(5);
		txtArticulo = new WebTextField();
		txtFactura = new WebTextField(8);
		txtFecha = new WebDateField(new Date());
		tblArticulos = new WebTable();
		tblArticulosCompra = new WebTable();
		cboProveedor = new WebComboBox();
		cboAlmacen = new WebComboBox();
		btnConOrden = new WebSwitch();
		btnBuscarOrden = Utilidades.getIconButton(
			new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[0])),
			new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[1]))
		);
		btnBuscarArticulo = Utilidades.getIconButton(
			new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[0])),
			new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[1]))
		);
		btnAgregar = new WebButton("Agregar");
		btnBorrar = new WebButton("Borrar");
		btnEditar = new WebButton("Editar");
		btnGuardar = new WebButton("Guardar");
		detalles = new ArrayList<DetalleCompra>();
		ordenCompra = null;

		btnConOrden.getLeftComponent().setText("Sí");
		btnConOrden.getRightComponent().setText("No");

		conOrden(btnConOrden.isSelected());
		llenarProveedor();
		llenarAlmacen();

		configTablaArticulos();
		configTablaDetalles();

		add(new WebPanel(){{
			setBackground(Color.white);
			setLayout(new BorderLayout());

			//Cabecera
			add(new GroupPanel(){{
				setLayout(new GridLayout(2,1));
				add(new GroupPanel(){{
					setLayout(new GridBagLayout());
					GridBagConstraints gbc = new GridBagConstraints();
					gbc.gridx = 0;
					gbc.gridy = 0;
					gbc.gridheight = 1;
					gbc.weightx = 0.55;
					gbc.fill = GridBagConstraints.HORIZONTAL;

					add(new GroupPanel(){{
						add(new WebLabel("Compra con orden"));
						add(btnConOrden);
						add(pnlOrden);
					}}, gbc);

					gbc.gridx = 1;
					gbc.weightx = 0.15;
					add(new GroupPanel(){{
						add(new WebLabel("Fecha"));
						add(txtFecha);
					}}, gbc);

					gbc.gridx = 2;
					add(new GroupPanel(){{
						add(new WebLabel("Proveedor"));
						add(cboProveedor);
					}}, gbc);

					gbc.gridx = 3;
					add(new GroupPanel(){{
						add(new WebLabel("Almacén"));
						add(cboAlmacen);
					}}, gbc);
				}});
				add(new GroupPanel(){{
					setLayout(new GridLayout(1,2));
					add(new GroupPanel(){{
						setLayout(new BorderLayout());
						add(new WebLabel("Artículo"), BorderLayout.WEST);
						add(txtArticulo, BorderLayout.CENTER);
						add(btnBuscarArticulo, BorderLayout.EAST);
					}});
					add(new GroupPanel(){{
						setLayout(new BorderLayout());
						add(new GroupPanel(){{
							add(new WebLabel("Factura Nº"));
							add(txtFactura);
						}}, BorderLayout.EAST);
					}});
				}});
			}}, BorderLayout.NORTH);

			//Tablas
			add(new WebSplitPane(WebSplitPane.HORIZONTAL_SPLIT,
					pnlArticulos,
					pnlArticulosCompra){{
						setDividerLocation(350);
					}}, BorderLayout.CENTER);

			pnlArticulos.setLayout(new BorderLayout());
			pnlArticulos.add(new WebScrollPane(tblArticulos), BorderLayout.CENTER);
			pnlArticulos.add(new GroupPanel(){{
				setLayout(new GridLayout(2,1));
				add(new GroupPanel(){{
					add(lblIdCompra);
				}});
				add(new GroupPanel(){{
					setLayout(new BorderLayout());
					add(btnAgregar, BorderLayout.EAST);
				}});
			}}, BorderLayout.SOUTH);

			pnlArticulosCompra.setLayout(new BorderLayout());
			pnlArticulosCompra.add(new WebScrollPane(tblArticulosCompra), BorderLayout.CENTER);
			pnlArticulosCompra.add(new GroupPanel(){{
				setLayout(new GridLayout(2, 1));
				add(new GroupPanel(){{
					add(lblMonto);
				}});
				add(new GroupPanel(){{
					setLayout(new BorderLayout());
					add(new GroupPanel(){{
						add(btnBorrar);
						add(btnEditar);
					}}, BorderLayout.WEST);
					add(btnGuardar, BorderLayout.EAST);
				}});
			}}, BorderLayout.SOUTH);
		}});

		pnlOrden.setLayout(new BorderLayout());
		pnlOrden.add(new WebLabel("Nº Orden"), BorderLayout.WEST);
		pnlOrden.add(txtOrdenCompra, BorderLayout.CENTER);
		pnlOrden.add(btnBuscarOrden, BorderLayout.EAST);
		
		actualizarMonto();
		
		btnConOrden.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				conOrden(btnConOrden.isSelected());
			}
		});
		
		txtArticulo.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				configTablaArticulos();
			}
		});
		
		txtOrdenCompra.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				cargarOrden_id();
			}
		});
		
		tblArticulosCompra.getModel().addTableModelListener(new TableModelListener(){
			@Override
			public void tableChanged(TableModelEvent e){
				actualizarMonto();
			}
		});

		btnAgregar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				addFila();
			}
		});
		
		btnGuardar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				guardar();
			}
		});
		
		btnEditar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editarFila();
			}
		});
		
		btnBorrar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				borrarFila();
			}
		});
		
		txtFecha.addDateSelectionListener(new DateSelectionListener(){
			@Override
			public void dateSelected(Date fechaFin) {
				if(Utilidades.compararFechas(new Date(), fechaFin) > 0){
					WebOptionPane.showMessageDialog(FrmCompra.this,
							"La fecha no puede ser futura.", 
							"Conflicto de fechas", WebOptionPane.WARNING_MESSAGE);
					txtFecha.setDate(new Date());
				}
			}			
		});

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent we){
				FrmPrincipal.bolFrmCompra = false;
			}
		});
	}

	private void conOrden(boolean val){
		pnlOrden.setVisible(val);
		if(!val){
			cboProveedor.setEnabled(true);
			ordenCompra = null;
		}
	}
	
	private void limpiarFormulario(){
		txtFactura.setText("");
		txtArticulo.setText("");
		configTablaArticulos();
		configTablaDetalles();
		actualizarMonto();
	}

	private void cargarOrden_id(){
		try {
			ordenCompra = CtrlOrdenCompra.get(Integer.parseInt(txtOrdenCompra.getText()), false);
			if(ordenCompra == null){
				WebOptionPane.showMessageDialog(this, "La orden no ha sido encontrada",
					"Advertencia", WebOptionPane.WARNING_MESSAGE);
			}else{
				switch(ordenCompra.getEstado()){
					case 'A':
						WebOptionPane.showMessageDialog(this, "La orden ha sido anulada",
								"Advertencia", WebOptionPane.WARNING_MESSAGE);
						break;
					case 'P':
						WebOptionPane.showMessageDialog(this, "La orden ha sido procesada",
								"Advertencia", WebOptionPane.WARNING_MESSAGE);
						break;
					case 'G':
						cboProveedor.setSelectedItem(ordenCompra.getProveedor());
						cboProveedor.setEnabled(false);
						configTablaDetalles();
						DefaultTableModel model = (DefaultTableModel)tblArticulosCompra.getModel();
						ArrayList<Object[]> detalles = CtrlOrdenCompra.getConsulta_Detalle(ordenCompra.getIdOrdenCompra(), false);
						for(Object[] detalle : detalles){
							model.addRow(detalle);
						}
						Utilidades.packColumns(tblArticulosCompra, 0);
						break;
				}
			}
			actualizarMonto();
		} catch (SessionException e){
			WebOptionPane.showMessageDialog(this, "No tiene permisos para acceder a este recurso",
				"Acceso denegado", WebOptionPane.WARNING_MESSAGE);
		} catch (NumberFormatException e){
			WebOptionPane.showMessageDialog(this, "Ingrese un número correcto",
				"Advertencia", WebOptionPane.WARNING_MESSAGE);
		}catch(Exception ex){
			WebOptionPane.showMessageDialog(this, "No se ha podido cargar la orden",
					"Error interno", WebOptionPane.ERROR_MESSAGE);
		}
	}

	private void llenarProveedor(){
		ArrayList<Proveedor> proveedores;
		try{
			proveedores = CtrlProveedor.getProveedores_busqueda("", false);
			cboProveedor = new WebComboBox(proveedores.toArray());
		}catch(Exception ex){
			WebOptionPane.showMessageDialog(this,
				"Error al cargar los proveedores", "Error interno",
				WebOptionPane.ERROR_MESSAGE);
		}
	}

	private void llenarAlmacen(){
		ArrayList<Almacen> almacenes;
		try{
			almacenes = CtrlAlmacen.getAlmacenes("", false);
			cboAlmacen = new WebComboBox(almacenes.toArray());
		}catch(Exception ex){
			WebOptionPane.showMessageDialog(this,
				"Error al cargar los almacenes", "Error interno",
				WebOptionPane.ERROR_MESSAGE);
		}
	}

	public void configTablaArticulos(){
		DefaultTableModel model = new DefaultTableModel();
		tblArticulos.setModel(model);
		model.addColumn("ID");
		model.addColumn("Nombre");
		model.addColumn("Stock");

		try {
			CtrlArticulo.getArticulosStock_almacen(
					((Almacen)cboAlmacen.getSelectedItem()).getIdAlmacen(), txtArticulo.getText(), false)
					.stream().forEach((articulo)->{
				model.addRow(articulo);
			});
		}catch(Exception ex){
			WebOptionPane.showMessageDialog(this,
				"Error al cargar los artículos", "Error interno",
				WebOptionPane.ERROR_MESSAGE);
		}
		Utilidades.packColumns(tblArticulos, 0);
		tblArticulos.setEditable(false);
	}

	public void configTablaDetalles(){
		DefaultTableModel model = new DefaultTableModel();
		tblArticulosCompra.setModel(model);
		model.addColumn("ID");
		model.addColumn("Nombre");
		model.addColumn("Cantidad");
		model.addColumn("S/.");
		model.addColumn("Total");
		model.addColumn("IGV");

		Utilidades.packColumns(tblArticulosCompra, 0);
		tblArticulosCompra.setEditable(false);
	}
	
	private void actualizarMonto(){
		float monto = 0f;
		float igv = 0f;
		for(int i = 0; i < tblArticulosCompra.getRowCount(); i++){
			monto += utilidades.Utilidades.getMoneda(tblArticulosCompra.getModel().getValueAt(i, 4).toString());
		}
		igv = monto * Configuracion.getIgv();
		String monto_sub = utilidades.Utilidades.setUpMoneda(monto - igv);
		String monto_igv = utilidades.Utilidades.setUpMoneda(igv);
		String monto_tot = utilidades.Utilidades.setUpMoneda(monto);
		lblMonto.setText("Subtotal: " + monto_sub + "  -  IGV: " + monto_igv + "  -  Total: " + monto_tot);
		Utilidades.packColumns(tblArticulosCompra, 0);
	}
		
	private boolean existeProducto(int idProducto){
		for(int i = 0; i<tblArticulosCompra.getRowCount(); i++){
			if((int)tblArticulosCompra.getValueAt(i, 0) == idProducto){
				WebOptionPane.showMessageDialog(this, "El producto seleccionado ya ha sido agregado", "Advertencia", WebOptionPane.WARNING_MESSAGE);
				return true;
			}
		}
		return false;
	}

	private void addFila(){
		int row = tblArticulos.getSelectedRow();
		int idArticulo = (int)tblArticulos.getValueAt(row, 0);
		if( row != -1 && !existeProducto(idArticulo)){
			try{
				float lastCost = CtrlCompra.lastCost(idArticulo, false);
				DialogCantidadCosto dcc = new DialogCantidadCosto(this, true, lastCost);
				dcc.setVisible(true);
				int cantidad = dcc.getCantidad();
				float costo = dcc.getCosto();
				if(cantidad != -1) {
					((DefaultTableModel)tblArticulosCompra.getModel())
				        .addRow(new Object[]{
				            tblArticulos.getModel().getValueAt(row, 0),
				            tblArticulos.getModel().getValueAt(row, 1),
				            cantidad,
				            utilidades.Utilidades.setUpMoneda(costo),
				            utilidades.Utilidades.setUpMoneda(cantidad * costo),
				            utilidades.Utilidades.setUpMoneda(cantidad * costo * Configuracion.getIgv())
			        });
				}else{
					WebOptionPane.showMessageDialog(this, "Ingrese cantidad correctamente", "Advertencia",
	    					WebOptionPane.WARNING_MESSAGE);    
				}
			}catch(NumberFormatException | Excepcion | SessionException | SQLException e){
				WebOptionPane.showMessageDialog(this, "Ingresa un número válido", "Advertencia", WebOptionPane.WARNING_MESSAGE);
			}
			actualizarMonto();
		}
	}

	private void borrarFila(){
		int row = tblArticulosCompra.getSelectedRow();
		((DefaultTableModel)tblArticulosCompra.getModel()).removeRow(row);
		actualizarMonto();
	}

	private void editarFila(){
		int row = tblArticulosCompra.getSelectedRow();
		int idArticulo = (int) tblArticulosCompra.getValueAt(row, 0);
		try{
			float lastCost = CtrlCompra.lastCost(idArticulo, false);
			DialogCantidadCosto dcc = new DialogCantidadCosto(this, true, lastCost);
			dcc.setVisible(true);
			int cantidad = dcc.getCantidad();
			float costo = dcc.getCosto();
			if(cantidad != -1){
				tblArticulosCompra.setValueAt(cantidad, row, 2);
				tblArticulosCompra.setValueAt(utilidades.Utilidades.setUpMoneda(costo), row, 3);
				tblArticulosCompra.setValueAt(utilidades.Utilidades.setUpMoneda(cantidad * costo), row, 4);
				tblArticulosCompra.setValueAt(utilidades.Utilidades.setUpMoneda(cantidad * costo * Configuracion.getIgv()), row, 5);
			}else{
				WebOptionPane.showMessageDialog(this, "Ingrese cantidad correctamente", "Advertencia",
    					WebOptionPane.WARNING_MESSAGE);    
			}
			
		}catch(NumberFormatException | Excepcion | SessionException | SQLException e){
			WebOptionPane.showMessageDialog(this, "Ingresar un número válido",
					"Atención", WebOptionPane.ERROR_MESSAGE);
		}
		actualizarMonto();
	}
	
	private boolean prepararDatos(){
		if(tblArticulosCompra.getRowCount() > 0){
			LocalDate fecha = txtFecha.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			compra = new Compra();
			compra.setEstado('G');
			compra.setFecha(fecha);
			float monto = 0f;
			float igv = 0f;
			for(int i = 0; i < tblArticulosCompra.getRowCount(); i++){
				monto += utilidades.Utilidades.getMoneda(tblArticulosCompra.getValueAt(i, 4).toString());				
			}
			igv = monto * Configuracion.getIgv();
			compra.setIgv(igv);
			compra.setMonto(monto);
			compra.setMovimientoCaja(null);
			compra.setOrdenCompra(ordenCompra);
			compra.setProveedor((Proveedor)cboProveedor.getSelectedItem());
			compra.setUsuario(CtrlSesion.usuario);
			compra.setFactura(txtFactura.getText());
			
			detalles = new ArrayList<DetalleCompra>();
			AlmacenArticulo almacenArticulo;
			Almacen almacen = (Almacen)cboAlmacen.getSelectedItem();
			for (int i = 0; i < tblArticulosCompra.getRowCount(); i++) {
				try {
					almacenArticulo = CtrlArticulo.getAlmacenArticulo(almacen.getIdAlmacen(),
							(int) tblArticulosCompra.getValueAt(i, 0), false);
					float det_monto = utilidades.Utilidades.getMoneda(tblArticulosCompra.getValueAt(i, 4).toString());
					detalles.add(new DetalleCompra(
							-1,
							compra,
							almacenArticulo,
							Integer.parseInt(tblArticulosCompra.getValueAt(i, 2).toString()),
							utilidades.Utilidades.getMoneda(tblArticulosCompra.getValueAt(i, 3).toString()),
							det_monto,
							det_monto * Configuracion.getIgv()
					));
				}catch(Exception ex){
					WebOptionPane.showMessageDialog(this,
							"Error al obtener datos necesarios para guardar la información, intente nuevamente",
							"Error interno", WebOptionPane.ERROR_MESSAGE);
					return false;
				}
			}
		}else{
			WebOptionPane.showMessageDialog(this, "No se han seleccionado artículos", "Advertencia", WebOptionPane.WARNING_MESSAGE);
			return false;
		}
		return true;
	}

	private void guardar(){
		if (prepararDatos()){
			try {
				CtrlCompra.insert(this.compra, false, detalles);
				this.lblIdCompra.setText("Último ID: " + this.compra.getIdCompra());
				WebOptionPane.showMessageDialog(this,
                        "Información registrada correctamente",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
				limpiarFormulario();			
				if(ordenCompra != null)
					CtrlOrdenCompra.setEstado(ordenCompra.getIdOrdenCompra(), false);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	class DialogCantidadCosto extends WebDialog{
    	private WebSpinner txtCantidad;
    	private WebTextField txtCostoUnit, txtCostoTot;
    	private WebLabel lblCostoAnterior;
    	private WebButton btnAceptar;
    	private WebButton btnCancelar;
    	
    	float costoAnterior;
    	boolean flag;
    	    	
    	public DialogCantidadCosto(WebFrame frame, boolean modal, float costoAnterior){    		
    		super(frame, modal);
    		setDefaultCloseOperation(WebFrame.HIDE_ON_CLOSE);
    		setSize(250, 400);
    		this.costoAnterior = costoAnterior;
    		initComponents();
    		setLocationRelativeTo(null);
    		pack();
    	}
    	
    	private void initComponents(){
    		txtCantidad = new WebSpinner(new SpinnerNumberModel(1, 1, 10000, 1));
    		txtCostoUnit = new WebTextField("" + this.costoAnterior);
    		txtCostoTot = new WebTextField();
    		lblCostoAnterior = new WebLabel("Anterior: " + costoAnterior);
    		btnAceptar = new WebButton("Aceptar");
    		btnCancelar = new WebButton("Cancelar");
    		flag = false;
    		    		
    		add(new WebPanel(){{
    			setBackground(Color.white);
    			setLayout(new BorderLayout());
    			add(new GroupPanel(){{
    				setLayout(new GridBagLayout());
    				GridBagConstraints gbc = new GridBagConstraints();
    				gbc.gridx = 0;
    				gbc.gridy = 0;
    				gbc.gridwidth = 1;
    				gbc.weightx = 0.2;
    				gbc.fill = GridBagConstraints.HORIZONTAL;
    				
    				add(new WebLabel("Cantidad"), gbc);
    				
    				gbc.gridx = 1;
    				gbc.weightx = 0.8;
    				add( txtCantidad, gbc);
    				
    				gbc.gridx = 0;
    				gbc.gridy = 1;
    				gbc.weightx = 0.2;
    				add(new WebLabel("Costo Unit"), gbc);
    				
    				gbc.gridx = 1;
    				gbc.weightx = 0.8;
    				add(txtCostoUnit, gbc);
    				
    				gbc.gridx = 0;
    				gbc.gridy = 2;
    				gbc.weightx = 0.2;
    				add(new WebLabel("Costo Total"), gbc);
    				
    				gbc.gridx = 1;
    				gbc.weightx = 0.8;
    				add(txtCostoTot, gbc);
    				
    				gbc.gridx = 0;
    				gbc.gridy = 3;
    				gbc.weightx = 0.2;
    				add(new WebLabel("Costo Anterior"), gbc);
    				
    				gbc.gridx = 1;
    				gbc.weightx = 0.8;
    				add(lblCostoAnterior, gbc);
    			}}, BorderLayout.CENTER);
    			add(new GroupPanel(){{
    				setLayout(new FlowLayout(FlowLayout.RIGHT));
    				add(btnAceptar);
    				add(btnCancelar);
    			}}, BorderLayout.SOUTH);
    		}});
    		txtCantidad.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					calcularCosto();
				}
			});

    		txtCostoTot.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					calcularCosto();					
				}
			});
    		btnAceptar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					flag = true;
					setVisible(false);
				}
			});
    		btnCancelar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					flag = false;
					setVisible(false);
				}
			});
    		
    		txtCantidad.getEditor().getComponent(0).addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent e) {
					if(e.getKeyChar() == KeyEvent.VK_ENTER){
						flag = true;
						setVisible(false);
					}
					
				}
				
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
    	}
    	
    	private void calcularCosto(){
    		float costoTotal;
    		float costoUnit;
			int cantidad;
			try{
				costoTotal = Float.parseFloat(txtCostoTot.getText());
				cantidad = (Integer)txtCantidad.getValue();
			}catch(NumberFormatException ex){
				costoTotal = 0f;
				cantidad = 1;
			}
			
			costoUnit = costoTotal / cantidad;
			
			if(costoUnit != 0)
				txtCostoUnit.setText(String.valueOf(costoUnit));
    	}
    	
    	public int getCantidad(){
    		int cantidad;
    		try{
    			cantidad = Integer.parseInt(txtCantidad.getValue().toString());
    		}catch(NumberFormatException e){
    			cantidad = -1;
    		}
    		return flag ? cantidad : -1;
    	}
    	
    	public float getCosto(){
    		float costoUnit;
    		try{
    			costoUnit = Float.parseFloat(txtCostoUnit.getText());
    		}catch(NumberFormatException e){
    			costoUnit = 0f;
    		}
    		return flag ? costoUnit : -1;
    	}
    }
}
