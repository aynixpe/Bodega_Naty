package vista;

import com.alee.laf.WebLookAndFeel;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import modelo.Conexion;
import utilidades.Configuracion;
import utilidades.SessionException;

/**
 *
 * @author Edder
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws SessionException 
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException, SessionException {
        try {
        	UIManager.setLookAndFeel(WebLookAndFeel.class.getCanonicalName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
        	Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
        	GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT,
					Main.class.getResourceAsStream("/resources/fonts/roboto.ttf")));
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}
        String[] dataConexion = Configuracion.getDB();
        if(dataConexion != null)
        	Conexion.setDefaultConexion(dataConexion[0],
        			5432,
        			dataConexion[1], 
        			dataConexion[2],
        			dataConexion[3], Conexion.POSTGRESQL);
        else
        	Conexion.setDefaultConexion("local", 5432, "bodega", "postgres", "enter", Conexion.POSTGRESQL);
        WebLookAndFeel.setDecorateAllWindows(true);
        FrmInicioSesion fis = new FrmInicioSesion();
        fis.setVisible(true);
    }
}
