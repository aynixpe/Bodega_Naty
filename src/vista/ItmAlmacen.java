/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import clases.Almacen;

import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;

import utilidades.Utilidades;

/**
 *
 * @author Edder
 */
@SuppressWarnings("serial")
public class ItmAlmacen extends WebPanel implements ActionListener, MouseListener{
    private final Almacen almacen;
    private final FrmAlmacenes parent;
    private WebLabel lblAlmacen;
    private WebButton btnEditar;
    
    public ItmAlmacen(FrmAlmacenes parent, Almacen almacen){
        this.parent = parent;
        this.almacen = almacen;
        initComponents();
    }
    
    /**
     * Inicializa los componentes del panel
     */
    private void initComponents(){
        setBackground(Color.white);
        setMaximumSize(new Dimension(600, 35));
        this.lblAlmacen = new WebLabel();
        this.lblAlmacen.setText(this.almacen.getNombre());
        this.btnEditar = Utilidades.getIconButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_IEDITAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_IEDITAR[1])));
        setLayout(new BorderLayout());
        this.add(this.lblAlmacen, BorderLayout.CENTER);
        this.add(new WebPanel(){
            {
                setBackground(new Color(0,0,0,0));
                setLayout(new FlowLayout(FlowLayout.RIGHT));
                this.add(btnEditar);
            }
        }, BorderLayout.EAST);
        this.btnEditar.addActionListener(this);
        this.addMouseListener(this);
    }

    /**
     * Abre Dialog para editar almacen,
     * luego actualiza la información a mostrar
     */
    private void editarAlmacen(){
        FrmAlmacen fc = new FrmAlmacen(null, true, this.almacen);
        fc.setVisible(true);
        this.parent.llenarAlmacenes();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.btnEditar){
            editarAlmacen();
            this.btnEditar.setBackground(new Color(79,192,232));
        }
    }

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
        this.setBackground(new Color(79,192,232));
        this.lblAlmacen.setForeground(Color.white);
    }

    @Override
    public void mouseExited(MouseEvent e) { 
        this.setBackground(Color.white);
        this.lblAlmacen.setForeground(Color.black);
    }
}
