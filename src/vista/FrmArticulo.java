package vista;

import clases.Articulo;
import clases.Categoria;
import clases.Almacen;

import com.alee.extended.image.WebImage;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.spinner.WebSpinner;
import com.alee.laf.text.WebTextField;

import controlador.CtrlArticulo;
import controlador.CtrlCategoria;
import controlador.CtrlAlmacen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.ComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;

import utilidades.Excepcion;
import utilidades.Utilidades;

/**
 * Formulario para crear, editar y/o eliminar un artículo
 * @author Edder
 * @version 1.0
 */
@SuppressWarnings("serial")
public class FrmArticulo extends WebDialog implements ActionListener{
    private Articulo articulo;
    
    private WebTextField 
            txtNombre,
            txtPresentacion,
            txtDescripcion,
            txtTamanio,
            txtUnidad,
            txtBarCode,
            txtPrecio;
    private WebSpinner txtStockMinimo;    
    private WebComboBox cboCategoria;
    private WebButton /*btnGenerateBarCode, */btnEliminar, btnGuardar, btnCancelar;
    private WebLabel lblId;
    
    /**
     * Constructor principal
     */
    FrmArticulo(WebFrame owner, boolean modal, Articulo articulo){
        super(owner, modal);
        setTitle("Articulo");
        setSize(600, 400);
        setDefaultCloseOperation(WebDialog.DISPOSE_ON_CLOSE);
        setShowResizeCorner(false);
        setShowMaximizeButton(false);
        this.articulo = articulo;
        initComponents();
        setLocationRelativeTo(null);
    }

    /**
     * Inicializa los componentes del formulario
     */
    private void initComponents(){
    	setIconImage(new ImageIcon(getClass().getResource("/resources/img/cart_add.png")).getImage());
    	txtNombre = new WebTextField();
    	txtPresentacion = new WebTextField();
    	txtStockMinimo = new WebSpinner();
    	txtDescripcion = new WebTextField();
    	txtTamanio = new WebTextField();
        txtUnidad = new WebTextField();
        txtPrecio = new WebTextField();
        txtPrecio.setTrailingComponent(new WebImage(getClass().getResource("/resources/img/coins.png")));        
        cboCategoria = new WebComboBox();
        SpinnerNumberModel snm = new SpinnerNumberModel();
        txtStockMinimo.setModel(snm);
        txtBarCode = new WebTextField();
//        btnGenerateBarCode = new WebButton("Generar código");
   
        cargarCategorias();
        
        btnEliminar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_ELIMINAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_ELIMINAR[1]))
        		);
        btnGuardar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[1]))
        		);
        btnCancelar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[1]))
        		);
        lblId = new WebLabel("ID: ");

        if(articulo != null){
        	cargarArticulo();
        }
        add(new WebPanel(){{
        	setBackground(Color.white);
        	setLayout(new BorderLayout());
        	add(new GroupPanel(){{
        		setLayout(new SpringLayout());
                add(new WebLabel("Nombre"));
                add(txtNombre);
                
                add(new WebLabel("Presentacion"));
                add(txtPresentacion);
                
                add(new WebLabel("Descripcion"));
                add(txtDescripcion);

                add(new WebLabel("Stock Mínimo"));
                add(txtStockMinimo);

                add(new WebLabel("Tamanio"));
                add(txtTamanio);
                
                add(new WebLabel("Unidad"));
                add(txtUnidad);
                
                add(new WebLabel("Código de Barras"));
                add(txtBarCode);

                add(new WebLabel("Categoria"));
                add(cboCategoria);

                add(new WebLabel("Precio"));
                add(txtPrecio);

                SpringUtilities.makeCompactGrid(this,
                            9, 2,
                            6, 6,
                            6, 6);
        	}}, BorderLayout.CENTER);
        	add(new GroupPanel(){{
                setLayout(new BorderLayout());
                add(lblId, BorderLayout.WEST);
                add(new GroupPanel(){{
                	setLayout(new FlowLayout(FlowLayout.RIGHT));
                	add(btnEliminar, btnGuardar, btnCancelar);
                }}, BorderLayout.EAST);
        	}}, BorderLayout.SOUTH);
        }});
        
        btnEliminar.addActionListener(this);
        btnGuardar.addActionListener(this);
        btnCancelar.addActionListener(this);
        addWindowListener(new WindowListener() {
                @Override
                public void windowOpened(WindowEvent e) {
                    txtNombre.requestFocusInWindow();
                }

                @Override
                public void windowClosing(WindowEvent e) {}

                @Override
                public void windowClosed(WindowEvent e) {
                	((FrmArticulos)FrmArticulo.this.getParent()).buscar();
                }

                @Override
                public void windowIconified(WindowEvent e) {}

                @Override
                public void windowDeiconified(WindowEvent e) {}

                @Override
                public void windowActivated(WindowEvent e) {}

                @Override
                public void windowDeactivated(WindowEvent e) {}
        });
        btnGuardar.addActionListener((ActionEvent e) -> {
        	guardar();
        }); 
            
        this.pack();
    }
    
    private void cargarCategorias(){
    	ArrayList<Categoria> categorias = new ArrayList<>();
    	try{
    		categorias = CtrlCategoria.getCategorias("", false);
    		cboCategoria = new WebComboBox(categorias.toArray());
    	}catch(SQLException | Excepcion e){
    		WebOptionPane.showMessageDialog(this, 
    				"Error al cargar las categorías, intente nuevamente", "Error interno", 
    				WebOptionPane.ERROR_MESSAGE);
    	}
    }
    
    @SuppressWarnings("unchecked")
	private Categoria getCategoria(){
    	try{
    		Categoria cat = CtrlCategoria.get(articulo.getCategoria().getIdCategoria(), false);
    		ComboBoxModel<Categoria> model = cboCategoria.getModel();
    		for(int i = 0; i < model.getSize(); i++){
    			if(cat.getIdCategoria() == model.getElementAt(i).getIdCategoria())
    				return model.getElementAt(i);
    		}
    	}catch(Exception e){
    		WebOptionPane.showMessageDialog(this, 
    				"Error al cargar las categorías, intente nuevamente", "Error interno", 
    				WebOptionPane.ERROR_MESSAGE);
    	}
    	return null;
    }
    
    /**
     * Carga la información del artículo, si es null, deja los campos vacíos
     */
    private void cargarArticulo(){
        txtNombre.setText(articulo.getNombre());
        txtPresentacion.setText(articulo.getPresentacion());
        txtDescripcion.setText(articulo.getDescripcion());
        cboCategoria.setSelectedItem(getCategoria());
        txtTamanio.setText(String.valueOf(articulo.getTamanio()));
        txtUnidad.setText(articulo.getUnidad());
        txtPrecio.setText(String.valueOf(articulo.getPrecioVenta()));
        txtStockMinimo.setValue(articulo.getStockMinimo());
        txtBarCode.setText(articulo.getCodigoBarras());
        String estado = articulo.getEstado() == 'A' ? "Activo": "Inactivo";
        lblId.setText("ID: " + articulo.getIdArticulo() + " - " + estado);        
    }
        
    /**
     *  Guarda la información, si es nuevo inserta, sino actualiza
     */
    private void guardar(){
        if(this.articulo == null){
            this.articulo = new Articulo();
            this.articulo.setIdArticulo(-1);
            this.articulo.setEstado('A');
        }
        prepararDatos();
        if(this.articulo.getIdArticulo() == -1){
            try {
                //Crear
                CtrlArticulo.insert(this.articulo, false);                
                this.lblId.setText("ID: " + String.valueOf(this.articulo.getIdArticulo()));
                WebOptionPane.showMessageDialog(
                        this,
                        "Información registrada correctamente",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
                
                asignar();
                this.dispose();
            }catch(Exception e){
                /*WebOptionPane.showMessageDialog(
                        this,
                        "No se pudo guardar la información, verificar e intentar nuevamente",
                        "Error",
                        WebOptionPane.ERROR_MESSAGE);*/
            	e.printStackTrace();            	
            }
        }else{
            try {
                CtrlArticulo.update(this.articulo, false);
                WebOptionPane.showMessageDialog(
                        this,
                        "Información registrada correctamente",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            }catch(Exception e){
            	e.printStackTrace();
                /*WebOptionPane.showMessageDialog(
                        this,
                        "No se pudo guardar la información, verificar e intentar nuevamente",
                        "Error",
                        WebOptionPane.ERROR_MESSAGE);*/
            }
        }
    }
    
    private void asignar(){
    	int idArticulo = this.articulo.getIdArticulo();
    	try {
			ArrayList<Almacen> almacenes = CtrlAlmacen.getAlmacenes("", false);
			ArrayList<Integer> ids = new ArrayList<>();
			ids.add(idArticulo);
			for(Almacen almacen : almacenes){
				CtrlArticulo.asignarAlmacen(ids, almacen.getIdAlmacen(), false);
			}
		} catch (Excepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
    
    /**
     * Da de baja el artículo, cambia de estado
     */
    private void eliminar(){
        if(WebOptionPane.showConfirmDialog(
                this,
                "¿Seguro que desea eliminar el artículo?",
                "Advertencia",
                WebOptionPane.YES_NO_OPTION,
                WebOptionPane.WARNING_MESSAGE) == WebOptionPane.YES_OPTION)
            try {
                CtrlArticulo.dar_baja(this.articulo, false);
                WebOptionPane.showMessageDialog(
                        this,
                        "Artículo eliminado",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            }catch(Exception e){
                WebOptionPane.showMessageDialog(
                        this,
                        "No se pudo eliminar el artículo, verificar e intentar nuevamente",
                        "Error",
                        WebOptionPane.ERROR_MESSAGE);
            }
    }
    
    /**
     * Asigna la información de los campos del formulario al objeto categoría
     */
    private void prepararDatos(){
        this.articulo.setNombre(this.txtNombre.getText());
        this.articulo.setPresentacion(this.txtPresentacion.getText());
        this.articulo.setDescripcion(this.txtDescripcion.getText());
        this.articulo.setStockMinimo((Integer)this.txtStockMinimo.getValue());
        this.articulo.setTamanio(this.txtTamanio.getText());
        this.articulo.setUnidad(this.txtUnidad.getText());
        this.articulo.setCodigoBarras(this.txtBarCode.getText());
        this.articulo.setCategoria((Categoria)this.cboCategoria.getSelectedItem());
        this.articulo.setPrecioVenta(Float.parseFloat(this.txtPrecio.getText()));
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.btnGuardar)
            this.guardar();
        else if(e.getSource() == this.btnCancelar){
            if(WebOptionPane.showConfirmDialog(
                    this,
                    "¿Está seguro que desea cerrar?",
                    "Advertencia",
                    WebOptionPane.YES_NO_OPTION,
                    WebOptionPane.WARNING_MESSAGE) == WebOptionPane.YES_OPTION)
                this.dispose();
        }
        else if(e.getSource() == this.btnEliminar)
            this.eliminar();
        /**
         * Un posible cambio es que el botón Eliminar sea eliminar cuando el artículo está activo, activa cuando está inactivo
         * y que se oculte cuando se está agregando un artículo
         */
    }
}