package vista;

import clases.Cliente;
import clases.DetalleVenta;
import clases.Venta;
import clases.Categoria;
import clases.AlmacenArticulo;

import com.alee.extended.date.WebDateField;
import com.alee.extended.image.WebDecoratedImage;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.spinner.WebSpinner;
import com.alee.laf.table.WebTable;
import com.alee.laf.text.WebTextField;

import controlador.CtrlArticulo;
import controlador.CtrlCategoria;
import controlador.CtrlSesion;
import controlador.CtrlVenta;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import utilidades.Utilidades;

/**
 *
 * @author togapaulo
 */
@SuppressWarnings("serial")
public class FrmVenta extends WebFrame{	
    private Cliente cliente;
    private Categoria categoria;
    private Venta venta;
    
    private ArrayList<DetalleVenta> detalles;
        
    private WebTable tblDetalleVenta;
    private WebDateField txtFecha;
    private WebTextField txtCliente,
    					txtCodigoBarras,
    					txtBusquedaArticulo;
    private WebLabel lblSubTotal, lblDescuento, lblTotal, lblIdVenta;
	private WebButton btnBuscarArticulo,
    					btnBuscarCliente,
    					btnEliminar,
    					btnEditar,
    					btnGuardar;
//    					btnCancelar;
    private GroupPanel pnlArticulos, pnlCategorias;
    //private WebCheckBox chbSinStock;
    
    //private FrmBusquedaCliente frmBusquedaCliente;
    
    public FrmVenta(){
        setTitle("Venta");
        setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
        setSize(900,600);
        setLocationRelativeTo(null);
        setShowResizeCorner(true);
        initComponents();
    }

    private void initComponents() {
        tblDetalleVenta = new WebTable();
        txtFecha = new WebDateField(new Date());
        txtCliente = new WebTextField();
        txtCodigoBarras = new WebTextField(10);
        txtBusquedaArticulo = new WebTextField();
        lblTotal = new WebLabel("Total: S/. 0.0");
        lblSubTotal = new WebLabel("Sub total: S/. 0.0");
        lblDescuento = new WebLabel("Descuento: S/. 0.0");
        lblIdVenta = new WebLabel("ID: ");
        btnBuscarArticulo = Utilidades.getIconButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[1])));
        btnBuscarCliente = Utilidades.getIconButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[1])));
        btnEditar = new WebButton("Editar");
        btnEliminar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_ELIMINAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_ELIMINAR[1])));
        btnGuardar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[1])));
//        btnCancelar = Utilidades.getButton(
//        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[0])),
//        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[1])));
        pnlArticulos = new GroupPanel();
        pnlCategorias = new GroupPanel();
        
        lblSubTotal.setHorizontalAlignment(WebLabel.RIGHT);
        lblTotal.setHorizontalAlignment(WebLabel.RIGHT);
        lblDescuento.setHorizontalAlignment(WebLabel.RIGHT);
        txtCliente.setEnabled(false);
        
        //chbSinStock = new WebCheckBox("Artículos sin Stock");
        //lblUsuario = new WebLabel("ID: ");
        
        add(new WebPanel(){{
        	setBackground(Color.white);
        	setLayout(new BorderLayout());
        	add(new GroupPanel(){{
        		setLayout(new GridLayout(1,2));
        		add(new GroupPanel(){{
        			setLayout(new BorderLayout());
        			add(new GroupPanel(){{
        				setLayout(new GridLayout(3,1));
        				add(new GroupPanel());
        				add(new GroupPanel());
        				add(new GroupPanel(){{
        					setLayout(new BorderLayout());
        					add(new WebLabel("Artículo"), BorderLayout.WEST);
        					add(txtBusquedaArticulo, BorderLayout.CENTER);
        					add(btnBuscarArticulo, BorderLayout.EAST);
        				}});
        			}}, BorderLayout.NORTH);
        			add(new WebScrollPane(pnlCategorias){{
        				setHorizontalScrollBarPolicy(WebScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        				setMinimumWidth(80);
        			}}, BorderLayout.WEST);
        			add(new WebScrollPane(pnlArticulos){{
        				setHorizontalScrollBarPolicy(WebScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        			}}, BorderLayout.CENTER);
        			add(new GroupPanel(){{
        				add(lblIdVenta);
        			}}, BorderLayout.SOUTH);
        		}});
        		add(new GroupPanel(){{
        			setLayout(new BorderLayout());
        			add(new GroupPanel(){{
        				setLayout(new GridLayout(3,1));
        				add(new GroupPanel(){{
        					add(new WebLabel("Fecha"));
        					add(txtFecha);
        				}});
        				add(new GroupPanel(){{
        					setLayout(new BorderLayout());
        					add(new WebLabel("Cliente"), BorderLayout.WEST);
        					add(txtCliente, BorderLayout.CENTER);
        					add(btnBuscarCliente, BorderLayout.EAST);
        				}});
        				add(new GroupPanel(){{
        					add(new WebLabel("Código"));
        					add(txtCodigoBarras);
        				}});
        			}}, BorderLayout.NORTH);
        			add(new WebScrollPane(tblDetalleVenta), BorderLayout.CENTER);
        			add(new GroupPanel(){{
        				setLayout(new GridLayout(4,1));
        				add(lblSubTotal);
        				add(lblDescuento);
        				add(lblTotal);
        				add(new GroupPanel(){{
        					setLayout(new BorderLayout());
        					add(new GroupPanel(){{
        						add(btnEditar);
        						add(btnEliminar);
        					}}, BorderLayout.WEST);
        					add(btnGuardar, BorderLayout.EAST);
        				}});        				
        			}}, BorderLayout.SOUTH);
        		}});
        	}}, BorderLayout.CENTER);
        	add(new GroupPanel(){{
        		
        	}}, BorderLayout.SOUTH);
        }});
        
        cargarCategorias();
        configTablaDetalles();
        cargarArticulos();
        
        txtCodigoBarras.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buscarArticulo();
			}
		});
        
        txtBusquedaArticulo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cargarArticulos();
			}
		});
        
        btnBuscarArticulo.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e) {
				cargarArticulos();
			}
        });
        
        btnEditar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editar();
			}
		});
        
        btnEliminar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
        
        btnBuscarCliente.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FrmBusquedaCliente fbc = new FrmBusquedaCliente(FrmVenta.this, true);
				fbc.setVisible(true);
				if((cliente = fbc.getCliente()) != null){
					txtCliente.setText(cliente.getNombre());
				}
			}
		});
        
        btnGuardar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				guardar();
			}
		});
        
        addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent ev){
                FrmPrincipal.bolFrmVenta = false;
            }
        });
    }
    
    private void editar(){
    	int index = tblDetalleVenta.getSelectedRow();
    	if(index >= 0){
    		try{
    			int stock = CtrlArticulo.getArticuloStock((int)tblDetalleVenta.getValueAt(index, 0),
    					CtrlSesion.almacen.getIdAlmacen(), false);
    			float precio = utilidades.Utilidades.getMoneda(tblDetalleVenta.getValueAt(index, 3).toString());
    			DialogCantidadDescuento dcd = new DialogCantidadDescuento(this, true, stock, precio);
    			dcd.setVisible(true);
    			int cantidad;
    			float descuento;
    			if((cantidad = dcd.getCantidad()) != -1){
    				descuento = dcd.getDescuento();
    				float total;
    				total = cantidad * precio - descuento;
    				tblDetalleVenta.setValueAt(cantidad, index, 2);
    				tblDetalleVenta.setValueAt(utilidades.Utilidades.setUpMoneda(descuento), index, 4);
    				tblDetalleVenta.setValueAt(utilidades.Utilidades.setUpMoneda(total), index, 5);
    				tblDetalleVenta.getModel();
    				Utilidades.packColumns(tblDetalleVenta, 0);
    			}else{
    				WebOptionPane.showMessageDialog(this, "Ingrese cantidad correctamente", "Advertencia",
    						WebOptionPane.WARNING_MESSAGE);    		
    			}
    			dcd.dispose();
    			actualizarMonto();
    		}catch(Exception e){
    			WebOptionPane.showMessageDialog(this, 
    					"Error al obtener stock del producto, imposible verificar", 
    					"Error interno", WebOptionPane.ERROR_MESSAGE);
    			System.err.println(e.getMessage());
    		}
    	}
    }

    private void borrar(){
    	int index = tblDetalleVenta.getSelectedRow();
    	if(index >= 0){
    		DefaultTableModel model = (DefaultTableModel)tblDetalleVenta.getModel();
    		model.removeRow(index);
    		actualizarMonto();
    	}
    }
    
    private float actualizarMonto(){
    	float subtotal, descuento;
    	subtotal = descuento = 0f;
    	for(int i = 0; i < tblDetalleVenta.getRowCount(); i++){
    		subtotal += utilidades.Utilidades.getMoneda(tblDetalleVenta.getValueAt(i, 3).toString()) *
    				(int)tblDetalleVenta.getValueAt(i, 2);
    		descuento -= utilidades.Utilidades.getMoneda((String)tblDetalleVenta.getValueAt(i, 4));;
    	}
    	lblSubTotal.setText("Sub total: " + utilidades.Utilidades.setUpMoneda(subtotal));
    	lblDescuento.setText("Descuento: " + utilidades.Utilidades.setUpMoneda(descuento));
    	lblTotal.setText("Total: " + utilidades.Utilidades.setUpMoneda(subtotal - descuento));
    	return subtotal - descuento;
    }
    
    private void cargarCategorias(){
    	pnlCategorias.removeAll();
    	try {
    		pnlCategorias.setLayout(new BoxLayout(pnlCategorias, BoxLayout.Y_AXIS));
    		ArrayList<Categoria> categorias = CtrlCategoria.getCategorias("", false);
    		pnlCategorias.add(new ItemCategoria(null){{
    			addMouseListener(new MouseListener() {
					@Override
					public void mouseReleased(MouseEvent e) {
					}
					@Override
					public void mousePressed(MouseEvent e) {
					}
					@Override
					public void mouseExited(MouseEvent e) {
					}					
					@Override
					public void mouseEntered(MouseEvent e) {
					}
					@Override
					public void mouseClicked(MouseEvent e) {
						FrmVenta.this.categoria = null;
						cargarArticulos();
					}
				});
    		}});
    		for(Categoria categoria : categorias){
    			ItemCategoria x = new ItemCategoria(categoria);
    			pnlCategorias.add(x);
    			x.addMouseListener(new MouseListener() {
					@Override
					public void mouseReleased(MouseEvent e) {
					}
					@Override
					public void mousePressed(MouseEvent e) {
					}
					@Override
					public void mouseExited(MouseEvent e) {
					}					
					@Override
					public void mouseEntered(MouseEvent e) {
					}
					@Override
					public void mouseClicked(MouseEvent e) {
						FrmVenta.this.categoria = categoria;
						cargarArticulos();
					}
				});
    		}
    	}catch(Exception e){
			e.printStackTrace();
		}
    }
    
    private boolean existeDetalle(int idArticulo){
    	for(int i = 0; i < tblDetalleVenta.getRowCount(); i++){
    		if(idArticulo == (int) tblDetalleVenta.getValueAt(i, 0))
    			return true;
    	}
    	return false;
    }
    
    private void configTablaDetalles(){
    	DefaultTableModel model = new DefaultTableModel();
    	model.addColumn("ID");
    	model.addColumn("Artículo");
    	model.addColumn("#");
    	model.addColumn("S/.");
    	model.addColumn("Desc");
    	model.addColumn("Tot");
    	tblDetalleVenta.setModel(model);
    	Utilidades.packColumns(tblDetalleVenta, 0);
    	tblDetalleVenta.setEditable(false);
    	tblDetalleVenta.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
        
    private void cargarArticulos(){
    	pnlArticulos.removeAll();
    	ArrayList<Object[]> articulos;
    	try{
	    	articulos = CtrlArticulo.getArticulos(CtrlSesion.almacen.getIdAlmacen(), categoria, txtBusquedaArticulo.getText(), false);
	    	pnlArticulos.setLayout(new BoxLayout(pnlArticulos, BoxLayout.PAGE_AXIS));
	    	for(int i = 0; i < articulos.size(); i ++){
	    		Object[] articulo = articulos.get(i);
	    		pnlArticulos.add(new ItemArticulo(articulo){{
	    			addMouseListener(new MouseListener() {
						@Override
						public void mouseReleased(MouseEvent e) {
						}
						
						@Override
						public void mousePressed(MouseEvent e) {
						}
						
						@Override
						public void mouseExited(MouseEvent e) {
						}
						
						@Override
						public void mouseEntered(MouseEvent e) {
						}
						
						@Override
						public void mouseClicked(MouseEvent e) {
							addFila(articulo);
						}
					});
	    		}});     		
	    	}
    	}catch(Exception e){
    		WebOptionPane.showMessageDialog(this, "Error al cargar los artículos", "Error interno",
    				WebOptionPane.ERROR_MESSAGE);
    		e.printStackTrace();
    	}
    	pnlArticulos.updateUI();
    }
    
    private void buscarArticulo(){
    	Object[] result;
    	try{
    		result = CtrlArticulo.getArticulos(CtrlSesion.almacen.getIdAlmacen(),
    			null, txtCodigoBarras.getText(), false).get(0);
			addFila(result);    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
        
    private void addFila(Object[] articulo){
    	if(!existeDetalle((int)articulo[0])){
    		Object[] fila = new Object[6];
    		DialogCantidadDescuento dcd = new DialogCantidadDescuento(this, true, (int)articulo[2], (float)articulo[3]);
    		dcd.setVisible(true);
    		int cantidad;
    		float descuento;
    		if((cantidad = dcd.getCantidad()) != -1){
    			descuento = dcd.getDescuento();
    			float total;
    			total = cantidad * (float)articulo[3] - descuento;
    			fila[0] = articulo[0];
    			fila[1] = articulo[1];
    			fila[2] = cantidad;
    			fila[3] = utilidades.Utilidades.setUpMoneda((float)articulo[3]);
    			fila[4] = utilidades.Utilidades.setUpMoneda(descuento);
    			fila[5] = utilidades.Utilidades.setUpMoneda(total);
    			DefaultTableModel model = (DefaultTableModel)tblDetalleVenta.getModel();
    			model.addRow(fila);
    			Utilidades.packColumns(tblDetalleVenta, 0);
    		}else{
    			WebOptionPane.showMessageDialog(this, "Ingrese cantidad correctamente", "Advertencia",
    					WebOptionPane.WARNING_MESSAGE);    		
    		}
    		dcd.dispose();
    		txtCodigoBarras.setText("");
    		txtCodigoBarras.requestFocus();
    		actualizarMonto();
    	}else
			WebOptionPane.showMessageDialog(this, "El artículo ya ha sido agregado", "Advertencia",
    				WebOptionPane.WARNING_MESSAGE);
    }
       
    private boolean prepararDatos(){
    	try {
    		if(tblDetalleVenta.getRowCount() <= 0)
    			return false;
    		venta = new Venta();
    		LocalDate fecha = this.txtFecha.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			
    		venta.setCliente(cliente);
			venta.setEstado('G');
			venta.setUsuario(CtrlSesion.usuario);
			venta.setFecha(fecha);
			venta.setMonto(actualizarMonto());
			
			detalles = new ArrayList<>();
			AlmacenArticulo almacenArticulo;
			
			for (int i = 0; i < tblDetalleVenta.getRowCount(); i++) {
				almacenArticulo = CtrlArticulo.getAlmacenArticulo(
						CtrlSesion.almacen.getIdAlmacen(),
						(int)tblDetalleVenta.getModel().getValueAt(i, 0),
						false);
				detalles.add(new DetalleVenta(
						-1,
						venta,
						almacenArticulo,
						(int)tblDetalleVenta.getValueAt(i, 2),
						utilidades.Utilidades.getMoneda(tblDetalleVenta.getValueAt(i, 3).toString()),
						utilidades.Utilidades.getMoneda((String)tblDetalleVenta.getValueAt(i, 4)),
						null
				));
			}
    	}catch(Exception e){		
			e.printStackTrace();
			venta = null;
			return false;
		}
    	return true;
    }
    
    private void guardar(){
    	if(prepararDatos()){
    		try{
    			CtrlVenta.insert(venta, false, detalles);
    			lblIdVenta.setText("ID: " + venta.getIdVenta());
    			int option = WebOptionPane.showConfirmDialog(this, 
    					"Venta registrada, ¿Desea generar comprobante?", "Éxito", WebOptionPane.INFORMATION_MESSAGE);
    			if(option == WebOptionPane.YES_OPTION){
    				FrmComprobante fc = new FrmComprobante(this, true, new ArrayList<Venta>(){{add(venta);}});
    				fc.setVisible(true);
    			}
    			nuevaVenta();
    		}catch(Exception e){
    			e.printStackTrace();
    			WebOptionPane.showMessageDialog(this, 
    					"Error al intentar guardar los datos", "Error interno", WebOptionPane.ERROR_MESSAGE);
    		}
    	}
    }
    
    private void nuevaVenta(){ 
    	txtCodigoBarras.setText("");
    	lblIdVenta.setText("Último ID: " + venta.getIdVenta());
    	venta = null;
    	detalles = new ArrayList<>();
    	configTablaDetalles();;
    	cliente = null;
    	cargarArticulos();
    	txtCliente.setText("");
    	actualizarMonto();
    	txtCodigoBarras.requestFocus();
    }
    
    class ItemCategoria extends WebDecoratedImage{
    	private Categoria categoria;
    	public ItemCategoria(Categoria categoria){
    		Image img;
    		if(categoria == null || categoria.getImagen() == null)
    			img = Utilidades.getImageFromImageIcon(
						new ImageIcon(getClass().getResource("/resources/img/categoria_default.png")),
						50);
    		else
    			img = Utilidades.getImageFromImageIcon(
        			categoria.getImage(), 50);
    		super.setImage(img, true);
    		this.categoria = categoria;
    	}
    	
    	public Categoria getCategoria(){
    		return categoria;
    	}
    }
    
    class ItemArticulo extends WebPanel{
    	/**
    	 * Object debe recibir:
    	 * 0: idArticulo
    	 * 1: nombre
    	 * 2: precio
    	 * 3: stock
    	 */
    	private Object[] articulo;
    	private WebLabel lblArticulo, lblPrecio, lblStock;
    	public ItemArticulo(Object[] articulo){
    		this.articulo = articulo;
    		initComponents();
    	}
    	
    	private void initComponents(){
            setMaximumSize(new Dimension(600, 45));
            setMargin(5);
            setBackground(Color.white);
    		setLayout(new BorderLayout());
    		lblArticulo = new WebLabel(articulo[1].toString());
    		lblPrecio = new WebLabel("Stock: " + articulo[2].toString());
    		lblStock = new WebLabel("S/. " +articulo[3].toString());
    		add(lblArticulo, BorderLayout.NORTH);
    		add(lblPrecio, BorderLayout.WEST);
    		add(lblStock, BorderLayout.EAST);
    		addMouseListener(new MouseListener() {
				@Override
				public void mouseReleased(MouseEvent e) {
				}
				@Override
				public void mousePressed(MouseEvent e) {
				}				
				@Override
				public void mouseExited(MouseEvent e) {
					setBackground(Color.white);
	                lblArticulo.setForeground(Color.black);
	                lblPrecio.setForeground(Color.black);
	                lblStock.setForeground(Color.black);
				}				
				@Override
				public void mouseEntered(MouseEvent e) {
					setBackground(new Color(79,192,232));
	                lblArticulo.setForeground(Color.white);
	                lblPrecio.setForeground(Color.white);
	                lblStock.setForeground(Color.white);
				}				
				@Override
				public void mouseClicked(MouseEvent e) {	
				}
			});
    	}
    	
    	public int getIdArticulo(){
    		return Integer.parseInt(articulo[0].toString());
    	}
    	
    	public Object[] getArticulo(){
    		return this.articulo;
    	}
    }

    class DialogCantidadDescuento extends WebDialog{
    	private WebSpinner txtCantidad;
    	private WebTextField txtDescuento, txtDescuentoPercent;
    	private WebButton btnAceptar;
    	private WebButton btnCancelar;
    	
    	int stockMax;
    	float precio;
    	boolean flag;
    	    	
    	public DialogCantidadDescuento(WebFrame frame, boolean modal, int stock, float precio){    		
    		super(frame, modal);
    		setDefaultCloseOperation(WebFrame.HIDE_ON_CLOSE);
    		setSize(250,150);
    		stockMax = stock;
    		this.precio = precio;
    		initComponents();
    		setLocationRelativeTo(null);
    		pack();
    	}
    	
    	private void initComponents(){
    		txtCantidad = new WebSpinner(new SpinnerNumberModel(1, 1, stockMax, 1));
    		txtDescuento = new WebTextField();
    		txtDescuentoPercent = new WebTextField();
    		btnAceptar = new WebButton("Aceptar");
    		btnCancelar = new WebButton("Cancelar");
    		flag = false;
    		    		
    		add(new WebPanel(){{
    			setBackground(Color.white);
    			setLayout(new BorderLayout());
    			add(new GroupPanel(){{
    				setLayout(new GridBagLayout());
    				GridBagConstraints gbc = new GridBagConstraints();
    				gbc.gridx = 0;
    				gbc.gridy = 0;
    				gbc.gridwidth = 1;
    				gbc.weightx = 0.2;
    				gbc.fill = GridBagConstraints.HORIZONTAL;
    				
    				add(new WebLabel("Cantidad"), gbc);
    				
    				gbc.gridx = 1;
    				gbc.weightx = 0.8;
    				add( txtCantidad, gbc);
    				
    				gbc.gridx = 0;
    				gbc.gridy = 1;
    				gbc.weightx = 0.2;
    				add(new WebLabel("% Descuento"), gbc);
    				
    				gbc.gridx = 1;
    				gbc.weightx = 0.8;
    				add(txtDescuentoPercent, gbc);
    				
    				gbc.gridx = 0;
    				gbc.gridy = 2;
    				gbc.weightx = 0.2;
    				add(new WebLabel("Descuento"), gbc);
    				
    				gbc.gridx = 1;
    				gbc.weightx = 0.8;
    				add(txtDescuento, gbc);
    			}}, BorderLayout.CENTER);
    			add(new GroupPanel(){{
    				setLayout(new FlowLayout(FlowLayout.RIGHT));
    				add(btnAceptar);
    				add(btnCancelar);
    			}}, BorderLayout.SOUTH);
    		}});
    		txtCantidad.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					calcularDescuento();
				}
			});

    		txtDescuentoPercent.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					calcularDescuento();					
				}
			});
    		btnAceptar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					flag = true;
					setVisible(false);
				}
			});
    		btnCancelar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					flag = false;
					setVisible(false);
				}
			});
    		
    		txtCantidad.getEditor().getComponent(0).addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent e) {
					if(e.getKeyChar() == KeyEvent.VK_ENTER){
						flag = true;
						setVisible(false);
					}
					
				}
				
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
    	}
    	
    	private void calcularDescuento(){
    		float porcentaje;
			int cantidad;
			try{
				porcentaje = Float.parseFloat(txtDescuentoPercent.getText());
				cantidad = (Integer)txtCantidad.getValue();
			}catch(NumberFormatException ex){
				porcentaje = 0f;
				cantidad = 1;
			}
			float subTotal = precio * cantidad;
			float desc = subTotal * porcentaje / 100;
			
			txtDescuento.setText(String.valueOf(desc));
    	}
    	
    	public int getCantidad(){
    		int cantidad;
    		try{
    			cantidad = Integer.parseInt(txtCantidad.getValue().toString());
    		}catch(NumberFormatException e){
    			cantidad = -1;
    		}
    		return cantidad <= stockMax ? (flag ? cantidad : -1) : -1;
    	}
    	
    	public float getDescuento(){
    		float descuento;
    		try{
    			descuento = Float.parseFloat(txtDescuento.getText());
    		}catch(NumberFormatException e){
    			descuento = 0f;
    		}
    		return descuento <= (precio * getCantidad()) ? descuento : 0f;
    	}
    }
} 