package vista;

import clases.Almacen;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;

import controlador.CtrlAlmacen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import utilidades.Excepcion;
import utilidades.Utilidades;

/**
 *
 * @author Edder
 */

@SuppressWarnings("serial")
public class FrmAlmacenes extends WebFrame implements ActionListener{
    private WebButton btnAgregar;
    private WebPanel lstAlmacenes;
    private WebCheckBox chbInactivos;
    
    public FrmAlmacenes(){
        setTitle("Almacenes");
        setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(300, 400));
        setShowResizeCorner(false);
        setShowMaximizeButton(false);
        initComponents();
        llenarAlmacenes();
        this.setLocationRelativeTo(null);
    }
    
    /**
     * Obtiene las categorías de la base de datos y llena la lista para mostrar
     */
    public final void llenarAlmacenes(){
        try {
            ArrayList<Almacen> almacenes = this.chbInactivos.isSelected() ? 
                    CtrlAlmacen.getAlmacenes("", false) : 
                    CtrlAlmacen.getAlmacenes_estado("", 'A', false);
            this.lstAlmacenes.removeAll();
            almacenes.stream().forEach((almacen) -> {
                this.lstAlmacenes.add(new ItmAlmacen(this, almacen));
            });
            this.lstAlmacenes.updateUI();
        } catch (SQLException | Excepcion ex) {
            Logger.getLogger(FrmAlmacenes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Inicializa los componentes del formulario
     */
    private void initComponents(){
        this.btnAgregar = Utilidades.getIconButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_IAGREGAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_IAGREGAR[1])));
        this.lstAlmacenes = new WebPanel();
        this.lstAlmacenes.setLayout(new BoxLayout(this.lstAlmacenes, BoxLayout.PAGE_AXIS));
        this.chbInactivos = new WebCheckBox("Inactivos");
        
        this.add(new WebPanel(){
            {
                setBackground(Color.white);
                setLayout(new BorderLayout());
                add(new GroupPanel(){
                    {
                        this.setLayout(new BorderLayout());
                        this.add(new WebLabel("Almacenes"), BorderLayout.WEST);
                        this.add(new WebPanel(){
                            {
                                setBackground(Color.white);
                                this.setLayout(new FlowLayout(FlowLayout.RIGHT));
                                this.add(chbInactivos);
                                this.add(btnAgregar);
                            }
                        });
                    }
                }, BorderLayout.NORTH);
                add(new GroupPanel(){
                    {
                        setLayout(new BorderLayout());
                        add(new WebScrollPane(lstAlmacenes), BorderLayout.CENTER);
                    }
                }, BorderLayout.CENTER);
            }
        });
        this.chbInactivos.addActionListener((ActionEvent e) -> {
            llenarAlmacenes();
        });
        this.btnAgregar.addActionListener(this);
        this.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent evt){
                FrmPrincipal.bolFrmAlmacenes = false;
            }
        });
        this.pack();
    }
    
    /**
     * Abre el formulario para agregar categoría
     * luego actualiza la lista
     */
    private void agregarAlmacen(){
        FrmAlmacen fc = new FrmAlmacen(null, true, null);
        fc.setVisible(true);
        llenarAlmacenes();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.btnAgregar){
            agregarAlmacen();
        }
    }
}
