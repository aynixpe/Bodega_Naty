/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import clases.Categoria;

import com.alee.extended.image.WebDecoratedImage;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.filechooser.WebFileChooser;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.text.WebTextField;

import controlador.CtrlCategoria;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.SpringLayout;

import utilidades.Utilidades;

/**
 *
 * @author Edder
 */
@SuppressWarnings("serial")
public class FrmCategoria extends WebDialog implements ActionListener{
    private Categoria categoria;
	private File file;
    
    private WebTextField txtNombre;
    private WebDecoratedImage imgIcono;
    private WebButton btnCambiar, btnEliminar, btnGuardar, btnCancelar;
    
    FrmCategoria(WebFrame owner, boolean modal, Categoria categoria){
        super(owner, modal);
        setTitle("Categoría");
        setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
        setSize(300,100);
        setShowResizeCorner(false);
        setShowMaximizeButton(false);
        this.categoria = categoria;
        initComponents();
        setLocationRelativeTo(null);
    }
    
    /**
     * Inicializa los componentes del formulario
     */
    private void initComponents(){
        txtNombre = new WebTextField(20);
        btnCambiar = new WebButton("Cambiar");
        btnGuardar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[1])));        
        btnCancelar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[1])));
        btnEliminar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_ELIMINAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_ELIMINAR[1])));
                
        imgIcono = new WebDecoratedImage(Utilidades.getImageFromImageIcon(
           		new ImageIcon(getClass().getResource("/resources/img/categoria_default.png")), 50));
                
        btnEliminar.setVisible(false);
        if(categoria != null)
        	cargarCategoria();
        
        add(new WebPanel(){{
        	setBackground(Color.white);
        	setLayout(new BorderLayout());
        	add(new GroupPanel(){{
        		setLayout(new SpringLayout());
                add(new WebLabel("Nombre"));
                add(txtNombre);
                add(new WebLabel("Imagen"));
                add(imgIcono);
                add(new WebLabel(""));
                add(btnCambiar);
                
                SpringUtilities.makeCompactGrid(this,
                        3, 2,
                        6, 6,
                        6, 6);
        	}}, BorderLayout.CENTER);
        	add(new GroupPanel(){{
        		setLayout(new FlowLayout(FlowLayout.RIGHT));
        		add(btnEliminar, btnGuardar, btnCancelar);
        	}}, BorderLayout.SOUTH);
        }});
        btnEliminar.addActionListener(this);
        btnCancelar.addActionListener(this);
        btnGuardar.addActionListener(this);
        
        btnCambiar.addActionListener(new ActionListener() {
        	private WebFileChooser fileChooser = null;

            @Override
            public void actionPerformed ( ActionEvent e )
            {
                if ( fileChooser == null )
                {
                    fileChooser = new WebFileChooser ();
                    fileChooser.setMultiSelectionEnabled ( false );
                    fileChooser.setAcceptAllFileFilterUsed ( false );
                }
                if ( fileChooser.showOpenDialog(FrmCategoria.this.getParent()) == WebFileChooser.APPROVE_OPTION )
                {
                    file = fileChooser.getSelectedFile();
                    imgIcono.setImage(Utilidades.getImageFromImageIcon(
                			new ImageIcon(file.getAbsolutePath()), 50), true);
                }
            }
		});
        pack();
    }
    
    private void cargarCategoria(){
    	if(categoria.getImagen() != null){
    		imgIcono.setImage(Utilidades.getImageFromImageIcon(categoria.getImage(), 50), true);
    	}
    	txtNombre.setText(categoria.getNombre());
    	btnEliminar.setVisible(true);
    }
    
    /**
     * Elimina una categoría
     */
    private void eliminar(){
        if(WebOptionPane.showConfirmDialog(
                this,
                "¿Seguro que desea eliminar esta categoría?",
                "Advertencia",
                WebOptionPane.YES_NO_OPTION,
                WebOptionPane.WARNING_MESSAGE) == WebOptionPane.YES_OPTION)
            try {
                CtrlCategoria.eliminar(this.categoria, false);
                WebOptionPane.showMessageDialog(
                        this,
                        "Categoría eliminada",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            }catch(Exception ex){
                WebOptionPane.showMessageDialog(
                        this,
                        "No se pudo eliminar la categoría, posiblemente hay artículos de esta categoría,"
                                + " verificar e intentar nuevamente",
                        "Error",
                        WebOptionPane.ERROR_MESSAGE);
                ex.printStackTrace();
            }
    }
    
    /**
     * Guarda la información, si está creando uno nuevo, inserta en la base de datos, sino actualiza
     */
    private void guardar(){
        if(categoria == null){
            categoria = new Categoria();
            categoria.setIdCategoria(-1);
        }
        prepararDatos();
        if(categoria.getIdCategoria() == -1){
            try {
                //Crear
                CtrlCategoria.insert(categoria, false);
                WebOptionPane.showMessageDialog(
                        this,
                        "Información registrada correctamente",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
                dispose();
            }catch(Exception ex){
                WebOptionPane.showMessageDialog(
                        this,
                        "No se pudo guardar la información, verificar e intentar nuevamente",
                        "Error",
                        WebOptionPane.ERROR_MESSAGE);
                ex.printStackTrace();
            }
        }else{
            try {
                CtrlCategoria.update(categoria, false);
                if(file != null)
                	CtrlCategoria.changeImagen(categoria, false);
                WebOptionPane.showMessageDialog(
                        this,
                        "Información registrada correctamente",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
                dispose();
            }catch(Exception ex){
                /*WebOptionPane.showMessageDialog(
                        this,
                        "No se pudo guardar la información, verificar e intentar nuevamente",
                        "Error",
                        WebOptionPane.ERROR_MESSAGE);*/
            	ex.printStackTrace();
            }
        }
    }
    
    /**
     * obtiene la información de los campos del formulario y llena los atributos de la categoría
     */
    private void prepararDatos(){
        categoria.setNombre(txtNombre.getText());
        categoria.setImagen(file);
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.btnCancelar){
            if(WebOptionPane.showConfirmDialog(
                    this,
                    "¿Está seguro que desea cerrar?",
                    "Advertencia",
                    WebOptionPane.YES_NO_OPTION,
                    WebOptionPane.WARNING_MESSAGE) == WebOptionPane.YES_OPTION)
                this.dispose();
        }else if(e.getSource() == this.btnEliminar){
            this.eliminar();
        }else if(e.getSource() == this.btnGuardar){
            this.guardar();
        }
    }
}
