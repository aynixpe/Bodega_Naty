package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import utilidades.Configuracion;
import utilidades.Utilidades;
import clases.Articulo;
import clases.Comprobante;
import clases.DetalleComprobante;
import clases.DetalleVenta;
import clases.Serie;
import clases.TipoComprobante;
import clases.Venta;

import com.alee.extended.button.WebSwitch;
import com.alee.extended.date.WebDateField;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.text.WebTextField;

import controlador.CtrlArticulo;
import controlador.CtrlComprobante;
import controlador.CtrlSerie;
import controlador.CtrlVenta;

@SuppressWarnings("serial")
public class FrmComprobante extends WebDialog{	
	private WebTextField txtCliente,
						txtDireccion,
						txtDni,
						txtNumero;
	private WebLabel lblSubTotal, lblIgv, lblTotal;
	private WebComboBox cboTipoComprobante, cboSerie;
	private WebDateField txtFecha;
	private WebButton btnAceptar, btnCancelar;
	private WebTable tblDetalles;
	private WebSwitch btnResumen;
		
	private Comprobante comprobante;
	private ArrayList<DetalleComprobante> detallesComprobante;
	private ArrayList<Venta> ventas;
	private ArrayList<DetalleVenta> detallesVenta;
	
	public FrmComprobante(WebFrame owner, boolean modal, ArrayList<Venta> ventas){		
		super(owner, modal);
		setTitle("Comprobante");
		this.ventas = ventas;
		setDefaultCloseOperation(WebDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		setShowMaximizeButton(false);
		setShowResizeCorner(false);
		setShowMinimizeButton(false);
		setLocationRelativeTo(null);
		setSize(600, 400);
		initComponents();
	}

	private void initComponents(){
		comprobante = new Comprobante();
		detallesComprobante = new ArrayList<>();
		
		cboTipoComprobante = new WebComboBox();
		cboSerie = new WebComboBox();
		txtFecha = new WebDateField(new Date());
		txtNumero = new WebTextField(6);
		txtCliente = new WebTextField();
		txtDireccion = new WebTextField();
		txtDni = new WebTextField();
		btnAceptar = new WebButton("Aceptar");
		btnCancelar = new WebButton("Cancelar");
		tblDetalles = new WebTable();
		lblSubTotal = new WebLabel("Sub total: S/. ");
		lblIgv = new WebLabel("IGV: S/. ");
		lblTotal = new WebLabel("Total: S/. ");
		btnResumen = new WebSwitch();
		
		btnResumen.getLeftComponent().setText("Sí");
		btnResumen.getRightComponent().setText("No");
		
		lblSubTotal.setHorizontalAlignment(WebLabel.RIGHT);
		lblIgv.setHorizontalAlignment(WebLabel.RIGHT);
		lblTotal.setHorizontalAlignment(WebLabel.RIGHT);
		
		txtNumero.setEditable(false);
		configTablaDetalles();
		cargarTiposComprobante();
		cargarDetalles();
				
		add(new WebPanel(){{
			setBackground(Color.white);
			setLayout(new BorderLayout());
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				GridBagConstraints gbc = new GridBagConstraints();
				add(new GroupPanel(){{
					setLayout(new GridBagLayout());
					gbc.gridx = 0;
					gbc.gridy = 0;
					gbc.fill = GridBagConstraints.HORIZONTAL;					
					gbc.weightx = 0.1;
					
					add(new WebLabel("Señor(es): "), gbc);
					gbc.gridy = 1;
					add(new WebLabel("Dirección: "), gbc);

					gbc.gridy = 2;
					add(new WebLabel("DNI: "), gbc);
					
					gbc.gridx = 1;
					gbc.gridy = 0;
					gbc.gridwidth = 2;
					gbc.weightx = 0.9;
					add(txtCliente, gbc);
					
					gbc.gridy = 1;					
					add(txtDireccion, gbc);
										
					gbc.gridy = 2;
					gbc.gridwidth = 1;
					gbc.weightx = 0.2;
					add(txtDni, gbc);
					
					gbc.gridx = 2;
					add(new WebLabel(""), gbc);
				}}, BorderLayout.CENTER);
				add(new GroupPanel(){{
					setLayout(new GridBagLayout());					
					gbc.gridx = 0;
					gbc.gridy = 0;
					gbc.weightx = 0.1;
					gbc.fill = GridBagConstraints.HORIZONTAL; 
					add(new WebLabel("Tipo:"), gbc);
					
					gbc.gridy = 1;
					add(new WebLabel("Serie: "), gbc);
					
					gbc.gridy = 2;
					add(new WebLabel("Fecha: "), gbc);
					
					gbc.gridx = 1;
					gbc.gridy = 0;
					gbc.gridwidth = 3;
					add(cboTipoComprobante, gbc);
					
					gbc.gridy = 1;
					gbc.gridwidth = 1;
					add(cboSerie, gbc);
					
					gbc.gridy = 2;
					add(txtFecha, gbc);
					
					gbc.gridy = 1;
					gbc.gridx = 2;
					add(new WebLabel("Nº"), gbc);
					
					gbc.gridx = 3;
					add(txtNumero, gbc);
				}}, BorderLayout.EAST);
			}}, BorderLayout.NORTH);
			add(new WebScrollPane(tblDetalles), BorderLayout.CENTER);
			add(new GroupPanel(){{
				setLayout(new GridLayout(4, 1));
				add(new GroupPanel(){{
					setLayout(new BorderLayout());
					add(new GroupPanel(){{
						setLayout(new BorderLayout());
						add(new WebLabel("Resúmen"), BorderLayout.WEST);
						add(btnResumen, BorderLayout.CENTER);
					}}, BorderLayout.WEST);
					add(lblSubTotal, BorderLayout.EAST);
				}});
				add(lblIgv);
				add(lblTotal);
				add(new GroupPanel(){{
					setLayout(new BorderLayout());
					add(new GroupPanel(){{
						add(btnAceptar);
						add(btnCancelar);
					}}, BorderLayout.EAST);
				}});
			}}, BorderLayout.SOUTH);
		}});
		
		btnAceptar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				guardar();
			}
		});
		
		cboTipoComprobante.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED)
					cargarSeries();
			}
		});
	}
	
	private void configTablaDetalles(){
		DefaultTableModel model = new DefaultTableModel();
		tblDetalles.setModel(model);
		model.addColumn("Cant");
		model.addColumn("Descripción");
		model.addColumn("P. Unit");
		model.addColumn("Total");
		tblDetalles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblDetalles.setEditable(false);
		Utilidades.packColumns(tblDetalles, 0);
	}
	
	private void actualizarMonto(){
		float monto = getMonto();
		float igv = monto * Configuracion.getIgv();
		lblSubTotal.setText("Sub total: S/. " + (monto - igv));
		lblIgv.setText("IGV: S/. " + igv);
		lblTotal.setText("Total: S/. " + monto);
	}
	
	private float getMonto(){
		float monto = 0f;
		for(int i= 0; i < tblDetalles.getRowCount(); i++){
			monto += (float)tblDetalles.getValueAt(i, 3);
		}
		return monto;
	}
	
	private void cargarTiposComprobante(){
		try {
			ArrayList<TipoComprobante> tipos = CtrlComprobante.getTiposComprobante("", false);
			cboTipoComprobante = new WebComboBox(tipos.toArray());
			cargarSeries();
		}catch(Exception e){
			e.printStackTrace();
		} 
	}
	
	@SuppressWarnings("unchecked")
	private void cargarSeries(){
		try {
			ArrayList<Serie> series = CtrlSerie.getSeries_TipoComprobante(
					((TipoComprobante)cboTipoComprobante.getSelectedItem()).getIdTipoComprobante(), false);
			cboSerie.removeAllItems();
			@SuppressWarnings("rawtypes")
			DefaultComboBoxModel model = new DefaultComboBoxModel(series.toArray());
			cboSerie.setModel(model);
			cargarNumero();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void cargarNumero(){
		try {
			int x = CtrlComprobante.LastNumero_Serie(
					((Serie)cboSerie.getSelectedItem()).getIdSerie(), false);
			txtNumero.setText(String.valueOf(x + 1));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
		
	private void cargarDetalles(){
		try{
			detallesVenta = new ArrayList<>();
			comprobante = new Comprobante();
			for(Venta venta : ventas){
				detallesVenta.addAll(CtrlVenta.getDetalles(venta.getIdVenta(), false));
			}
			for(DetalleVenta detalleVenta : detallesVenta){
				DetalleComprobante detalle = new DetalleComprobante();
				int idArticulo = CtrlArticulo.getIdArticulo(detalleVenta.getAlmacenArticulo().getIdAlmacenArticulo(), false);
				Articulo articulo = CtrlArticulo.get(idArticulo, false);
				float precio = detalleVenta.getPrecio() * detalleVenta.getCantidad() - detalleVenta.getDescuento();
				float igv = precio * Configuracion.getIgv();
				detalle.setArticulo(articulo);
				detalle.setCantidad(detalleVenta.getCantidad());
				detalle.setIgv(igv);
				detalle.setPrecio(precio - igv);
				detalle.setComprobante(comprobante);
				detallesComprobante.add(detalle);
				addFila(detalle);
			}
			Utilidades.packColumns(tblDetalles, 0);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void addFila(DetalleComprobante detalle){
		float total = detalle.getPrecio() + detalle.getIgv();
		float punit = total / (float)detalle.getCantidad();
				
		Object[] fila = new Object[]{
			detalle.getCantidad(),
			detalle.getArticulo().getNombre(),
			punit,
			total
		};
		((DefaultTableModel) tblDetalles.getModel()).addRow(fila);
		actualizarMonto();
	}
	
	private void guardar(){		
		try {
			LocalDate fecha = (new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			CtrlComprobante.insert(btnResumen.isSelected(), fecha, 
					txtCliente.getText(), 
					(Serie) cboSerie.getSelectedItem(), 
					detallesVenta, 
					false);
			WebOptionPane.showMessageDialog(
                    this,
                    "Información registrada correctamente",
                    "Éxito",
                    WebOptionPane.INFORMATION_MESSAGE);
		}catch(Exception e){
			e.printStackTrace();
		}
	}	
}
