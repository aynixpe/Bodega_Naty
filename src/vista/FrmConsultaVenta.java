package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import clases.Almacen;
import clases.Venta;

import com.alee.extended.date.WebDateField;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.splitpane.WebSplitPane;
import com.alee.laf.table.WebTable;

import controlador.CtrlAlmacen;
import controlador.CtrlVenta;
import utilidades.Utilidades;

@SuppressWarnings("serial")
public class FrmConsultaVenta extends WebFrame{
	private WebDateField txtFechaInicio, txtFechaFin;
	private WebComboBox cboAlmacen;
	private WebCheckBox chbAnulados;
	private WebTable tblVentas,
					 tblDetalles;
	private WebButton btnAnular,
					  btnCerrar,
					  btnBuscar,
					  btnGenerarComprobante;
		
	public FrmConsultaVenta() {	
		setTitle("Registro de Ventas");
		setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
		setShowResizeCorner(false);
		setSize(1000, 600);
		setLocationRelativeTo(null);
		initComponents();
	}
	
	private void initComponents(){		
		txtFechaInicio = new WebDateField(new Date());
		txtFechaFin = new WebDateField(new Date());
		cboAlmacen = new WebComboBox();
		chbAnulados = new WebCheckBox("Anulados");		
		tblVentas = new WebTable();
		tblDetalles = new WebTable();
		btnAnular = new WebButton("Anular");
		btnCerrar = new WebButton("Cerrar");
		btnBuscar = new WebButton("Buscar");
		btnGenerarComprobante = new WebButton("Generar comprobante");
		
		cargarAlmacenes();
		configTablaVentas();
		configTablaDetalles();
		
		buscar();
		
		add(new WebPanel(){{
			setBackground(Color.white);
			setLayout(new BorderLayout());
			
			add(new GroupPanel(){{
				setLayout(new GridBagLayout());
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.weightx = 0.2;
				add(new GroupPanel(){{
					add(new WebLabel("Inicio"));
					add(txtFechaInicio);					
				}}, gbc);
				
				gbc.gridx = 1;
				add(new GroupPanel(){{
					add(new WebLabel("Fin"));
					add(txtFechaFin);					
				}}, gbc);
				
				gbc.gridx = 2;
				add(new GroupPanel(){{					
					add(new WebLabel("Almacen"));
					add(cboAlmacen);
				}}, gbc);
				
				gbc.gridx = 3;
				add(chbAnulados, gbc);
				
				gbc.gridx = 4;
				add(new GroupPanel(){{
					setLayout(new BorderLayout());
					add(btnBuscar, BorderLayout.EAST);
				}}, gbc);
				
			}}, BorderLayout.NORTH);
			add(new WebSplitPane(
					WebSplitPane.HORIZONTAL_SPLIT,
					new WebScrollPane(tblVentas), 
					new WebScrollPane(tblDetalles)){{
						setDividerLocation(650);
					}}, BorderLayout.CENTER);
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				add(new GroupPanel(){{
					add(btnAnular);
					add(btnGenerarComprobante);
				}}, BorderLayout.WEST);				
				add(btnCerrar, BorderLayout.EAST);
			}}, BorderLayout.SOUTH);
		}});
		
		btnAnular.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				anular();
			}
		});
		
		btnBuscar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buscar();
			}
		});
		ListSelectionModel tblSelect = tblVentas.getSelectionModel();
		tblSelect.addListSelectionListener((ListSelectionEvent arg0)-> {
			getDetalles();
		});
		
		btnCerrar.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		btnGenerarComprobante.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				generarComprobante();
			}
		});
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent we){
				FrmPrincipal.bolFrmConsultaVenta = false;
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	private void cargarAlmacenes(){
		try{
			ArrayList<Almacen> almacenes = CtrlAlmacen.getAlmacenes("", false);
			almacenes.add(0, new Almacen(){{
				setNombre("Todos");
			}});
			cboAlmacen.removeAllItems();
			@SuppressWarnings("rawtypes")
			DefaultComboBoxModel model = new DefaultComboBoxModel(almacenes.toArray());
			cboAlmacen.setModel(model);
		}catch(Exception e){
			WebOptionPane.showMessageDialog(this, "Error al cargar almacenes",
					"Error interno", WebOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private void configTablaVentas(){
		DefaultTableModel model = new DefaultTableModel();
		tblVentas.setModel(model);
		model.addColumn("ID");
		model.addColumn("Cliente");
		model.addColumn("S/.");
		model.addColumn("Fecha");
		model.addColumn("Usuario");
		model.addColumn("Almacén");
		model.addColumn("Estado");
		tblVentas.setEditable(false);
		Utilidades.packColumns(tblVentas, 0);
	}
	
	private void configTablaDetalles(){
		DefaultTableModel model = new DefaultTableModel();
		tblDetalles.setModel(model);
		model.addColumn("ID");
		model.addColumn("Articulo");
		model.addColumn("Cant");
		model.addColumn("Precio");
		model.addColumn("Desc");
		tblDetalles.removeColumn(tblDetalles.getColumnModel().getColumn(0));
		Utilidades.packColumns(tblDetalles, 0);
		tblDetalles.setEditable(false);
	}
	
	private void getDetalles(){
		configTablaDetalles();
		DefaultTableModel model = (DefaultTableModel)tblDetalles.getModel();
		
		int row = tblVentas.getSelectedRow();
		
		if(row >= 0){
			try{				
				int idVenta = (int)tblVentas.getValueAt(row, 0);
				ArrayList<Object[]> detalles = CtrlVenta.getConsulta_Detalle(idVenta, false);
				for (Object[] detalle : detalles) {
					detalle[3] = utilidades.Utilidades.setUpMoneda((float)detalle[3]);
					detalle[4] = utilidades.Utilidades.setUpMoneda((float)detalle[4]);
					model.addRow(detalle);
				}
				Utilidades.packColumns(tblDetalles, 0);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private void buscar(){
		configTablaVentas();
		LocalDate fechaInicio = (txtFechaInicio.getDate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate fechaFin = (txtFechaFin.getDate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		
		try{
			Almacen almacen = cboAlmacen.getSelectedIndex() == 0? null : (Almacen) cboAlmacen.getSelectedItem();
			ArrayList<Object[]> ventas = CtrlVenta.getConsulta(fechaInicio.toString(),
					fechaFin.toString(), null, almacen, chbAnulados.isSelected(), false);
			for (Object[] venta : ventas) {
				venta[2] = utilidades.Utilidades.setUpMoneda((float)venta[2]);
				((DefaultTableModel)tblVentas.getModel()).addRow(venta);
			}
			Utilidades.packColumns(tblVentas, 0);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void anular(){
		int[] seleccionadas = tblVentas.getSelectedRows();
		if(seleccionadas.length > 0 && verificarSeleccion(seleccionadas, 'A') 
				&& WebOptionPane.showConfirmDialog(this, "¿Seguro que desea anular las ventas seleccionadas?",
					"Advertencia", WebOptionPane.WARNING_MESSAGE)== WebOptionPane.YES_OPTION){			
			for(int i = 0; i < seleccionadas.length; i++){			
				int idVenta = (int) tblVentas.getValueAt(seleccionadas[i], 0);
				anular(idVenta);
			}
			buscar();
		}
	}
	
	private boolean anular(int idVenta){
		try{			
			CtrlVenta.anularVenta(idVenta, false);
			return true;
		}catch(Exception e){
			WebOptionPane.showMessageDialog(this, "No puede anular",
					"Error", WebOptionPane.WARNING_MESSAGE);
			return false;
		}
	}
	
	public void generarComprobante(){
		int[] seleccionadas = tblVentas.getSelectedRows();
		if(seleccionadas.length > 0 && verificarSeleccion(seleccionadas, 'P') 
				&& verificarSeleccion(seleccionadas, 'A')){
			ArrayList<Venta> ventas = new ArrayList<>();
			try{
				for(int i = 0; i < seleccionadas.length; i++){
					int fila = seleccionadas[i];
					int id = (int)tblVentas.getValueAt(fila, 0);
					Venta venta = CtrlVenta.get(id, false);
					ventas.add(venta);
				}
				FrmComprobante fc = new FrmComprobante(this, true, ventas);
				fc.setVisible(true);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public boolean verificarSeleccion(int[] seleccionadas, char estado){
		for(int i = 0; i < seleccionadas.length; i++){
			if(tblVentas.getValueAt(seleccionadas[i], 6).toString().charAt(0) == estado){
				return false;
			}
		}
		return true;
	}
	
}
