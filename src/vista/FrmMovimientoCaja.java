package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SpringLayout;
import javax.swing.table.DefaultTableModel;

import utilidades.SessionException;
import utilidades.Utilidades;
import clases.Caja;
import clases.DetalleMovimientoCaja;
import clases.MovimientoCaja;
import clases.TipoMovimientoCaja;

import com.alee.extended.date.WebDateField;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.text.WebFormattedTextField;
import com.alee.laf.text.WebTextField;

import controlador.CtrlCaja;
import controlador.CtrlMovimientoCaja;
import controlador.CtrlSesion;

@SuppressWarnings("serial")
public class FrmMovimientoCaja extends WebFrame {
	private WebDateField txtFecha;
	private WebComboBox cboTipo, cboOrigen, cboDestino;
	private WebTextField txtConcepto;
	private WebFormattedTextField txtMonto;
	private WebButton btnAgregar, btnGuardar, btnEliminar;
	private WebTable tblDetalles;
	private WebLabel lblId, lblTotal;
	
	private MovimientoCaja movimientoCaja;
	private ArrayList<DetalleMovimientoCaja> detalles;
	
	public FrmMovimientoCaja(){
		setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
		setSize(500, 600);
        setShowResizeCorner(false);
        setShowMaximizeButton(false);
        setLocationRelativeTo(null);
        initComponents();
	}
	
	public void initComponents(){
		txtFecha = new WebDateField(new Date());
		cboTipo = new WebComboBox();
		cboOrigen = new WebComboBox();
		cboDestino = new WebComboBox();
		txtConcepto = new WebTextField();
		txtMonto = new WebFormattedTextField();
		btnAgregar = new WebButton("Agregar");
		btnGuardar = new WebButton("Guardar");
		btnEliminar = new WebButton("Eliminar");
		tblDetalles = new WebTable();
		lblId = new WebLabel("ID: ");
		lblTotal = new WebLabel("Total: S/. 0.00");
		
		txtConcepto.setInputPrompt("Concepto");
		txtMonto.setInputPrompt("S/. 0.0");
		
		txtMonto.setValue(0.1f);
		
		cargarTiposMovimiento();
		cargarCajas();
		configTablaDetalles();
		
		inhabilitarCargoDescargo();
		
		add(new WebPanel(){{
			setBackground(Color.white);
			setLayout(new BorderLayout());
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
        		add(new GroupPanel(){{
        			setLayout(new GridLayout(1,2));
	                add(new GroupPanel(){{
	                	setLayout(new SpringLayout());
	                	add(new WebLabel("Tipo"));
	                	add(cboTipo);
	                	add(new WebLabel("Origen"));
	                	add(cboOrigen);
	                	SpringUtilities.makeCompactGrid(this,
	                			2, 2,
	                			6, 6,
	                			6, 6
	                			);
	                }});
	                add(new GroupPanel(){{
	                	setLayout(new SpringLayout());
	                	add(new WebLabel("Fecha"));
	                	add(txtFecha);
	                	add(new WebLabel("Destino"));
	                	add(cboDestino);
	                	SpringUtilities.makeCompactGrid(this,
	                			2, 2,
	                			6, 6,
	                			6, 6
	                			);
	                }});
        		}}, BorderLayout.CENTER);	                
                
                add(new GroupPanel(){{
                    setLayout(new GridBagLayout());
                    GridBagConstraints gbc = new GridBagConstraints();
                    
                    gbc.gridx = 0;
                    gbc.gridy = 0;
                    gbc.gridwidth = 1;
                    gbc.weightx = 0.95;
                    gbc.fill = GridBagConstraints.HORIZONTAL;
                    add(txtConcepto, gbc);
                    
                    gbc.gridx = 1;
                    gbc.weightx = 0.045;
                    add(txtMonto, gbc);
                    
                    gbc.gridx = 2;
                    gbc.weightx = 0.005;
                    add(btnAgregar, gbc);
                }}, BorderLayout.SOUTH);
			}}, BorderLayout.NORTH);
			add(new WebScrollPane(tblDetalles)
			, BorderLayout.CENTER);
			add(new GroupPanel(){{
				setLayout(new GridLayout(2,1));
				add(new GroupPanel(){{
					setLayout(new BorderLayout());
					add(lblTotal, BorderLayout.EAST);
				}});
				add(new GroupPanel(){{					
					setLayout(new BorderLayout());				
					add(lblId, BorderLayout.WEST);
					add(new GroupPanel(){{
						add(btnEliminar);
						add(btnGuardar);
					}}, BorderLayout.EAST);
				}});
			}}, BorderLayout.SOUTH);
		}});
		
		cboTipo.addActionListener((ActionEvent e) -> {
			inhabilitarCargoDescargo();
		});
		
		cboOrigen.addActionListener((ActionEvent e) -> {
        	comprobarAlmacenes();
        });
        
        cboDestino.addActionListener((ActionEvent e) -> {
        	comprobarAlmacenes();
        });
		
        btnEliminar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				eliminar();
			}
		});
		
		btnGuardar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				guardar();
			}
		});
		
		btnAgregar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addFila();
			}
		});
		
		addWindowListener(new WindowAdapter(){
		    @Override
		    public void windowClosed(WindowEvent ev){
		        FrmPrincipal.bolFrmMovimientoCaja = false;
		    }
		});
	}
	
	@SuppressWarnings("unchecked")
	private void cargarCajas(){
		try {
			ArrayList<Caja> cajas = CtrlCaja.get("", false);
			DefaultComboBoxModel<Caja> modelOrigen = new DefaultComboBoxModel<Caja>((Caja[]) cajas.toArray());
			DefaultComboBoxModel<Caja> modelDestino = new DefaultComboBoxModel<Caja>((Caja[]) cajas.toArray());
			cboOrigen.removeAllItems();
			cboDestino.removeAllItems();
			cboOrigen.setModel(modelOrigen);
			cboDestino.setModel(modelDestino);
		}catch(Exception e){
			e.printStackTrace();
		}		
	}
	
	public void inhabilitarCargoDescargo(){
		char item = ((TipoMovimientoCaja)cboTipo.getSelectedItem()).getTipo();
    	cboOrigen.setEnabled(! (item == 'I'));
    	cboDestino.setEnabled(! (item == 'E'));
    }
	
	private boolean comprobarAlmacenes(){
		char tipo = cboTipo.getSelectedItem().toString().charAt(0);
    	if (cboDestino.getSelectedIndex() == cboOrigen.getSelectedIndex() && tipo == 'T') {
            WebOptionPane.showMessageDialog(FrmMovimientoCaja.this,
            		"No se puede hacer transferencia al mismo almacén", 
            		"Advertencia", WebOptionPane.WARNING_MESSAGE);
            return true;
        }
    	return false;
    }
	
	@SuppressWarnings("unchecked")
	private void cargarTiposMovimiento(){
		try{
			ArrayList<TipoMovimientoCaja> tipos = new ArrayList<>();
			tipos = CtrlMovimientoCaja.getTipoMovimientosCaja("", false);
			DefaultComboBoxModel<TipoMovimientoCaja> model = 
					new DefaultComboBoxModel<TipoMovimientoCaja>((TipoMovimientoCaja[]) tipos.toArray());
			cboTipo.removeAllItems();
			cboTipo.setModel(model);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void configTablaDetalles(){
		DefaultTableModel model = new DefaultTableModel();
		tblDetalles.setModel(model);
		model.addColumn("Concepto");
		model.addColumn("Monto");
		tblDetalles.setEditable(false);
		Utilidades.packColumns(tblDetalles, 0);
	}
	
	private void addFila(){
		String concepto = txtConcepto.getText().trim();
		float monto = (float)txtMonto.getValue();
		
		if(concepto.length() != 0 && monto > 0f){
			Object[] fila = new Object[]{
				concepto, monto
			};
			((DefaultTableModel)tblDetalles.getModel()).addRow(fila);
			Utilidades.packColumns(tblDetalles, 0);
			actualizarMonto();
		}else{
			WebOptionPane.showMessageDialog(this, "Ingrese un concepto y monto válidos",
					"Advertencia", WebOptionPane.WARNING_MESSAGE);
		}
	}
	
	private void guardar(){
		if(tblDetalles.getRowCount() > 0){
	        LocalDate fecha = (txtFecha.getDate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	        
	        Caja origen = cboOrigen.isEnabled() ? (Caja)cboOrigen.getSelectedItem() : null;
	        Caja destino = cboDestino.isEnabled() ? (Caja)cboDestino.getSelectedItem() : null;
	        
			movimientoCaja = new MovimientoCaja(-1, 
					(TipoMovimientoCaja)cboTipo.getSelectedItem(),
					1,
					fecha,
					getMonto(),
					CtrlSesion.usuario,
					'G',
					origen,
					destino
			);
			
			detalles = new ArrayList<DetalleMovimientoCaja>();
			for(int i = 0; i < tblDetalles.getRowCount(); i++){
				float monto = Float.parseFloat(tblDetalles.getValueAt(i, 1).toString());
				String descripcion = tblDetalles.getValueAt(i, 0).toString();
				detalles.add(new DetalleMovimientoCaja(-1, movimientoCaja, monto, descripcion));
			}
			try{
				CtrlMovimientoCaja.insert(movimientoCaja, false, detalles);
				WebOptionPane.showMessageDialog(this, 
						"Información guardada correctamente",
						"Éxito", WebOptionPane.INFORMATION_MESSAGE);
				nuevoMovimiento();
			}catch(SessionException e){
				WebOptionPane.showMessageDialog(this, 
						"No tiene permisos para acceder a este recurso",
						"Acceso denegado", WebOptionPane.WARNING_MESSAGE);
			}catch(Exception e){
				WebOptionPane.showMessageDialog(this, 
						"Error al intentar guardar la información, intente nuevamente",
						"Error interno", WebOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	private float getMonto(){
		float monto = 0f;
		for(int i = 0; i < tblDetalles.getRowCount(); i++){
			monto += Float.valueOf(tblDetalles.getValueAt(i, 1).toString());
		}
		return monto;
	}
	
	private void actualizarMonto(){
		lblTotal.setText("Total: S/. " + getMonto());
	}
	
	private void eliminar(){
		int row = tblDetalles.getSelectedRow();
		if(row >= 0){
			((DefaultTableModel)tblDetalles.getModel()).removeRow(row);
		}
		actualizarMonto();
	}
	
	private void nuevoMovimiento(){
		int id = movimientoCaja.getIdMovimientoCaja();
		for(int i = 0; i < tblDetalles.getRowCount(); i++){
			((DefaultTableModel)tblDetalles.getModel()).removeRow(i);
		}
		txtConcepto.setText("");
		txtMonto.setValue(0.1f);
		lblTotal.setText("Total: S/. 0.00");
		lblId.setText("Último ID: " + id);
		movimientoCaja = null;
		detalles = new ArrayList<>();
	}
}
