package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

import clases.Proveedor;

import com.alee.extended.date.WebDateField;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.splitpane.WebSplitPane;
import com.alee.laf.table.WebTable;

import controlador.CtrlCompra;
import controlador.CtrlProveedor;
import utilidades.SessionException;
import utilidades.Utilidades;

@SuppressWarnings("serial")
public class FrmConsultaCompra extends WebFrame{
	private WebDateField txtFechaInicio, txtFechaFin;
	private WebComboBox cboProveedor;
	private WebCheckBox chbAnulados;
	private WebTable tblCompras, tblDetalles;	
	private WebButton btnAnular, btnCerrar, btnBuscar;
	
	public FrmConsultaCompra() {
		setTitle("Registro de Compras");
		setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
		setShowResizeCorner(false);
		setSize(900, 500);
		initComponents();
		setLocationRelativeTo(null);
	}
	
	private void initComponents(){
		
		txtFechaInicio = new WebDateField(new Date());
		txtFechaFin = new WebDateField(new Date());
		cboProveedor = new WebComboBox();
		chbAnulados = new WebCheckBox("Anulados");
		tblCompras = new WebTable();
		tblDetalles = new WebTable();
		btnBuscar = Utilidades.getIconButton(
				new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[0])),
				new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[1])));
		btnAnular = new WebButton("Anular");
		btnCerrar = new WebButton("Cerrar");
		
		llenarProveedor();
		configTablaCompra();
		configTablaDetalles();
		
		add(new WebPanel(){{
			setBackground(Color.white);
			setLayout(new BorderLayout());
			add(new GroupPanel(){{
				setLayout(new GridBagLayout());
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = 0;
				gbc.gridheight = 1;
				gbc.weightx = 0.2;
				gbc.fill = GridBagConstraints.HORIZONTAL;

				add(new GroupPanel(){{
					add(new WebLabel("Inicio"));
					add(txtFechaInicio);
				}}, gbc);

				gbc.gridx = 1;
				add(new GroupPanel(){{
					add(new WebLabel("Fin"));
					add(txtFechaFin);
				}}, gbc);

				gbc.gridx = 2;
				gbc.weightx = 0.3;
				add(new GroupPanel(){{
					add(new WebLabel("Proveedor"));
					add(cboProveedor);
				}}, gbc);

				gbc.gridx = 3;
				gbc.weightx = 0.2;
				add(chbAnulados, gbc);

				gbc.gridx = 4;
				gbc.weightx = 0.1;
				add(new GroupPanel(){{
					add(btnBuscar);
				}}, gbc);
			}}, BorderLayout.NORTH);
			add(new WebSplitPane(
					WebSplitPane.HORIZONTAL_SPLIT,
					new WebScrollPane(tblCompras),
					new WebScrollPane(tblDetalles)
					){
				{
					setDividerLocation(500);
				}
			}, BorderLayout.CENTER);
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				add(btnAnular, BorderLayout.WEST);
				add(btnCerrar, BorderLayout.EAST);
			}}, BorderLayout.SOUTH);
		}});
		
		ListSelectionModel tblSelect = tblCompras.getSelectionModel();
		tblSelect.addListSelectionListener((ListSelectionEvent e) ->{
			configTablaDetalles();	
		});

		chbAnulados.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				configTablaCompra();
			}
		});

		btnBuscar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				configTablaCompra();
			}
		});

		btnAnular.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				anular();
			}
		});

		btnCerrar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				cerrar();
			}
		});

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent we){
				FrmPrincipal.bolFrmConsultaCompra = false;
			}
		});
	}
	
	private void llenarProveedor(){
		ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
		try{
			proveedores = CtrlProveedor.getProveedores_busqueda("", false);
			proveedores.add(0, new Proveedor(){{
				setIdProveedor(-1); setNombre("Todos");
			}});
			cboProveedor = new WebComboBox(proveedores.toArray());
		}catch(Exception e){
			WebOptionPane.showMessageDialog(this,
					"Error al cargar los proveedores", 
					"Error interno", WebOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void configTablaCompra(){
		DefaultTableModel model = new DefaultTableModel();
		tblCompras.setModel(model);
		model.addColumn("ID");
		model.addColumn("Proveedor");
		model.addColumn("Monto");
		model.addColumn("IGV");
		model.addColumn("Fecha");
		model.addColumn("Usuario");
		model.addColumn("Factura");
		model.addColumn("Estado");
		try{
			LocalDate fechaInicio = (txtFechaInicio.getDate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			LocalDate fechaFin = (txtFechaFin.getDate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			Proveedor proveedor = cboProveedor.getSelectedIndex() == 0 ? null : (Proveedor)cboProveedor.getSelectedItem();
			
			ArrayList<Object[]> compras = CtrlCompra.getConsulta(fechaInicio.toString(), fechaFin.toString(), proveedor, chbAnulados.isSelected(), false);
			for (Object[] compra : compras) {
				compra[2] = utilidades.Utilidades.setUpMoneda((float)compra[2]);
				compra[3] = utilidades.Utilidades.setUpMoneda((float)compra[3]);
				model.addRow(compra);
			}
		}catch(SessionException e){
			WebOptionPane.showMessageDialog(this, "No tiene los permisos suficientes para ejecutar esta acción",
					"Acceso denegado", WebOptionPane.WARNING_MESSAGE);
		}catch(Exception e){
			WebOptionPane.showMessageDialog(this,
					"Error al cargar las compras", 
					"Error interno", WebOptionPane.ERROR_MESSAGE);
		}
		Utilidades.packColumns(tblCompras, 0);
		tblCompras.setEditable(false);
		tblCompras.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}
	
	private void configTablaDetalles(){
		DefaultTableModel model = new DefaultTableModel();
		tblDetalles.setModel(model);
		model.addColumn("ID");
		model.addColumn("Articulo");
		model.addColumn("Cantidad");
		model.addColumn("Precio");
		model.addColumn("Total");
		
		int row = tblCompras.getSelectedRow();
		
		if(row >= 0){
			try{
				int idCompra = (int)tblCompras.getValueAt(row, 0);
				ArrayList<Object[]> detalles = CtrlCompra.getConsulta_Detalle(idCompra, false);
				for (Object[] detalle : detalles) {
					detalle[3] = utilidades.Utilidades.setUpMoneda((float)detalle[3]);
					detalle[4] = utilidades.Utilidades.setUpMoneda((float)detalle[4]);
					model.addRow(detalle);
				}
			}catch(SessionException e){
				WebOptionPane.showMessageDialog(this, "No tiene los permisos suficientes para ejecutar esta acción",
						"Acceso denegado", WebOptionPane.WARNING_MESSAGE);
			}catch(Exception e){
				WebOptionPane.showMessageDialog(this,
						"Error al cargar los detalles", 
						"Error interno", WebOptionPane.ERROR_MESSAGE);
			}
			
		}
		tblDetalles.removeColumn(tblDetalles.getColumnModel().getColumn(0));
		Utilidades.packColumns(tblDetalles, 0);
		tblDetalles.setEditable(false);
		tblDetalles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}
	
	private void anular(){
		int row;
		if((row = tblCompras.getSelectedRow()) >= 0){
			int idCompra = (int)tblCompras.getValueAt(row, 0);
			try {
				CtrlCompra.anularCompra(idCompra, false);
				WebOptionPane.showMessageDialog(this, "Anulado correctamente", "Éxito", WebOptionPane.INFORMATION_MESSAGE);
			} catch (SessionException e) {
				WebOptionPane.showMessageDialog(this, "No tiene los permisos suficientes para ejecutar esta acción",
						"Acceso denegado", WebOptionPane.WARNING_MESSAGE);
			} catch (Exception e) {
				WebOptionPane.showMessageDialog(this, "No puede anular",
						"Error", WebOptionPane.WARNING_MESSAGE);
			}
			configTablaCompra();
		}		
	}
	
	private void cerrar(){
		dispose();
	}
}
