package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import utilidades.Excepcion;
import utilidades.SessionException;
import utilidades.Utilidades;
import clases.Articulo;
import clases.DetalleOrdenCompra;
import clases.OrdenCompra;
import clases.Proveedor;

import com.alee.extended.date.DateSelectionListener;
import com.alee.extended.date.WebDateField;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.spinner.WebSpinner;
import com.alee.laf.splitpane.WebSplitPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.text.WebTextField;

import controlador.CtrlArticulo;
import controlador.CtrlCompra;
import controlador.CtrlOrdenCompra;
import controlador.CtrlProveedor;
import controlador.CtrlSesion;

@SuppressWarnings("serial")
public class FrmOrdenCompra extends WebFrame{
	private WebDateField txtFecha,txtEntrega;
	private WebComboBox cboProveedor;
	private WebTextField txtArticulo;
	private WebCheckBox chbStock;
	private WebTable tblResultadoArticulos,
					 tblOrdenArticulos;
	private WebButton btnNuevo,
					btnAgregar,
					btnBorrar,
					btnEditar,
					btnGuardar,
					btnBuscar;
	private WebLabel lblLastId,
					lblMonto;

	private OrdenCompra orden;
	private ArrayList<DetalleOrdenCompra> detalles;

	private float monto;

	public FrmOrdenCompra(){
		setTitle("Preventa");
        setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
        setSize(800, 500);
        setShowResizeCorner(false);
        setLocationRelativeTo(null);
        initComponents();
	}

	private void initComponents() {
		GroupPanel pnlResultado = new GroupPanel();
		GroupPanel pnlOrden = new GroupPanel();

		txtFecha = new WebDateField(new Date());
		txtEntrega = new WebDateField(new Date());
		cboProveedor = new WebComboBox();
		chbStock = new WebCheckBox("Sin Stock");
		txtArticulo = new WebTextField();
		tblResultadoArticulos = new WebTable();
		tblOrdenArticulos = new WebTable();
		lblLastId = new WebLabel("ID: ");
		lblMonto = new WebLabel("S/. ");

		detalles = new ArrayList<DetalleOrdenCompra>();

		btnAgregar = Utilidades.getButton(
				new ImageIcon(getClass().getResource(Utilidades.N_AGREGAR[0])),
				new ImageIcon(getClass().getResource(Utilidades.N_AGREGAR[1])));
		btnGuardar = Utilidades.getButton(
				new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[0])),
				new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[1])));
		btnBuscar = Utilidades.getIconButton(
				new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[0])),
				new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[1])));
		btnNuevo = new WebButton("Nuevo");
		btnBorrar = new WebButton("Borrar");
		btnEditar = new WebButton("Editar");

		llenarProveedores();
		configTablaResultado();
		configTablaOrden();

		add(new WebPanel(){{
			setBackground(Color.white);
			setLayout(new BorderLayout());
			add(new GroupPanel(){{
				setLayout(new GridLayout(2,1));
				add(new GroupPanel(){{
					setLayout(new GridBagLayout());
					GridBagConstraints gbc = new GridBagConstraints();
					gbc.gridx = 0;
		            gbc.gridy = 0;
		            gbc.gridwidth = 1;
		            gbc.weightx = 0.2;
		            gbc.fill = GridBagConstraints.HORIZONTAL;
					add(new GroupPanel(){{
						add(new WebLabel("Fecha: "));
						add(txtFecha);
					}}, gbc);

					gbc.gridx = 1;
					add(new GroupPanel(){{
						add(new WebLabel("Fecha Entrega: "));
						add(txtEntrega);
					}}, gbc);

					gbc.gridx = 2;
					gbc.weightx = 0.5;
					add(new GroupPanel(){{
						add(new WebLabel("Proveedor: "));
						add(cboProveedor);
					}}, gbc);
				}});
				add(new GroupPanel(){{
					setLayout(new GridBagLayout());
					GridBagConstraints gbc = new GridBagConstraints();
					gbc.gridx = 0;
		            gbc.gridy = 0;
		            gbc.gridwidth = 1;
		            gbc.weightx = 0.5;
		            gbc.fill = GridBagConstraints.HORIZONTAL;
					add(new GroupPanel(){{
						setLayout(new BorderLayout());
						add(new WebLabel("Articulo: "), BorderLayout.WEST);
						add(txtArticulo, BorderLayout.CENTER);
					}}, gbc);

					gbc.gridx = 1;
					gbc.weightx = 0.1;
					add(new GroupPanel(){{
						setLayout(new BorderLayout());
						add(chbStock, BorderLayout.CENTER);
						add(btnBuscar, BorderLayout.EAST);
					}}, gbc);
				}});
			}}, BorderLayout.NORTH);
			add(new WebSplitPane(WebSplitPane.HORIZONTAL_SPLIT, pnlResultado, pnlOrden){
				private static final long serialVersionUID = 1L;
			{
				setDividerLocation(500);
			}}, BorderLayout.CENTER);
		}});

		pnlResultado.setLayout(new BorderLayout());
		pnlResultado.add(new WebScrollPane(tblResultadoArticulos), BorderLayout.CENTER);
		pnlResultado.add(new GroupPanel(){{
			setLayout(new GridLayout(2,1));
			add(new GroupPanel(){{
				add(lblLastId);
			}});
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				add(btnNuevo, BorderLayout.WEST);
				add(btnAgregar, BorderLayout.EAST);
			}});

		}}, BorderLayout.SOUTH);

		pnlOrden.setLayout(new BorderLayout());
		pnlOrden.add(new WebScrollPane(tblOrdenArticulos), BorderLayout.CENTER);
		pnlOrden.add(new GroupPanel(){{
			setLayout(new GridLayout(2, 1));
			add(new GroupPanel(){{
				add(lblMonto);
			}});
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				add(new GroupPanel(){{
					add(btnBorrar);
					add(btnEditar);
				}}, BorderLayout.WEST);
				add(btnGuardar, BorderLayout.EAST);
			}});
		}}, BorderLayout.SOUTH);

		addWindowListener(new WindowAdapter(){
			public void windowClosed(WindowEvent we){
				FrmPrincipal.bolFrmOrdenCompra = false;
			}
		});

		btnGuardar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				guardar();
			}
		});

		btnBuscar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				configTablaResultado();
			}
		});

		btnAgregar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				addFila();
			}
		});

		btnEditar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				editarFila();
			}
		});

		btnBorrar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				borrarFila();
			}
		});

		chbStock.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				configTablaResultado();
			}
		});

		txtArticulo.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				configTablaResultado();
			}
		});
		
		txtFecha.addDateSelectionListener(new DateSelectionListener(){
        	@Override
			public void dateSelected(Date fecha) {
				if(Utilidades.compararFechas(fecha, new Date()) < 0){
					WebOptionPane.showMessageDialog(FrmOrdenCompra.this,
							"No se puede registrar una Orden con una fecha futura",
							"Conflicto de fechas", WebOptionPane.WARNING_MESSAGE);
					txtFecha.setDate(new Date());
				}
			}
        });
		
		txtEntrega.addDateSelectionListener(new DateSelectionListener(){
			@Override
			public void dateSelected(Date fechaFin) {
				if(Utilidades.compararFechas(txtFecha.getDate(), fechaFin) == -1){
					WebOptionPane.showMessageDialog(FrmOrdenCompra.this,
							"La fecha de entrega no puede ser menor a la fecha inicial.", 
							"Conflicto de fechas", WebOptionPane.WARNING_MESSAGE);
					txtEntrega.setDate(txtFecha.getDate());
				}
			}			
		});
	}

	private void llenarProveedores(){
		try {
			cboProveedor = new WebComboBox(CtrlProveedor.getProveedores_busqueda("", false).toArray());
		}catch(Exception e){
			WebOptionPane.showMessageDialog(this,
					"Error al cargar los proveedores, por favor intente más tarde",
					"Error interno", WebOptionPane.ERROR_MESSAGE);
		}
	}

	public void configTablaResultado(){
        DefaultTableModel model = new DefaultTableModel();
        tblResultadoArticulos.setModel(model);
        model.addColumn("ID");
        model.addColumn("Nombre");
        model.addColumn("Stock");
        model.addColumn("Total");
        model.addColumn("Pedido");
        try {
			ArrayList<Object[]> articulos = CtrlArticulo.getArticulos_OrdenCompra(
					CtrlSesion.almacen.getIdAlmacen(),
					txtArticulo.getText(),
					chbStock.isSelected(), false);
			for (Object[] articulo : articulos) {
				model.addRow(articulo);
			}
        }catch(Exception e){
			WebOptionPane.showMessageDialog(this,
					"Error al cargar los artículos, por favor intente más tarde",
					"Error interno", WebOptionPane.ERROR_MESSAGE);
		}
        Utilidades.packColumns(tblResultadoArticulos, 0);
        tblResultadoArticulos.setEditable(false);
        tblResultadoArticulos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

	private void configTablaOrden(){
        DefaultTableModel model = new DefaultTableModel();
        tblOrdenArticulos.setModel(model);
        model.addColumn("ID");
        model.addColumn("Nombre");
        model.addColumn("Cant.");
        model.addColumn("S/.");
        model.addColumn("Total");
        Utilidades.packColumns(tblOrdenArticulos, 0);
        tblOrdenArticulos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblOrdenArticulos.setEditable(false);
    }

	private void addFila(){
		int idArticulo = (int)tblResultadoArticulos.getValueAt(tblResultadoArticulos.getSelectedRow(), 0);
		if(!existeProducto(idArticulo)){
			try{
				float lastCost = CtrlCompra.lastCost(idArticulo, false);
				DialogCantidadCosto dcc = new DialogCantidadCosto(this, true, lastCost);
				dcc.setVisible(true);
				int cantidad = dcc.getCantidad();
				float costo = dcc.getCosto();
				if(cantidad != -1){
					((DefaultTableModel)tblOrdenArticulos.getModel()).addRow(new Object[]{
						tblResultadoArticulos.getValueAt(tblResultadoArticulos.getSelectedRow(), 0),
						tblResultadoArticulos.getValueAt(tblResultadoArticulos.getSelectedRow(), 1),
						cantidad,
						utilidades.Utilidades.setUpMoneda(costo),
						utilidades.Utilidades.setUpMoneda(cantidad * costo)
					});
					Utilidades.packColumns(tblOrdenArticulos, 0);
				}else
					WebOptionPane.showMessageDialog(this, "Ingrese cantidad correctamente", "Advertencia",
	    					WebOptionPane.WARNING_MESSAGE);
				
				actualizarMonto();
			}catch(NumberFormatException | Excepcion | SessionException | SQLException e){
				WebOptionPane.showMessageDialog(this, "Ingresar un número válido",
						"Atención", WebOptionPane.ERROR_MESSAGE);
			}
		}else{
			WebOptionPane.showMessageDialog(this,
					"El artículo ya ha sido agregado a la lista del pedido",
					"Atención", WebOptionPane.WARNING_MESSAGE);
		}
	}

	private void borrarFila(){
		int row = tblOrdenArticulos.getSelectedRow();
		((DefaultTableModel)tblOrdenArticulos.getModel()).removeRow(row);
		actualizarMonto();
	}

	private void editarFila(){
		int row = tblOrdenArticulos.getSelectedRow();
		int idArticulo = (int) tblOrdenArticulos.getValueAt(row, 0);
		if(row >= 0)			
			try{
				float lastCost = CtrlCompra.lastCost(idArticulo, false);
				DialogCantidadCosto dcc = new DialogCantidadCosto(this, true, lastCost);
				dcc.setVisible(true);
				int cantidad = dcc.getCantidad();
				float costo = dcc.getCosto();
				if(cantidad != -1){
					tblOrdenArticulos.setValueAt(cantidad, row, 2);
					tblOrdenArticulos.setValueAt(utilidades.Utilidades.setUpMoneda(costo), row, 3);
					tblOrdenArticulos.setValueAt(utilidades.Utilidades.setUpMoneda(cantidad * costo), row, 4);
				}else{
					WebOptionPane.showMessageDialog(this, "Ingrese cantidad correctamente", "Advertencia",
	    					WebOptionPane.WARNING_MESSAGE);
				}
				actualizarMonto();
			}catch(NumberFormatException | Excepcion | SessionException | SQLException e){
				WebOptionPane.showMessageDialog(this, "Ingresar un número válido",
						"Atención", WebOptionPane.ERROR_MESSAGE);
			}
	}

	private boolean existeProducto(int idProducto){
		for(int i = 0; i<tblOrdenArticulos.getRowCount(); i++){
			if((int)tblOrdenArticulos.getValueAt(i, 0) == idProducto)
				return true;
		}
		return false;
	}

	private void actualizarMonto(){
		monto = 0f;
		for(int i = 0; i < tblOrdenArticulos.getRowCount(); i++){
			monto += utilidades.Utilidades.getMoneda(tblOrdenArticulos.getValueAt(i, 4).toString());
		}
		lblMonto.setText("S/. " + monto);
	}

	private boolean prepararDatos(){
		if(tblOrdenArticulos.getRowCount() > 0){
			LocalDate fecha = this.txtFecha.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			LocalDate fechaEntrega = this.txtEntrega.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			orden = new OrdenCompra(){{
				setEstado('G');
				setMonto(monto);
				setUsuario(CtrlSesion.usuario);
				setProveedor((Proveedor)cboProveedor.getSelectedItem());
				setFecha(fecha);
				setFechaEntrega(fechaEntrega);
			}};

			Articulo articulo;
			for(int i = 0; i < tblOrdenArticulos.getRowCount(); i++){
				try{
					articulo = CtrlArticulo.get((int)tblOrdenArticulos.getModel().getValueAt(i, 0), false);
					detalles.add(new DetalleOrdenCompra(
						-1,
						orden,
						articulo,
						(int)tblOrdenArticulos.getModel().getValueAt(i, 2),
						utilidades.Utilidades.getMoneda(tblOrdenArticulos.getModel().getValueAt(i, 3).toString()),
						utilidades.Utilidades.getMoneda(tblOrdenArticulos.getModel().getValueAt(i, 4).toString())
					));
				}catch(Exception e){
					WebOptionPane.showMessageDialog(this,
							"Error al guardar la información, intentar nuevamente y",
							"Error interno", WebOptionPane.ERROR_MESSAGE);
					return false;
				}
			}
		}else{
			WebOptionPane.showMessageDialog(this, "No se han añadido artículos a la lista",
				"Advertencia", WebOptionPane.WARNING_MESSAGE);
			return false;
		}
		return true;
	}

	private void guardar(){
		if(prepararDatos()){
			try{
				CtrlOrdenCompra.insert(orden, false, detalles);
				WebOptionPane.showMessageDialog(this,
                        "Información registrada correctamente",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
				lblLastId.setText("ID: " + orden.getIdOrdenCompra());
			} catch (SessionException e) {
				e.printStackTrace();
			}catch(Exception e){
				WebOptionPane.showMessageDialog(this,
						"Error al guardar la información, intentar nuevamente x",
						"Error interno", WebOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}
	class DialogCantidadCosto extends WebDialog{
    	private WebSpinner txtCantidad;
    	private WebTextField txtCostoUnit, txtCostoTot;
    	private WebLabel lblCostoAnterior;
    	private WebButton btnAceptar;
    	private WebButton btnCancelar;
    	
    	float costoAnterior;
    	boolean flag;
    	    	
    	public DialogCantidadCosto(WebFrame frame, boolean modal, float costoAnterior){    		
    		super(frame, modal);
    		setDefaultCloseOperation(WebFrame.HIDE_ON_CLOSE);
    		setSize(250, 400);
    		this.costoAnterior = costoAnterior;
    		initComponents();
    		setLocationRelativeTo(null);
    		pack();
    	}
    	
    	private void initComponents(){
    		txtCantidad = new WebSpinner(new SpinnerNumberModel(1, 1, 10000, 1));
    		txtCostoUnit = new WebTextField("" + this.costoAnterior);
    		txtCostoTot = new WebTextField();
    		lblCostoAnterior = new WebLabel("Anterior: " + costoAnterior);
    		btnAceptar = new WebButton("Aceptar");
    		btnCancelar = new WebButton("Cancelar");
    		flag = false;
    		    		
    		add(new WebPanel(){{
    			setBackground(Color.white);
    			setLayout(new BorderLayout());
    			add(new GroupPanel(){{
    				setLayout(new GridBagLayout());
    				GridBagConstraints gbc = new GridBagConstraints();
    				gbc.gridx = 0;
    				gbc.gridy = 0;
    				gbc.gridwidth = 1;
    				gbc.weightx = 0.2;
    				gbc.fill = GridBagConstraints.HORIZONTAL;
    				
    				add(new WebLabel("Cantidad"), gbc);
    				
    				gbc.gridx = 1;
    				gbc.weightx = 0.8;
    				add( txtCantidad, gbc);
    				
    				gbc.gridx = 0;
    				gbc.gridy = 1;
    				gbc.weightx = 0.2;
    				add(new WebLabel("Costo Unit"), gbc);
    				
    				gbc.gridx = 1;
    				gbc.weightx = 0.8;
    				add(txtCostoUnit, gbc);
    				
    				gbc.gridx = 0;
    				gbc.gridy = 2;
    				gbc.weightx = 0.2;
    				add(new WebLabel("Costo Total"), gbc);
    				
    				gbc.gridx = 1;
    				gbc.weightx = 0.8;
    				add(txtCostoTot, gbc);
    				
    				gbc.gridx = 0;
    				gbc.gridy = 3;
    				gbc.weightx = 0.2;
    				add(new WebLabel("Costo Anterior"), gbc);
    				
    				gbc.gridx = 1;
    				gbc.weightx = 0.8;
    				add(lblCostoAnterior, gbc);
    			}}, BorderLayout.CENTER);
    			add(new GroupPanel(){{
    				setLayout(new FlowLayout(FlowLayout.RIGHT));
    				add(btnAceptar);
    				add(btnCancelar);
    			}}, BorderLayout.SOUTH);
    		}});
    		txtCantidad.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					calcularCosto();
				}
			});

    		txtCostoTot.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					calcularCosto();					
				}
			});
    		btnAceptar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					flag = true;
					setVisible(false);
				}
			});
    		btnCancelar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					flag = false;
					setVisible(false);
				}
			});
    		
    		txtCantidad.getEditor().getComponent(0).addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent e) {
					if(e.getKeyChar() == KeyEvent.VK_ENTER){
						flag = true;
						setVisible(false);
					}
					
				}
				
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
    	}
    	
    	private void calcularCosto(){
    		float costoTotal;
    		float costoUnit;
			int cantidad;
			try{
				costoTotal = Float.parseFloat(txtCostoTot.getText());
				cantidad = (Integer)txtCantidad.getValue();
			}catch(NumberFormatException ex){
				costoTotal = 0f;
				cantidad = 1;
			}
			
			costoUnit = costoTotal / cantidad;
			
			if(costoUnit != 0)
				txtCostoUnit.setText(String.valueOf(costoUnit));
    	}
    	
    	public int getCantidad(){
    		int cantidad;
    		try{
    			cantidad = Integer.parseInt(txtCantidad.getValue().toString());
    		}catch(NumberFormatException e){
    			cantidad = -1;
    		}
    		return flag ? cantidad : -1;
    	}
    	
    	public float getCosto(){
    		float costoUnit;
    		try{
    			costoUnit = Float.parseFloat(txtCostoUnit.getText());
    		}catch(NumberFormatException e){
    			costoUnit = 0f;
    		}
    		return flag ? costoUnit : -1;
    	}
    }
}
