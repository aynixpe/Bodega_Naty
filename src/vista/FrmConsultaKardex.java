package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import clases.Almacen;
import clases.Articulo;

import com.alee.extended.date.DateSelectionListener;
import com.alee.extended.date.WebDateField;
import com.alee.extended.image.WebImage;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.text.WebTextField;

import controlador.CtrlAlmacen;
import controlador.CtrlKardex;
import utilidades.Utilidades;

@SuppressWarnings("serial")
public class FrmConsultaKardex extends WebFrame{
	private WebDateField txtFechaInicio, txtFechaFin;
	private WebComboBox cboAlmacen;
	private WebTable tblKardex;
	private WebTextField txtArticulo;
	private WebButton btnBuscar;
	
	private Articulo articulo;
	
	public FrmConsultaKardex() {
		setTitle("Kardex");
		setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
		setShowMaximizeButton(false);
		setShowResizeCorner(false);
		setSize(900, 500);
		initComponents();
		setLocationRelativeTo(null);
	}
	
	private void initComponents(){
		articulo = new Articulo(){{
			setIdArticulo(-1);
		}};
		
		cboAlmacen = new WebComboBox();		
		tblKardex = new WebTable();
		txtFechaInicio = new WebDateField(new Date());
		txtFechaFin = new WebDateField(new Date());
		txtArticulo = new WebTextField(30);
		btnBuscar = Utilidades.getIconButton(
				new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[0])),
				new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[1])));
		
		txtArticulo.setTrailingComponent(new WebImage(getClass().getResource("/resources/img/zoom.png")));
		cargarAlmacenes();
		configTable();
		articulo = null;
		add(new WebPanel(){{
			setBackground(Color.white);
			setLayout(new BorderLayout());
			
			add(new GroupPanel(){{
				setLayout(new GridLayout(2, 1));
				add(new GroupPanel(){{
					setLayout(new GridBagLayout());
					GridBagConstraints gbc = new GridBagConstraints();
					gbc.gridx = 0;
	                gbc.gridy = 0;
	                gbc.gridwidth = 1;
	                gbc.weightx = 0.7;
	                gbc.fill = GridBagConstraints.HORIZONTAL;
	                add(new GroupPanel(){{
						add(new WebLabel("Almacen"));
						add(cboAlmacen);
					}}, gbc);
	                
	                gbc.gridx = 1;
	                gbc.weightx = 0.15;
	                add(new GroupPanel(){{
						add(new WebLabel("Inicio"));
						add(txtFechaInicio);
					}}, gbc);
	                
	                gbc.gridx = 2;
	                gbc.weightx = 0.15;
	                add(new GroupPanel(){{
						add(new WebLabel("Fin"));
						add(txtFechaFin);
					}}, gbc);
				}});
				add(new GroupPanel(){{
					setLayout(new BorderLayout());
					add(new WebLabel("Artículo"), BorderLayout.WEST);
					add(txtArticulo, BorderLayout.CENTER);
					add(btnBuscar, BorderLayout.EAST);
				}});
			}}, BorderLayout.NORTH);
			
			add(new WebScrollPane(tblKardex), BorderLayout.CENTER);
		}});
		
		txtFechaInicio.addDateSelectionListener(new DateSelectionListener(){
			@Override
			public void dateSelected(Date fechaInicio) {
				if(Utilidades.compararFechas(fechaInicio, txtFechaFin.getDate()) == -1){
					WebOptionPane.showMessageDialog(FrmConsultaKardex.this,
							"La fecha inicial no puede ser mayor a la fecha final.", 
							"Conflicto de fechas", WebOptionPane.WARNING_MESSAGE);
					txtFechaInicio.setDate(txtFechaFin.getDate());
				}
			}			
		});
		
		txtFechaFin.addDateSelectionListener(new DateSelectionListener(){
			@Override
			public void dateSelected(Date fechaFin) {
				if(Utilidades.compararFechas(txtFechaInicio.getDate(), fechaFin) == -1){
					WebOptionPane.showMessageDialog(FrmConsultaKardex.this,
							"La fecha final no puede ser menor a la fecha inicial.", 
							"Conflicto de fechas", WebOptionPane.WARNING_MESSAGE);
					txtFechaFin.setDate(txtFechaInicio.getDate());
				}
			}			
		});
		
		txtArticulo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FrmBusquedaArticulo fba = new FrmBusquedaArticulo(FrmConsultaKardex.this,
						true, txtArticulo.getText(), (Almacen)cboAlmacen.getSelectedItem(), false, false);
				fba.setVisible(true);
				articulo = fba.getArticulo();
				if(articulo != null){
					txtArticulo.setText(articulo.getNombre());
				}
				configTable();
			}
		});
		
		btnBuscar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				configTable();
			}
		});
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent we){
				FrmPrincipal.bolFrmConsultaKardex = false;
			}
		});
	}
	
	private void cargarAlmacenes(){
    	cboAlmacen.removeAllItems();
    	try{
    		ArrayList<Almacen> almacenes = CtrlAlmacen.getAlmacenes("", false);
            cboAlmacen = new WebComboBox(almacenes.toArray());
            cboAlmacen.setSelectedIndex(0);
    	}catch(Exception e){
    		WebOptionPane.showMessageDialog(this, 
    				"Error al cargar los almacences: " +  e.getMessage(),
    				"Error interno", WebOptionPane.ERROR_MESSAGE);
    	}
        
    }
		
	private void configTable(){
		DefaultTableModel model = new DefaultTableModel();
		Almacen almacen = (Almacen) cboAlmacen.getSelectedItem();
		
		tblKardex.setModel(model);
		model.addColumn("ID");
		model.addColumn("Fecha");
		model.addColumn("Almacen");
		model.addColumn("Articulo");
		model.addColumn("Movimiento");
		model.addColumn("Tipo");
		model.addColumn("-");
		model.addColumn("#");
		model.addColumn("Antes");
		model.addColumn("Después");
		
		if(articulo != null){
			try{
				String fechaInicio = (txtFechaInicio.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).toString();
				String fechaFin = (txtFechaFin.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).toString();
				
				ArrayList<Object[]> kardex = CtrlKardex.getConsulta(fechaInicio, fechaFin, articulo, almacen, false);
				for (Object[] objects : kardex) {
					model.addRow(objects);
				}
			}catch(Exception e){
				e.printStackTrace();
				WebOptionPane.showMessageDialog(this,
						"Error al obtener los movimientos", "Error interno", WebOptionPane.ERROR_MESSAGE);			
			}
		}else{
			WebOptionPane.showMessageDialog(this,
					"No se ha elegido un artículo", "Advertencia", WebOptionPane.WARNING_MESSAGE);
		}
		Utilidades.packColumns(tblKardex, 0);
		tblKardex.setEditable(false);
		tblKardex.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}
}
