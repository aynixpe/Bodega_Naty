package vista;

import clases.TipoUsuario;

import com.alee.extended.image.DisplayType;
import com.alee.extended.image.WebImage;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;

import controlador.CtrlSesion;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import utilidades.Utilidades;

/**
 *
 * @author Edder
 */
@SuppressWarnings("serial")
public class FrmPrincipal extends WebFrame{
	private final int VENTAS = 0,
			COMPRAS = 1,
			ALMACEN = 2,
			CAJA = 3,
			SEGURIDAD = 4;
	private int selected;
	private Menu[] pnlMenu;
	private PnlContenido[] pnlContenido;
	private WebPanel contenido;
	
	public static boolean bolFrmAlmacenes,
						bolFrmCategorias,
						bolFrmArticulos,
						bolFrmMovimiento,
						bolFrmVenta,
						bolFrmOrdenCompra,
						bolFrmCompra,
						bolFrmConsultaMovimientos,
						bolFrmConsultaKardex,
						bolFrmConsultaOrdenCompra,
						bolFrmConsultaCompra,
						bolFrmConsultaVenta,
						bolFrmConsultaComprobante,
						bolFrmGestionUsuario,
						bolFrmMovimientoCaja,
						bolFrmConsultaMovimientoCaja,
						bolFrmProveedores
						;
	
	public FrmPrincipal(){
		super("Minimarket La SKINA");
		setSize(600,500);
		setDefaultCloseOperation(WebFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setShowMaximizeButton(false);
		initComponents();
	}
	
	private void initComponents(){
		pnlMenu = new Menu[4];
		pnlContenido = new PnlContenido[4];
		
		contenido = new WebPanel();
		contenido.setBackground(Color.white);
		contenido.setLayout(new BorderLayout());
		
		selected = COMPRAS;
		pnlMenu[VENTAS] = new Menu("Ventas", "venta", 150, 40);
		pnlMenu[COMPRAS] = new Menu("Compras", "compra", 150, 40);
		pnlMenu[ALMACEN] = new Menu("Almacén", "almacen", 150, 40);
		pnlMenu[CAJA] = new Menu("Caja", "caja", 150, 40);
		//pnlMenu[SEGURIDAD] = new Menu("Seguridad", "seguridad", 150, 40);

		loadVentas();
		loadCompras();
		loadAlmacen();
		loadCaja();
		//loadSeguridad();
		
		for(int i = 0; i < 4; i++){
			int index = i;
			pnlMenu[i].addMouseListener(new MouseListener() {
				@Override
				public void mouseReleased(MouseEvent e) {}
				
				@Override
				public void mousePressed(MouseEvent e) {}
				
				@Override
				public void mouseExited(MouseEvent e) {
					if (pnlMenu[index].state == Menu.ACTIVO)
						pnlMenu[index].state = Menu.INACTIVO;
					pnlMenu[index].update(pnlMenu[index].getGraphics());
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					if (pnlMenu[index].state == Menu.INACTIVO)
						pnlMenu[index].state = Menu.ACTIVO;
					pnlMenu[index].update(pnlMenu[index].getGraphics());
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					select(index);
				}
			});
		}
		
		add(new WebPanel(){{
			setBackground(Color.white);
			setLayout(new BorderLayout());
			add(new WebPanel(){{
				setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
				setBackground(Color.white);
				add(pnlMenu[VENTAS],
						pnlMenu[COMPRAS],
						pnlMenu[ALMACEN],
						pnlMenu[CAJA]
						//pnlMenu[SEGURIDAD]
						);
			}}, BorderLayout.WEST);
			add(contenido, BorderLayout.CENTER);
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				add(new WebLabel("Usuario: " + CtrlSesion.usuario.getNombre(), WebLabel.CENTER),
						BorderLayout.CENTER);
			}}, BorderLayout.SOUTH);
		}});
		select(VENTAS);
	}
	
	private void loadVentas(){
		ArrayList<ItemMenu> items = new ArrayList<ItemMenu>();
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/venta_add.png")).getImage(),
				"Nueva venta"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
			        		try {
								if(CtrlSesion.validarPermiso(TipoUsuario.VENTA, TipoUsuario.INSERTAR)){
									if(!FrmPrincipal.bolFrmVenta){
										FrmPrincipal.bolFrmVenta = true;
										new FrmVenta().setVisible(true);
									}	
								}else{
									WebOptionPane.showMessageDialog(FrmPrincipal.this, 
											"No tiene permisos para acceder a este recurso", 
											"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
								}
			        		}catch(Exception e){
								e.printStackTrace();
							}
			        	}
					});
				}});
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/venta_search.png")).getImage(),
				"Ver ventas"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
			        		try {
								if(CtrlSesion.validarPermiso(TipoUsuario.VENTA, TipoUsuario.CONSULTAR)){
									if(!FrmPrincipal.bolFrmConsultaVenta){
										FrmPrincipal.bolFrmConsultaVenta = true;
										new FrmConsultaVenta().setVisible(true);
									}	
								}else{
									WebOptionPane.showMessageDialog(FrmPrincipal.this, 
											"No tiene permisos para acceder a este recurso", 
											"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
								}
			        		}catch(Exception e){
								e.printStackTrace();
							}
			        	}
					});
				}});
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/comprobante_search.png")).getImage(),
				"Ver comprobantes"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
			        		try {
								if(CtrlSesion.validarPermiso(TipoUsuario.COMPROBANTE, TipoUsuario.CONSULTAR)){
									if(!FrmPrincipal.bolFrmConsultaComprobante){
										FrmPrincipal.bolFrmConsultaComprobante = true;
										new FrmConsultaComprobante().setVisible(true);
									}	
								}else{
									WebOptionPane.showMessageDialog(FrmPrincipal.this, 
											"No tiene permisos para acceder a este recurso", 
											"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
								}
			        		}catch(Exception e){
								e.printStackTrace();
							}
			        	}
					});
				}});
		/*items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/cliente_add.png")).getImage(),
				"Agregar cliente"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){/*
			        		try {
								if(!FrmPrincipal.bolFrm){
									FrmPrincipal.bolFrmConsultaComprobante = true;
									new FrmConsultaComprobante().setVisible(true);
								}
							} catch (HeadlessException e) {
								e.printStackTrace();
							}
			        	}
					});
				}});
		
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/cliente_search.png")).getImage(),
				"Consultar clientes"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){/*
			        		try {
								if(!FrmPrincipal.bolFrm){
									FrmPrincipal.bolFrmConsultaComprobante = true;
									new FrmConsultaComprobante().setVisible(true);
								}
							} catch (HeadlessException e) {
								e.printStackTrace();
							}
			        	}
					});
				}});*/
		
		pnlContenido[VENTAS] = new PnlContenido("VENTAS",
				"En este módulo puedes gestionar tus ventas, es decir,"
				+ " crear una nueva venta, consultar las ventas en un rango de fechas, así como anularlas, "
				+ "además puedes revisar los comprobantes generados, y también generar comprobantes "
				+ "de las ventas hechas en el día. \nTambién puedes gestionar tus clientes y consultar sus deudas "
				+ "pendientes.", 
				new ImageIcon(getClass().getResource("/resources/img/fp_ventas.png")).getImage(), 
				items);
	}
	
	private void loadCompras(){
		ArrayList<ItemMenu> items = new ArrayList<ItemMenu>();
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/orden_compra_add.png")).getImage(),
				"Nueva Orden de Compra"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
			        		try {
								if(CtrlSesion.validarPermiso(TipoUsuario.ORDEN_COMPRA, TipoUsuario.INSERTAR)){
									if(!FrmPrincipal.bolFrmOrdenCompra){
										FrmPrincipal.bolFrmOrdenCompra = true;
										new FrmOrdenCompra().setVisible(true);
									}	
								}else{
									WebOptionPane.showMessageDialog(FrmPrincipal.this, 
											"No tiene permisos para acceder a este recurso", 
											"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
								}
			        		}catch(Exception e){
								e.printStackTrace();
							}
			        	}
					});
				}});
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/orden_compra_search.png")).getImage(),
				"Consultar Órdenes de Compra"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
			        		try {
								if(CtrlSesion.validarPermiso(TipoUsuario.ORDEN_COMPRA, TipoUsuario.CONSULTAR)){
									if(!FrmPrincipal.bolFrmConsultaOrdenCompra){
										FrmPrincipal.bolFrmConsultaOrdenCompra= true;
										new FrmConsultaOrdenCompra().setVisible(true);
									}	
								}else{
									WebOptionPane.showMessageDialog(FrmPrincipal.this, 
											"No tiene permisos para acceder a este recurso", 
											"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
								}
			        		}catch(Exception e){
								e.printStackTrace();
							}
			        	}
					});
				}});
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/compra_add.png")).getImage(),
				"Nueva Compra"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
			        		try {
								if(CtrlSesion.validarPermiso(TipoUsuario.COMPRA, TipoUsuario.INSERTAR)){
									if(!FrmPrincipal.bolFrmCompra){
										FrmPrincipal.bolFrmCompra= true;
										new FrmCompra().setVisible(true);
									}	
								}else{
									WebOptionPane.showMessageDialog(FrmPrincipal.this, 
											"No tiene permisos para acceder a este recurso", 
											"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
								}
			        		}catch(Exception e){
								e.printStackTrace();
							}
			        	}
					});
				}});
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/compra_search.png")).getImage(),
				"Consultar Compras"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
			        		try {
								if(CtrlSesion.validarPermiso(TipoUsuario.COMPRA, TipoUsuario.CONSULTAR)){
									if(!FrmPrincipal.bolFrmConsultaCompra){
										FrmPrincipal.bolFrmConsultaCompra= true;
										new FrmConsultaCompra().setVisible(true);
									}	
								}else{
									WebOptionPane.showMessageDialog(FrmPrincipal.this, 
											"No tiene permisos para acceder a este recurso", 
											"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
								}
			        		}catch(Exception e){
								e.printStackTrace();
							}
			        	}
					});
				}});
		
		pnlContenido[COMPRAS] = new PnlContenido("COMPRAS",
				"En este módulo puedes gestionar tus pedidos (órdenes de compra), "
				+ "y tus compras, que pueden hacerse basadas en una orden de compra o "
				+ "de manera independiente", 
				new ImageIcon(getClass().getResource("/resources/img/fp_ventas.png")).getImage(), 
				items);
	}
	
	private void loadAlmacen(){
		ArrayList<ItemMenu> items = new ArrayList<ItemMenu>();
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/movimiento_add.png")).getImage(),
				"Nuevo Movimiento"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
			        		try {
								if(CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO, TipoUsuario.INSERTAR)){
									if(!FrmPrincipal.bolFrmMovimiento){
										FrmPrincipal.bolFrmMovimiento = true;
										new FrmMovimiento().setVisible(true);
									}	
								}else{
									WebOptionPane.showMessageDialog(FrmPrincipal.this, 
											"No tiene permisos para acceder a este recurso", 
											"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
								}
			        		}catch(Exception e){
								e.printStackTrace();
							}
			        	}
					});
				}});
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/movimiento_search.png")).getImage(),
				"Ver Movimientos"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
			        		try {
								if(CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO, TipoUsuario.CONSULTAR)){
									if(!FrmPrincipal.bolFrmConsultaMovimientos){
										FrmPrincipal.bolFrmConsultaMovimientos = true;
										new FrmConsultaMovimientos().setVisible(true);
									}	
								}else{
									WebOptionPane.showMessageDialog(FrmPrincipal.this, 
											"No tiene permisos para acceder a este recurso", 
											"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
								}
			        		}catch(Exception e){
								e.printStackTrace();
							}
			        	}
					});
				}});
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/kardex_search.png")).getImage(),
				"Ver Kardex"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
			        		try {
								if(CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO, TipoUsuario.CONSULTAR)){
									if(!FrmPrincipal.bolFrmConsultaKardex){
										FrmPrincipal.bolFrmConsultaKardex = true;
										new FrmConsultaKardex().setVisible(true);
									}	
								}else{
									WebOptionPane.showMessageDialog(FrmPrincipal.this, 
											"No tiene permisos para acceder a este recurso", 
											"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
								}
			        		}catch(Exception e){
								e.printStackTrace();
							}
			        	}
					});
				}});
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/almacenes.png")).getImage(),
				"Almacenes"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
							if(!FrmPrincipal.bolFrmAlmacenes){
								FrmPrincipal.bolFrmAlmacenes = true;
								new FrmAlmacenes().setVisible(true);
							}
			        	}
					});
				}});
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/categorias.png")).getImage(),
				"Categorías"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
							if(!FrmPrincipal.bolFrmCategorias){
								FrmPrincipal.bolFrmCategorias = true;
								new FrmCategorias().setVisible(true);
							}
			        	}
					});
				}});
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/articulos.png")).getImage(),
				"Artículos"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
							if(!FrmPrincipal.bolFrmArticulos){
								FrmPrincipal.bolFrmArticulos = true;
								new FrmArticulos().setVisible(true);
							}
			        	}
					});
				}});
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/almacenes.png")).getImage(),
				"Proveedores"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
							if(!FrmPrincipal.bolFrmProveedores){
								FrmPrincipal.bolFrmProveedores = true;
								new FrmProveedores().setVisible(true);
							}
			        	}
					});
				}});
		pnlContenido[ALMACEN] = new PnlContenido("ALMACÉN",
				"Gestiona tus almacenes de manera sencilla. Puedes modificar los almacenes, las categorías, y "
				+ "los artículos. Así como generar movimientos entre almacenes, o cargos y descargos, "
				+ "también puedes revisar el kardex de cada artículo en un determinado almacén.", 
				new ImageIcon(getClass().getResource("/resources/img/fp_ventas.png")).getImage(), 
				items);
	}
	
	private void loadCaja(){
		ArrayList<ItemMenu> items = new ArrayList<ItemMenu>();
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/caja_add.png")).getImage(),
				"Nuevo Movimiento Caja"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
			        		try {
								if(CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO_CAJA, TipoUsuario.INSERTAR)){
									if(!FrmPrincipal.bolFrmMovimientoCaja){
										FrmPrincipal.bolFrmMovimientoCaja = true;
										new FrmMovimientoCaja().setVisible(true);
									}	
								}else{
									WebOptionPane.showMessageDialog(FrmPrincipal.this, 
											"No tiene permisos para acceder a este recurso", 
											"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
								}
			        		}catch(Exception e){
								e.printStackTrace();
							}
			        	}
					});
				}});
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/iconos/caja_search.png")).getImage(),
				"Ver Movimientos Caja"){{
					addMouseListener(new MouseAdapter() {
						@Override
			        	public void mouseClicked(MouseEvent me){
			        		try {
								if(CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO_CAJA, TipoUsuario.CONSULTAR)){
									if(!FrmPrincipal.bolFrmConsultaMovimientoCaja){
										FrmPrincipal.bolFrmConsultaMovimientoCaja = true;
										new FrmConsultaMovimientoCaja().setVisible(true);
									}	
								}else{
									WebOptionPane.showMessageDialog(FrmPrincipal.this, 
											"No tiene permisos para acceder a este recurso", 
											"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
								}
			        		}catch(Exception e){
								e.printStackTrace();
							}
			        	}
					});
				}});
		
		pnlContenido[CAJA] = new PnlContenido("CAJA",
				"Gestiona caja y movimientos. Puedes realizar transferencias, así como ingresos y "
				+ "egresos. Todos tus movimientos son registrados para un mejor control, fácil, "
				+ "rápido y seguro.", 
				new ImageIcon(getClass().getResource("/resources/img/fp_ventas.png")).getImage(), 
				items);
	}
	
	@SuppressWarnings("unused")
	private void loadSeguridad(){
		ArrayList<ItemMenu> items = new ArrayList<ItemMenu>();
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/img/add.png")).getImage(),
				"Nueva Orden de Compra"));
		items.add(new ItemMenu(
				new ImageIcon(getClass().getResource("/resources/img/zoom.png")).getImage(),
				"Consultar Órdenes de Compra"));
		
		pnlContenido[SEGURIDAD] = new PnlContenido("SEGURIDAD",
				"En un lugar de la Mancha, de cuyo nombre no quiero acordarme, "
				+ "no ha mucho tiempo que vivía un hidalgo de los de lanza en astillero, "
				+ "adarga antigua, rocín flaco y galgo corredor. "
				+ "Una olla de algo más vaca que carnero, salpicón las más noches, "
				+ "duelos y quebrantos los sábados, lantejas los viernes, "
				+ "algún palomino de añadidura los domingos, consumían las "
				+ "tres partes de su hacienda. El resto della concluían sayo de velarte, "
				+ "calzas de velludo para las fiestas, con sus pantuflos de lo mesmo, "
				+ "y los días de entresemana se honraba con su vellorí de lo más fino.", 
				new ImageIcon(getClass().getResource("/resources/img/fp_ventas.png")).getImage(), 
				items);
	}
		
	private void select(int seleccion){
		pnlMenu[selected].state = Menu.INACTIVO;
		pnlMenu[selected].update(pnlMenu[selected].getGraphics());
		selected = seleccion;
		pnlMenu[seleccion].state = Menu.SELECCIONADO;
		pnlMenu[seleccion].update(pnlMenu[seleccion].getGraphics());
		contenido.removeAll();
		contenido.add(pnlContenido[seleccion], BorderLayout.CENTER);
		contenido.revalidate();
		contenido.repaint();
	}
	
	class Menu extends WebPanel{
		public static final int ACTIVO = 0,
							INACTIVO = 1,
							SELECCIONADO = 2;
		public int state;
		private WebLabel lblItem;
		private Image normal, white;
		private WebImage icono;
		
		public Menu(String label, String icon, int width, int height){
			normal = new ImageIcon(getClass().getResource("/resources/iconos/" + icon + ".png")).getImage();
			white = new ImageIcon(getClass().getResource("/resources/iconos/" + icon + "_white.png")).getImage();
			icono = new WebImage(normal);
			icono.setDisplayType(DisplayType.fitComponent);
			state = INACTIVO;
			lblItem = new WebLabel(label);
			lblItem.setForeground(new Color(68,68,68));
			lblItem.setFont(Utilidades.getRobotoFont(getClass().getResourceAsStream(Utilidades.roboto_font)));
			setSize(width, height);
			setMinimumSize(new Dimension(width, height));
			setPreferredSize(new Dimension(width, height));
			setMaximumSize(new Dimension(width, height));
			setLayout(new BorderLayout());
			add(icono, BorderLayout.WEST);
			add(lblItem, BorderLayout.CENTER);
			update(getGraphics());
		}
		
		@Override
		public void update(Graphics g){
			switch(state){
				case SELECCIONADO:
					setBackground(new Color(33,150,243));
					lblItem.setForeground(Color.white);
					icono.setImage(white);
					break;
				case INACTIVO:
					setBackground(Color.white);
					lblItem.setForeground(new Color(68,68,68));
					icono.setImage(normal);
					break;
				default:
					setBackground(new Color(235,235,235));
					lblItem.setForeground(new Color(68,68,68));
					icono.setImage(normal);
					break;
			}
		}
	}
	
	class PnlContenido extends WebPanel{
		public PnlContenido(String title, String description, Image img, ArrayList<ItemMenu> items){
			initComponents(title, description, img, items);
		}
		
		private void initComponents(String title, String description, Image img, ArrayList<ItemMenu> items){
			setBackground(new Color(246,246,246));
			setMargin(new Insets(0, 10, 0, 10));
			add(new WebPanel(){{
				setBackground(Color.white);
				setLayout(new BorderLayout());
				add(new WebPanel(){{
					setMargin(20, 0, 10, 0);
					setBackground(new Color(33,150,243));
					setLayout(new BorderLayout());
					add(new WebLabel(title, WebLabel.CENTER){{
						setFont(Utilidades.getRobotoFont(getClass().getResourceAsStream(Utilidades.roboto_font)));
						setFontSize(20);
						setForeground(Color.white);
					}}, BorderLayout.NORTH);
					add(new WebTextArea(){{
						setFont(Utilidades.getRobotoFont(getClass().getResourceAsStream(Utilidades.roboto_font)));
						setFontSize(14);
						setEditable(false);
						setAlignmentX(WebTextArea.CENTER_ALIGNMENT);
						setLineWrap(true);
						setWrapStyleWord(true);
						setBackground(new Color(33,150,243));
						setForeground(Color.white);
						setText(description);
					}}, BorderLayout.CENTER);
				}}, BorderLayout.NORTH);
				GroupPanel gp = new GroupPanel(){{
					setLayout(null);
					for(int i = 0; i < items.size(); i++){
						items.get(i).setBounds(210*(i%2), 120*(i/2), 210, 120);
						add(items.get(i));			
					}
					setPreferredSize(new Dimension(410, 120*((items.size() + 1)/2)));
				}};
				add(new WebScrollPane(gp){{
					this.setViewportView(gp);
					setBorder(null);
					setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_AS_NEEDED);
					setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
				}}, BorderLayout.CENTER);
			}});
		}
	}
	
	class ItemMenu extends WebPanel{
		public ItemMenu(Image img, String text){
			setMaximumSize(new Dimension(210, 120));
			setBackground(Color.white);
			initComponents(img, text);
		}
		
		private void initComponents(Image img, String text){
			setLayout(new BorderLayout());
			setMargin(10,0,10,0);
			add(new WebImage(img), BorderLayout.CENTER);
			add(new WebLabel(text, WebLabel.CENTER){{
				setFont(Utilidades.getRobotoFont(getClass().getResourceAsStream(Utilidades.roboto_font)));
				setFontSize(13);				
			}}, BorderLayout.SOUTH);
			addMouseListener(new MouseListener() {				
				@Override
				public void mouseReleased(MouseEvent e) {}
				
				@Override
				public void mousePressed(MouseEvent e) {}
				
				@Override
				public void mouseExited(MouseEvent e) {
					setBackground(Color.white);
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					setBackground(new Color(235, 235, 235));
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {}
			});
			this.revalidate();
			this.repaint();
		}
	}
}