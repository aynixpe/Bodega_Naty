/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import com.alee.extended.image.WebDecoratedImage;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;

/**
 *
 * @author Edder
 */
@SuppressWarnings("serial")
public class ItemPrincipal extends WebPanel implements MouseListener{
    private final WebDecoratedImage imgPrograma;
    private final WebLabel lblNombre;
    
    ItemPrincipal(ImageIcon img, String nombre){
        this.imgPrograma = new WebDecoratedImage(img.getImage().getScaledInstance(60, 60, Image.SCALE_DEFAULT));
        this.lblNombre = new WebLabel(nombre);
        initComponents();
    }
    
    private void initComponents(){
        setBackground(Color.white);
        this.setMaximumSize(new Dimension(1000, 70));
        setLayout(new BorderLayout());
        add(this.imgPrograma, BorderLayout.WEST);
        add(this.lblNombre, BorderLayout.CENTER);
        this.addMouseListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) { }

    @Override
    public void mouseReleased(MouseEvent e) { }

    @Override
    public void mouseEntered(MouseEvent e) {
        this.setBackground(new Color(79,192,232));
        this.lblNombre.setForeground(Color.white);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.setBackground(Color.white);
        this.lblNombre.setForeground(Color.black);
    }
}
