package vista;

import clases.Almacen;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.text.WebTextField;

import controlador.CtrlAlmacen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.ImageIcon;

import utilidades.Excepcion;
import utilidades.Utilidades;

/**
 *
 * @author Edder
 */
@SuppressWarnings("serial")
public class FrmAlmacen extends WebDialog implements ActionListener{
    private Almacen almacen;
    
    private WebTextField txtNombre;
    private WebButton btnEliminar, btnGuardar, btnCancelar;
    
    FrmAlmacen(WebFrame owner, boolean modal, Almacen almacen){
        super(owner, modal);
        setBackground(Color.white);
        setTitle("Almacén");
        setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
        setSize(300,100);
        setShowResizeCorner(false);
        setShowMaximizeButton(false);
        this.almacen = almacen;
        initComponents();
        setLocationRelativeTo(null);
    }

    /**
     * Inicializa los componentes del formulario
     */
    private void initComponents(){
        txtNombre = new WebTextField(20);
        btnGuardar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[1])));
        btnCancelar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[1])));
       
        btnEliminar = (almacen != null && almacen.getEstado() == 'A')  ? Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_ELIMINAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_ELIMINAR[1])))
        		: new WebButton("Activar");
        
        llenarDatos();
        		
        add(new WebPanel(){
            {
                setLayout(new BorderLayout());
                add(new GroupPanel(){
                    {
                        setLayout(new BorderLayout());
                        add(new WebLabel("Nombre"), BorderLayout.WEST);
                        add(txtNombre, BorderLayout.CENTER);
                    }
                }, BorderLayout.CENTER);
                add(new GroupPanel(){
                    {
                        setLayout(new FlowLayout(FlowLayout.RIGHT));
                        add(btnEliminar, btnGuardar, btnCancelar);
                    } 
                }, BorderLayout.SOUTH);
            }
        });
        
        this.btnEliminar.addActionListener(this);
        this.btnCancelar.addActionListener(this);
        this.btnGuardar.addActionListener(this);
        
        
        pack();
    }
    
    /**
     * obtiene la información de la almacenes y llena los campos del formulario
     */
    private void llenarDatos(){
        if(almacen != null){
            txtNombre.setText(almacen.getNombre());
            btnEliminar.setVisible(true);
        }else{
            btnEliminar.setVisible(false);
        }        
    }
    
    /**
     * Elimina una almacenes
     */
    private void eliminar(){
    	if(almacen.getEstado() == 'I')
    		activar();
    	else
	        if(WebOptionPane.showConfirmDialog(
	                this,
	                "¿Seguro que desea eliminar este almacén?",
	                "Advertencia",
	                WebOptionPane.YES_NO_OPTION,
	                WebOptionPane.WARNING_MESSAGE) == WebOptionPane.YES_OPTION)
	            try {
	                CtrlAlmacen.dar_baja(almacen, true, false);
	                WebOptionPane.showMessageDialog(
	                        this,
	                        "Almacén eliminado",
	                        "Éxito",
	                        WebOptionPane.INFORMATION_MESSAGE);
	                this.dispose();
	            } catch (Excepcion | SQLException ex) {
	                WebOptionPane.showMessageDialog(
	                        this,
	                        "No se pudo eliminar el almacén,"
	                                + " verificar e intentar nuevamente",
	                        "Error",
	                        WebOptionPane.ERROR_MESSAGE);
	            }
    }
    
    private void activar(){
    	try {
            CtrlAlmacen.dar_baja(almacen, false, false);
            WebOptionPane.showMessageDialog(
                    this,
                    "Almacén activado",
                    "Éxito",
                    WebOptionPane.INFORMATION_MESSAGE);
            this.dispose();
        } catch (SQLException | Excepcion ex) {
            WebOptionPane.showMessageDialog(
                    this,
                    "No se pudo activar el almacén,"
                            + " verificar e intentar nuevamente",
                    "Error",
                    WebOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Guarda la información, si está creando uno nuevo, inserta en la base de datos, sino actualiza
     */
    private void guardar(){
        if(this.almacen == null){
            this.almacen = new Almacen();
            this.almacen.setIdAlmacen(-1);
        }
        prepararDatos();
        if(this.almacen.getIdAlmacen() == -1){
            try {
                //Crear
                CtrlAlmacen.insert(this.almacen, false);
                WebOptionPane.showMessageDialog(
                        this,
                        "Información registrada correctamente",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            } catch (SQLException | Excepcion ex) {
                WebOptionPane.showMessageDialog(
                        this,
                        "No se pudo guardar la información, verificar e intentar nuevamente",
                        "Error",
                        WebOptionPane.ERROR_MESSAGE);
            }
        }else{
            try {
                CtrlAlmacen.update(this.almacen, false);
                WebOptionPane.showMessageDialog(
                        this,
                        "Información registrada correctamente",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            } catch (SQLException | Excepcion ex) {
                WebOptionPane.showMessageDialog(
                        this,
                        "No se pudo guardar la información, verificar e intentar nuevamente",
                        "Error",
                        WebOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    /**
     * obtiene la información de los campos del formulario y llena los atributos de la almacenes
     */
    private void prepararDatos(){
        this.almacen.setNombre(this.txtNombre.getText());
        this.almacen.setEstado('A');
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.btnCancelar){
            if(WebOptionPane.showConfirmDialog(
                    this,
                    "¿Está seguro que desea cerrar?",
                    "Advertencia",
                    WebOptionPane.YES_NO_OPTION,
                    WebOptionPane.WARNING_MESSAGE) == WebOptionPane.YES_OPTION)
                this.dispose();
        }else if(e.getSource() == this.btnEliminar){
            this.eliminar();
        }else if(e.getSource() == this.btnGuardar){
            this.guardar();
        }
    }
}
