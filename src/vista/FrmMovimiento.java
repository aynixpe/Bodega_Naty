/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import clases.Almacen;
import clases.Articulo;
import clases.DetalleMovimiento;
import clases.Movimiento;
import clases.TipoMovimiento;

import com.alee.extended.date.DateSelectionListener;
import com.alee.extended.date.WebDateField;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.menu.WebMenu;
import com.alee.laf.menu.WebMenuBar;
import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.spinner.WebSpinner;
import com.alee.laf.table.WebTable;
import com.alee.laf.text.WebTextField;

import controlador.CtrlAlmacen;
import controlador.CtrlArticulo;
import controlador.CtrlMovimiento;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;
import javax.swing.table.DefaultTableModel;

import utilidades.Excepcion;
import utilidades.SessionException;
import utilidades.Utilidades;

/**
 *
 * @author togapaulo
 * @version 0.2
 */
@SuppressWarnings("serial")
public class FrmMovimiento extends WebFrame{
    private Movimiento movimiento;
    private ArrayList<DetalleMovimiento> detallesMovimiento;
    private WebComboBox cboTipoMovimiento,
            cboOrigen,
            cboDestino;
    private WebDateField txtFecha;
    private WebTable tblArticulos;
    private WebTextField txtArticulo;
    private WebSpinner txtCantidad;
    private WebButton btnEliminar,
            btnAgregar,
            btnGuardar,
            btnEditar,
            btnCancelar;
    private WebLabel lblId;
    private WebMenuBar menuBar;
    
    private int almacenAnterior;
    private int tipoAnterior;
    private Articulo articulo;
    
    private FrmBusquedaArticulo frmBusquedaArticulo;
    
    FrmMovimiento(){
        setTitle("Movimientos");
        setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
        setSize(500,600);
        setShowResizeCorner(false);
        setShowMaximizeButton(false);
        setLocationRelativeTo(null);
        initComponents();
    }
    
    private void initComponents(){
    	articulo = null;       
        /**
         * Inicializando variables
         */
        txtFecha = new WebDateField(new Date());
        txtArticulo = new WebTextField();
        txtCantidad = new WebSpinner();
        cboTipoMovimiento = new WebComboBox();
        cboOrigen = new WebComboBox();
        cboDestino = new WebComboBox();
        btnAgregar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_AGREGAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_AGREGAR[1])));
        tblArticulos = new WebTable();
        btnGuardar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[1])));
        btnCancelar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[1])));
        btnEliminar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_ELIMINAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_ELIMINAR[1])));
        btnEditar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_EDITAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_EDITAR[1])));
        lblId = new WebLabel("Id: ");
        menuBar = new WebMenuBar();
        
        WebMenu wbArchivo = new WebMenu("Archivo");
        WebMenuItem wbi_archivo_salir = new WebMenuItem("Salir", KeyEvent.VK_ESCAPE);
        wbi_archivo_salir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
        wbArchivo.add(wbi_archivo_salir);
        menuBar.add(wbArchivo);
        this.setJMenuBar(menuBar);
        
        wbi_archivo_salir.addActionListener((ActionEvent e) -> {
        	System.out.println("Salir");
        });
        
        cargarTiposMovimiento();
        cargarAlmacenes();
        configTabla();        
        
        txtArticulo.setInputPrompt("Artículos");
        txtCantidad.setModel(new SpinnerNumberModel(1, 1,10000, 1));        
        btnEliminar.setVisible(false);
        
        add(new WebPanel(){{
        	setBackground(Color.white);
        	setLayout(new BorderLayout());
        	add(new GroupPanel(){{
        		setLayout(new BorderLayout());
        		add(new GroupPanel(){{
        			setLayout(new GridLayout(1,2));
	                add(new GroupPanel(){{
	                	setLayout(new SpringLayout());
	                	add(new WebLabel("Tipo"));
	                	add(cboTipoMovimiento);
	                	add(new WebLabel("Origen"));
	                	add(cboOrigen);
	                	SpringUtilities.makeCompactGrid(this,
	                			2, 2,
	                			6, 6,
	                			6, 6
	                			);
	                }});
	                add(new GroupPanel(){{
	                	setLayout(new SpringLayout());
	                	add(new WebLabel("Fecha"));
	                	add(txtFecha);
	                	add(new WebLabel("Destino"));
	                	add(cboDestino);
	                	SpringUtilities.makeCompactGrid(this,
	                			2, 2,
	                			6, 6,
	                			6, 6
	                			);
	                }});
        		}}, BorderLayout.CENTER);	                
                
                add(new GroupPanel(){{
                    setLayout(new GridBagLayout());
                    GridBagConstraints gbc = new GridBagConstraints();
                    
                    gbc.gridx = 0;
                    gbc.gridy = 0;
                    gbc.gridwidth = 1;
                    gbc.weightx = 0.95;
                    gbc.fill = GridBagConstraints.HORIZONTAL;
                    add(txtArticulo, gbc);
                    
                    gbc.gridx = 1;
                    gbc.weightx = 0.045;
                    add(txtCantidad, gbc);
                    
                    gbc.gridx = 2;
                    gbc.weightx = 0.005;
                    add(btnAgregar, gbc);
                }}, BorderLayout.SOUTH);
            }}, BorderLayout.NORTH);
        	
        	add(new WebScrollPane(tblArticulos), BorderLayout.CENTER);
        	
        	add(new GroupPanel(){{
        		setLayout(new BorderLayout());
        		add(lblId, BorderLayout.WEST);
        		add(new GroupPanel(){{
        			setLayout(new FlowLayout(FlowLayout.RIGHT));
                    add(btnEliminar);
                    add(btnEditar);
                    add(btnGuardar);
                    add(btnCancelar);
                }}, BorderLayout.EAST);
            }}, BorderLayout.SOUTH);
        }});
        
        txtFecha.addDateSelectionListener(new DateSelectionListener(){
        	@Override
			public void dateSelected(Date fecha) {
				if(Utilidades.compararFechas(fecha, new Date()) < 0){
					WebOptionPane.showMessageDialog(FrmMovimiento.this,
							"No se puede registrar un movimiento con una fecha futura",
							"Conflicto de fechas", WebOptionPane.WARNING_MESSAGE);
					txtFecha.setDate(new Date());
				}
			}
        });
        
        btnGuardar.addActionListener((ActionEvent e) -> {
            guardar();
        });
        
        btnEditar.addActionListener((ActionEvent e) -> {
        	editar();
        });
        
        txtArticulo.addActionListener((ActionEvent e) -> {
        	buscar();
        });
        
        btnAgregar.addActionListener((ActionEvent e) -> {
            addFila();
        });
        
        btnCancelar.addActionListener((ActionEvent e) -> {
            dispose();
        });
        
        cboTipoMovimiento.addActionListener((ActionEvent e) -> {
        	comprobarAlmacenes();
            inhabilitarCargoDescargo(cboTipoMovimiento.getSelectedItem().toString());
            TipoMovimiento tipo = (TipoMovimiento)cboTipoMovimiento.getSelectedItem();
            if(tipo.getTipo() == 'T' || tipo.getTipo() == 'D')
            {
            	ArrayList<Integer> conflictos = comprobarCantidades();
            	if(conflictos.size() != 0)
            	{
            		String mensaje = "Los siguientes artículos tienen un stock menor a la cantidad a descargar: \n";
            		for(Integer row : conflictos)
            		{
            			mensaje += (tblArticulos.getModel().getValueAt(row, 1) + "\n");
            		}
            		mensaje += ("¿Desea eliminar los artículos de la lista y continuar?");
            		
            		if(WebOptionPane.showConfirmDialog(this, mensaje) != WebOptionPane.YES_OPTION)
            		{
            			cboTipoMovimiento.setSelectedIndex(tipoAnterior);
            		}
            		else
            		{
            			for(Integer row : conflictos)
            			{
            				DefaultTableModel modelo = (DefaultTableModel) tblArticulos.getModel();
                            modelo.removeRow(row);
                            tblArticulos.setModel(modelo);
            			}
            		}
            		
            	}
            }
    		tipoAnterior = cboTipoMovimiento.getSelectedIndex();
        });
        
        cboOrigen.addActionListener((ActionEvent e) -> {        	
        	String mensaje = "¿Está seguro que desea cambiar el almacén de origen? Todos los artículos serán borrados de la lista.";
        	if(	tblArticulos.getRowCount() > 0 && 
        			WebOptionPane.showConfirmDialog(FrmMovimiento.this, mensaje) != WebOptionPane.YES_OPTION){
        		cboOrigen.setSelectedIndex(almacenAnterior);
        	}else{
        		configTabla();
        		almacenAnterior = cboOrigen.getSelectedIndex();
        	}
        	comprobarAlmacenes();
        });
        
        cboDestino.addActionListener((ActionEvent e) -> {
        	comprobarAlmacenes();
        });
        
        tblArticulos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e){
                if (e.getClickCount()==1) {
                    btnEliminar.setVisible(true);
                }
            }
        });
        
        btnEliminar.addActionListener((ActionEvent e) -> {
            int row = tblArticulos.getSelectedRow();
            int opcion = WebOptionPane.showConfirmDialog(
                    this, 
                    "¿Desea eliminar este producto?",
                    "Advertencia",
                    WebOptionPane.WARNING_MESSAGE);
            if (opcion == 0) {
                DefaultTableModel modelo = (DefaultTableModel) tblArticulos.getModel();
                modelo.removeRow(row);
                tblArticulos.setModel(modelo);
            }
            btnEliminar.setVisible(false);
        });
        
        addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent ev){
                FrmPrincipal.bolFrmMovimiento = false;
            }
        });
    }
    
    private ArrayList<Integer> comprobarCantidades()
    {
    	ArrayList<Integer> conflictos = new ArrayList<>();
    	for(int i = tblArticulos.getRowCount() - 1; i >= 0; i--)
    	{
    		try
    		{
    			int idArticulo = (int)tblArticulos.getModel().getValueAt(i,0);
    			int idAlmacen = ((Almacen)cboOrigen.getSelectedItem()).getIdAlmacen();
    			int cantidad = (int)tblArticulos.getModel().getValueAt(i,2);
				int stock = CtrlArticulo.getArticuloStock(idArticulo, idAlmacen, false);
				if(cantidad > stock)
					conflictos.add(i);
					
			}
    		catch (Excepcion | SQLException e)
    		{
				e.printStackTrace();
			}
    		
    	}
    	return conflictos;
    }
    
    private void cargarTiposMovimiento(){
    	cboTipoMovimiento.removeAllItems();
    	try{
    		ArrayList<TipoMovimiento> tiposMovimiento = CtrlMovimiento.getTipoMovimientos("", false);
	        cboTipoMovimiento = new WebComboBox(tiposMovimiento.toArray());
	        cboTipoMovimiento.setSelectedIndex(0);
	        tipoAnterior = 0;
    	}catch(Exception e){
    		WebOptionPane.showMessageDialog(this, 
    				"Error al cargar tipos de movimiento: " +  e.getMessage(),
    				"Error interno", WebOptionPane.ERROR_MESSAGE);
    	}
    }
    
    private void cargarAlmacenes(){
    	cboOrigen.removeAllItems();
    	cboDestino.removeAllItems();
    	try{
    		ArrayList<Almacen> almacenes = CtrlAlmacen.getAlmacenes("", false);
            cboOrigen = new WebComboBox(almacenes.toArray());
            cboOrigen.setSelectedIndex(0);
            almacenAnterior = 0;
            cboDestino = new WebComboBox(almacenes.toArray());
            cboDestino.setSelectedIndex(1);
    	}catch(Exception e){
    		WebOptionPane.showMessageDialog(this, 
    				"Error al cargar los almacences: " +  e.getMessage(),
    				"Error interno", WebOptionPane.ERROR_MESSAGE);
    	}
        
    }
        
    private void guardar(){
        if(prepararDatos()){
            try { 
                CtrlMovimiento.insert(movimiento, false, detallesMovimiento);
                lblId.setText("ID: " + movimiento.getIdMovimiento());
                WebOptionPane.showMessageDialog(this,
                        "Información registrada correctamente",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
                limpiarFormulario();
            } catch (SessionException e) {
				WebOptionPane.showMessageDialog(this, e.getMessage(), "Acceso denegado", WebOptionPane.WARNING_MESSAGE);
            }catch(Exception e){
            	Logger.getLogger(FrmMovimiento.class.getName()).log(Level.SEVERE, null, e);
			}
        }
    }
    
    private boolean prepararDatos(){
        if(comprobarAlmacenes())
        	return false;
        if(tblArticulos.getRowCount() <= 0){
        	WebOptionPane.showMessageDialog(this, 
        			"No se ha elegido artículos", 
        			"Advertencia", WebOptionPane.WARNING_MESSAGE);
        	return false;
        }
        
        movimiento = new Movimiento(){{
        	setIdMovimiento(-1);
        	setEstado('G');
        }};
        
        detallesMovimiento = new ArrayList<>();
        LocalDate fecha = (txtFecha.getDate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        
        movimiento.setTipoMovimiento((TipoMovimiento)cboTipoMovimiento.getSelectedItem());
        movimiento.setFecha(fecha);
        
        movimiento.setAlmacenOrigen(
        	(cboOrigen.isEnabled()) ? (Almacen) cboOrigen.getSelectedItem() : null
        );
        movimiento.setAlmacenDestino(
        	(cboDestino.isEnabled()) ? (Almacen) cboDestino.getSelectedItem() : null
        );
        
        for (int i = 0; i < tblArticulos.getRowCount(); i++) {
        	int idArticulo = (int)tblArticulos.getModel().getValueAt(i, 0);
            detallesMovimiento.add(
        		new DetalleMovimiento(
    				-1,
                    movimiento,
                    new Articulo(){{
                    	setIdArticulo(idArticulo);
                    }},
                    (int)tblArticulos.getModel().getValueAt(i, 2)
                )
            );
        }
        return true;
    }

    private void configTabla(){
        DefaultTableModel model = new DefaultTableModel();
        tblArticulos.setModel(model);
        model.addColumn("idArticulo");
        model.addColumn("Nombre");
        model.addColumn("Cantidad");
        tblArticulos.removeColumn(this.tblArticulos.getColumnModel().getColumn(0));
        Utilidades.packColumns(tblArticulos, 0);
        tblArticulos.setEditable(false);
        tblArticulos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    private void addFila(){
    	if(articulo == null)
    		WebOptionPane.showMessageDialog(this,
    				"No ha seleccionado un artículo",
    				"Atención", WebOptionPane.WARNING_MESSAGE);
    	else{
    		if(buscarIdArticulo(articulo.getIdArticulo()) >= 0)
    			WebOptionPane.showMessageDialog(this,
        				"El artículo seleccionado ya ha sido agregado",
        				"Atención", WebOptionPane.WARNING_MESSAGE);
    		else{
    			((DefaultTableModel)tblArticulos.getModel())
                    .addRow(new Object[]{
                        articulo.getIdArticulo(),
                        articulo.toString(),
                        txtCantidad.getValue()
                    });
                Utilidades.packColumns(tblArticulos, 0);
    		}
    	}
    	articulo = null;
    	txtCantidad.setValue(1);
    	txtArticulo.setText("");
    	txtArticulo.requestFocus();
    }
    
    private void editar()
    {
    	int row = tblArticulos.getSelectedRow();
    	if(row >= 0)
    	{
    		int idArticulo = (int)tblArticulos.getModel().getValueAt(row, 0);
    		try {
				int stock = CtrlArticulo.getArticuloStock(idArticulo, utilidades.Configuracion.getIdAlmacen(), false);
				int cantidad = Integer.parseInt(WebOptionPane.showInputDialog(this, "Cantidad"));
				if(cboOrigen.isEnabled() && cantidad > stock)
					WebOptionPane.showMessageDialog(this, "Actualmente hay " + stock + " artículos en stock. No hay suficiente para descargar");
				else
					tblArticulos.getModel().setValueAt(cantidad, row, 2);
			} catch (Excepcion | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch(NumberFormatException e){
				WebOptionPane.showMessageDialog(this, "Ingrese la cantidad correctamente");
			}
    	}
    }
    
    private int buscarIdArticulo(int idArticulo){
    	for(int i = 0; i < tblArticulos.getRowCount(); i++){
    		if( (int) tblArticulos.getModel().getValueAt(i, 0) == idArticulo)
    			return i;
    	}
    	return -1;
    }
    
    public void limpiarFormulario(){
        cboTipoMovimiento.setSelectedIndex(0);
        cboOrigen.setSelectedIndex(0);
        cboDestino.setSelectedIndex(1);
        txtArticulo.setText("");
        txtCantidad.setValue(1);
        configTabla();
    }
    
    public void inhabilitarCargoDescargo(String item){
    	cboOrigen.setEnabled(! (item.charAt(0) == 'C'));
    	cboDestino.setEnabled(! (item.charAt(0) == 'D'));
    }
    
    private void buscar(){
    	Almacen almacen = cboOrigen.isEnabled() ? (Almacen) cboOrigen.getSelectedItem() : (Almacen) cboDestino.getSelectedItem(); 
    	frmBusquedaArticulo = new FrmBusquedaArticulo(this, true,  txtArticulo.getText(), almacen, true, cboOrigen.isEnabled());
        frmBusquedaArticulo.setVisible(true);
        articulo = frmBusquedaArticulo.getArticulo();
        int stock = frmBusquedaArticulo.getStock();
        if(articulo != null && stock > 0){
        	txtArticulo.setText(articulo.getNombre());
        	if(cboOrigen.isEnabled())
	        	txtCantidad.setModel(
	        		new SpinnerNumberModel(1, 1, stock, 1) 
	        	);
        	else
        		txtCantidad.setModel(
	        		new SpinnerNumberModel(1, 1, 100000, 1) 
	        	);
        	txtCantidad.getEditor().getComponent(0).requestFocus();
        }       
    }
    
    private boolean comprobarAlmacenes(){
		char tipo = cboTipoMovimiento.getSelectedItem().toString().charAt(0);
    	if (cboDestino.getSelectedIndex() == cboOrigen.getSelectedIndex() && tipo == 'T') {
            WebOptionPane.showMessageDialog(FrmMovimiento.this,
            		"No se puede hacer transferencia al mismo almacén", 
            		"Advertencia", WebOptionPane.WARNING_MESSAGE);
            return true;
        }
    	return false;
    }
}