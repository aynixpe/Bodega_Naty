package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

import clases.Proveedor;

import com.alee.extended.date.WebDateField;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.splitpane.WebSplitPane;
import com.alee.laf.table.WebTable;

import controlador.CtrlOrdenCompra;
import controlador.CtrlProveedor;
import utilidades.SessionException;
import utilidades.Utilidades;

@SuppressWarnings("serial")
public class FrmConsultaOrdenCompra extends WebFrame {
	private WebDateField txtFechaInicio, txtFechaFin;
	private WebComboBox cboProveedor;
	private WebCheckBox chbAnulados;
	private WebTable tblOrdenCompra,
					 tblDetalles;
	private WebButton btnCerrar,
					  btnAnular,
					  btnBuscar;

	public FrmConsultaOrdenCompra(){
		setTitle("Registro de Preventas");
		setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
		setSize(900, 600);
		setLocationRelativeTo(null);
		initComponents();
	}

	private void initComponents(){
		txtFechaInicio = new WebDateField(new Date());
		txtFechaFin = new WebDateField(new Date());
		cboProveedor = new WebComboBox();
		chbAnulados = new WebCheckBox("Anulados");
		tblOrdenCompra = new WebTable();
		tblDetalles = new WebTable();
		btnAnular = new WebButton("Anular");
		btnCerrar = new WebButton("Cerrar");
		btnBuscar = Utilidades.getIconButton(
				new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[0])),
				new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[1])));

		llenarProveedores();
		configTablaOrdenCompra();
		configTablaDetalles();

		add(new WebPanel(){{
			setBackground(Color.white);
			setLayout(new BorderLayout());
			add(new GroupPanel(){{
				setLayout(new GridBagLayout());
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = 0;
				gbc.gridheight = 1;
				gbc.weightx = 0.2;
				gbc.fill = GridBagConstraints.HORIZONTAL;

				add(new GroupPanel(){{
					add(new WebLabel("Inicio"));
					add(txtFechaInicio);
				}}, gbc);

				gbc.gridx = 1;
				add(new GroupPanel(){{
					add(new WebLabel("Fin"));
					add(txtFechaFin);
				}}, gbc);

				gbc.gridx = 2;
				gbc.weightx = 0.3;
				add(new GroupPanel(){{
					add(new WebLabel("Proveedor"));
					add(cboProveedor);
				}}, gbc);

				gbc.gridx = 3;
				gbc.weightx = 0.2;
				add(chbAnulados, gbc);

				gbc.gridx = 4;
				gbc.weightx = 0.1;
				add(new GroupPanel(){{
					add(btnBuscar);
				}}, gbc);
			}}, BorderLayout.NORTH);
			add(new WebSplitPane(
					WebSplitPane.HORIZONTAL_SPLIT,
					new WebScrollPane(tblOrdenCompra),
					new WebScrollPane(tblDetalles)
					){
				{
					setDividerLocation(500);
				}
			}, BorderLayout.CENTER);
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				add(btnAnular, BorderLayout.WEST);
				add(btnCerrar, BorderLayout.EAST);
			}}, BorderLayout.SOUTH);
		}});
		
		ListSelectionModel tblSelect = tblOrdenCompra.getSelectionModel();
		tblSelect.addListSelectionListener((ListSelectionEvent e) ->{
			configTablaDetalles();	
		});

		chbAnulados.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				configTablaOrdenCompra();
			}
		});

		btnBuscar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				configTablaOrdenCompra();
			}
		});

		btnAnular.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				anular();
			}
		});

		btnCerrar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				cerrar();
			}
		});

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent we){
				FrmPrincipal.bolFrmConsultaOrdenCompra = false;
			}
		});
	}

	private void cerrar(){
		dispose();
	}

	private void llenarProveedores(){
		try {
			ArrayList<Proveedor> proveedores = CtrlProveedor.getProveedores_busqueda("", false);
			proveedores.add(0, new Proveedor(){{
				setIdProveedor(-1);
				setNombre("Todos");
			}});
			cboProveedor = new WebComboBox(proveedores.toArray());
		}catch(Exception e){
			WebOptionPane.showMessageDialog(this,
					"Error al cargar los proveedores", "Error interno",
					WebOptionPane.ERROR_MESSAGE);
		}
	}

	private void configTablaOrdenCompra(){
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Proveedor");
		model.addColumn("Monto");
		model.addColumn("Fecha");
		model.addColumn("Entrega");
		model.addColumn("Usuario");
		model.addColumn("Estado");
		tblOrdenCompra.setModel(model);

		LocalDate fechaInicio = (txtFechaInicio.getDate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate fechaFin = (txtFechaFin.getDate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		Proveedor proveedor = cboProveedor.getSelectedIndex() == 0 ? null : (Proveedor) cboProveedor.getSelectedItem();
		try{
			ArrayList<Object[]> ordenes = CtrlOrdenCompra.getConsulta(false, fechaInicio.toString(), fechaFin.toString(),
					proveedor, chbAnulados.isSelected(), false);

			for (Object[] orden : ordenes) {
				orden[2] = utilidades.Utilidades.setUpMoneda((float)orden[2]);
				model.addRow(orden);
			}
		}catch(SessionException e){
			WebOptionPane.showMessageDialog(this, "No tiene permisos para acceder a este recurso", "Acceso Denegado",
				WebOptionPane.WARNING_MESSAGE);
		}catch(Exception e){
			WebOptionPane.showMessageDialog(this, "Error al guardar la información, intente nuevamente", "Error interno",
					WebOptionPane.ERROR_MESSAGE);
		}
		Utilidades.packColumns(tblOrdenCompra, 0);
		tblOrdenCompra.setEditable(false);
		tblOrdenCompra.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	private void configTablaDetalles(){
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Articulo");
		model.addColumn("Cantidad");
		model.addColumn("Precio");
		tblDetalles.setModel(model);
		
		int row = tblOrdenCompra.getSelectedRow(); 
		if(row >= 0){
			int idOrdenCompra = (int)tblOrdenCompra.getValueAt(tblOrdenCompra.getSelectedRow(), 0);
			if (idOrdenCompra != 0) {
				try{
					ArrayList<Object[]> detallesOC = CtrlOrdenCompra.getConsulta_Detalle(idOrdenCompra, false);
					for (Object[] detalle : detallesOC) {
						detalle[3] = utilidades.Utilidades.setUpMoneda((float)detalle[3]);
						model.addRow(detalle);
					}
				}catch(SessionException e){
					WebOptionPane.showMessageDialog(this, "No tiene permisos para acceder a este recurso", "Acceso Denegado",
						WebOptionPane.WARNING_MESSAGE);
				}catch(Exception e){
					WebOptionPane.showMessageDialog(this, "Error al cargar los detalles, intente nuevamente", "Error interno",
							WebOptionPane.ERROR_MESSAGE);
				}
			}
			tblDetalles.removeColumn(tblDetalles.getColumnModel().getColumn(0));
			Utilidades.packColumns(tblDetalles, 0);
			tblDetalles.setEditable(false);
		}			
	}

	private void anular(){
		int idOrdenCompra = (int)tblOrdenCompra.getValueAt(tblOrdenCompra.getSelectedRow(), 0);
		try {
			CtrlOrdenCompra.anularOrdenCompra(idOrdenCompra, false);
			WebOptionPane.showMessageDialog(this, "Anulado correctamente", "Éxito", WebOptionPane.INFORMATION_MESSAGE);
		} catch (SessionException e) {
			WebOptionPane.showMessageDialog(this, "No tiene los permisos suficientes para ejecutar esta acción",
					"Acceso denegado", WebOptionPane.WARNING_MESSAGE);
		}catch(Exception e){
			WebOptionPane.showMessageDialog(this, "Error al anular, intente nuevamente",
					"Error", WebOptionPane.ERROR_MESSAGE);
		}
		configTablaOrdenCompra();
	}
}
