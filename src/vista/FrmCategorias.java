/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import clases.Categoria;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;

import controlador.CtrlCategoria;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import utilidades.Utilidades;

/**
 *
 * @author Edder
 */
@SuppressWarnings("serial")
public class FrmCategorias extends WebFrame implements ActionListener{
    private WebButton btnAgregar;
    private WebPanel lstCategorias;

    public FrmCategorias(){
        setTitle("Categorías");
        setMinimumSize(new Dimension(300, 400));
        setDefaultCloseOperation(WebDialog.DISPOSE_ON_CLOSE);
        setShowResizeCorner(false);
        setShowMaximizeButton(false);
        initComponents();
        llenarCategorias();
        this.setLocationRelativeTo(null);
    }

    /**
     * Obtiene las categorías de la base de datos y llena la lista para mostrar
     */
    public final void llenarCategorias(){
        try {
            ArrayList<Categoria> categorias = CtrlCategoria.getCategorias("", false);
            lstCategorias.removeAll();
            categorias.stream().forEach((categoria) -> {
                lstCategorias.add(new ItmCategoria(this, categoria));
            });
            lstCategorias.updateUI();
        }catch(Exception ex){
            Logger.getLogger(FrmCategorias.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Inicializa los componentes del formulario
     */
    private void initComponents(){
        this.btnAgregar = Utilidades.getIconButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_IAGREGAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_IAGREGAR[1])));
        this.lstCategorias = new WebPanel();
        this.lstCategorias.setLayout(new BoxLayout(this.lstCategorias, BoxLayout.PAGE_AXIS));

        this.add(new WebPanel(){
            {
                setBackground(Color.white);
                setLayout(new BorderLayout());
                add(new GroupPanel(){
                    {
                        this.setLayout(new BorderLayout());
                        this.add(new WebLabel("Categorias"), BorderLayout.WEST);
                        this.add(new WebPanel(){
                            {
                                setBackground(Color.white);
                                this.setLayout(new FlowLayout(FlowLayout.RIGHT));
                                this.add(btnAgregar);
                            }
                        });
                    }
                }, BorderLayout.NORTH);
                add(new GroupPanel(){
                    {
                        setLayout(new BorderLayout());
                        add(new WebScrollPane(lstCategorias), BorderLayout.CENTER);
                    }
                }, BorderLayout.CENTER);
            }
        });
        this.btnAgregar.addActionListener(this);
        this.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent ev){
                FrmPrincipal.bolFrmCategorias = false;
            }
        });
        this.pack();
    }

    /**
     * Abre el formulario para agregar categoría
     * luego actualiza la lista
     */
    private void agregarCategoria(){
        FrmCategoria fc = new FrmCategoria(null, true, null);
        fc.setVisible(true);
        llenarCategorias();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.btnAgregar){
            agregarCategoria();
        }
    }
}
