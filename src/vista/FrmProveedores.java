/**
 * 
 */
package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.splitpane.WebSplitPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.text.WebTextField;

import clases.Articulo;
import clases.Proveedor;
import controlador.CtrlArticulo;
import controlador.CtrlProveedor;
import utilidades.Excepcion;
import utilidades.Utilidades;

/**
 * @author edder
 *
 */
@SuppressWarnings("serial")
public class FrmProveedores extends WebFrame {
	private WebTextField txtBusqueda;
	private WebTable tblProveedores;
	private WebButton btnBuscar, btnAgregar, btnEditar, btnSalir;
	
	FrmProveedores(){
		setTitle("Proveedores");
		setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
		setSize(900, 600);
		setShowResizeCorner(false);
		initComponents();
		setLocationRelativeTo(null);
	}
	
	private void initComponents(){
		txtBusqueda = new WebTextField ();
        txtBusqueda.setInputPrompt ( "Búsqueda" );
        txtBusqueda.setInputPromptFont ( txtBusqueda.getFont ().deriveFont ( Font.ITALIC ) );
        
        tblProveedores = new WebTable();
        
        btnBuscar = Utilidades.getIconButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[1])));
        btnAgregar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_AGREGAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_AGREGAR[1]))
        		);
		btnEditar = Utilidades.getIconButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_EDITAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_EDITAR[1])));		
		btnSalir = Utilidades.getIconButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[1])));
		
		add(new WebPanel(){{
        	setBackground(Color.white);
        	setLayout(new BorderLayout());
        	
        	add(new GroupPanel(){{
        		setLayout(new BorderLayout());
        		add(new WebLabel("Búsqueda"), BorderLayout.WEST);
        		add(txtBusqueda, BorderLayout.CENTER);
        		add(new GroupPanel(){{
        			add(btnBuscar);
        		}}, BorderLayout.EAST);
        	}}, BorderLayout.NORTH);
        	
        	add(new WebScrollPane(tblProveedores), BorderLayout.CENTER);
        	/*add(new WebSplitPane(
        			WebSplitPane.HORIZONTAL_SPLIT,
        			new WebScrollPane(tblProveedores),
        			new WebScrollPane(tblStocks)
        	), BorderLayout.CENTER);
        	*/
        	
        	add(new GroupPanel(){{
        		setLayout(new FlowLayout(FlowLayout.RIGHT));
        		add(btnAgregar, btnEditar, btnSalir);
        	}}, BorderLayout.SOUTH);
        }});
		buscar();
		txtBusqueda.addActionListener((ActionEvent e) -> {
			buscar();
		});
		btnAgregar.addActionListener((ActionEvent e) -> {
			FrmProveedor frmProveedor = new FrmProveedor(this, true, null);
			frmProveedor.setVisible(true);
		});
		btnEditar.addActionListener((ActionEvent e)->{
			int row = tblProveedores.getSelectedRow() == -1 ? 0 : tblProveedores.getSelectedRow();
        	try{
	            Proveedor proveedor = CtrlProveedor.get((int) tblProveedores.getModel().getValueAt(row, 0), false);
	            FrmProveedor fp = new FrmProveedor(
	                    this, 
	                    false, 
	                    proveedor);
	            fp.setVisible(true);
        	}catch(Exception ex){
        		WebOptionPane.showMessageDialog(this, "Error al cargar el proveedor, intente nuevamente", "Error interno",
        				WebOptionPane.ERROR_MESSAGE);
        	}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent we){
				FrmPrincipal.bolFrmProveedores = false;
			}
		});
	}
	
	public void buscar(){
		try {
			ArrayList<Proveedor> proveedores = CtrlProveedor.getProveedores_busqueda(
					txtBusqueda.getText(), false);
			DefaultTableModel model = new DefaultTableModel();
			this.tblProveedores.setModel(model);
			model.addColumn("ID");
			model.addColumn("Nombre");
			model.addColumn("Descripcion");
			model.addColumn("Preventa");
			model.addColumn("Mercaderista");
			model.addColumn("Direccion");
			model.addColumn("Teléfonos");
			for(Proveedor proveedor : proveedores){
				model.addRow(new Object[]{
						proveedor.getIdProveedor(),
						proveedor.getNombre(),
						proveedor.getDescripcion(),
						proveedor.getPreventa(),
						proveedor.getMercaderista(),
						proveedor.getDireccion(),
						proveedor.getTelefono()
				});
			}
			Utilidades.packColumns(tblProveedores, 0);
			tblProveedores.setEditable(false);
			tblProveedores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		} catch (Excepcion | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
