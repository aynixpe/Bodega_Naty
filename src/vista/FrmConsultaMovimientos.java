package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

import clases.Almacen;
import clases.TipoMovimiento;

import com.alee.extended.date.DateSelectionListener;
import com.alee.extended.date.WebDateField;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.splitpane.WebSplitPane;
import com.alee.laf.table.WebTable;

import controlador.CtrlAlmacen;
import controlador.CtrlMovimiento;
import utilidades.Excepcion;
import utilidades.SessionException;
import utilidades.Utilidades;

@SuppressWarnings("serial")
public class FrmConsultaMovimientos extends WebFrame {
	private WebComboBox cboAlmacen, cboTipoMovimiento;
	private WebDateField txtFechaInicio, txtFechaFin;
	private WebTable tblMovimientos,
					 tblDetalles;
	private WebCheckBox chbAnulados;
	private WebButton btnBuscar, btnAnular;
	
	public FrmConsultaMovimientos() {
		setTitle("Consultar Movimientos");
		setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
		setShowResizeCorner(false);
		setSize(850, 500);
		initComponents();
		setLocationRelativeTo(null);
	}
	
	private void initComponents(){
		/**
		 * Inicializando variables
		 */
		txtFechaInicio = new WebDateField(new Date());
		txtFechaFin = new WebDateField(new Date());
		cboAlmacen = new WebComboBox();
		cboTipoMovimiento = new WebComboBox();
		tblMovimientos = new WebTable();
		tblDetalles = new WebTable();
		chbAnulados = new WebCheckBox("Anulados");
		btnBuscar = Utilidades.getIconButton(
				new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[0])),
				new ImageIcon(getClass().getResource(Utilidades.N_IBUSCAR[1])));
		btnAnular = new WebButton("Anular");
		
		cargarAlmacenes();
		cargarTiposMovimiento();
		configTable_Movimiento();
		configTable_Detalles();
		
		add(new WebPanel(){{
			setLayout(new BorderLayout());
			setBackground(Color.white);
			add(new GroupPanel(){{
				setLayout(new GridBagLayout());
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.gridwidth = 1;
                gbc.weightx = 0.2;
                gbc.fill = GridBagConstraints.HORIZONTAL;
				add(new GroupPanel(){{
					add(new WebLabel("Almacen"));
					add(cboAlmacen);
				}}, gbc);
				
				gbc.gridx = 1;
                gbc.weightx = 0.25;
				add(new GroupPanel(){{
					add(new WebLabel("Tipo"));
					add(cboTipoMovimiento);
				}}, gbc);
				
				gbc.gridx = 2;
                gbc.weightx = 0.1;
				add(chbAnulados, gbc);
				
				gbc.gridx = 3;
                gbc.weightx = 0.2;
				add(new GroupPanel(){{
					add(new WebLabel("Inicio"));
					add(txtFechaInicio);
				}}, gbc);
				
				gbc.gridx = 4;
                gbc.weightx = 0.25;
				add(new GroupPanel(){{
					setLayout(new BorderLayout());
					add(new WebLabel("Fin"), BorderLayout.WEST);
					add(new GroupPanel(){{
						add(txtFechaFin);
					}}, BorderLayout.CENTER);
					add(btnBuscar, BorderLayout.EAST);
				}}, gbc);
			}}, BorderLayout.NORTH);

			add(new WebSplitPane( 
					WebSplitPane.HORIZONTAL_SPLIT, 
					new WebScrollPane(tblMovimientos),
					new WebScrollPane(tblDetalles)
					){
				{
					this.setDividerLocation(500);
				}
			},BorderLayout.CENTER);
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				add(btnAnular, BorderLayout.WEST);
			}}, BorderLayout.SOUTH);
		}
		});
		
		btnBuscar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				configTable_Movimiento();
			}
			
		});
		
		btnAnular.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				anular();
			}
		});
		
		addWindowListener(new WindowAdapter() {
			 @Override
			 public void windowClosed(WindowEvent we){
				 FrmPrincipal.bolFrmConsultaMovimientos = false;
			 }
		});
		
		ListSelectionModel tblSelect = tblMovimientos.getSelectionModel();
		tblSelect.addListSelectionListener((ListSelectionEvent e) ->{
			configTable_Detalles();
		});
		
		txtFechaInicio.addDateSelectionListener(new DateSelectionListener(){
			@Override
			public void dateSelected(Date fechaInicio) {
				if(Utilidades.compararFechas(fechaInicio, txtFechaFin.getDate()) == -1){
					WebOptionPane.showMessageDialog(FrmConsultaMovimientos.this,
							"La fecha inicial no puede ser mayor a la fecha final.", 
							"Conflicto de fechas", WebOptionPane.WARNING_MESSAGE);
					txtFechaInicio.setDate(txtFechaFin.getDate());
				}
			}			
		});
		
		txtFechaFin.addDateSelectionListener(new DateSelectionListener(){
			@Override
			public void dateSelected(Date fechaFin) {
				if(Utilidades.compararFechas(txtFechaInicio.getDate(), fechaFin) == -1){
					WebOptionPane.showMessageDialog(FrmConsultaMovimientos.this,
							"La fecha final no puede ser menor a la fecha inicial.", 
							"Conflicto de fechas", WebOptionPane.WARNING_MESSAGE);
					txtFechaFin.setDate(txtFechaInicio.getDate());
				}
			}			
		});
	}
	
	@SuppressWarnings("unchecked")
	private void cargarAlmacenes(){
		cboAlmacen.removeAllItems();
		Almacen objTodos = new Almacen(-1, "Todos", 'A');
		cboAlmacen.addItem(objTodos);
		try{
			ArrayList<Almacen> almacenes = CtrlAlmacen.getAlmacenes("", false);
			for(Almacen almacen : almacenes){
				cboAlmacen.addItem(almacen);
			}
		}catch(Exception e){
			WebOptionPane.showMessageDialog(this, "Error al cargar almacenes: " + e.getMessage(), 
					"Error interno", WebOptionPane.ERROR_MESSAGE);
		}
		cboAlmacen.setSelectedIndex(0);
	}
	private void cargarTiposMovimiento(){
		cboTipoMovimiento.removeAllItems();
		TipoMovimiento objTodos = new TipoMovimiento(-1, ' ',  "Todos");
    	try{
    		ArrayList<TipoMovimiento> tiposMovimiento = CtrlMovimiento.getTipoMovimientos("", false);
    		tiposMovimiento.add(0, objTodos);
	        cboTipoMovimiento = new WebComboBox(tiposMovimiento.toArray());
    	}catch(Exception e){
    		WebOptionPane.showMessageDialog(this, 
    				"Error al cargar tipos de movimiento: " +  e.getMessage(),
    				"Error interno", WebOptionPane.ERROR_MESSAGE);
    	}
        cboTipoMovimiento.setSelectedIndex(0);
    }
	
	private void configTable_Detalles(){
		DefaultTableModel model = new DefaultTableModel();
		tblDetalles.setModel(model);
		model.addColumn("idDetalleMovimiento");
		model.addColumn("Artículo");
		model.addColumn("#");
		int row = tblMovimientos.getSelectedRow();
		if (row >= 0) {
			int idMovimiento = (int)tblMovimientos.getValueAt(row, 0);
			try{
				ArrayList<Object[]> detalles = CtrlMovimiento.getConsultaDetalles(idMovimiento, false);
				for (Object[] detalle : detalles) {
					model.addRow(detalle);
				}
			}catch(SessionException se){
				WebOptionPane.showMessageDialog(this, se.getMessage(),
						"Acceso denegado", WebOptionPane.ERROR_MESSAGE);
			}catch(Exception e){
				WebOptionPane.showMessageDialog(this, "Error al cargar detalles: " + e.getMessage(),
						"Error interno", WebOptionPane.ERROR_MESSAGE);
			}
			
		}
		tblDetalles.removeColumn(tblDetalles.getColumnModel().getColumn(0));
		Utilidades.packColumns(tblDetalles, 0);
		tblDetalles.setEditable(false);
	}
	
	private void configTable_Movimiento(){
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Tipo");
		model.addColumn("Origen");
		model.addColumn("Destino");
		model.addColumn("Fecha");
		
		Almacen objAlmacen = cboAlmacen.getSelectedIndex() == 0 ? null : (Almacen)cboAlmacen.getSelectedItem();
		TipoMovimiento objTipoMovimiento = cboTipoMovimiento.getSelectedIndex() == 0 ? null : (TipoMovimiento)cboTipoMovimiento.getSelectedItem();
		String fechaInicio = (txtFechaInicio.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).toString();
		String fechaFin = (txtFechaFin.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).toString();
		
		try{
			ArrayList<Object[]> movimientos = CtrlMovimiento.getConsulta(
					fechaInicio, 
					fechaFin,
					objAlmacen,
					objTipoMovimiento,
					chbAnulados.isSelected(), false);
			for (Object[] movimiento : movimientos) {
				model.addRow(movimiento);
			}
		}catch(SessionException se){
			WebOptionPane.showMessageDialog(
					this, se.getMessage(), "Acceso Denegado", 
					WebOptionPane.WARNING_MESSAGE);
		}catch(Exception e){
			WebOptionPane.showMessageDialog(
					this, "Error al cargar tabla de movimientos: " + e.getMessage(),
					"Error interno", WebOptionPane.WARNING_MESSAGE);
		}
		tblMovimientos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblMovimientos.setModel(model);
		Utilidades.packColumns(tblMovimientos, 0);
		tblMovimientos.setEditable(false);
	}
	
	private void anular(){
		try{
			CtrlMovimiento.anularMovimiento((int)tblMovimientos.getModel().getValueAt(tblMovimientos.getSelectedRow(), 0), false);
			WebOptionPane.showMessageDialog(this, 
					"Movimiento anulado correctamente", 
					"Éxito", WebOptionPane.INFORMATION_MESSAGE);
			configTable_Movimiento();
		} catch (Excepcion e) {
			WebOptionPane.showMessageDialog(this, "No puede anular",
					"Error", WebOptionPane.WARNING_MESSAGE);
		}catch(Exception e){
			WebOptionPane.showMessageDialog(this, 
					"Error al intentar guardar la información, por favor intente nuevamente", 
					"Error Interno", WebOptionPane.ERROR_MESSAGE);
		}
		
	}
}