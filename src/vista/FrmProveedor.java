/**
 * 
 */
package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ImageIcon;
import javax.swing.SpringLayout;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.text.WebTextField;

import clases.Articulo;
import clases.Proveedor;
import controlador.CtrlArticulo;
import controlador.CtrlProveedor;
import utilidades.Utilidades;

/**
 * @author edder
 *
 */
public class FrmProveedor extends WebDialog {
	private Proveedor proveedor;
	private WebTextField txtNombre,
						txtDescripcion,
						txtPreventa,
						txtMercaderista,
						txtDireccion,
						txtTelefonos;
	private WebButton btnGuardar, btnCancelar, btnEliminar;
	private WebLabel lblId;
	
	FrmProveedor(WebFrame owner, boolean modal, Proveedor proveedor){
		super(owner, modal);
		setTitle("Proveedor");
		setSize(600, 400);
		setDefaultCloseOperation(WebDialog.DISPOSE_ON_CLOSE);
        setShowResizeCorner(false);
        setShowMaximizeButton(false);
        this.proveedor = proveedor;
        initComponents();
        setLocationRelativeTo(null);
        setMinimumSize(new Dimension(400, 0));
        pack();
	}
	
	public void initComponents(){
		txtNombre = new WebTextField();
		txtDescripcion = new WebTextField();
		txtPreventa = new WebTextField();
		txtMercaderista = new WebTextField();
		txtDireccion = new WebTextField();
		txtTelefonos = new WebTextField();
		
		btnEliminar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_ELIMINAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_ELIMINAR[1]))
        		);
        btnGuardar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_GUARDAR[1]))
        		);
        btnCancelar = Utilidades.getButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_CANCELAR[1]))
        		);
        lblId = new WebLabel("ID: ");
        
        if(proveedor != null)
        	cargarProveedor();
        add(new WebPanel(){{
        	setBackground(Color.white);
        	setLayout(new BorderLayout());
        	add(new GroupPanel(){{
        		setLayout(new SpringLayout());
                add(new WebLabel("Nombre"));
                add(txtNombre);
                
                add(new WebLabel("Descripción"));
                add(txtDescripcion);
                
                add(new WebLabel("Preventa"));
                add(txtPreventa);

                add(new WebLabel("Mercaderista"));
                add(txtMercaderista);

                add(new WebLabel("Dirección"));
                add(txtDireccion);
                
                add(new WebLabel("Teléfonos"));
                add(txtTelefonos);
                
                SpringUtilities.makeCompactGrid(this,
                            6, 2,
                            6, 6,
                            6, 6);
        	}}, BorderLayout.CENTER);
        	add(new GroupPanel(){{
                setLayout(new BorderLayout());
                add(lblId, BorderLayout.WEST);
                add(new GroupPanel(){{
                	setLayout(new FlowLayout(FlowLayout.RIGHT));
                	add(btnEliminar, btnGuardar, btnCancelar);
                }}, BorderLayout.EAST);
        	}}, BorderLayout.SOUTH);
        }});
        addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
                txtNombre.requestFocusInWindow();
            }

            @Override
            public void windowClosing(WindowEvent e) {}

            @Override
            public void windowClosed(WindowEvent e) {
            	((FrmProveedores)FrmProveedor.this.getParent()).buscar();
            }

            @Override
            public void windowIconified(WindowEvent e) {}

            @Override
            public void windowDeiconified(WindowEvent e) {}

            @Override
            public void windowActivated(WindowEvent e) {}

            @Override
            public void windowDeactivated(WindowEvent e) {}
    });
        
        btnGuardar.addActionListener((ActionEvent e) -> {
        	guardar();
        });
	}
	
	public void prepararDatos(){
		proveedor.setNombre(txtNombre.getText());
		proveedor.setDescripcion(txtDescripcion.getText());
		proveedor.setPreventa(txtPreventa.getText());
		proveedor.setMercaderista(txtMercaderista.getText());
		proveedor.setDireccion(txtDireccion.getText());
		proveedor.setTelefono(txtTelefonos.getText());
	}
	
	private void guardar(){
        if(proveedor == null){
            proveedor = new Proveedor();
            proveedor.setIdProveedor(-1);
        }
        prepararDatos();
        if(proveedor.getIdProveedor() == -1){
            try {
                CtrlProveedor.insert(proveedor, false);                
                lblId.setText("ID: " + String.valueOf(proveedor.getIdProveedor()));
                WebOptionPane.showMessageDialog(
                        this,
                        "Información registrada correctamente",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            }catch(Exception e){
                WebOptionPane.showMessageDialog(
                        this,
                        "No se pudo guardar la información, verificar e intentar nuevamente",
                        "Error",
                        WebOptionPane.ERROR_MESSAGE);
            	e.printStackTrace();            	
            }
        }else{
            try {
                CtrlProveedor.update(proveedor, false);
                WebOptionPane.showMessageDialog(
                        this,
                        "Información registrada correctamente",
                        "Éxito",
                        WebOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            }catch(Exception e){
            	e.printStackTrace();
                WebOptionPane.showMessageDialog(
                        this,
                        "No se pudo guardar la información, verificar e intentar nuevamente",
                        "Error",
                        WebOptionPane.ERROR_MESSAGE);
            }
        }
    }
	
	public void cargarProveedor(){
		txtNombre.setText(proveedor.getNombre());
		txtDescripcion.setText(proveedor.getDescripcion());
		txtPreventa.setText(proveedor.getPreventa());
		txtMercaderista.setText(proveedor.getMercaderista());
		txtDireccion.setText(proveedor.getDireccion());
		txtTelefonos.setText(proveedor.getTelefono());
		lblId.setText("ID: " + proveedor.getIdProveedor());
	}
}
