package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

import utilidades.Utilidades;
import clases.TipoUsuario;
import clases.Usuario;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;

import controlador.CtrlUsuario;

@SuppressWarnings("serial")
public class FrmGestionUsuarios extends WebFrame{
	private WebPanel pnlTiposUsuario;
	private WebTable tblUsuarios,
					 tblPermisos;
	private WebButton btnNuevoTipo,
					  btnNuevoUsuario,
					  btnEditarPermisos;

	public FrmGestionUsuarios() {
		setDefaultCloseOperation(WebFrame.DISPOSE_ON_CLOSE);
		setShowMaximizeButton(false);
		setShowResizeCorner(false);
		setSize(900, 400);
		initComponents();
	}

	private void initComponents(){
		pnlTiposUsuario = new WebPanel();
		tblUsuarios = new WebTable();
		tblPermisos = new WebTable();
		btnNuevoTipo = new WebButton("Nuevo");
		btnNuevoUsuario = Utilidades.getIconButton(
        		new ImageIcon(getClass().getResource(Utilidades.N_IAGREGAR[0])),
        		new ImageIcon(getClass().getResource(Utilidades.N_IAGREGAR[1])));
		btnEditarPermisos = new WebButton("Editar");

		pnlTiposUsuario.setLayout(new BoxLayout(pnlTiposUsuario, BoxLayout.PAGE_AXIS));
		pnlTiposUsuario.setBackground(Color.white);

		llenarTipos();

		config_TableUsuarios();
		config_TablePermisos();
		llenarUsuarios();

		add(new WebPanel(){{
			setBackground(Color.white);
			setLayout(new BorderLayout());
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				add(new GroupPanel(){{
					setLayout(new BorderLayout());
					add(new WebLabel("Tipos"), BorderLayout.CENTER);
					add(btnNuevoTipo, BorderLayout.EAST);
				}}, BorderLayout.NORTH);
				add(new WebScrollPane(pnlTiposUsuario), BorderLayout.CENTER);
			}}, BorderLayout.WEST);
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				add(new GroupPanel(){{
					setLayout(new BorderLayout());
					add(new WebLabel("Permisos"), BorderLayout.CENTER);
					add(btnEditarPermisos, BorderLayout.EAST);
				}}, BorderLayout.NORTH);
				add(new WebScrollPane(tblPermisos), BorderLayout.CENTER);
			}}, BorderLayout.CENTER);
			add(new GroupPanel(){{
				setLayout(new BorderLayout());
				add(new GroupPanel(){{
					setLayout(new BorderLayout());
					add(new WebLabel("Usuarios"), BorderLayout.CENTER);
					add(btnNuevoUsuario, BorderLayout.EAST);
				}}, BorderLayout.NORTH);
				add(new WebScrollPane(tblUsuarios), BorderLayout.CENTER);
			}}, BorderLayout.EAST);
		}});
		
		addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent evt){
                FrmPrincipal.bolFrmGestionUsuario = false;
            }
        });
	}

	private void llenarTipos(){
		try {
			ArrayList<TipoUsuario> tiposUsuario = CtrlUsuario.getTipos(false);
			pnlTiposUsuario.removeAll();
			tiposUsuario.stream().forEach((tipo)->{
				ItmTipoUsuario item = new ItmTipoUsuario(this, tipo);				
				pnlTiposUsuario.add(item);
				
				item.addMouseListener(new MouseListener() {
					@Override
					public void mouseReleased(MouseEvent e) {
					}
					
					@Override
					public void mousePressed(MouseEvent e) {
					}
					
					@Override
					public void mouseExited(MouseEvent e) {
					}
					
					@Override
					public void mouseEntered(MouseEvent e) {
					}
					
					@Override
					public void mouseClicked(MouseEvent e) {
						llenarPermisos(tipo);
					}
				});
			});
			pnlTiposUsuario.updateUI();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void config_TableUsuarios(){
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.addColumn("Nombre");
		modelo.addColumn("Usuario");
		modelo.addColumn("Tipo");
		tblUsuarios.setModel(modelo);
	}

	private void config_TablePermisos(){
		DefaultTableModel modelo = new DefaultTableModel();
		tblPermisos.setModel(modelo);
		modelo.addColumn("Accion");
		modelo.addColumn("Buscar");
		modelo.addColumn("Agregar");
		modelo.addColumn("Editar");
		modelo.addColumn("Eliminar");
	}


	private void llenarUsuarios(){
		try{
			ArrayList<Usuario> usuarios = CtrlUsuario.getUsuarios(false);
			if(usuarios.size() > 0){
				DefaultTableModel model = (DefaultTableModel)(tblUsuarios.getModel());
				for(Usuario usuario : usuarios){
					model.addRow(new Object[]{
						usuario.getNombre()
					});
				}
				Utilidades.packColumns(tblUsuarios, 0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void llenarPermisos(TipoUsuario tipoUsuario){
		Object[][] filas = new Object[6][5]; 
		Object[] fila;
		
		fila = new Object[]{
			"Movimiento",
			(tipoUsuario.getP_movimiento() & TipoUsuario.CONSULTAR) == TipoUsuario.CONSULTAR,
			(tipoUsuario.getP_movimiento() & TipoUsuario.INSERTAR) == TipoUsuario.INSERTAR,
			(tipoUsuario.getP_movimiento() & TipoUsuario.EDITAR) == TipoUsuario.EDITAR,
			(tipoUsuario.getP_movimiento() & TipoUsuario.ELIMINAR) == TipoUsuario.ELIMINAR
		};
		filas[0] = fila;

		fila = new Object[]{
			"Venta",
			(tipoUsuario.getP_venta() & TipoUsuario.CONSULTAR) == TipoUsuario.CONSULTAR,
			(tipoUsuario.getP_venta() & TipoUsuario.INSERTAR) == TipoUsuario.INSERTAR,
			(tipoUsuario.getP_venta() & TipoUsuario.EDITAR) == TipoUsuario.EDITAR,
			(tipoUsuario.getP_venta() & TipoUsuario.ELIMINAR) == TipoUsuario.ELIMINAR
		};
		filas[1] = fila;

		fila = new Object[]{
			"Comprobante",
			(tipoUsuario.getP_comprobante() & TipoUsuario.CONSULTAR) == TipoUsuario.CONSULTAR,
			(tipoUsuario.getP_comprobante() & TipoUsuario.INSERTAR) == TipoUsuario.INSERTAR,
			(tipoUsuario.getP_comprobante() & TipoUsuario.EDITAR) == TipoUsuario.EDITAR,
			(tipoUsuario.getP_comprobante() & TipoUsuario.ELIMINAR) == TipoUsuario.ELIMINAR
		};
		filas[2] = fila;

		fila = new Object[]{
			"Orden de Compra",
			(tipoUsuario.getP_ordenCompra() & TipoUsuario.CONSULTAR) == TipoUsuario.CONSULTAR,
			(tipoUsuario.getP_ordenCompra() & TipoUsuario.INSERTAR) == TipoUsuario.INSERTAR,
			(tipoUsuario.getP_ordenCompra() & TipoUsuario.EDITAR) == TipoUsuario.EDITAR,
			(tipoUsuario.getP_ordenCompra() & TipoUsuario.ELIMINAR) == TipoUsuario.ELIMINAR
		};
		filas[3] = fila;

		fila = new Object[]{
			"Compra",
			(tipoUsuario.getP_compra() & TipoUsuario.CONSULTAR) == TipoUsuario.CONSULTAR,
			(tipoUsuario.getP_compra() & TipoUsuario.INSERTAR) == TipoUsuario.INSERTAR,
			(tipoUsuario.getP_compra() & TipoUsuario.EDITAR) == TipoUsuario.EDITAR,
			(tipoUsuario.getP_compra() & TipoUsuario.ELIMINAR) == TipoUsuario.ELIMINAR
		};
		filas[4] = fila;

		fila = new Object[]{
			"Detalle Movimiento Caja",
			(tipoUsuario.getP_movimiento_caja() & TipoUsuario.CONSULTAR) == TipoUsuario.CONSULTAR,
			(tipoUsuario.getP_movimiento_caja() & TipoUsuario.INSERTAR) == TipoUsuario.INSERTAR,
			(tipoUsuario.getP_movimiento_caja() & TipoUsuario.EDITAR) == TipoUsuario.EDITAR,
			(tipoUsuario.getP_movimiento_caja() & TipoUsuario.ELIMINAR) == TipoUsuario.ELIMINAR
		};
		filas[5] = fila;
		
		tblPermisos.setModel(new TablePermiso(filas));
		
		Utilidades.packColumns(tblPermisos, 0);
	}
	
	class TablePermiso extends AbstractTableModel{
		private String[] columnNames = { "Acción", "Buscar", "Agregar", "Editar", "Eliminar" };
		private Object[][] data;

        public TablePermiso(Object[][] data){
        	this.data = data;
        }
        
        @Override
        public int getColumnCount (){
            return columnNames.length;
        }

        @Override
        public int getRowCount (){
            return data.length;
        }

        @Override
        public String getColumnName ( int col ){
            return columnNames[ col ];
        }

        @Override
        public Object getValueAt ( int row, int col ){
            return data[ row ][ col ];
        }

        @SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
        public Class getColumnClass ( int c ){
        	return data[0][ c ].getClass ();
        }

        @Override
        public boolean isCellEditable ( int row, int col ){
            return col >= 1;
        }

        @Override
        public void setValueAt ( Object value, int row, int col ){
            data[ row ][ col ] = value;
            fireTableCellUpdated ( row, col );
        }
	}
}
