package controlador;

import java.sql.SQLException;

import modelo.Conexion;
import modelo.MdlFlujoCaja;
import utilidades.Excepcion;
import utilidades.SessionException;
import clases.FlujoCaja;

public class CtrlFlujoCaja {
	public static void insert(FlujoCaja objeto, boolean trans) throws SQLException,
			Excepcion, SessionException{
		try{
			float variacion = (objeto.getMovimiento() == 'E') ? objeto.getMonto()* -1 : objeto.getMonto();
			float montoActual = CtrlCaja.get(objeto.getCaja().getIdCaja(), true).getMonto();
			objeto.setMontoBefore(montoActual);
			objeto.setMontoAfter(montoActual + variacion);
			MdlFlujoCaja.insert(objeto);
			objeto.setIdFlujoCaja(MdlFlujoCaja.getLastId());
			CtrlCaja.updateMonto(objeto.getCaja().getIdCaja(), variacion, true);
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static void update(FlujoCaja objeto, boolean trans) throws SQLException,
			Excepcion, SessionException {
		try{
			MdlFlujoCaja.update(objeto);
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static FlujoCaja get(int id, boolean trans) throws SQLException,
			Excepcion, SessionException {
		try{
			FlujoCaja flujoCaja = MdlFlujoCaja.get(id);
			Conexion.evaluateTrans(trans);
			return flujoCaja;
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

}
