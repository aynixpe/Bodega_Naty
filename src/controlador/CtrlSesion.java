package controlador;

import java.sql.SQLException;

import modelo.Conexion;
import utilidades.Configuracion;
import utilidades.Excepcion;
import utilidades.Utilidades;
import clases.Almacen;
import clases.Caja;
import clases.Usuario;

public class CtrlSesion {

	public static Usuario usuario;
	public static Almacen almacen;
	public static Caja caja;
	
	public static boolean login(String user, String pass, Caja cj, Almacen alm)
			throws SQLException, Excepcion{
		try{
			usuario = CtrlUsuario.get(user, Utilidades.md5(pass), true);
			caja =  cj != null ? cj : CtrlCaja.get(Configuracion.getIdCaja(), true);
			almacen = alm != null ? alm : CtrlAlmacen.get(Configuracion.getIdAlmacen(), true);
			Conexion.evaluateTrans(false);
			return (usuario != null);
		}catch(SQLException ex){
			Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}
	
	public static boolean validarPermiso(int permiso, int accion) throws Excepcion, SQLException{        
        try{
			int p = CtrlUsuario.getPermiso(permiso, usuario.getIdUsuario(), false);
	        return (p & accion) == accion;
        }catch(SQLException ex){
			Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}
}
