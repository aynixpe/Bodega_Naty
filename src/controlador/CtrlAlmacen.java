package controlador;

import clases.Almacen;

import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.Excepcion;
import modelo.Conexion;
import modelo.MdlAlmacen;

/**
 * Clase que maneja
 * @author Edder
 * @version 1.0
 */
public class CtrlAlmacen{
	    
    /**
     * Este método inserta un Almacen, y a la vez asigna el id insertado al objeto
     * Si Conexion es null, trabaja con conexion propia
     * @param almacen Objeto de clases.Almacen con los datos a insertar (sin idAlmacen)
     * @param cnx
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public static void insert(Almacen almacen, boolean trans) throws Excepcion, SQLException{
    	try{
	    	MdlAlmacen.insert(almacen);
	    	almacen.setIdAlmacen(MdlAlmacen.getLastId());
	    	Conexion.evaluateTrans(trans);
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }
    
    /**
     * Este método actualiza la información del almacén
     * Si Conexion es null, trabaja con conexion propia
     * @param almacen Objeto de clases.Almacen
     * @param cnx
     * @throws SQLException
     * @throws ClassNotFoundException
     * @see MdlAlmacen.update(Almacen almacen)
     */
    public static void update(Almacen almacen, boolean trans) throws SQLException, Excepcion{
    	try{
	        MdlAlmacen.update(almacen);
	        Conexion.evaluateTrans(trans);
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }
    
    public static void dar_baja(Almacen almacen, boolean estado, boolean trans) throws Excepcion, SQLException{
    	try{
	        MdlAlmacen.dar_baja(almacen, estado);
	        Conexion.evaluateTrans(trans);
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }
    
    /**
     * Este método devuelve un ArrayList de almacenes cuyo nombre coincide con el parámetro %query%
     * Si Conexion es null, trabaja con conexion propia
     * @param query Cadena con la consulta de almacen por ejemplo: "Principal"
     * @param cnx
     * @return ArrayList - Lista de almacenes según búsqueda
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public static ArrayList<Almacen> getAlmacenes(String query, boolean trans) 
    		throws Excepcion, SQLException{
    	try{    	
	        ArrayList<Almacen> almacenes = MdlAlmacen.getAlmacenes(query);
	        Conexion.evaluateTrans(trans);     
	        return almacenes;
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }
    
    /**
     * Este método devuelve un ArrayList de almacenes cuyo nombre coincide con el parámetro %query%
     * Si Conexion es null, trabaja con conexion propia
     * @param query Cadena con la consulta de almacen por ejemplo: "Principal"
     * @param cnx
     *
     * @param estado @return ArrayList - Lista de almacenes según búsqueda
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public static ArrayList<Almacen> getAlmacenes_estado(String query, char estado, boolean trans) 
    		throws Excepcion, SQLException{
    	try{
	        ArrayList<Almacen> almacenes = MdlAlmacen.getAlmacenes_estado(query, estado);
	        Conexion.evaluateTrans(trans);      
	        return almacenes;
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }
    
    /**
     * Este método devuelve un objeto Almacen según el id
     * Si Conexion es null, trabaja con conexion propia
     * @param idAlmacen Un dato int con el id del Almacén
     * @param cnx
     * @return Almacen
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public static Almacen get(int idAlmacen, boolean trans) throws SQLException, Excepcion{
    	try{
	        Almacen almacen = MdlAlmacen.get(idAlmacen);
	        Conexion.evaluateTrans(trans);
	        return almacen;
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }
}
