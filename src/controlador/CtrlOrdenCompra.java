package controlador;

import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.Error;
import utilidades.Excepcion;
import utilidades.SessionException;
import clases.DetalleOrdenCompra;
import clases.OrdenCompra;
import clases.Proveedor;
import clases.TipoUsuario;
import modelo.Conexion;
import modelo.MdlDetalleOrdenCompra;
import modelo.MdlOrdenCompra;

public class CtrlOrdenCompra{
	public static void insert(OrdenCompra ordenCompra, boolean trans) throws SQLException, Excepcion {
		try{
			MdlOrdenCompra.insert(ordenCompra);
			ordenCompra.setIdOrdenCompra(MdlOrdenCompra.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static void insertDetalle(DetalleOrdenCompra detalle, boolean trans) throws SQLException, Excepcion{
		try{
			MdlDetalleOrdenCompra.insert(detalle);
			detalle.setIdDetalleOrdenCompra(MdlDetalleOrdenCompra.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static void insert(OrdenCompra ordenCompra, boolean trans, ArrayList<DetalleOrdenCompra> detalles)
			throws SQLException, Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.ORDEN_COMPRA, TipoUsuario.INSERTAR)) {
				CtrlOrdenCompra.insert(ordenCompra, true);
				for (DetalleOrdenCompra detalleOrdenCompra : detalles) {
					CtrlOrdenCompra.insertDetalle(detalleOrdenCompra, true);
				}
				Conexion.evaluateTrans(trans);
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para registrar orden de compra");
			}
		}catch( SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	/*
	 * Ultima orden de compra de un articulo determinado
	 */
	public static Object[] lastOrdenCompra_Articulo(int id, boolean trans)
			throws Excepcion, SQLException, SessionException{
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.ORDEN_COMPRA, TipoUsuario.CONSULTAR)) {
				Object[] ordencompra = MdlOrdenCompra.lastOrdenCompra_Articulo(id);
				Conexion.evaluateTrans(trans);
				return ordencompra;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar orden de compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static void update(OrdenCompra ordenCompra, boolean trans)
			throws SQLException, Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.ORDEN_COMPRA, TipoUsuario.EDITAR)) {
				MdlOrdenCompra.update(ordenCompra);
				Conexion.evaluateTrans(trans);
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar orden de compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static OrdenCompra get(int id, boolean trans) throws SQLException, Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.ORDEN_COMPRA, TipoUsuario.CONSULTAR)) {
				OrdenCompra ordenCompra = MdlOrdenCompra.get(id);
				Conexion.evaluateTrans(trans);
				return ordenCompra;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar orden de compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static ArrayList<OrdenCompra> getAll(String query, boolean trans) throws Excepcion, SQLException, SessionException{
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.ORDEN_COMPRA, TipoUsuario.CONSULTAR)) {
				ArrayList<OrdenCompra> ordenes = MdlOrdenCompra.get();
				Conexion.evaluateTrans(trans);
				return ordenes;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar orden de compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	/*
	 * Cambia de estado de una orden de compra
	 */
	public static void setEstado(int id, boolean trans) throws Excepcion, SQLException, SessionException{
		try{
			if(CtrlSesion.validarPermiso(TipoUsuario.ORDEN_COMPRA, TipoUsuario.EDITAR)){
				MdlOrdenCompra.setEstado(id);
				Conexion.evaluateTrans(trans);
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para editar orden de compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	/*
	 * Método usado en el frame de consulta para devolver los datos de orden de compra
	 */
	public static ArrayList<Object[]> getConsulta(boolean tipoFecha, String fechaInicio, String fechaFin, Proveedor proveedor, boolean estado, boolean trans)
			throws Excepcion, SQLException, SessionException{
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.ORDEN_COMPRA, TipoUsuario.CONSULTAR)) {
				ArrayList<Object[]> ordenes = MdlOrdenCompra.getConsulta(
						tipoFecha, fechaInicio, fechaFin, proveedor, estado);
				Conexion.evaluateTrans(trans);
				return ordenes;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar orden de compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	/*
	 * Método usado en el frame de consulta para mostrar los detalles de una orden de compra
	 */
	public static ArrayList<Object[]> getConsulta_Detalle(int idOrdenCompra, boolean trans) throws Excepcion, SQLException, SessionException{
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.ORDEN_COMPRA, TipoUsuario.CONSULTAR)) {
				ArrayList<Object[]> detalles = MdlDetalleOrdenCompra.getConsulta_Detalle(idOrdenCompra);
				Conexion.evaluateTrans(trans);
				return detalles;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar orden de compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static void anularOrdenCompra(int idOrdenCompra, boolean trans) throws SQLException, Excepcion, SessionException{
		try{
			if(CtrlSesion.validarPermiso(TipoUsuario.ORDEN_COMPRA, TipoUsuario.ELIMINAR)){
				MdlOrdenCompra.anularOrdenCompra(idOrdenCompra);
				Conexion.evaluateTrans(trans);
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para anular orden de compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

}
