package controlador;

import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.Excepcion;
import utilidades.Error;
import utilidades.SessionException;
import clases.Almacen;
import clases.Cliente;
import clases.DetalleMovimiento;
import clases.DetalleVenta;
import clases.Movimiento;
import clases.TipoMovimiento;
import clases.TipoUsuario;
import clases.Venta;
import modelo.Conexion;
import modelo.MdlDetalleVenta;
import modelo.MdlMovimiento;
import modelo.MdlVenta;

public class CtrlVenta{
	/*	genera movimiento y detalles
	*	venta y detalles
	*/
	public static void insert(Venta venta, boolean trans, ArrayList<DetalleVenta> detalles)
			throws SQLException, Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.VENTA, TipoUsuario.INSERTAR)) {
				ArrayList<DetalleMovimiento> detallesMovimiento = new ArrayList<DetalleMovimiento>();
				Movimiento movimiento = new Movimiento(
						-1,
						new TipoMovimiento(){{
							//descargo por venta
							setIdTipoMovimiento(9);
						}},
						venta.getFecha(),
						new Almacen(){{
							setIdAlmacen(1);
						}},
						null,
						'G'
				);
				for(DetalleVenta detalleVenta : detalles){
					detallesMovimiento.add(new DetalleMovimiento(
							-1,
							movimiento,
							detalleVenta.getAlmacenArticulo().getArticulo(),
							detalleVenta.getCantidad()
					));
				}
				CtrlMovimiento.insert(movimiento, true, detallesMovimiento);
				movimiento.setIdMovimiento(MdlMovimiento.getLastId());
				venta.setMovimiento(movimiento);
				CtrlVenta.insert(venta, true);
				for (DetalleVenta detalle: detalles) {
					CtrlVenta.insertDetalle(detalle, true);
				}
				Conexion.evaluateTrans(trans);
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permisos para registrar venta");
			}
		}catch(Excepcion | SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static void update(Venta venta, boolean trans) 
			throws SQLException, Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.VENTA, TipoUsuario.EDITAR)) {
				MdlVenta.update(venta);
				Conexion.evaluateTrans(trans);
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permisos para editar venta");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static Venta get(int id, boolean trans) throws SQLException, Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.VENTA, TipoUsuario.CONSULTAR)) {
				Venta venta = MdlVenta.get(id);
				Conexion.evaluateTrans(trans);
				return venta;
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permisos para consultar venta");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static void setDetalleComprobante(DetalleVenta detalleVenta, boolean trans)
			throws Excepcion, SQLException{
		try{
			MdlDetalleVenta.setDetalleComprobante(detalleVenta);
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static void insert(Venta venta, boolean trans) throws SQLException, Excepcion {
		try{
			MdlVenta.insert(venta);
			venta.setIdVenta(MdlVenta.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static void insertDetalle(DetalleVenta detalle, boolean trans)
			throws Excepcion, SQLException{
		try{
			MdlDetalleVenta.insert(detalle);
			detalle.setIdDetalleVenta(MdlDetalleVenta.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static ArrayList<Object[]> getConsulta(String fechaInicio, String fechaFin, 
			Cliente cliente, Almacen almacen, boolean estado, boolean trans) 
					throws Excepcion, SQLException, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.VENTA, TipoUsuario.CONSULTAR)) {
				ArrayList<Object[]> ventas = MdlVenta.getConsulta(fechaInicio, fechaFin, cliente, almacen, estado);
				Conexion.evaluateTrans(trans);
				return ventas;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar venta");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static ArrayList<Object[]> getConsulta_Detalle(int idVenta, boolean trans)
			throws Excepcion, SQLException, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.VENTA, TipoUsuario.CONSULTAR)) {
				ArrayList<Object[]> detalles = MdlDetalleVenta.getConsulta_Detalle(idVenta);
				Conexion.evaluateTrans(trans);
				return detalles;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar venta");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static void anularVenta(int idVenta, boolean trans)
			throws Excepcion, SQLException, SessionException, Excepcion{
		try{
			if(CtrlSesion.validarPermiso(TipoUsuario.VENTA, TipoUsuario.ELIMINAR)){
				Venta venta = CtrlVenta.get(idVenta, true);
				MdlVenta.anularVenta(idVenta);
				CtrlMovimiento.anularMovimiento(venta.getMovimiento().getIdMovimiento(), true);
				Conexion.evaluateTrans(trans);
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para anular venta");
			}
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static ArrayList<DetalleVenta> getDetalles(int idVenta, boolean trans) 
			throws SQLException, Excepcion{
		try{
			ArrayList<DetalleVenta> detalles = MdlDetalleVenta.getDetalles(idVenta);
			Conexion.evaluateTrans(trans);
			return detalles;
		}catch(ClassNotFoundException | SQLException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
		}
	}

	public static void procesarVenta(int idVenta, boolean trans) throws ClassNotFoundException, SQLException{
		MdlVenta.procesarVenta(idVenta);
		Conexion.evaluateTrans(trans);
	}
}
