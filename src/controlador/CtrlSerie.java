package controlador;

import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.Excepcion;
import clases.Serie;
import modelo.Conexion;
import modelo.MdlSerie;

public class CtrlSerie{
	public static void insert(Serie serie, boolean trans) throws SQLException,
			Excepcion {
		try{
			MdlSerie.insert(serie);
			serie.setIdSerie(MdlSerie.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}

	public static void update(Serie serie, boolean trans) throws SQLException,
			Excepcion {
		try{
			MdlSerie.update(serie);
			Conexion.evaluateTrans(trans);
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}

	public static Serie get(int id, boolean trans) throws SQLException,
			Excepcion {
		try{
			Serie serie = MdlSerie.get(id);
			Conexion.evaluateTrans(trans);
			return serie;
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}
	
	public static ArrayList<Serie> get(String query, boolean trans) throws SQLException, Excepcion{
		try{
			ArrayList<Serie> series = MdlSerie.get(query);
			Conexion.evaluateTrans(trans);
			return series;
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}
	
	public static ArrayList<Serie> getSeries_TipoComprobante(int idTipoComprobante, boolean trans)
			throws Excepcion, SQLException{
		try{
			ArrayList<Serie> series = MdlSerie.getSeries_TipoComprobante(idTipoComprobante);
			Conexion.evaluateTrans(trans);
			return series;
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}
}
