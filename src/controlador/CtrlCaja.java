package controlador;

import java.sql.SQLException;
import java.util.ArrayList;

import modelo.Conexion;
import modelo.MdlCaja;
import utilidades.Excepcion;
import utilidades.SessionException;
import clases.Caja;

public class CtrlCaja{


	public static void insert(Caja caja, boolean trans) throws SQLException,
			Excepcion, SessionException {
		try{
			MdlCaja.insert(caja);
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static void updateMonto(int idCaja, float variacion, boolean trans)
			throws Excepcion, SQLException{
		try{
			MdlCaja.updateMonto(idCaja, variacion);
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}


	public static void update(Caja caja, boolean trans) throws SQLException,
			Excepcion, SessionException {
		try{
			MdlCaja.update(caja);
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}


	public static Caja get(int id, boolean trans) throws SQLException, Excepcion{
		try{
			Caja caja = MdlCaja.get(id);
			Conexion.evaluateTrans(trans);
			return caja;
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static ArrayList<Caja> get(String query, boolean trans) throws SQLException, Excepcion{
		try{
			ArrayList<Caja> cajas = MdlCaja.get(query);
			Conexion.evaluateTrans(trans);
			return cajas;
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}
}
