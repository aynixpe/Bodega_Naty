package controlador;

import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.Excepcion;
import clases.Cliente;
import modelo.Conexion;
import modelo.MdlCliente;

public class CtrlCliente{

	public static void insert(Cliente cliente, boolean trans) throws Excepcion, SQLException{
		try{
			MdlCliente.insert(cliente);
			cliente.setIdCliente(MdlCliente.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static void update(Cliente cliente, boolean trans) throws Excepcion, SQLException{
		try{
			MdlCliente.update(cliente);
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static ArrayList<Cliente> getClientes(String query, boolean trans) 
			throws SQLException, Excepcion{
		try{
			ArrayList<Cliente> clientes = MdlCliente.get(query);
			Conexion.evaluateTrans(trans);
			return clientes;
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static Cliente get(int id, boolean trans) throws SQLException, Excepcion {
		try{
			Cliente cliente = MdlCliente.get(id);
			Conexion.evaluateTrans(trans);
			return cliente;
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static String getNombre(int idCliente, boolean trans) throws Excepcion, SQLException{
		try{
			String nombre = MdlCliente.getNombre(idCliente);
			Conexion.evaluateTrans(trans);
			return nombre;
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}
}
