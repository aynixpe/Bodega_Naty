package controlador;

import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.Excepcion;
import clases.Proveedor;
import modelo.Conexion;
import modelo.MdlProveedor;

public class CtrlProveedor{
	public static void insert(Proveedor proveedor, boolean trans) throws SQLException,
			ClassNotFoundException, Excepcion {
		try{
			MdlProveedor.insert(proveedor);
			proveedor.setIdProveedor(MdlProveedor.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar proveedor");
		}
	}

	public static void update(Proveedor proveedor, boolean trans) throws SQLException,
			ClassNotFoundException, Excepcion {
		try{
			MdlProveedor.update(proveedor);
			Conexion.evaluateTrans(trans);
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar proveedor");
		}
	}

	public static Proveedor get(int id, boolean trans) throws SQLException,
			Excepcion {
		try{
			Proveedor proveedor = MdlProveedor.get(id);
			Conexion.evaluateTrans(trans);
			return proveedor;
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Consultar proveedor");
		}
	}
	
	public static ArrayList<Proveedor> getProveedores_busqueda(String query, boolean trans) 
			throws Excepcion, SQLException{
		try{
			ArrayList<Proveedor> proveedores = MdlProveedor.getProveedores_busqueda(query);
			Conexion.evaluateTrans(trans);
			return proveedores;
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}

}
