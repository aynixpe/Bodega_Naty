/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import clases.AlmacenArticulo;
import clases.Articulo;
import clases.Categoria;

import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.Excepcion;
import modelo.Conexion;
import modelo.MdlAlmacenArticulo;
import modelo.MdlArticulo;

/**
 *
 * @author Edder
 */
public class CtrlArticulo{

    /**
     *
     * @param articulo
     * @param cnx
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void insert(Articulo articulo, boolean trans) throws Excepcion, SQLException {
    	try{
	    	MdlArticulo.insert(articulo);
	        articulo.setIdArticulo(MdlArticulo.getLastId());
	        Conexion.evaluateTrans(trans);
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		ex.printStackTrace();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    public static void fastInsert(Articulo articulo, boolean trans) 
    		throws Excepcion, SQLException {
    	try{
	        MdlArticulo.fastInsert(articulo);
	        articulo.setIdArticulo(MdlArticulo.getLastId());
	        Conexion.evaluateTrans(trans);
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param articulo
     * @param cnx
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void update(Articulo articulo, boolean trans) throws Excepcion, SQLException {
    	try{
	        MdlArticulo.update(articulo);
	        Conexion.evaluateTrans(trans);
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		ex.printStackTrace();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     * Cambia estado de artículo a 'I': Inactivo
     * @param articulo
     * @param cnx
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void dar_baja(Articulo articulo, boolean trans) throws Excepcion, SQLException {
    	try{
	        MdlArticulo.dar_baja(articulo);
	        Conexion.evaluateTrans(trans);
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param query
     * @param cnx
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static ArrayList<Articulo> getArticulos(String query, boolean trans) 
    		throws Excepcion, SQLException {
    	try{
	        ArrayList<Articulo> articulos = MdlArticulo.getArticulos(query, null);
	        Conexion.evaluateTrans(trans);
	        return articulos;
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param query
     * @param inactivo
     * @param cnx
     * @param categorias
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static ArrayList<Articulo> getArticulos_categorias(String query, ArrayList<Categoria> categorias, 
    		boolean inactivo, boolean trans) throws Excepcion, SQLException {
    	try{
	        ArrayList<Articulo> articulos;
	        articulos = MdlArticulo.getCategoria(query, categorias, inactivo);
	        
	        Conexion.evaluateTrans(trans);
	        return articulos;
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *Devuelve un arreglo de Object de  3 columnas (idArticulo, nombre, stock)
     * @param idAlmacen
     * @param query
     * @param cnx
     * @return ArrayList<Object[]>
     * @throws SQLException
     * @throws java.lang.ClassNotFoundException
     */
    public static ArrayList<Object[]> getArticulosStock_almacen(int idAlmacen, String query, boolean trans) 
    		throws SQLException, Excepcion{
        try{
	    	ArrayList<Object[]> articulosStock = MdlArticulo.getArticulosStock_almacen(idAlmacen, query);
	        Conexion.evaluateTrans(trans);
	        return articulosStock;
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *Devuelve un arreglo con los articulos de una orden de compra
     */
    public static ArrayList<Object[]> getArticulos_OrdenCompra(int idAlmacen, String query, boolean flag, boolean trans) 
    		throws Excepcion, SQLException{
    	try{
	    	ArrayList<Object[]> articulos_S_SM = MdlArticulo.getArticulos_OrdenCompra(idAlmacen, query, flag);
	        Conexion.evaluateTrans(trans);
	    	return articulos_S_SM;
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param idAlmacen
     * @param cnx
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static ArrayList<Articulo> getArticulos_almacen(int idAlmacen, String busqueda, boolean trans) 
    		throws Excepcion, SQLException {
        try{
	    	ArrayList<Articulo> articulos = MdlArticulo.getArticulos_almacen(idAlmacen, busqueda);
	        Conexion.evaluateTrans(trans);
	        return articulos;
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param idCategoria
     * @param query
     * @param cnx
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static ArrayList<Articulo> getArticulos_categoria(int idCategoria, String query, boolean trans) 
    		throws Excepcion, SQLException {
        try{
	    	ArrayList<Articulo> articulos = MdlArticulo.getArticulos_categoria(idCategoria, query);
	        Conexion.evaluateTrans(trans);
	        return articulos;
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param idArticulo
     * @param cnx
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static Articulo get(int idArticulo, boolean trans) throws Excepcion, SQLException {
        try{
	    	Articulo articulo = MdlArticulo.get(idArticulo);
	        Conexion.evaluateTrans(trans);
	        return articulo;
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     * Actualiza el stock de un artículo en un almacén
     *
     * @param idAlmacenArticulo
     * @param variacion
     * @param cnx
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void updateStock(int idAlmacenArticulo, int variacion, boolean trans) 
    		throws Excepcion, SQLException {
        try{
	    	MdlAlmacenArticulo.updateStock(idAlmacenArticulo, variacion);
	        Conexion.evaluateTrans(trans);
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     * Asigna a almacen a los articulos, stock y stock mínimo en 0
     *
     * @param idArticulo
     * @param idAlmacen
     * @param cnx
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void asignarAlmacen(ArrayList<Integer> ids, int idAlmacen, boolean trans) 
    		throws Excepcion, SQLException {
        try{
	    	for (Integer id : ids) {
	        	if(!existAlmacenArticulo(id, idAlmacen, true))
	        		MdlArticulo.asignarAlmacen(id, idAlmacen);
	        	else
	        		System.out.println("No asignó almacén");
			}
	        Conexion.evaluateTrans(trans);
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param almacenArticulo
     * @param cnx
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void insertAlmacenArticulo(AlmacenArticulo almacenArticulo, boolean trans) 
    		throws Excepcion, SQLException {
        try{
        	MdlAlmacenArticulo.insert(almacenArticulo);
        	Conexion.evaluateTrans(trans);
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param idArticulo
     * @param cnx
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static ArrayList<Object[]> getStockforArticuloInAlmacen(int idArticulo, boolean trans) 
    		throws SQLException, Excepcion {
        try{
	    	ArrayList<Object[]> almacenArticulos = MdlAlmacenArticulo.getStockforArticuloInAlmacen(idArticulo);
	        Conexion.evaluateTrans(trans);
	        return almacenArticulos;
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }
    /**
     *
     * @param idAlmacen
     * @param cnx
     * @return AlmacenArticulo, para obtener los stocks en un almacén determinado
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static ArrayList<AlmacenArticulo> getStocks_almacen(int idAlmacen, boolean trans) 
    		throws Excepcion, SQLException{
        try{
	    	ArrayList<AlmacenArticulo> almacenArticulos = MdlAlmacenArticulo.getAlmacenArticulos_almacen(idAlmacen);
	        for(AlmacenArticulo almacenArticulo : almacenArticulos){
	            int idArticulo = almacenArticulo.getArticulo().getIdArticulo();
	            almacenArticulo.setArticulo(MdlArticulo.get(idArticulo));
	        }
	        Conexion.evaluateTrans(trans);
	        return almacenArticulos;
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    public static int getArticuloStock(int idArticulo, int idAlmacen, boolean trans) 
    		throws Excepcion, SQLException{
        try{
	    	AlmacenArticulo almacenArticulo = MdlAlmacenArticulo.get(idAlmacen, idArticulo);
	        Conexion.evaluateTrans(trans);
	        return almacenArticulo.getStock();
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }


    public static AlmacenArticulo getAlmacenArticulo(int idAlmacen, int idArticulo, boolean trans) 
    		throws Excepcion, SQLException{
    	try{
	    	AlmacenArticulo almacenArticulo = MdlAlmacenArticulo.get(idAlmacen, idArticulo);
	        Conexion.evaluateTrans(trans);
	    	return almacenArticulo;
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    public static AlmacenArticulo getAlmacenArticulo(int idAlmacenArticulo, boolean trans) 
    		throws Excepcion, SQLException{
    	try{
	    	AlmacenArticulo almacenArticulo = MdlAlmacenArticulo.get(idAlmacenArticulo);
	        Conexion.evaluateTrans(trans);
	    	return almacenArticulo;
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    public static ArrayList<Object[]> getArticulos(int idAlmacen, Categoria categoria, String query, boolean trans) 
    		throws Excepcion, SQLException{
        try{
	    	ArrayList<Object[]> rows = MdlArticulo.getArticulos(idAlmacen, categoria, query);
	        Conexion.evaluateTrans(trans);
	        return rows;
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    public static int getIdArticulo(int idAlmacenArticulo, boolean trans) 
    		throws Excepcion, SQLException{
    	try{
	    	int id = MdlAlmacenArticulo.getIdArticulo(idAlmacenArticulo);
	        Conexion.evaluateTrans(trans);
	    	return id;
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }
    
    private static boolean existAlmacenArticulo(int idArticulo, int idAlmacen, boolean trans)
    		throws Excepcion, SQLException{
    	try{
	    	AlmacenArticulo aa = CtrlArticulo.getAlmacenArticulo(idArticulo, idAlmacen, trans);
	    	if(aa!=null)
	    		return true;
	    	return false;
    	}catch(SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }
}
