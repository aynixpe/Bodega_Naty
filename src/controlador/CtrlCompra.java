package controlador;

import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.Excepcion;
import utilidades.Error;
import utilidades.SessionException;
import clases.Compra;
import clases.DetalleCompra;
import clases.DetalleMovimiento;
import clases.DetalleMovimientoCaja;
import clases.Movimiento;
import clases.MovimientoCaja;
import clases.Proveedor;
import clases.TipoMovimiento;
import clases.TipoMovimientoCaja;
import clases.TipoUsuario;
import modelo.Conexion;
import modelo.MdlCompra;
import modelo.MdlDetalleCompra;
import modelo.MdlMovimiento;
import modelo.MdlMovimientoCaja;

public class CtrlCompra{

	public static void insert(Compra compra, boolean trans) throws SQLException, Excepcion {
		try{
			MdlCompra.insert(compra);
			compra.setIdCompra(MdlCompra.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static void insertDetalle(DetalleCompra detalle, boolean trans) 
			throws SQLException, Excepcion{
		try{
			MdlDetalleCompra.insert(detalle);
			detalle.setIdDetalleCompra(MdlDetalleCompra.getLastId());
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static void insert(Compra compra, boolean trans, ArrayList<DetalleCompra> detalles) 
			throws SQLException, Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.COMPRA, TipoUsuario.INSERTAR)) {
				ArrayList<DetalleMovimientoCaja> detallesMovimientoCaja = new ArrayList<DetalleMovimientoCaja>();
				MovimientoCaja movimientoCaja = new MovimientoCaja(
						0,
						new TipoMovimientoCaja(){{
							setIdTipoMovimientoCaja(3);
						}},
						2,
						compra.getFecha(),
						compra.getMonto(),
						CtrlSesion.usuario,
						'G',
						//Configurar almacenes
						CtrlSesion.caja,
						null
				);
				for(DetalleCompra detallecompra : detalles){
					detallesMovimientoCaja.add(new DetalleMovimientoCaja(
							0, movimientoCaja,
							(detallecompra.getPrecio()*detallecompra.getCantidad()),
							"Pago por compra"));
				}
				CtrlMovimientoCaja.insert(movimientoCaja, true, detallesMovimientoCaja);
				movimientoCaja.setIdMovimientoCaja(MdlMovimientoCaja.getLastId());
				compra.setMovimientoCaja(movimientoCaja);
	
				ArrayList<DetalleMovimiento> detallesMovimiento = new ArrayList<DetalleMovimiento>();
				Movimiento movimiento = new Movimiento(
						0,
						new TipoMovimiento(){{
							//Cargo por compra
							setIdTipoMovimiento(5);
						}},
						compra.getFecha(),
						null,
						detalles.get(0).getAlmacenArticulo().getAlmacen(),
						'G'
				);
				for (DetalleCompra detallecompra : detalles) {
					detallesMovimiento.add(new DetalleMovimiento(0,
							movimiento,
							detallecompra.getAlmacenArticulo().getArticulo(),
							detallecompra.getCantidad()
						));
				}
				CtrlMovimiento.insert(movimiento, true, detallesMovimiento);
				movimiento.setIdMovimiento(MdlMovimiento.getLastId());
				compra.setMovimiento(movimiento);
	
				CtrlCompra.insert(compra, true);
				for (DetalleCompra detalleCompra : detalles) {
					CtrlCompra.insertDetalle(detalleCompra, true);
				}
				Conexion.evaluateTrans(trans);
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para registrar Compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar compra");
    	}
	}

	public static void setMovimiento(Compra compra, boolean trans) throws Excepcion, SQLException{
		try{
			MdlCompra.setMovimiento(compra);
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}


	public static void update(Compra objeto, boolean trans) throws SQLException, Excepcion, SessionException {
		//Cambio
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.COMPRA, TipoUsuario.EDITAR)) {
				MdlCompra.update(objeto);
				Conexion.evaluateTrans(trans);
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para editar compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}


	public static Compra get(int id, boolean trans) throws SQLException, Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.COMPRA, TipoUsuario.CONSULTAR)) {
				Compra compra = MdlCompra.get(id);
				Conexion.evaluateTrans(trans);
				return compra;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static ArrayList<Object[]> getConsulta(String fechaInicio, String fechaFin, Proveedor proveedor, 
			boolean estado, boolean trans) throws Excepcion, SQLException, SessionException{
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.COMPRA, TipoUsuario.CONSULTAR)) {
				ArrayList<Object[]> compras = MdlCompra.getConsulta(fechaInicio, fechaFin, proveedor, estado);
				Conexion.evaluateTrans(trans);
				return compras;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static ArrayList<Object[]> getConsulta_Detalle(int idCompra, boolean trans) 
			throws SQLException, Excepcion, SessionException{
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.COMPRA, TipoUsuario.CONSULTAR)) {
				ArrayList<Object[]> detalles = MdlDetalleCompra.getConsulta_Detalle(idCompra);
				Conexion.evaluateTrans(trans);
		    	return detalles;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static void anularCompra(int idCompra, boolean trans) throws Excepcion, SQLException, SessionException, Excepcion{
		try{
			if(CtrlSesion.validarPermiso(TipoUsuario.COMPRA, TipoUsuario.CONSULTAR)){
				Compra compra = CtrlCompra.get(idCompra, true);
				MdlCompra.anularCompra(idCompra);
				CtrlMovimiento.anularMovimiento(compra.getMovimiento().getIdMovimiento(), true);
				CtrlMovimientoCaja.anularMovimientoCaja(compra.getMovimientoCaja().getIdMovimientoCaja(), true);
				Conexion.evaluateTrans(trans);
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}
	
	public static float lastCost(int idArticulo, boolean trans) throws Excepcion, SessionException, SQLException{
		float costo = 0f;
		try{
			if(CtrlSesion.validarPermiso(TipoUsuario.COMPRA, TipoUsuario.CONSULTAR)){
				costo = MdlDetalleCompra.lastCost(utilidades.Configuracion.getIdAlmacen(), idArticulo);
				Conexion.evaluateTrans(trans);
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar compra");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
		return costo;
	}
}
