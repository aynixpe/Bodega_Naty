package controlador;

import clases.Categoria;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.Excepcion;
import modelo.Conexion;
import modelo.MdlCategoria;
/**
 *
 * @author edder
 */
public class CtrlCategoria{

    /**
     *
     * @param categoria
     * @param cnx
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws FileNotFoundException
     */

    public static void insert(Categoria categoria, boolean trans)
            throws Excepcion, SQLException, FileNotFoundException{
    	try{
	        MdlCategoria.insert(categoria);
	        categoria.setIdCategoria(MdlCategoria.getLastId());
			Conexion.evaluateTrans(trans);
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		System.out.println(ex.getMessage());
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar categoría");
    	}
    }

    /**
     *
     * @param categoria
     * @param cnx
     * @throws ClassNotFoundException
     * @throws SQLException
     */

    public static void update(Categoria categoria, boolean trans) throws Excepcion, SQLException{
        try{
	    	MdlCategoria.update(categoria);
			Conexion.evaluateTrans(trans);
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     * Eliminar el registro de la base de datos
     * @param categoria
     * @param cnx
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void eliminar(Categoria categoria, boolean trans) throws SQLException, Excepcion{
        try{
	    	MdlCategoria.eliminar(categoria);
			Conexion.evaluateTrans(trans);
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param query
     * @param cnx
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static ArrayList<Categoria> getCategorias(String query, boolean trans)
            throws Excepcion, SQLException{
    	try{
	        ArrayList<Categoria> categorias = MdlCategoria.getCategorias(query);
			Conexion.evaluateTrans(trans);
	        return categorias;
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param idCategoria
     * @param cnx
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */

    public static Categoria get(int idCategoria, boolean trans) throws Excepcion, SQLException{
        try{
	    	Categoria categoria = MdlCategoria.get(idCategoria);
			Conexion.evaluateTrans(trans);
	        return categoria;
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    public static void changeImagen(Categoria categoria, boolean trans)
            throws Excepcion, SQLException, FileNotFoundException{
        try{
	    	MdlCategoria.changeImagen(categoria);
			Conexion.evaluateTrans(trans);
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }
}
