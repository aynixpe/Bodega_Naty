/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import clases.Almacen;
import clases.AlmacenArticulo;
import clases.DetalleMovimiento;
import clases.Kardex;
import clases.Movimiento;
import clases.TipoMovimiento;
import clases.TipoUsuario;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import modelo.Conexion;
import modelo.MdlAlmacenArticulo;
import modelo.MdlDetalleMovimiento;
import modelo.MdlMovimiento;
import modelo.MdlTipoMovimiento;
import utilidades.Excepcion;
import utilidades.SessionException;
import utilidades.Error;

/**
 *
 * @author Edder
 */
public class CtrlMovimiento{
    /**
     *
     * @param movimiento
     * @param cnx
     * @param detalles
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws SessionException
     */
    public static void insert(
    		Movimiento movimiento, boolean trans, ArrayList<DetalleMovimiento> detalles) 
    				throws Excepcion, SQLException, SessionException {
        try{
	    	if (CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO, TipoUsuario.INSERTAR)) {
	        	Kardex kardex;
	            CtrlMovimiento.insert(movimiento, true);
	            for (DetalleMovimiento detalle : detalles) {
	                CtrlMovimiento.insertDetalle(detalle, true);
	                if (movimiento.getAlmacenOrigen() != null) {
	                	int idAlmArtOrigen = MdlAlmacenArticulo.get(
	                            movimiento.getAlmacenOrigen().getIdAlmacen(),
	                            detalle.getArticulo().getIdArticulo()).getIdAlmacenArticulo();
	                    kardex = new Kardex(
	                            -1,
	                            new AlmacenArticulo(){{
	                            	setIdAlmacenArticulo(idAlmArtOrigen);
	                            }},
	                            'E',
	                            detalle.getCantidad(),
	                            -1,
	                            -1,
	                            movimiento.getFecha(),
	                            detalle
	                    );
	                    CtrlKardex.insert(
	                            kardex,
	                            true);
	                }
	                if (movimiento.getAlmacenDestino() != null) {
	                	int idAlmArtDestino = MdlAlmacenArticulo.get(
	                            movimiento.getAlmacenDestino().getIdAlmacen(),
	                            detalle.getArticulo().getIdArticulo()).getIdAlmacenArticulo();
	                    kardex = new Kardex(
	                            -1,
	                            new AlmacenArticulo(){
	                                {
	                                    this.setIdAlmacenArticulo(idAlmArtDestino);
	                                }
	                            },
	                            'I',
	                            detalle.getCantidad(),
	                            -1,
	                            -1,
	                            movimiento.getFecha(),
	                            detalle
	                    );
	                    CtrlKardex.insert(
	                            kardex,
	                            true);
	                }
	            }
	    		Conexion.evaluateTrans(trans);
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para insertar movimiento");
			}
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar Movimiento");
    	}
    }

    /**
     *
     * @param objeto
     * @param cnx
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void insert(Movimiento objeto, boolean trans) 
    		throws Excepcion, SQLException {
        try{
	    	MdlMovimiento.insert(objeto);
	        objeto.setIdMovimiento(MdlMovimiento.getLastId());
			Conexion.evaluateTrans(trans);
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param objeto
     * @param cnx
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public static void insertDetalle(DetalleMovimiento objeto, boolean trans)
    		throws SQLException, Excepcion {
        try{
	    	MdlDetalleMovimiento.insert(objeto);
	        objeto.setIdDetalleMovimiento(MdlDetalleMovimiento.getLastId());
			Conexion.evaluateTrans(trans);
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param objeto
     * @param cnx
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws SessionException
     */
    public static void update(Movimiento objeto, boolean trans)
    		throws SQLException, Excepcion, SessionException {
    	try{
	    	if (CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO, TipoUsuario.EDITAR)) {
	    		MdlMovimiento.update(objeto);
	    		Conexion.evaluateTrans(trans);
	    	}else{
	    		throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para editar movimiento");
	    	}
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param id
     * @param cnx
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws SessionException
     */
    public static Movimiento get(int id, boolean trans) 
    		throws SQLException, Excepcion, SessionException {
    	try{
	    	if (CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO, TipoUsuario.CONSULTAR)) {
	    		Movimiento movimiento = MdlMovimiento.get(id);
	    		Conexion.evaluateTrans(trans);
	            return movimiento;
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar movimientos");
			}
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param idMovimiento
     * @param cnx
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws SessionException
     */
    public static ArrayList<DetalleMovimiento> getDetalleMovimientos_movimiento(
    		int idMovimiento, boolean trans)
    				throws SQLException, Excepcion, SessionException {
    	try{
	    	if (CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO, TipoUsuario.CONSULTAR)) {
	    		ArrayList<DetalleMovimiento> detalleMovimientos = 
	        		   MdlDetalleMovimiento.getDetalleMovimientos_movimiento(idMovimiento);
	   			Conexion.evaluateTrans(trans);
	   			return detalleMovimientos;
	   		}else{
	   			throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar movimientos");
	   		}
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    public static ArrayList<Object[]> getConsulta(
    		String fechaInicio, String fechaFin, Almacen almacen, 
    		TipoMovimiento tipoMovimiento, boolean estado, boolean trans)
    				throws Excepcion, SQLException, SessionException{
    	try{
	    	if (CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO, TipoUsuario.CONSULTAR)) {
	    		ArrayList<Object[]> movimientos = MdlMovimiento.getConsulta(
	    				fechaInicio, fechaFin, almacen, tipoMovimiento, estado);
	    		Conexion.evaluateTrans(trans);
	        	return movimientos;
	        }else{
	     	   throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar movimientos");
	        }
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    public static ArrayList<Object[]> getConsultaDetalles(int movimiento, boolean trans)
    		throws Excepcion, SQLException, SessionException{
    	try{
	    	if (CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO, TipoUsuario.CONSULTAR)) {
	    		ArrayList<Object[]> movimientos = 
	    				MdlDetalleMovimiento.getConsulta(movimiento);
	    		Conexion.evaluateTrans(trans);
	        	return movimientos;
	        }else{
	        	throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar movimientos");
	        }
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    public static void anularMovimiento(int movimiento, boolean trans)
    		throws Excepcion, SessionException, SQLException, Excepcion{
    	try{
	    	if (CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO, TipoUsuario.ELIMINAR)) {
	    		Movimiento mov = CtrlMovimiento.get(movimiento, true);
	    		ArrayList<DetalleMovimiento> detalles = new ArrayList<>();
	    		if (MdlMovimiento.compraMovimiento(movimiento) == false 
	    				&& MdlMovimiento.ventaMovimiento(movimiento) == false) {
	    			detalles = CtrlMovimiento.verificarDetalles(
	    					mov, true);
	    		}
	    		if(detalles.size()!=0){
	    			for(int i = 0; i < detalles.size(); i++){
	    				detalles.get(i).setMovimiento(mov);
	    			}
	    			if(mov.getTipoMovimiento().getIdTipoMovimiento() != 10){
						MdlMovimiento.anularMovimiento(movimiento);
						Almacen almacenOrigen = mov.getAlmacenOrigen();
						LocalDate fecha = (new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
						mov.setAlmacenOrigen(mov.getAlmacenDestino());
						mov.setAlmacenDestino(almacenOrigen);
						mov.setFecha(fecha);
						mov.setTipoMovimiento(new TipoMovimiento(){{this.setIdTipoMovimiento(10);}});
						mov.setEstado('G');
						CtrlMovimiento.insert(mov, true, detalles);
	    			}else{
	    				throw new Excepcion(Error.ERR_ANULACION, "No puede anular este movimiento");
	    			}
				}
	    		Conexion.evaluateTrans(trans);
	    	}else{
	    		throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar movimientos");
	    	}
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    private static ArrayList<DetalleMovimiento> verificarDetalles(
    		Movimiento mov, boolean trans) 
    				throws Excepcion, SQLException, SessionException{
    	try{
	    	ArrayList<DetalleMovimiento> detalles = CtrlMovimiento.getDetalleMovimientos_movimiento(
	    			mov.getIdMovimiento(), true);
			if(mov.getAlmacenDestino() != null){
				for (DetalleMovimiento detalleMovimiento : detalles) {
					if(!CtrlMovimiento.comprobar(detalleMovimiento, mov.getAlmacenDestino(), true)){
						return new ArrayList<DetalleMovimiento>();
					}
					detalleMovimiento.setMovimiento(mov);
				}
			}
			Conexion.evaluateTrans(trans);
			return detalles;
    	}catch(SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    private static boolean comprobar(DetalleMovimiento detalle, Almacen almacen, boolean trans)
    		throws Excepcion, SQLException{
    	try{
	    	int idArticulo = detalle.getArticulo().getIdArticulo();
	    	int cantidad = detalle.getCantidad();
	    	int idAlmacen = almacen.getIdAlmacen();
	    	int stock = CtrlArticulo.getArticuloStock(idArticulo, idAlmacen, true);
	    	if (stock >= cantidad) {
				return true;
			}
	    	return false;
    	}catch(SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    public static ArrayList<TipoMovimiento> getTipoMovimientos(
    		String query, boolean trans) throws Excepcion, SQLException{
        try{
	    	ArrayList<TipoMovimiento> tipoMovimientos = MdlTipoMovimiento.getTipoMovimientos(query);
	        Conexion.evaluateTrans(trans);
	        return tipoMovimientos;
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }
}
