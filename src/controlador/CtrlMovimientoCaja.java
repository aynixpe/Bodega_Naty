package controlador;

import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.Excepcion;
import utilidades.Error;
import utilidades.SessionException;
import modelo.Conexion;
import modelo.MdlDetalleMovimientoCaja;
import modelo.MdlMovimientoCaja;
import modelo.MdlTipoMovimientoCaja;
import clases.Caja;
import clases.DetalleMovimientoCaja;
import clases.FlujoCaja;
import clases.MovimientoCaja;
import clases.TipoMovimientoCaja;
import clases.TipoUsuario;

public class CtrlMovimientoCaja {
	public static void insert(MovimientoCaja movimientoCaja, boolean trans, ArrayList<DetalleMovimientoCaja> detalles) 
			throws Excepcion, SQLException, SessionException{
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO_CAJA, TipoUsuario.INSERTAR)) {
				FlujoCaja flujoCaja;
				MdlMovimientoCaja.insert(movimientoCaja);
				movimientoCaja.setIdMovimientoCaja(MdlMovimientoCaja.getLastId());
				for (DetalleMovimientoCaja detalleMovimientoCaja : detalles) {
					CtrlMovimientoCaja.insertDetalle(detalleMovimientoCaja, true);
					if (movimientoCaja.getCajaOrigen()!=null) {
						flujoCaja = new FlujoCaja(
								-1,
								detalleMovimientoCaja,
								movimientoCaja.getCajaOrigen(),
								'E',
								detalleMovimientoCaja.getMonto(),
								-1,
								-1,
								movimientoCaja.getFecha()
								);
						CtrlFlujoCaja.insert(flujoCaja, true);
					}
					if(movimientoCaja.getCajaDestino()!=null){
						flujoCaja = new FlujoCaja(
								-1,
								detalleMovimientoCaja,
								movimientoCaja.getCajaDestino(),
								'I',
								detalleMovimientoCaja.getMonto(),
								-1,
								-1,
								movimientoCaja.getFecha()
								);
						CtrlFlujoCaja.insert(flujoCaja, true);
					}
				}
				Conexion.evaluateTrans(trans);
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permisos para registrar movimiento caja");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}
	
	public static void insert(MovimientoCaja objeto, boolean trans)
			throws SQLException, Excepcion {
		try{
			MdlMovimientoCaja.insert(objeto);
			objeto.setIdMovimientoCaja(MdlMovimientoCaja.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static void insertDetalle(DetalleMovimientoCaja objeto, boolean trans) 
			throws SQLException, Excepcion{
		try{
			MdlDetalleMovimientoCaja.insert(objeto);
			objeto.setIdDetalleMovimientoCaja(MdlDetalleMovimientoCaja.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	
	public static void update(MovimientoCaja objeto, boolean trans)
			throws SQLException, Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO_CAJA, TipoUsuario.EDITAR)) {
				MdlMovimientoCaja.update(objeto);
				Conexion.evaluateTrans(trans);
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permisos para editar movimiento caja");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	
	public static MovimientoCaja get(int id, boolean trans) throws SQLException,
			Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO_CAJA, TipoUsuario.CONSULTAR)) {
				MovimientoCaja movimientoCaja = MdlMovimientoCaja.get(id);
				Conexion.evaluateTrans(trans);
				return movimientoCaja;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permisos para consultar movimiento caja");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}
	
	public static ArrayList<Object[]> getConsulta(String fechaInicio, String fechaFin, Caja caja, 
			TipoMovimientoCaja tipomovimientocaja, boolean estado, boolean trans) 
					throws Excepcion, SQLException, SessionException{
		try{
			if(CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO_CAJA, TipoUsuario.CONSULTAR)){
				ArrayList<Object[]> movimientos = MdlMovimientoCaja.getConsulta(fechaInicio, fechaFin,caja, tipomovimientocaja, estado);
				Conexion.evaluateTrans(trans);
				return movimientos;
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permisos para consultar movimiento caja");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}
	
	public static ArrayList<Object[]> getConsultaDetalles(int idmovimientocaja, boolean trans) 
			throws Excepcion, SQLException, SessionException{
		try{
			if(CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO_CAJA, TipoUsuario.CONSULTAR)){
				ArrayList<Object[]> detalles = MdlDetalleMovimientoCaja.getConsultaDetalles(idmovimientocaja);
				Conexion.evaluateTrans(trans);
				return detalles;
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permisos para consultar movimiento caja");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static void anularMovimientoCaja(int idMovimientoCaja, boolean trans) 
			throws SessionException, SQLException, Excepcion{
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.MOVIMIENTO_CAJA, TipoUsuario.ELIMINAR)) {
				MovimientoCaja mc = CtrlMovimientoCaja.get(idMovimientoCaja, true);
				ArrayList<DetalleMovimientoCaja> detalles = new ArrayList<DetalleMovimientoCaja>();
				if (!MdlMovimientoCaja.movimientoCajaCompra(idMovimientoCaja) && !MdlMovimientoCaja.movimientoCajaComprobante(idMovimientoCaja)) {
					detalles = CtrlMovimientoCaja.verificarDetalles(mc, true);
					if (detalles.size()!=0) {
						if(mc.getTipoMovimientoCaja().getIdTipoMovimientoCaja() != 6){
							MdlMovimientoCaja.anularMovimientoCaja(idMovimientoCaja);
							Caja caja = mc.getCajaOrigen();
							mc.setCajaOrigen(mc.getCajaDestino());
							mc.setCajaDestino(caja);
							char tipo = MdlTipoMovimientoCaja.get(mc.getTipoMovimientoCaja().getIdTipoMovimientoCaja()).getTipo();
							if(tipo=='E'){
								mc.setTipoMovimientoCaja(new TipoMovimientoCaja(){{setIdTipoMovimientoCaja(4);}});
							}else{
								mc.setTipoMovimientoCaja(new TipoMovimientoCaja(){{setIdTipoMovimientoCaja(5);}});
							}
							mc.setTipoMovimientoCaja(new TipoMovimientoCaja(){{setIdTipoMovimientoCaja(6);}});
							CtrlMovimientoCaja.insert(mc, true, detalles);
						}else{
							throw new Excepcion(Error.ERR_ANULACION, "No puede anular este movimiento");
						}
					}
				}			
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permisos para consultar movimiento caja");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	private static ArrayList<DetalleMovimientoCaja> getDetalles_MovimientoCaja(int idMovimientoCaja, boolean trans)
			throws SQLException, Excepcion{
		try{
			ArrayList<DetalleMovimientoCaja> detalles = MdlDetalleMovimientoCaja.getDetalles_MovimientCaja(
					idMovimientoCaja);
			Conexion.evaluateTrans(trans);
			return detalles;
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	private static ArrayList<DetalleMovimientoCaja> verificarDetalles(MovimientoCaja movimientocaja, boolean trans) 
			throws Excepcion, SQLException, SessionException{
		try{
			ArrayList<DetalleMovimientoCaja> detalles = CtrlMovimientoCaja.getDetalles_MovimientoCaja(
					movimientocaja.getIdMovimientoCaja(), true);
			if (movimientocaja.getCajaDestino()!=null && movimientocaja.getCajaDestino().getIdCaja()!=0) {
				for (DetalleMovimientoCaja detalleMovimientoCaja : detalles) {
					if (!CtrlMovimientoCaja.comparar(detalleMovimientoCaja, 
							CtrlCaja.get(movimientocaja.getCajaDestino().getIdCaja(), true))) {
						return new ArrayList<DetalleMovimientoCaja>();
					}
					detalleMovimientoCaja.setMovimientoCaja(movimientocaja);
				}
			}
			Conexion.evaluateTrans(trans);
			return detalles;
		}catch(SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	private static boolean comparar(DetalleMovimientoCaja detalleMovimientoCaja, Caja caja){
		if (caja.getMonto() >= detalleMovimientoCaja.getMonto()) 
			return true;
		return false;
	}
	
	public static ArrayList<TipoMovimientoCaja> getTipoMovimientosCaja(String query, boolean trans) 
			throws Excepcion, SQLException{
		try{
			ArrayList<TipoMovimientoCaja> tipos = new ArrayList<TipoMovimientoCaja>();
			tipos = MdlTipoMovimientoCaja.getTipoMovimientosCaja(query);
			Conexion.evaluateTrans(trans);
			return tipos;
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

}
