package controlador;

import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.Excepcion;
import modelo.Conexion;
import modelo.MdlTipoUsuario;
import modelo.MdlUsuario;
import clases.Usuario;
import clases.TipoUsuario;

public class CtrlUsuario{
	public static void insert(Usuario usuario, boolean trans) throws SQLException,
			Excepcion {
		try{
			MdlUsuario.insert(usuario);
			usuario.setIdUsuario(MdlUsuario.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}

	public static void update(Usuario objeto, boolean trans) throws SQLException,
			ClassNotFoundException {
	}

	public static Usuario get(int id, boolean trans) throws SQLException,
			ClassNotFoundException {
		return null;
	}
	
	public static ArrayList<Usuario> getUsuarios(boolean trans) throws Excepcion, SQLException{
		try{
			ArrayList<Usuario> usuarios = MdlUsuario.getUsuarios("");
			Conexion.evaluateTrans(trans);
			return usuarios;
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}
	
	public static ArrayList<Object[]> getUsers(boolean trans) throws SQLException, Excepcion{
		try{
			ArrayList<Object[]> usuarios = MdlUsuario.getUsers();
			Conexion.evaluateTrans(trans);
			return usuarios;
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}
	
	/**
	 * Devuelve lista de tipos de usuario
	 * @param cnx
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ArrayList<TipoUsuario> getTipos(boolean trans) throws Excepcion, 
			SQLException{
		try{
			ArrayList<TipoUsuario> tipos = MdlTipoUsuario.get();
			Conexion.evaluateTrans(trans);
			return tipos;
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}
	
	/**
	 * Devuelve un tipo de usuario según id
	 * @param id
	 * @param cnx
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static TipoUsuario getTipo(int id, boolean trans) throws Excepcion, 
			SQLException{
		try{
			TipoUsuario tipo = MdlTipoUsuario.get(id);
			Conexion.evaluateTrans(trans);
			return tipo;
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}
	
	public static int getPermiso(int permiso, int idusuario, boolean trans) 
			throws Excepcion, SQLException{
		try{
			int p = MdlUsuario.get_Permiso(permiso, idusuario);
			Conexion.evaluateTrans(trans);
			return p;
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}
	
	/**
	 * Devuelve un usuario que coincida con usuario y contraseña, no tiene tipoUsuario
	 * @param user
	 * @param pass
	 * @param cnx
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static Usuario get(String user, String pass, boolean trans) throws SQLException, Excepcion{
		try{
			Usuario usuario = MdlUsuario.get(user, pass);
			Conexion.evaluateTrans(trans);
			return usuario;
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}
	
	public static void updateTipo(TipoUsuario tipoUsuario, boolean trans) 
			throws Excepcion, SQLException{
		try{
			MdlTipoUsuario.update(tipoUsuario);
			Conexion.evaluateTrans(trans);
		}catch(SQLException | ClassNotFoundException ex){
			if(!trans)
				Conexion.rollback();
			throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar usuario");
		}
	}

}
