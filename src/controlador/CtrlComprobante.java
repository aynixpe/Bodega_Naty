package controlador;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import utilidades.Configuracion;
import utilidades.Excepcion;
import utilidades.Error;
import utilidades.SessionException;
import modelo.Conexion;
import modelo.MdlComprobante;
import modelo.MdlDetalleComprobante;
import modelo.MdlMovimientoCaja;
import modelo.MdlTipoComprobante;
import clases.Articulo;
import clases.Caja;
import clases.Comprobante;
import clases.DetalleComprobante;
import clases.DetalleMovimientoCaja;
import clases.DetalleVenta;
import clases.MovimientoCaja;
import clases.Serie;
import clases.TipoComprobante;
import clases.TipoMovimientoCaja;
import clases.TipoUsuario;

public class CtrlComprobante{

	public static void insert(boolean resumen, LocalDate fecha, 
			String cliente, Serie serie, ArrayList<DetalleVenta> detallesVenta, boolean trans)
	 		throws SQLException, Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.COMPROBANTE, TipoUsuario.INSERTAR)) {
				float monto = 0f;
				int n = 0;
				int nItems = 10;
				for(DetalleVenta detalle : detallesVenta){
					monto += (detalle.getPrecio() * detalle.getCantidad() - detalle.getDescuento()) 
							/ (1 + Configuracion.getIgv());
				}
				ArrayList<Comprobante> comprobantes = new ArrayList<>();
				ArrayList<DetalleMovimientoCaja> detallesMovimientoCaja = new ArrayList<DetalleMovimientoCaja>();
				ArrayList<DetalleComprobante> detallesComprobante = new ArrayList<>();
				MovimientoCaja movimientoCaja = new MovimientoCaja(
						0,
						new TipoMovimientoCaja(){{setIdTipoMovimientoCaja(2);}},
						2, //referencia: egreso por compra
						fecha,
						monto * Configuracion.getIgv(),
						CtrlSesion.usuario,
						'G',
						//configurar almacenes
						null,
						CtrlSesion.caja
						);
				comprobantes.add(CtrlComprobante.generarComprobante(resumen, fecha, 
						cliente, serie, movimientoCaja));
				
				for(DetalleVenta detalle : detallesVenta){
					if(n >= nItems && !resumen){
						n = 0;
						comprobantes.add(CtrlComprobante.generarComprobante(resumen, fecha, 
								cliente, serie, movimientoCaja));
					}
					DetalleComprobante detalleComprobante = new DetalleComprobante();
					int idArticulo = CtrlArticulo.getIdArticulo(detalle.getAlmacenArticulo().getIdAlmacenArticulo(), false);
					Articulo articulo = CtrlArticulo.get(idArticulo, false);
					float precio = (detalle.getPrecio() * detalle.getCantidad() - detalle.getDescuento()) / 
							(1 + Configuracion.getIgv());
					float igv = precio * Configuracion.getIgv();
					detalleComprobante.setArticulo(articulo);
					detalleComprobante.setCantidad(detalle.getCantidad());
					detalleComprobante.setIgv(igv);
					detalleComprobante.setPrecio(precio);
					Comprobante comp = comprobantes.get(comprobantes.size()-1);
					detalleComprobante.setComprobante(comp);
					comp.setMonto( comp.getMonto() + precio);
					comp.setIgv( comp.getIgv() + igv);
					detallesComprobante.add(detalleComprobante);
					detallesMovimientoCaja.add(
							new DetalleMovimientoCaja(-1, movimientoCaja,
							precio + igv,
							"Pago por Venta"));
					detalle.setDetalleComprobante(detalleComprobante);
					n++;
				}
				
				CtrlMovimientoCaja.insert(movimientoCaja, true, detallesMovimientoCaja);
				movimientoCaja.setIdMovimientoCaja(MdlMovimientoCaja.getLastId());
				for(Comprobante comprobante : comprobantes){
					comprobante.setMovimientoCaja(movimientoCaja);					
					comprobante.setNumero(CtrlComprobante.LastNumero_Serie(serie.getIdSerie(), true) + 1);				
					CtrlComprobante.insert(comprobante, true);
				}
				for (DetalleComprobante detalleComprobante : detallesComprobante) {
					CtrlComprobante.insertDetalle(detalleComprobante, true);
					detalleComprobante.setIdDetalleComprobante(MdlDetalleComprobante.getLastId());
				}
				for(DetalleVenta detalleVenta : detallesVenta){
					CtrlVenta.setDetalleComprobante(detalleVenta, true);
					CtrlVenta.procesarVenta(detalleVenta.getVenta().getIdVenta(), true);
				}
				Conexion.evaluateTrans(trans);
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para registrar comprobante");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	
	public static void insert(Comprobante comprobante, boolean trans) 
			throws SQLException, Excepcion{
		try{
			MdlComprobante.insert(comprobante);
			comprobante.setIdComprobante(MdlComprobante.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}
	
	public static Comprobante generarComprobante(boolean resumen, LocalDate fecha, 
			String cliente, Serie serie, MovimientoCaja movCaja){
		return new Comprobante(){{
			setIdComprobante(-1);
			setFecha(fecha);
			setMonto(0);
			setNombreCliente(cliente);		
			setSerie(serie);
			setIgv(0);
			setMovimientoCaja(new MovimientoCaja(){{setIdMovimientoCaja(-1);}});
			setUsuario(CtrlSesion.usuario);
			if(resumen)
				setDescripcion("Por consumo");
			else
				setDescripcion("");
			setEstado('G');
		}};
	}

	public static void insertDetalle(DetalleComprobante detalle, boolean trans) 
			throws SQLException, Excepcion{
		try{
			MdlDetalleComprobante.insert(detalle);
			detalle.setIdDetalleComprobante(MdlDetalleComprobante.getLastId());
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static void setMovimiento(Comprobante comprobante, boolean trans) 
			throws Excepcion, SQLException{
		try{
			MdlComprobante.setMovimiento(comprobante);
			Conexion.evaluateTrans(trans);
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	/*
	 * Devuelve el ultimo numero de la serie especificada
	 */
	public static int LastNumero_Serie(int idSerie, boolean trans) 
			throws SQLException, Excepcion, SessionException{
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.COMPROBANTE, TipoUsuario.CONSULTAR)) {
				int numero = MdlComprobante.getLastNumero_serie(idSerie);
				Conexion.evaluateTrans(trans);
				return numero;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar comprobante");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static void update(Comprobante objeto, boolean trans) 
			throws SQLException, Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.COMPROBANTE, TipoUsuario.EDITAR)) {
				MdlComprobante.update(objeto);
				Conexion.evaluateTrans(trans);
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para editar comprobante");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static Comprobante get(int id, boolean trans)
			throws SQLException, Excepcion, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.COMPROBANTE, TipoUsuario.CONSULTAR)) {
				Comprobante comprobante = MdlComprobante.get(id);
				Conexion.evaluateTrans(trans);
				return comprobante;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar comprobante");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static ArrayList<Object[]> getConsulta(String fechaInicio, String fechaFin, String cliente, Caja caja, boolean trans)
			throws Excepcion, SQLException, SessionException{
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.COMPROBANTE, TipoUsuario.CONSULTAR)) {
				ArrayList<Object[]> comprobantes = MdlComprobante.getConsulta(
						fechaInicio, fechaFin, cliente, caja);
				Conexion.evaluateTrans(trans);
				return comprobantes;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar comprobante");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static ArrayList<Object[]> getConsulta_Detalle(int idComprobante, boolean trans)
			throws Excepcion, SQLException, SessionException {
		try{
			if (CtrlSesion.validarPermiso(TipoUsuario.COMPROBANTE, TipoUsuario.CONSULTAR)) {
				ArrayList<Object[]> detalles = MdlDetalleComprobante.getConsulta_Detalle(
						idComprobante);
				Conexion.evaluateTrans(trans);
				return detalles;
			} else {
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para consultar comprobante");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static void anularComprobante(int idComprobante, boolean trans) throws Excepcion, SQLException, SessionException, Excepcion{
		try{
			if(CtrlSesion.validarPermiso(TipoUsuario.COMPROBANTE, TipoUsuario.ELIMINAR)){
				Comprobante comprobante = CtrlComprobante.get(idComprobante, true);
				MdlComprobante.anularComprobante(idComprobante);
				CtrlMovimientoCaja.anularMovimientoCaja(
						comprobante.getMovimientoCaja().getIdMovimientoCaja(), true);
				Conexion.evaluateTrans(trans);
			}else{
				throw new SessionException(Error.ERR_PERMISO, "No tiene permiso para anular comprobante");
			}
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

	public static ArrayList<TipoComprobante> getTiposComprobante(
			String query, boolean trans) throws Excepcion, SQLException{
		try{
			ArrayList<TipoComprobante> tipos = MdlTipoComprobante.get(query);
			Conexion.evaluateTrans(trans);
			return tipos;
		}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
	}

}
