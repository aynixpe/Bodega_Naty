/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import clases.Almacen;
import clases.AlmacenArticulo;
import clases.Articulo;
import clases.Kardex;
import clases.DetalleMovimiento;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import utilidades.Excepcion;
import modelo.Conexion;
import modelo.MdlKardex;

/**
 *
 * @author Edder
 */
public class CtrlKardex{
    /**
     *
     * @param objeto
     * @param cnx
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void insert(Kardex objeto, boolean trans)
    		throws SQLException, Excepcion {
        try{
	    	int variacion = (objeto.getMovimiento() == 'E')? objeto.getCantidad() * -1: objeto.getCantidad();
	        int stockActual = CtrlArticulo.getAlmacenArticulo(
	        		objeto.getAlmacenArticulo().getIdAlmacenArticulo(), true).getStock();
	        objeto.setStockBefore(stockActual);
	        objeto.setStockAfter(stockActual + variacion);
	        MdlKardex.insert(objeto);
	        objeto.setIdKardex(MdlKardex.getLastId());
	        CtrlArticulo.updateStock(
	        		objeto.getAlmacenArticulo().getIdAlmacenArticulo(), variacion, true);
			Conexion.evaluateTrans(trans);
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    public static void insert(
    		int idAlmacenArticulo, char movimiento, int cantidad, int stockBefore, 
    		int stockAfter, LocalDate fecha, DetalleMovimiento detalleMovimiento, boolean trans)
    				throws SQLException, Excepcion {
        try{
	    	Kardex kardex = new Kardex(
	                -1,
	                new AlmacenArticulo(){{
	                	setIdAlmacenArticulo(idAlmacenArticulo);
	                }},
	                movimiento,
	                cantidad,
	                stockBefore,
	                stockAfter,
	                fecha,
	                detalleMovimiento
	                );
	        CtrlKardex.insert(kardex, true);
			Conexion.evaluateTrans(trans);
        }catch(SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param objeto
     * @param cnx
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void update(Kardex objeto, boolean trans) 
    		throws SQLException, Excepcion {
        try{
	    	MdlKardex.update(objeto);
			Conexion.evaluateTrans(trans);
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    /**
     *
     * @param id
     * @param cnx
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static Kardex get(int id, boolean trans) 
    		throws SQLException, Excepcion {
        try{
	    	Kardex kardex = MdlKardex.get(id);
			Conexion.evaluateTrans(trans);
	        return kardex;
        }catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }

    public static ArrayList<Object[]> getConsulta(
    		String fechaInicio, String fechaFin, Articulo articulo, Almacen almacen,
    		boolean trans) throws Excepcion, SQLException{
    	try{
	    	ArrayList<Object[]> kardex = MdlKardex.getConsulta(
	    			fechaInicio, fechaFin, articulo, almacen);
			Conexion.evaluateTrans(trans);
	    	return kardex;
    	}catch(ClassNotFoundException | SQLException ex){
    		if(!trans)
    			Conexion.rollback();
    		throw new Excepcion(utilidades.Error.ERR_CONEXION, "Insertar almacén");
    	}
    }
}
