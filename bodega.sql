--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: almacen; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE almacen (
    idalmacen integer NOT NULL,
    nombre character varying(45) NOT NULL,
    estado character(1) NOT NULL
);


ALTER TABLE almacen OWNER TO postgres;

--
-- Name: almacen_idalmacen_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE almacen_idalmacen_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE almacen_idalmacen_seq OWNER TO postgres;

--
-- Name: almacen_idalmacen_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE almacen_idalmacen_seq OWNED BY almacen.idalmacen;


--
-- Name: almacenarticulo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE almacenarticulo (
    idalmacenarticulo integer NOT NULL,
    idalmacen integer NOT NULL,
    idarticulo integer NOT NULL,
    stock integer NOT NULL,
    stockminimo integer NOT NULL
);


ALTER TABLE almacenarticulo OWNER TO postgres;

--
-- Name: almacenarticulo_idalmacenarticulo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE almacenarticulo_idalmacenarticulo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE almacenarticulo_idalmacenarticulo_seq OWNER TO postgres;

--
-- Name: almacenarticulo_idalmacenarticulo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE almacenarticulo_idalmacenarticulo_seq OWNED BY almacenarticulo.idalmacenarticulo;


--
-- Name: articulo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE articulo (
    idarticulo integer NOT NULL,
    nombre character varying(45) NOT NULL,
    presentacion character varying(45) DEFAULT NULL::character varying,
    descripcion character varying(45) DEFAULT NULL::character varying,
    stockminimo integer,
    tamanio character varying(45) DEFAULT NULL::character varying,
    unidad character varying(45) DEFAULT NULL::character varying,
    codigobarras character varying(45) DEFAULT NULL::character varying,
    idcategoria integer,
    precioventa numeric(4,2) NOT NULL,
    estado character(1) DEFAULT NULL::bpchar
);


ALTER TABLE articulo OWNER TO postgres;

--
-- Name: articulo_idarticulo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE articulo_idarticulo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE articulo_idarticulo_seq OWNER TO postgres;

--
-- Name: articulo_idarticulo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE articulo_idarticulo_seq OWNED BY articulo.idarticulo;


--
-- Name: caja; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE caja (
    idcaja integer NOT NULL,
    nombre character varying(50) NOT NULL,
    monto numeric(10,2) NOT NULL
);


ALTER TABLE caja OWNER TO postgres;

--
-- Name: caja_idcaja_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE caja_idcaja_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE caja_idcaja_seq OWNER TO postgres;

--
-- Name: caja_idcaja_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE caja_idcaja_seq OWNED BY caja.idcaja;


--
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categoria (
    idcategoria integer NOT NULL,
    nombre character varying(45) NOT NULL,
    imagen bytea
);


ALTER TABLE categoria OWNER TO postgres;

--
-- Name: categoria_idcategoria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categoria_idcategoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categoria_idcategoria_seq OWNER TO postgres;

--
-- Name: categoria_idcategoria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categoria_idcategoria_seq OWNED BY categoria.idcategoria;


--
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cliente (
    idcliente integer NOT NULL,
    nombre character varying(45) NOT NULL,
    dni character(8) NOT NULL
);


ALTER TABLE cliente OWNER TO postgres;

--
-- Name: cliente_idcliente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cliente_idcliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cliente_idcliente_seq OWNER TO postgres;

--
-- Name: cliente_idcliente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cliente_idcliente_seq OWNED BY cliente.idcliente;


--
-- Name: compra; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE compra (
    idcompra integer NOT NULL,
    fecha date NOT NULL,
    monto numeric(10,2) NOT NULL,
    igv numeric(10,2) NOT NULL,
    idordencompra integer,
    idproveedor integer,
    idusuario integer NOT NULL,
    idmovimientocaja integer,
    estado character(1) NOT NULL,
    factura character varying(20) NOT NULL,
    idmovimiento integer
);


ALTER TABLE compra OWNER TO postgres;

--
-- Name: compra_idcompra_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE compra_idcompra_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE compra_idcompra_seq OWNER TO postgres;

--
-- Name: compra_idcompra_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE compra_idcompra_seq OWNED BY compra.idcompra;


--
-- Name: comprobante; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comprobante (
    idcomprobante integer NOT NULL,
    idserie integer NOT NULL,
    numero integer NOT NULL,
    nombrecliente character varying(45) DEFAULT NULL::character varying,
    fecha date NOT NULL,
    monto numeric(10,2) NOT NULL,
    igv numeric(10,2) NOT NULL,
    idmovimientocaja integer,
    idusuario integer NOT NULL,
    estado character(1) NOT NULL,
    descripcion character varying(100)
);


ALTER TABLE comprobante OWNER TO postgres;

--
-- Name: comprobante_idcomprobante_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE comprobante_idcomprobante_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE comprobante_idcomprobante_seq OWNER TO postgres;

--
-- Name: comprobante_idcomprobante_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE comprobante_idcomprobante_seq OWNED BY comprobante.idcomprobante;


--
-- Name: detallecompra; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE detallecompra (
    iddetallecompra integer NOT NULL,
    idcompra integer NOT NULL,
    idalmacenarticulo integer NOT NULL,
    cantidad integer NOT NULL,
    precio numeric(10,2) NOT NULL,
    total numeric(10,2) NOT NULL,
    igv numeric(10,2) NOT NULL
);


ALTER TABLE detallecompra OWNER TO postgres;

--
-- Name: detallecompra_iddetallecompra_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE detallecompra_iddetallecompra_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE detallecompra_iddetallecompra_seq OWNER TO postgres;

--
-- Name: detallecompra_iddetallecompra_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE detallecompra_iddetallecompra_seq OWNED BY detallecompra.iddetallecompra;


--
-- Name: detallecomprobante; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE detallecomprobante (
    iddetallecomprobante integer NOT NULL,
    idcomprobante integer NOT NULL,
    idarticulo integer NOT NULL,
    cantidad integer NOT NULL,
    precio numeric(10,2) NOT NULL,
    igv numeric(10,2) NOT NULL
);


ALTER TABLE detallecomprobante OWNER TO postgres;

--
-- Name: detallecomprobante_iddetallecomprobante_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE detallecomprobante_iddetallecomprobante_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE detallecomprobante_iddetallecomprobante_seq OWNER TO postgres;

--
-- Name: detallecomprobante_iddetallecomprobante_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE detallecomprobante_iddetallecomprobante_seq OWNED BY detallecomprobante.iddetallecomprobante;


--
-- Name: detallemovimiento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE detallemovimiento (
    iddetallemovimiento integer NOT NULL,
    idmovimiento integer NOT NULL,
    idarticulo integer NOT NULL,
    cantidad integer NOT NULL
);


ALTER TABLE detallemovimiento OWNER TO postgres;

--
-- Name: detallemovimiento_iddetallemovimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE detallemovimiento_iddetallemovimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE detallemovimiento_iddetallemovimiento_seq OWNER TO postgres;

--
-- Name: detallemovimiento_iddetallemovimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE detallemovimiento_iddetallemovimiento_seq OWNED BY detallemovimiento.iddetallemovimiento;


--
-- Name: detallemovimientocaja; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE detallemovimientocaja (
    iddetallemovimientocaja integer NOT NULL,
    idmovimientocaja integer NOT NULL,
    monto numeric(10,2) NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE detallemovimientocaja OWNER TO postgres;

--
-- Name: detallemovimientocaja_iddetallemovimientocaja_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE detallemovimientocaja_iddetallemovimientocaja_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE detallemovimientocaja_iddetallemovimientocaja_seq OWNER TO postgres;

--
-- Name: detallemovimientocaja_iddetallemovimientocaja_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE detallemovimientocaja_iddetallemovimientocaja_seq OWNED BY detallemovimientocaja.iddetallemovimientocaja;


--
-- Name: detalleordencompra; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE detalleordencompra (
    iddetalleordencompra integer NOT NULL,
    idordencompra integer NOT NULL,
    idarticulo integer NOT NULL,
    cantidad integer NOT NULL,
    precio numeric(10,2) NOT NULL,
    total numeric(10,2) NOT NULL
);


ALTER TABLE detalleordencompra OWNER TO postgres;

--
-- Name: detalleordencompra_iddetalleordencompra_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE detalleordencompra_iddetalleordencompra_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE detalleordencompra_iddetalleordencompra_seq OWNER TO postgres;

--
-- Name: detalleordencompra_iddetalleordencompra_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE detalleordencompra_iddetalleordencompra_seq OWNED BY detalleordencompra.iddetalleordencompra;


--
-- Name: detalleventa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE detalleventa (
    iddetalleventa integer NOT NULL,
    idventa integer NOT NULL,
    idalmacenarticulo integer NOT NULL,
    cantidad integer NOT NULL,
    precio numeric(10,2) NOT NULL,
    descuento numeric(10,2) NOT NULL,
    iddetallecomprobante integer
);


ALTER TABLE detalleventa OWNER TO postgres;

--
-- Name: detalleventa_iddetalleventa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE detalleventa_iddetalleventa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE detalleventa_iddetalleventa_seq OWNER TO postgres;

--
-- Name: detalleventa_iddetalleventa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE detalleventa_iddetalleventa_seq OWNED BY detalleventa.iddetalleventa;


--
-- Name: flujocaja; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE flujocaja (
    idflujocaja integer NOT NULL,
    iddetallemovimientocaja integer NOT NULL,
    idcaja integer NOT NULL,
    movimiento character(1) NOT NULL,
    monto double precision NOT NULL,
    montobefore double precision NOT NULL,
    montoafter double precision NOT NULL,
    fecha date NOT NULL
);


ALTER TABLE flujocaja OWNER TO postgres;

--
-- Name: flujocaja_idflujocaja_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE flujocaja_idflujocaja_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE flujocaja_idflujocaja_seq OWNER TO postgres;

--
-- Name: flujocaja_idflujocaja_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE flujocaja_idflujocaja_seq OWNED BY flujocaja.idflujocaja;


--
-- Name: kardex; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kardex (
    idkardex integer NOT NULL,
    idalmacenarticulo integer NOT NULL,
    movimiento character(1) NOT NULL,
    cantidad integer NOT NULL,
    fecha date NOT NULL,
    stockbefore integer NOT NULL,
    stockafter integer NOT NULL,
    iddetallemovimiento integer NOT NULL
);


ALTER TABLE kardex OWNER TO postgres;

--
-- Name: kardex_idkardex_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kardex_idkardex_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE kardex_idkardex_seq OWNER TO postgres;

--
-- Name: kardex_idkardex_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kardex_idkardex_seq OWNED BY kardex.idkardex;


--
-- Name: lote; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE lote (
    idlote integer NOT NULL,
    idarticulo integer NOT NULL,
    fechavencimiento date,
    lote date,
    estado character(1) NOT NULL
);


ALTER TABLE lote OWNER TO postgres;

--
-- Name: lote_idlote_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE lote_idlote_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE lote_idlote_seq OWNER TO postgres;

--
-- Name: lote_idlote_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE lote_idlote_seq OWNED BY lote.idlote;


--
-- Name: movimiento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE movimiento (
    idmovimiento integer NOT NULL,
    idtipomovimiento integer NOT NULL,
    fecha date NOT NULL,
    idalmacenorigen integer,
    idalmacendestino integer,
    estado character(1) NOT NULL
);


ALTER TABLE movimiento OWNER TO postgres;

--
-- Name: movimiento_idmovimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE movimiento_idmovimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE movimiento_idmovimiento_seq OWNER TO postgres;

--
-- Name: movimiento_idmovimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE movimiento_idmovimiento_seq OWNED BY movimiento.idmovimiento;


--
-- Name: movimientocaja; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE movimientocaja (
    idmovimientocaja integer NOT NULL,
    idtipomovimientocaja integer NOT NULL,
    referencia integer NOT NULL,
    fecha date NOT NULL,
    monto numeric(10,2) NOT NULL,
    idusuario integer NOT NULL,
    estado character(1) NOT NULL,
    idcajaorigen integer,
    idcajadestino integer
);


ALTER TABLE movimientocaja OWNER TO postgres;

--
-- Name: movimientocaja_idmovimientocaja_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE movimientocaja_idmovimientocaja_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE movimientocaja_idmovimientocaja_seq OWNER TO postgres;

--
-- Name: movimientocaja_idmovimientocaja_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE movimientocaja_idmovimientocaja_seq OWNED BY movimientocaja.idmovimientocaja;


--
-- Name: ordencompra; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ordencompra (
    idordencompra integer NOT NULL,
    fecha date NOT NULL,
    monto numeric(10,2) NOT NULL,
    fechaentrega date,
    idproveedor integer NOT NULL,
    idusuario integer NOT NULL,
    estado character(1) NOT NULL
);


ALTER TABLE ordencompra OWNER TO postgres;

--
-- Name: ordencompra_idordencompra_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ordencompra_idordencompra_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ordencompra_idordencompra_seq OWNER TO postgres;

--
-- Name: ordencompra_idordencompra_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ordencompra_idordencompra_seq OWNED BY ordencompra.idordencompra;


--
-- Name: pago; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pago (
    idpago integer NOT NULL,
    idtipopago integer NOT NULL,
    monto numeric(10,2) NOT NULL,
    estado character(1) NOT NULL,
    idcomprobante integer NOT NULL
);


ALTER TABLE pago OWNER TO postgres;

--
-- Name: pago_idpago_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pago_idpago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pago_idpago_seq OWNER TO postgres;

--
-- Name: pago_idpago_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pago_idpago_seq OWNED BY pago.idpago;


--
-- Name: proveedor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE proveedor (
    idproveedor integer NOT NULL,
    nombre character varying(45) NOT NULL,
    preventa character varying(45) DEFAULT NULL::character varying,
    mercaderista character varying(45) DEFAULT NULL::character varying,
    direccion character varying(45) DEFAULT NULL::character varying,
    telefono character varying(45) DEFAULT NULL::character varying,
    descripcion character varying(45) DEFAULT NULL::character varying
);


ALTER TABLE proveedor OWNER TO postgres;

--
-- Name: proveedor_idproveedor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE proveedor_idproveedor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE proveedor_idproveedor_seq OWNER TO postgres;

--
-- Name: proveedor_idproveedor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE proveedor_idproveedor_seq OWNED BY proveedor.idproveedor;


--
-- Name: serie; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE serie (
    idserie integer NOT NULL,
    idtipocomprobante integer NOT NULL,
    nombre character varying(45) NOT NULL
);


ALTER TABLE serie OWNER TO postgres;

--
-- Name: serie_idserie_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE serie_idserie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE serie_idserie_seq OWNER TO postgres;

--
-- Name: serie_idserie_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE serie_idserie_seq OWNED BY serie.idserie;


--
-- Name: tipocomprobante; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipocomprobante (
    idtipocomprobante integer NOT NULL,
    nombre character varying(45) NOT NULL
);


ALTER TABLE tipocomprobante OWNER TO postgres;

--
-- Name: tipocomprobante_idtipocomprobante_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipocomprobante_idtipocomprobante_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipocomprobante_idtipocomprobante_seq OWNER TO postgres;

--
-- Name: tipocomprobante_idtipocomprobante_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipocomprobante_idtipocomprobante_seq OWNED BY tipocomprobante.idtipocomprobante;


--
-- Name: tipomovimiento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipomovimiento (
    idtipomovimiento integer NOT NULL,
    tipo character(1) NOT NULL,
    nombre character varying(45) NOT NULL
);


ALTER TABLE tipomovimiento OWNER TO postgres;

--
-- Name: tipomovimiento_idtipomovimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipomovimiento_idtipomovimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipomovimiento_idtipomovimiento_seq OWNER TO postgres;

--
-- Name: tipomovimiento_idtipomovimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipomovimiento_idtipomovimiento_seq OWNED BY tipomovimiento.idtipomovimiento;


--
-- Name: tipomovimientocaja; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipomovimientocaja (
    idtipomovimientocaja integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    tipo character(1) NOT NULL
);


ALTER TABLE tipomovimientocaja OWNER TO postgres;

--
-- Name: tipomovimientocaja_idtipomovimientocaja_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipomovimientocaja_idtipomovimientocaja_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipomovimientocaja_idtipomovimientocaja_seq OWNER TO postgres;

--
-- Name: tipomovimientocaja_idtipomovimientocaja_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipomovimientocaja_idtipomovimientocaja_seq OWNED BY tipomovimientocaja.idtipomovimientocaja;


--
-- Name: tipopago; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipopago (
    idtipopago integer NOT NULL,
    nombre character varying(45) NOT NULL
);


ALTER TABLE tipopago OWNER TO postgres;

--
-- Name: tipopago_idtipopago_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipopago_idtipopago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipopago_idtipopago_seq OWNER TO postgres;

--
-- Name: tipopago_idtipopago_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipopago_idtipopago_seq OWNED BY tipopago.idtipopago;


--
-- Name: tipousuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipousuario (
    idtipousuario integer NOT NULL,
    nombre character varying(45) NOT NULL,
    p_movimiento integer NOT NULL,
    p_venta integer NOT NULL,
    p_comprobante integer NOT NULL,
    p_ordencompra integer NOT NULL,
    p_compra integer NOT NULL,
    p_detallemovimientocaja integer NOT NULL
);


ALTER TABLE tipousuario OWNER TO postgres;

--
-- Name: tipousuario_idtipousuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipousuario_idtipousuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipousuario_idtipousuario_seq OWNER TO postgres;

--
-- Name: tipousuario_idtipousuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipousuario_idtipousuario_seq OWNED BY tipousuario.idtipousuario;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    idusuario integer NOT NULL,
    idtipousuario integer NOT NULL,
    nombre character varying(45) NOT NULL,
    usuario character varying(50) NOT NULL,
    password character varying(32) NOT NULL
);


ALTER TABLE usuario OWNER TO postgres;

--
-- Name: usuario_idusuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuario_idusuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario_idusuario_seq OWNER TO postgres;

--
-- Name: usuario_idusuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuario_idusuario_seq OWNED BY usuario.idusuario;


--
-- Name: venta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE venta (
    idventa integer NOT NULL,
    fecha date NOT NULL,
    idcliente integer,
    monto numeric(10,2) NOT NULL,
    idusuario integer NOT NULL,
    estado character(1) NOT NULL,
    idmovimiento integer NOT NULL
);


ALTER TABLE venta OWNER TO postgres;

--
-- Name: venta_idventa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE venta_idventa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE venta_idventa_seq OWNER TO postgres;

--
-- Name: venta_idventa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE venta_idventa_seq OWNED BY venta.idventa;


--
-- Name: idalmacen; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY almacen ALTER COLUMN idalmacen SET DEFAULT nextval('almacen_idalmacen_seq'::regclass);


--
-- Name: idalmacenarticulo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY almacenarticulo ALTER COLUMN idalmacenarticulo SET DEFAULT nextval('almacenarticulo_idalmacenarticulo_seq'::regclass);


--
-- Name: idarticulo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY articulo ALTER COLUMN idarticulo SET DEFAULT nextval('articulo_idarticulo_seq'::regclass);


--
-- Name: idcaja; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY caja ALTER COLUMN idcaja SET DEFAULT nextval('caja_idcaja_seq'::regclass);


--
-- Name: idcategoria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria ALTER COLUMN idcategoria SET DEFAULT nextval('categoria_idcategoria_seq'::regclass);


--
-- Name: idcliente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cliente ALTER COLUMN idcliente SET DEFAULT nextval('cliente_idcliente_seq'::regclass);


--
-- Name: idcompra; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY compra ALTER COLUMN idcompra SET DEFAULT nextval('compra_idcompra_seq'::regclass);


--
-- Name: idcomprobante; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comprobante ALTER COLUMN idcomprobante SET DEFAULT nextval('comprobante_idcomprobante_seq'::regclass);


--
-- Name: iddetallecompra; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detallecompra ALTER COLUMN iddetallecompra SET DEFAULT nextval('detallecompra_iddetallecompra_seq'::regclass);


--
-- Name: iddetallecomprobante; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detallecomprobante ALTER COLUMN iddetallecomprobante SET DEFAULT nextval('detallecomprobante_iddetallecomprobante_seq'::regclass);


--
-- Name: iddetallemovimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detallemovimiento ALTER COLUMN iddetallemovimiento SET DEFAULT nextval('detallemovimiento_iddetallemovimiento_seq'::regclass);


--
-- Name: iddetallemovimientocaja; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detallemovimientocaja ALTER COLUMN iddetallemovimientocaja SET DEFAULT nextval('detallemovimientocaja_iddetallemovimientocaja_seq'::regclass);


--
-- Name: iddetalleordencompra; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalleordencompra ALTER COLUMN iddetalleordencompra SET DEFAULT nextval('detalleordencompra_iddetalleordencompra_seq'::regclass);


--
-- Name: iddetalleventa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalleventa ALTER COLUMN iddetalleventa SET DEFAULT nextval('detalleventa_iddetalleventa_seq'::regclass);


--
-- Name: idflujocaja; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flujocaja ALTER COLUMN idflujocaja SET DEFAULT nextval('flujocaja_idflujocaja_seq'::regclass);


--
-- Name: idkardex; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kardex ALTER COLUMN idkardex SET DEFAULT nextval('kardex_idkardex_seq'::regclass);


--
-- Name: idlote; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lote ALTER COLUMN idlote SET DEFAULT nextval('lote_idlote_seq'::regclass);


--
-- Name: idmovimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY movimiento ALTER COLUMN idmovimiento SET DEFAULT nextval('movimiento_idmovimiento_seq'::regclass);


--
-- Name: idmovimientocaja; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY movimientocaja ALTER COLUMN idmovimientocaja SET DEFAULT nextval('movimientocaja_idmovimientocaja_seq'::regclass);


--
-- Name: idordencompra; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ordencompra ALTER COLUMN idordencompra SET DEFAULT nextval('ordencompra_idordencompra_seq'::regclass);


--
-- Name: idpago; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pago ALTER COLUMN idpago SET DEFAULT nextval('pago_idpago_seq'::regclass);


--
-- Name: idproveedor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY proveedor ALTER COLUMN idproveedor SET DEFAULT nextval('proveedor_idproveedor_seq'::regclass);


--
-- Name: idserie; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY serie ALTER COLUMN idserie SET DEFAULT nextval('serie_idserie_seq'::regclass);


--
-- Name: idtipocomprobante; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipocomprobante ALTER COLUMN idtipocomprobante SET DEFAULT nextval('tipocomprobante_idtipocomprobante_seq'::regclass);


--
-- Name: idtipomovimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipomovimiento ALTER COLUMN idtipomovimiento SET DEFAULT nextval('tipomovimiento_idtipomovimiento_seq'::regclass);


--
-- Name: idtipomovimientocaja; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipomovimientocaja ALTER COLUMN idtipomovimientocaja SET DEFAULT nextval('tipomovimientocaja_idtipomovimientocaja_seq'::regclass);


--
-- Name: idtipopago; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipopago ALTER COLUMN idtipopago SET DEFAULT nextval('tipopago_idtipopago_seq'::regclass);


--
-- Name: idtipousuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipousuario ALTER COLUMN idtipousuario SET DEFAULT nextval('tipousuario_idtipousuario_seq'::regclass);


--
-- Name: idusuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN idusuario SET DEFAULT nextval('usuario_idusuario_seq'::regclass);


--
-- Name: idventa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY venta ALTER COLUMN idventa SET DEFAULT nextval('venta_idventa_seq'::regclass);


--
-- Data for Name: almacen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY almacen (idalmacen, nombre, estado) FROM stdin;
1	Principal	A
2	Tienda 2	I
\.


--
-- Name: almacen_idalmacen_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('almacen_idalmacen_seq', 1, false);


--
-- Data for Name: almacenarticulo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY almacenarticulo (idalmacenarticulo, idalmacen, idarticulo, stock, stockminimo) FROM stdin;
2550	1	5	100	0
2551	1	6	100	0
2552	1	7	100	0
2553	1	8	100	0
2554	1	11	100	0
2555	1	12	100	0
2556	1	13	100	0
2557	1	14	100	0
2558	1	16	100	0
2559	1	17	100	0
2560	1	18	100	0
2561	1	19	100	0
2562	1	20	100	0
2563	1	21	100	0
2564	1	23	100	0
2565	1	24	100	0
2566	1	25	100	0
2567	1	26	100	0
2568	1	27	100	0
2569	1	28	100	0
2570	1	29	100	0
2571	1	30	100	0
2572	1	31	100	0
2573	1	33	100	0
2574	1	36	100	0
2575	1	37	100	0
2576	1	38	100	0
2577	1	39	100	0
2578	1	40	100	0
2579	1	43	100	0
2580	1	44	100	0
2581	1	45	100	0
2582	1	46	100	0
2583	1	47	100	0
2584	1	48	100	0
2585	1	50	100	0
2586	1	51	100	0
2587	1	52	100	0
2588	1	53	100	0
2589	1	60	100	0
2590	1	35	100	0
2591	1	15	100	0
2592	1	22	100	0
2593	1	32	100	0
2594	1	34	100	0
2595	1	41	100	0
2596	1	49	100	0
2597	1	57	100	0
2598	1	58	100	0
2599	1	61	100	0
2600	1	59	100	0
2601	1	55	100	0
2602	1	9	100	0
2603	1	67	100	0
2604	1	68	100	0
2605	1	10	100	0
2606	1	121	100	0
2607	1	123	100	0
2608	1	125	100	0
2609	1	126	100	0
2610	1	127	100	0
2611	1	128	100	0
2612	1	129	100	0
2613	1	130	100	0
2614	1	133	100	0
2615	1	135	100	0
2616	1	136	100	0
2617	1	143	100	0
2618	1	144	100	0
2619	1	145	100	0
2620	1	146	100	0
2621	1	152	100	0
2622	1	153	100	0
2623	1	154	100	0
2624	1	155	100	0
2625	1	156	100	0
2626	1	157	100	0
2627	1	158	100	0
2628	1	159	100	0
2629	1	162	100	0
2630	1	163	100	0
2631	1	164	100	0
2632	1	165	100	0
2633	1	166	100	0
2634	1	167	100	0
2635	1	168	100	0
2636	1	171	100	0
2637	1	172	100	0
2638	1	175	100	0
2639	1	176	100	0
2640	1	177	100	0
2641	1	122	100	0
2642	1	134	100	0
2643	1	137	100	0
2644	1	138	100	0
2645	1	161	100	0
2646	1	139	100	0
2647	1	132	100	0
2648	1	160	100	0
2649	1	150	100	0
2650	1	149	100	0
2651	1	147	100	0
2652	1	141	100	0
2653	1	140	100	0
2654	1	142	100	0
2655	1	170	100	0
2656	1	174	100	0
2657	1	173	100	0
2658	1	103	100	0
2659	1	105	100	0
2660	1	120	100	0
2661	1	114	100	0
2662	1	178	100	0
2663	1	179	100	0
2664	1	180	100	0
2665	1	181	100	0
2666	1	182	100	0
2667	1	183	100	0
2668	1	184	100	0
2669	1	185	100	0
2670	1	186	100	0
2671	1	187	100	0
2672	1	188	100	0
2673	1	189	100	0
2674	1	190	100	0
2675	1	191	100	0
2676	1	192	100	0
2677	1	193	100	0
2678	1	194	100	0
2679	1	195	100	0
2680	1	196	100	0
2681	1	197	100	0
2682	1	198	100	0
2683	1	199	100	0
2684	1	200	100	0
2685	1	201	100	0
2686	1	202	100	0
2687	1	203	100	0
2688	1	204	100	0
2689	1	205	100	0
2690	1	206	100	0
2691	1	207	100	0
2692	1	208	100	0
2693	1	209	100	0
2694	1	210	100	0
2695	1	211	100	0
2696	1	212	100	0
2697	1	213	100	0
2698	1	214	100	0
2699	1	215	100	0
2700	1	216	100	0
2701	1	217	100	0
2702	1	218	100	0
2703	1	219	100	0
2704	1	220	100	0
2705	1	221	100	0
2706	1	222	100	0
2707	1	223	100	0
2708	1	224	100	0
2709	1	225	100	0
2710	1	226	100	0
2711	1	227	100	0
2712	1	229	100	0
2713	1	230	100	0
2714	1	232	100	0
2715	1	233	100	0
2716	1	234	100	0
2717	1	235	100	0
2718	1	236	100	0
2719	1	237	100	0
2720	1	238	100	0
2721	1	239	100	0
2722	1	240	100	0
2723	1	241	100	0
2724	1	242	100	0
2725	1	243	100	0
2726	1	244	100	0
2727	1	245	100	0
2728	1	246	100	0
2729	1	247	100	0
2730	1	248	100	0
2731	1	249	100	0
2732	1	250	100	0
2733	1	251	100	0
2734	1	252	100	0
2735	1	253	100	0
2736	1	254	100	0
2737	1	255	100	0
2738	1	256	100	0
2739	1	257	100	0
2740	1	334	100	0
2741	1	258	100	0
2742	1	259	100	0
2743	1	260	100	0
2744	1	261	100	0
2745	1	262	100	0
2746	1	263	100	0
2747	1	264	100	0
2748	1	265	100	0
2749	1	266	100	0
2750	1	267	100	0
2751	1	268	100	0
2752	1	269	100	0
2753	1	270	100	0
2754	1	271	100	0
2755	1	272	100	0
2756	1	273	100	0
2757	1	274	100	0
2758	1	275	100	0
2759	1	276	100	0
2760	1	277	100	0
2761	1	278	100	0
2762	1	279	100	0
2763	1	280	100	0
2764	1	281	100	0
2765	1	282	100	0
2766	1	283	100	0
2767	1	284	100	0
2768	1	285	100	0
2769	1	286	100	0
2770	1	287	100	0
2771	1	288	100	0
2772	1	289	100	0
2773	1	290	100	0
2774	1	291	100	0
2775	1	292	100	0
2776	1	293	100	0
2777	1	294	100	0
2778	1	295	100	0
2779	1	296	100	0
2780	1	297	100	0
2781	1	298	100	0
2782	1	299	100	0
2783	1	300	100	0
2784	1	301	100	0
2785	1	302	100	0
2786	1	303	100	0
2787	1	304	100	0
2788	1	305	100	0
2789	1	306	100	0
2790	1	307	100	0
2791	1	308	100	0
2792	1	309	100	0
2793	1	310	100	0
2794	1	311	100	0
2795	1	312	100	0
2796	1	313	100	0
2797	1	314	100	0
2798	1	315	100	0
2799	1	316	100	0
2800	1	317	100	0
2801	1	318	100	0
2802	1	319	100	0
2803	1	320	100	0
2804	1	321	100	0
2805	1	322	100	0
2806	1	323	100	0
2807	1	324	100	0
2808	1	325	100	0
2809	1	326	100	0
2810	1	327	100	0
2811	1	328	100	0
2812	1	329	100	0
2813	1	330	100	0
2814	1	331	100	0
2815	1	332	100	0
2816	1	333	100	0
2817	1	335	100	0
2818	1	336	100	0
2819	1	337	100	0
2820	1	338	100	0
2821	1	339	100	0
2822	1	340	100	0
2823	1	341	100	0
2824	1	342	100	0
2825	1	343	100	0
2826	1	344	100	0
2827	1	345	100	0
2828	1	346	100	0
2829	1	347	100	0
2830	1	348	100	0
2831	1	349	100	0
2832	1	350	100	0
2833	1	351	100	0
2834	1	352	100	0
2835	1	353	100	0
2836	1	354	100	0
2837	1	355	100	0
2838	1	356	100	0
2839	1	357	100	0
2840	1	358	100	0
2841	1	359	100	0
2842	1	360	100	0
2843	1	361	100	0
2844	1	362	100	0
2845	1	363	100	0
2846	1	364	100	0
2847	1	365	100	0
2848	1	366	100	0
2849	1	367	100	0
2850	1	368	100	0
2851	1	369	100	0
2852	1	370	100	0
2853	1	371	100	0
2854	1	372	100	0
2855	1	373	100	0
2856	1	374	100	0
2857	1	375	100	0
2858	1	376	100	0
2859	1	377	100	0
2860	1	378	100	0
2861	1	379	100	0
2862	1	380	100	0
2863	1	381	100	0
2864	1	382	100	0
2865	1	383	100	0
2866	1	384	100	0
2867	1	385	100	0
2868	1	386	100	0
2869	1	387	100	0
2870	1	388	100	0
2871	1	389	100	0
2872	1	390	100	0
2873	1	391	100	0
2874	1	392	100	0
2875	1	393	100	0
2876	1	394	100	0
2877	1	395	100	0
2878	1	396	100	0
2879	1	397	100	0
2880	1	398	100	0
2881	1	399	100	0
2882	1	400	100	0
2883	1	401	100	0
2884	1	402	100	0
2885	1	403	100	0
2886	1	404	100	0
2887	1	405	100	0
2888	1	406	100	0
2889	1	407	100	0
2890	1	408	100	0
2891	1	409	100	0
2892	1	410	100	0
2893	1	411	100	0
2894	1	412	100	0
2895	1	413	100	0
2896	1	414	100	0
2897	1	415	100	0
2898	1	416	100	0
2899	1	417	100	0
2900	1	418	100	0
2901	1	419	100	0
2902	1	420	100	0
2903	1	421	100	0
2904	1	422	100	0
2905	1	423	100	0
2906	1	424	100	0
2907	1	425	100	0
2908	1	426	100	0
2909	1	427	100	0
2910	1	428	100	0
2911	1	429	100	0
2912	1	430	100	0
2913	1	431	100	0
2914	1	432	100	0
2915	1	433	100	0
2916	1	434	100	0
2917	1	435	100	0
2918	1	436	100	0
2919	1	437	100	0
2920	1	438	100	0
2921	1	439	100	0
2922	1	440	100	0
2923	1	441	100	0
2924	1	442	100	0
2925	1	443	100	0
2926	1	444	100	0
2927	1	445	100	0
2928	1	446	100	0
2929	1	447	100	0
2930	1	448	100	0
2931	1	449	100	0
2932	1	450	100	0
2933	1	451	100	0
2934	1	452	100	0
2935	1	453	100	0
2936	1	454	100	0
2937	1	455	100	0
2938	1	456	100	0
2939	1	457	100	0
2940	1	458	100	0
2941	1	459	100	0
2942	1	460	100	0
2943	1	461	100	0
2944	1	462	100	0
2945	1	463	100	0
2946	1	464	100	0
2947	1	465	100	0
2948	1	466	100	0
2949	1	467	100	0
2950	1	468	100	0
2951	1	469	100	0
2952	1	470	100	0
2953	1	471	100	0
2954	1	472	100	0
2955	1	473	100	0
2956	1	474	100	0
2957	1	475	100	0
2958	1	476	100	0
2959	1	477	100	0
2960	1	478	100	0
2961	1	479	100	0
2962	1	480	100	0
2963	1	481	100	0
2964	1	482	100	0
2965	1	483	100	0
2966	1	484	100	0
2967	1	485	100	0
2968	1	486	100	0
2969	1	487	100	0
2970	1	488	100	0
2971	1	489	100	0
2972	1	490	100	0
2973	1	491	100	0
2974	1	492	100	0
2975	1	493	100	0
2976	1	494	100	0
2977	1	495	100	0
2978	1	496	100	0
2979	1	497	100	0
2980	1	498	100	0
2981	1	499	100	0
2982	1	500	100	0
2983	1	501	100	0
2984	1	502	100	0
2985	1	503	100	0
2986	1	504	100	0
2987	1	505	100	0
2988	1	506	100	0
2989	1	507	100	0
2990	1	508	100	0
2991	1	509	100	0
2992	1	510	100	0
2993	1	511	100	0
2994	1	512	100	0
2995	1	513	100	0
2996	1	514	100	0
2997	1	515	100	0
2998	1	516	100	0
2999	1	517	100	0
3000	1	518	100	0
3001	1	519	100	0
3002	1	520	100	0
3003	1	521	100	0
3004	1	522	100	0
3005	1	523	100	0
3006	1	524	100	0
3007	1	525	100	0
3008	1	526	100	0
3009	1	527	100	0
3010	1	528	100	0
3011	1	529	100	0
3012	1	530	100	0
3013	1	531	100	0
3014	1	532	100	0
3015	1	533	100	0
3016	1	534	100	0
3017	1	535	100	0
3018	1	536	100	0
3019	1	537	100	0
3020	1	538	100	0
3021	1	539	100	0
3022	1	540	100	0
3023	1	541	100	0
3024	1	542	100	0
3025	1	543	100	0
3026	1	544	100	0
3027	1	545	100	0
3028	1	546	100	0
3029	1	547	100	0
3030	1	548	100	0
3031	1	549	100	0
3032	1	550	100	0
3033	1	551	100	0
3034	1	552	100	0
3035	1	553	100	0
3036	1	554	100	0
3037	1	555	100	0
3038	1	556	100	0
3039	1	557	100	0
3040	1	558	100	0
3041	1	559	100	0
3042	1	560	100	0
3043	1	561	100	0
3044	1	562	100	0
3045	1	563	100	0
3046	1	564	100	0
3047	1	565	100	0
3048	1	566	100	0
3049	1	567	100	0
3050	1	568	100	0
3051	1	569	100	0
3052	1	570	100	0
3053	1	571	100	0
3054	1	572	100	0
3055	1	573	100	0
3056	1	574	100	0
3057	1	575	100	0
3058	1	576	100	0
3059	1	577	100	0
3060	1	583	100	0
3061	1	585	100	0
3062	1	587	100	0
3063	1	582	100	0
3064	1	579	100	0
3065	1	581	100	0
3066	1	584	100	0
3067	1	580	100	0
3068	1	590	100	0
3069	1	591	100	0
3070	1	596	100	0
3071	1	597	100	0
3072	1	600	100	0
3073	1	606	100	0
3074	1	611	100	0
3075	1	612	100	0
3076	1	613	100	0
3077	1	614	100	0
3078	1	615	100	0
3079	1	616	100	0
3080	1	617	100	0
3081	1	618	100	0
3082	1	619	100	0
3083	1	620	100	0
3084	1	621	100	0
3085	1	622	100	0
3086	1	623	100	0
3087	1	624	100	0
3088	1	625	100	0
3089	1	626	100	0
3090	1	627	100	0
3091	1	628	100	0
3092	1	629	100	0
3093	1	630	100	0
3094	1	631	100	0
3095	1	632	100	0
3096	1	633	100	0
3097	1	634	100	0
3098	1	635	100	0
3099	1	636	100	0
3100	1	637	100	0
3101	1	638	100	0
3102	1	639	100	0
3103	1	640	100	0
3104	1	641	100	0
3105	1	642	100	0
3106	1	643	100	0
3107	1	644	100	0
3108	1	645	100	0
3109	1	646	100	0
3110	1	647	100	0
3111	1	648	100	0
3112	1	649	100	0
3113	1	650	100	0
3114	1	651	100	0
3115	1	652	100	0
3116	1	653	100	0
3117	1	654	100	0
3118	1	655	100	0
3119	1	656	100	0
3120	1	657	100	0
3121	1	658	100	0
3122	1	659	100	0
3123	1	660	100	0
3124	1	661	100	0
3125	1	662	100	0
3126	1	663	100	0
3127	1	664	100	0
3128	1	665	100	0
3129	1	666	100	0
3130	1	667	100	0
3131	1	670	100	0
3132	1	674	100	0
3133	1	677	100	0
3134	1	598	100	0
3135	1	602	100	0
3136	1	682	100	0
3137	1	672	100	0
3138	1	679	100	0
3139	1	593	100	0
3140	1	595	100	0
3141	1	683	100	0
3142	1	675	100	0
3143	1	673	100	0
3144	1	678	100	0
3145	1	605	100	0
3146	1	603	100	0
3147	1	607	100	0
3148	1	608	100	0
3149	1	609	100	0
3150	1	610	100	0
3151	1	676	100	0
3152	1	671	100	0
3153	1	589	100	0
3154	1	668	100	0
3155	1	669	100	0
3156	1	686	100	0
3157	1	687	100	0
3158	1	688	100	0
3159	1	690	100	0
3160	1	694	100	0
3161	1	695	100	0
3162	1	696	100	0
3163	1	697	100	0
3164	1	698	100	0
3165	1	699	100	0
3166	1	700	100	0
3167	1	701	100	0
3168	1	702	100	0
3169	1	703	100	0
3170	1	704	100	0
3171	1	705	100	0
3172	1	706	100	0
3173	1	707	100	0
3174	1	708	100	0
3175	1	709	100	0
3176	1	710	100	0
3177	1	711	100	0
3178	1	712	100	0
3179	1	713	100	0
3180	1	714	100	0
3181	1	715	100	0
3182	1	716	100	0
3183	1	717	100	0
3184	1	718	100	0
3185	1	719	100	0
3186	1	720	100	0
3187	1	721	100	0
3188	1	722	100	0
3189	1	723	100	0
3190	1	724	100	0
3191	1	725	100	0
3192	1	726	100	0
3193	1	727	100	0
3194	1	728	100	0
3195	1	729	100	0
3196	1	730	100	0
3197	1	731	100	0
3198	1	732	100	0
3199	1	733	100	0
3200	1	734	100	0
3201	1	735	100	0
3202	1	736	100	0
3203	1	737	100	0
3204	1	738	100	0
3205	1	739	100	0
3206	1	740	100	0
3207	1	743	100	0
3208	1	745	100	0
3209	1	752	100	0
3210	1	753	100	0
3211	1	754	100	0
3212	1	755	100	0
3213	1	756	100	0
3214	1	758	100	0
3215	1	762	100	0
3216	1	763	100	0
3217	1	764	100	0
3218	1	765	100	0
3219	1	693	100	0
3220	1	685	100	0
3221	1	684	100	0
3222	1	744	100	0
3223	1	741	100	0
3224	1	742	100	0
3225	1	689	100	0
3226	1	747	100	0
3227	1	748	100	0
3228	1	750	100	0
3229	1	749	100	0
3230	1	760	100	0
3231	1	759	100	0
3232	1	766	100	0
3233	1	761	100	0
3234	1	769	100	0
3235	1	770	100	0
3236	1	771	100	0
3237	1	773	100	0
3238	1	774	100	0
3239	1	778	100	0
3240	1	779	100	0
3241	1	783	100	0
3242	1	785	100	0
3243	1	789	100	0
3244	1	790	100	0
3245	1	793	100	0
3246	1	794	100	0
3247	1	801	100	0
3248	1	810	100	0
3249	1	814	100	0
3250	1	815	100	0
3251	1	816	100	0
3252	1	818	100	0
3253	1	820	100	0
3254	1	822	100	0
3255	1	824	100	0
3256	1	825	100	0
3257	1	831	100	0
3258	1	834	100	0
3259	1	836	100	0
3260	1	837	100	0
3261	1	838	100	0
3262	1	839	100	0
3263	1	840	100	0
3264	1	841	100	0
3265	1	842	100	0
3266	1	843	100	0
3267	1	844	100	0
3268	1	847	100	0
3269	1	850	100	0
3270	1	851	100	0
3271	1	768	100	0
3272	1	835	100	0
3273	1	832	100	0
3274	1	812	100	0
3275	1	829	100	0
3276	1	828	100	0
3277	1	830	100	0
3278	1	813	100	0
3279	1	809	100	0
3280	1	772	100	0
3281	1	787	100	0
3282	1	788	100	0
3283	1	796	100	0
3284	1	791	100	0
3285	1	797	100	0
3286	1	776	100	0
3287	1	823	100	0
3288	1	821	100	0
3289	1	819	100	0
3290	1	817	100	0
3291	1	849	100	0
3292	1	775	100	0
3293	1	845	100	0
3294	1	846	100	0
3295	1	800	100	0
3296	1	781	100	0
3297	1	780	100	0
3298	1	807	100	0
3299	1	786	100	0
3300	1	795	100	0
3301	1	784	100	0
3302	1	792	100	0
3303	1	798	100	0
3304	1	803	100	0
3305	1	804	100	0
3306	1	802	100	0
3307	1	848	100	0
3308	1	826	100	0
3309	1	852	100	0
3310	1	853	100	0
3311	1	854	100	0
3312	1	855	100	0
3313	1	856	100	0
3314	1	857	100	0
3315	1	858	100	0
3316	1	859	100	0
3317	1	860	100	0
3318	1	861	100	0
3319	1	862	100	0
3320	1	863	100	0
3321	1	864	100	0
3322	1	865	100	0
3323	1	866	100	0
3324	1	867	100	0
3325	1	868	100	0
3326	1	869	100	0
3327	1	874	100	0
3328	1	877	100	0
3329	1	888	100	0
3330	1	893	100	0
3331	1	896	100	0
3332	1	897	100	0
3333	1	899	100	0
3334	1	902	100	0
3335	1	904	100	0
3336	1	908	100	0
3337	1	909	100	0
3338	1	915	100	0
3339	1	916	100	0
3340	1	918	100	0
3341	1	921	100	0
3342	1	923	100	0
3343	1	925	100	0
3344	1	927	100	0
3345	1	928	100	0
3346	1	929	100	0
3347	1	930	100	0
3348	1	931	100	0
3349	1	932	100	0
3350	1	871	100	0
3351	1	873	100	0
3352	1	882	100	0
3353	1	892	100	0
3354	1	891	100	0
3355	1	872	100	0
3356	1	878	100	0
3357	1	879	100	0
3358	1	884	100	0
3359	1	885	100	0
3360	1	895	100	0
3361	1	887	100	0
3362	1	926	100	0
3363	1	922	100	0
3364	1	924	100	0
3365	1	876	100	0
3366	1	890	100	0
3367	1	900	100	0
3368	1	894	100	0
3369	1	919	100	0
3370	1	910	100	0
3371	1	911	100	0
3372	1	913	100	0
3373	1	914	100	0
3374	1	912	100	0
3375	1	875	100	0
3376	1	907	100	0
3377	1	905	100	0
3378	1	906	100	0
3379	1	903	100	0
3380	1	889	100	0
3381	1	933	100	0
3382	1	934	100	0
3383	1	940	100	0
3384	1	941	100	0
3385	1	943	100	0
3386	1	946	100	0
3387	1	949	100	0
3388	1	954	100	0
3389	1	956	100	0
3390	1	958	100	0
3391	1	959	100	0
3392	1	961	100	0
3393	1	964	100	0
3394	1	971	100	0
3395	1	972	100	0
3396	1	974	100	0
3397	1	976	100	0
3398	1	980	100	0
3399	1	981	100	0
3400	1	988	100	0
3401	1	989	100	0
3402	1	990	100	0
3403	1	991	100	0
3404	1	992	100	0
3405	1	994	100	0
3406	1	996	100	0
3407	1	997	100	0
3408	1	998	100	0
3409	1	999	100	0
3410	1	1000	100	0
3411	1	1001	100	0
3412	1	1002	100	0
3413	1	1003	100	0
3414	1	1004	100	0
3415	1	1005	100	0
3416	1	1006	100	0
3417	1	1007	100	0
3418	1	1008	100	0
3419	1	1009	100	0
3420	1	1010	100	0
3421	1	1011	100	0
3422	1	1012	100	0
3423	1	1013	100	0
3424	1	993	100	0
3425	1	944	100	0
3426	1	995	100	0
3427	1	985	100	0
3428	1	945	100	0
3429	1	987	100	0
3430	1	967	100	0
3431	1	968	100	0
3432	1	966	100	0
3433	1	970	100	0
3434	1	969	100	0
3435	1	978	100	0
3436	1	977	100	0
3437	1	979	100	0
3438	1	984	100	0
3439	1	983	100	0
3440	1	982	100	0
3441	1	957	100	0
3442	1	960	100	0
3443	1	962	100	0
3444	1	963	100	0
3445	1	948	100	0
3446	1	950	100	0
3447	1	955	100	0
3448	1	953	100	0
3449	1	952	100	0
3450	1	951	100	0
3451	1	938	100	0
3452	1	937	100	0
3453	1	936	100	0
3454	1	1014	100	0
3455	1	1015	100	0
3456	1	1016	100	0
3457	1	1017	100	0
3458	1	1018	100	0
3459	1	1019	100	0
3460	1	1020	100	0
3461	1	1021	100	0
3462	1	1022	100	0
3463	1	1023	100	0
3464	1	1024	100	0
3465	1	1025	100	0
3466	1	1026	100	0
3467	1	1027	100	0
3468	1	1028	100	0
3469	1	1	100	0
3470	1	883	100	0
3471	1	870	100	0
3472	1	898	100	0
3473	1	942	100	0
3474	1	880	100	0
3475	1	886	100	0
3476	1	881	100	0
3477	1	920	100	0
3478	1	935	100	0
3479	1	917	100	0
3480	1	901	100	0
3481	1	986	100	0
3482	1	973	100	0
3483	1	975	100	0
3484	1	965	100	0
3485	1	947	100	0
3486	1	939	100	0
3487	1	124	100	0
3488	1	599	100	0
3489	1	592	100	0
3490	1	692	100	0
3491	1	594	100	0
3492	1	680	100	0
3493	1	691	100	0
3494	1	604	100	0
3495	1	681	100	0
3496	1	231	100	0
3497	1	228	100	0
3498	1	42	100	0
3499	1	169	100	0
3500	1	588	100	0
3501	1	131	100	0
3502	1	148	100	0
3503	1	151	100	0
3504	1	578	100	0
3505	1	586	100	0
3506	1	601	100	0
3507	1	104	100	0
3508	1	56	100	0
3509	1	54	100	0
3510	1	4	100	0
3511	1	746	100	0
3512	1	751	100	0
3513	1	767	100	0
3514	1	757	100	0
3515	1	833	100	0
3516	1	811	100	0
3517	1	799	100	0
3518	1	806	100	0
3519	1	827	100	0
3520	1	777	100	0
3521	1	782	100	0
3522	1	808	100	0
3523	1	805	100	0
3526	2	5	100	0
3527	2	6	100	0
3528	2	7	100	0
3529	2	8	100	0
3530	2	11	100	0
3531	2	12	100	0
3532	2	13	100	0
3533	2	14	100	0
3534	2	16	100	0
3535	2	17	100	0
3536	2	18	100	0
3537	2	19	100	0
3538	2	20	100	0
3539	2	21	100	0
3540	2	23	100	0
3541	2	24	100	0
3542	2	25	100	0
3543	2	26	100	0
3544	2	27	100	0
3545	2	28	100	0
3546	2	29	100	0
3547	2	30	100	0
3548	2	31	100	0
3549	2	33	100	0
3550	2	36	100	0
3551	2	37	100	0
3552	2	38	100	0
3553	2	39	100	0
3554	2	40	100	0
3555	2	43	100	0
3556	2	44	100	0
3557	2	45	100	0
3558	2	46	100	0
3559	2	47	100	0
3560	2	48	100	0
3561	2	50	100	0
3562	2	51	100	0
3563	2	52	100	0
3564	2	53	100	0
3565	2	60	100	0
3566	2	35	100	0
3567	2	15	100	0
3568	2	22	100	0
3569	2	32	100	0
3570	2	34	100	0
3571	2	41	100	0
3572	2	49	100	0
3573	2	57	100	0
3574	2	58	100	0
3575	2	61	100	0
3576	2	59	100	0
3577	2	55	100	0
3578	2	9	100	0
3579	2	67	100	0
3580	2	68	100	0
3581	2	10	100	0
3582	2	121	100	0
3583	2	123	100	0
3584	2	125	100	0
3585	2	126	100	0
3586	2	127	100	0
3587	2	128	100	0
3588	2	129	100	0
3589	2	130	100	0
3590	2	133	100	0
3591	2	135	100	0
3592	2	136	100	0
3593	2	143	100	0
3594	2	144	100	0
3595	2	145	100	0
3596	2	146	100	0
3597	2	152	100	0
3598	2	153	100	0
3599	2	154	100	0
3600	2	155	100	0
3601	2	156	100	0
3602	2	157	100	0
3603	2	158	100	0
3604	2	159	100	0
3605	2	162	100	0
3606	2	163	100	0
3607	2	164	100	0
3608	2	165	100	0
3609	2	166	100	0
3610	2	167	100	0
3611	2	168	100	0
3612	2	171	100	0
3613	2	172	100	0
3614	2	175	100	0
3615	2	176	100	0
3616	2	177	100	0
3617	2	122	100	0
3618	2	134	100	0
3619	2	137	100	0
3620	2	138	100	0
3621	2	161	100	0
3622	2	139	100	0
3623	2	132	100	0
3624	2	160	100	0
3625	2	150	100	0
3626	2	149	100	0
3627	2	147	100	0
3628	2	141	100	0
3629	2	140	100	0
3630	2	142	100	0
3631	2	170	100	0
3632	2	174	100	0
3633	2	173	100	0
3634	2	103	100	0
3635	2	105	100	0
3636	2	120	100	0
3637	2	114	100	0
3638	2	178	100	0
3639	2	179	100	0
3640	2	180	100	0
3641	2	181	100	0
3642	2	182	100	0
3643	2	183	100	0
3644	2	184	100	0
3645	2	185	100	0
3646	2	186	100	0
3647	2	187	100	0
3648	2	188	100	0
3649	2	189	100	0
3650	2	190	100	0
3651	2	191	100	0
3652	2	192	100	0
3653	2	193	100	0
3654	2	194	100	0
3655	2	195	100	0
3656	2	196	100	0
3657	2	197	100	0
3658	2	198	100	0
3659	2	199	100	0
3660	2	200	100	0
3661	2	201	100	0
3662	2	202	100	0
3663	2	203	100	0
3664	2	204	100	0
3665	2	205	100	0
3666	2	206	100	0
3667	2	207	100	0
3668	2	208	100	0
3669	2	209	100	0
3670	2	210	100	0
3671	2	211	100	0
3672	2	212	100	0
3673	2	213	100	0
3674	2	214	100	0
3675	2	215	100	0
3676	2	216	100	0
3677	2	217	100	0
3678	2	218	100	0
3679	2	219	100	0
3680	2	220	100	0
3681	2	221	100	0
3682	2	222	100	0
3683	2	223	100	0
3684	2	224	100	0
3685	2	225	100	0
3686	2	226	100	0
3687	2	227	100	0
3688	2	229	100	0
3689	2	230	100	0
3690	2	232	100	0
3691	2	233	100	0
3692	2	234	100	0
3693	2	235	100	0
3694	2	236	100	0
3695	2	237	100	0
3696	2	238	100	0
3697	2	239	100	0
3698	2	240	100	0
3699	2	241	100	0
3700	2	242	100	0
3701	2	243	100	0
3702	2	244	100	0
3703	2	245	100	0
3704	2	246	100	0
3705	2	247	100	0
3706	2	248	100	0
3707	2	249	100	0
3708	2	250	100	0
3709	2	251	100	0
3710	2	252	100	0
3711	2	253	100	0
3712	2	254	100	0
3713	2	255	100	0
3714	2	256	100	0
3715	2	257	100	0
3716	2	334	100	0
3717	2	258	100	0
3718	2	259	100	0
3719	2	260	100	0
3720	2	261	100	0
3721	2	262	100	0
3722	2	263	100	0
3723	2	264	100	0
3724	2	265	100	0
3725	2	266	100	0
3726	2	267	100	0
3727	2	268	100	0
3728	2	269	100	0
3729	2	270	100	0
3730	2	271	100	0
3731	2	272	100	0
3732	2	273	100	0
3733	2	274	100	0
3734	2	275	100	0
3735	2	276	100	0
3736	2	277	100	0
3737	2	278	100	0
3738	2	279	100	0
3739	2	280	100	0
3740	2	281	100	0
3741	2	282	100	0
3742	2	283	100	0
3743	2	284	100	0
3744	2	285	100	0
3745	2	286	100	0
3746	2	287	100	0
3747	2	288	100	0
3748	2	289	100	0
3749	2	290	100	0
3750	2	291	100	0
3751	2	292	100	0
3752	2	293	100	0
3753	2	294	100	0
3754	2	295	100	0
3755	2	296	100	0
3756	2	297	100	0
3757	2	298	100	0
3758	2	299	100	0
3759	2	300	100	0
3760	2	301	100	0
3761	2	302	100	0
3762	2	303	100	0
3763	2	304	100	0
3764	2	305	100	0
3765	2	306	100	0
3766	2	307	100	0
3767	2	308	100	0
3768	2	309	100	0
3769	2	310	100	0
3770	2	311	100	0
3771	2	312	100	0
3772	2	313	100	0
3773	2	314	100	0
3774	2	315	100	0
3775	2	316	100	0
3776	2	317	100	0
3777	2	318	100	0
3778	2	319	100	0
3779	2	320	100	0
3780	2	321	100	0
3781	2	322	100	0
3782	2	323	100	0
3783	2	324	100	0
3784	2	325	100	0
3785	2	326	100	0
3786	2	327	100	0
3787	2	328	100	0
3788	2	329	100	0
3789	2	330	100	0
3790	2	331	100	0
3791	2	332	100	0
3792	2	333	100	0
3793	2	335	100	0
3794	2	336	100	0
3795	2	337	100	0
3796	2	338	100	0
3797	2	339	100	0
3798	2	340	100	0
3799	2	341	100	0
3800	2	342	100	0
3801	2	343	100	0
3802	2	344	100	0
3803	2	345	100	0
3804	2	346	100	0
3805	2	347	100	0
3806	2	348	100	0
3807	2	349	100	0
3808	2	350	100	0
3809	2	351	100	0
3810	2	352	100	0
3811	2	353	100	0
3812	2	354	100	0
3813	2	355	100	0
3814	2	356	100	0
3815	2	357	100	0
3816	2	358	100	0
3817	2	359	100	0
3818	2	360	100	0
3819	2	361	100	0
3820	2	362	100	0
3821	2	363	100	0
3822	2	364	100	0
3823	2	365	100	0
3824	2	366	100	0
3825	2	367	100	0
3826	2	368	100	0
3827	2	369	100	0
3828	2	370	100	0
3829	2	371	100	0
3830	2	372	100	0
3831	2	373	100	0
3832	2	374	100	0
3833	2	375	100	0
3834	2	376	100	0
3835	2	377	100	0
3836	2	378	100	0
3837	2	379	100	0
3838	2	380	100	0
3839	2	381	100	0
3840	2	382	100	0
3841	2	383	100	0
3842	2	384	100	0
3843	2	385	100	0
3844	2	386	100	0
3845	2	387	100	0
3846	2	388	100	0
3847	2	389	100	0
3848	2	390	100	0
3849	2	391	100	0
3850	2	392	100	0
3851	2	393	100	0
3852	2	394	100	0
3853	2	395	100	0
3854	2	396	100	0
3855	2	397	100	0
3856	2	398	100	0
3857	2	399	100	0
3858	2	400	100	0
3859	2	401	100	0
3860	2	402	100	0
3861	2	403	100	0
3862	2	404	100	0
3863	2	405	100	0
3864	2	406	100	0
3865	2	407	100	0
3866	2	408	100	0
3867	2	409	100	0
3868	2	410	100	0
3869	2	411	100	0
3870	2	412	100	0
3871	2	413	100	0
3872	2	414	100	0
3873	2	415	100	0
3874	2	416	100	0
3875	2	417	100	0
3876	2	418	100	0
3877	2	419	100	0
3878	2	420	100	0
3879	2	421	100	0
3880	2	422	100	0
3881	2	423	100	0
3882	2	424	100	0
3883	2	425	100	0
3884	2	426	100	0
3885	2	427	100	0
3886	2	428	100	0
3887	2	429	100	0
3888	2	430	100	0
3889	2	431	100	0
3890	2	432	100	0
3891	2	433	100	0
3892	2	434	100	0
3893	2	435	100	0
3894	2	436	100	0
3895	2	437	100	0
3896	2	438	100	0
3897	2	439	100	0
3898	2	440	100	0
3899	2	441	100	0
3900	2	442	100	0
3901	2	443	100	0
3902	2	444	100	0
3903	2	445	100	0
3904	2	446	100	0
3905	2	447	100	0
3906	2	448	100	0
3907	2	449	100	0
3908	2	450	100	0
3909	2	451	100	0
3910	2	452	100	0
3911	2	453	100	0
3912	2	454	100	0
3913	2	455	100	0
3914	2	456	100	0
3915	2	457	100	0
3916	2	458	100	0
3917	2	459	100	0
3918	2	460	100	0
3919	2	461	100	0
3920	2	462	100	0
3921	2	463	100	0
3922	2	464	100	0
3923	2	465	100	0
3924	2	466	100	0
3925	2	467	100	0
3926	2	468	100	0
3927	2	469	100	0
3928	2	470	100	0
3929	2	471	100	0
3930	2	472	100	0
3931	2	473	100	0
3932	2	474	100	0
3933	2	475	100	0
3934	2	476	100	0
3935	2	477	100	0
3936	2	478	100	0
3937	2	479	100	0
3938	2	480	100	0
3939	2	481	100	0
3940	2	482	100	0
3941	2	483	100	0
3942	2	484	100	0
3943	2	485	100	0
3944	2	486	100	0
3945	2	487	100	0
3946	2	488	100	0
3947	2	489	100	0
3948	2	490	100	0
3949	2	491	100	0
3950	2	492	100	0
3951	2	493	100	0
3952	2	494	100	0
3953	2	495	100	0
3954	2	496	100	0
3955	2	497	100	0
3956	2	498	100	0
3957	2	499	100	0
3958	2	500	100	0
3959	2	501	100	0
3960	2	502	100	0
3961	2	503	100	0
3962	2	504	100	0
3963	2	505	100	0
3964	2	506	100	0
3965	2	507	100	0
3966	2	508	100	0
3967	2	509	100	0
3968	2	510	100	0
3969	2	511	100	0
3970	2	512	100	0
3971	2	513	100	0
3972	2	514	100	0
3973	2	515	100	0
3974	2	516	100	0
3975	2	517	100	0
3976	2	518	100	0
3977	2	519	100	0
3978	2	520	100	0
3979	2	521	100	0
3980	2	522	100	0
3981	2	523	100	0
3982	2	524	100	0
3983	2	525	100	0
3984	2	526	100	0
3985	2	527	100	0
3986	2	528	100	0
3987	2	529	100	0
3988	2	530	100	0
3989	2	531	100	0
3990	2	532	100	0
3991	2	533	100	0
3992	2	534	100	0
3993	2	535	100	0
3994	2	536	100	0
3995	2	537	100	0
3996	2	538	100	0
3997	2	539	100	0
3998	2	540	100	0
3999	2	541	100	0
4000	2	542	100	0
4001	2	543	100	0
4002	2	544	100	0
4003	2	545	100	0
4004	2	546	100	0
4005	2	547	100	0
4006	2	548	100	0
4007	2	549	100	0
4008	2	550	100	0
4009	2	551	100	0
4010	2	552	100	0
4011	2	553	100	0
4012	2	554	100	0
4013	2	555	100	0
4014	2	556	100	0
4015	2	557	100	0
4016	2	558	100	0
4017	2	559	100	0
4018	2	560	100	0
4019	2	561	100	0
4020	2	562	100	0
4021	2	563	100	0
4022	2	564	100	0
4023	2	565	100	0
4024	2	566	100	0
4025	2	567	100	0
4026	2	568	100	0
4027	2	569	100	0
4028	2	570	100	0
4029	2	571	100	0
4030	2	572	100	0
4031	2	573	100	0
4032	2	574	100	0
4033	2	575	100	0
4034	2	576	100	0
4035	2	577	100	0
4036	2	583	100	0
4037	2	585	100	0
4038	2	587	100	0
4039	2	582	100	0
4040	2	579	100	0
4041	2	581	100	0
4042	2	584	100	0
4043	2	580	100	0
4044	2	590	100	0
4045	2	591	100	0
4046	2	596	100	0
4047	2	597	100	0
4048	2	600	100	0
4049	2	606	100	0
4050	2	611	100	0
4051	2	612	100	0
4052	2	613	100	0
4053	2	614	100	0
4054	2	615	100	0
4055	2	616	100	0
4056	2	617	100	0
4057	2	618	100	0
4058	2	619	100	0
4059	2	620	100	0
4060	2	621	100	0
4061	2	622	100	0
4062	2	623	100	0
4063	2	624	100	0
4064	2	625	100	0
4065	2	626	100	0
4066	2	627	100	0
4067	2	628	100	0
4068	2	629	100	0
4069	2	630	100	0
4070	2	631	100	0
4071	2	632	100	0
4072	2	633	100	0
4073	2	634	100	0
4074	2	635	100	0
4075	2	636	100	0
4076	2	637	100	0
4077	2	638	100	0
4078	2	639	100	0
4079	2	640	100	0
4080	2	641	100	0
4081	2	642	100	0
4082	2	643	100	0
4083	2	644	100	0
4084	2	645	100	0
4085	2	646	100	0
4086	2	647	100	0
4087	2	648	100	0
4088	2	649	100	0
4089	2	650	100	0
4090	2	651	100	0
4091	2	652	100	0
4092	2	653	100	0
4093	2	654	100	0
4094	2	655	100	0
4095	2	656	100	0
4096	2	657	100	0
4097	2	658	100	0
4098	2	659	100	0
4099	2	660	100	0
4100	2	661	100	0
4101	2	662	100	0
4102	2	663	100	0
4103	2	664	100	0
4104	2	665	100	0
4105	2	666	100	0
4106	2	667	100	0
4107	2	670	100	0
4108	2	674	100	0
4109	2	677	100	0
4110	2	598	100	0
4111	2	602	100	0
4112	2	682	100	0
4113	2	672	100	0
4114	2	679	100	0
4115	2	593	100	0
4116	2	595	100	0
4117	2	683	100	0
4118	2	675	100	0
4119	2	673	100	0
4120	2	678	100	0
4121	2	605	100	0
4122	2	603	100	0
4123	2	607	100	0
4124	2	608	100	0
4125	2	609	100	0
4126	2	610	100	0
4127	2	676	100	0
4128	2	671	100	0
4129	2	589	100	0
4130	2	668	100	0
4131	2	669	100	0
4132	2	686	100	0
4133	2	687	100	0
4134	2	688	100	0
4135	2	690	100	0
4136	2	694	100	0
4137	2	695	100	0
4138	2	696	100	0
4139	2	697	100	0
4140	2	698	100	0
4141	2	699	100	0
4142	2	700	100	0
4143	2	701	100	0
4144	2	702	100	0
4145	2	703	100	0
4146	2	704	100	0
4147	2	705	100	0
4148	2	706	100	0
4149	2	707	100	0
4150	2	708	100	0
4151	2	709	100	0
4152	2	710	100	0
4153	2	711	100	0
4154	2	712	100	0
4155	2	713	100	0
4156	2	714	100	0
4157	2	715	100	0
4158	2	716	100	0
4159	2	717	100	0
4160	2	718	100	0
4161	2	719	100	0
4162	2	720	100	0
4163	2	721	100	0
4164	2	722	100	0
4165	2	723	100	0
4166	2	724	100	0
4167	2	725	100	0
4168	2	726	100	0
4169	2	727	100	0
4170	2	728	100	0
4171	2	729	100	0
4172	2	730	100	0
4173	2	731	100	0
4174	2	732	100	0
4175	2	733	100	0
4176	2	734	100	0
4177	2	735	100	0
4178	2	736	100	0
4179	2	737	100	0
4180	2	738	100	0
4181	2	739	100	0
4182	2	740	100	0
4183	2	743	100	0
4184	2	745	100	0
4185	2	752	100	0
4186	2	753	100	0
4187	2	754	100	0
4188	2	755	100	0
4189	2	756	100	0
4190	2	758	100	0
4191	2	762	100	0
4192	2	763	100	0
4193	2	764	100	0
4194	2	765	100	0
4195	2	693	100	0
4196	2	685	100	0
4197	2	684	100	0
4198	2	744	100	0
4199	2	741	100	0
4200	2	742	100	0
4201	2	689	100	0
4202	2	747	100	0
4203	2	748	100	0
4204	2	750	100	0
4205	2	749	100	0
4206	2	760	100	0
4207	2	759	100	0
4208	2	766	100	0
4209	2	761	100	0
4210	2	769	100	0
4211	2	770	100	0
4212	2	771	100	0
4213	2	773	100	0
4214	2	774	100	0
4215	2	778	100	0
4216	2	779	100	0
4217	2	783	100	0
4218	2	785	100	0
4219	2	789	100	0
4220	2	790	100	0
4221	2	793	100	0
4222	2	794	100	0
4223	2	801	100	0
4224	2	810	100	0
4225	2	814	100	0
4226	2	815	100	0
4227	2	816	100	0
4228	2	818	100	0
4229	2	820	100	0
4230	2	822	100	0
4231	2	824	100	0
4232	2	825	100	0
4233	2	831	100	0
4234	2	834	100	0
4235	2	836	100	0
4236	2	837	100	0
4237	2	838	100	0
4238	2	839	100	0
4239	2	840	100	0
4240	2	841	100	0
4241	2	842	100	0
4242	2	843	100	0
4243	2	844	100	0
4244	2	847	100	0
4245	2	850	100	0
4246	2	851	100	0
4247	2	768	100	0
4248	2	835	100	0
4249	2	832	100	0
4250	2	812	100	0
4251	2	829	100	0
4252	2	828	100	0
4253	2	830	100	0
4254	2	813	100	0
4255	2	809	100	0
4256	2	772	100	0
4257	2	787	100	0
4258	2	788	100	0
4259	2	796	100	0
4260	2	791	100	0
4261	2	797	100	0
4262	2	776	100	0
4263	2	823	100	0
4264	2	821	100	0
4265	2	819	100	0
4266	2	817	100	0
4267	2	849	100	0
4268	2	775	100	0
4269	2	845	100	0
4270	2	846	100	0
4271	2	800	100	0
4272	2	781	100	0
4273	2	780	100	0
4274	2	807	100	0
4275	2	786	100	0
4276	2	795	100	0
4277	2	784	100	0
4278	2	792	100	0
4279	2	798	100	0
4280	2	803	100	0
4281	2	804	100	0
4282	2	802	100	0
4283	2	848	100	0
4284	2	826	100	0
4285	2	852	100	0
4286	2	853	100	0
4287	2	854	100	0
4288	2	855	100	0
4289	2	856	100	0
4290	2	857	100	0
4291	2	858	100	0
4292	2	859	100	0
4293	2	860	100	0
4294	2	861	100	0
4295	2	862	100	0
4296	2	863	100	0
4297	2	864	100	0
4298	2	865	100	0
4299	2	866	100	0
4300	2	867	100	0
4301	2	868	100	0
4302	2	869	100	0
4303	2	874	100	0
4304	2	877	100	0
4305	2	888	100	0
4306	2	893	100	0
4307	2	896	100	0
4308	2	897	100	0
4309	2	899	100	0
4310	2	902	100	0
4311	2	904	100	0
4312	2	908	100	0
4313	2	909	100	0
4314	2	915	100	0
4315	2	916	100	0
4316	2	918	100	0
4317	2	921	100	0
4318	2	923	100	0
4319	2	925	100	0
4320	2	927	100	0
4321	2	928	100	0
4322	2	929	100	0
4323	2	930	100	0
4324	2	931	100	0
4325	2	932	100	0
4326	2	871	100	0
4327	2	873	100	0
4328	2	882	100	0
4329	2	892	100	0
4330	2	891	100	0
4331	2	872	100	0
4332	2	878	100	0
4333	2	879	100	0
4334	2	884	100	0
4335	2	885	100	0
4336	2	895	100	0
4337	2	887	100	0
4338	2	926	100	0
4339	2	922	100	0
4340	2	924	100	0
4341	2	876	100	0
4342	2	890	100	0
4343	2	900	100	0
4344	2	894	100	0
4345	2	919	100	0
4346	2	910	100	0
4347	2	911	100	0
4348	2	913	100	0
4349	2	914	100	0
4350	2	912	100	0
4351	2	875	100	0
4352	2	907	100	0
4353	2	905	100	0
4354	2	906	100	0
4355	2	903	100	0
4356	2	889	100	0
4357	2	933	100	0
4358	2	934	100	0
4359	2	940	100	0
4360	2	941	100	0
4361	2	943	100	0
4362	2	946	100	0
4363	2	949	100	0
4364	2	954	100	0
4365	2	956	100	0
4366	2	958	100	0
4367	2	959	100	0
4368	2	961	100	0
4369	2	964	100	0
4370	2	971	100	0
4371	2	972	100	0
4372	2	974	100	0
4373	2	976	100	0
4374	2	980	100	0
4375	2	981	100	0
4376	2	988	100	0
4377	2	989	100	0
4378	2	990	100	0
4379	2	991	100	0
4380	2	992	100	0
4381	2	994	100	0
4382	2	996	100	0
4383	2	997	100	0
4384	2	998	100	0
4385	2	999	100	0
4386	2	1000	100	0
4387	2	1001	100	0
4388	2	1002	100	0
4389	2	1003	100	0
4390	2	1004	100	0
4391	2	1005	100	0
4392	2	1006	100	0
4393	2	1007	100	0
4394	2	1008	100	0
4395	2	1009	100	0
4396	2	1010	100	0
4397	2	1011	100	0
4398	2	1012	100	0
4399	2	1013	100	0
4400	2	993	100	0
4401	2	944	100	0
4402	2	995	100	0
4403	2	985	100	0
4404	2	945	100	0
4405	2	987	100	0
4406	2	967	100	0
4407	2	968	100	0
4408	2	966	100	0
4409	2	970	100	0
4410	2	969	100	0
4411	2	978	100	0
4412	2	977	100	0
4413	2	979	100	0
4414	2	984	100	0
4415	2	983	100	0
4416	2	982	100	0
4417	2	957	100	0
4418	2	960	100	0
4419	2	962	100	0
4420	2	963	100	0
4421	2	948	100	0
4422	2	950	100	0
4423	2	955	100	0
4424	2	953	100	0
4425	2	952	100	0
4426	2	951	100	0
4427	2	938	100	0
4428	2	937	100	0
4429	2	936	100	0
4430	2	1014	100	0
4431	2	1015	100	0
4432	2	1016	100	0
4433	2	1017	100	0
4434	2	1018	100	0
4435	2	1019	100	0
4436	2	1020	100	0
4437	2	1021	100	0
4438	2	1022	100	0
4439	2	1023	100	0
4440	2	1024	100	0
4441	2	1025	100	0
4442	2	1026	100	0
4443	2	1027	100	0
4444	2	1028	100	0
4445	2	883	100	0
4446	2	870	100	0
4447	2	898	100	0
4448	2	942	100	0
4449	2	880	100	0
4450	2	886	100	0
4451	2	881	100	0
4452	2	920	100	0
4453	2	935	100	0
4454	2	917	100	0
4455	2	901	100	0
4456	2	986	100	0
4457	2	973	100	0
4458	2	975	100	0
4459	2	965	100	0
4460	2	947	100	0
4461	2	939	100	0
4462	2	124	100	0
4463	2	599	100	0
4464	2	592	100	0
4465	2	692	100	0
4466	2	594	100	0
4467	2	680	100	0
4468	2	691	100	0
4469	2	604	100	0
4470	2	681	100	0
4471	2	231	100	0
4472	2	228	100	0
4473	2	42	100	0
4474	2	169	100	0
4475	2	588	100	0
4476	2	131	100	0
4477	2	148	100	0
4478	2	151	100	0
4479	2	578	100	0
4480	2	586	100	0
4481	2	601	100	0
4482	2	104	100	0
4483	2	56	100	0
4484	2	54	100	0
4485	2	4	100	0
4486	2	746	100	0
4487	2	751	100	0
4488	2	767	100	0
4489	2	757	100	0
4490	2	833	100	0
4491	2	811	100	0
4492	2	799	100	0
4493	2	806	100	0
4494	2	827	100	0
4495	2	777	100	0
4496	2	782	100	0
4497	2	808	100	0
4498	2	805	100	0
\.


--
-- Name: almacenarticulo_idalmacenarticulo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('almacenarticulo_idalmacenarticulo_seq', 4498, true);


--
-- Data for Name: articulo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY articulo (idarticulo, nombre, presentacion, descripcion, stockminimo, tamanio, unidad, codigobarras, idcategoria, precioventa, estado) FROM stdin;
5	Don Victorio Corbata Grande 250 g	bolsa		\N	\N		\N	7	1.20	A
6	Don Victorio Corbata Mediana 250 g	bolsa		\N	\N		\N	7	1.20	A
7	Don Victorio Lingúini gosso 500 g	bolsa		\N	\N		\N	7	2.30	A
8	Don Victorio Spaguetti 500 g	bolsa		\N	\N		\N	7	2.30	A
11	Sayón Canuto Rallado 250 g	bolsa		\N	\N		\N	7	1.00	A
12	Sayón Codito 250 g	bolsa		\N	\N		\N	7	1.00	A
13	Sayón Corbata 250 g	bolsa		\N	\N		\N	7	1.00	A
14	Sayón Tallarín 500 g	bidón		\N	\N		\N	7	1.90	A
16	Capri 1.5 L	botella		\N	\N		\N	7	3.70	A
17	Capri 5 L	bidón		\N	\N		\N	7	29.00	A
18	Cil 1 L	botella		\N	\N		\N	7	6.70	A
19	Cil 200 ml	botella		\N	\N		\N	7	1.70	A
20	Cil 5 L	bidón		\N	\N		\N	7	28.50	A
21	Cil 500 ml	botella		\N	\N		\N	7	3.50	A
23	Cocinero 5 L	bidón		\N	\N		\N	7	27.50	A
24	Cocinero 500 ml	botella		\N	\N		\N	7	3.50	A
25	Cristalino 1 L	botella		\N	\N		\N	7	6.70	A
26	Cristalino 200 ml	botella		\N	\N		\N	7	1.70	A
27	Cristalino 500 ml	botella		\N	\N		\N	7	3.50	A
28	Friol 1 L	botella		\N	\N		\N	7	6.50	A
29	Friol 200 ml	botella		\N	\N		\N	7	1.50	A
30	Friol 5 L	bidón		\N	\N		\N	7	27.50	A
31	Friol 500 ml	botella		\N	\N		\N	7	3.40	A
33	Ideal 200 ml	botella		\N	\N		\N	7	2.00	A
36	Primor Clásico 5 L	bidón		\N	\N		\N	7	30.00	A
37	Primor Clásico 500 ml	botella		\N	\N		\N	7	3.80	A
38	Primor Premium 1 L	botella		\N	\N		\N	7	7.40	A
39	Primor Premium 500 ml	botella		\N	\N		\N	7	3.90	A
40	Sao 1 L	botella		\N	\N		\N	7	6.00	A
43	Tondero 1 L	botella		\N	\N		\N	7	5.50	A
44	Tondero 500 ml	botella		\N	\N		\N	7	3.00	A
45	graté de atún florida 165 g	lata		\N	\N		\N	7	2.50	A
46	filete de atún florida 170 g	lata		\N	\N		\N	7	5.50	A
47	filete de atún gloria 170 g	lata		\N	\N		\N	7	5.50	A
48	sardinas en salsa de tomate real 425 g	lata		\N	\N		\N	7	7.00	A
50	graté de sardina gloria 170 g	lata		\N	\N		\N	7	2.50	A
51	trozos de atún monteverde 170 g	lata		\N	\N		\N	7	4.50	A
52	atún desmenuzado antartic 170 g	lata		\N	\N		\N	7	2.80	A
53	filete de atún marinero 170 g	lata		\N	\N		\N	7	4.50	A
60	rocoto molido alacena 100 ml	sachet		\N	\N		\N	7	1.80	A
35	Primor Clásico 1 L	botella		0	\N		7750243042949	7	7.00	A
15	Capri 1 L	botella		0	\N		7750243039697	7	6.70	A
22	Cocinero 1 L	botella		0	\N		7750243042338	7	6.70	A
32	Ideal 1L	botella		0	\N		7790070219343	7	6.80	A
34	Ideal 500 ml	botella		0	\N		7790070219350	7	3.80	A
41	Sao 200 ml	botella		0	\N		7752028000168	7	1.70	A
49	filete de atún compass 170 g	lata		0	\N		7751158002974	7	5.50	A
57	mayonesa alacena 200 ml	sachet		0	\N		7750243027281	7	4.20	A
58	mayonesa alacena 115 ml	sachet		0	\N		7750243035248	7	2.20	A
61	crema de ají tarí 85 g	sachet		0	\N		7750243030328	7	2.00	A
59	ketchup alacena 100 g	sachet		0	\N		7750243003070	7	1.80	A
55	salsa completa don vittorio 200 g	sachet		0	\N		7750243043816	7	1.80	A
9	Molitalia Canuto 250 g	bolsa		0	\N		7750885176125	7	1.10	A
67	Molitalia Aritos 250 g	bolsa		0	\N		7750885176286	7	0.00	A
68	Marco Polo Aritos 250 g	bolsa		0	\N		7750885429719	7	0.00	A
10	Molitalia Tallarin 500 g	bolsa		0	\N		7750885003049	7	2.20	A
121	ketchup B&D 100 g	sachet		\N	\N		\N	7	1.40	A
123	sopa instantánea ajinomen gallina 86 g	bolsa		\N	\N		\N	7	1.00	A
125	sopa instantánea ajinomen carne 86 g	bolsa		\N	\N		\N	7	1.00	A
126	sopa instantánea ajinomen oriental 86 g	bolsa		\N	\N		\N	7	1.00	A
127	mezcla para apanar maggie 80 g	bolsa		\N	\N		\N	7	1.50	A
128	harina favorita cocinera  180 g	bolsa		\N	\N		\N	7	0.70	A
129	sémola molitalia 200 g	bolsa		\N	\N		\N	7	1.20	A
130	sal yodada de mesa marina 1 Kg	bolsa		\N	\N		\N	7	1.10	A
133	vinagre del firme blanco 1 L	sachet		\N	\N		\N	7	3.00	A
135	vinagre compass blanco 1 L	botella		\N	\N		\N	7	2.50	A
136	vinagre compass tinto 1 L	botella		\N	\N		\N	7	2.50	A
143	gelatina negrita piña 80 g	bolsa		\N	\N		\N	7	1.50	A
144	gelatina negrita fresa 80 g	bolsa		\N	\N		\N	7	1.50	A
145	gelatina negrita naranja 80 g	bolsa		\N	\N		\N	7	1.50	A
146	gelatina royal fresa 180 g	bolsa		\N	\N		\N	7	3.20	A
152	flan royal vainilla 80 g	bolsa		\N	\N		\N	7	2.20	A
153	flan royal manjar 80 g	bolsa		\N	\N		\N	7	2.20	A
154	pudín royal xxx g	bolsa		\N	\N		\N	7	3.00	A
155	polvo de hornear universal 25 g	bolsa		\N	\N		\N	7	1.00	A
156	colapiz universal 20 g	bolsa		\N	\N		\N	7	1.70	A
157	manjar blanco bonlé 200 g	bolsa		\N	\N		\N	7	2.80	A
158	manjar blanco nestlé 200 g	bolsa		\N	\N		\N	7	3.50	A
159	esencia de vainilla negrita 30 ml	botella		\N	\N		\N	7	0.60	A
162	café para pasar altomayo 50 g	sobre		\N	\N		\N	7	1.50	A
163	refrescos negrita maracuyá 15 g	sobre		\N	\N		\N	7	0.80	A
164	refrescos negrita naranja 15 g	sobre		\N	\N		\N	7	0.80	A
165	refrescos negrita granadilla 15 g	sobre		\N	\N		\N	7	0.80	A
166	refrescos negrita chicha morada 15 g	sobre		\N	\N		\N	7	0.80	A
167	frutísimos negrita chicha morada 35 g	sobre		\N	\N		\N	7	1.00	A
168	mermelada gloria fresa 100 g	sachet		\N	\N		\N	7	1.20	A
171	duraznos en mitades dos caballos 820 g	lata		\N	\N		\N	7	8.50	A
172	duraznos en mitades aconcagua 820 g	lata		\N	\N		\N	7	8.20	A
175	agua Cielo Con Gas 625 ml	botella		\N	\N		\N	8	1.00	A
176	agua Cielo Q10 350 ml	botella		\N	\N		\N	8	1.00	A
177	agua Cielo Q10 625 ml	botella		\N	\N		\N	8	1.50	A
122	sopa instantánea ajinomen pollo 86 g	bolsa		0	\N		7754487000307	7	1.00	A
134	vinagre del firme tinto 1 L	sachet		0	\N		7753121109161	7	3.00	A
137	sillao tito 500 ml	botella		0	\N		7753121219570	7	2.50	A
138	sillao tito 150 ml	botella		0	\N		7753121001151	7	1.00	A
161	Café tostado y molido cafetal 50 g	sobre		0	\N		7754308000172	7	1.80	A
139	maizena duryea 100 g	caja		0	\N		7752285104814	7	1.30	A
132	harina blanca flor 1 Kg	bolsa		0	\N		7750243006880	7	5.80	A
160	esencia de vainilla negrita 90 ml	botella		0	\N		7750243011884	7	1.00	A
150	mazamorra negrita 170 g	bolsa		0	\N		7750243041683	7	2.50	A
149	gelatina royal limón 160 g	bolsa		0	\N		7622300794880	7	3.20	A
147	gelatina royal naranja 160 g	bolsa		0	\N		7622300794859	7	3.20	A
141	gelatina negrita fresa 180 g	bolsa		0	\N		7750243044080	7	2.80	A
140	gelatina negrita piña 180 g	bolsa		0	\N		7750243044097	7	2.80	A
142	gelatina negrita naranja 180 g	bolsa		0	\N		7750243044103	7	2.80	A
170	duraznos en mitades fanny 820 g	lata		0	\N		7750885006088	7	8.50	A
174	coctel de frutas florida 820 g	lata		0	\N		7751158002356	7	9.20	A
173	piña en rodajas florida 560 g	lata		0	\N		7751158015165	7	4.50	A
103	graté de atún compass 165 g	lata		0	\N		7751158005883	7	3.20	A
105	filete de atún sao 170 g	lata		0	\N		7752028000267	7	5.50	A
120	mostaza libbys 100 g	sachet		0	\N		7613030779070	7	1.80	A
114	ketchup Libbys 100 g	sachet		0	\N		7613030778943	7	1.50	A
178	agua Cielo Sin Gas 2.5 L	botella		\N	\N		\N	8	2.50	A
179	agua Cielo Sin Gas 7 L	bidón		\N	\N		\N	8	6.50	A
180	agua Cielo Sin Gas 625ml	botella		\N	\N		\N	8	1.00	A
181	agua San Carlos 2.5 L	botella		\N	\N		\N	8	2.80	A
182	agua San Carlos Con Gas 500 ml	botella		\N	\N		\N	8	1.30	A
183	agua San Carlos Sin Gas 600 ml	botella		\N	\N		\N	8	1.20	A
184	agua San Luis Con Gas 2.5 L	botella		\N	\N		\N	8	3.00	A
185	agua San Luis Con Gas 625 ml	botella		\N	\N		\N	8	1.30	A
186	agua San Luis Sin Gas 20 L	caja		\N	\N		\N	8	21.50	A
187	agua San Luis Sin Gas 20 L	bidón		\N	\N		\N	8	17.50	A
188	agua San Luis Sin Gas 500 ml	botella		\N	\N		\N	8	1.20	A
189	agua San Luis Sin Gas 625 ml	botella		\N	\N		\N	8	1.30	A
190	agua San Luis Sin Gas 7 L	bidón		\N	\N		\N	8	8.50	A
191	agua San Lus Sin Gas 2.5 L	botella		\N	\N		\N	8	3.00	A
192	agua San Mateo Con Gas 2.5 L	botella		\N	\N		\N	8	3.20	A
193	agua San Mateo Con Gas 600 ml	botella		\N	\N		\N	8	1.40	A
194	agua San Mateo Sin Gas 2.5 L	botella		\N	\N		\N	8	3.00	A
195	agua San Mateo Sin Gas 600 ml	botella		\N	\N		\N	8	1.30	A
196	agua San Mateo Sin Gas 7 L	bidón		\N	\N		\N	8	8.50	A
197	agua casinelli 20 L	bidón		\N	\N		\N	8	12.50	A
198	Papel higienico suave jumbo x 2	rollo		\N	\N		\N	6	1.80	A
199	Papel higienico suave jumbo x 4	rollo		\N	\N		\N	6	3.50	A
200	suave jumbo x 6	rollo		\N	\N		\N	6	5.20	A
201	suave jumbo x 20 rollos (plancha)	rollo		\N	\N		\N	6	16.50	A
202	Papel higienico suave extra x 2	rollo		\N	\N		\N	6	1.50	A
203	Papel higienico suave extra x 20 – Plancha	rollo		\N	\N		\N	6	12.80	A
204	papel higienico noble x 2	rollo		\N	\N		\N	6	1.30	A
205	Papel higienico elite doble hoja naranja x 2	rollo		\N	\N		\N	6	1.50	A
206	Papel higienico elite doble hoja naranja x 20	rollo		\N	\N		\N	6	12.80	A
207	Papel higienico elite doble hoja maxima suavi	rollo		\N	\N		\N	6	1.80	A
208	Papel higienico elite doble hoja maxima suavi	rollo		\N	\N		\N	6	3.50	A
209	Papel higienico elite doble hoja maxima suavi	rollo		\N	\N		\N	6	5.20	A
210	Papel higienico elite doble hoja maxima suavi	rollo		\N	\N		\N	6	16.50	A
211	papel higienico elite ultra doble hoja purpur	rollo		\N	\N		\N	6	1.80	A
212	Toallas higienicas always pink pack 8 unidade	paquete		\N	\N		\N	6	3.70	A
213	Toallas higienicas always active pack 8 unida	paquete		\N	\N		\N	6	3.70	A
214	Toallas higienicas always suave pack 8 unidad	paquete		\N	\N		\N	6	3.00	A
215	Toallas higienicas always noche regular pack 	paquete		\N	\N		\N	6	5.50	A
216	Toallas higienicas always noche ultra fino pa	paquete		\N	\N		\N	6	5.50	A
217	Toallas higienicas ladysoft delgada pack 10 u	paquete		\N	\N		\N	6	2.70	A
218	Toallas higienicas ladysoft natural normal pa	paquete		\N	\N		\N	6	2.50	A
219	Toallas higienicas ladysoft nocturna normal p	paquete		\N	\N		\N	6	3.80	A
220	Toallas higienicas ladysoft normal pack 10 un	paquete		\N	\N		\N	6	2.50	A
221	Toallas higienicas ladysoft planes pack 8 uni	paquete		\N	\N		\N	6	1.50	A
222	Toallas higienicas nosotras invisible rapigel	paquete		\N	\N		\N	6	3.70	A
223	toallas higienicas nosotras clasica tela pack	paquete		\N	\N		\N	6	3.50	A
224	toallas higienicas nosotras natural alas tela	paquete		\N	\N		\N	6	3.30	A
225	toallas higienicas kotex normal pack 10 unida	paquete		\N	\N		\N	6	3.50	A
226	toallas higienicas nosotras natural buenas no	paquete		\N	\N		\N	6	5.90	A
227	Practipañal plenitud pack 10 unidades	paquete		\N	\N		\N	6	3.70	A
229	Pañales Pampers tripack g	paquete		\N	\N		\N	6	2.70	A
230	Pañales Pampers tripack xg	paquete		\N	\N		\N	6	3.00	A
232	Pañales Pampers m x 11	paquete		\N	\N		\N	6	6.90	A
233	Pañales Pampers g x 10	paquete		\N	\N		\N	6	6.90	A
234	Pañales Pampers xg x 8	paquete		\N	\N		\N	6	6.90	A
235	Pañales Pampers xxg x 8	paquete		\N	\N		\N	6	6.90	A
236	Pañales huggies m	unidad		\N	\N		\N	6	0.80	A
237	Pañales huggies g	unidad		\N	\N		\N	6	0.90	A
238	Pañales huggies xg	unidad		\N	\N		\N	6	1.00	A
239	Pañales huggies xxg	unidad		\N	\N		\N	6	1.10	A
240	Pañales huggies m x -	paquete		\N	\N		\N	6	44.00	A
241	Pañales huggies g x -	paquete		\N	\N		\N	6	44.00	A
242	Pañales huggies xg x 52	paquete		\N	\N		\N	6	44.00	A
243	Pañales huggies xxg x 48	paquete		\N	\N		\N	6	44.00	A
244	Pañales Babysec ultra m	unidad		\N	\N		\N	6	0.70	A
245	Pañales Babysec ultra g	unidad		\N	\N		\N	6	0.80	A
246	Pañales Babysec ultra xg	unidad		\N	\N		\N	6	0.90	A
247	Pañales Babysec ultra xxg	unidad		\N	\N		\N	6	1.00	A
248	pañales babysec premium xxg x 14	paquete		\N	\N		\N	6	13.00	A
249	pañales babysec premium xg x 16	paquete		\N	\N		\N	6	13.00	A
250	pañales babysec premium g x 18	paquete		\N	\N		\N	6	13.00	A
251	Pasta Dental Kolynos 22 ml	caja		\N	\N		\N	6	1.50	A
252	Pasta Dental Kolynos 75 ml	caja		\N	\N		\N	6	3.00	A
253	Pasta Dental Kolynos 100 ml	caja		\N	\N		\N	6	4.00	A
254	Pasta Dental Kolynos Herbal 90 g	caja		\N	\N		\N	6	2.00	A
255	Pasta Dental Colgate Triple Acción Menta Orig	caja		\N	\N		\N	6	1.60	A
256	Pasta Dental Colgate 90 g	caja		\N	\N		\N	6	2.10	A
257	Pasta Dental Colgate Total 12 50 ml	caja		\N	\N		\N	6	4.50	A
334	Shampoo Savital Savila 530 ml	frasco		\N	\N		\N	6	14.00	A
258	Pasta Dental Colgate Máxima Protección Anti C	caja		\N	\N		\N	6	2.90	A
259	Pasta Dental Colgate Máxima Protección Anti C	caja		\N	\N		\N	6	3.50	A
260	Pasta Dental Colgate Triple Acción Extra Blan	caja		\N	\N		\N	6	2.50	A
261	Pasta Dental Colgate Triple Acción Menta Orig	caja		\N	\N		\N	6	4.50	A
262	Pasta Dental Colgate Triple Acción Menta Orig	caja		\N	\N		\N	6	7.60	A
263	Pasta Dental Oral B Complete 50 ml + H&S	caja		\N	\N		\N	6	2.90	A
264	Pasta Dental Oral B 1 2 3 50 ml	caja		\N	\N		\N	6	2.50	A
265	Pasta Dental Oral B 3DWHITE 53 ml	caja		\N	\N		\N	6	4.50	A
266	Pasta Dental Oral B Complete con Enjuage Buca	caja		\N	\N		\N	6	4.30	A
267	Pasta Dental Dento 150 ml + Cepillo	caja		\N	\N		\N	6	4.00	A
268	Pasta Dental Dento 75 ml	caja		\N	\N		\N	6	2.00	A
269	Pasta Dental Dento 3 75 ml	caja		\N	\N		\N	6	2.50	A
270	Pasta Dental Dentito Chicle Globo 75 ml	caja		\N	\N		\N	6	2.00	A
271	Pasta Dental Dentito Chicha Morada 75 ml	caja		\N	\N		\N	6	2.00	A
272	Jaboncillo Protex Avena 130 g	paquete		\N	\N		\N	6	3.00	A
273	Jaboncillo Protex Fresh 130 g	paquete		\N	\N		\N	6	3.00	A
274	Jaboncillo Protex Limpieza Profunda 130 g	paquete		\N	\N		\N	6	3.00	A
275	Jaboncillo Protex Avena 75 g	paquete		\N	\N		\N	6	1.80	A
276	Jaboncillo Protex Fresh 75 g	paquete		\N	\N		\N	6	1.80	A
277	Jaboncillo Protex Limpieza Profunda 75 g	paquete		\N	\N		\N	6	1.80	A
278	Jaboncillo Rexona Acqua Fresh 90 g	paquete		\N	\N		\N	6	1.60	A
279	Jaboncillo Rexona Menta Fresh 90 g	paquete		\N	\N		\N	6	1.60	A
280	Jaboncillo Rexona Sensible Fresh 90g	paquete		\N	\N		\N	6	1.60	A
281	Jaboncillo Rexona Nutritiva Fresh 90g	paquete		\N	\N		\N	6	1.60	A
282	Jaboncillo Rexona Acqua Fresh 125 g	paquete		\N	\N		\N	6	2.70	A
283	Jaboncillo Rexona Menta Fresh 125 g	paquete		\N	\N		\N	6	2.70	A
284	Jaboncillo Rexona Sensible Fresh 125 g	paquete		\N	\N		\N	6	2.70	A
285	Jaboncillo Rexona Nutritiva Fresh 125 g	paquete		\N	\N		\N	6	2.70	A
286	Jaboncillo LUX Tono Luminoso 90 g	paquete		\N	\N		\N	6	1.50	A
287	Jaboncillo LUX Suavidad De Pétalos 90 g	paquete		\N	\N		\N	6	1.50	A
288	Jaboncillo LUX Refréscate 90 g	paquete		\N	\N		\N	6	1.50	A
289	Jaboncillo LUX Tono Luminoso 125 g	paquete		\N	\N		\N	6	2.50	A
290	Jaboncillo LUX Suavidad De Pétalos 125 g	paquete		\N	\N		\N	6	2.50	A
291	Jaboncillo LUX Refréscate 125 g	paquete		\N	\N		\N	6	2.50	A
292	Jaboncillo Palmolive Sensación Humectante 75 	paquete		\N	\N		\N	6	1.40	A
293	Jaboncillo Palmolive Exfoliación Diaria 75 g	paquete		\N	\N		\N	6	1.40	A
294	Jaboncillo Spa Humectante Coco 90 g	paquete		\N	\N		\N	6	1.50	A
295	Jaboncillo Spa Energizante Naranja y Lima 90 	paquete		\N	\N		\N	6	1.50	A
296	Jaboncillo Nivea 90 g	paquete		\N	\N		\N	6	1.30	A
297	Jaboncillo Etiquet Exfoliante 90 g	paquete		\N	\N		\N	6	1.70	A
298	Jaboncillo Etiquet 90 g	paquete		\N	\N		\N	6	1.50	A
299	Jaboncillo Camay Clásico 90 g	paquete		\N	\N		\N	6	1.50	A
300	Jaboncillo Camay Pink 90 g	paquete		\N	\N		\N	6	1.50	A
301	Jaboncillo Camay Romantic 90 g	paquete		\N	\N		\N	6	1.50	A
302	Jaboncillo Neko Extra Protección 75 g	paquete		\N	\N		\N	6	1.90	A
303	Jaboncillo Dermex Glicerina Kiwi Semillas Nat	paquete		\N	\N		\N	6	2.20	A
304	Jaboncillo Dermex Glicerina Té Verde 100 g	paquete		\N	\N		\N	6	2.20	A
305	Jaboncillo Dermex Glicerina Guaraná 100 g	paquete		\N	\N		\N	6	2.20	A
306	Jaboncillo Dermex Glicerina Mujer 100 g	paquete		\N	\N		\N	6	2.20	A
307	Jaboncillo Dermex Glicerina Maracuyá 100 g	paquete		\N	\N		\N	6	2.20	A
308	Jaboncillo Jhonsons Avena y Aceite de Almendr	paquete		\N	\N		\N	6	3.00	A
309	Jaboncillo Heno de Pravia Original 85 g	paquete		\N	\N		\N	6	2.20	A
310	Jaboncillo Heno de Pravia Frescura y Protecci	paquete		\N	\N		\N	6	2.20	A
311	Jaboncillo Heno de Pravia Con Crema Hidratant	paquete		\N	\N		\N	6	2.20	A
312	Jaboncillo Heno de Pravia Original 150 g	paquete		\N	\N		\N	6	3.60	A
313	Jaboncillo Heno de Pravia Frescura y Protecci	paquete		\N	\N		\N	6	3.60	A
314	Jaboncillo Heno de Pravia Con Crema Hidratant	paquete		\N	\N		\N	6	3.60	A
315	Jaboncillo Moncler 160 g	caja		\N	\N		\N	6	3.00	A
316	Jaboncillo Dove Exfoliación Diaria 90 g	caja		\N	\N		\N	6	3.00	A
317	Jaboncillo Dove Energiaznte 90 g	caja		\N	\N		\N	6	3.00	A
318	Toallitas Húmedas babysec ultra 72 unidades	paquete		\N	\N		\N	6	7.00	A
319	Toallitas húmedas pampers 64 und	paquete		\N	\N		\N	6	9.80	A
320	Toallitas húmedas huggies clasic 70 und	paquete		\N	\N		\N	6	6.20	A
321	toallitas húmedas huggies recien nacido 48 un	paquete		\N	\N		\N	6	10.50	A
322	toallitas húmedas babysec premium 50 und	paquete		\N	\N		\N	6	6.10	A
323	Shampoo pantene pro-v rizos definidos 400 ml	frasco		\N	\N		\N	6	18.00	A
324	Shampoo pantene pro-v hidrocauterización 400 	frasco		\N	\N		\N	6	18.00	A
325	Shampoo con Acondicionador pantene pro-v cuid	frasco		\N	\N		\N	6	18.00	A
326	Shampoo pantene pro-v rizos definidos 200 ml	frasco		\N	\N		\N	6	12.00	A
327	Shampoo pantene pro-v hidrocauterización 200 	frasco		\N	\N		\N	6	12.00	A
328	Shampoo pantene pro-v restauración 200 ml	frasco		\N	\N		\N	6	12.00	A
329	Shampoo pantene pro-v cuidado brillo extremo 	frasco		\N	\N		\N	6	12.00	A
330	Shampoo con Acondicionador pantene pro-v rizo	frasco		\N	\N		\N	6	4.80	A
331	Shampoo con Acondicionador pantene pro-v cuid	frasco		\N	\N		\N	6	4.80	A
332	Shampoo pantene pro-v restauración 100 ml	frasco		\N	\N		\N	6	4.80	A
333	Acondicinador Pantene Pro-V rizos definidos 2	frasco		\N	\N		\N	6	11.60	A
335	Acondicionador Head & Shoulders fuerza rejuve	frasco		\N	\N		\N	6	18.50	A
336	shampoo Head & Shoulders Manzana Fresh 400 ml	frasco		\N	\N		\N	6	18.90	A
337	shampoo Head & Shoulders Men 400 ml	frasco		\N	\N		\N	6	18.90	A
338	shampoo Head & Shoulders Limpieza Renovadora 	frasco		\N	\N		\N	6	18.90	A
339	shampoo Head & Shoulders Protección Caída 400	frasco		\N	\N		\N	6	18.90	A
340	shampoo Head & Shoulders Suave y Manejable 40	frasco		\N	\N		\N	6	18.90	A
341	Crema para Peinar Pantane Pro-V Rizos definid	frasco		\N	\N		\N	6	4.80	A
342	shampoo Head & Shoulders Limpieza Renovadora 	frasco		\N	\N		\N	6	4.80	A
343	shampoo Head & Shoulders men 90 ml	frasco		\N	\N		\N	6	4.80	A
344	shampoo Head & Shoulders Men 200 ml	frasco		\N	\N		\N	6	12.50	A
345	shampoo Head & Shoulders Limpieza Renovadora 	frasco		\N	\N		\N	6	12.50	A
346	shampoo Head & Shoulders Protección Caída 200	frasco		\N	\N		\N	6	12.50	A
347	shampoo Head & Shoulders Suave y Manejable 20	frasco		\N	\N		\N	6	12.50	A
348	Desodorante All Spice After Party 150ml	spray		\N	\N		\N	6	11.50	A
349	Desodorante All Spice Pure Sport 150ml	spray		\N	\N		\N	6	11.50	A
350	Desodorante All Spice Fresh 50 gr	barra		\N	\N		\N	6	9.50	A
351	Desodorante All Spice Show Time 50 gr	barra		\N	\N		\N	6	9.50	A
352	Desodorante All Spice After Party 50 gr	barra		\N	\N		\N	6	9.50	A
353	Desodorante All Spice Champion 50 gr	barra		\N	\N		\N	6	9.50	A
354	Desodorante Gillete 150 ml	spray		\N	\N		\N	6	12.50	A
355	Desodorante Rexona Teens Tropical Energy 108 	spray		\N	\N		\N	6	5.80	A
356	Desodorante Rexona Teens Beauty 108 ml	spray		\N	\N		\N	6	5.80	A
357	Desodorante Rexona Women 108 ml	spray		\N	\N		\N	6	5.80	A
358	Desodorante Rexona UV8 50 ml x 2	caja		\N	\N		\N	6	10.20	A
359	Desodorante Rexona cotton 50 ml x 2	caja		\N	\N		\N	6	10.20	A
360	Crema Desodorante Etiquet 55 gr	caja		\N	\N		\N	6	12.50	A
361	Desodorante Rexona Men Sensitive 50 gr	barra		\N	\N		\N	6	9.50	A
362	Desodorante Rexona Men Extreme 50 gr	barra		\N	\N		\N	6	9.50	A
363	Desodorante Rexona Men UV8 50 gr	barra		\N	\N		\N	6	9.50	A
364	Desodorante Rexona Men Quatom 50 gr	barra		\N	\N		\N	6	9.50	A
365	Desodorante Rexona Men Xtracool 50 gr	barra		\N	\N		\N	6	9.50	A
366	Desodorante Rexona Women Cotton 50 gr	barra		\N	\N		\N	6	9.50	A
367	Desodorante Rexona Women extra fresh 50 gr	barra		\N	\N		\N	6	9.50	A
369	Desodorante Rexona Women Active Emotion 50 gr	barra		\N	\N		\N	6	9.50	A
370	Desodorante Rexona Women Crystal 50 gr	barra		\N	\N		\N	6	9.50	A
371	Crema para Peinar Sedal yuko 300 ml	frasco		\N	\N		\N	6	10.60	A
372	Crema para Peinar Sedal ceramidas 300 ml	frasco		\N	\N		\N	6	10.60	A
373	Shampoo konzil 375 ml + 200 ml	frasco		\N	\N		\N	6	16.50	A
374	Espuma para afeitar Gillete 250 ml	spray		\N	\N		\N	6	15.90	A
375	Talco Desodorante Hansaplast 250 gr	frasco		\N	\N		\N	6	12.00	A
376	Crema Corporal Nivea Hidratación Express 400 	frasco		\N	\N		\N	6	17.30	A
377	Enguaje Bucal Listerine Antimanchas 500 ml	frasco		\N	\N		\N	6	15.50	A
378	Enguaje Bucal Listerine Zero 500 ml	frasco		\N	\N		\N	6	15.00	A
379	Desodorante Dove 169 ml + Jabón	spray		\N	\N		\N	6	12.50	A
380	Desodorante Dove Dermo Aclarante 169 ml + Jab	spray		\N	\N		\N	6	12.50	A
381	Shampoo Dove + Acondicionador Dove 400 ml	frasco		\N	\N		\N	6	26.50	A
382	Shampoo konzil 200 ml	frasco		\N	\N		\N	6	8.90	A
383	Nivea Creme 30 ml	lata		\N	\N		\N	6	3.20	A
384	Leche de Magnesia 120 ml	frasco		\N	\N		\N	6	8.50	A
385	Leche de Magnesia cereza 120 ml	frasco		\N	\N		\N	6	8.50	A
386	Leche de Magnesia ciruela 120 ml	frasco		\N	\N		\N	6	8.50	A
387	Leche de Magnesia durazno 120 ml	frasco		\N	\N		\N	6	8.50	A
388	Shampoo Amens 112 ml	frasco		\N	\N		\N	6	6.90	A
389	Quita esmalte sivela 30 ml	frasco		\N	\N		\N	6	1.20	A
390	Esmalte DLady 7 ml	frasco		\N	\N		\N	6	1.00	A
391	Quita esmalte Samy 30 ml	frasco		\N	\N		\N	6	1.50	A
392	Quita esmalte Samy 75 ml	frasco		\N	\N		\N	6	2.50	A
393	Enguaje Bucal Listerine Zero 180 ml	frasco		\N	\N		\N	6	5.50	A
394	Enguaje Bucal Listerine Antimanchas 250 ml	frasco		\N	\N		\N	6	7.90	A
395	Enguaje Bucal Listerine Zero 500 ml + 180 ml	frasco		\N	\N		\N	6	19.00	A
396	Enguaje Bucal Listerine Antimanchas 500 ml + 	frasco		\N	\N		\N	6	21.50	A
397	Jabón Líquido Aval Pétalos de rosa 400 ml	frasco		\N	\N		\N	6	5.50	A
398	Jabón Líquido Aval Almendras y Miel 400 ml	frasco		\N	\N		\N	6	5.50	A
399	Jabón Líquido Aval Lavanda Francesa 400 ml	frasco		\N	\N		\N	6	5.50	A
400	Jabón Líquido Aval Pasión Cítrica 400 ml	frasco		\N	\N		\N	6	5.50	A
401	Jabón Líquido Aval Fresco Limón 400 ml	frasco		\N	\N		\N	6	5.50	A
402	Shampoo Jhonsons Baby 100 ml	frasco		\N	\N		\N	6	4.90	A
403	Talco Desodorante Hansaplast 120 gr	frasco		\N	\N		\N	6	7.90	A
404	Espuma para afeitar Gillete Piel Sensible 322	spray		\N	\N		\N	6	17.90	A
405	Gel para peinar ego black 200 gr	pote		\N	\N		\N	6	6.40	A
406	Gel para peinar ego black 100 gr	pote		\N	\N		\N	6	2.50	A
407	Gel para peinar ego xtreme 100 gr	pote		\N	\N		\N	6	3.00	A
408	Gel para peinar ego xtreme 27 gr	sachet		\N	\N		\N	6	0.70	A
409	Desodorante Rexona Men UV8 98ml	spray		\N	\N		\N	6	5.80	A
410	Desodorante Rexona Men Active 98 ml	spray		\N	\N		\N	6	5.80	A
411	Desodorante Rexona Women 108 ml	spray		\N	\N		\N	6	5.80	A
412	Desodorante Nivea 43 gr	barra		\N	\N		\N	6	8.30	A
413	Desodorante Rexona Women Bamboo 30 ml	rollon		\N	\N		\N	6	3.00	A
414	Desodorante Rexona Men UV8 30 ml	rollon		\N	\N		\N	6	3.00	A
415	Desodorante Gillete triple protection system 	rollon		\N	\N		\N	6	7.50	A
416	Desodorante Gillete sport 60 gr	rollon		\N	\N		\N	6	7.50	A
417	Desodorante Axe Black 90 ml	spray		\N	\N		\N	6	5.80	A
418	Desodorante Axe Exite 90 ml	spray		\N	\N		\N	6	5.80	A
419	Desodorante Axe Anarchy 90 ml	spray		\N	\N		\N	6	5.80	A
420	Oxigenta higora 60 ml	frasco		\N	\N		\N	6	2.60	A
421	Tinte para cabello pallete	caja		\N	\N		\N	6	12.50	A
422	Protector solar banana boat ultra defense 50 	tubo		\N	\N		\N	6	8.50	A
423	Protector solar banana boat ultra defense kid	tubo		\N	\N		\N	6	8.50	A
424	Espuma de afeitar Aval hidratante 200 ml	spray		\N	\N		\N	6	7.50	A
425	Espuma de afeitar Aval extra proteccion 200 m	spray		\N	\N		\N	6	7.50	A
426	Shampoo Savital 90 ml	bolsa		\N	\N		\N	6	2.00	A
427	Prestobarba Guillette dos hojas	paquete		\N	\N		\N	6	2.50	A
428	Prestobarba Guillette tres hojas hombre	paquete		\N	\N		\N	6	3.50	A
429	Prestobarba Guillette tres hojas mujer	paquete		\N	\N		\N	6	3.50	A
430	prestobarba shick dos	paquete		\N	\N		\N	6	1.20	A
431	Prestobarba shick dos hojas hombre	paquete		\N	\N		\N	6	2.50	A
432	prestobarba shick tres hojas hombre	paquete		\N	\N		\N	6	3.50	A
433	Prestobarba shick dos hojas mujer	paquete		\N	\N		\N	6	2.50	A
434	prestobarba shick tres hojas mujer	paquete		\N	\N		\N	6	3.50	A
435	Prestobarba shick 4 hijas titanium	paquete		\N	\N		\N	6	4.00	A
436	Prestobarba Colt II	paquete		\N	\N		\N	6	1.40	A
437	Cepillo de Dientes Kolynos Master Plus	estuche		\N	\N		\N	6	1.70	A
438	Cepillo de Dientes colgate extra suave 5 años	estuche		\N	\N		\N	6	2.00	A
439	cepillo de dientes oral b 1 2 3 con tapa	estuche		\N	\N		\N	6	1.70	A
440	cepillo de dientes pro doble acción	estuche		\N	\N		\N	6	5.00	A
441	cepillo de dientes pro kids suave	estuche		\N	\N		\N	6	4.50	A
442	cepillo de dientes colgate triple acción 1 2 	estuche		\N	\N		\N	6	2.50	A
443	cepillo de dientes colgate premier	estuche		\N	\N		\N	6	1.80	A
444	cepillo de dientes dento galaxy niños	estuche		\N	\N		\N	6	2.00	A
445	desodorante lady speed stick double defense 1	sachet		\N	\N		\N	6	0.70	A
446	desodorante lady speed stick active 12 g	sachet		\N	\N		\N	6	0.70	A
447	desodorante rexona nutritive 10 g	sachet		\N	\N		\N	6	0.70	A
448	desodorante rexona men v8 15 g	sachet		\N	\N		\N	6	0.70	A
449	desodorante nivea men invisible 10 ml	sachet		\N	\N		\N	6	1.00	A
450	crema ponds aclaradora 15 g	sachet		\N	\N		\N	6	0.80	A
451	crema ponds de limpieza 10 g	sachet		\N	\N		\N	6	0.80	A
452	crema ponds rejuvenece 10 g	sachet		\N	\N		\N	6	0.80	A
453	crema ponds humectante 10 g	sachet		\N	\N		\N	6	0.80	A
454	pilas panasonic AA x2	paquete		\N	\N		\N	9	1.40	A
455	pilas panasonic AAA x2	paquete		\N	\N		\N	9	1.40	A
456	pilas panasonic D x2	paquete		\N	\N		\N	9	2.80	A
457	pilas duracell AAA x2	paquete		\N	\N		\N	9	4.50	A
458	pilas duracell AA x2	paquete		\N	\N		\N	9	4.50	A
459	batería duracell 9V x1	paquete		\N	\N		\N	9	9.50	A
460	7up 1.5 L	botella		\N	\N		\N	2	4.00	A
461	7up 3 L	botella		\N	\N		\N	2	6.50	A
462	7up 355 ml	lata		\N	\N		\N	2	2.00	A
463	7up 500 ml	botella		\N	\N		\N	2	1.50	A
464	Aquarius Granadilla 1.5 L	botella		\N	\N		\N	2	5.00	A
465	Aquarius Granadilla 500 ml	botella		\N	\N		\N	2	2.00	A
466	Aquarius Manzana 1.5 L	botella		\N	\N		\N	2	5.00	A
467	Aquarius Manzana 500 ml	botella		\N	\N		\N	2	2.00	A
468	Aquarius Pera 1.5 L	botella		\N	\N		\N	2	5.00	A
469	Aquarius Pera 500 ml	botella		\N	\N		\N	2	2.00	A
470	Chicha Morada Gloria 1.5 L	botella		\N	\N		\N	2	4.50	A
471	Chicha Morada Gloria 400 ml	botella		\N	\N		\N	2	1.00	A
472	Cifrut granadilla 1.5 L	botella		\N	\N		\N	2	3.00	A
473	Cifrut Granadilla 250 ml	botella		\N	\N		\N	2	0.60	A
474	Cifrut Granadilla 3 L	botella		\N	\N		\N	2	5.50	A
475	Cifrut Granadilla 500 ml	botella		\N	\N		\N	2	1.20	A
476	Cifrut Naranja 1.5 L	botella		\N	\N		\N	2	3.00	A
477	Cifrut Naranja 250 ml	botella		\N	\N		\N	2	0.60	A
478	Cifrut Naranja 3 L	botella		\N	\N		\N	2	5.50	A
479	Cifrut Naranja 500 ml	botella		\N	\N		\N	2	1.20	A
480	Cifrut Tropical 1.5 L	botella		\N	\N		\N	2	3.00	A
481	Cifrut Tropical 250 ml	botella		\N	\N		\N	2	0.60	A
482	Cifrut Tropical 3 L	botella		\N	\N		\N	2	5.50	A
483	Cifrut Tropical 500 ml	botella		\N	\N		\N	2	1.20	A
484	Coca Cola 1.5 L	botella		\N	\N		\N	2	5.00	A
485	Coca Cola 192 ml	botella		\N	\N		\N	2	0.70	A
486	Coca Cola 2.5 L	botella		\N	\N		\N	2	6.50	A
487	Coca Cola 3 L	botella		\N	\N		\N	2	8.50	A
488	Coca Cola 500 ml	botella		\N	\N		\N	2	1.80	A
489	Coca Cola Zero 500 ml	botella		\N	\N		\N	2	1.80	A
490	Concordia Fresa 1.5 L	botella		\N	\N		\N	2	3.50	A
491	Concordia Fresa 3 L	botella		\N	\N		\N	2	5.50	A
492	Concordia Fresa 500 ml	botella		\N	\N		\N	2	1.30	A
493	Concordia Naranja 1.5 L	botella		\N	\N		\N	2	3.50	A
494	Concordia Naranja 3 L	botella		\N	\N		\N	2	5.50	A
495	Concordia Naranja 500 ml	botella		\N	\N		\N	2	1.30	A
496	Concordia Piña 1.5 L	botella		\N	\N		\N	2	3.50	A
497	Concordia Piña 3 L	botella		\N	\N		\N	2	5.50	A
498	Concordia Piña 500 ml	botella		\N	\N		\N	2	1.30	A
499	Crush 1.5 L	botella		\N	\N		\N	2	4.00	A
500	Crush 3 L	botella		\N	\N		\N	2	6.00	A
501	Crush 450 ml	botella		\N	\N		\N	2	1.50	A
502	Everves 1.5 L	botella		\N	\N		\N	2	5.00	A
503	Everves 500 ml	botella		\N	\N		\N	2	2.00	A
504	Fanta 1.5 L	botella		\N	\N		\N	2	4.50	A
505	Fanta 192 ml	botella		\N	\N		\N	2	0.70	A
506	Fanta 2.25 L	botella		\N	\N		\N	2	5.50	A
507	Fanta Kola Inglesa 3 L	botella		\N	\N		\N	2	6.50	A
508	Fanta Kola Inglesa 500 ml	botella		\N	\N		\N	2	1.80	A
509	Fanta Naranja 3 L	botella		\N	\N		\N	2	6.50	A
510	Fanta Naranja 500 ml	botella		\N	\N		\N	2	1.80	A
511	Fanta Zero 500 ml	botella		\N	\N		\N	2	1.80	A
512	Frugos 1 L	caja		\N	\N		\N	2	3.20	A
513	Frugos 1.5 L	caja		\N	\N		\N	2	4.20	A
514	Frugos 235 ml	caja		\N	\N		\N	2	1.00	A
515	Frugos 296 ml	botella		\N	\N		\N	2	1.50	A
516	Frugos Kids	caja		\N	\N		\N	2	0.50	A
517	Inka Cola 1.5 L	botella		\N	\N		\N	2	5.00	A
518	Inka Cola 192 ml	botella		\N	\N		\N	2	0.70	A
519	Inka Cola 2.25 L	botella		\N	\N		\N	2	6.50	A
520	Inka Cola 3 L	botella		\N	\N		\N	2	8.50	A
521	Inka Cola 500 ml	botella		\N	\N		\N	2	1.80	A
522	Inka Cola Zero 500 ml	botella		\N	\N		\N	2	1.80	A
523	Kr Cola 500 ml	botella		\N	\N		\N	2	1.20	A
524	Kr Fresa 500 ml	botella		\N	\N		\N	2	1.20	A
525	Kr Naranja 500 ml	botella		\N	\N		\N	2	1.20	A
526	Kr Piña 500 ml	botella		\N	\N		\N	2	1.20	A
527	Oro 500 ml	botella		\N	\N		\N	2	1.00	A
528	Oro 3 L	botella		\N	\N		\N	2	5.00	A
529	Pepsi 1.5 L	botella		\N	\N		\N	2	4.50	A
530	Pepsi 3 L	botella		\N	\N		\N	2	6.50	A
531	Pepsi 500 ml	botella		\N	\N		\N	2	1.50	A
532	Powerade Lima Limón 473 ml	botella		\N	\N		\N	2	1.70	A
533	Powerade Mandarina 473 ml	botella		\N	\N		\N	2	1.70	A
534	Powerade Mora 473 ml	botella		\N	\N		\N	2	1.70	A
535	Powerade Tropical 473 ml	botella		\N	\N		\N	2	1.70	A
536	Pulp durazno 1 L	botella		\N	\N		\N	2	2.50	A
537	Pulp 1.5 L	botella		\N	\N		\N	2	3.50	A
538	Pulp 200 ml	botella		\N	\N		\N	2	1.00	A
539	Pulp Botella	botella		\N	\N		\N	2	1.50	A
540	Pulpin 145ml	caja		\N	\N		\N	2	0.50	A
541	Schweppers 1.5 L	botella		\N	\N		\N	2	5.00	A
542	Schweppers 500 ml	botella		\N	\N		\N	2	2.00	A
543	Schweppers Citrus 1.5 L	botella		\N	\N		\N	2	5.00	A
544	Schweppers Citrus 500 ml	botella		\N	\N		\N	2	2.00	A
545	Sprite 1.5 L	botella		\N	\N		\N	2	4.50	A
546	Sprite 2.25 L	botella		\N	\N		\N	2	6.50	A
547	Sprite 500 ml	botella		\N	\N		\N	2	1.80	A
548	bebida energizante maltin power 269 ml	lata		\N	\N		\N	2	1.20	A
549	bebida energizante maltin power 330 ml	botella		\N	\N		\N	2	1.20	A
550	gaseosa guaraná 355 ml	lata		\N	\N		\N	2	1.50	A
551	gaseosa guaraná 500 ml	botella		\N	\N		\N	2	1.30	A
552	gaseosa guaraná 3 L	botella		\N	\N		\N	2	5.50	A
553	gaseosa casinelli 500 ml	botella		\N	\N		\N	2	1.00	A
554	gaseosa casinelli 1.5 L	botella		\N	\N		\N	2	2.50	A
555	gaseosa casinelli 3 L	botella		\N	\N		\N	2	5.00	A
556	cereales angel chock 22 g	bolsa		\N	\N		\N	10	0.50	A
557	cereales angel mel 22 g	bolsa		\N	\N		\N	10	0.50	A
558	cereales angel zuck 22 g	bolsa		\N	\N		\N	10	0.50	A
559	cereales angel frutt 22 g	bolsa		\N	\N		\N	10	0.50	A
560	cereales angel alfa y beto 22 g	bolsa		\N	\N		\N	10	0.50	A
561	cereales angel copix 20 g	bolsa		\N	\N		\N	10	0.40	A
562	cereales angel tito almohada 28 g	bolsa		\N	\N		\N	10	0.50	A
563	cereales angel fresia almohada 18 g	bolsa		\N	\N		\N	10	0.50	A
564	cereales angel fibra con salvado 22 g	bolsa		\N	\N		\N	10	0.50	A
565	cereales hummana ojuelas azucaradas 22 g	bolsa		\N	\N		\N	10	0.50	A
566	cereales hummana bolitas de chocolate 22 g	bolsa		\N	\N		\N	10	0.50	A
567	cereales hummana ojuelas de maíz 22 g	bolsa		\N	\N		\N	10	0.50	A
568	cereales angel chock 140 g	bolsa		\N	\N		\N	10	2.20	A
569	cereales angel mel 140 g	bolsa		\N	\N		\N	10	2.20	A
570	cereales angel zuck 140 g	bolsa		\N	\N		\N	10	2.20	A
571	cereales angel frutt 140 g	bolsa		\N	\N		\N	10	2.20	A
572	cereales angel alfa y beto 140 g	bolsa		\N	\N		\N	10	2.20	A
573	cereales angel copix fresa 130 g	bolsa		\N	\N		\N	10	2.20	A
574	cereales angel tito almohada 110 g	bolsa		\N	\N		\N	10	2.50	A
575	cereales angel meli almohada 110 g	bolsa		\N	\N		\N	10	2.50	A
576	cereales angel flakes 150 g	bolsa		\N	\N		\N	10	2.20	A
577	cereales angel frutilla almohada 110 g	bolsa		\N	\N		\N	10	2.50	A
583	Café Nescafé decaf 9 g	sobre		\N	\N		\N	11	1.00	A
585	Café nescafé tradición 10 g	sobre		\N	\N		\N	11	1.10	A
587	café nescafé kirma 7 g	sobre		\N	\N		\N	11	0.70	A
582	Cebada Eco 10 g	sobre		0	\N		7613035140318	11	0.70	A
579	Chocolate Milo 18 g	sobre		0	\N		7613034883209	11	1.00	A
581	Café Kirma 17 g	sobre		0	\N		7613035268869	11	2.00	A
584	café altomayo 9 g	sobre		0	\N		7750463002754	11	1.00	A
580	Café Kirma 9 g	sobre		0	\N		7613035180987	11	1.00	A
590	cocoa winter 45 g	sobre		\N	\N		\N	11	2.00	A
591	cocoa winter 400 g	sobre		\N	\N		\N	11	5.50	A
596	chocolate milo 50 g	sobre		\N	\N		\N	11	2.80	A
597	chocolate milo 400 g	lata		\N	\N		\N	11	16.50	A
600	mermelada gloria fresa 320 g	frasco		\N	\N		\N	11	4.00	A
606	té mc collins 20 unds	caja		\N	\N		\N	12	1.20	A
611	Animalitos 150 g	bolsa		\N	\N		\N	4	1.00	A
612	Animalitos 60 g	bolsa		\N	\N		\N	4	0.50	A
613	Casino Bum Chocolate	paquete		\N	\N		\N	4	0.60	A
614	Casino Bum Menta	paquete		\N	\N		\N	4	0.60	A
615	Casino Menta	paquete		\N	\N		\N	4	0.60	A
616	Chocman 30 g	paquete		\N	\N		\N	4	0.50	A
617	Chocosoda	paquete		\N	\N		\N	4	0.60	A
618	Chocotravesura	bolsa		\N	\N		\N	4	1.00	A
619	Doña Pepa	paquete		\N	\N		\N	4	0.60	A
620	Full Cherry	bolsa		\N	\N		\N	4	0.50	A
621	Full Limon	bolsa		\N	\N		\N	4	0.50	A
622	Full Mandarina	bolsa		\N	\N		\N	4	0.50	A
623	Glacitas Chocolate	paquete		\N	\N		\N	4	0.60	A
624	Glacitas Choconieve	paquete		\N	\N		\N	4	0.60	A
625	Glacitas Fresa	paquete		\N	\N		\N	4	0.60	A
626	Lentejas 16 g	caja		\N	\N		\N	4	1.00	A
627	Margarita 180 g	paquete		\N	\N		\N	4	1.30	A
628	Margarita 55 g	paquete		\N	\N		\N	4	0.50	A
629	Mentitas	bolsa		\N	\N		\N	4	0.50	A
630	Miniglacitas Chocolate	bolsa		\N	\N		\N	4	1.00	A
631	Miniglacitas Fresa	bolsa		\N	\N		\N	4	1.00	A
632	Miniglacitas Vainilla	bolsa		\N	\N		\N	4	1.00	A
633	Nik Chocolate 24 g	paquete		\N	\N		\N	4	0.50	A
634	Nik Chocolate 77 g	paquete		\N	\N		\N	4	1.00	A
635	Nik Fresa 24 g	paquete		\N	\N		\N	4	0.50	A
636	Nik Fresa 77 g	paquete		\N	\N		\N	4	1.00	A
637	Nik Vainilla 24 g	paquete		\N	\N		\N	4	0.50	A
638	Nik Vainilla 77 g	paquete		\N	\N		\N	4	1.00	A
639	Obsesion	paquete		\N	\N		\N	4	0.70	A
640	Princesa 32 g	barra		\N	\N		\N	4	1.00	A
641	Rellenitas Chocolate 42 g	paquete		\N	\N		\N	4	0.40	A
642	Rellenitas Chocolate Fresa 42 g	paquete		\N	\N		\N	4	0.40	A
643	Rellenitas Coco 42 g	paquete		\N	\N		\N	4	0.40	A
644	Rellenitas Fresa 42 g	paquete		\N	\N		\N	4	0.40	A
645	Rellenitas Menta 42 g	paquete		\N	\N		\N	4	0.40	A
646	Soda Field 146 g	paquete		\N	\N		\N	4	2.00	A
647	Soda Field 34 g	paquete		\N	\N		\N	4	0.50	A
648	Sublime 16 g	barra		\N	\N		\N	4	0.60	A
649	Sublime 30 g	barra		\N	\N		\N	4	1.00	A
650	Sublime 66 g	barra		\N	\N		\N	4	2.00	A
651	Sublime Almendra 24 g	bolsa		\N	\N		\N	4	1.00	A
652	Sublime Almendra 55 g	barra		\N	\N		\N	4	2.00	A
653	Sublime Blanco 30 g	barra		\N	\N		\N	4	1.20	A
654	Triangulo 32 g	barra		\N	\N		\N	4	1.20	A
655	Triangulo Doble Sensación 40 g	barra		\N	\N		\N	4	2.00	A
656	Vainilla Field 146 g	paquete		\N	\N		\N	4	2.00	A
657	Vainilla Field 34 g	paquete		\N	\N		\N	4	0.50	A
658	Vizzio (más grande)	caja		\N	\N		\N	4	7.90	A
659	Vizzio 182 g	lata		\N	\N		\N	4	13.50	A
660	Vizzio 24 g	bolsa		\N	\N		\N	4	1.00	A
661	Vizzio 72 g	caja		\N	\N		\N	4	3.90	A
662	Vizzio Bombóm 96 g	caja		\N	\N		\N	4	13.50	A
663	Leche Chocolatada Chicolac 145 g	sachet		\N	\N		\N	3	0.50	A
664	Gloria Calcio 170 g	lata		\N	\N		\N	3	1.80	A
665	Gloria Chocolatada 180 ml	caja		\N	\N		\N	3	1.20	A
666	Gloria Deslactosada 170 g	lata		\N	\N		\N	3	1.80	A
667	Gloria Light 1 L	caja		\N	\N		\N	3	4.20	A
670	Leche Condensada Nestlé 397 g	lata		\N	\N		\N	3	5.20	A
674	Leche Gloria Calcio 1 L	caja		\N	\N		\N	3	4.20	A
677	Leche Gloria Deslactosada 1 L	caja		\N	\N		\N	3	4.00	A
598	cebada eco 58 g	lata		0	\N		7802950007329	11	3.00	A
602	cebada kimbo 190 g	frasco		0	\N		7750463001023	11	5.80	A
682	Leche Gloria Niños 170 g	lata		0	\N		7751271001410	3	1.80	A
672	Leche Gloria Azul 170 g	lata		0	\N		7751271021579	3	1.70	A
679	Leche Gloria Light 170 g	lata		0	\N		7751271001403	3	1.80	A
593	café kirma 50 g	sobre		0	\N		7613031592722	11	5.50	A
595	chocolate milo 200 g	sobre		0	\N		7613031592753	11	8.90	A
683	Leche Gloria Niños 400 g	lata		0	\N		7751271016766	3	3.20	A
675	Leche Gloria Calcio + Hierro 410 g	lata		0	\N		7751271003674	3	3.20	A
673	Leche Gloria Azul 410 g	lata		0	\N		7751271103213	3	3.00	A
678	Leche Gloria Deslactosada 410 g	lata		0	\N		7751271010504	3	3.20	A
605	anís mc collins 25 unds	caja		0	\N		7752285008723	12	2.00	A
603	hierba luisa Mc Collins 25 unds	caja		0	\N		7752285011303	12	2.00	A
607	hierba luisa Mc Collins 1 und	sobre		0	\N		7752285020787	12	0.10	A
608	manzanilla mc collins 1 und	sobre		0	\N		7752285012287	12	0.10	A
609	anís mc collins 1 und	sobre		0	\N		7752285009065	12	0.10	A
610	té mc collins 1 und	sobre		0	\N		7752285016308	12	0.10	A
676	Leche Gloria Chocolatada 1 L	caja		0	\N		7751271019569	3	4.20	A
671	Leche Gloria Azul 1 L	caja		0	\N		7751271019545	3	3.80	A
589	cocoa winter 23 g	sobre		0	\N		7752748004620	11	1.00	A
668	Leche Condensada Gloria 397 g	lata		0	\N		7751271019927	3	4.80	A
669	Leche Condensada Nestlé 100 g	lata		0	\N		7702024002826	3	2.00	A
686	Leche Laive Chocolatada 180 g	caja		\N	\N		\N	3	1.20	A
687	Leche Laive Deslactosada 1 L	caja		\N	\N		\N	3	4.00	A
688	Leche Laive Deslactosada 400 g	caja		\N	\N		\N	3	3.20	A
690	Leche Nan 2 Años 400 g	lata		\N	\N		\N	3	4.20	A
694	Leche Soyvida 170 g	lata		\N	\N		\N	3	0.80	A
695	Leche Soyvida 400 g	lata		\N	\N		\N	3	1.50	A
696	yogurt gloria fresa 190 g	botella		\N	\N		\N	3	1.00	A
697	yogurt gloria fresa 500 g	botella		\N	\N		\N	3	3.00	A
698	yogurt gloria durazno 190 g	botella		\N	\N		\N	3	1.00	A
699	yogurt gloria durazno 500 g	botella		\N	\N		\N	3	3.00	A
700	yogurt gloria vainilla 190 g	botella		\N	\N		\N	3	1.00	A
701	yogurt gloria vainilla 500 g	botella		\N	\N		\N	3	3.00	A
702	yogurt gloria lúcuma 190 g	botella		\N	\N		\N	3	1.00	A
703	yogurt gloria lúcuma 500 g	botella		\N	\N		\N	3	3.00	A
704	yogurt gloria guanábana 190 g	botella		\N	\N		\N	3	1.00	A
705	yogurt gloria  guanábana 500 g	botella		\N	\N		\N	3	3.00	A
706	yogurt gloria fresa 1 Kg	botella		\N	\N		\N	3	5.00	A
707	yogurt gloria durazno 1 Kg	botella		\N	\N		\N	3	5.00	A
708	yogurt gloria vainilla 1 Kg	botella		\N	\N		\N	3	5.00	A
709	yogurt gloria lúcuma 1 Kg	botella		\N	\N		\N	3	5.00	A
710	yogurt gloria guanábana 1 Kg	botella		\N	\N		\N	3	5.00	A
711	yogurt gloria acti bio granadilla con linasa 	botella		\N	\N		\N	3	3.50	A
712	yogurt gloria acti bio fresa con linasa 500 g	botella		\N	\N		\N	3	3.50	A
713	yogurt gloria acti bio frutos secos con linas	botella		\N	\N		\N	3	3.50	A
714	yogurt gloria acti bio granadilla con linasa 	botella		\N	\N		\N	3	5.50	A
715	yogurt gloria acti bio fresa con linasa 1 Kg	botella		\N	\N		\N	3	5.50	A
716	yogurt gloria acti bio frutos secos con linas	botella		\N	\N		\N	3	5.50	A
717	yogurt gloria batimix con ojuelas azucaradas 	botella		\N	\N		\N	3	3.50	A
718	yogurt gloria batimix con bolitas de chocolat	botella		\N	\N		\N	3	3.50	A
719	yogurt laive fresa 190 g	botella		\N	\N		\N	3	1.00	A
720	yogurt laive fresa 500 g	botella		\N	\N		\N	3	3.00	A
721	yogurt laive durazno 190 g	botella		\N	\N		\N	3	1.00	A
722	yogurt laive durazno 500 g	botella		\N	\N		\N	3	3.00	A
723	yogurt laive vainilla 190 g	botella		\N	\N		\N	3	1.00	A
724	yogurt laive vainilla 500 g	botella		\N	\N		\N	3	3.00	A
725	yogurt laive lúcuma 190 g	botella		\N	\N		\N	3	1.00	A
726	yogurt laive lúcuma 500 g	botella		\N	\N		\N	3	3.00	A
727	yogurt laive guanábana 190 g	botella		\N	\N		\N	3	1.00	A
728	yogurt laive  guanábana 500 g	botella		\N	\N		\N	3	3.00	A
729	yogurt laive fresa 1 Kg	botella		\N	\N		\N	3	5.00	A
730	yogurt laive durazno 1 Kg	botella		\N	\N		\N	3	5.00	A
731	yogurt laive vainilla 1 Kg	botella		\N	\N		\N	3	5.00	A
732	yogurt laive lúcuma 1 Kg	botella		\N	\N		\N	3	5.00	A
733	yogurt laive guanábana 1 Kg	botella		\N	\N		\N	3	5.00	A
734	yogurt laive laivemix con ojuelas azucaradas 	botella		\N	\N		\N	3	3.50	A
735	yogurt laive laivemix con bolitas de chocolat	botella		\N	\N		\N	3	3.50	A
736	yogurt laive yopimix con ojuelas azucaradas 1	botella		\N	\N		\N	3	1.50	A
737	yogurt laive yopimix con bolitas de chocolate	botella		\N	\N		\N	3	1.50	A
738	Leche Chocolatada Chicolac 180 ml	caja		\N	\N		\N	3	1.20	A
739	Leche Chocolatada UHT Laive 180 ml	caja		\N	\N		\N	3	1.20	A
740	Leche Gloria UHT Entera 1 L	caja		\N	\N		\N	3	4.00	A
743	Leche Gloria UHT Niños 1 L	caja		\N	\N		\N	3	4.20	A
745	Leche Gloria UHT Chocolatada 1 L	caja		\N	\N		\N	3	4.20	A
752	Coctel Bercheva Leche 750 ml	botella		\N	\N		\N	5	16.00	A
753	Coctel Piconi Cacao 750 ml	botella		\N	\N		\N	5	21.60	A
754	Coctel Piconi Café 750 ml	botella		\N	\N		\N	5	21.60	A
755	Coctel Thiago Café 750 ml	botella		\N	\N		\N	5	18.90	A
756	Crema de Whisky Baileys 750 ml	botella		\N	\N		\N	5	56.50	A
758	Espumante Tabernero Especial 750 ml	botella		\N	\N		\N	5	15.50	A
762	Pisco Del Fino Quebranta 750 ml	botella		\N	\N		\N	5	20.90	A
763	Pisco Rotondo Acholado	botella		\N	\N		\N	5	43.00	A
764	Pisco Rotondo Italia	botella		\N	\N		\N	5	43.00	A
765	Pisco Rotondo Quebranta	botella		\N	\N		\N	5	43.00	A
693	Leche Pura Vida 400 g	lata		0	\N		7751271018814	3	2.30	A
685	Leche Ideal Cremosita 400 g	lata		0	\N		7613032920524	3	3.00	A
684	Leche Ideal Amanecer 400 g	lata		0	\N		7613033464874	3	2.30	A
744	Leche Gloria UHT Calcio + Hierro 1 L	caja		0	\N		7751271019590	3	4.20	A
741	Leche Gloria UHT Deslactosada 1 L	caja		0	\N		7751271019606	3	4.20	A
742	Leche Gloria UHT Light 1 L	caja		0	\N		7751271019552	3	4.20	A
689	Leche Laive Familia 500 g	caja		0	\N		7750151002301	3	2.60	A
747	Anís Najar Seco Especial 750 ml	botella		0	\N		7759291179119	5	31.00	A
748	Anís Najar Semi Dulce Especial 750 ml	botella		0	\N		7759291179126	5	31.00	A
750	Anisado Torre Blanca 750 ml	botella		0	\N		7750066553783	5	12.00	A
749	Anisado Torre Blanca 250 ml	botella		0	\N		7750066553783	5	5.50	A
760	Licor de Menta Tres Plumas 750 ml	botella		0	\N		7790260013157	5	25.00	A
759	Licor de Menta Le Mans 750 ml	botella		0	\N		7752486000243	5	16.00	A
766	Pisco Santiago Queirolo Acholado	bolsa		0	\N		7758218000024	5	31.00	A
761	Licor de Ron Cartavio 700 ml	botella		0	\N		7751738123358	5	22.00	A
769	Pisco Tabernero Acholado 750 ml	botella		\N	\N		\N	5	31.50	A
770	Pisco Tabernero Italia 750 ml	botella		\N	\N		\N	5	31.50	A
771	Pisco Tabernero Quebranta 750 ml	botella		\N	\N		\N	5	31.50	A
773	Pisco Vargas 750 ml	botella		\N	\N		\N	5	24.00	A
774	Pisco Viña La Esperanza Quebranta 750 ml	botella		\N	\N		\N	5	20.50	A
778	Ron Barceló Añejo	caja		\N	\N		\N	5	36.00	A
779	Ron Cabo Blanco Black 375 ml	botella		\N	\N		\N	5	7.50	A
783	Ron Cabo Blanco Silver 750 ml	botella		\N	\N		\N	5	12.50	A
785	Ron Cartavio 5 Años	botella		\N	\N		\N	5	25.00	A
789	Ron Cartavio Black 750 ml	botella		\N	\N		\N	5	18.50	A
790	Ron Cartavio Blanco 125 ml	botella		\N	\N		\N	5	5.00	A
793	Ron Cartavio Selecto 5 años 750 ml	botella		\N	\N		\N	5	23.00	A
794	Ron Cartavio Solera 12 Años 750 ml	botella		\N	\N		\N	5	72.00	A
801	Ron Flor De Caña 7 Años 750 ml	botella		\N	\N		\N	5	67.50	A
810	Tequila José Cuervo Black 750 ml	botella		\N	\N		\N	5	64.50	A
814	Vino Ocucaje El Abuelo	botella		\N	\N		\N	5	23.50	A
815	Vino Oporto Tres Piernas	botella		\N	\N		\N	5	17.50	A
816	Vino Santiago Queirolo Blanco 750 ml	botella		\N	\N		\N	5	17.00	A
818	Vino Santiago Queirolo Magdalena 750 ml	botella		\N	\N		\N	5	17.00	A
820	Vino Señorío de Najar Borgoña 750 ml	botella		\N	\N		\N	5	19.50	A
822	Vino Tabernero Borgoña 187.5 ml	botella		\N	\N		\N	5	4.50	A
824	Vino Tabernero Rose Semi Seco 750 ml	botella		\N	\N		\N	5	17.50	A
825	Vino Tacama Blanco 750 ml	botella		\N	\N		\N	5	21.00	A
831	Vino Viña Los Reyes Borgoña 187.5 ml	botella		\N	\N		\N	5	4.50	A
834	Vodka Russkaya Clásico	botella		\N	\N		\N	5	18.50	A
836	Whisky Ballantines 750 ml	botella		\N	\N		\N	5	52.00	A
837	Whisky Chivas 12 Años	botella		\N	\N		\N	5	99.99	A
838	Whisky Jhonnie Walker Black Label 12 Años 750	botella		\N	\N		\N	5	99.99	A
839	Whisky Jhonnie Walker Double Black	botella		\N	\N		\N	5	99.99	A
840	Whisky Jhonnie Walker Red Label	botella		\N	\N		\N	5	58.00	A
841	Whisky Old Times Red 750 ml	botella		\N	\N		\N	5	30.00	A
842	Whisky Something Special 750 ml	botella		\N	\N		\N	5	55.00	A
843	Whisky Vat 69 750 ml	botella		\N	\N		\N	5	30.00	A
844	Whisky White Horse 750 ml	botella		\N	\N		\N	5	64.00	A
847	Vino Astica Trapiche 750 ml	botella		\N	\N		\N	5	20.00	A
850	Crema de Ron Cartavio 750 ml	botella		\N	\N		\N	5	23.50	A
851	Cerveza cristal 650 ml	botella		\N	\N		\N	5	4.20	A
768	Pisco Santiago Queirolo Quebranta	botella		0	\N		7758218181112	5	31.00	A
835	Vodka Sky	botella		0	\N		721059007504	5	42.00	A
832	Vodka Premium Stolichnaya 750 ml	botella		0	\N		4750021000157	5	35.00	A
812	Vino Blanco Gato Negro	caja		0	\N		7804300004019	5	11.00	A
829	Vino Tinto Gato Negro	caja		0	\N		7804300004002	5	11.00	A
828	Vino Tinto Clos	caja		0	\N		7804320569307	5	11.00	A
830	Vino Tinto Zumuva	caja		0	\N		7790314004346	5	11.00	A
813	Vino Blanco Zumuva	caja		0	\N		7790314004353	5	11.00	A
809	Sangría Tabernero	caja		0	\N		7750533010801	5	11.50	A
772	Pisco Vargas 125 ml	botella		0	\N		7758645000376	5	5.50	A
787	Ron Cartavio Black 125 ml	botella		0	\N		7751738053969	5	5.00	A
788	Ron Cartavio Black 250 ml	botella		0	\N		7751738000062	5	7.50	A
796	Ron Cartavio Superior 125 ml	botella		0	\N		7751738053976	5	5.00	A
791	Ron Cartavio Blanco 250 ml	botella		0	\N		7751738001960	5	7.00	A
797	Ron Cartavio Superior 250 ml	botella		0	\N		7751738001977	5	7.00	A
776	Ron Apleton Special 200 ml	botella		0	\N		636191007506	5	10.00	A
823	Vino Tabernero Borgoña 750 ml	botella		0	\N		7750533000147	5	17.50	A
821	Vino Tabernero Blanco 750 ml	botella		0	\N		7750533017770	5	18.50	A
819	Vino Santiago Queirolo Rose 750 ml	botella		0	\N		7758218195959	5	17.00	A
817	Vino Santiago Queirolo Borgoña 750 ml	botella		0	\N		7758218195973	5	17.00	A
849	Vino Casillero Del Diablo Cabernet Sauvignon 	botella		0	\N		7804320303178	5	28.00	A
775	Ron Apleton Special 1 L	botella		0	\N		636191007209	5	29.50	A
845	Vino De La Mancha Borgoña 750 ml	botella		0	\N		7750388224378	5	17.50	A
846	Vino De La Mancha Rose 750 ml	botella		0	\N		7750388532763	5	17.50	A
800	Ron Flor De Caña 5 Años 750 ml	botella		0	\N		026964334227	5	42.50	A
781	Ron Cabo Blanco Durazno 750 ml	botella		0	\N		7751738002301	5	12.50	A
780	Ron Cabo Blanco Black 750 ml	botella		0	\N		7751738002042	5	12.50	A
807	Ron Pomalca Oro Añejo 750 ml	botella		0	\N		7753194000433	5	15.50	A
786	Ron Cartavio Black 1 L	botella		0	\N		7751738445672	5	22.00	A
795	Ron Cartavio Superior 1 L	botella		0	\N		7751738001595	5	19.50	A
784	Ron Cartavio 3 Años 750 ml	botella		0	\N		7751738001601	5	25.00	A
792	Ron Cartavio Blanco 750 ml	botella		0	\N		7751738120883	5	16.00	A
798	Ron Cartavio Superior 750 ml	botella		0	\N		7751738001588	5	15.50	A
803	Ron Kankun Limón	botella		0	\N		7753194000259	5	14.50	A
804	Ron Kankun Oro	botella		0	\N		7753194588856	5	14.50	A
802	Ron Kankun Durazno	botella		0	\N			5	14.50	A
848	Vino Frontera Chile Cabernet Sauvignon 750 ml	botella		0	\N		7804320559001	5	16.00	A
826	Vino Tacama Rose Semi Seco 750 ml	botella		0	\N		7750359000369	5	21.00	A
852	cerveza cusqueña malta 620 ml	botella		\N	\N		\N	5	5.00	A
853	cerveza cusqueña dorada 620 ml	botella		\N	\N		\N	5	4.50	A
854	cerveza pilsen callao 630 ml	botella		\N	\N		\N	5	4.20	A
855	cerveza cusqueña trigo 620 ml	botella		\N	\N		\N	5	5.00	A
856	cerveza cusqueña red lager 620 ml	botella		\N	\N		\N	5	5.00	A
857	cerveza cristal 473 ml	lata		\N	\N		\N	5	3.50	A
858	cerveza pilsen callao 473 ml	lata		\N	\N		\N	5	3.50	A
859	cerveza cusqueña dorada 455 ml	lata		\N	\N		\N	5	3.50	A
860	cerveza backus ice 355 ml	lata		\N	\N		\N	5	3.00	A
861	cerveza malta 330 ml no retornable	botella		\N	\N		\N	5	3.80	A
862	cerveza cristal 330 ml	lata		\N	\N		\N	5	3.00	A
863	cerveza pilsen callao 330 ml	lata		\N	\N		\N	5	3.00	A
864	cerveza miller 355 ml no retornable	botella		\N	\N		\N	5	4.20	A
865	Detergente Ace Aroma Zen 2.3 Kg	bolsa		\N	\N		\N	1	28.50	A
866	Detergente Marsella Max 2.6 Kg	bolsa		\N	\N		\N	1	18.60	A
867	Detergente Opal Ultra 2.6 Kg	bolsa		\N	\N		\N	1	21.50	A
868	Detergente Magia Blanca 3 en 1 2.6 Kg	bolsa		\N	\N		\N	1	18.60	A
869	Detergente Marsella Max 850 g	bolsa		\N	\N		\N	1	5.70	A
874	Enjuague Downy 450 ml	botella		\N	\N		\N	1	4.00	A
877	Detergente Ariel 520 g	bolsa		\N	\N		\N	1	5.00	A
888	Detergente Magia Blanca 160 g	bolsa		\N	\N		\N	1	1.10	A
893	Detergente Marsella Max 160 g	bolsa		\N	\N		\N	1	1.30	A
896	Detergente Bolívar Blancos Perfectos 360 g	bolsa		\N	\N		\N	1	3.50	A
897	Detergente Bolívar Color 360 g	bolsa		\N	\N		\N	1	3.50	A
899	Detergente Magia Blanca Bebé 3 en 1 360 g	bolsa		\N	\N		\N	1	2.70	A
902	Lavavajillas Ayudín 180 g	pote		\N	\N		\N	1	1.50	A
904	Lavavajillas Ayudín 600 g	pote		\N	\N		\N	1	4.60	A
908	Sacagrasa Mr Músculo Advance 500 ml	sachet		\N	\N		\N	1	4.90	A
909	Lejía Clorox Total 345 g	botella		\N	\N		\N	1	1.00	A
915	Lejía Clorox Total 680 g	botella		\N	\N		\N	1	1.50	A
916	Lejía Clorox Ropa Poder Dual Colores 480 ml	botella		\N	\N		\N	1	3.00	A
918	Desinfectante Sapolio Limpia Todo Floral 250 	botella		\N	\N		\N	1	1.00	A
921	Desinfectante Sapolio Limpia Todo Manzana 250	botella		\N	\N		\N	1	1.00	A
923	Desinfectante Sapolio Limpia Todo Limón 900 m	botella		\N	\N		\N	1	2.80	A
925	Desinfectante Sapolio Limpia Todo Manzana 900	botella		\N	\N		\N	1	2.80	A
927	Desinfectante Poett Limpia Todo Bebe 295 ml	botella		\N	\N		\N	1	1.20	A
928	Desinfectante Poett Limpia Todo Lavanda 295 m	botella		\N	\N		\N	1	1.20	A
929	Desinfectante Poett Limpia Todo Primavera 295	botella		\N	\N		\N	1	1.20	A
930	Desinfectante Poett Limpia Todo solo para ti 	botella		\N	\N		\N	1	1.20	A
931	Desinfectante Poett Limpia Todo Bebe 648 ml	botella		\N	\N		\N	1	2.20	A
932	Desinfectante Poett Limpia Todo Lavanda 648 m	botella		\N	\N		\N	1	2.20	A
871	Detergente Ariel Líquido Nueva Fórmula 1 L	botella		0	\N		7501065906922	1	16.00	A
873	Detergente Ariel Líquido 450 ml	sachet		0	\N		7506195130544	1	6.50	A
882	Detergente Ariel 360 g	bolsa		0	\N		7501006729795	1	4.20	A
892	Detergente Ariel 90 g	bolsa		0	\N		7506309879321	1	1.00	A
891	Detergente Ace Limón 160 g	bolsa		0	\N		7506339394818	1	1.50	A
872	Detergente Ace Blancos Diamante 1 L	botella		0	\N		7501006736052	1	15.00	A
878	Detergente Ace 520 g	bolsa		0	\N		7506195184035	1	4.00	A
879	Detergente Bolívar 520 g	bolsa		0	\N		7750243045131	1	4.00	A
884	Detergente Bolívar Matic 360 g	bolsa		0	\N		7750243037006	1	3.50	A
885	Detergente Bolívar 360 g	bolsa		0	\N		7750243045124	1	3.00	A
895	Detergente Bolívar Negros Intensos 360 g	bolsa		0	\N		7750243035040	1	3.50	A
887	Detergente Opal Ultra 360 g	bolsa		0	\N		7750243040525	1	2.70	A
926	Desinfectante Sapolio Limpia Todo Bebe 900 ml	botella		0	\N		7751851000819	1	2.80	A
922	Desinfectante Sapolio Limpia Todo Floral 900 	botella		0	\N		7751851000758	1	2.80	A
924	Desinfectante Sapolio Limpia Todo Lavanda 900	botella		0	\N		7751851000741	1	2.80	A
876	Detergente Caricia 100 g	bolsa		0	\N		7501007493459	1	2.00	A
890	Detergente Magia Blanca Bebé 3 en 1 150 g	bolsa		0	\N		7506339322286	1	1.10	A
900	Detergente Magia Blanca 520 g	bolsa		0	\N		7506195186169	1	3.50	A
894	Detergente Opal Ultra 160 g	bolsa		0	\N		7750243035897	1	1.30	A
919	Desinfectante Sapolio Limpia Todo Limón 250 m	botella		0	\N		7751851005630	1	1.00	A
910	Lejía Clorox Ropa Blancos Intensos 264 ml	botella		0	\N		7756641002912	1	1.20	A
911	Lejía Clorox Ropa Poder Dual Colores 264 ml	botella		0	\N		7756641002738	1	1.90	A
913	Lejía Clorox Power Gel 292 ml	botella		0	\N		7756641003216	1	1.50	A
914	Lejía Clorox Power Gel Magia Floral 292 ml	botella		0	\N		7756641003209	1	1.50	A
912	Lejía Patito 308 g	botella		0	\N		7751851021432	1	0.60	A
875	Enjuague Amor 180 ml	botella		0	\N		7751851022668	1	2.00	A
907	Lavavajillas Ayudín Líquido 300 ml	botella		0	\N		7506195195444	1	5.50	A
905	Lavavajillas Ayudín 900 g	pote		0	\N		7506195101735	1	7.00	A
906	Lavavajillas Lava 1 Kg	pote		0	\N		7861036712076	1	7.20	A
903	Lavavajillas Ayudín 330 g	pote		0	\N		7506195145456	1	2.50	A
889	Detergente Patito 150 g	bolsa		0	\N		7751851023351	1	1.00	A
933	Desinfectante Poett Limpia Todo Primavera 648	botella		\N	\N		\N	1	2.20	A
934	Desinfectante Poett Limpia Todo solo para ti 	botella		\N	\N		\N	1	2.20	A
940	Jabon de ropa marsella bebe 240 gr	barra		\N	\N		\N	1	1.60	A
941	Jabon de ropa marsella bebe 240 gr	barra		\N	\N		\N	1	1.60	A
943	Jabon de ropa trome –gr	barra		\N	\N		\N	1	1.00	A
946	ceras sapolio pasta 300ml negra	sachet		\N	\N		\N	1	3.00	A
949	ceras sapolio pasta 300ml verde	sachet		\N	\N		\N	1	3.00	A
954	ceras sapolio liquida 300ml verde	sachet		\N	\N		\N	1	2.80	A
956	ceras emperatriz pasta 300ml negra	sachet		\N	\N		\N	1	3.50	A
958	ceras emperatriz pasta 300ml neutro	sachet		\N	\N		\N	1	3.50	A
959	ceras emperatriz pasta 300ml verde	sachet		\N	\N		\N	1	3.50	A
961	ceras emperatriz liquida 300ml negra	sachet		\N	\N		\N	1	3.20	A
964	ceras emperatriz liquida 300ml verde	sachet		\N	\N		\N	1	3.20	A
971	aromatizadores poett 360 ml bosque de bambú	spray		\N	\N		\N	1	6.50	A
972	aromatizadores glade 360 ml campos de lavanda	spray		\N	\N		\N	1	6.50	A
974	aromatizadores glade 360 ml caricias de bebe	spray		\N	\N		\N	1	6.50	A
976	insecticidas sapolio mata pulgas y garrapatas	spray		\N	\N		\N	1	7.00	A
980	insecticidas sapolio mata acaros 360 ml	spray		\N	\N		\N	1	11.50	A
981	insecticidas sapolio mata todo 360 ml	spray		\N	\N		\N	1	7.00	A
988	Paño absorbente virutex	unidad		\N	\N		\N	1	1.50	A
989	guantes virutex talla S	par		\N	\N		\N	1	4.50	A
990	guantes virutex talla L	par		\N	\N		\N	1	4.50	A
991	guantes virutex talla M	par		\N	\N		\N	1	4.50	A
992	paño multiusos scoth-brite	unidad		\N	\N		\N	1	1.20	A
994	esponja verde scoth-brite	unidad		\N	\N		\N	1	1.50	A
996	Piqueo Snack 24 g	bolsa		\N	\N		\N	13	0.60	A
997	Piqueo Snack 42 g	bolsa		\N	\N		\N	13	1.20	A
998	chizitos chipy 20 g	bolsa		\N	\N		\N	13	0.50	A
999	doritos chipy 24 g	bolsa		\N	\N		\N	13	0.60	A
1000	cheetos chipy 16 g	bolsa		\N	\N		\N	13	0.50	A
1001	cheese tris chipy 14 g	bolsa		\N	\N		\N	13	0.60	A
1002	tor-tees chipy 30 g	bolsa		\N	\N		\N	13	0.60	A
1003	papas lays chipy 38 g	bolsa		\N	\N		\N	13	1.20	A
1004	habas karinto 18 g	bolsa		\N	\N		\N	13	0.50	A
1005	mani clásico karinto 20 g	bolsa		\N	\N		\N	13	0.50	A
1006	mani salado karinto 20 g	bolsa		\N	\N		\N	13	0.50	A
1007	mani picante karinto 20 g	bolsa		\N	\N		\N	13	0.50	A
1008	mani confitado karinto 20 g	bolsa		\N	\N		\N	13	0.50	A
1009	mani clásico karinto 45 g	bolsa		\N	\N		\N	13	1.00	A
1010	mani salado karinto 45 g	bolsa		\N	\N		\N	13	1.00	A
1011	mani picante karinto 45 g	bolsa		\N	\N		\N	13	1.00	A
1012	mani confitado karinto 45 g	bolsa		\N	\N		\N	13	1.00	A
1013	los cuates karinto 40 g	bolsa		\N	\N		\N	13	0.50	A
993	esponjas scoth-brite- la maquina limpieza pro	unidad		0	\N		7750373104593	1	1.60	A
944	Limpia vidrios cif 450ml	sachet		0	\N		7791290007505	1	4.00	A
995	acida harpic-power ultra 500 ml	pomo		0	\N		7702626203799	1	11.00	A
985	cif crema limon (oferta + paño) 500 ml	Pomo		0	\N		7752285025294	1	15.50	A
945	Limpia vidrios sapolio 650 ml	spray		0	\N		7751851559300	1	5.80	A
987	Removedor de manchas vanish 100 ml color	sachet		0	\N		7702626206653	1	1.20	A
967	aromatizadores sapolio 360 ml arullos de bebe	spray		0	\N		7751851006088	1	6.00	A
968	aromatizadores sapolio 360 ml jardin de rosas	spray		0	\N		7751851019187	1	6.00	A
966	aromatizadores sapolio 360 ml lavanda silvest	spray		0	\N		7751851005982	1	6.00	A
970	aromatizadores poett 360 ml espiritu joven	spray		0	\N		7793253039264	1	6.50	A
969	aromatizadores poett 360 ml bebe	spray		0	\N		7793253011048	1	6.50	A
978	insecticidas sapolio mata moscas y zancudos 3	spray		0	\N		7751851006620	1	7.00	A
977	insecticidas sapolio mata cucarachas y hormig	spray		0	\N		7751851006613	1	7.00	A
979	insecticidas sapolio mata polillas 360 ml	spray		0	\N		7751851006651	1	7.00	A
984	insecticidas baygon 360 ml	spray		0	\N		7591005003446	1	10.50	A
983	insecticidas raid max mata cucarachas y araña	spray		0	\N		7790520982100	1	10.50	A
982	insecticidas raid mata moscas mosquitos y zan	spray		0	\N		7591005005556	1	10.50	A
957	ceras emperatriz pasta 300ml amarilla	sachet		0	\N		7756641002622	1	3.50	A
960	ceras emperatriz pasta 300ml roja	sachet		0	\N		7756641002639	1	3.50	A
962	ceras emperatriz liquida 300ml amarilla	sachet		0	\N		7756641002608	1	3.20	A
963	ceras emperatriz liquida 300ml neutro	sachet		0	\N		7756641002653	1	3.20	A
948	ceras sapolio pasta 300ml neutro	sachet		0	\N		7751851012751	1	3.00	A
950	ceras sapolio pasta 300ml roja	sachet		0	\N		7751851013529	1	3.00	A
955	ceras sapolio liquida 300ml roja	sachet		0	\N		7751851013888	1	2.80	A
953	ceras sapolio liquida 300ml neutro	sachet		0	\N		7751851012959	1	2.80	A
952	ceras sapolio liquida 300ml amarilla	sachet		0	\N		7751851012942	1	2.80	A
951	ceras sapolio liquida 300ml negra	sachet		0	\N		7751851013895	1	2.80	A
938	Jabon de ropa bolivar floral 240 gr	barra		0	\N		7750243038911	1	1.90	A
937	Jabon de ropa bolivar limon 240 gr	barra		0	\N		7750243038928	1	1.90	A
936	Jabon de ropa bolivar bebe 240 gr	barra		0	\N		7750243038935	1	1.90	A
1014	los cuates picante 40 g	bolsa		\N	\N		\N	13	0.50	A
1015	free papas karinto 20 g	bolsa		\N	\N		\N	13	0.50	A
1016	dulci ricas ricas 28 g	bolsa		\N	\N		\N	13	0.50	A
1017	papas lays 16 g	bolsa		\N	\N		\N	13	0.60	A
1018	nachos picantes ricas 40 g	bolsa		\N	\N		\N	13	0.50	A
1019	mini kraps 80 g	bolsa		\N	\N		\N	13	1.00	A
1020	sublime power en cubos 51 g	bolsa		\N	\N		\N	13	2.50	A
1021	olé olé x 8 40 g	bolsa		\N	\N		\N	13	1.00	A
1022	mini ritz 50 g	bolsa		\N	\N		\N	13	1.00	A
1023	vela llanas	unidad		\N	\N		\N	14	0.30	A
1024	vela misionera 10 h	unidad		\N	\N		\N	14	0.50	A
1025	vela misionera 12 h	unidad		\N	\N		\N	14	0.70	A
1026	vela misionera 15 h	unidad		\N	\N		\N	14	0.90	A
1027	vela misionera 24 h	unidad		\N	\N		\N	14	1.60	A
1028	espiral tokay x5	caja		\N	\N		\N	14	2.50	A
1	Gaseosa Coca Cola 1.5 L	botella	Bebida gasificada saborizada	5	1.5	Litro	123456789	2	5.20	A
883	Detergente Ace 360 g	bolsa		0	\N		7506339394825	1	3.00	A
870	Detergente Ariel Líquido Con Downy 1 L	botella		0	\N		7501065906939	1	16.00	A
898	Detergente Ariel 160 g	bolsa		0	\N		7506195145647	1	1.90	A
942	Jabon de ropa ace 240 gr	barra		0	\N		7506195123843	1	1.60	A
880	Detergente Bolívar Matic 520 g	bolsa		0	\N		7750243037013	1	5.00	A
886	Detergente Opal Ultra 2 en 1 360 g	bolsa		0	\N		7750243040525	1	3.00	A
881	Detergente Marsella Aromaterapia 520 g	bolsa		0	\N		7750243044509	1	3.70	A
920	Desinfectante Sapolio Limpia Todo Lavanda 250	botella		0	\N		7751851005586	1	1.00	A
935	Desinfectante Pinesol Limpia Todo 267 ml	botella		0	\N		7756641002943	1	1.70	A
917	Lejía Patito 615 g	botella		0	\N		7751851021449	1	1.10	A
901	Lavavajillas Ayudín Repuesto 150 g	pote		0	\N		7506309838991	1	1.00	A
986	Removedor de manchas vanish 100 ml blanco	sachet		0	\N		7702626210582	1	1.30	A
973	aromatizadores glade 360 ml espiritu potpourr	spray		0	\N		7790520007308	1	6.50	A
975	insecticidas sapolio mata arañas 360 ml	spray		0	\N		7751851010283	1	7.20	A
965	ceras emperatriz liquida 300ml roja	sachet		0	\N		7756641002615	1	3.20	A
947	ceras sapolio pasta 300ml amarilla	sachet		0	\N		7751851013512	1	3.00	A
939	jabon de ropa bolivar antibacterial 240 gr	barra		0	\N		7750243038980	1	1.90	A
124	sopa instantánea ajinomen gallina picante 86 	bolsa		0	\N		7754487000376	7	1.00	A
599	cebada eco 195 g	lata		0	\N		7802950002584	11	6.90	A
592	café kirma 190 g	lata		0	\N		7613035041493	11	22.00	A
692	Leche Pura Vida 170 g	lata		0	\N		7751271018821	3	1.20	A
594	café nescafé tradición 50 g	frasco		0	\N		7613031592814	11	7.20	A
680	Leche Gloria Light 410 g	lata		0	\N		7751271103237	3	3.20	A
691	Leche Nan 3 Años 410 g	lata		0	\N		7613034146236	3	3.80	A
604	manzanilla mc collins 25 unds	caja		0	\N		7752285012775	12	2.00	A
681	Leche Gloria Niños 1 L	caja		0	\N		7751271019613	3	4.20	A
231	Pañales Pampers tripack xxg	paquete		0	\N		7501006737011	6	3.30	A
228	Pañales Pampers tripack m	paquete		0	\N		7501006736984	6	2.40	A
42	Sao 500 ml	botella		0	\N		7773101000714	7	3.30	A
169	duraznos en mitades monteverde 820 g	lata		0	\N		7752335000035	7	8.00	A
588	Cocoa winter 11 g	sobre		0	\N		7752748004484	11	0.50	A
131	harina espiga de oro 1 Kg	bolsa		0	\N		7752476101271	7	4.80	A
586	Café nescafé tradición 17 g	sobre		0	\N		7613035205277	11	2.00	A
151	flan royal chocolate 80 g	bolsa		0	\N		7622300719609	7	2.20	A
578	Chocolate Nesquik 15 g	sobre		0	\N		7613034873033	11	0.70	A
601	mermelada gloria fresa 1 Kg	frasco		0	\N		7751271071277	11	8.00	A
104	filete de atún fanny 170 g	lata		0	\N		7750885006293	7	5.50	A
56	mayonesa alacena 500 ml	sachet		0	\N		7750243514521	7	7.80	A
54	salsa molitalia pomarola 160 g	sachet		0	\N		7750885003469	7	1.80	A
4	Don Victorio Canuto Rayado 250 g	bolsa		0	\N		7750243037556	7	1.20	A
746	Anís Najar Crema Especial 750 ml	botella		0	\N		7759291179133	5	35.00	A
751	Coctel Bercheva Café 750 ml	botella		0	\N		7750084000078	5	16.00	A
767	Pisco Santiago Queirolo Italia	bolsa		0	\N		7758218000192	5	29.50	A
757	Dry Gin Paramonga Gin Seco 750 ml	botella		0	\N		7751738149068	5	18.50	A
833	Vodka Russkaya Citrus 750 ml	botella		0	\N		7751738002295	5	19.50	A
811	Vino Blanco Clos	caja		0	\N		7804320568300	5	11.00	A
799	Ron Flor De Caña 5 Años 200 ml	botella		0	\N		026964929270	5	11.50	A
806	Ron Pomalca Oro Añejo 250 ml	botella		0	\N		7753194000549	5	6.50	A
827	Vino Tacama Tinto 750 ml	botella		0	\N		7750359000215	5	21.00	A
777	Ron Appleton Special 750 ml	botella		0	\N		636191007407	5	27.50	A
782	Ron Cabo Blanco Limón 750 ml	botella		0	\N		7751738001373	5	12.50	A
808	Ron Pomalca Special Black 3 Años 1 L	botella		0	\N		7753194000112	5	20.00	A
805	Ron Medellin Añejo 3 Años 750 ml	botella		0	\N		7702049100576	5	23.00	A
148	Gelatina royal piña 160 g	bolsa		0	\N		7622300794910	7	3.20	A
368	Desodorante Rexona Women Nutritive 50 g	barra		0	\N			6	9.50	A
\.


--
-- Name: articulo_idarticulo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('articulo_idarticulo_seq', 1029, true);


--
-- Data for Name: caja; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY caja (idcaja, nombre, monto) FROM stdin;
1	Principal	24.50
\.


--
-- Name: caja_idcaja_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('caja_idcaja_seq', 1, false);


--
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY categoria (idcategoria, nombre, imagen) FROM stdin;
7	Abarrotes	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000b5d49444154789ced9d6b6c5be51dc61fdbc78e8fefc749ecd8b4499d344d4349180997b5e352064d270445144de2b27d9bd8f665d3a4f19d4f036948684cd33434b49bd46963432adaa6ad62a5acd0d2b5346b0385429c5b133bb1133bf6f1e51cfbd8de07c789d33ac939f67b2e4efbfb92443ee77fdef8f1ffbd3cefc5bac7da5e2de1169a41af76016eb11e4aed0254e09c1970ce74cdd76c111728cea87089d441754152de04c69e3e0bce99d9f4ba8eb12ef4ff6d58a152a987ea5596183100607e601a930f7ca64089d445554116f784448951617e605ac6d26803550561bd0949d773ce0c4a94eab5acaca82a089db048be4770ba5034d33294461ba82a886bba5dd2f5ad53bb000005ab1582c38992de2043a9d4455541cc090b769cdf2dea5a2a6742e7c76bbdac92d108c1b5fdb245f50ab9f7dd413cb3770ea6fbbedcf09acb97fb30ffd72330b3f6f52fe8742858ad289a4ca0d824506a7ed341754100a02f6fc073b75fdbf0f50933f0e29f0d486df07ac968449e71c39062a1cfe5e429a442a83e0ee9ee0b61ffc14f37bfa6fb1a7efaea2bb0d936e922eb7428d81d28d81d804e47b894caa1aa20dd7d21bcfcc62f11d813dafa5a31a200289a4cc8336e144d2652c55414d504a98861b571e2ef5911654b2ad962b3375db6a822483d62acdedb7d0d3f7ef1d7a2ae2db6b440703128199bc798545c9046c4a870e8d087a24529e9f5101c4e14acd6a6c81645052121460529a20040d14c97b345e3d68b62823cfac405bc7eec352262543874e8437ceffbc7445f5fd2eb21385d2858a45b364aa1d8c725f4d4397ce3fd7dc4e352b6a8e47b8ab40525a30986740a3a41205ea646504c108a33e274d4413ceeb08e423de64989a220385dd067333064c44f01c88d62559667c1254b5c67c8d7d0fd45daa229a352b946fd62b72c610bff6d7c5a574b46a562829c3db50f8ffb634463fa4d0514163c6482ad18956a678b6282a4591afb40761cd0936f211a0fa8ca961633f1d86250741cc2ccb4118d679fdd4134de2a3a1d0a369b2a46a5a28224cef7c0612c108b97bef01562b16aa18651a9a8201f9dba030fb64b5bd8b01144db8fcd50d8d657dccbeae5c9187d5d592b913862299a4c8a18958a0b629ff01289434f7711892305258c4ac505593ed74ba41d499e1b22509afa58352a65c816c50519fbb80707988d66c7c5d1612cdeb8e04161d6650b41d499a04a37367e0870da716b8b661a82d345ccd6574510eb7863fe931aedc766548c4a12b6be2ab335b1b37d481f7badeefb0f8fbc40b034e42061ebab92211357fd888419351e2d3b258a82e070d66d54aab6ea2412aa5f90c13b3f2758121968c0a854a70db16771c7f044ddf7df39a8714156a8c7d657459081e16043f76b3e43aa59c916b1d68b2a82ec7f78f3a5a35b31387875cb158c5a43ac51d994190200834d526dad438451a9ca42398f2fde709ca6aab6ae63b36c515c1012d901007736b12000d6b2e53aa3527141f61ffc84489ceeee6b4dd78ed4e27aa35251411aedee5ecf81031789c552936aa3525141485557159ab91da945d14c2beb6589eaee2600242b3f4bd07100aa578b56ed7c1bc1071819f940f4f317b2ad58e0ca3b7f53790b826c270020c876222d587029b657742cb95054907519c2038800b856822e82b200d297e94ac24b2fc14b2fadfe7dc073639597166804d92e04d94e04939de59f2bc22981628274f785e011e2d09d2c953fe532bff9f562a5b218643ec720b3561da6051a9762fd381319c2a5f85e2c64c92e67aa4627f701665e6b0c4ff69dc683ddff036364e57c94624cb03bf1f6f4619c08dd4f3cb66c8278ad313c3f70028f042ec8115e132c645bf187e053448591459047bbcfe385a1e3b01ac96dced13297e37bf1d2e80f90121a9f3124deeded6642f8d17d7fba69c4008041e673bc74d7eb4462111764ff0e3223f166a3ba13d008c405619e9e241df2a682b820176ff381d3f84e573978af2740240e7141388ac22ff6df8b29469e2d6c5ae4bd9e004e121244968ff2326dc69bf70ce1ae50185f0f4ec295dd9e0dfca8df87933d012cd3e436f7c85ab78cfa7d18f5fb1088c57157681efd9128cc1adb862c9579bb0da37e1fae78da890a514191ca7ed2cd60d2cd00e84720164720be8c402c8e5df165251edf10cbb419930c8349b70b930c238b08d528defaae8ab352e7fad8143a58164c96432016875910d0c136b618bb1e388ac2bcdd86384daf88e042d86157bc83a27a77286cb7216cb795ffa86a18cd82005fb2ec7df9d8d46a55e7ca7260b2d9ba9e55ced2b5e7721405ce685c7bbe06505d908de0286af50d9c743330f14504c6b328cdf11092ebdba1d08e164ceeb660a9bd798e61da08cd0a52c13fcb63e0228b5dc18db3c237cb63f8a3245807858fbfeac0d57dca6e772389660531f1457cedd432f65ca9fd8d09b5b027051c3c1143df9534fe79a40db916d58f94948c264bec9fe5f1fc9b61496254e39be571e4ad084c7c51d27ded331cda67d41d33692e43fa3e4de3e089b52338a88edb60397c1485f939643f7c17c55452549cd6681e47de8ae02fdfead8f4baf6190e9d632cba3e59ebd99d7ed68768a73a2739684a905a6278de7807530b51b85c2eb43ef64d447ff8ace878add13c062eb2181bba713f62d7580a5d632cdaae69cb45d04c95654f0a38f0fefa81a2e5f0519c19bd84977ff20adc6e375aee18867177bfa4b803a3ebc734ed331c1ef9cd1c86ff11dd500cb5b203d09020779f4dd6acf353a914c6c7c7f1c4e347ea8a6b4f0af0cff270467278f058180ffc310c6764e3d3afc3bdea6e28d54c9555ab5b9bf9d7db18f9d571f87ef61abc5e2ff84be7901f97fe2d3bc327633086c5554dd14e75cfccd28c20b510e6e710f9ee93088c1c85303f8ba533ffae2b8e2b9283d8fe5ae856869409ed68a99925c2fc1c92bfff7943b1f5228fc148784cc838d57d4b34d386c839ba2e8afc1a8b658ffae7c56b4690a91e1aa11de44f88038082c8854e09af3ccf97826604018033071959ec8e7c49dc883d712b43d6b3d46ec49987c8cec5f3c592e80cd102c405091fef6de8feabfbac3835e226541ae08bddea0df2ea81b820cb171b3fa08c9428a746dc38f3a407d303f24f402583640e6633f45a465e2212690521d182dc120d7bff12f446696e6b354b1e13c23bcdf0cff26891e8da0265312a3db7706f7963e5564e6e788f156cabf449ae64d08b2f7efb304a42e3e7fd12170400b2330ec4cff961b00ab0748a73676bc13a285cdd674581d2a32d9a87414463c03a289c38d286a9ddeb47dc8b9d66649c46b4cf701bc661dd462c4a18a9f3712b66deb917d3c7ef212206a0c0fe10535b169e9149b88616606aad6f2e1c284f58ed0a661118cfa2359a87bd6a1a97755008ed6cc1540f8da99ecddfd08aa765ac9175e3773b70f991d62dcb920931983f7d3ba2e77ba4ff235b20bb20d5d8f72ee1feef9c85eeb60ce639f5161618f92286ff1e85ffcbf5dbaa17779af19fe76e3c5cadc099900c7a11ffa413c9a0177c4cbeb22b2a4805af3f8ec7bf7d1a1d8361704c01e1ac0dcb39b3e222edbe9044ff07f1d56c59dc69c67b47bb909e639009b99109b9577f570a5504d90c535b16a6761e46bf0093878781cec1527588bfa367a1eed87cdcbafae92e702664e6dc70e432189a98442f378d092e80df259f69f87f6804cd988b15728b34728b34f059f974b682c52afbe1c557b05fd6f852d0d448fd7a7482002a9900954c4057acbf0bdd4c685a900aba7c1e543c06432ab52dbe8078333457656d869ee7a0cff128d074f9d8bc26f85e42a9344586aca35482219381311e839ee7d52e0d719a4f900aa5120c2916543c0e5d3eaf766988d1bc82aca02b16d61afe6d204cd30b524197cf834a266048a59aba47d6548dba18f43c073dcfa160b13465c3bf6d32e47a561b7eae7e43530db6ad2000ca0d7f3a0d2a1e6f9a1ed9f61664055db150ee913541c37f53085261b5e167939a6df86f2a412ae873b9b215934e6bce8ad976bd2c29e8b96cb947a6212be6ff71420ec96c2b672f0000000049454e44ae426082
8	Aguas	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a0000060349444154789ced9d6f4c1b651cc7bf77d7026da10c1849b110196822d498f94216b3b8f086cdb8f86ae1c58c2f66b277982cf18df18d8ef81203f385d198eddd4c541c6ed16413b26cd90c364e13d76d3a6120cd8a43b12bb0d28eb6f79c2ff09863b4bbb6f7dcfd389ecfcbeb73bffb95cf3dffee29cf49af6cff4083800cb2dd09081e4608218610420c2184184208318410620821c41042882184104308218610420c97dd09bcf7cd8b053fef7ff5078b32a181a821c41042882184104308218610420c218418e485686076a76029b6cd43baf6b563ffe19de8dc192c58eedd2f1b7079780a17877fb328337b91acfe91436ba8117d833d68eddc5ed47933bffe838fde1ac3cc8d794e99d1c0d226abbbb70303e70e162d03005a3bb763e0dc4174f77670c88c0e9609e9eeed40df604fd971fa067b1c2dc512215dfbda4d91a1d337d883ae7deda6c5a30477213e7f25fa86cc93a1d337d4039fbfd2f4b876c35dc8a1a37be0ada9303daeb7a602878eee313daedd7015e2f357726defbb7b3b1c574bb8ce433a7737e1c68f7f142ca38141d5b225c597a1a0737713ae9c9d29e97c8a701512eca8043c8b05cb48652611784606ce961180185c9bac405b35cff0abd768f722c392dcaf63155c6b48a0cdc7333c0020d8568f7bea1c6aa516b8a4d2fa937ccbc8762c1f937fb868040d0c4bea9f8e7810e9082100c0b42c1673b74d95628760c70801809cb6827bea9c69f1ee66a72def9f1c250400322c699a14bd29b4b239749c100058614b48b38469f1322c89bbd9695363e6c3f61fcaf162599d87040555b23f6f99eede0eec3ffc7cdee580e3e79fc0579f5ec0b92fc2d0c0b0acce23c392a8560250243797bc1d59437492ea1c72daca23c75b438d18f8eeb5c72e94b5878278fbc3d771fcfc3b78ead9660040564b632117454a8d73c9d9d14200603177fb2129ada146f40f1f286a91ac3d14c4b191236b523430a4581c0bb9e886c2cbc1f14234b0b5e17063b31ffdc3074a7afaecf37b706ce408022d0d6bc772da0a1672512cabf3a675fa8e17023c90f246ff4b652d05f8fc1ebcf9fe81478ea75902895c14592d554e9a00b6881060b5a97a616ff9ab8cbb5f7e6eade9fa3fab13d318eea97365d516c78eb2d613dad582d1af2f4396142852fe5aa269ecb1fd4268570b6e5d8f6df8d90a5b42862551a304502117ff7075cb08e9ec6a42b0adc660e9c7346b7213ce9cc8ffb13ea1acd0aae1931b8b1a226f1921c1b67acb6365581259968257698047ae3374ce96e943ec429f50ae1f7ee74308b108a3134a21c462f40965be21b2106203396d058bb9d886134a21c446369a500a2136a34f28f535972d33eca54e862571974d8b1a42090d4c08a18610420c2184184208318410620821c41042882184104308218610420c2184184208318410620821c41042882184104308218610420c2184184208318410620821c41042882184104308218610420c218418420831b80ab9bf9ce3191e00904e66b85fc34ab80a99b95678475233988cdce17e0d2be12a247c7a96677800c08591eb86cac5a6ccdbbdc7cc58ebe15b43220bf865ec2fc3e56353f1a2be6c78740293578dd5908b23370cc7b532d67ab877eaa7877e372ce5d267b3183b69ec4d3ae1d1099c1cb864388ff0e804c2a31305cb44c6a3888c47cb8e530ecad3debd47b945ff8f9be1f86a7fa201db025570553cb80fe6a693b8f55302a7872610bdb684c5590dd7c7670177061a34f8ebbd6b65d3c90c7ebe3885531f874bba4b23e351c4a6e3f0d779d11078b0cd466c2a8eefbfbd893327ae20321e452ea3c2535df1d0b527afdec1a94fc218fb3c52e25fc11896bff2a818eeb3252cab7f3b623f5ea390fea7cf2ad98f4ab91a693581344b6c0931e4278612647895066c733d89ca021b5a3a05f2427414c98d1a25805a5733dc92c7ee74b841bac9da08b7e445adcb8bfb6c092916072bf1dd2354d9744274aa643faa643f526adc51fdcba669b2f2e1551a50ef6e734cffb2e98500ab1d7f8d12409d6bc7a6ef5f1c21444791dca875b5a0d6d50c99d356e0bc7194101db7e445bd6b077c4a23a44df615376da76e048f5c872ab91669358114e3f784d64c36d7ed5302fac4b2ceb5a3a47d74ade65f6dd1fe74a48651200000000049454e44ae426082
6	Aseo Personal	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000b6849444154789ced9d5b4c1cd719c7ffcc2e7b3397f5b2e0858dc1601c2e75b510373889719afadaba12add5c6b6f2e0c496d28726aa2a5589fad6c73ef4a595928754a9133fd432a9e414dca46e0d562db01d233b06713158162120ee0b0bbbb097d961b60f9b59ef2cb3f7736666b17f4fccedec61fffb7ddf39e73b734e5ee0f29e109ea11a18a52bf00c31cf045119cf045119cf045119cf0451195aa52b400a765b3578c6807cff2c34c115a5ab93315b4610b7ed27e00c360080d63f876d4b77605c79a070add2674bb82c765b75440c00e00c36acda4fc2b9fbd708469dcf05b684203e73b3e479ce60c3f2aef3f01736c85ca3ccc97941788d013e7353dceb218d016b810df03c2f63ad3227e76388b7e4e5e4f74cde021b6261b194c0a037c850abccc9790b89e7ae04f885af81a0171cc76161611eae9565555b4b4e0be233376323df9cf01e7efe6bd1b1c7e3c1dcfc2cfc013fcdaa654cce0b920c7ee1fea6736ab6969c156423df0c76dbae84f708ee2a1e6ab4969c1564adec50d27b62dd95146ab3969c15c4b4741b4573ff46be7f2eee3d52ee2a1e6ab1969c68f6cecdd830365c1739aeda3d015bf93c4cfe3b302dddc18a9f81d776184cd93e20df0420b9bb9242b096c2c242141799c130f2ff5ef37221857bf1a3b3f876bc6ad3f9bac631ec6978086bf9bfc227f24dd0541d83a6ea38b8a18fc1cfa76e21b168b55a45fa2daa17646ec686bffee5ed84f7e8743e34365d47a3a30b3abd376c25695a473ce4b616d50bd2f1591b06ee3b52ba57a7f3a1697f271a1dd789d6414e6b5175505f71995316030058d688be9ed3b8f6f97b58735b89d523dd96d88acb8cf68ba7d07ef154da9fa56a41eef6eecfe8b9b9e93a745efe0326c793771cd3219596d88acb8c8ffefc2b8c8dd4616ca40e1312b12f11aa15c4ef33a0ff5eead6110bcb1a71e3cb7730375d97fce63448662ded174f21e0d7675cbe6a0519b8efc8ea1f1328285c22509bcd08d6e2f33d693cfce7ea71cccfee881cdb2ae6b0abe6dbb4ca556d3fe4ab0cdd553416eb140a8a9c046a230dc77158742ec26834c1397700777b5b22d70c463f4e9dfd0778831521468fbca0074cd09db44c550a3270cf81555771d6e5d436dc26509be42cce9bd0d9de263ad7f67a274cbbabb05e7630724ee71a807ebe276159aa7459fd69b4ac1251592dcf24871b5fbe039635468e5f3d7c13cfef5b42204a0c0060b73be02f3f92b02cd50932315e25d92b4f97caea7eaaee4aa0afe70c969d3b23c736fb18f638ae60bde284e4fdc1e27a048be3e7f85527c8dd9eec63070054d6d0b78ec9f1668c0c3cf9c5ebf45eb41efe04c6c6d7015d51dce7f8fcc2b8d75425c88acb8cb11132cd54da82acb9ade8ed3a2f3ad77af81394ec6d407ee9f7123eabf54ec7bda62a416e5e7f95483995d5fde1312d8ac4c68d4647176af6b961aa6b4bf014a05fe881261704f1fb0c181dae2752166deb888d1b16eb14f6ffa803054d6f257c2eb8388c8de93b09ef514db3f76eef7e221d4180ae205271e3d0890f51d0f416f2b4f1071f799f0beb43ed58e37c301a4d28b194488e20abc64248740401a0b6fe363577152f6e9436bf00edf69ac4cf0e7c8a10e70300f87c5ecccc4e8b7af902aa1064e01e99611280ae75f4769f938c1b869aa3099ff38e7562c333233ac7f33c169d8b58742e8ac6c45421c8ffba7e48a41c9dde4b4d90febe36d140a5c53a85e603ff4d1e37168610988cdf3b8fb516c563c8c4781591611220dcbaa2c1dc741dfafb9eb49e747a2f5a8f5c80a5e574f2b831fc59d2f2056b311a4dca5bc8cdeb64ac03a0e7ae6287f05b5adb61dfdf9056dc48059fcfabac202b2e3391611280aebb8a16a4b6e1166a1b6e65143752415141487504017aee0a80281ddcf4e255006177148f647123118a09926ebe3c198d4d64273608acb9ad58f39400000a8a9c91014befa34ec9fb373c3329c58d782826c84016e9d9580a8a9cb058a78895178d6824b7e251e4efe0c210d687db453122c4f9369d4b17455a597e9f81584710a0ebaea2274ad8ec63a26beccc3d041786a1abf801f2b40670aef18ce246348a0832365c47ac230884032d2da205916a3484385fc6f1420a455c16a98e2040d75d4d8e37477ae6728c20030a08422a5f2e505b4f2f6f1e6d1d34ad301ad90521952f17a025081b30e1f1e82b00c2562847061290591052f972019ad37ca287d8695a612cb20a42b2a90bd09be6c3064c18e90ff7c4757a2f1a1d5d543e470ad90421dd1104e84df31919381209e634f32b52c82608c96112809ebb8ab60e00b25a0720932024f3e502b4dc55ac75c831b72b1a590421992f17a8ad27df0c5d735b23790f9dde8b968397897f46326411847453975627adafe74ce4efc8eb7132435d10d21d41804e226a72bc1993df8457152a2872a2a9457a349736d40521398828405a10366012cd2669696d275a7e3a50156462bc4af4020b0968b8abdeaef34fc6ac6a1ec8d62b9782aa2024f3e502a4bfac9181a31157a5d37b15b50e80a22024f3e502a4f3e6cbce9de8eb391d396e6ab92a7b3337166a8290ee080264dd151b30e1da95f723c736fb18f1f7db33818a207e9f81f8300940d65d5dfbfcbd48dc10deeb50035404c9f4fdf244907457bd5de745b9f296d676c55d9500714148e7cb0548e5cd7bbbce47f21cc09379566a81b820a4f3e50224ac6364e0a8480c8b754af156552cc40521992f172091b17bfcf080a84525cccf55627824114405191dae233e4c0264efae1e3f3c80deee73a2732dadedd426476403514168047320bb090693e3cd9bc4687474a92a6e44434c90b9191bf18e2090dd349f65e7ce4d6f3c55d63c5064583d55880912bdce0749327557cbce9db876e57dd11b4f16eb946afa1bf12022088d7cb94026ae454a0c9dde8b1f9ffc93ea82782c4404a1153b327157b92c064040906c171a4b44a3a33badfbd9800937be7857520c35b6a8a4c85a10520b8d4991ce341f36600aafb5f8ddbb1c02874e7c983362000404a1314c02a437cd4710237a7c0a08bf431efb0a81dac94a101af9728154a7f9241243ad7d8d4464fc7e08af3160c79bbfc0b13a1e431d3c6606c92eff9b8abb92124358ea22d72c4320634184bd3b76bdc460d74b0c66874218fc278f89afb2df61201577154f8c5c0ae052642c48ecde4fe57bf350be5783b50506f72ef178d495b930c9a66f2e3b77e2c617ef8a02f8561003c82286142cdc00b3be79dda782b23cbcf65b0ddeb8a0c5f77fc640b72dfdb2138dec0afd8c68312cd629fcf2ecef735e0c80c0daefebfae7e0b1be06989f97bcceae03831d3c063b36c0ae272fafb2ba1f877efa81e4b5f010fa19513fa3b6e1165a5adb73a2d3970ac416e35fe50bb15ef232988ad6b8f73ceae671ff120fcf7cfc8f8cd73aeaeb39b3699daaa6964ed967a7d386e8ee081cc7c1e567107ceeb8687395581e75f318ece0b134bef9a3df78fb37a25ffb9adb8adeee739b56e2693d72614bb8a858a86c57e1f379b1e4f621aff2083455c7e30a333b14c2fdbf6f449accb1eeaabfaf0d23fd4745b3431a1d5d8acdbb95036afb87f03c8f55f70a3cfe0d30652f40537b127946e92d2496be0961b0834759e86f2277f5e9071f47feae6db885a617959fc8461bea1bbaf8037e2c2f2f81e33830f65630f683602cd22fef707d7f04bf3c1a397e3cfa0ad88049b64591d5806c3becacaeae60d5bd0a00602cf5606a4f6e122678f37708f99e8e2f3e1eb26e79c40659b85c2e04bedb1025cf6885a6f6e760ece135d2d96b6fca5515d5a2c81e541e8f1babeed5c8e28f79462b98aa63d818bd2477555487629b82711c07d78a4b72a9d4a719c5d6cbd26ab528b596a2d45aaac8068e6a45f16fc26834a1a2dc8ec2c2f83b063c4d282e0800300c83ed660bcaca7640ab557ce55a455185200206bd0115e5761417d1c942e602aa1244a0b8d88c8a723bf432ef43ab06542908100efa3bca7660bb79fb5315f455ff9f161616a1a2dc0ea3517a8072aba17a418070d0179ac85b3de8e7d47f67349aa0d71bc2a3c81e8fd2d5a1c2ff01ca87fc2d2289427c0000000049454e44ae426082
9	Baterias	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a0000053349444154789ced9dcd6f1b4518877ffb6537b11b5bc1b86e5a0248a1295205116ab87204016aff0024ee70e5d0432f5cf9a80407da5b90684f884b4145ad84c2b1880a241a0ea50a1052cb35969b26a9e3afb5677b70bcd9759c8fb57767df64df47b2e41d7b6726fbecbc33b3bb192b179fffc602430635ec0a306e580831580831580831580831580831580831580831580831580831580831580831580831580831580831580831580831580831580831749985bd7ee13872b3094ffbfc7363157f7e5df6bd2ea359036f7cfe1c8c516fe7e44f1ffe876ac9f4bd3e5da4b610af320020f5623c809a00a3cfea9e6574f70b12a92de4305069afa1d4fcdfde6e88aafdde50e2c8c626a12a839fe7525b885915328bdb464354ed57a5bd36501ed5f6ba2b1f27a6d540a9b90c610dfe772a7e3d9775eebb293fb2f14cfdeeeff8e1631d75c7c179e7b35731fed24828f5f9eadd1b43b594031fb29aa28152f3a12b6d9833d40f4cab81878dbf918d4dc250bdf5813cec0d080181527319a66878da8f8504c820527c0b59c9f67dbfb2f2c4e3cafab634b3d6463c1ecc70d92b5d2969238b8496daf3fbbe093955bbe457569eb8b7380d60da95b6f6a086ccdbd950ead30f018115b308007b4ae190259115b3888d3d86db2c44322b66116bad9d2f05b19010586f3db243582f077e1e42817551c3cdda026e5517706c9ffb7443d7b89173a5b39001714ab8555bb0d3dfc7e97de7d19592d6b3f6ac9e8578a42be166ed2e9e88fad0f96db4d7d01475fb520b0bd9077e4be8a57b51321b9b64213b11b4845eba52588803d9127a31ad060b095b422f9114424d8293c808a12cc1c9a11672bbbe886f377e252fc18934214a7a16d6ea1d2965fd76ec012e167e44beb522a53c3f91762d4bcd9d87929e9552d6f2d8e303290390284494e7a19df9124a72f74b0bea0b1f481347116942acf23c00403bf305a01fed5f99dcf94d216765558b1c522fbf5be579e0c804b499b96d5294e469a85317ecf75145aa10b1d94a94e434b4cd830f00d08f6eb69c64e7730e5972b0caf340bd00005072e7ec16a1cdcc014726b6bea827dddb1142fa1d4351fe79abf093ef419b9983929cdef6bda8862de942ace275d7f64e1d7854c3967c21957b76d8da8d7ead260a84f29083336ced445487bea108e90d5b00805605ad5fde025a153b298afd483842fa842d91bf0ad40b104b97edb428b692d09ecb12f96b5b1bad8abd2df2d76055fe02c02d442add49220088c54f80d613c7f6a79d3711ecd8c37b72b15ee8b4847a01a2a74fb156efc02a7e1fc99156a88f925ac5eb104b57fa7ed65eba0cb42a919b8f847ac7d0d58ff4522f40e4af42499f9576638b02a41fb6164b57007d2cec6a4885b41000ae617014202fc439fa8a02f485440c16420c16420c16420c16420c16420c16420c16420c16420c16420c16420c16420cdfee87dc1ff9c8afac3c9198fa1740cb95f64a6a026faa2f87529fdbf863a8fd7d1352d14ef9959527b4e42300ee7fce491923784d39194a7d8615c2218b182c84182c8418be2da40c74d6cb2d3597615ade964665b6f0b585a88a8a4cec04546e7803e3fb91d315a3b3f6134b1988408e9aa1c691899d0822eb434f60a7715c1dddb69e20b33781c6958496c298fe4c90451c3a020ff4293db3af25b6990e527ade712387113529a3a8038fb4a1d0b8711c864263817ccac85b0d48513bab6e2a86ac220f2452270baaa22263f0c47137a41f19438df3c4711742392a861a47daa0f3fb1e9408ed344d68299e38f621d4b891d0523c47e921f4403e6ee4588a83d085001d293c47e940420880ce8f30b2143a42ba13c7a80f879f02abcdb7d643777a410000000049454e44ae426082
2	Bebidas	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a000008f249444154789ced9d4f6c1c571dc7bf336f76f69f77371bff8dff5574718bea402908caa14442549168f853212a71a9e0c28122a1b6aa10420a07a40a0448850b48dc10528544c52dc0a1429436b83d2416b1c9216ed2d8d978b35e3bf6ae7777766667e671b026dd38bb3bffdecc9b71fd39d9c99bdf3cef77def7fde6fd5be199b15f531c131944de1538e67e8e058918c782448c634122c6b12011e3589088712c48c43816246248bc2bd0cb53b3cbf8d2f475476597aaf37873fdf301d7287c22d542168a1b8ecbcee53603ac093f22d542fe56c9e3647603633250d3740040cb3c18d959cc267153d1902122261209acdc2da06dec2043467956993942d4c6b2aa89256c49ef0e2d93531f455efd040080083272640aa210a967cb3391b22c0030a1da96a142f7decf06d550d7cbd0cc5690d50a8d4809b22d5dc6b6b46c5bae29df4053feb0f3a730d134aa681b3b41562f14222548d15844ca1cb72d97300ac874e71ff8f78e59475d2fc3a47a10d50b8548094268125da1615bce10db1069a2ffffc5dcc22225080018827d1f62f6f421fdb02c4c317659552b3422254847ac392edb2575db328ab98b86be090ad34fb542255282680eeccac210da8ecae9b483bdee06ba54f15aad508994208ab8e5b8ace6a085585098d8d72bb1b0b04809d212cb8ecb6a92fb14370e16165b4154b2ede91e51b7b0c808e2460c0b55f2264a942d2c328234c8fbaeafe948155ff78ca285454690a6971642fc0f9558166650cd772c164442104d68b87a07b1e8923a0cd159fa3b0c0a1375bd8c8ee93c730b8a4808e2c5ae2c149fb6d54bdbd841d3a872b5b04808e2a543b7f0dab10f42335ba8eb656e16c65d104350d120cee6d1fbd191eed88e6db9c5a43a370be32e4843f46e57167eb3ad41f0b030ee82389990b2a32ddf625093fe846d615c05f19a5d1d4625db4cb2ad418469615c05f1935d1d8665b63588302c8cab20bbe42ab358ed4470b6d58b66b6d0d03703b3306e821882cac4ae2cbaa4ce3cdb1a84413534f44da8e63ef3d8dc0461915d1d26a86cab1f14265a460d2da3c6d4c2f809e2e3dd63104a223c412c54739fa9857113c4cfdbf9203406838d5e606961dcd65fbe3853c293a36c3df83fdb32aa1f7f1c2ffde25b989a3be929c6f2c535bcf6e337b0b6e2ee81b12c4ca71d64c828048fcf3ab7b5bd3ff8f45f715266e7bdd987179039fb4d94bef6acef585d55c79f7fff4f681d1dd756caf8f785ffbaba9e083246c8048820bbbe37971692225d666248d9113c72fe97c87ff20926f100209194f0fc8b67effdbeb67a1b2f9c7b0dcdbab3695fcbc23264144931e7eade5cfa90c9ec1eb358a597cf3315a31f0ba767f0bb0b2fb9baa6370b7303174166f23799c4494e9e42f10b6798c4b263e1f40cce9c7bdcf575aab9ef6abd3117cba2307cc798facef7917bec53d01af78f2f754d01ba19cc73f6bd9f7c055fffee9378e5b93fb8bace5a6f9c25e390c5ecd0b25c04f958feaeef18a9874a901f2a3db89b84002209264f999c3f81c9f9139eaeb5d61ba76861e8ae2f2e8298d47f0b0100511421080293584ea094c234fd25231db38e2e5506eefa8aed3eb09b3f7b05a597cf63fccbcf8476cfe58b6b78e1dc6f7cc71966615c3a75016c9e6ab51aee50496583dd48c0a05d5f5c0411e1fe85a91f8d2b9799c4714a65dd7fdf7798c3bbbeb808a21884491c752bdc1672cde5708a537a777d711164bd31c1244ed896d5ac07374d6c591817416e37e698c56aacf85f24e194cbefac057e0f2e59d63575077facec2143fc3f0fdf5edfc0e7021e3a01d876e8c3e02288226ee1c2369ba1f76fa81f6077f50d26b186a13b1c58f40b1741d2269b3e0400dacd3d74ebc1bf1c124689881d5cfa104293cc626d6cba5bdbbba976704369e186e26e1f7bbdc92655b7834b0bc99ab3cc62fdfdad4b78f6cc826db94db583bf6c95b16d18908904839a80a1e3b989192c66f3b6d79737c2b12c6e73ea32b5ff109c50a9d96fa5564c037fbab38ea64870229d43464e2397cc229d1ac1eb5b6547ada55a0d6789113741128c04d9acd92fefbcb4bf878e409092eeb74a4924c8ca19bc53b7cfa02ebdcb6e0dd930b809c2d2b6b6f6870fb75f6d359094faf7014949c6d5967d2b6b36c35901cf4d10969956b93a7c4ab8282560d2fe1f28a51469d13e835a5d09ee2dbd176e82383986c9297699d6c3e92c946eff436d946e078f65872f446877fa9f3c1404b1efd481834c6b189fcd15f1482a8d7db5054a3fb437a5db816c1af8ead8a9a1d7375be17d4c5c57bfb3ea479c645acf4fcde3a95c012da581dd761db5e65d3c2a27f1c3b992ad657d70c3fec82856709d3164f582e824d30280a78b1378bae8beefaa6d85770c07d71692a2ecfa915a23b8676be9edf0d60cc7764efd30afffa385577ffb23e671d756ca585df939f3b883e0da429c1c09eb94dbb76e338bd54be516fb69db617015c4cbf9268378fbbd3799c5ea65ed4a30d3b683e02608ab1db8165d52476583eda90e4078135316dc04d995fec73c66b9ccdeb63e128218828a1dc27e2efce27bff621e33a8952683e02248437cdfd1f9bc6eb9555e671ed3e99e1056f01124800d9f00502eb3ddabbe7c31f8552687e1b3508e6167decbd2d212d378fb21b70e8093204ece77f70acb561276ca0b44e03420d6b0ccb4c2eed0015e9b3ecd71a6ef20bdbcfad35f612ef14526b1d6563f22828c98b3810972fd4a0ddb9df03b635670b1ac51fd3381c51ed14a81c50e032e82c8348f59edac7d419714952740cc0cf3b861c26df8bd682c021a5049bce5fb2551a409143aa7fb7e0d52dce03a1f523416513416d120d7d112cbf7beaec2ee609aa43e060048980524f531a4f4a9c0eb1a169198a0ca1b25e48d07bdffe0d0fc3bd0698743adf810e9f7100122f2d23452628177554223d2825864c82846c8a4e7238fe2446cfe4259cc222f4d7b3af2284ec44610e0e01caabc34edfac8a338112b4180837e254bc68fdcb7445bc44e108b9458404e3a75e4fa9558ff3509218d1389f923d5afc45a10e0c0c20ad2ec91e957622f8845968c234bc6636f61f1aefd2192620e7969baef395471e14809021ca4c60569d6f628bda872e404010efa95113289b458e45d15d71c49412cd2a418bb2197f8d4d423b29845419a8d4d6a7ce405010051906233e4f27fae0784819b42c4340000000049454e44ae426082
10	Cereales	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000b4949444154789ced9d5b5054e71dc07fcbb2ee2d2c97851585959b22e26a698bad69b424253113dbc63ab14d9387dc1e1c7d483b493b6dd387a4693ad34c6792ced469ccf4a5c6e9a8996823e90c691d9386b438b6600b06454091155158586497eccd65973e6c7615d985bd9efd48cfef09cee53bffd91fdff7ff7fdf397b50fcf81fcc22230c39d90e40662eb210c1908508862c4430642182210b110c598860c8420443162218b210c1908508862c4430642182210b110c598860c8420443162218b210c1904c8856a9a3465f8b56a9636b71331643c39c6d8d8577d358787764bf56a9c3626898b7ad465f8bc5d000c0d6e2660a971959a931d3587837000faff85ed46d2b3566566acc6c2d6e0660dbf26fb15263a67099911a7d2dc119a93e89855148754f7d6ff5f354eb6ba5b85452f48ef5f36afbeb98d664378edcec5e5e1c8241981e8760004cab21274b9f8c9c43eec03509d7ce91b5214cbabf03771f041d925d2e61dca3911f7dae9014d36a58a697360cb987c4c0e782911eb8e992f6bab2900508064252a66dd25d5316b208c100d82e4a2745161227b68b307139f3d791852480e33ad806327b0d5948824c8fc3b59ecc95c5b29024f03833375791852489cf0557bbd35f16cb4252c0ef4bff5c6549af650d5d323274a968ceb6ca9a492a6bec92c5109eab1457419e29f5f6969c90a14b46ba3acae8ee2c8fbabf0d506b66d8fcf5cb6cde3a8446ebcf784ce1b90aa42e65c908999ad4f2d7967aface2d5ff4589f3797b6136be8ea28e7fb4f9fa174a553820843523c0e525ac25f1239a4aba39c375fdf1a978cdb71dcd072e08dcd8c5e336428b2f94c8fa73657115ec8f1231b69797b233e6f729dd9e7cde5f8918d698e6a61a6c74315583265b1d0428e1fd918335724c2d835031f9d90f6566078093f5129c20a49978c30a73fae4a5b5bf1e27381f54c6265b19042ba3aca9392b1e7fe67b198dba2eef37973b9d093580e4a07e1b2385e29c209095753c960d04ed0bcfe20ab8ce7a3ee9732b9df4e3000c3ddf12de10b27e4f8db5f483881ab556ed6947602d0debf8bed5fdc8f5ae59e77dcd025635a624c9678eeab0825a4aba31ceb1d33ef7878ec6bbf64e7a6d770b84be81c7c88d959056b4a3b321061ead82e2e5c160b35318c5509a9556e342a170e7749e477009f5f87c5dc86c96005c0e6ac00a0bdff111aabdfa767b8694e3ba323d20f5901a51e7bd10eecc61d0472f428832e6a079e810117c555f31f3712a687747594e3b8a18dbaafb1ba95ed0dfb81908c3dcdcfb2a5f62800f9baf1c87103a38d000cdbeb23926e47abcbfc32caedb8f41bb858bd0f5bc9e30472428fafe4045c2803aed07d952865b1304216aa8034b9eec807bca7f959d42a37a6fc218048afb1392b223dc2e12ec1e12e9997dcf30b3d19887c3e01a59eeba5bbb95cf16bfcaa5b8b5bcaa08b8ae15f457e0fcf55fcde5be70a3164793daa0597454cf94391614aad72d3dab5870d9f95b7cb0d566cce0a0e9f7a71ce394e4f71e6025e00977e0357573e374784ca6fa3d8de4281e324cac0dcfa377c5fa5cc127a064c0821f1cc0f1cee121aabdf07c0e92ec16cece5a9a69f61325839d0f62a3ebf6ed13632b92c1f50eab9be7c375305cd916d7a770f055327299c3ab9e0b9e1b94a699d2042e2991f2814b37c63fd4100aed8ebf1f975980c565abbf64492f9ed988dbd78fd731f3bacac994c4fc077e0ccdbcc48d973913c61983e8d71b205bdeb93b8db080642c39770424c062bbe195d243784f1faf518b413913c111ea2a2c95853da89cfaf9bb34fa3f5a7bd87dc542d67a4ec395c3a0b0005531f601a3fc432ff58d26d0a21646a32545d59cc6d34af3f486bd75e2ce6367a869b70b84b2265efb07d1dad5d7b80e822c23456b7462aae306bd727ff2145c356f23876e30e004ce387304eb6cccb0fc9208410c70d2d6a959be6f50779b7e3475cb1d7b3bd613fc313a1251493c1ca6ffe72987bd61e5db4aded0dfb311b7bf9a0e7c939db1b368da425d670d20e2af518ed2d691311460821001bcc6d383c255cb1d793af1b8f5455e125919f7cfb31dafb1f8979bec960a5d9f21666632f9d830fcde9419535f645872b57508d336f335e4d3510aa8cb4de4134de4100bc9a6aae97eec6aba9ca888830c20851ab5c9161c6fcd9fcc1626e8be403b5ca1de931b1e81fdd446bd7de79f9a769dbc598e7f47af2d937ba8e938e15609ebf5fefee2190a3c7aba9c2347e8855c3af644444182184547c56fd3456bd8fcd51c996b54769ef7f847b6a8fe17097f0e1b927d8deb09f316765cc366cce8aa879a5ce3216b377ec1b5dc7bed1ba056373e92c28832e560ffe20d25b328910420a0add0c5faee79eda63ecdcf41a366705ed7dbb18b8be098727f4d77ef8d48b71cd356e47a3f5f3e0c3d197e27f7ae5cbbc3bb92abef8a63e9044060822a4b26692eece8db4f7edc25c7c9e0f7b9e00e65652367fecaa2a163b1e3d4b41d1fce592938e1571cb50065d28039f267ced641142489d658cbfbde70f25ed05127722346d1ba0ce12bdd4fddde8ba84da324eb6a423a4b810627151a3f5f3d5ad43696baf61d355eedd16fba6c3054f7edc6d996c87329ac4ef440821009bb70e451d5e12e5de6d03ec78f46c1a220ae50e297b07082444a3f5f3e85367927ef4337c7ed3023d23ccfdf9d7173dc638f91ee5d77e9b542ca9208c1080d2954e9edcfbaf84a5346cbaca0f7ffe51cc9c7127afae3ac357ee9a88ba4fefeea1cafa022b46ff90500ce942ba576b947e936a4d695cc77a3d2ada4eaca6abb31caf4715f5188dd64f9d658ca60706921eea7a3df9f47af219b9a9c7e4efe3d02969f3453484a8b2ee44a3f5f3e08e5e9ab65dc43a5834ef5e78658d3d32994c85755a07ebb4a197199cb38d665d06082864d6e78edc685603b5d5766aaba31c97e6bbb1ba192feb0da9bfb6c1351360c8ed5dfcc0180825243036c8ec74f4b13dd3988197eaa2984f8277466cbc3392dc72bf50493d5b32d2cd77cb92ffd68e504264041392535496ed10d2c21b8357933e57a81c9253548622af18667c916db30a0dfea2dd90a3ce6264b139f19fdff3effe5b77326d3e3fe3be9b49b727941000854a0daa5b1fbe02c855f4e32f783a7b41c560c269e58f9d6fa5b54da186ac58285da72028cd53878970f49f2fa7bdcd25210440e93e95ed10e6d139703ced6d2e1d219eff663b84399c1968c1ed9b4a7bbb92e590c9899be814bec50f8cc9590a4a163f4a2ace0f47ffea5caa2c991e0210705fc9760811acb6ae8cb42b999099e42bc1082209397fe5a38cb42bd990f5a7eb6dac9da9e6f4d820e6bb0a2952eb393d36c87d65759cb55f459baba25c5fc8e9b14176d534f2f7910bf3b6e5e59ce281e22d52851c9309e7fc2f03a50bc9ee8740e84d6c9e145e3bd2b8e63b3cbff3cfe90b2849ce0cb4f0dabb3b33d2b6a439a464352853e893991a2612a5632073f7d92515a2d284a4248bdb37c5c73de99d19278ad5d6cdc73d0732d6bee45596be08f257247ffeb1f69771fbb2f7caf2375b33bb849395b2b7b80ad449de9c1b770cf1cae1fbb222e5cdd6673256ee86c9da3cc494423eb1daba78e5f07d586ddde90d2a06134e2b2f1cf8524687aa3092565977326dbbf56abc64f9bae529766d79896243e2cffe2ec684d34adb270738d69efe45c458645508c0e885d0ffec48950a53034d1b9ea469f536b4f9893dbb7b3b8a4ffbb0daba39da7d24238b878b5e3fdb42823370f5ecdc2fcfa7c22fd655539fa767f6aeb5909b07b986d0cfb1f05e43e10d7ddd4d3115fab6562a0f29a44ad66f50e5e442e9dad0eb8bd2c1b8ef26e4e9517cda17d9a698f830f136b244d67b48981bc330399ced28b28f30abbd8566d066e7fd6242218c1008bd5a2295a595cf034209c9c94d6d69e5f380504220f5a595a58e704220b5a595a58e904220b5a595a58cb04296e9c15899ed28a447582110fad70ffac45f52baa4115a0884862e9526db514887f042c24b2bff2ffc0f9b380215ee059b7f0000000049454e44ae426082
11	Desayuno	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000ccb49444154789ced9d6d6c5bd779c77fbcbc7c114d5194444b946cc74b6c2556ec34ae81d8b133a4499af505c3967af960f443932e438761fbb0adfbb2b535b61619b6622952a02f18960149db0f8b8315711a245903bb059cc6f562d7915f342bb12559b6de284bf41579495e5e92f7ee834c8aa4488997e2bde115f80304e81e9e739e43fef99c739e73cebd7424feb157a745d3207cd20d68514a4b9026a3254893d112a4c96809d264b40469325a8234192d419a8c96204d464b902643b4dce09924e2500a2192b5dab421b45e91ecde36b20ffb2cb56b99200e45c7f34ab4e985c82344b2b87f19471c4a91fe6a17bad7618d5d4bac009e5725db88518c10c9e279256a9d3d2b8c8843298409d50a53a62044b28843296b6c5962e47ac60a33a6e21c495b62c71a41a49c15664cc5a158b36dd49af636199608a2852d9f5d371cabde832582642c9ecb9b8155efc11241f4a013f5a98015a64c417d2a801e745a62cbb2be24bbb70dedeb3f46bf779f55261b86feef8f5b66cbd2415df75bf32db333960ae250352bcdd9124bd7b21ce7aee098b65990e871a049b98d35863847d2b88f2fe2483f5f481bd92c3297d3d9aae8dc93acec39633e8149af83cdaacea05c39cfa4d7c1151774e4607fb272f01611e1629b838e1c7c2aa5e3ad906dd1091ff81c78743890d4f11499d33d0e94bfe8b64414d3057148393cc7a492b4378053b796171aff14d85356ee2cf06a5283e4d2f5e781cf95e519057eace8a02c5dcf004f95e589023fc8422abea4c239e02fcbf2a4807fcb2de7790ff866f17b48ebe827e3f074709577da184c1f432a2dca9d2abb3e5ba15c79dab50a792eaf512f2c0950dc82d1b26b80e9b2b4e89d7cc5287319aedd4a91d5cc5d42315d907237afb4903d5d21ed76853ce51f64f98756a9fe4af6cacbd552cf0d49674ece70612ac1a262de3682e982bc2be50adfdc29e0950a79a2c0ab2c7de0f9ffcb3f9054517a0af8e59dfaca79a5a8ec292a7bdfbb2c8b7096ca9ef506cb1e380afcb7aa73f542867456637826c9f882628ab738cc3cfdae283adffbfe2d94f4c63960ffd89f7869f32fed1e7a44819d9bbd74781b37149bea21e787521b4a0c80eb5796bb2b33bcc55441ae58b4a963250bb32ba7df3331b561638ba982ccceda2c08ac81f8edcaf150de5b6ede5edf97d05441365a77550b37a53417a61224d4fa76495b3b86269050735c984ad4e52d2d414ca41e6f6909623246bda5258845d4ea2d2d412c24a1e6b83c93642656fdd0a0e5c741f61d39cca78f7cc96ab375f1e1b1e39c3ff67a43ebcc693ae30b0a0b890c039bdbf088a53e61b920c16dfddc7df021abcdd6c5f8e90f4cab3ba6e4189a4a7057a787be80bb906edb2e6b568d56fcdf4ee4bde5f24c82747629e0b4a520bf912ef393e9779173298e8ebeccd16b2f7fd24d5a17796f9989a9d67759ebe1e773a7188a8ff2be74991d6dfd7cf9d2f3ec6ddfc98bf795ef01da8fbcb7d8ca437e32f90eef4b4bbb14a3a96912390561729aa3a32f23e7acb95dc06c6c2388eb851ff1d3e7dec59f285d517dcf1fe5427c94d3e7deb4b43d0eaf1f67471fe296dd88bd03383bfa7088eeb50bae812dba2cf75f7f03e76bc7f9cfbffd14f2269181f13857ef6e2fc9f3c3f4fb7cf17fbac97de1b3a6b523b82dc4f6470669dbbf0387c75f314f367295ccd819f46c7d372835bd20ce63afe37ced38007f7862927bc7620c8cc5f8ab7f39005010a76f2e85eb9bdf403b7b023dd0be5a9586d97e689081271fa4635b68cdbc62ef00ceeeed28678fd5254ad30be27ae14785fff75d8ab2ef529499deb642dad1172f3230162b5c8b2ffc90cc77fea121b6451f7cf15f9fc5d76d4c6087e8c673ff1fa05c7ccbb0cda61e4384e1111c932bcfa4f445527cf7f9f3fccd4b574ac40070be7372dd76d5649cb173efd0becd61588c3c42471867f776c3e59ada431c372b9d2b59e2d13391ca652a086884c5d97126864e92ab730c28c6d9bd9ddcc284a1324d2d887079c47099db807cfa03861d737cdbfd6b1e09ee21eceee4c1f69dfc7eb0fc7ce432b94c9ac9e1df109dfc681d2d2ec519ec335ca6a90531c224f03b2001f0f457197bb817beb58ff7a5cbf4ba3b7924f80043f151f6b6ef585136159b6762e857a462f30d6d53b599d86a34b520b9430f217e6fed7c97eefce5997aa08bb7beb57c635044bdcd77afff17614f1717e23b78b6fff385d752b179ae9e3e5ed245699a862c27d1340d4110f07a3db8ddae06bca3b569ea41bd162629150360cba5287f7ee404a1f17821cdb520f374cfa32562446f8e3072eab515e3852008f8fd3ec08124c5999e9e63727216498aa169e6dee3d2d4826887f6affaba0afcb6ca6b63077b982f0a1e27db33ecf4f517aea3374798b8f0abaa750b824030d8ced6adbd04027eb2d91c92146772324232a9d4d6fec4424df94aec1a2e6131daee5d555f3b0fac75f26bf0e4f24c6df6a32bc0da62142308025d5d1df4f7f7e076bbd0348db9b905a2d1c535cbea8a5c938d629a7a0c01d077ef82e195b3ad0430b64ab9c113530c9e5812e39edf4688f5b6b1f5199721318a71bb5d84c321a2d1456439492c26238a4e0281ea03b79630be4fd3f41e92adb2dd6b64427ccf99087bdfb88ede26d425461e411008853a0906976ef18e46175194eaa7493469c6b88dba5b6711daa1fde8dbb6ac489f34584fe0a1fb99890c35a44dc1603ba15027009214af9847cfaae41637a0200099bf2bdd80bacd9d78a346049f87d09f7db621d1771ebfdf4720e04751d215bdc468849ec71682e48e1c2e997195df5db51677fdd33368cec69f33eeeaeac0e7f3569c75e52257ebaad3168200a82fff80dc91c300c89eda36825c8176ee7bf1ebd0e92949d7348d584c46926224930aaa5aff29fd50a87345d0a82b725ddd15d848103dd08efafd7f2635f37ff4bcf5ea9af9ef3a7298878fff0769ffca6e4a100402013fa228128d4a4c4fcf71e3c60cd1e822d9acb153eb8220208aa5f751666e9c375447314d3fedad44c7ee5decfcda335c7be9a72bd2b71f394cdf179ec0130e317cf267abd6e3f7fbf0fb7d48529c584c2efc0583ed8599542d78bdcb1ea867d5bac70fb0a920000f7ce7efd9f1b5af90bcb3dcdeb17b17aea29dc2b173efd43c880783ed04029b989fbf4d32a92049719249857038842018eb44b2d3c3756fdf828d0501f06ddb82afc294585e98627176dc505d8220d0d3d34d2c26138d2ea2aa19e6e6a284c36b6fdbe6d11599ec54f9ddf3c6b05c10e9e6b4a947340162d2c5bacbe6c796f9f9db284a1a498ad5dc7d656e9c5f977780c9b7451ffd76e55d3d33b9ff8ff733f847ab2f4ad682aa66989d9d47d334b66e0daf18b8cbc92dce90bef8f6baedda6696550b2e9f879d4fee6d485d6eb78b9e9e2e0062b1b5170933a3ffdb10bb1b4a9081271fc4d5b6fec36a79bc5e0fa15027c9e4eaa72233373eac6ba9bd121b469072efc8667385658df5047e7ebf0fb7db55b50e2db14066a2feb8a31c5bcfb28a29f70e5174a22869643989a2a41104019fcf8bdfef2b891b6a21bf90588e9e55513f7a6f5ded2e67c308b2fdd0e08ab47ce027cb492429862c2791e5247ebf8faeae8e9a638c6af93263671ad655156c35b4b632bc16fdc4c3e6fbb6ac7aa0cdeff7b1756bb8307d95e524d3d373ebeacab291ab64eb5c405c0d530509f75a7352a392775422186c2f44dfd96c8ed9d979c36b57b0346ea81f577aa8d3fa315590c15dc6faea7ae9fff43d35e7f57a3d2bf6c78d90bc35df9078a31aa60ab26f6fdbda99d649705bc8f05457149d84c3a1c2ec4996933595cb24d3a4874fae3b1a5f0dd3c790c73fb3c94c1384ee5bb996550b8220100e8710452792145b337f2699e6dacfdfc4e3347e92c450bb4cad1d78e2313f7d26feb2c0a650fdcf94cf2f286a9abeea61854c32cdef5e7a83fe7ea96a9e46614960f8dcb35ddcfd7b8d8ba08ba9e5269ad570bb5d747575541ddcf3620cee89e1729b3f6bb44410afd7c173cf76f2c4637ecba6c246c8c72be54837e7b9fe8b372d13032c0e0c1fffcc260e1ef0f1e18514e3d755c6afab2816fd949051948505b48fde66cb960c60dd97c8f248ddeb7570f0808f83076aff8194d3e3d5075d6f6fe31fff9d9d1a461b3b83c1159686608bc5c54deeea1fbaa6543ea8560f7a5645fdf814ead89986d569145b08b2bbcf575514adcee336e5e41667503e7cdd94e51023d842105170b0abb70da7b0b22fcf2d4cac2b50cb7b45fae2db759d566f34b61004969e22bda7cfb74294fc076a143dab9299388f72f6d827ee15c5d86af97d93dbc9c0e6364622a54b1db98509d48f4fe1bef7d135ebd015996ce4e3751fd7310b5b0902d0e513d9b9b98d6bb74ab755b391abe4166770f5ef410886113675034b0268e9389a34436ee146c3f72f1a8ded0401e8f1bb48a4732b9e5da82bf2273a436a04b61943cab9bbdb4b8fdf9afd162bb1ad20003b37b7ad1aa3d8115b0b02abc72876c4f682ac16a3d811db0b02d563143bb2210481e518c5ee6c1841603946b1331b4a10588a518a9f146d37fe1ffb6b0bc6ca10c0a70000000049454e44ae426082
12	Filtrantes	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a0000114149444154789ced9d796c23d77dc73f7371484aa22452dad5b13abc92b5bbce6eec780f3576e075d64913d78e9df8af38059c146d022488fd4f03146d0334281aa4015a20488ad8499ad875eb386de1ab3e121bb1610749ebddf56e7cc6bb8af75edd374971c8b95effe09222454a22c5114539fa008234338ff39ee63bbfe3bd797c23fd63e84ec1163583bcd10dd8229f2d416a8c2d416a8c2d416a8c2d416a8c2d416a8c2d416a8c4d2b48e7ed07b9e5a56fd2ffc54fa0360436ba399e216dd68ee14d4f7d9d407b73767be4d9e3bcf7c31730466736b05595a36e7403d642e7ed07f3c400e8b8ed201db71d64e4d9e30c3f739c99136736a87595b12905e9fbe227963d961166e6e419cefce8f94d27cca61324bcbf2f6b1d8a2cb3abb78b78c2606c7a06d3b217cb5ddf47f8feaf10fbfd08171efd15c3cf1cdfa8269785f2317df73736ba11e5b0f7efee26d01e06607ba499c6fa3a027e9dd6e6267c9a86914ae1b86eb6bc1e6960dbe1bd74de7e083b6e101b1ad9a8a697c4a60aeae1fd7d1cbcff2b40da3aaee9eb41910b13c5f9f80293b3f3c41346c1312316e717fff1df3cf5f04f31620bc4458a3a49272069b4cb21fad5560ef8bad151b9eccce152ddcbb3a90439f8c057085fdf07405b4b98b648f38ae5d3ae6cb6a83066dce0bd475fe2e2d3c79819192726929cb367981309e26e8ac37a3f877cbdccba0952c22e72f6f561d30852aa7514c3b46cc6a66798998f153d9e499995b105da9510979d399e4bbec38230f94ce05a029246d44d7af6bfacc4a689214b6347a82e58f2671545a6b1be8e706308c775312d1b2116efc386814e7aeebe89c8ee6edc990582e329f6fbbad124856fc59ee75aad93362584212ccfffaf82b66e0641c2fbfbe8bf92ea2ab24c6fc77664492afb3c19615a9a1b9165192399ca13c6d713a1e14ff611b8be1b7b6c9e6d137044dfc583c6ab44e43a7ad53086b0d635aa6c8aa193dc7e476bb8a96457b51c8a2cd31669e69abe1eda5ac2f8b4fcecdfffa16e9afffc230034c9013e1f1ce4b9d43bc4dd143b942664cabf194aa5e60509efefcb06721c9b8bbf7d1533e58d3fcf0ab3332d4c2eb33ffe75f6efab94081ff50df0bd85579091b84a8da04bebd385ab794172ad6361f82ca74e1ce5e99f3cc0ccc498a7f598d6627c48fef622c6c98b79c70febfd34cb415eb5ce2323b143692224fb3d6d03d4b82005d6f1ceeb002c44e778e6c10778f7b5573da9c7b4ecbc0c2cd73a72f9947f2faf9ae7490a0b1989ed720311b9ce933664a86941965a87954ae51d3ffee22f78fea70f55ecc2c6a61747888b5947863d6a1b4149e3b43d91dd1796836c571a3c8b2b352bc872d6b194f14be779fcfeef3076f1fc9aea29d53a327c48ebe2b875216f5f48f27b16ec6b5690d5ac23173395e485471fe2f88bbf28bb9e52ad23c36e751be34e0c4b3879fb7549f524d8d7a420a55ac752de7ded559e7ef001e2f37325952fd73a00c2721d0db2ce79a7f0419817c1be2687dfcbb18ea5cc4e8cf1cc830f70f0964fd2b7efba15cb966b1d197a94306fdb235cadb666f7591f0c62ef498f1e84682528bb58ae43f497c358e3856369cb51731692671d50b275e462a692fce6b927f9cdb34f2e1bf0d7621d19f66bdd1c3717c57376f8306f6ec4e9d1b33f5257005f4f3d917baec6d7557a2656738274de7e30fbb739395c96752ce5ccdbaf2fdb6759ab7500ec509a18505b792af916a25ec1f86c0bc29fbe94ad9ddda42c8bed5d3d00489a4cf3a77bf1f5d69774ee9a1224d01ea6e3b645412ebd75a2e27316ebb354621d19ee0a5cc751f31c3ff01f63726e8ec99959de38f57bbef5bdef73ffa38f313b319e2d2b3768045a4ab3929a1a5cdcf39777d230d099de706c744d213e3385eb382b7fb00446cebdc7d8c5f3740fec666c660e23650269eb588b20baa4728bbe8bd74313fce7cb2f71e2ed538c4c4e71f387ff882fddf339144dc5588867cbd79f7570c792ab3e5ba9a9e721b7bcf44dd4fa25198a63931cbfccf0a9375988ce575cc7b61d3db41db829bb3dfad59f96e5ae9692fc74187b60f57961faf373686f2c30e71a4cbaf165cbd594cb7ae5ce7fe0cc8f5ec08ee7046245c5dfd14bdf913bd87de311ea428d15d511ee594c18ca8d1d9520d9e9fbbe490ed0a18496ed44d694cb724d9b999367b8f4f8ff214c9bd0ae4e64df6266aed485085fb58be6480bc6fc4cd901bf2ed448ebdec51835f9cd67b1472bb33a7b770037a2ad5a4e3d6d204fa5dd954f52a9937c24b00a9ed9d794201932c22cbc7d1e296512ecd986e481307d076e44a90b016b8f1d4b295990a145410054492624fb49081387c5593235d531ec18eca763f06a3a06fb693fd40f806544b112b32c580a861ac6cd199af0b576d277a4137372984b6f9d5831c6d4851af1b57666b7f59918cd877a983d7661d9cf9482f095e8f5edc2502d23d1ad3433eec6b2cfec373ca847f674f2c12fdc4cefc7f7a10655ccd834c6fc1856621ecb88169477eb3b719baf46a88581d49c1c66eafcef991abe54706cf78d47b282a84e82163581af21829b70997cf13497feed28f153e3059f5b0d67872fdd439700253f2e88cc3e21d05f8e22c597cf1633c17ec304e918ece7c07db7d27ea89fc4f4258cb93192f3a53f7472eb3b719a07402d1c377262b38c9f7a232b4c5da891be2377648faba3479192e98ea1bfb18d40531bc1481773c72f70ee5f7e55b1d5ac950591aabe20be50808f7efb4fe9fdd83e927363cc5d7e1bc7cc1feb11928cd0745c5547c82a42d30170551da47c1721a921645f18a4423f9e11a6a5f7eaac7548a959d491c2075b8a2f40d38ebdf89bda987a7188dffdf5ff60c7aa33f52797aa0ae20b05b8e3917b89ecee64f6c2eb24a6175d8ba3d7e16a015c5f00a1ea659f7b256172719397c18c215b06b26920a7e2486231a806235d34f75c47fcd43827eff9f7aa8b52d52cebb69f7c996dd7f6327ff91d16a62e20140dbb2e8cd5d886eb0f21343fc86bcc33dc14c29a036123293a484a4111e118086b066419a1eab87a1d4eb011a1fa90848be4d8584614e1d884765e45e3759d8c3ef16685ff757954ad63d877db87683fd48f6326884f9cc5aa6f2115e9c1093615b8a14a10761427711e37398a7096b8426bbaf003928ce36fc06ceac46c6c474832f189b3b88e45d3c11eb6dd7a8d676d2b05cfd3de86ce703a580ff6d3d0b938b5c64ea4fb0aae63930a77adc92d958370e208278ea40490b4c8957d2b3f9770f53acc7037be998b08c702452372533f133fffddbab635174f053970dfadecbff793c52b0aa605d002211459a15ad3978563209ccba5975754a4a676145ffa61931dad6e0cf1cc57dcf0f5bb961563296dad3dc84ef566949783ecd8b475edcd6e573ba87b2248c7603ffb3e7fb8e4f27a204457c700baa87c58dd4bfc08ba3a065054df86b5c11397b5ef0b3717ec1b3df61ea71f3b466c789ac89e1d5c73f78d34eddcb658b1a6b3a36b2f2923cae4d4454c248487c1bd5424e1e243d0dad28d1e0855bdfea5782248efc7f6e56d9fffe55b3cffe57fcd6e8f1c7d0f3d1428ead2f440881d5d7bb1ad14b1e804092386e9b8b84a61daea15b2e3e053648281069a235deb56cf5aa858908ec1fe827daf7df7e7e53744d3698e7491f94e946da5482ccc9248cc61dae9af00b8025ca5f426cb8e8d2c812249a88a4a30d844b0ae19555bdf0caf12d665b477fadde1827d53ef969ee9405aa050531ba1a6b665cba48c28a95422bbadeb41cfdd4efcddf2071c2ba16ac3ef66b4f4b949a5a20742ebeef7376596550a2347dfab56559e52ed91df751124b7879ecbe8b1cd25cadcf1ea0fc3572cc8549178d1317875d1b29bcd4a36e2b948c582985183e953f9a2ecbfaf788ffdf463472badaeaa547ba4173c7259a71f3b96b7ddd0192eda598c0dcf14cdc06a91f8a97192c3a5cda2f7126f0479fc28662c3f8bbae16f3f43efc7f715947deba157bca872ddb9f4f0b1d50bad039e0862468da217fa13dfff0b76dd3598b7eff4e347890dd7f62263c9e139469f786343eaf62ccb7aedbb3f2f9a45ddfcedcf71c723f7e6655e2fffd5235e55bb2ebcfb374f6f58dd9e3ec23df7cbb7e83ebc87604b7e672d13535aaed94173df7662c333b47ea09b404b8357557bc6fc894b9cfddec6b955cf2739e44e64d8ccc44f8d337bec0273c72e307bec42d57aeceb32ebc4170a30f8b54f71cddd377a7dea0d237e6a9cd127de60f48937d7551ccf05e918ec67d75d830cdc75c8cbd3d614a34fbec9d8136fac4bc7d133411a3ac3dcf0f5bb0a9e8dac8e40b802a9c205652ac14e2d00204912b2e2432a71887feac52186bef582a7fd154f467b237b3ab9e3917bf195b0a0b163a5b09331849bfff856d174d44023d21a965d5a2b42086c631ec7ca9f3d2fc90aaabf016595e7262db70cd074a88793f73cbca679c1c5a8f8b62c470ce13a5889b90231202d542a3a816b9b9536a9245cdb24159d281003566ee752d4069deb1fbe87fadddb3d6957c5821cb8efd692c40056fd07adc43cf1c973a46253889c9545bd44b82ea9d814f1c973588995bfac538a20901665e7574b9fe4b1e2b92a3d4139314352d4f42c45b1ccc59664100ea9f8349611456f88a0052afb0a5b2e96314f2a368deb588bf52ddb58b9e4580269f7e505150b928a1ae8a1d22c449264f4ba664c631e51645e96eaafc34925705d1bd7b1d25f51884ea0051ad1028dabfaf46238560acb98c732e6f3ac4e965514bdf8ba8d92a2e20b342295310bc6ab54b86241cebffc3abbeef870c9e5254545af8fe05849ecd4429e309224a3faeb118e8de398b8b689705dcc8559cc8559644543f1059195c58b292b1ab2a2e13a56f6ce7752095cc7c631138bd6700559f5a12c9349498a8aaad7a168e5af5532f6b237d34d2b16e4d7fffc337a3f7a2d7a43e9ab8402289a1f45f3235c07d74ee158295cc706e1a62f8ca2822f88706c5cd74efb7321b08c2b7e3f5e64e2f412645945563424594196d5421124392daea623ab3a92bcb6a94756cce0edef3c858fd5bf6bb81a95bbac98c1535ffa27eefce1d7ca1605d229a6e20b66e7d2e60a90f991affcce4388a24157921558923a4bb252f05354a03560c50cfef7cb3fc08e263d11a4e28ea1eb37119a4d43478423dff8333a0fecaab8512b21845b34fe2c4552d4b262c05a983e7186d7fffebf488ccea2993e7ca9cad760acbca72e099cba244882b01264f7ed3730f0a53f26d8bef232e09b99c4e82c433f7c814bcfa6d762915d097fa21e4954dea9f564e844282e7575d0a42c665b5db7ed7fdf09b35408004948f8134164d79ba9af9e08522fe9b4a9f5a4fc068e9aefd743031decfcec47683bfc01b44df8ae282b6630f6ca3b9cfdd9af892e79d585622be8c90092f0ce35562c882ea9790b40da9a85e54be2ca85a70d0d74d076f803b45cbf93c8febe82e3b5c2f489334c9d3ccbd82bef1488006917a5997e54abf220be948ad20c1989f6250ba9a896866a694585890e8d101d1a61e8ca76e4fa9d340e74101ae8c8feae36d1a111e6afb46b7e6884e99367972dbb9e4264a8c842ba95e65557e1746507cb67e2a836425abdaa607b3381f6661a073a501b0268f57e1a73842ac7b2a673de3f353f3482154f62c70ce68746304667498cceae7a0e494828b68a66fa3c8b132bb1ee93ad6537ed67212d8ea3da388a5d106b3224ae5ca895eed4f546b115144745b1d5aa88904b45825c76e6d8a13495bc56adec2ac8a682467a4cca516c5cd9c5551c84e42e2bd27aa2d80a9290911d05d995519c8d5d8fa7a2da5d44d9a2e4a2382a8a03e40c3709c9c5955d842c10527a30d051ec9ce3e02aab0b273b0ab91e3273a1252123b912b22b7b9a1d7945c5b74346946eb5198dcacd5b12328a2343ce35cf58d41f029edc222e8251275af5379abd1ff1cc6653c2de90d7ccbddff0d4896e8952399e47b594b019712a5fcef50f957549330c6131ee167f67e0162bb36e795fd44d6e89b206d63511df12a57cd6bd67147593ccb9de7f47fdfd4a55baaa936e9ca8a8fe82929b91aa8d1d8c3bb12d514aa0aa8339e34e8c05b1f617b4fc2150f5d1b5312756d5f7936f36aa2e486630724b94e26cc8f8f39628cbb3610f0432a26c8d7be5b3a14f68b64429e4ff01015370138f500d3b0000000049454e44ae426082
4	Golosinas	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000fa049444154789ced9d6d7053d799c7ff92aede2dd9f8457e1132d89671b011a1b59798848e099492618ab730617771f6cbaec966779b366e5366baec4c893bd374da401a4fd399ed1036d30f98b669970d99b890c4216431186a270db231b61c19646c6319cbe8fdc557d27e9025fb5af7ca9275255d157e5fec7befb9e71e9dbfce739ef39c73ae78c11ded413c8233f0335d804750792408c7782408c7782408c7782408c7782408c720325d00d6d9bc167e8f03a4cc173925f00a80f920885147060b161fd92f48b112d856093c5905e8d4008040f775783b3aa3d30a0181761d84a25c109324f8339e34177665b25790cd6b816f6d0989b10ce1aeadf0755f877f6034ea9a7ff40efc0bffcbfeed1f40f44c0237eea6b8b0f1937d7d885c0cbcbc1bf8d9015a31c2880f3db3725e1585a17c5ede1dca97036495207e8d1cae5ac05fa95c312da1d342b0491b5fc65fdf08fce69f80caa2244b983c5923c8bc4e0197cd00f22f4370b6bd06f71b9d083add31ef11edda1aff03e422e0578780dd1b932c6972648520f33a05dcb7be40d0b5d809cf7f7c1dce975e83df38c1781fd1a88b99afa0521d7df2fbbb332a0ae705f16fc883fbd617b4d702660b9c6daf61befb3aed759e5cca68b6783209787229fd435f68ca98f9e2b420413901d7f4cd15d3b93b3a19451154d0b40200025d35738672117c4fe623284fbf13ca6941dc6b7d143315336d4727adf9a2354b0875fa4c04cc16787ef72778aae22b279b705610529b03d2783ba17b5c477f19d5d1f354f9b46963f52fee374283caf9a111f80b8509952159382b88973f97f03d4197275299b11054a8c16710ca77ee126540e9ca7383f407122ecb6ae1e648bd5809f1e1ed11b342ea431514189b00a91f45606c0201b385f656f29a1ea47e34a649123537d19ef71b27e079eb2ce55cf08e09064f11ca8242e4e64856f36912829b827c6b0ba54223ffebb491ca247bf598efd563fee3e8cedc7be63c08dd8bb459f355f910d28c4ffcc609b88efe92f69ebcba5c0cbe3b86d242053425792004a9332cdc34599bd7ae988468d441dad6829cb77e04e14e6a05fb074623ad0acbfa14c94b2d517985c5607220e482501e53f7edf862640a5647ea8292dc13442e062a0be34ece57e543dad602d94f5ea4f40b6137d83fb6e87989f6354599b2f9eeeb31c500009816f3f0fa480c7e398db1094b4afa16ee0952b5ba0119a1d342de712432ee207b6f84fe2eb41441851ae296c58063d0e986eb27a7e0eee88ccbb52ed015508e53d55ab8274802ad63393cb914f28e2310eedc8aa0cb03bf7102fe8151082ad490bdfa22787229824e37bc67cec3d1da0ef29a3ebe7c6512f072a247f5a9682ddcebd473e8c3e07ee344c4830a9a2d515e165f950f9e2a1f844e0be1c218c3f73fdd1131fcfa51ccf7ea41f6de887bb00900c4133a489edf8fc00fdf05403f6f3275df0e8bcd0dada620694f8c7b822c2160b6c0f7ee25cc775f5bb11203660b60b644c6107c4d31e0f68257b406f643ff91f0b3853bb742d4dc1419e9cbebd4b09c1f604c1f6e2dc97a629c1424e874c373f22cad4b1b0be2091d848d3af0abd6c2f38bd3e06f5e07c1ba12089fdab2e2f84550a106bf420d42a705d1a88b0a3c3a0799a3ca4b49b6b5704e10d26c86bbb53d21b322dcb915e29667c057e5873aeba36f22303d0b517313dc1d9d90bed402d97fb6529fa31f0d458319625dcbf1db62cfbd2c2599d6c23941609c895b0cc1262da46d2d1177372c867f6c02c29d5b21587071c38147c9f3fb23f7c61ac9d3415a13f7a656d35a38e765f17df1154972783fe4af52c71e9e936723e30e4165285e1576837def5d82f3e89b8c266b25e23559cb49d413e39e20261b7852e66f134f2681fc8d2351f128523f4ae973f80b422c0d93f80746e1fceecf19e74e98b05dfd32a1f47484c72d16ab2b663ace090200c4fa72daf33c9904b257bf436bf73dcbd661853b65e1aeade0c916050eba3c707774c271f8c7f09dbb14578b998de15d2582d747e2d6ed19dcba3dc3d85ab8d7870010ce4b314f739e498cf9eeeb51151b4ec7934b21fadb1df09e394fb91e305b4291ddb7ce8278420741a51a824d5a082ad5511e562c77773558ac2ef43b3ca8d614203f5746b9c6494188213bf8257908cc3e889c931cdecfe8112dafece5880f3d432b5a18f29a9e71d4eeafac84777c75fd4e2cfcfe006edd9e417eae0c5a4d41c413e3a4c9020071d1bac8ff824dda9873187415bdfc1c5d94371eee0dc5b6f9c962b1bad03f3411e95b382b88506f87405d0c0090b631572653071d98a60a42e8b4901cde4f9b96097f6525e6f43309ddb31ac2ade5d6ed19ee0a020012bf0ac29d5b19a75b01d0aedf0542b38bcb11353745cd9d302215c3f8c1747c6959c26275715b10c1b813d240ec70bc9fa6e281e8161246dad6129728130fd6c0fb20fdabe3392d0800e0c321e0a321da4b915941ba6b3142ebd2b69698e6cba6aa4a8ba9a283fb8200c0890f1945612260b6c45c662a6a6e82fc8d23510be96caa2a98ba6eafa694ac901d820021517efd6942b7f8ce5d8a795d50a986bce308a42fb5805f5e8a7bc275191503c8264100e07fff027cfb0c60bc1f5772b2f7c68a2be401c04eca70f3233bee5fcdfcc61d4e0e0c63629c01bedd195aa1fe37c53193065d1ef8ce5d62dcbc63bbfa25c64f5c80f54af2b12ab6c83e41c27c3804c21d84ec47ff0edf0797e1bf6140d015dd1abc67ce47422300e0bd3b07eb95514c9dfcbf5547705349f60a0200db6b406c7f1cc4f6901bebbad807cba93f412ca55a62e7db9fc23ae18173703225611036c96e41b6aca71c5a871f3074cac674948615b2ab535f8ab60428cea59c329fe9c95061d8237b0579e671caa1777c164ebd294385618fec1564b9b9ba3c9c9972b04c76f62125794015d5e5b5747d1695cc29b2634269825532072f418d4b110102729f02b99e3528701541ee53a4b4c8f1929d826cafa11cfa6d6e58deff3c724cf24918f38761ce9962cc82e493b04ae66095ccc19467849894a0d85186325b398840e6aa252b05211bab2905b75ebeb5788d4f425fd20fa7c89e509e5ec203539e11934a13ca6ce5191326fbfa901c09887aea2b352c5d8badc394674c588ca5907c12a63c23fad65ec674cee4aaf3592d59278845a7893eb760ae720ed46152c98ea745f249180a6f425fd20f924fb292673c64d46479833e4cfb670100e364b4bdd710a50080624101c43c11487f00bcaf51dfb260eb1906697541fbab569882434074df9e1456c91cfad65e86ee5e7d5a3afeb40a62f6cfc2307f0726ff14cc7e0bbc415fec1bbc8ba648cc1341492a51375582f5c322a86a6a0100b35d9f63dd4fff0eaa434fa2e7fb6fa7a4dc249fc4e765d7507dbf16c58eb2943c230c2fd52f52b6061c18f08d403f6f802dc0de1bdd94a56a343cd78ab9d37fc6eef7ff0b0070e6f9bfc7787f2f6bcfa023d5a2a4ac8578833e747b7a31e033a4247fdbd4043e3efe63ecfac1b194e4cf84a130f4aa8f5489921241067c06747b7a5736492ca069d896f2672c2795a2b02e4897fbd394b50a3a541b163b798962e5179bb1c558fe08727c0ad63b7a56ddde748bb11c4d4363da9e45f249dc547dc1ba4bcc9a203d9ecf322a060094d7a7d77c8547f76cc28a20d680033d4b5cd44ca1aaa985b234be2d6a6c31b910bc640b5604e9f1b23c1a4b80e56eaeaef960dacb305238c85a5eac086298bfc34636abc2d44715a4a1a515e29cf486d2bd8487b5b8172b82a4c3bd65c2f0c905cab158a144c373ad0ca95347ac507f22645d707139e6e19bb04d5197f334b4b4a6bd2fb14ae6928a3287c97a4100a0eff429cab158a14cfb081e006665c92fd0664510ede627d9c866d5e8df7b075e07f5db59fdf41ed4b7fc735acbc186b7c58a201b0e1c60239b55e3b5dbd07dbc3deafcf617be4719c9a71ace08b2a9f92034f5e91b25d33170ee9d281758ac50e2d0c9dfa7ddeb4a06d6fa9003af9fccf807ef3af67294e9ca36515811c4e7b073e2835b27efd29a2e554d6dc6cb162fac08327231341650d5d4e25fdfbf9251f33570ee1df477fe77d4f9b028e9ec5356033ba1935fff22f27fa8a5fc0e7b5f399eb66fa444495de3db7dbc1d03effd212a5d5894ea1ddf4849390a5cc9bfc05ff0cafaa75f4936938fef5f02c043f992f0b7aaa60e5f79f61f212f2cc2ecd868946d670365a91abb7e700cfb7efa26ac9377611e597c71bfe1930f905ba689ccbd8721c4626cdcd30cb14289b12bb1b7bc25ca5aeb7ae424393fc28a2006f20e6efdf9625405106231ca745f45c373add0d46f8358a184f3be392971541b36e2b13dcdd873f455347df78750d5d40100ca1bb661ecca2770ce2e6e7763120500ca745f45f58e3d98bdfd256c53c96f6593fb14d0ce266f0e5959e4d0e3f92c127edfdb7e029bf63d1b33bdd76ec3f4f04d8cf75f8575f22eac93cc15a2aaa98544a184a67e1b8a6b6a218e312be8b5db70ead9afc33143ddf0ffd40bdfc3532fb431de67b87801ddc7dba34230f14204086c997c021292e1f74812801541bc411fde769c8dac2aa97e7a0ff6b69f48ab57e375d8d175ec65182e5ea0bd5eded088fdafbf15b34c868b1730f0de1f60f8e483b89f5b282fc7fa610d2b62002c2e03329153f8adb32b721c8e27add45a92c5ebb0a3eff429f4759e82d76e0310fac64a48191c221b25ad58a1c4def6132b76ea5ebb0d868b1760eaef85797810e611ea1e7965a91ae50ddb50ad6bc2ccbfbccbeae761755dd680cf802e37752fb958a144434b2b74cd07598dc0daa626d077fa54288e655fac78224060a3f971e47ad6c0983f42bbb4b4bca1117bdb5f4faa3c7e9b1b03fb7ecefa2621d617ca99c8299c757d443b47a2aaa945f58e3d286f6884aaa62e2193e675d861eabb8af1be5e98faafc23c1cfd53484480885af2392b9b81a170907631c2a6e6836868694d786ce2b33b30f4cd1329d9b19592958bdea00f5dee4f579c49142b94285ef08034340b14c6fbaf0200a6876f525a011d65b672943fa8a4dd42105a383dc8181e57d5d442b7ef20340ddb628ae375d83178ee8f701eeb816036353ff292d2a5a426720a3ddecf304ede4bd523a07294426d2b8f6b7d5478734eaca86cf84ba22cd320b734f4b319d6a9bba1be64f866ca9792a67c6d2f105a95d2e71bc0a0cb088f20fe17123321f72950ec2845beab6855de8d553287e99cc984a65d890081eafb75ac8cc663911641c28cdf7b00c3ecbd85e94e079c223b3c843b6affdf52c4a40412521ad90f28f7e5b0e662927c12b33233a53ccbc9f5ac81ca518a02972a2d3baad22a080038dd3e8c4dcec196c25fa9c966d23ea72e978ab0a9aa1815656b2048e16f39652b19ab91d22225b66c288d7a6fedc34e46bfa2621181c7d617e1b1f5458f5acb029ca885fc5c19ea37aa515ac8fd19bd54c30941008010f051a1ce475d5531c4a2acdc3ecf0a9c11244c6e8e04f51bd5d02c7bd3cfc302e70409a329c9c3e31b4aa14cc3cf9d7209ce0a023c9c2e72567cca87c945ce0a418087c745ceba4ff6d7ee22679d20c05fb78bfcff65054777e41b568d0000000049454e44ae426082
3	Lácteos	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000e4549444154789ced9d5b6c1cd779c77fe79c99bdef7297146fa26ea14ccb90aca046add44683a08d534348d1d6455ae4c906d2020dd097f4ad6f7def4351204fcd438b2269fbd0a080512041d1a28d813a682e76d5c4966cc9b26c49214591d4ee72ef3b9773fa30e4f2b6e4ee7297e46cb47f80203133e7cc99f9cdf9beef7ce7cc50ccfff33f19460a8de4493760a49d1a010999464042a6119090690424641a010999464042a6119090690424641a010999464042a6119090c93ae90674d2df2d247961fe378ea4ee1bf7dee28f3eaa1e49dd8755e881dc797483978adf3f9abaebb3c0b34752f761157a20957a11d97cef48ea5e2f38900a17905f2a1f6262d398d874d7c7eb7a03e7c1c3236c51ef0a7d0fe946eec2d7f1677f0b632501105e15f5e83fb03ffa56c7b25ebe0040e4dcd9236d63b70a3d90d9fca778f5fd9f627df975fcb3afedd866ac24ded9d730cbb791b7beb36fd9d3751b381f2a28a13759af346e1eb8ffc9cc970fb50fe0d5d816682f5f0885f90a3590571a374999c681c7d4d6ee532814f66c2f140ad4d6ee1f58362ddcd04109b5c9bae6dceb788c6816595d5d65757595783c0e40bd5e0720dd2c762cff6b9115febdb165aabc7c01994a618de70ed9eafe14ea1ef2c5e6c1e60a20b7fa83d6dff57abd0563f7befdf46a746f8f701e3c6cf995e356687bc873ee12a7fdce37259dff31176efe054f667f1b373a0580dd5c61e2d1f74896deef587e4e55b96c17b8e5eeec119ba6ebb87b4a6881bc567fb7eb6393a5f7bbbaf9fbe92bf17bdc727f75cff69380125a9375cdf9f8d8cef552e4f1befb8edb7c85b6875c99b580e319175c0558db7fff71f694d0f690b0e9b87aca08480f3a0e2823203dca79f010bd2db41eb446400ea1e6dd7b470625b44efd30d242d3b01af8d2435b0dd036ca8b10f76328ad06761ee3fb34efde23facc3c72233b3028fdd20029474b885895843458ca414596d15e16cfcbd0d0eb349b51d2cdecc0c01c1594a137595a68f2a915c6c696c8a53ec1b64b08e180d00855430a8f4ce201a7b2f7a9a65670a53bb0736f4219a4f91a7a20c5e41ab9888730c1a548ab843141c7373a86b44a2034006396a19e5ac397fec0ce3f6828430da462579988e7b1232b681d43fb29101eadcbd21652d530c6c277a690769ef1c463caf1fc40db314828430dc48f9750560da16a58d1258c97c2e8c8562f1106d068e7142ab282b44a08552311ad0ed474c1e0a00c2d90a66a129506df3985f63200a8e832c64f8370404710b289f6b2483b8f900d8c9fc06fce102542c31e7cd8ba09c5f8873789430bc4951e09cb415a1540e23b53f8ce24c86670e3dd71b49741a81ac64fe13b53183f81500d84aa0561f1112880f2f1a1a10c6dd8ab856e3df940eb4a8c9f401b9be059d38109b3dbf88c5ae6e8da566fd0bcfb31d1672e22546f61f6d0029146a2dd71840c9c36c642080fa16a3b8e13d241bbe318630306211c8c891c79fb0e0b65684d96ad2d6a5e04a31348d540459791761eed2791d63aca5e0b9cbb9f44a80a2af278c3d94702d3e50e7684dd4e9b507a315f430b24ea4769fa1269af215405a32341686bad63740c211d8c8e22ad42d043fc0442365091159a38c4bdd8b1b4b35728430b04c06e6470bd0cda4fe13b3341582b1d8c9f08c623466090a8c84ae0e4dd2cda4f51af8f616bfbd8dad90b94a10692701394aae378ce29a4aa041195dee61fa4b70147a3226b682f4bbe3649ba3176ec6ded16ca50030148d7c7596f46f075109f682f8b900e00423682318a91186351742589ca29a43999cbde8472908636cada9434926cf51465378a89254928434439181dc378293c6d51a99ec36bc64937c74e0cc6a63657dcefb78e78a880f8d2c78995c072306e946833dd4aa7a79d3438691a5683ba748109009456c4fdd88983d8ae8316770f0d9046b44c227b1fdb4fa0a2cbc1c83cb5427d7d8eb8936c1d17f362c4389e08aa1fed07253c8fcd016a5a0d12d9870863235403af7e01a96a2804b1cce240d3e9c7a9768bbb8702881fab208c44a82a1836c61816d22aa384a46987ebc5cd5eb41bca5000b1ed0ad2ce6f84b46263024a81911b69f5e64937b12f6d8732143ec475836cad54150c2084d7dae73767d05ef4e41a37206dfa94a1e821aa9142a80ada4f06e1ecc65c87d1717ce11175936dcb69a169aa267a630a37ecf2f285e10012f562948a9f41a83a6082892863e11b43a374baed4a9246b48cc82d131b7b82ce2ee31dd1fcc7a03514260b20d918a3b6f62c7eb402720c7c8b68638c781b185a68e29945a410186d13954d9cb13a3cb978022def4d470a24adebbcd2bcc9171b3799f30b5cf21e755dd66bb34d6dfc008828a8a9f6f30cd248ccea736c0f863bcd48bcf7fc5b145c8bef1526f9cb954b5db773d01247f121e5b4aef37aed6ddea8beddf1a5cdae1577c1daf205c24e604d2d7455d4341ad0c39c84e769befb78823f5fbada7333fbd5c0813ce72ef1cde2b7bb7a1dad2bc55db8b806919d7d46386922e7beda7d3dae8ba954316bf90050175aaf6baedffe1c8b6ea29716f7a5813af5e7dc25fe3effadc1c150ba2d8c6ee47d7807e7873f423fd9984fb76d442e8b5c98479e3f0b5d4cab8ec5253fb8fc13aec58fef0daa8101d984313013053055de8251ee6dac618a456422817be367985d6ba544268d5a9847c43ae7bc6211c93f3efb33e6ec5ac76307a1810049eb3adf2c7e7bb03000a62ac1efd571c4f2999ea088580c6159c84402ff178b7b0fb06de4fc0588749e398c45246f2ebcd3f5b9fbd14080bc5e7b7b70666a53d97a60b200c64a98d41a24dc008adfb9d9727a0a5dad621c07cc3e6e52c9c07c75a1e924fcf1f8276df7bd3ceb723635980467df616f5ad779a3faf620dab253a96df9a98807b3ebb03686589fc4f855b85069edf6efdec37fbc823a7f1675660e00393b0352a24b25ac0b17f63d8d88c510b92ca6d0f9ab0f7f76fa3e7f9bff0c00d7cf3b5c3fd7e4fa7987742400fedd8fa2fcd58d040f2b877fe5a16f20af343b7f8fe4504a387b36093f83ca4ce12dee4c599be23a2a95c2bbf9410b0804bd444e4fb5ad5e3fc9231271443c8e9c9ec4ef02483697e06f7efd092fcc09cea482deebdf7f80fb7805fb855fe10f1702505ff9fe1837f387bbb57d9bac6ebe477228c50f580cbdabd5c6db70fcdbc71aae872995db16f73eb88dffd15d9c1ffe2870f8b6bdbf838fd88853e34174b630cfef5c322d1800382eba546e050ee988e15fbebc7e6813d63790e7dca57eab682fd52e21584157ab1babdab7249209bcc545d4f973ad6ddeed3bb8efde403f5ed9538b1002994c2163b12d879fda36d6502a0891e72fa02e2d2067670260ae87ffc97dbc8feeb600a8858b44bff49b884cba553c1d31fcf5172a1c467d9bac5ed221fdca4c17302b1e9cae035b2b0fadab57b0ae5ed9796cd301df47974a7bcc96a95631f506ba52c1ba1040144a41268dc86577dcdced727efc138454884804af780bfbdacecf71985219fff163d4f4342fcfa47979d6e57f1ef5b6fe2bdcd95ec7829f9f874f27b7b66d1f9b1c20393d89c86577f8944d599fbd8a989ec4baf46c6bbf983c853c7f16efe62dbcf76fb5af333386cc6410a924d6f397f7ecd72babe8a565fcc5c06a7cf599de7d6bb8b3bd4d854ca4d0e5de47eaeacc5c5b1800d816d6c2333bb70911943b7716916c9f2ab13e7ba5edf6d6399f99474e4db67ad89589defd48df40de89ccf3e25139f67413535b82e98347c9a654c6b82e3293017bff4bd24ff288540a110d56371abd2d92f35cf07c442e15ecab9530ce3ac6ec9c1eaeba82f7ea1b2b1f4d93146d7ce846b626a20570fde06bdca5be812caa1c2ff65b493bf91294c64c1f3ce0d4ab6b781fdc0602471ff9c2e7db1ee73c7e1363d6a09ff1ab2f89ad8df162a206e9cef3f80f8a177a3e45df3ee4bfa20777e343abde9d3334b51a2a9743e5c6c1f3f70d750deb0757b494834fda8f5980e001f97016513f87583ddd55db1cbff7b9febe81fc67ec0a8bea083e5b54dbf5524ddd86dbb370677a272cd70b5224f57a9056dfcf64990e2f79961308f78045d8c538329a42269310eb6e8e5e1de2f60e24cafa87447b33d1972a3b9f2eb1741e159f814a041e6e3d00eafc398cd3c45f5b432d5c446c7c55c1fbbf9fe3bcf5dffbf6983d9ace63b2cb071e629a2ebabc86493cd9b9c3976d736cff5befdd230c24cafa4ef2f3fc5ee3ddc10e128bf1961f0110426e8dc8b7a7556c6b8fdf30a532f81a393686fbdefbd82f3ddff97cd90e6fe54e5431fe2a26e2ed3df6ee2c2a3e855e7f8439b305f517f5de5f7b18d838e41bd93728cb01bf26b6926afda927efa3e30fe04c11664b1d8b1ad705373053c6df356af6257c3887bcbf006b6d6e9a2fe1f61ce2ce59286f0b81a7caedc13932e899aaff99c58101595439be96fb93c14259496ff98b7433c8f84e95f749ab6c4964d220057eb1b0239dd2d29324528d61aadec6971f76a96623550a953b8b284c746e67d4c35b7908ee4ef358764fc0a96fd787f669fe60e21bfcd49e1f4c85be844f27ba8eb8b6cbfedc8b445ffd523038d4bb32c7e9065a1730d9224cb459176c694ca3815e5f876ebef8f0ec23985fc69c5bddb1f956b9fbffd4d03a75cf253a6851e5f8dac4d7f9ddea4ff9d3e2bf71461d2ec9d652dd860f66f66e8f46f1d7dba738764b271fc2766b1277e1d20139b8b88b99ca63fc62d023bbd1eeecb4b198b47acf301c59eae45f93d778533ecfc54fdfe1f72377b96c0723b2cb56818cdc3bd7711cda1ed60a2f587e6a8c411b103a82e34729fb164d2303db21690d246d2f4dde9794da58cba880c95d7352be91e4e8fd7df8235997b55d61f8c0fd30e9c8b3bdd6780e7ba6775bfab4ea58d2eff6ccf489fdb78161d3b1cd87d873a791f1f0bffb77d23a362042a9437d1de769d3b1ce180650e647500ed0b14fe1ca781c7baebbf4f5d3a81399531f455efbebc416398c22aff63ad15527a3c86baf4e14c828f2daab135f97358abc76eac481c028f2daae50008151e4b5a9d0008151e405210302a3c82b74409ef6c82b7440e0e98ebc4209049edec8ebff014a93ea5584518f480000000049454e44ae426082
5	Licores	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a0000083149444154789ced9d5b6c1c571980bf19efd5bb5e7b1d27b19d38b6e38b62d204a75552c54a55681e680308a84951692544c54d8ae0c1a2209a07788127e001848484402a425521e58587c41234821ae28834699b3a171ce7623b8e2fb19d78edf8b2bb33c3c366bd76b2de8b772e67c6fb49237977cefefbcb9fcf9ef39f333b96e8eed328220cb2d50914594d5188601485084651886014850846518860b8ac4e60253f38f7faf2cff1c638f1c6f8f2e35fcdfeda8a944ca7d84304c33e42eeddb53a0353b08f90e11b70ab1f14c5ea4c0cc53e420022f7e0dac7b0306f752686612f2100d1a58494f1db56676208f61392647c04ae5f71dc47987d85003c88c0d50f121f650ec1de4220d1436ef5c39d41ab33d105fb0b49323906fdf61ff08511e29397329f77c7339e0760711e6e5cb675cd228c901af704ae2acff221074a4096968f9ad05c6e8114c5d6358b506b59a5cf54a41ec423c494d8fa83256b96fa56f097169e9c4908d3430cc186358bb38524b151cdb23184806d6a16db08090774f845daa066114648d83593f17c9927aadf9b095cb30823a4a224b39072bfcebf3c416b166184d4b827329edf5c66c067bf80358b30421abdc319cf87fcd3c6bdb940fb2c42087932d09775e94425caa19dd78d4b42909ac572213e798923e5a7736a7ba0f1a3dcd6b40ac1e29ac572219de193597b471285455ed9ff9ec1196169cd62e95a5667e529dafc0379bd26e81fe3d5fdbdfce9dcc1b4e78f3ffd49de38b037e77891d91c172d81c9b8ca9e8b6339b75f0f96f5908ee079f695f6adebb59b423739dcdaffd8f32f35efe0bbed6d85a6b626952e99af6c3276a1d212218dde618e54e4366eacc5deba0bb46c995cf5dc82aa51e6711714371332f065270ae9ac3c59700c1595cf3e7166f971c8e3e6c5e68682e366e340a98bda12e3e29b2ee4c9401f1525115d6269d21ccf36ff0f80e777d4b06f4b5897b89990258997e50786d52ca60be9089ed735dede6d8949c1eeca0a5ac2e5bac64e874b929094b861358ba942c2ae19aab32c91e48bcb354bd8bfc83f46c6758d9b898e9acd891f0ca8594c15d2e6cb6f8a9b0b2a2a4f373e3ee33292c6b260ea81ce358ba9421abd4386c4dd563199bd918e54fa3cab9fd0719fc55421351e7d3fae92047dfa4c120a46877d165385e835bb7a14975cc0d5297a53e03e8be56b597a10d7623494054c7b3f295b8302f6591c2104e0d5d606d3deab6f3af3eee632ebd867718c90d9688c8b770ddcc47a485cd308e5b33c93e73e8b25abbd71bc84dd7519db28da100b4ae6bf2c150548dc3be7ccd824df7bf7cf949e3995572eb33f7d33aff692c7cbd0ec3a06edf111989b85865628597bedc51221b34a35af37fd30639b9ea977e899fc6be638f1089294107279fa3ef70f3c477d9e42f245737bb970779d157ab266a96b8250fa651e4b3fb2145565767e21edb1148da1695ada231ddd43a354b5b411dbb4d5b07c35594676b9f8f98757d61f244bcd62a9104dd3882b4ada4351353458f348c7898121ae3cfb45c3f28d7b7c8c3c58d027d81a358b63067580bf0c0cb2e9f0e70de9259a2c237b7dfca64fc7659a34358ba94226a64699981ae57e449fd990aaaacb0740efd82497a667e87bf13bbac45f49cc17602e16e7cdab37f50dfc48cde2a81e02f093ff5e644bfb7e063b5ed02d66dce3a5c4ede667172ee916f3311ed62c8e13d23b3649f7d028ae578e11697ea2e078aacb85e40f70fbc182febde351a24bce1302d0f5eff30ccfcd33f1cd3798f8c4fe75c7515c6ed4d232a2aaca974e9970f9110e1bd49344a231be71fa2c318f9fd8b11fd3ff5c67de31e25e1f04cad08063efbdcf1dbd66575970a410804bd3331cedee21128d11e8fc3a7dc77fcbc8539fcafa3ac5ed21162c47f295a26a1adffed7394e0ede313ee18708f5a54fbdb9343dc367fe769a5f1e7a8a83b5dbe1b5ef33f0d2b798bd789ec0e82d00025e1f009a5c82e4f62049123230b514e5b577cff2be09eb632b71b41080e1b9798e76f770b479075ded6dd40543f83b3ebd7c5e7a58f92797d4a3aaca5bd706397ef6230bb2b54848a34f9f8bd94a64195553736a7b6260881303431cacaea2abbd8d83d555abce4f2d45f9c587578c9f4965c19231e4e6a23e3b7c8a9a9b8c95f48e4d72b4bb87deb1d43efcd45294bd6f9fb45c06387850cfc6ef2fa7ae80f9a30022926c58216756f490b7aeddb22e9147d8b04222d1d4c7a65935462e6c5821a2521422184521825114221845218261e9d249892c5356ea4f7bcebbe84292d25f2398f5ca411b638910599be746e4e38c6dee47c7513481aed935094b84f8b5dbfceeea8f3237722b68eecc4b236b74205b531c4304a32844308a4204a32844308a4204a328443036ac90dd95a99b0c1ca9afb53093d51485008792370210006708c9f31f90873c6eba56dcc6e9e5967a6a03e99770ccc65421236a9321712373a19cdb1eacaee29de79f617b30759b258f2cf3cf2f1ce66bbb1a8d482f2f2433ffc1bd575ae0739e3fb04dcee16696392c9da02564bcfdc10b2c29ab2f2dea6a6fa363c5a53ebb2bcbd3de4b4b4af38d2c55d3b8b7628bb76f6a86affefd3fd973d6015385ac62611e86af27beb4526419ebc6107f29b4ee81dafa8cdf4add68583fa8575543cb1e08e43e0e3819eb850078bcd0d496f53bdc1b01318424098561d73e0857656feb50c41202891e52d7043bdb123d6783219e9024c150626ca9aab63a1353115708247a4b6d7d428ccf3eff69ad10c41692243945debacdf183be3d8424d9baddf153647b0981d414b96ea7237b8bfd8424096f4e4c91d7b8cd915db1af1048f4908656474d91ed2d248983a6c8ce1002a929f2ce365b4f919d23244930949a22db10e70949b2753bec6ab7dd14d9b942203545b6d19ecbff010d82e3f3a9bc14f70000000049454e44ae426082
1	Limpieza	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000a7549444154789ced9d5f6c1c471dc7bf33e7fbb3f7c7e73fb19dd80976e3d8286d9ab606ab4422364aaa484d2a815281f314514085940744284f11112245a214f28248c443699010181e502b55a99222449287284aeb124313294ed298c67f0f9f73be5beffddb191eae67fbecdbbbbddb9dbdbdcb7d244be79dd999b9fbdefc7e33bf999d237f6afe26471ddb402bdd803ab9d405b11975416c465d109b5117c466d405b11975416c465d109b5117c466d405b11975416c465d109b5117c46654ad20324f625a8d40e6c94a37c5541a2add8042dcecde86d78e7ea3609e97cfbd899d939f2248247888addf8e2eaab687ac25c1d39867512c301969b04a37c710b616e47f4d8d45f32c3635afbc967912b3ea12224c014375aebbd95a905053b0689ec5a6a69cff1938223c5eb5fec5d6821881816381c998654b88f374a59ba31b5b0b120a9666b2f291e42ae659142116ab0aff42ecbac9e1d4d111c45d4e309afb9d4941055fd362ca18dcc9045e3ef7a6ae7283c48300f5808298d95cd3b0ed38f156f75621e546781c513581209510206e217518c1d6264b140c1c8b6c19d36ac476fee59114244b1a0cf32c8a3916b58d7f79a405c992e0694cab112c30b9e2f3175bf990c9cded90dd19bbde3d3b9f93a68215fcb0eef53c0600f0c4e3e89c9d29ab7e9927a1a82904881b412a955586516c35ca3a7574c4b033df7eff13dd23ae423480a2997a2111a7e1b24aa16eb234488321c46298635124b96a59bd75418a90e069ccb225cb0297b6f2217666ad7f1139b1acf79012c8062e67d52561814b5bf410e697901ce847679b0faa8b83391ce05289b368250ea2aad8d2e6071d1e041bbb09446521ed4d836181c9889184e90b63151b6531bf84c4f0d39047f621b5434c98844f4c828d9e07bbf48130710040224e34532f1a4c3038960bc2fc12e491fd9047f681fb2d1aebc796c146cf431d7d4fa83066042e2d152439d08ff02fbe679d10eb892d23fde35f818fdd145605054133f5c2475c65dd6f9920caa13d7878e2a8155515453d7516ecdd4b39d71cef2de6e679bef03a4b315cc48126e22dd9bf58e2d4e5917d58fac1d7c556125b06fc5e5d591d3f3906f87d60a3e7b1dcc63139c44094dc6f347f5145f7650a6fa83cf393e42ae679146ed28056ead3ed5f840b121f7a4abc18c08a8f207ddda05f1904191e04d9bc4933bfe38747819910e4f96bb875580594752192c32a36dd24650b92251bb8ccc6c78af917c78bd2d33f35546301985fc2c26f8f032eed78909262f86f248e45258d45258d16a9bcd8111fbb097ef506301302bf7a036cf43c301b02f9fc63201a3de7c613ffc6bd6df79068cc6fb5233d1cb14ea063dcf8e8290915319e002104ee02664c680f897de7054d07aea418deb915c2f5a968ce754f03c5504f13867a9a2039f57f1064e0f10dd7d8bb97c02e7d00c7c963a0435fdc90bec81f20b2553b4e15f91c875366001cbadb5188ecc2581471cdc0a530a7cefc12e62e9ece9ba6a418ce5c9bc27434a1797f67c08d97beb0597f8f892d23b5ff5b9ac98e375ecd2b0a00a49efb361c7f7d9073cda853d7839b34a09978e122ab820beb21caa13d9a6917ee840b8a0100d3d104defa7016af3cdba5afa7f8bd20fd3de0b7efe74d567f7616e48faf03add340f222b83ab792e6f8cdc63008fd656ecfc51c057fdf0d3e6ede4796e069ccf225f8880bcdd40b0a62bd206125852bf71fea2a633a9ac05b633378e5d92e5df9c9c0e39a82202a835df935c8feb18d691d79ca7a72dd5afb9300792e0976da07fe7e79730c2dd6062e850417d52dad9ae190cbf72325957537ace0b24e01e90bc39a6964773abf1825428fcb20bbcddf18910d5c0a112439d0af99f69fb958c9e55d98084349155f8b207ddd205bdaf2a77d77b9e47a35eb31b1acf5081124d597bf778495141695d2bf5df13433dc4bc876f356fdcc2c6b3d8204d996f7faddb052769917ef8411565245f3d123078180afec7a2a8d10417820ffdc23bc6cccf65e9c0817cfe4f7c271e4f98d6dba6ade660533cb5a8f981ea2e1d08df41000b83e152dbb97f0b73d86ea1655d67a2c5dc2d5e3988b313a3e5f3c93df9b8955ad818f37809d366ecad8699fa97391f5582a48b1c9a01eee86155d3d8d1e1a061d1eccb9b6224ab28c59f81c152e0650a59b1cfe3c3e573c1300c7c9632bc360b23b0dc7b908e87119702d16b9330f1d0cf4b80cc7b98890794816cb0431c35c655954d2b8a0d7c1bff12a10f081ec316f97889965adc73241cc30576bb97827accb7491be6e349c3d0968cf554ba757dc3cc416db80cae5f71fcee0475fde5634224cfaba411abb01dc06007cacb8104a95f65d6c73323c21897f88b4aa0589a799fe88b0e4013e33fda11445285dc61a87057b33aad2a9af653a9ac0996b53c57d547adcb43a3744824da4ea05014a10e53352bcf475f272ee29879a1004284d94876ae96fbb9c7bcaa16604010a88a2de35bd2e5111df9a1204c888f2da3fef636a697598cd9980eda37e31fb0b6b4e102033fa3a736d2a47942ca154f93b488cdcab17cb046996ac1d61e788c266cdafa043ccd354960952ee063823c4d30ca3e3f348a7cc17847454910f717d3491f77a2544998e26f0d1ecea3abe91d19215232d4b7d88d5662b8b925c5a799d34309fc8b9d757034ebdb7a532cf8574791f14cf542a82028c4204a1b1fcdb6476b456e8419d352cb3f27b88917bf522449086dbf9bf919d81ca1f8724abe57fa846eed58ba5264b7252eceab07e8bcef6c6fc830c23880a300a11c439f1a9665a2504598b9120a1150146218238661634d376b5fb4b7aeec36c1ec961af73427b54233929f6763769a69b4daf00739545c4660761923bef688b32d4d3549149a2192647b4d9122688d6480bc8f492afeed47e2053140fd3c6dfae1965144258e99ecbff2a98beabc387bd3de24d57a788496116016b22c204718ddd2e9ae76b3b3761b0abf861c94608b88ced272e04f19b1ff11526088d29f05cb95134df91dded7869608b9091576f8b843ddb56cf8f9f2f67a7c93acc28a31042a37dbebffc03f1bd4f15cdb7abc387132d3db832f910d71fe8dbe1ae85e4a4d8d5eec7e0d6007a5b247039046147f30a5813112a886bec363c576ee8124572521cd8d182033b5a30b594c0c7f332ee2c28988e260a6e5c909c149d0137ba1adde86d91364c3cd73e6d6b3ad5260800044ffd0189bffdbca41380ba1a331ff0811dabd7d63f0ee769a0e86a2c2d3666c6c44ef4e450f8d493c614b47eff3448cc98736d919ce86d9156fe748bc15717a7ccd8ef9d53868035114b568c9c130fd07ef804165f3f86e4337d5654b98a7a6fe5658f3b8d36a731557c74f57e115b812c5bc2cbf694e4403f960fee8172f04b5655bd428fdb5e07efe7a3a2275b2707fa9178a61fea96d6cc9f5f82aaf148b55ef8c4241095c16742999381dc7f0739f489492dce85fdce0bfeb6b96b3cb63a6a1cc89c68106231246cf633125661bb8d7214041d34802011f7a4ab9db19d20598254423b0dd8f6a78944615b4100c0431ad0e908163c81add6b0b520c0a367c26c2f48962095d046fd356fc2aa46102073a4f7664763ce9178b546550902647ef966336db4e54fde9941d50992a5997a6bd28455ad20406d9ab0aa1604c898b0761a28fbf07bbb51f5820099a1712bf5a195faaade84d58420597cc485761aa86a1356538200999f89a86613f67f7e5fd2e0b2d262e10000000049454e44ae426082
13	Snacks	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00000a0b49444154789ced9d5b6c1c571dc6bf3397bd8dbd5e1bdf62c78ed3d62589936095540d299552b5a1095551830479a954a004f1c0431069251efa52043c3412052488221e52212e116a2a845002b4e212d569a16d1c50d224a46513dbf1651dd7f1de7776e6f0b099cdce7ad7de9d393b73d6999f646976cf9973ceceb7ffcb39337b4c1e3bfe04850737086e0fc0c38c270867788270862708677882708627086778827086270867788270862708677882708627086778827086274805d2b3292426e2a0baf30be1929d935fdd7d048ad26d7a4f1d1f43fcdbfb8baf3bfe3a6b2a4f1f7f19e9578f143affd42e845f79dd54be74683ff2e7c7aa9697d7093e7b18c1af3c5f571b0b8ff6148f5b7ff43ae4d15d553fa3367f05df7af765c4b46cd53a2cb16521272fbfc66a1cdc72f2cc514c7d1c77ac3f5b82fc7efa2cb4f80cabb1704772f23d1cbb7e15e9b914320b1947fab41d432e44df64310e2ef9e53bbf291e67173248cfa61a1e576c0bf2c71b6fb118077768f169bc3679c5f45e2e9e43722ad150516c0bf25e3e013a7f65f58a4dc69973cb930900d0b21ae2d1256859ad21fddacab20ca2e78fe3bed6010000bd3103f99ea16299f8cf9f9b3b4c5d2d964badfab272b9550759a17c599dd4d5badb281d9f14fd13c4dc7953b94a097e71f1ef553f2fd529925309043a83f0857d55eb5981b078eae40b4b317cfde6148bf170c169da821fea9d35d5f5770410e80830eb9bc9c4f07fbe208b66b8619cd67e8159077b2682fc27d0c2a2196e38434375d56719ec992d9dac152b19a701242c5c1656c19e99207312dbe0e616e3b01e0f8c609f5bca596e8399201fad110b395747fca804d5a9ad993d3b97e55f1b825ca56c2cdd6ab067264852105935c51ec50f323200f4770072f5a9d72c244bf1a31a46b0d755bde673984c0c01ce33ad901fd8b21ec4781d5b027deb32a0e64dd5a629b3cb5144cb6a484cc4a1f4b740f4affea5657a832ac5b39594d2150622cb535b3b017d25a84e919888d714ec990ab256027ba348cfa590994faf58c7bb855b82dd0cab16b28b59a4a69355833d5341d6ca5ca4d1a849b56ab0671ac5669b44105da3c83e300ce435080b7188738b10a76e1653decfadf70300c6660b3e7fff5010e71754fcfba60a00d8d5e3c3bd610963b3397cb894afdcc92a540bf6ecd30a5e50fc404e5b96490100288520098024007d1dd0fa3a40284562a2e030f6acf7637b878cef00984e69581712f10c0af1f12f9359f4292246da253c331c44324f71f462127f9eacff210823d807bb43c5657ca68224457eb22cf2f9070a07aa06dc4a951502fe50d9477ff89338a5527cf574ccf4f6ba90f933ed59efc7472556a148047bd6fb2d0962909e4b41cf69087406d90ac2d50263325bb01259043a5bcb0a49c55314996047afbff163ab4076310b5dd5d75e964522853b974859fbc6bef0601b760c2a204265d12ad1d522e3078fb463dfc6205a7cd62fa99a54d79620f4c9af813cf11cf4d6faee6794230a8024d77e693a5b447cb63f80ef3e14c1ef9eeac6be8dd63d45730b32bc0e18ea02220a68d0876cfe3c528b7f406ed766686de56eca19149960df46eb5f88a6ceb2c8e8d09d638d40ccaad0f285dc9e521d80334986e233f793a86331b11cd705218136483b0f824406a18dff165a746cc5fa42ef56889bf6822e4e40cb470ba92b008814a22c14057112b1cccffcf7636b7313c0659745026d081cfa17f4e818f26f1f83bcfbf965e54690167ab72278e85d08bd2390771f06890c80d07673fd9240ac3bf4e43aa93df6d784ab16228e1e000984417ab78246c7903bfd2200c0f7f44f000042ef08486400d9e35f84bcfb30b4e818f2e327e07bfac700001208836ab72ab64d1dfa250129370f00e373d6e7244c2da4de7b22daa553a09925f8f6be04dfdeef81660a17571afd32e8e275a8a75f040984216eda0b616817e8e275d3f9349534bd26cdb2fcbf024c05d99649d4559f2e4e20f3ca83d02e9d8630f41904bef9e69d794485ba24d0061268030088430f83688aa90e2125f1c32113112acc57a693d69f3c713da88ba307901f3f01ba380169e741889bf615cbf4990b000012192cd6259101a87f3b021219843ef93e707f57c57675ad7182f4a425205c38ae1443669a551069e737208d1e40e6e86340e616a49d078b6542ef56d0cc2de8d1b3107a47a0de8e2ffacc85a26b2323cbad8990c61b87586288f5cce86bc1750b31dc10e9dd0a7de602b4f113d037ed8376e9140020737c7fc5f3d289206e9e6dc5c096b2f64401d4988b68006970582917e4c6350d1d59010b7e6be9b7ab82e4df3e56b40c7de642c152505d048385d90e5cbbbc01adc32b87400a0a526521b15184558217ce85f1d36d714c29f5bb2ed72d243f7ea2aefab98c0fd72e6fa85aeeece50704d1dc636a9222a0113cf7410b5eda5139255f09a682287a637ec4528a9a0ea05dbe7d11a214f33f534de554a7c549a120aa2015a22ea5b4e2fbd5da2947cf01b9db37479392b97f7581a25d2668a3d67c2553413666577ea282054afb126e955ccbec872bf96a7afbaf5a99758c5ef358debf8f00b33e6b31a429577be550e385b7cbfbedd61eb86e4e4194d4ea955c266631cb622ac83d3967beb9cd6021d742d6e22953419c08ea002029fc0b120d595b826f4e9715e2db65c5fc3a52a2b5a4a1295d96206910fdd67fa5d468e62d6658006341420eb92c005c0b72b1555dbd52159809e254fc30f0879ddba1a75ea216033ac05010a7dc9501cf9956cccf81204e5b08cf7311ab292fc0501027964d4ae13586d811036028484fdef90be40bd777cbd809ae599c7f183013a4db0541789c8fc46ca4bc401307750090399cb15f0c7362214ece410c788c2356974c0c980852efe33face06d2e9212a9e52513032682b8113f0c78725b76332c809520aa7b82883e7edc965d770534b9cb02f89a20f263212eba2c9ee62256ef129662fb21872ed18ff6a78ea2747d33fb8f37a0cd17f67c97376f87bc799be99cd4c95f158fcbcbf5d82c3267de28be0e3cf23884ae1ed3f9a57568880258b4fb3198606795d7c0b620bbdbee05eddf617a2ff3cef78b9be1e3d93e488f9bcbd3279fbcf3a2ac3c1f1b43fae4afef0cf0912f412e6bbfbc0e19de0f9a72662bf06ab07057000397f5e9c8fd2cc6610b69c390db4340ccc7812054a7e8ebdeb67ac506236fdee4f610f8b090e45402a14f0c3319881da40d836e0fc1d64da9526c09f250df96d52b3980d059db2ed48dc4ee2aaf81ada0fee87ddb990cc22ed2864193db523fb8e4f81858a4bc0083bddf8dbd6a1bf5df02ee366c6759442050fa5b0adb1d79d886c955240241685d7d1bb6785486d9d75af48b50fa5b3c516cc2d4cf78a2d887b9e317fd2242eb94d52b7a54a42191580a4a0876dbdbb3ea6ea561a9912fecf344b1404373554f94fa69f8e4c117f6c11f716763c966c491d95ca033085f6b736cb2ec368e4daf833d214f941a7074bd23d81382acc84e76d97438be0015ec09d5f48f4dee561c17c4588cf444a98c2b4bb49e28d5716dcddc10c55bf732e3ee36b19e28cbf83f895178537a6279d90000000049454e44ae426082
14	Velas	\\x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000000473424954080808087c086488000000097048597300000dd700000dd70142289b780000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a0000071249444154789ced9d696c145500c7ff33bbd3ee02edb6b4d016b69452b180341220625208c41891a48648828246408246418d1a02221ee807820a6a38128404c3919848a2c16a404522027258a0f4c0067a6fa1dd760bdd967677676667fc80b4d676b7b3db99798feefb7debce3bfee96f77debc39de7019eb4a5430a881271d80d11b2684329810ca6042288309a10c2684329810ca6042288309a10c2684329810caa05ac8f8610da423980eb54256e71cc4c9b98bf154c671d2514c854a214b338f6063ee0e08bc8c9dd33ec09cd40ba4239906754266249561cbd44fc07177af0a08bc84bdd3df8999dd17554292e2bcd83b631d045eeaf579a2d081afa66f80c0cb8492990755423e9cf425d26d2dfd6ecb735460f584032627321f6a843c3af2329e71fe14b6cc9b13f761dcb09b262522033542de7e704ff7b8118a783e803539fb4d4a44062a84cc482ac3ec94224d6597380b9166f3189c881c54085934f6a8e6b2022fa1207de8ce4d880be13815f3d34e4654677e7a64e5ef27880b71da9a30c6ee8ea8ceace4e23e87c64305e24222950100022f23cb7ec38034e4212e24ddd61c553d475c87ce49e880b8107f303eaa7ab262d139091d1017d2e81f1d55bd163145e72474405c88ab6b0c1435b218b72547d4226987b890db920397db1e8aa8ce71773e549533281159880b018063eeb91195ff39c2f2f713540839dc50a07970770752f15b73bec189c8418590e6400abe712dd4547657d5720494e88eccc2c1712a26275ed7bddd48a14208006caf7a11edf288b065eabac6e250fdd386f4fffea4ed3896bf0c8f8dfad390f6b5428d10b73f159b2b5e0b5b667de986a8e72de17836f347bc32e110045ec6eee9ef62524295ee7d68851a210070b07e51c8f1e1ebbac5f8c3334bf73ea72797e3d3a99bbbff1e61edc4be996b31c2daa97b5f5aa04a88aa7278fdca47a8ef1ad3ebf38bb7f3b0e9ea5bbaf727f012b6e57ddce744e5f8610d589fbb5bf7feb44095100068131d58727e273c816400406d97132b8bb6425204ddfb7acef9037213aafbddb622eb5b640f77e9dee740502704006abb32b1f4c20e5cf14ec192f3bbd0228ed4bd0f9e53b0e681d097832d9c8257271cd4bddf81a052080094b7e762c1e9fd7d765f7a3133a9144e7b63d83205e92760e58286f41f0a6a8518cdbc5167072c9314e7c5b4a47213d2f410b342b2866bbbc065f61d9356431b57158c0f34c11634ff726b87d50e577c2a9410dfb964a14d533b23e3b595d30bc3843803ad3870ed33648ae46ed929b56761d9a4b5b8c3dbfb6c6b159335b571ef68cf2c0cdb65ad74ff42540600e4f9eab0b0f6fb7eb7d5753935b551d799a967a401314c48aaec35aae988486d77416abed6e7f3132d039f31be253a50ec9d6244ac90c4c4a02e3557406ae92de572db14d47685fff61f697c02c108af660e969810020092bb02524bcfe97555e5b0a37279e8f28a15bbab5e30235a2f6246080048eebf7b4939dc508012efe47ecbeea9791e2e5f8659d1ba892921406f29b26ac1da928d7d2e785dbf938dcfafaf22112ff68400ff4af1540200cada73f146f1a6ee9b26da440756146d832f6823922d26850080d47415d2adbb677a0b1b1fc71795ab1050e2f1d2a52da831f950f7bf183a53a71de96619a0721052b2b1f5dacbf8eec693a8ee1c473453ccfe42ee213596426ead0100e2320026040020369642be554b3a068018db654d55bd784fbedaffc6bab3e09b12c059b50de6126f41a163068e8c9aad63c218139204117394fe1fbb0600f8c26ceb8739de329c1132e049ca1964b21ed82e6b10f05091d8700eb257bf8787989041a2aa0a44d745c8adb5bab4c784e884d85802a929c4f814014c888e489e4a88378a0135fae5f499109d916fd723e0fa0b5095a8ea332106106c6f82bff61c108c7cf52226c420944e0f7cd5a7a0cafe88ea312106a2063ae0af3e0525a0fdc66d26c46054d18740cd69287e6df718302126a0ca01f86bce207867e033014c885904658875e7217bc32fc0c68498c8dd597d11644fe827b4981002884de52167f54c0821244f25020d97fbccea99108204db5c08d4f79ed5332184097634c15f7bb67b56cf845080d2d90a5fcd29a8928f09a105d5df017ff51926842654a98b09a10d2684329810ca6042288309a10c2684329810ca6042288309a10c2684329810ca6042288309a10c2684320c13227174bcdf43e47a16edb7447f53ba691826e47741dbf24746a280c349aee7b51663549faeed8be0e1e6faaec535180c7bc6b0d0b900126fc5232d45e054f3df612b83c3514b068ab89e154d25f03864c9d2a57d051c7ee5d3e0419c2eeddd83cb585762e80f59957c08345c82d2d96a64374306c307754eb0c3969d0f6174aed15d0d094c3bca1246e7c2969daff939f058c5d4c35e7e780a6c13e7c192986666b7f715a6cf43384b1ce2c7cd42dcd887018e4d83fe0fb1ff8835390bf69cb9e06c09a4225009d1af28674b803d672e849409246350c53febd128161c30aaa20000000049454e44ae426082
\.


--
-- Name: categoria_idcategoria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categoria_idcategoria_seq', 1, false);


--
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cliente (idcliente, nombre, dni) FROM stdin;
1	edder sanchez baca	10234567
2	paulo torres gaviño	47455489
\.


--
-- Name: cliente_idcliente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cliente_idcliente_seq', 1, false);


--
-- Data for Name: compra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY compra (idcompra, fecha, monto, igv, idordencompra, idproveedor, idusuario, idmovimientocaja, estado, factura, idmovimiento) FROM stdin;
8	2015-07-18	40.00	7.20	10	1	1	21	G		37
9	2015-07-18	2.00	0.36	11	1	1	23	G		40
\.


--
-- Name: compra_idcompra_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('compra_idcompra_seq', 9, true);


--
-- Data for Name: comprobante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY comprobante (idcomprobante, idserie, numero, nombrecliente, fecha, monto, igv, idmovimientocaja, idusuario, estado, descripcion) FROM stdin;
6	2	1	Edder Sánchez	2015-07-18	3.81	0.69	20	1	G	Por consumo
7	2	1		2015-07-18	7.37	1.33	24	1	G	Por consumo
8	2	1		2015-07-18	2.97	0.53	25	1	G	
9	2	1		2015-07-18	2.12	0.38	26	1	G	
10	2	1		2015-07-18	4.24	0.76	27	1	G	
11	2	2		2015-07-18	0.85	0.15	28	1	G	
\.


--
-- Name: comprobante_idcomprobante_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('comprobante_idcomprobante_seq', 11, true);


--
-- Data for Name: detallecompra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY detallecompra (iddetallecompra, idcompra, idalmacenarticulo, cantidad, precio, total, igv) FROM stdin;
\.


--
-- Name: detallecompra_iddetallecompra_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('detallecompra_iddetallecompra_seq', 9, true);


--
-- Data for Name: detallecomprobante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY detallecomprobante (iddetallecomprobante, idcomprobante, idarticulo, cantidad, precio, igv) FROM stdin;
6	6	199	1	2.97	0.53
7	6	175	1	0.85	0.15
8	7	1	1	4.41	0.79
9	7	199	1	2.97	0.53
10	8	199	1	2.97	0.53
11	9	556	5	2.12	0.38
12	10	556	10	4.24	0.76
13	11	175	1	0.85	0.15
\.


--
-- Name: detallecomprobante_iddetallecomprobante_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('detallecomprobante_iddetallecomprobante_seq', 13, true);


--
-- Data for Name: detallemovimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY detallemovimiento (iddetallemovimiento, idmovimiento, idarticulo, cantidad) FROM stdin;
41	36	199	1
42	36	175	1
43	37	484	10
44	38	484	5
45	39	1	1
46	39	199	1
47	40	460	1
48	41	199	1
49	42	556	5
50	43	556	10
51	44	175	1
\.


--
-- Name: detallemovimiento_iddetallemovimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('detallemovimiento_iddetallemovimiento_seq', 51, true);


--
-- Data for Name: detallemovimientocaja; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY detallemovimientocaja (iddetallemovimientocaja, idmovimientocaja, monto, descripcion) FROM stdin;
16	20	3.50	Pago por Venta
17	20	1.00	Pago por Venta
18	21	40.00	Pago por compra
19	22	120.00	Luz, agua, internet
20	23	2.00	Pago por compra
21	24	5.20	Pago por Venta
22	24	3.50	Pago por Venta
23	25	3.50	Pago por Venta
24	26	2.50	Pago por Venta
25	27	5.00	Pago por Venta
26	28	1.00	Pago por Venta
\.


--
-- Name: detallemovimientocaja_iddetallemovimientocaja_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('detallemovimientocaja_iddetallemovimientocaja_seq', 26, true);


--
-- Data for Name: detalleordencompra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY detalleordencompra (iddetalleordencompra, idordencompra, idarticulo, cantidad, precio, total) FROM stdin;
7	10	484	10	4.00	40.00
8	11	460	1	2.00	2.00
9	12	928	5	3.50	17.50
\.


--
-- Name: detalleordencompra_iddetalleordencompra_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('detalleordencompra_iddetalleordencompra_seq', 9, true);


--
-- Data for Name: detalleventa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY detalleventa (iddetalleventa, idventa, idalmacenarticulo, cantidad, precio, descuento, iddetallecomprobante) FROM stdin;
\.


--
-- Name: detalleventa_iddetalleventa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('detalleventa_iddetalleventa_seq', 20, true);


--
-- Data for Name: flujocaja; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY flujocaja (idflujocaja, iddetallemovimientocaja, idcaja, movimiento, monto, montobefore, montoafter, fecha) FROM stdin;
21	16	1	I	3.49999979999999988	161.300000000000011	164.800000000000011	2015-07-18
22	17	1	I	0.999999940000000032	164.800000000000011	165.800000000000011	2015-07-18
23	18	1	E	40	165.800000000000011	125.799999999999997	2015-07-18
24	19	1	E	120	125.799999999999997	5.80000300000000024	2015-07-18
25	20	1	E	2	5.79999999999999982	3.80000019999999994	2015-07-18
26	21	1	I	5.20000000000000018	3.79999999999999982	9	2015-07-18
27	22	1	I	3.49999979999999988	9	12.5	2015-07-18
28	23	1	I	3.49999979999999988	12.5	16	2015-07-18
29	24	1	I	2.5	16	18.5	2015-07-18
30	25	1	I	5	18.5	23.5	2015-07-18
31	26	1	I	0.999999940000000032	23.5	24.5	2015-07-18
\.


--
-- Name: flujocaja_idflujocaja_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('flujocaja_idflujocaja_seq', 31, true);


--
-- Data for Name: kardex; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kardex (idkardex, idalmacenarticulo, movimiento, cantidad, fecha, stockbefore, stockafter, iddetallemovimiento) FROM stdin;
\.


--
-- Name: kardex_idkardex_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kardex_idkardex_seq', 64, true);


--
-- Data for Name: lote; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY lote (idlote, idarticulo, fechavencimiento, lote, estado) FROM stdin;
\.


--
-- Name: lote_idlote_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('lote_idlote_seq', 1, false);


--
-- Data for Name: movimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY movimiento (idmovimiento, idtipomovimiento, fecha, idalmacenorigen, idalmacendestino, estado) FROM stdin;
36	9	2015-07-18	1	\N	G
37	5	2015-07-18	\N	1	G
38	1	2015-07-18	1	2	G
39	9	2015-07-18	1	\N	G
40	5	2015-07-18	\N	1	G
41	9	2015-07-18	1	\N	G
42	9	2015-07-18	1	\N	G
43	9	2015-07-18	1	\N	G
44	9	2015-07-18	1	\N	G
\.


--
-- Name: movimiento_idmovimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('movimiento_idmovimiento_seq', 44, true);


--
-- Data for Name: movimientocaja; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY movimientocaja (idmovimientocaja, idtipomovimientocaja, referencia, fecha, monto, idusuario, estado, idcajaorigen, idcajadestino) FROM stdin;
20	2	2	2015-07-18	0.69	1	G	\N	1
21	3	2	2015-07-18	40.00	1	G	1	\N
22	1	1	2015-07-18	120.00	1	G	1	\N
23	3	2	2015-07-18	2.00	1	G	1	\N
24	2	2	2015-07-18	1.33	1	G	\N	1
25	2	2	2015-07-18	0.53	1	G	\N	1
26	2	2	2015-07-18	0.38	1	G	\N	1
27	2	2	2015-07-18	0.76	1	G	\N	1
28	2	2	2015-07-18	0.15	1	G	\N	1
\.


--
-- Name: movimientocaja_idmovimientocaja_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('movimientocaja_idmovimientocaja_seq', 28, true);


--
-- Data for Name: ordencompra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ordencompra (idordencompra, fecha, monto, fechaentrega, idproveedor, idusuario, estado) FROM stdin;
10	2015-07-18	40.00	2015-07-18	1	1	P
11	2015-07-18	2.00	2015-07-18	1	1	P
12	2015-07-18	17.50	2015-07-18	1	1	G
\.


--
-- Name: ordencompra_idordencompra_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ordencompra_idordencompra_seq', 12, true);


--
-- Data for Name: pago; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pago (idpago, idtipopago, monto, estado, idcomprobante) FROM stdin;
\.


--
-- Name: pago_idpago_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pago_idpago_seq', 1, false);


--
-- Data for Name: proveedor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY proveedor (idproveedor, nombre, preventa, mercaderista, direccion, telefono, descripcion) FROM stdin;
1	Backus					
2	Gloria					
\.


--
-- Name: proveedor_idproveedor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('proveedor_idproveedor_seq', 2, true);


--
-- Data for Name: serie; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY serie (idserie, idtipocomprobante, nombre) FROM stdin;
1	1	001
2	2	002
\.


--
-- Name: serie_idserie_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('serie_idserie_seq', 1, false);


--
-- Data for Name: tipocomprobante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tipocomprobante (idtipocomprobante, nombre) FROM stdin;
1	Factura
2	Boleta
\.


--
-- Name: tipocomprobante_idtipocomprobante_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipocomprobante_idtipocomprobante_seq', 1, false);


--
-- Data for Name: tipomovimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tipomovimiento (idtipomovimiento, tipo, nombre) FROM stdin;
1	T	Transferencia
2	I	Cargo por Ajuste
3	I	Cargo por Bonificación
4	E	Descargo por avería
5	I	Cargo por Compra
6	E	Descargo por Robo
7	E	Descargo por Pérdida
8	E	Descargo por Vencimiento
9	E	Descargo por venta
10	A	Anulacion
\.


--
-- Name: tipomovimiento_idtipomovimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipomovimiento_idtipomovimiento_seq', 1, false);


--
-- Data for Name: tipomovimientocaja; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tipomovimientocaja (idtipomovimientocaja, descripcion, tipo) FROM stdin;
1	Egreso pago de servicios	E
2	Ingreso por Venta	I
3	Egreso por compra	E
4	Ingreso Anulacion	I
5	Egreso Anulacion	E
6	Anulacion	A
\.


--
-- Name: tipomovimientocaja_idtipomovimientocaja_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipomovimientocaja_idtipomovimientocaja_seq', 1, false);


--
-- Data for Name: tipopago; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tipopago (idtipopago, nombre) FROM stdin;
\.


--
-- Name: tipopago_idtipopago_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipopago_idtipopago_seq', 1, false);


--
-- Data for Name: tipousuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tipousuario (idtipousuario, nombre, p_movimiento, p_venta, p_comprobante, p_ordencompra, p_compra, p_detallemovimientocaja) FROM stdin;
1	Administrador	15	15	15	15	15	15
\.


--
-- Name: tipousuario_idtipousuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipousuario_idtipousuario_seq', 1, false);


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY usuario (idusuario, idtipousuario, nombre, usuario, password) FROM stdin;
1	1	Edder Sanchez	esanchez	e10adc3949ba59abbe56e057f20f883e
\.


--
-- Name: usuario_idusuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuario_idusuario_seq', 1, true);


--
-- Data for Name: venta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY venta (idventa, fecha, idcliente, monto, idusuario, estado, idmovimiento) FROM stdin;
9	2015-07-18	\N	4.50	1	P	36
10	2015-07-18	1	8.70	1	P	39
11	2015-07-18	\N	3.50	1	P	41
12	2015-07-18	\N	2.50	1	P	42
13	2015-07-18	\N	5.00	1	P	43
14	2015-07-18	\N	1.00	1	P	44
\.


--
-- Name: venta_idventa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('venta_idventa_seq', 14, true);


--
-- Name: almacen_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY almacen
    ADD CONSTRAINT almacen_pkey PRIMARY KEY (idalmacen);


--
-- Name: almacenarticulo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY almacenarticulo
    ADD CONSTRAINT almacenarticulo_pkey PRIMARY KEY (idalmacenarticulo);


--
-- Name: articulo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY articulo
    ADD CONSTRAINT articulo_pkey PRIMARY KEY (idarticulo);


--
-- Name: caja_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY caja
    ADD CONSTRAINT caja_pkey PRIMARY KEY (idcaja);


--
-- Name: categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pkey PRIMARY KEY (idcategoria);


--
-- Name: cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (idcliente);


--
-- Name: compra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY compra
    ADD CONSTRAINT compra_pkey PRIMARY KEY (idcompra);


--
-- Name: comprobante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY comprobante
    ADD CONSTRAINT comprobante_pkey PRIMARY KEY (idcomprobante);


--
-- Name: detallecompra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY detallecompra
    ADD CONSTRAINT detallecompra_pkey PRIMARY KEY (iddetallecompra);


--
-- Name: detallecomprobante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY detallecomprobante
    ADD CONSTRAINT detallecomprobante_pkey PRIMARY KEY (iddetallecomprobante);


--
-- Name: detallemovimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY detallemovimiento
    ADD CONSTRAINT detallemovimiento_pkey PRIMARY KEY (iddetallemovimiento);


--
-- Name: detallemovimientocaja_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY detallemovimientocaja
    ADD CONSTRAINT detallemovimientocaja_pkey PRIMARY KEY (iddetallemovimientocaja);


--
-- Name: detalleordencompra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY detalleordencompra
    ADD CONSTRAINT detalleordencompra_pkey PRIMARY KEY (iddetalleordencompra);


--
-- Name: detalleventa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY detalleventa
    ADD CONSTRAINT detalleventa_pkey PRIMARY KEY (iddetalleventa);


--
-- Name: flujocaja_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY flujocaja
    ADD CONSTRAINT flujocaja_pkey PRIMARY KEY (idflujocaja);


--
-- Name: kardex_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kardex
    ADD CONSTRAINT kardex_pkey PRIMARY KEY (idkardex);


--
-- Name: lote_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY lote
    ADD CONSTRAINT lote_pkey PRIMARY KEY (idlote);


--
-- Name: movimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY movimiento
    ADD CONSTRAINT movimiento_pkey PRIMARY KEY (idmovimiento);


--
-- Name: movimientocaja_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY movimientocaja
    ADD CONSTRAINT movimientocaja_pkey PRIMARY KEY (idmovimientocaja);


--
-- Name: ordencompra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ordencompra
    ADD CONSTRAINT ordencompra_pkey PRIMARY KEY (idordencompra);


--
-- Name: pago_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pago
    ADD CONSTRAINT pago_pkey PRIMARY KEY (idpago);


--
-- Name: proveedor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY proveedor
    ADD CONSTRAINT proveedor_pkey PRIMARY KEY (idproveedor);


--
-- Name: serie_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY serie
    ADD CONSTRAINT serie_pkey PRIMARY KEY (idserie);


--
-- Name: tipocomprobante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipocomprobante
    ADD CONSTRAINT tipocomprobante_pkey PRIMARY KEY (idtipocomprobante);


--
-- Name: tipomovimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipomovimiento
    ADD CONSTRAINT tipomovimiento_pkey PRIMARY KEY (idtipomovimiento);


--
-- Name: tipomovimientocaja_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipomovimientocaja
    ADD CONSTRAINT tipomovimientocaja_pkey PRIMARY KEY (idtipomovimientocaja);


--
-- Name: tipopago_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipopago
    ADD CONSTRAINT tipopago_pkey PRIMARY KEY (idtipopago);


--
-- Name: tipousuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipousuario
    ADD CONSTRAINT tipousuario_pkey PRIMARY KEY (idtipousuario);


--
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (idusuario);


--
-- Name: venta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY venta
    ADD CONSTRAINT venta_pkey PRIMARY KEY (idventa);


--
-- Name: almacenarticulo_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY almacenarticulo
    ADD CONSTRAINT almacenarticulo_ibfk_1 FOREIGN KEY (idalmacen) REFERENCES almacen(idalmacen);


--
-- Name: almacenarticulo_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY almacenarticulo
    ADD CONSTRAINT almacenarticulo_ibfk_2 FOREIGN KEY (idarticulo) REFERENCES articulo(idarticulo);


--
-- Name: articulo_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY articulo
    ADD CONSTRAINT articulo_ibfk_1 FOREIGN KEY (idcategoria) REFERENCES categoria(idcategoria);


--
-- Name: compra_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY compra
    ADD CONSTRAINT compra_ibfk_1 FOREIGN KEY (idordencompra) REFERENCES ordencompra(idordencompra);


--
-- Name: compra_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY compra
    ADD CONSTRAINT compra_ibfk_2 FOREIGN KEY (idproveedor) REFERENCES proveedor(idproveedor);


--
-- Name: compra_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY compra
    ADD CONSTRAINT compra_ibfk_3 FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- Name: compra_ibfk_4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY compra
    ADD CONSTRAINT compra_ibfk_4 FOREIGN KEY (idmovimientocaja) REFERENCES movimientocaja(idmovimientocaja);


--
-- Name: compra_ibfk_5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY compra
    ADD CONSTRAINT compra_ibfk_5 FOREIGN KEY (idmovimiento) REFERENCES movimiento(idmovimiento);


--
-- Name: comprobante_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comprobante
    ADD CONSTRAINT comprobante_ibfk_1 FOREIGN KEY (idserie) REFERENCES serie(idserie);


--
-- Name: comprobante_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comprobante
    ADD CONSTRAINT comprobante_ibfk_2 FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- Name: comprobante_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comprobante
    ADD CONSTRAINT comprobante_ibfk_3 FOREIGN KEY (idmovimientocaja) REFERENCES movimientocaja(idmovimientocaja);


--
-- Name: detallecompra_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detallecompra
    ADD CONSTRAINT detallecompra_ibfk_1 FOREIGN KEY (idalmacenarticulo) REFERENCES almacenarticulo(idalmacenarticulo);


--
-- Name: detallecompra_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detallecompra
    ADD CONSTRAINT detallecompra_ibfk_2 FOREIGN KEY (idcompra) REFERENCES compra(idcompra);


--
-- Name: detallecomprobante_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detallecomprobante
    ADD CONSTRAINT detallecomprobante_ibfk_1 FOREIGN KEY (idarticulo) REFERENCES articulo(idarticulo);


--
-- Name: detallecomprobante_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detallecomprobante
    ADD CONSTRAINT detallecomprobante_ibfk_2 FOREIGN KEY (idcomprobante) REFERENCES comprobante(idcomprobante);


--
-- Name: detallemovimiento_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detallemovimiento
    ADD CONSTRAINT detallemovimiento_ibfk_1 FOREIGN KEY (idmovimiento) REFERENCES movimiento(idmovimiento);


--
-- Name: detallemovimiento_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detallemovimiento
    ADD CONSTRAINT detallemovimiento_ibfk_2 FOREIGN KEY (idarticulo) REFERENCES articulo(idarticulo);


--
-- Name: detallemovimientocaja_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detallemovimientocaja
    ADD CONSTRAINT detallemovimientocaja_ibfk_1 FOREIGN KEY (idmovimientocaja) REFERENCES movimientocaja(idmovimientocaja);


--
-- Name: detalleordencompra_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalleordencompra
    ADD CONSTRAINT detalleordencompra_ibfk_1 FOREIGN KEY (idarticulo) REFERENCES articulo(idarticulo);


--
-- Name: detalleordencompra_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalleordencompra
    ADD CONSTRAINT detalleordencompra_ibfk_2 FOREIGN KEY (idordencompra) REFERENCES ordencompra(idordencompra);


--
-- Name: detalleventa_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalleventa
    ADD CONSTRAINT detalleventa_ibfk_1 FOREIGN KEY (idventa) REFERENCES venta(idventa);


--
-- Name: detalleventa_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalleventa
    ADD CONSTRAINT detalleventa_ibfk_2 FOREIGN KEY (idalmacenarticulo) REFERENCES almacenarticulo(idalmacenarticulo);


--
-- Name: detalleventa_iddetallecomprobante_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalleventa
    ADD CONSTRAINT detalleventa_iddetallecomprobante_fkey FOREIGN KEY (iddetallecomprobante) REFERENCES detallecomprobante(iddetallecomprobante);


--
-- Name: fg_id_detallemovimiento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kardex
    ADD CONSTRAINT fg_id_detallemovimiento FOREIGN KEY (iddetallemovimiento) REFERENCES detallemovimiento(iddetallemovimiento);


--
-- Name: flujocaja_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flujocaja
    ADD CONSTRAINT flujocaja_ibfk_1 FOREIGN KEY (iddetallemovimientocaja) REFERENCES detallemovimientocaja(iddetallemovimientocaja);


--
-- Name: flujocaja_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flujocaja
    ADD CONSTRAINT flujocaja_ibfk_2 FOREIGN KEY (idcaja) REFERENCES caja(idcaja);


--
-- Name: kardex_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kardex
    ADD CONSTRAINT kardex_ibfk_1 FOREIGN KEY (idalmacenarticulo) REFERENCES almacenarticulo(idalmacenarticulo);


--
-- Name: lote_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lote
    ADD CONSTRAINT lote_ibfk_1 FOREIGN KEY (idarticulo) REFERENCES articulo(idarticulo);


--
-- Name: movimiento_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY movimiento
    ADD CONSTRAINT movimiento_ibfk_1 FOREIGN KEY (idalmacenorigen) REFERENCES almacen(idalmacen);


--
-- Name: movimiento_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY movimiento
    ADD CONSTRAINT movimiento_ibfk_2 FOREIGN KEY (idalmacendestino) REFERENCES almacen(idalmacen);


--
-- Name: movimiento_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY movimiento
    ADD CONSTRAINT movimiento_ibfk_3 FOREIGN KEY (idtipomovimiento) REFERENCES tipomovimiento(idtipomovimiento);


--
-- Name: movimientocaja_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY movimientocaja
    ADD CONSTRAINT movimientocaja_ibfk_1 FOREIGN KEY (idtipomovimientocaja) REFERENCES tipomovimientocaja(idtipomovimientocaja);


--
-- Name: movimientocaja_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY movimientocaja
    ADD CONSTRAINT movimientocaja_ibfk_2 FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- Name: movimientocaja_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY movimientocaja
    ADD CONSTRAINT movimientocaja_ibfk_3 FOREIGN KEY (idcajaorigen) REFERENCES caja(idcaja);


--
-- Name: movimientocaja_ibfk_4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY movimientocaja
    ADD CONSTRAINT movimientocaja_ibfk_4 FOREIGN KEY (idcajadestino) REFERENCES caja(idcaja);


--
-- Name: ordencompra_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ordencompra
    ADD CONSTRAINT ordencompra_ibfk_1 FOREIGN KEY (idproveedor) REFERENCES proveedor(idproveedor);


--
-- Name: ordencompra_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ordencompra
    ADD CONSTRAINT ordencompra_ibfk_2 FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- Name: pago_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pago
    ADD CONSTRAINT pago_ibfk_1 FOREIGN KEY (idtipopago) REFERENCES tipopago(idtipopago);


--
-- Name: pago_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pago
    ADD CONSTRAINT pago_ibfk_2 FOREIGN KEY (idcomprobante) REFERENCES comprobante(idcomprobante);


--
-- Name: serie_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY serie
    ADD CONSTRAINT serie_ibfk_1 FOREIGN KEY (idtipocomprobante) REFERENCES tipocomprobante(idtipocomprobante);


--
-- Name: usuario_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_ibfk_1 FOREIGN KEY (idtipousuario) REFERENCES tipousuario(idtipousuario);


--
-- Name: venta_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY venta
    ADD CONSTRAINT venta_ibfk_1 FOREIGN KEY (idcliente) REFERENCES cliente(idcliente);


--
-- Name: venta_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY venta
    ADD CONSTRAINT venta_ibfk_3 FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- Name: venta_ibfk_4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY venta
    ADD CONSTRAINT venta_ibfk_4 FOREIGN KEY (idmovimiento) REFERENCES movimiento(idmovimiento);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

